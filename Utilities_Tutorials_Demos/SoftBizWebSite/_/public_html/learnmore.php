<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - Who are we?</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="who-are-we">
            	<h1 align="right">Who we are and what we do</h1>
            </div>
            <div id="content-wrapper">
            	<div id="leftpane">
                	<h1 class="maintitle">Learn more about <span class="green">Creative Web Solution</span></h1>
                    <p>
               	  In today’s business world, the need to be known in the most efficient and quickest way is very high. You pay for advertising expenses and through different media to make your business known to many.</p>
                    <p>Why make your business known?</p>
                    <p>Broadening the scope of your business means  sales and income. To widen your target market is also to make your business  grow. But is every business man ready to pay a lot for TV and print  advertisements?</p>
                    <p>Perhaps not.</p>
                    <p>This is where the internet plays a  vital role in disseminating information the quickest and most efficient way  possible to every user. Millions of people surf the internet every day and the  likeliness of them having knowledge about your business is very high. With  proper tool and techniques, like Creative Web Solution has, your business is  sure to be popular.</p>
                  <p>We at Creative Web Solution can  build your business the best website it needs. We base our designs on the  nature of your business, the products and services it offers and its target  market, and then we work with skill, passion and creativity. We guarantee both  you and your clients’ satisfaction.</p>
                  <p>Building a corporate website is not  easy but we at Creative Web Solution are dedicated to our craft. Not only do we  make beautiful websites, we also make sure that the website helps your business  increase its sales. By means of techniques like Search Engine Optimization and  Pay Per Click campaigns, potential customers and clients are sure to find your  business in search engines such as Google, Yahoo and Bing.</p>
                  <p>We also offer software development from planning to debugging up to maintenance. We have a team of software engineers whose expertise in programming are honed through experience. In case of ambiguous or incomplete requirements during the planning stage, our software engineers recognize them and will meet your needs based on what you require and what needs to be done.</p>
                  <p>So take your business to a higher  level. We at Creative Web Solution guarantee you your business’ success because  we are not only skilled to perfection and creativity, we are also passionate in  what we do.</p>

                    
              </div><!-- end of #leftpane-->
                <div id="rightpane">
                	
                    <div class="rightpane-header"> <h2>Explore more</h2></div><!-- end of .rightpane-header -->
                    
                    <ul class="rightnavigation">
                    	
                        <li><a href="about-us.php" title="About company">About company</a></li>
                        <li><a href="online-support.php" title="Online Support">Online Support</a></li>
                        <li><a href="technology.php" title="Technology">Technology</a></li>
                        <li><a href="why-people-choose-us.php" title="Why People Choose Us">Why People Choose Us</a></li>
                       <li><a href="mission-vision.php" title="Mission Vision">Mission Vision</a></li>
                        <li><a href="#" title="Lorem Ipsum" class="rightnav-active">Learn more</a></li>
                    </ul>
                    
                    <br class="clear" /><!-- don't remove -->
                    
                    <?php include_once("php-include/contact-quote.php");?>
                    
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  
    
	<?php include_once("php-include/footer.php"); ?>

</body>
</html>
