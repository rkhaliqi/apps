<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Page not Found!</title>

<?php include_once("php-include/styles-js.php");?>

<style type="text/css">

	body{ 
		background:#fffffe url(images/error-bg.jpg) repeat-x;
	}
	#error-wrapper{
		width:177px;
		margin:0 auto;
		min-height:176px;
		height: auto !important;
		height:176px;
		background:url(images/error-404.jpg) no-repeat;
		
		padding:216px 0 0 314px;
	}
</style>
</head>

<body>
	<div id="error-wrapper">
    
    	<h2 class="orange">What would you do next?</h2>
        <ol>
        	<li><a href=".">Back to home</a></li>
            <li><a href="sitemap.php">View sitemap</a></li>
        </ol>
    
    </div>
</body>
</html>