
    
    <div id="footer-wrapper">
		<div id="footer-inner">
			
			<div class="four-column-wrapper">
				
				<div class="four-col-1">
					<h1>Who are we?</h1>
					<p>
					
				  	Creative Web Solution is a web-based IT company that works with creativity, skill and passion. It is your best choice in web design &amp; development, as well as internet marketing and software development. </p>
					<p>
						<a href="about-us.php" title="Read more"><img src="images/buttons/readmore.jpg" alt="Read more" /></a>
					</p>
				</div><!-- end of .four-col-1 -->
				
				<div class="four-col-2">
					
					<h1>Contact Details</h1>
					<p>You may contact us through:</p>
					
					<p>
						Email: <a href="mailto:info@creativewebsolution.net" title="Email Creative Web Solution">info@creativewebsolution.net</a><br />
						Phone: <strong>1-520-225-0169</strong><br />
						&nbsp;
					
					</p>
					
					<p>
                    	<br />
                        <a href="contact-us.php" title="Contact us"><img src="images/buttons/footer-contactus.jpg" alt="Read more" /></a>
                 	</p>
					
				</div><!-- end of .four-col-2 -->
				<div class="four-col-3">
					<h1>Clients Testimonial</h1>
					<p>
				  &quot;I’m very pleased with how my website was remodeled, and I’m happy with the 100% business increase I’ve gotten...</p>
					<p>	
						&rarr; Adil Khan<br />
                        <a href="http://www.blazintech.net/" target="_blank" title="View Website">www.blazintech.net</a>
                    </p>
					<p>
						<a href="testimonial.php" title="Read more"><img src="images/buttons/readmore.jpg" alt="Read more" /></a>
					</p>
					
				</div><!-- end of .four-col-3 -->
				<div class="four-col-4">
					<h1>News &amp; Events</h1>
					
                    
                    <table width="100%" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td valign="top">
                            	
                                <ol id="news-updates">
                                    
                                    <li>
                                        <a href="blog.php#entry1"><strong>10/09/2010</strong> <span>CWS Launches Website</span></a>
                                    </li>
                                    <li>
                                        <a href="blog.php#entry2"><strong>10/07/2010</strong> <span>Message from the CEO</span></a>
                                    </li> 
                                    
                                    <li>
                                        <a href="blog.php#entry3"><strong>09/29/2010</strong> <span>How important are Search...</span></a>
                                 	</li>
                                    <li>
                                        <a href="blog.php#entry4"><strong>09/23/2010</strong> <span>Android Smartphones Please the...</span></a>
                                    </li>
                                    <li>
                                        <a href="blog.php#entry5"><strong>09/20/2010</strong> <span>Google Page Rank:A Mere...</span></a>
                                    </li>
                                    
                                </ol>
                            
                            </td>
                        </tr>
                        <tr>
                        	<td valign="top">
                            	<br /><br />
                                <p>
                                    <a href="blog.php" title="Read more"><img src="images/buttons/readmore.jpg" alt="Read more" /></a>
                                </p>
                            </td>
                        </tr>
                    </table>
                    
                          
					
				</div><!-- end of .four-col-4 -->
				
			</div><!-- end of .four-column-wrapper -->
			
			<br class="clear" /><!-- don't remove -->
			
			<div id="footer-lower">
				<small class="FL">Copyright <?=date('Y'); ?> &copy; Creative Web Solution. All rights reserved.</small>
				<ul class="footer-menus">
					<li><a href="." title="Home">Home</a></li><li>|</li>
					<li><a href="faq.php" title="Frequently Asked Questions">FAQs</a></li><li>|</li>
					<li><a href="testimonial.php" title="Testimonial">Testimonial</a></li><li>|</li>
					<li><a href="sitemap.php" title="Sitemap">Sitemap</a></li><li>|</li>
					<li><a href="contact-us.php" title="Contact us">Contact us</a></li>
				  
				</ul>
			</div>
			
		</div><!-- end of #footer-inner -->
	</div><!-- end of #footer-wrapper-->