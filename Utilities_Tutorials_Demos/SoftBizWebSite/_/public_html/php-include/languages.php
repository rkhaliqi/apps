            <div id="lang-wrapper" class="FR">
                <ul class="FR">
                    <li>Select language:</li>
                    <li><a href="#" class="lang-english lang-english-active" title="English (English)">English (English)</a></li>
                    <li><a href="#" class="lang-spanish" title="Español (Spanish)">Español (Spanish)</a></li>
                    <li><a href="#" class="lang-spanish" title="Français (French)">Français (French)</a></li>
                </ul>
            </div>	<!-- end of #lang-wrapper -->