<?php
include_once 'common.php';
include_once 'class.captcha.php';

$captcha = new Captcha();

$captcha->chars_number = 8;
$captcha->font_size = 15;
$captcha->tt_font = './HelveticaNeueLTPro-LtEx.otf';

$captcha->show_image(132, 30);
?>
