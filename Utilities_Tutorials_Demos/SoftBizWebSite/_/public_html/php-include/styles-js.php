<link rel="stylesheet" type="text/css" href="css-include/reset.css" />
<link rel="stylesheet" type="text/css" href="css-include/common.css" />
<link rel="stylesheet" type="text/css" href="css-include/ie.css" />
<link rel="stylesheet" type="text/css" href="css-include/mainstyle.css" />

<link rel="shortcut icon" href="images/favicon.ico" />

<script src="js-include/jquery.min.js" type="text/javascript"></script>


<link rel="stylesheet" href="css-include/nivo-slider.css" type="text/css" media="screen" />
<script src="js-include/jquery.nivo.slider.js" type="text/javascript"></script>  
<script type="text/javascript">
$(window).load(function() {
	$('#slider').nivoSlider({
		effect:'sliceDown', //Specify sets like: 'fold,fade,sliceDown, sliceUp,sliceDownLeft,sliceUpLeft,sliceUpDown,sliceUpDownLeft,'
		slices:30,
		animSpeed:1000, //Slide transition speed
		pauseTime:3800,
		startSlide:0, //Set starting Slide (0 index)
		directionNav:true, //Next & Prev
		directionNavHide:true, //Only show on hover
		controlNav:false, //1,2,3...
		controlNavThumbs:false, //Use thumbnails for Control Nav
      	controlNavThumbsFromRel:false, //Use image rel for thumbs
		controlNavThumbsSearch: '.jpg', //Replace this with...
		controlNavThumbsReplace: '_thumb.jpg', //...this in thumb Image src
		keyboardNav:true, //Use left & right arrows
		pauseOnHover:true, //Stop animation while hovering
		manualAdvance:false, //Force manual transitions
		captionOpacity:0.8, //Universal caption opacity
		beforeChange: function(){},
		afterChange: function(){},
		slideshowEnd: function(){} //Triggers after all slides have been shown
	});

});
</script>

<script type="text/javascript" language="javascript" src="js-include/shadowbox/yui-utilities.js"> </script>
<script type="text/javascript" language="javascript" src="js-include/shadowbox/shadowbox-yui.js"> </script>
<script type="text/javascript" language="javascript" src="js-include/shadowbox/shadowbox.js"> </script>
<script type="text/javascript" language="javascript"> YAHOO.util.Event.onDOMReady(Shadowbox.init);</script> 

<link rel="stylesheet" type="text/css" href="css-include/treeview.css" />
<script src="js-include/treeview/jquery.cookie.js" type="text/javascript"></script> 
<script src="js-include/treeview/jquery.treeview.js" type="text/javascript"></script> 

<script type="text/javascript">
	$(function() {
		$("#tree").treeview({
			collapsed: true,
			animated: "medium",
			control:"#sidetreecontrol",
			prerendered: true,
			persist: "location"
		});
	})	
</script>


<script type="text/javascript">
	
	$(function(){
	
		$('a[href*=#]').click(function() {
	
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
		
			&& location.hostname == this.hostname) {
		
				var $target = $(this.hash);
			
				$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
			
				if ($target.length) {
			
				var targetOffset = $target.offset().top;
			
				$('html,body').animate({scrollTop: targetOffset}, 1000);
			
				return false;
			}
		}
		
		});
		
	});
	
</script>

<script type="text/javascript">
<!--
function new_captcha()
{
var c_currentTime = new Date();
var c_miliseconds = c_currentTime.getTime();

document.getElementById('captcha').src = 'php-include/captcha/image.php?x='+ c_miliseconds;
}
-->
</script>

<!-- Mootools -->
<script src="js-include/mootools.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="css-include/sexyalertbox.css" />
<script src="js-include/sexyalertbox/sexyalertbox.v1.js" type="text/javascript"></script>
<script type="text/javascript">
	window.addEvent('domready', function() {
		Sexy = new SexyAlertBox();
	});
</script>

