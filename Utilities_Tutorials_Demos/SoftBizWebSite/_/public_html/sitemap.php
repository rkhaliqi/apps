<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - Sitemap</title>

<?php include_once("php-include/styles-js.php");?>


</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="who-are-we">
            	<h1 align="right">Navigate our website map!</h1>
         </div>
            <div id="content-wrapper">
            	
                <div id="leftpane">
                	
                    <div id="sidetree" style="border-right:1px solid #CCC;">
                        <p>Our website has many features. You can use this  page to have an overview of our company. This page also serves as your guide in  browsing through our entire website.</p>
                        
                      <div class="treeheader hide">&nbsp;</div>
                        
                        <div id="sidetreecontrol"> <a href="?#">Collapse All</a> | <a href="?#">Expand All</a> </div>
                        
                        <ul class="treeview" id="tree">
                        
                        <li><strong><a href="." title="Home">Home</a></strong></li>
                        <li><strong><a href="our-works.php" title="Our Works">Our Works</a></strong></li>
                        
                        <li class="expandable">
                        	<div class="hitarea expandable-hitarea"></div><a href="services.php" title="Services"><strong>Services</strong></a>
                        	
                            <ul style="display:none">
                                <li class="expandable">
                                    <div class="hitarea expandable-hitarea"></div><a href="services.php#webdesign" title="Web Design &amp; Development"><strong>Web Design &amp; Development</strong></a>
                                    <ul style="display: none;">
                                        <li><a href="services.php#webdesign-1" title="Web Content and Writing Services">Web Content and Writing Services</a></li>
                                        <li><a href="services.php#webdesign-2" title="eBay Store/Amazon Professional Services">eBay Store/Amazon Professional Services</a></li>
                                        <li><a href="services.php#webdesign-3" title="Search Engine Optimization">Search Engine Optimization</a></li>
                                        <li><a href="services.php#webdesign-4" title="Pay Per Click">Pay Per Click</a></li>
                                        <li><a href="services.php#webdesign-5" title="Internet Marketing and Advetisement">Internet Marketing and Advetisement</a></li>
                                        <li class="last"><a href="services.php#webdesign-6" title="Custom Website Design and Programming">Custom Website Design and Programming</a></li>
                                        
                                    </ul>
                                
                                </li>
                                <li class="last"><strong><a href="services.php#software" title="Software Development">Software Development</a></strong></li>
                           	</ul>
                            
                        </li>
                        
                        <li><strong><a href="free-quote.php" title="Free quote">Free quote</a></strong></li>
                        
                        <li class="expandable">
                            <div class="hitarea expandable-hitarea"></div><a href="about-us.php" title="About us"><strong>About us</strong></a>
                            <ul style="display: none;">
                               
                                <li><a href="about-us.php" title="About company">About company</a></li>
                                <li><a href="online-support.php" title="Online Support">Online Support</a></li>
                                <li><a href="technology.php" title="Technology">Technology</a></li>
                                <li><a href="why-people-choose-us.php" title="Why people choose us?">Why people choose us?</a></li>
                                <li><a href="mission-vision.php" title="Mission Vision">Mission Vision</a></li>
                                <li class="last"><a href="learnmore.php" title="Learn more">Learn more</a></li>
                            </ul>
                        </li>
                        
                        <li><strong><a href="blog.php" title="Blog">Blog</a></strong></li>                        
                        <li><strong><a href="contact-us.php" title="Contact us">Contact us</a></strong></li>
                        
                        <li class="expandable">
                        	<div class="hitarea expandable-hitarea"></div><a href="faq.php" title="Frequently Asked Questions (FAQs)"><strong>Frequently Asked Questions (FAQs)</strong></a>
                        	<ul style="display: none;">
                                <li><a href="faq.php#question-group-1" title="Search Engine Optimization (SEO)">Search Engine Optimization (SEO)</a></li>
                                <li><a href="faq.php#question-group-2" title="Web Design &amp; Development">Web Design &amp; Development</a></li>
                                <li><a href="faq.php#question-group-3" title="Software Development">Software Development</a></li>
                                <li><a href="faq.php#question-group-4" title="Online Support">Online Support</a></li>
                                <li class="last"><a href="faq.php#question-group-5" title="Technical Terms">Technical Terms</a></li>
                                
                            </ul>
                        </li>
                        
                        <li class="last"><strong><a href="testimonial.php" title="Testimonial">Testimonial</a></strong></li>
                        
                        </ul>
                    </div><!-- end of #sidetree -->
                    
				</div><!-- end of #leftpane-->
                
                <div id="rightpane">
                	
                    <?php include_once("php-include/contact-quote.php");?>
                    
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  

	<?php include_once("php-include/footer.php"); ?>

</body>
</html>
