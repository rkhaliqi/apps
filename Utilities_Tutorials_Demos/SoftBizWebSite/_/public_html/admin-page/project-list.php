<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
	  <?php include_once("php-include/header.php"); ?>
	  <div id="content-wrapper">
	    
<table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="224">
                    	
                        <?php include_once("php-include/leftpanel.php"); ?>
                        
                    </td><!-- end of td leftpanel -->
                    
                    <td valign="top">
                    
                   	  <div id="rightpanel">
                        
                        	<div id="rightpanel-top">
                            	<!-- sample menubuttons -->
        						<ul class="menubuttons">
                                    <li><a href="#" title="Delete project/s">Delete project/s</a></li>
                                    <li><a href="project-addentry.php" title="Add new project">Add new project</a></li>
                                     <li><a href="client-list.php" title="Cancel">Cancel</a></li>
                              	</ul>
                            
                            </div><!-- end of #rightpanel-top -->
                            
                        	<div id="rightpanel-content">
                            	
                                <h1 class="leftpanel-selected">&raquo; You are viewing <span class="red">Client projects</span></h1>
                                
                                
                                 <!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                                
                                <table cellpadding="0" cellspacing="5">
                                	<tr>
                                    	<td class="first-column-width-settings"><strong>Client Fullname:</strong></td>
                                        <td><strong class="green">Mr. Adil Khan</strong></td>
                                    </tr>
                                    <tr>
                                    	<td><strong>Contact email:</strong></td>
                                        <td>info@mywebsite.com</td>
                                    </tr>
                                    <tr>
                                    	<td><strong>Location:</strong></td>
                                        <td>Arizona, United States</td>
                                    </tr>
                                    <tr>
                                    	<td colspan="2"><strong>List of Projects:</strong></td>
                                    </tr>
                                </table>
                                
                  				<table cellpadding="0" cellspacing="1" class="field-listings">
                                    <tr>
                                    	<th width="30">
                                        	<input type="checkbox" class="checkbox-settings" />
                                       	</th>
                                        <th  width="50">ID</th>
                                        <th>Project name</th>
                                        <th>Website URL</th>
                                        <th>Dev't budget</th>
                                        <th>Date started - Date end</th>
                                        <th width="70">Status</th>
                                        <th width="90">Actions</th>
                                       
                                    </tr>
                                    <?php
										$start=1;
										$end=3;
										
										while($start<=$end){
									?>
                                    <!-- IE6 Solution -->
                                    <tr class="tr-highlight" onmouseout="this.className='tr-highlight'" onmouseover="this.className='tr-highlight-over'">
                                    	<td><input type="checkbox" class="checkbox-settings" /></td>
                                        <td><?=$start?></td>
                                        <td>Project name_<?=$start?></td>
                                        <td><a href="#" title="View website">www.mywebsite.com</a></td>
                                        <td>$3,000 - $5,000</td>
                                    	<td>10/30/2010 - 11/02/2010</td>
                                        <td>Complete</td>
                                        
                                        <td align="center">
                                        	<ul class="action-list">
                                            	<li><a href="#" title="Edit">Edit</a></li>
                                            	<li><a href="#" title="Delete" onclick="return confirm('Are you sure you want to delete this project?')">Delete</a></li>
                                            </ul>
                                        </td>
                                        
                                    </tr>	
                                    <?php
											$start++;
										}
									?>
                                   
                                   
                                </table>
                                
                         	</div><!-- end of #rightpanel-content -->
                            
                      	</div><!-- end of #rightpanel-->
                    	
                        <?php include_once("php-include/footer.php");?>
                        
                    </td><!-- end of td rightpanel -->
                    
                </tr>
            </table><!--end of table content-wrapper -->
        </div><!-- end of #content-wrapper -->
    </div><!-- end of #mainwrapper -->
</body>
</html>