						<div id="leftpanel">
                            <h1 class="leftpanel-heading">Content Manager</h1>
                            
                            <ul id="nav">
                            	<li>
                                	<a href="#" title="Manage Blog" class="nav-normal">Manage Blog</a>
                                    <ul>
                          				<li>
                                        	<a href="blog-article-list.php" title="Manage Articles">Manage blog articles</a>
                                        	<ul>
                                   				<li><a href="#" title="Inactive articles">Inactive articles</a></li>
                                                <li><a href="#" title="Active articles">Active articles</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="blog-category-list.php" title="Manage Categories">Manage blog categories</a></li>
                                        <li><a href="blog-section-list.php" title="Manage Sections">Manage blog sections</a></li>
                                        
                                    </ul>
                                
                                </li>
                                <li>
                                	<a href="testimonial-list.php" title="Manage testimonials">Manage testimonials</a>
                                	<ul>
                                    	<li><a href="#" title="Inactive testimonials">Inactive testimonials</a></li>
                                        <li><a href="#" title="Active testimonials">Active testimonials</a></li>
                                    </ul>
                                </li>
                                <li><a href="portfolio-list.php" title="Manage portfolios">Manage portfolios</a></li>
                                <li><a href="client-list.php" title="Manage clients/projects">Manage clients/projects</a></li>
                                <li>
                                	<a href="admin-user-list.php" title="Manage admin users">Manage admin users</a>
                                	<ul>
                                  		<li><a href="#" title="Active admin users">Active admin users</a></li>
                                        <li><a href="#" title="Inactive admin users">Inactive admin users</a></li>
                                        
                                    </ul>
                                </li>
                                <li>
                                	<a href="#" class="nav-normal">My account options</a>
                                    <ul>
                                    	<li><a href="account-log-history.php" title="My account log history">Account log  history</a></li>
                                		<li><a href="account-settings.php" title="My account settings">Account settings</a></li>
                                    </ul>
                                </li>
                                <li><a href="." title="Logout account" onclick="return confirm('Are you sure you want to logout account?')">Logout account</a></li>
                            </ul><!-- end of #nav -->
                            
						</div><!-- end of #leftpanel-->