<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
	  <?php include_once("php-include/header.php"); ?>
	  <div id="content-wrapper">
        	
<table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="224">
                    	
                        <?php include_once("php-include/leftpanel.php"); ?>
                        
                    </td><!-- end of td leftpanel -->
                    
                    <td valign="top">
                    
                   	  <div id="rightpanel">
                        
                        	<div id="rightpanel-top">
                            	<!-- sample menubuttons -->
        						<ul class="menubuttons">
                                    <li><a href="#" title="Approve request">Approve request</a></li>
                                    <li><a href="#" title="Open request">Open request</a></li>
                                    <li><a href="#" title="Close request">Close request</a></li>
                                    <li><a href="#" title="Reject request">Reject request</a></li>
                                    <li><a href="#" title="On-hold request">On-hold request</a></li>
                                    <li><a href="design-request-list.php" title="Back">Back</a></li>
                              	</ul>
                            
                        </div><!-- end of #rightpanel-top -->
                            
                        	<div id="rightpanel-content">
                            	
                                <h1 class="leftpanel-selected">&raquo;You are viewing details for Request ID number: <span class="red">DES-0123456789</span> </h1>
                                
                                
                                 <!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                                <table cellpadding="0" cellspacing="1" class="field-listings">
                                    
                                    <tr>
                                    	<td width="150" align="right"><strong>Fullname:</strong></td>
                                    	<td>Adil Khan</td>
                                    </tr>
                                    <tr>
                                    	<td align="right"><strong>Email address:</strong></td>
                                    	<td>info@mywebsite.com</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Phone number:</strong></td>
                                        <td>1-520-225-0169</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Company name:</strong></td>
                                        <td>HOMEBASE</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Website URL:</strong></td>
                                        <td><a href="#">http://www.mywebsite.com</a></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Zipcode</strong></td>
                                        <td>60450</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>City:</strong></td>
                                        <td>Arizona</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Country:</strong></td>
                                        <td>United States</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Project type:</strong></td>
                                        <td>Template Design</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Project size:</strong></td>
                                        <td>Small Business Site</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Approximate pages:</strong></td>
                                        <td>3-5</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Dev't budget::</strong></td>
                                        <td>$3,000 - $5,000</td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Need maintainance?</strong></td>
                                        <td>Yes</td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><strong>Message archive:</strong></td>
                                        <td>
                                        	<p> <strong class="green">Posted by Adil Khan, on 11/03/2010 11:36 AM</strong><br />
                                        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in velit neque. Nulla ullamcorper fermentum tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas ut nunc purus. </p>
                                            
                                     	</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <th align="left">Quick  reply</th>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><textarea name="textarea">&nbsp;</textarea></td>
                                    </tr>
                                    <tr>
                                    	<td>&nbsp;</td>
                                    	<td><input type="submit" value="Send" style="width:90px" /></td>
                                    </tr>	
                                   
                                </table>
                        	</div><!-- end of #rightpanel-content -->
                            
                      	</div><!-- end of #rightpanel-->
                    	
                        <?php include_once("php-include/footer.php");?>
                        
                    </td><!-- end of td rightpanel -->
                    
                </tr>
            </table><!--end of table content-wrapper -->
        </div><!-- end of #content-wrapper -->
    </div><!-- end of #mainwrapper -->
</body>
</html>