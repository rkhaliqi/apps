<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
    	
        <?php include_once("php-include/header.php"); ?>
        
        <div id="content-wrapper">
        	
            <table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="224">
                    
                    	<?php include_once("php-include/leftpanel.php"); ?>
                    
                    </td><!-- end of td leftpanel -->
                    
                    <td valign="top">
                    
                   	  <div id="rightpanel">
                        
                        	<div id="rightpanel-top">
                            	<!-- sample menubuttons -->
        						<ul class="menubuttons">
                                    <li><a href="#" title="Update">Update</a></li>
                                    <li><a href="#" title="Apply">Apply</a></li>
                                    <li><a href="#" title="View Metadata Information">View Metadata Information</a></li>
                                    <li><a href="blog-article-list.php" title="Cancel">Cancel</a></li>
                              	</ul>
                            
                            </div><!-- end of #rightpanel-top -->
                            
                        	<div id="rightpanel-content">
                            	
                                <h1 class="leftpanel-selected">&raquo; You are editing <span class="red">Blog article ID #1</span></h1>
                                
                                <!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                  				<table cellpadding="0" cellspacing="5" class="field-details">
                                	
                                    <tr>
                                    	<td valign="top" class="first-column-width-settings"><strong>Blog article name:</strong></td>
                                        <td valign="top" ><input type="text" maxlength="70" /></td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Section:</strong></td>
                                        <td valign="top" >
                                        	<select class="sorting-width-settings">
                                            	<option>Uncategorized</option>
                                                <option>Lorem_1</option>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Categories:</strong></td>
                                        <td valign="top" >
                                        	<select class="sorting-width-settings">
                                            	<option>Uncategorized</option>
                                                <option>Quisque_1</option>
                                       	    	<option>Quisque_2</option>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Visibility:</strong></td>
                                        <td valign="top" >
                                        	<select class="sorting-width-settings">
                                            	<option>Show</option>
                                                <option>Hide</option>
                                            </select>
                                        </td>
                                    </tr>
                                   
                                    <tr>
                                    	<td valign="top" ><strong>Content:</strong></td>
                                        <td valign="top" ><textarea id="tiny_mce">&nbsp;</textarea></td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Date created:</strong></td>
                                        <td valign="top" >Tuesday, 23 November 2010 07:45 AM</td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Last modified:</strong></td>
                                        <td valign="top" >Tuesday, 23 November 2010 07:45 AM</td>
                                    </tr>
                                    
                                </table>
                                
                         	</div><!-- end of #rightpanel-content -->
                            
                      	</div><!-- end of #rightpanel-->
                    	
                        <?php include_once("php-include/footer.php");?>
                       
                    </td><!-- end of td rightpanel -->
                    
                </tr>
            </table><!--end of table content-wrapper -->
        </div><!-- end of #content-wrapper -->
    </div><!-- end of #mainwrapper -->
</body>
</html>