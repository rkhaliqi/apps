<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
	  <?php include_once("php-include/header.php"); ?>
	  <div id="content-wrapper">
        	
<table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="224">
                    	
                        <?php include_once("php-include/leftpanel.php"); ?>
                        
                    </td><!-- end of td leftpanel -->
                    
                    <td valign="top">
                    
                   	  <div id="rightpanel">
                        
                        	<div id="rightpanel-top">
                            	<!-- sample menubuttons -->
        						<ul class="menubuttons">
                                    <li><a href="#" title="Delete section/s">Delete section/s</a></li>
                                    <li><a href="#" title="Activate section">Activate section</a></li>
                                    <li><a href="#" title="Deactivate section">Deactivate section</a></li>
                                    <li><a href="blog-section-addentry.php" title="Add new section">Add new section</a></li>
                              	</ul>
                            
                            </div><!-- end of #rightpanel-top -->
                            
                        	<div id="rightpanel-content">
                            	
                                <h1 class="leftpanel-selected">&raquo; You are viewing <span class="red">Blog sections</span></h1>
                                
                               
                                 <!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                  				<table cellpadding="0" cellspacing="1" class="field-listings">
                                    <tr>
                                    	<th width="30" align="center" >
                                        	<input type="checkbox" class="checkbox-settings" />
                                       	</th>
                                        <th  width="50">ID</th>
                                        <th>Blog section name</th>
                                        <th  width="90">Status</th>
                                        <th align="center" width="90">#Category</th>
                                        <th align="center" width="90">Active Item(s)</th>
                                        <th  width="90">Actions</th>
                                    </tr>
                                   
                                    <!-- IE6 Solution -->
                                    <tr class="tr-highlight" onmouseout="this.className='tr-highlight'" onmouseover="this.className='tr-highlight-over'">
                                    	<td align="center" ><input type="checkbox" class="checkbox-settings" /></td>
                                        <td>1</td>
                                        <td>Lorem_1</td>
                                        <td>Active</td>
                                        <td>02</td>
                                        <td>08</td>
                                        <td align="center">
                                        	<ul class="action-list">
                                            	<li><a href="blog-section-entry.php" title="Edit">Edit</a></li>
                                            	<li><a href="#" title="Delete" onclick="return confirm('Are you sure you want to delete Section ID #1?')">Delete</a></li>
                                            </ul>
                                        </td>
                                    </tr>	
                                    
                                    
                                </table>
                                
                         	</div><!-- end of #rightpanel-content -->
                            
                      	</div><!-- end of #rightpanel-->
                    	
                        <?php include_once("php-include/footer.php");?>
                        
                    </td><!-- end of td rightpanel -->
                    
                </tr>
            </table><!--end of table content-wrapper -->
        </div><!-- end of #content-wrapper -->
    </div><!-- end of #mainwrapper -->
</body>
</html>