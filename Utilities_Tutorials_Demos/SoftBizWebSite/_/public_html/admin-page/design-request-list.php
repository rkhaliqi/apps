<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
	  <?php include_once("php-include/header.php"); ?>
	  <div id="content-wrapper">
        	
<table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="224">
                    	
                        <?php include_once("php-include/leftpanel.php"); ?>
                        
                    </td><!-- end of td leftpanel -->
                    
                    <td valign="top">
                    
                   	  <div id="rightpanel">
                        
                        	<div id="rightpanel-top">
                            	<!-- sample menubuttons -->
        						<ul class="menubuttons">
                                    <li><a href="#" title="Approve request">Approve request</a></li>
                                    <li><a href="#" title="Open request">Open request</a></li>
                                    <li><a href="#" title="Close request">Close request</a></li>
                                    <li><a href="#" title="Reject request">Reject request</a></li>
                                    <li><a href="#" title="On-hold request">On-hold request</a></li>
                              	</ul>
                            
                            </div><!-- end of #rightpanel-top -->
                            
                        	<div id="rightpanel-content">
                            	
                                <h1 class="leftpanel-selected">&raquo; You are viewing <span class="red">Web Design Free Quote Request</span></h1>
                                
                                <div align="right">
                               		
                                    <table cellpadding="0" cellspacing="5">
                                        <tr>
                                        	
                                            <td>
                                            	<small>Sort by email:</small><br />
                                                <select class="sorting-width-settings">
                                                    <option>(Please select one)</option>
                                                    <option>info@website1.com</option>
                                                    <option>info@website2.com</option>
                                                </select>
                                            </td>
                                            <td>
                                            	<small>Sort by status:</small><br />
                                                <select class="sorting-width-settings">
                                                   <option>(Please select one)</option>
                                                    <option>Approved request</option>
                                                    <option>Open request</option>
                                                    <option>Close request</option>
                                                    <option>Rejected request</option>
                                                    <option>On-hold request</option>
                                                </select>
                                            </td>
                                          
                                        </tr>
                                    </table>
                                </div>
                                
                                 <!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                  				<table cellpadding="0" cellspacing="1" class="field-listings">
                                    <tr>
                                    	<th width="30">
                                        	<input type="checkbox" class="checkbox-settings" />
                                       	</th>
                                        <th width="150">Design request ID</th>
                                    <th>From email</th>
                                        <th>Fullname</th>
                                        <th>Location</th>
                                    	<th width="120">Date requested</th>
                                      <th width="90">Status</th>
                                    </tr>
                                    <?php
										$start=1;
										$end=20;
										
										while($start<=$end){
									?>
                                    <!-- IE6 Solution -->
                                    <tr class="tr-highlight" onmouseout="this.className='tr-highlight'" onmouseover="this.className='tr-highlight-over'">
                                    	<td><input type="checkbox" class="checkbox-settings" /></td>
                                        <td><a href="design-request-details.php" title="View request">DES-0123456789</a></td>
                                <td>info@mywebsite<?=$start?>.com</td>
                                        <td>Adil Khan<?=$start?></td>
                                        <td>Arizona, United States</td>
                                    	<td>11/03/2010 09:42 AM</td>
                                        <td>Open</td>
                                       
                                </tr>	
                                    <?php
											$start++;
										}
									?>
                                    <tr>
                                    	<td colspan="9">
                                        	<ul class="pagination">
                                                <li>Page 1 of 25:</li>
                                                <li><a href="#" title="Previous">&laquo;</a></li>
                                                <li><a href="#" class="page-active">1</a></li>
                                                <li><a href="#" title="2">2</a></li>
                                                <li><a href="#" title="3">3</a></li>
                                                <li><a href="#" title="Next">&raquo;</a></li>
                                                <li><a href="#" title="10">10</a></li>
                                                <li><a href="#" title="20">20</a></li>
                                               
                                                <li>...</li>
                                                <li><a href="#" title="Go to last page">last &raquo;</a></li>
                                            </ul> 
                                        </td>
                                    </tr>
                                   
                                </table>
                                
                         	</div><!-- end of #rightpanel-content -->
                            
                      	</div><!-- end of #rightpanel-->
                    	
                        <?php include_once("php-include/footer.php");?>
                        
                    </td><!-- end of td rightpanel -->
                    
                </tr>
            </table><!--end of table content-wrapper -->
        </div><!-- end of #content-wrapper -->
    </div><!-- end of #mainwrapper -->
</body>
</html>