<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
	  <?php include_once("php-include/header.php"); ?>
	  <div id="content-wrapper">
        	
<table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="224">
                    	
                        <?php include_once("php-include/leftpanel.php"); ?>
                        
                    </td><!-- end of td leftpanel -->
                    
                    <td valign="top">
                    
                   	  <div id="rightpanel">
                        
                        	<div id="rightpanel-top">
                            	<!-- sample menubuttons -->
        						<ul class="menubuttons">
                                    <li><a href="#" title="Save">Save</a></li>
                                    <li><a href="#" title="Apply">Apply</a></li>
                                     <li><a href="client-list.php" title="Cancel">Cancel</a></li>
                              	</ul>
                            
                            </div><!-- end of #rightpanel-top -->
                            
                        	<div id="rightpanel-content">
                            	
                                <h1 class="leftpanel-selected">&raquo; You are addinew new <span class="red">Client project</span></h1>
                                
                                
                                 <!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                                
                              
                                <hr />
                  				<!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                  				
                  				<table cellpadding="0" cellspacing="5" class="field-details">
                                
                                	<tr>
                                    	<td valign="top" class="first-column-width-settings"><strong>Website owner:</strong></td>
                                        <td valign="top"><strong class="green">Mr. Adil Khan</strong></td>
                                  </tr>
                                    <tr>
                                    	<td valign="top"><strong>Contact email:</strong></td>
                                        <td valign="top">info@mywebsite.com</td>
                                    </tr>
                                    <tr>
                                    	<td valign="top"><strong>Location:</strong></td>
                                        <td valign="top">Arizona, United States</td>
                                    </tr>
                                   
                                    <tr>
                                    	<td valign="top"><strong>Project name:</strong></td>
                                        <td valign="top" ><input type="text" maxlength="70" /></td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" class="first-column-width-settings"><strong>Website URL:</strong></td>
                                        <td valign="top" ><input type="text" /></td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Development Budget:</strong></td>
                                        <td valign="top" >
                                        	<select class="sorting-width-settings" name="development_budget">
                                                <option value="" selected="selected">(Please select)</option>
                                                <option value="$3,000 to $5,000">$3,000 to $5,000</option>
                                                <option value="$5,000 to $7,000">$5,000 to $7,000</option>
                                                <option value="$7,000 to $10,000">$7,000 to $10,000</option>
                                                <option value="$10,000 to $15,000">$10,000 to $15,000</option>
                                                <option value="$15,000 to $30,000">$15,000 to $30,000</option>
                                                <option value="$30,000 to $50,000">$30,000 to $50,000</option>
                                                <option value="50+">$50,000+</option>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Project status:</strong></td>
                                        <td valign="top" >
                                        	<select class="sorting-width-settings" name="development_budget">
                                                <option>Complete</option>
                                                <option>On hold</option>
                                                <option>Cancelled</option>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Date start - Date end:</strong></td>
                                        <td valign="top" >
                                        	<input type="text" style="width:100px;" /> - <input type="text" style="width:100px;" />
                                        
                                        </td>
                                    </tr>
                                    
                                   
                                    
                              </table>
                                
                         	</div><!-- end of #rightpanel-content -->
                            
                      	</div><!-- end of #rightpanel-->
                    	
                        <?php include_once("php-include/footer.php");?>
                        
                    </td><!-- end of td rightpanel -->
                    
                </tr>
            </table><!--end of table content-wrapper -->
        </div><!-- end of #content-wrapper -->
    </div><!-- end of #mainwrapper -->
</body>
</html>