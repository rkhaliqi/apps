<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
	  <?php include_once("php-include/header.php"); ?>
	  <div id="content-wrapper">
        	
<table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="224">
                    	
                        <?php include_once("php-include/leftpanel.php"); ?>
                        
                    </td><!-- end of td leftpanel -->
                    
                    <td valign="top">
                    
                   	  <div id="rightpanel">
                        
                        	<div id="rightpanel-top">
                            	<!-- sample menubuttons -->
        						<ul class="menubuttons">
                                    <li><a href="#" title="Delete client/s">Delete client/s</a></li>
                                    <li><a href="client-addentry.php" title="Add new client">Add new client</a></li>
                              	</ul>
                            
                            </div><!-- end of #rightpanel-top -->
                            
                        	<div id="rightpanel-content">
                            	
                                <h1 class="leftpanel-selected">&raquo; You are viewing <span class="red">Client listings</span></h1>
                                
                                
                                 <!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                                
                                <div align="right">
                               		
                                    <table cellpadding="0" cellspacing="5">
                                        <tr>
                                        	
                                            <td><strong>Quick search:</strong></td>
                                            <td>
                                                <select class="sorting-width-settings">
                                                    <option>(Please select)</option>
                                                    <option>By client fullname</option>
                                                    <option>By client contact email</option>
                                                </select>
                                            </td>
                                            <td><input type="text" class="sorting-width-settings" value="Enter your text here..." /> </td>
                                          	<td><input type="submit" value="Search" /></td>
                                        </tr>
                                    </table>
                                </div>
                                
                  				<table cellpadding="0" cellspacing="1" class="field-listings">
                                    <tr>
                                    	<th width="30">
                                        	<input type="checkbox" class="checkbox-settings" />
                                       	</th>
                                        <th  width="50">ID</th>
                                        <th>Client fullname</th>
                                        <th>Contact Email</th>
                                        <th>Location</th>
                                        <th width="50">Gender</th>
                                        <th width="120">#Projects</th>
                                        <th  width="90">Actions</th>
                                    </tr>
                                    <?php
										$start=1;
										$end=10;
										
										while($start<=$end){
									?>
                                    <!-- IE6 Solution -->
                                    <tr class="tr-highlight" onmouseout="this.className='tr-highlight'" onmouseover="this.className='tr-highlight-over'">
                                    	<td><input type="checkbox" class="checkbox-settings" /></td>
                                        <td><?=$start?></td>
                                        <td>Mr. Adil Khan_<?=$start?></td>
                                        <td><a href="#" title="Send a message">info@mywebsite.com</a></td>
                                        <td>Arizona, United States</td>
                                        <td>Male</td>
                                    	<td align="center">
                                        	<a href="project-list.php" title="View projects">03</a>
                                            <a href="project-addentry.php" title="Add project" style="margin-left:10px">Add project</a>
                                        </td>
                                        <td align="center" >
                                        	<ul class="action-list">
                                            	<li><a href="client-entry.php" title="Edit">Edit</a></li>
                                            	<li><a href="#" title="Delete" onclick="return confirm('Are you sure you want to delete Client ID #<?=$start?>?')">Delete</a></li>
                                            </ul>
                                        </td>
                                    </tr>	
                                    <?php
											$start++;
										}
									?>
                                    
                                    <tr>
                                    	<td colspan="8">
                                        	<ul class="pagination">
                                                <li>Page 1 of 25:</li>
                                                <li><a href="#" title="Previous">&laquo;</a></li>
                                                <li><a href="#" class="page-active">1</a></li>
                                                <li><a href="#" title="2">2</a></li>
                                                <li><a href="#" title="3">3</a></li>
                                                <li><a href="#" title="Next">&raquo;</a></li>
                                                <li><a href="#" title="10">10</a></li>
                                                <li><a href="#" title="20">20</a></li>
                                               
                                                <li>...</li>
                                                <li><a href="#" title="Go to last page">last &raquo;</a></li>
                                            </ul> 
                                        </td>
                                    </tr>
                                   
                                   
                                </table>
                                
                         	</div><!-- end of #rightpanel-content -->
                            
                      	</div><!-- end of #rightpanel-->
                    	
                        <?php include_once("php-include/footer.php");?>
                        
                    </td><!-- end of td rightpanel -->
                    
                </tr>
            </table><!--end of table content-wrapper -->
        </div><!-- end of #content-wrapper -->
    </div><!-- end of #mainwrapper -->
</body>
</html>