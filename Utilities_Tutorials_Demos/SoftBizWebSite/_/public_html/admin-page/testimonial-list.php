<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
	  <?php include_once("php-include/header.php"); ?>
	  <div id="content-wrapper">
        	
<table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="224">
                    	
                        <?php include_once("php-include/leftpanel.php"); ?>
                        
                    </td><!-- end of td leftpanel -->
                    
                    <td valign="top">
                    
                   	  <div id="rightpanel">
                        
                        	<div id="rightpanel-top">
                            	<!-- sample menubuttons -->
        						<ul class="menubuttons">
                                    <li><a href="#" title="Delete testimonial/s">Delete testimonial/s</a></li>
                                    <li><a href="#" title="Show testimonial">Show testimonial</a></li>
                                    <li><a href="#" title="Hide testimonial">Hide testimonial</a></li>
                                    <li><a href="testimonial-addentry.php" title="Add new testimonial">Add new testimonial</a></li>
                              	</ul>
                            
                            </div><!-- end of #rightpanel-top -->
                            
                        	<div id="rightpanel-content">
                            	
                                <h1 class="leftpanel-selected">&raquo; You are viewing <span class="red">Testimonials</span></h1>
                                
                                
                                 <!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                  				<table cellpadding="0" cellspacing="1" class="field-listings">
                                    <tr>
                                    	<th width="30">
                                        	<input type="checkbox" class="checkbox-settings" />
                                       	</th>
                                        <th  width="50">ID</th>
                                        <th>Client fullname</th>
                                        <th>Client message</th>
                                        <th>Client location</th>
                                        <th  width="70">Status</th>
                                        <th  width="120">Date created</th>
                                        <th  width="90">Actions</th>
                                    </tr>
                                    <?php
										$start=1;
										$end=10;
										
										while($start<=$end){
									?>
                                    <!-- IE6 Solution -->
                                    <tr class="tr-highlight" onmouseout="this.className='tr-highlight'" onmouseover="this.className='tr-highlight-over'">
                                    	<td><input type="checkbox" class="checkbox-settings" /></td>
                                        <td><?=$start?></td>
                                        <td><a href="#" title="View profile">Mr. Adil Khan_<?=$start?></a></td>
                                        <td>My testimonial message!</td>
                                        <td>Arizona, USA</td>
                                        <td>Active</td>
                                        <td>10/28/2010</td>
                                       
                                        <td align="center">
                                        	<ul class="action-list">
                                            	<li><a href="testimonial-entry.php" title="Edit">Edit</a></li>
                                            	<li><a href="#" title="Delete" onclick="return confirm('Are you sure you want to delete Article ID #<?=$start?>?')">Delete</a></li>
                                            </ul>
                                        </td>
                                    </tr>	
                                    <?php
											$start++;
										}
									?>
                                    <tr>
                                    	<td colspan="9">
                                        	<ul class="pagination">
                                                <li>Page 1 of 25:</li>
                                                <li><a href="#" title="Previous">&laquo;</a></li>
                                                <li><a href="#" class="page-active">1</a></li>
                                                <li><a href="#" title="2">2</a></li>
                                                <li><a href="#" title="3">3</a></li>
                                                <li><a href="#" title="Next">&raquo;</a></li>
                                                <li><a href="#" title="10">10</a></li>
                                                <li><a href="#" title="20">20</a></li>
                                               
                                                <li>...</li>
                                                <li><a href="#" title="Go to last page">last &raquo;</a></li>
                                            </ul> 
                                        </td>
                                    </tr>
                                   
                                </table>
                                
                         	</div><!-- end of #rightpanel-content -->
                            
                      	</div><!-- end of #rightpanel-->
                    	
                        <?php include_once("php-include/footer.php");?>
                        
                    </td><!-- end of td rightpanel -->
                    
                </tr>
            </table><!--end of table content-wrapper -->
        </div><!-- end of #content-wrapper -->
    </div><!-- end of #mainwrapper -->
</body>
</html>