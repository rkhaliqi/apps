<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
    	
        <?php include_once("php-include/header.php"); ?>
        
        <div id="content-wrapper">
        	
            <table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="224">
                    
                    	<?php include_once("php-include/leftpanel.php"); ?>
                    
                    </td><!-- end of td leftpanel -->
                    
                    <td valign="top">
                    
                   	  <div id="rightpanel">
                        
                        	<div id="rightpanel-top">
                            	<!-- sample menubuttons -->
        						<ul class="menubuttons">
                                    <li><a href="#" title="Save">Save</a></li>
                                    <li><a href="#" title="Apply">Apply</a></li>
                                    <li><a href="portfolio-list.php" title="Cancel">Cancel</a></li>
                              	</ul>
                            
                            </div><!-- end of #rightpanel-top -->
                            
                        	<div id="rightpanel-content">
                            	
                                <h1 class="leftpanel-selected">&raquo; You are adding new <span class="red">Portfolio</span></h1>
                                
                                <!-- 
                                	field-details  : cellspacing = 5
                                    field-listings : cellspacing = 1
                                    td settings    : valign = top
                                -->
                  				<table cellpadding="0" cellspacing="5" class="field-details">
                                	
                                    <tr>
                                    	<td valign="top" class="first-column-width-settings"><strong>Portfolio / Website name:</strong></td>
                                        <td valign="top" ><input type="text" maxlength="70" /></td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Short Description:</strong></td>
                                        <td valign="top" ><input type="text" /></td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Website URL:</strong></td>
                                        <td valign="top" ><input type="text" /></td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" ><strong>Website Owner:</strong></td>
                                        <td valign="top" >
                                        	<select>
                                            	<?php
													$start=1;
													$end=10;
													while($start <= $end){
												?>
                                            	<option>Mr. Adil Khan_<?=$start?></option>
                                                <?php
														$start++;
													}
												?>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" class="first-column-width-settings"><strong>Thumb view Image:</strong></td>
                                        <td valign="top" ><input type="file" /></td>
                                    </tr>
                                    
                                    <tr>
                                    	<td valign="top" class="first-column-width-settings"><strong>Detailed view Image:</strong></td>
                                        <td valign="top" ><input type="file" /></td>
                                    </tr>
                                    
                                   
                                    <tr>
                                    	<td valign="top" ><strong>Visibility:</strong></td>
                                        <td valign="top" >
                                        	<select class="sorting-width-settings">
                                            	<option>Show</option>
                                                <option>Hide</option>
                                            </select>
                                        </td>
                                    </tr>
                                    
                                </table>
                                
                         	</div><!-- end of #rightpanel-content -->
                            
                      	</div><!-- end of #rightpanel-->
                    	
                        <?php include_once("php-include/footer.php");?>
                       
                    </td><!-- end of td rightpanel -->
                    
                </tr>
            </table><!--end of table content-wrapper -->
        </div><!-- end of #content-wrapper -->
    </div><!-- end of #mainwrapper -->
</body>
</html>