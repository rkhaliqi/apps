<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution: Administrator Page</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body id="loginbody">
	<div id="loginwrapper">
   	  <div id="loginheader">Recover password</div><!-- end of #loginheader-->
        <div class="loginbody">
        	<form action="#">
            	Enter your email address to receive a new login key and a link to sign in and change your password.
                <table cellpadding="0" cellspacing="5" width="100%">
                    <tr>
                        <td width="80"><strong>Enter email:</strong></td>
                        <td><input type="text" /></td>
                    </tr>
                    <tr>
                    	<td>&nbsp;</td>
                        <td><input type="submit" style="width:120px;" value="Recover password" /></td>
                    </tr>
                    
                </table>
            </form>
            
           
        </div><!-- end of #loginbody-->
    </div><!-- end of #loginwrapper -->
   
</body>
</html>