<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - About Company</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus aboutus-active">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="about-us">
            	<h1>Who We are and What We Do</h1>
         </div>
            <div id="content-wrapper">
            	<div id="leftpane">
                	<h1 class="maintitle">Why  <span class="green">People Choose Us</span></h1>
                    <p>We  cannot give you a reason on why you should not choose us.</p>
<p>The way  we take care of our clients is exceptional. From the beginning until the end of  the project, we assure you that we can give you what you need. Some design  firms will just follow your instructions without further clarifications, but  Creative Web Solution offers you more than that.</p>
<p>Our  expertise in the field of information technology is combined with our passion  to serve our clients. Our team of experts do not work on each project alone;  they also communicate with each client to see to it that every detail is  correct.</p>
<p>Our team  of web designers and developers has mastered their craft through experience.  Their creativity and eye for details are remarkable and they assure that no  designs are the same.</p>
<p>Our  SEO’s and Internet marketers have obtained different strategies and techniques  in making web pages rank in search engines. Moreover, they are equipped with  different tools to increase site popularity and traffic. </p>
<p>However,  the process does not just stop there. Our SEO’s and internet marketers make  sure that your website stays on top of the results page of every search engine  because we understand your business’s need for popularity.</p>
<p>Our  software developers are experts in programming. From planning to maintenance,  our developers devote their time, efforts and expertise to come up with the  result you need. </p>
<p>Should  there be cases of ambiguity or incompleteness, our experts know very well how  to correct them and are able to give you your desired output.</p>
<p>Our team  of friendly customer service representatives is available 24/7 for your  inquiries, comments and further instructions. They are trained not only to  master every detail of our craft but to assist you in the most amicable way.<br />
  To sum  it up, we do not give you what you want but what your clients want as well. We  define creativity, we know a lot about skill and we work with passion.</p>
</div><!-- end of #leftpane-->
                <div id="rightpane">
                	
                    <div class="rightpane-header"> <h2>Explore more</h2></div><!-- end of .rightpane-header -->
                    
                    <ul class="rightnavigation">
                    	
                        <li><a href="about-us.php" title="About company">About company</a></li>
                        <li><a href="online-support.php" title="Online Support">Online Support</a></li>
                        <li><a href="technology.php" title="Technology">Technology</a></li>
                        <li><a href="why-people-choose-us.php" title="Why People Choose Us" class="rightnav-active">Why People Choose Us</a></li>
                        <li><a href="mission-vision.php" title="Mission Vision">Mission Vision</a></li>
                        <li><a href="learnmore.php" title="Learn more">Learn more</a></li>
                    </ul>
                    <br class="clear" /><!-- don't remove -->
                 
                  	<?php include_once("php-include/contact-quote.php");?>
                
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  

	<?php include_once("php-include/footer.php"); ?>

</body>
</html>
