<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - About Company</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus aboutus-active">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="about-us">
            	<h1>Who we are and what we do</h1>
         </div>
            <div id="content-wrapper">
            	<div id="leftpane">
                	<h1 class="maintitle">Mission</h1>
                    
                    <p>Creative Web Solution is a company dedicated to  providing quality services to its clients. With its broad scope of information  technology-based services, it sees to it that every client obtains the needed  output to be competitive and superior in the marketplace.</p>
                    
                  	<h1 class="maintitle">Vision</h1>
                    
                    <p>Creative Web Solution aims to be the leading  provider of design, marketing, optimization and internet advertising inside and  outside US.</p>
                    
                </div><!-- end of #leftpane-->
                <div id="rightpane">
                	
                    <div class="rightpane-header"> <h2>Explore more</h2></div><!-- end of .rightpane-header -->
                    
                    <ul class="rightnavigation">
                    	
                        <li><a href="about-us.php" title="About company">About company</a></li>
                        <li><a href="online-support.php" title="Online Support">Online Support</a></li>
                        <li><a href="technology.php" title="Technology">Technology</a></li>
                        <li><a href="why-people-choose-us.php" title="Why People Choose Us">Why People Choose Us</a></li>
                        <li><a href="mission-vision.php" title="Mission Vision" class="rightnav-active">Mission Vision</a></li>
                        <li><a href="learnmore.php" title="Learn more">Learn more</a></li>
                    </ul>
                    <br class="clear" /><!-- don't remove -->
                    
                    <?php include_once("php-include/contact-quote.php");?>
                   	
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  

	<?php include_once("php-include/footer.php"); ?>

</body>
</html>
