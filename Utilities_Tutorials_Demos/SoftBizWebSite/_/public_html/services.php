<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - Services</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services services-active">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="services">
            	<h1>What we can do for you</h1>
         </div>
            <div id="content-wrapper">
            	<div id="leftpane">
           		  	<h1 class="maintitle">We offer a variety of software and web related  services.</h1>
                    
                  	<ol type="A">
                    	<li>
                        	<h1 class="blue" id="webdesign">Web Design &amp; Development </h1>
                            
                            <p><strong id="webdesign-1">Web Content and Writing Services</strong> – we also render writing services for different niches on the Internet. We offer all sorts of content types – from articles, to blogs, to actual website content and so on. We make use of different writing guidelines to suit your project needs. We also assure that the contents are 100% grammar-correct to live up to the expectations of the clients from the different parts of the world.</p>
                            <p><strong id="webdesign-2">eBay Store/Amazon Professional Services </strong>–  We can design/develop professional product listings in the major electronic commerce websites such as eBay and Amazon and offer designing for About Me pages, eBay stores and Amazon product listings. We can also add your product or service to our well-established and trusted eBay account. This way, your company, along with its products or services can be easily noticed since thousands of consumers use these e-commerce websites every day to search and cater to their needs. </p>
                            <p><strong id="webdesign-3">Search Engine Optimization</strong> – we render excellent SEO services. We do on-page and off-page optimization to ensure that your website becomes visible in different search engines. We also make sure that the websites we optimize are on top of search engine results page. We, at the same time, can meet your expected return of investment (ROI) into 300% or more.</p>
                            <p><strong id="webdesign-4">Pay Per Click</strong> –we cleverly place advertisements on web pages to increase your website traffic. We also direct the right searchers and users that are on the lookout for your products or services. This is one strategy for your website to be noticed and clicked thus, paving the way for your products or services to be availed by local and international customers. </p>
                            <p><strong id="webdesign-5">Internet Marketing</strong> –  we promote your website through different internet marketing strategies such as SEO, PPC, Social Media, etc. We create strategies on how internet users will be aware of your website and will eventually bring profit to your business.</p>
                        	<p><strong id="webdesign-6">Custom Website Design and Programming</strong> –  We provide web development packages according to your choice. We design and program code websites that will ensure 100% user experience and are appropriate to the nature of your business.</p>
                            
                            <p>What are you waiting for? Request a <a href="free-quote.php" title="Free Quote">free-quote</a> now!</p>
                        </li>
                        <li>
                        	<h1 class="blue" id="software">Software Development </h1>
                        	<p>We create software for computing functions such as sales and inventory system, use different database engines depending on the customer requirements and develop software based on your needs. Our software programs are highly customized, unique and are cleverly and artistically designed to give you more than just functionality but user-friendly interface as well.</p>
                        	<p>
                            	Our programmers make use of the common languages such as Java, Delphi, Visual Basic  and NET (C#). Our programmers depend on the usage of programming languages on the nature of the software to be developed.
                            </p>
                          	<p>
                                To avail this service, <a href="software-development-freequote-request.doc" title="Software Development Free Quote Request">download our software development form</a> and send back to us to <a href="mailto:info@creativewebsolution.net">info@creativewebsolution.net</a> 
                         	</p>
                        </li>
                    </ol>
                    
                    
                  	  
              </div><!-- end of #leftpane-->
                <div id="rightpane">
                	
                    <?php include_once("php-include/contact-quote.php");?>
                    
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  

	<?php include_once("php-include/footer.php"); ?>

</body>
</html>
