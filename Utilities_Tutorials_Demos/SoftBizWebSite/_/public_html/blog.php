<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - Blog</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog blog-active">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="blog">
            	<h1>News and interesting reads on information technology</h1>
         </div>
            <div id="content-wrapper">
            	<div id="leftpane">
                
                	<div class="blogwrapper">
                        <h2 class="blog-titles">
                            <span class="calendar">
                                <span class="dateholder">09</span>
                                <span class="month-year-holder">Oct 2010</span>
                            </span>
                            <span class="blog-title-text" id="entry1">CWS Launches Website</span>
                        </h2>
                        <!-- end of .blog-titles -->
                        <div class="blog-details">
                            
                            
                            <p>Launching a company website is beyond question, which is why  Creative Web Solution takes its part in the web. CEO Abbas Khan, along with his  teams, has finally come up with this website.</p>
                            <p>Creative  Web Solution has been operating for quite some time now, gaining clients whose  trust has been given to the company. </p>
                            <p>Bearing the slogan “We define  creativity”, Creative Web Solution strongly believes that the overall success  of a project is with its simplicity, along with user-friendly interface. This  website serves as an example of what the company is.</p>
                            <p>Creative Web Solution offers  different information technology services such as web design and programming,  SEO, ad listings, content, and software development. </p>
                            <p>“Our focus is on the clients,” said  CEO Khan, “we broaden our scope to give more services to them.” He sees  potential in the company, achieving growth in the next year.</p>
                            <p>
                                <strong class="green">
                                    Administrator<br />
                                    
                                </strong>
                            
                            	<a href=".">www.creativewebsolution.net</a>
                            </p>
                            
                      </div><!-- end of .blog-details -->
                  </div><!-- end of .blogwrapper-->	
                	
                    <div class="blogwrapper">
                        <h2 class="blog-titles">
                            <span class="calendar">
                                <span class="dateholder">07</span>
                                <span class="month-year-holder">Oct 2010</span>
                            </span>
                            <span class="blog-title-text" id="entry2">Message from the CEO</span>
                        </h2>
                        <!-- end of .blog-titles -->
                        <div class="blog-details">
                            
                            <p><img src="images-content/ceox.jpg" alt="" class="FL" />Today’s businesses greatly demand attention from people  through the internet. Millions of internet users are in search for particular  website that will cater to their wants and needs. The main point in founding Creative  Web Solution is basically to help businesses grow through information  technology.</p>
                            <p>Creative Web Solution, among the many IT-related companies  online, seeks to meet the business demands of clients. I founded this company  in pursuit of delivering superior outputs to clients. What separates this  company from others is the way we work. We believe that passion, skill and  creativity are the three main ingredients to a successful IT company.</p>
                            <p>In Creative Web Solution, we do not treat our employers as  merely human resources; we treat them as the key to our company growth.  Everyone in the company is useful. We believe and achieve synergy because of  that.</p>
                            <p>In a short span of time, this company has rendered different  IT services to many clients. I am glad to inform you that this milestone spells  further growth of the company. We do not stop studying and mastering IT  principles since these change and become more advanced every single day.</p>
                            <p>We are a client-oriented company. We prioritize our clients’  needs above all, without sacrificing our accuracy and precision in our job. Our  mission and vision statement greatly focuses on our clients.</p>
                            <p>We aim to grow more in the years to come – to broaden the  scope of our services and to cater to the needs and demands of more clients.</p>
                            <p>We are a versatile company, meaning, we can adjust to our  clients’ demands. We do not just stick to what we believe is right; we also  make room for other suggestions. We are a 360-degree company, because we are  versatile.</p>
                            <p>Creative Web Solution is your key towards your  business’ success. I welcome you to our website.</p>
                            <p>
                            	<strong class="green">
                                	Abbas Khan<br />
                                    CEO, Creative Web Solution
                                </strong><br />
                                
                            	<a href=".">www.creativewebsolution.net</a>
                            </p>
                            
                      </div><!-- end of .blog-details -->
                  </div><!-- end of .blogwrapper-->	
                	
                	<div class="blogwrapper">
                        <h2 class="blog-titles">
                            <span class="calendar">
                                <span class="dateholder">29</span>
                                <span class="month-year-holder">Sept 2010</span>
                            </span>
                            <span class="blog-title-text" id="entry3">How important are Search Engines in Today’s Biz</span>
                        </h2>
                    	<!-- end of .blog-titles -->
                    	<div class="blog-details">
                    
                            <p>You may notice that today’s businesses with different niches  have websites. These websites are not just a part of the businesses’ glamour;  they play an even more vital role.</p>
                            <p>Millions of  users use the internet everyday for a lot of purposes. A huge percentage of  these users are in search for products and services their either want or need.  If you have a business that offers products and services, you may want users to  go to your website. But how are you going to do that?</p>
                            <p>This is  where search engines are of great value. Search engines like Google, Yahoo and  Bing are among the most visited websites everyday. How are they going to help  you?</p>
                            <p>Picture  this: a very huge place has a wide variety of choices between similar products  and services. A user may find it difficult to choose among his options. How are  you going to make the user choose your product? Make it visible.</p>
                            <p>There are  overwhelmingly broad options a user may find in search engines. For example he types  “computers” in a search engine. There will be millions of results found in a  split second. These results are known as competitors.</p>
                            <p>Users would  only notice the results found on the first few pages of his query. Your goal is  to place your website on the first page for users to notice it. If your website  lands on the tenth page of the results, your website’s chance of being noticed  is close to zero.</p>
                            <p>The bottom  line is, you are closer to selling your product when users visit your website.  There are people who do this as a career since the process is complicated, let  alone timely.</p>
                            <p>This is  where search engine optimization (SEO) specialists enter the scene. SEOs have  many techniques on how to make websites rank on search engines. Their job is  not only to rank their clients’ websites; they also maintain such good ranking  as well.</p>
                            <p>Your  competitors will get in the way to get the top ranking. Their goal is similar  to yours. It is up to the SEO to use better approaches to rank your website.</p>
                            <p> Search  engines can be <em>very hard to please</em> but can play a very important role in your business.
                              
                            </p>
                            <p>
                            <strong class="green">Administrator</strong><br />
                                <a href=".">www.creativewebsolution.net</a>
                            </p>
                        
                    	</div><!-- end of .blog-details -->
                  	</div><!-- end of .blogwrapper-->	
                 	
                    
                    <div class="blogwrapper">
                        <h2 class="blog-titles">
                            <span class="calendar">
                                <span class="dateholder">23</span>
                                <span class="month-year-holder">Sept 2010</span>
                            </span>
                            <span class="blog-title-text" id="entry4">Android Smartphones Please the Corporate World</span>
                        </h2>
                        <!-- end of .blog-titles -->
                    	<div class="blog-details">
                    
                            <p>It is true that the Google-based open source operating  system, Android, has been widely used by corporations. This has been proven a  fact since Android has beaten Apple’s iPhone in the last three months, reports  say.</p>
                            <p>According  to a survey conducted by ChangeWave last August, 16% of 1,600 IT buyers are  using Android smartphones. This means that the Google OS has jumped to 60% in  the last three months, beating Apple’s iPhone increase of nearly 3%.</p>
                            <p>Research in  Motion’s BlackBerry remains the leading manufactured smartphone in the market  but its growth is nothing compared to that of Android-based smartphones.</p>
                            <p>With these  figures, analysts have tagged Android as the leading competitor of both the iOS  and the BlackBerry OS.</p>
                            <p>Many of  these analysts predict that the Android will beat Nokia’s Symbian OS by 2014.  Although the reports differ since some only covered the US, it is clear  that this Google OS is sure to give other operating systems a run for their  money.</p>
                            <p>HTC and  Motorola are known smartphones running on Android OS. The sales of both these  giants have increased since they have been chosen for business purchase.</p>
                            <p>Google’s  Android OS is seen as promising. It continues to please the corporate world.</p>
                            <p>
                                <strong class="green">Administrator</strong><br />
                                <a href=".">www.creativewebsolution.net</a><br />
                                
                            </p>
                    	</div><!-- end of .blog-details -->
                  	</div><!-- end of .blogwrapper-->
                       
              	
           	    	<div class="blogwrapper">
                        <h2 class="blog-titles">
                            <span class="calendar">
                                <span class="dateholder">20</span>
                                <span class="month-year-holder">Sept 2010</span>
                            </span>
                            <span class="blog-title-text" id="entry5">Google Page Rank: A Mere Webmaster Fetish?</span>
                        </h2>
                        <!-- end of .blog-titles -->
                        <div class="blog-details">
                        
                            <p>The Google  Page Rank is a scale to measure a web page’s importance in Google – in its  results page. Many webmasters and business site’s owners were desperate to  obtain a high page rank since they equate it to the success of the business but  is page rank really worth the effort or just a mere value?</p>
                            <p>Google’s  algorithm allows every indexed web page to have its worth in the scale of 1-10.  The search engine giant bases each page’s rank through the number of back links  (how many other sites link to the page) the page has and how important these  web pages are.</p>
                            <p>Search  engine optimization (SEO) specialists come to an argument that page rank is the  only factor to be considered in the success of an entire website. These people  do on-page and off-page optimization to make web pages rank in the Google  search engine results page (SERP). So is page rank the reason why SEOs exist? It  is, partially.</p>
                            <p>Page rank  is just one of the several factors a webmaster should consider in the success  of a website. While some webmasters are blinded with the fact that a web page  with a high page rank (say, a value greater than 5) concludes the work, the job  does not end there.</p>
                            <p>Traffic is  another major factor in the success of a web page. It is not directly related  to page rank, but it can contribute. For example, a webmaster built links on  web pages with 0 or 1 page rank; these links can still bring traffic to the web  page, even if it has a page rank of just 2 or 3. It does not follow that if a  web page has a low page rank it is not visited by other users. This fact has  made the Google page rank decrease its value.</p>
                            <p>So  while page rank is a “wow” factor for many, it does not totally declare a web  page’s entire success in search engines. It is not a mere webmaster fetish but  rather a very important factor. Traffic and page rank should complement. </p>
                            <p>
                            <strong class="green">Adminstrator</strong><br />
                                <a href=".">www.creativewebsolution.net</a><br />
                            </p>
                        </div><!-- end of .blog-details -->
                        
                        <div class="pagination-wrapper">
                       		<ul class="pagination">
                            	<li>Page 1 of 25:</li>
                                <li><a href="#" title="Previous">&laquo;</a></li>
                            	<li><a href="#" class="page-active">1</a></li>
                                <li><a href="#" title="2">2</a></li>
                                <li><a href="#" title="3">3</a></li>
                                <li><a href="#" title="Next">&raquo;</a></li>
                                <li><a href="#" title="10">10</a></li>
                                <li><a href="#" title="20">20</a></li>
                               
                                <li>...</li>
                                <li><a href="#" title="Go to last page">last &raquo;</a></li>
                            </ul> 
                        </div><!-- end of .pagination-wrapper -->
                        
                  	</div><!-- end of .blogwrapper-->
                    
                </div><!-- end of #leftpane-->
                <div id="rightpane">
                	 
					<div class="rightpane-header"> <h2>Categories</h2></div><!-- end of .rightpane-header -->
                    
                    <ul class="rightnavigation">
                        <li><a href="#" title="Company News (3)">Company News (3)</a></li>
                        <li><a href="#" title="SEO/Online Marketing (2)">SEO/Online Marketing (2)</a></li>
                        <li><a href="#" title="Software Development">Software Development</a></li>
                        <li><a href="#" title="Web Design">Web Design</a></li>
                        <li><a href="#" title="Tech News">Tech News</a></li>
                    </ul>
                    <br class="clear" /><!-- don't remove -->
                    
                    <div class="rightpane-header"> <h2>Recent Post</h2></div><!-- end of .rightpane-header -->
                    
                    <ul class="rightnavigation">
                        
                        <li><a href="#entry1" title="CWS Launches Website">CWS Launches Website</a></li>
                        <li><a href="#entry2" title="Message from the CEO">Message from the CEO</a></li>
                        <li><a href="#entry3" title="How important are Search Engines in....">How important are Search Engines in....</a></li>
                        <li><a href="#entry4" title="Google Page Rank: A Mere Webmaster...">Google Page Rank: A Mere Webmaster...</a></li>
                        <li><a href="#entry5" title="Android Smartphones Please the...">Android Smartphones Please the...</a></li>
                    </ul>
                    <br class="clear" /><!-- don't remove -->
                   
                   	<?php include_once("php-include/contact-quote.php");?>
                   
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  
  
	<?php include_once("php-include/footer.php"); ?>
  	
</body>
</html>
