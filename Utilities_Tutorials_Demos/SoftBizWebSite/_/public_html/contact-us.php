<?php
    include_once 'php-include/captcha/check.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - Contact us</title>

<?php include_once("php-include/styles-js.php");?>

<script type="text/javascript">
$(document).ready(function(){
    $('form').submit(function(){
        var fields = []
        if (!$('input[name=first_name]').val()) {
            fields.push('Firstname');
        }
        if (!$('input[name=last_name]').val()) {
            fields.push('Lastname');
        }
        if (!$('input[name=email]').val()) {
            fields.push('Email');
        }
        
        if (!$('input[name=city]').val()) {
            fields.push('City');
        }
        if (!$('select').val()) {
            fields.push('Country');
        }
        if (!$('textarea').val()) {
            fields.push('Questions');
        }
        if (!$('input[name=security_code]').val()) {
            fields.push('Validation code.');
        }

        var msg = '';
        if (fields.length > 0) {
            var msg = 'Please fill out ';
            if (fields.length > 1) {
                for (i = 0; i < fields.length - 1; i++) msg += fields[i] + ', ';
                msg += ' and ' + fields[i];
            } else {
                msg += fields[0];
            }
        }

        if ($('input[name=email]').val()) {
            var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
            if (!pattern.test($('input[name=email]').val())) {
                msg += msg ? '\n':'';
                msg = 'Invalid email address.';
            }
        }

        if (msg) {
            Sexy.error(msg);
            return false;
        }
    }) // end submit
	
}) // end ready


</script>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus contactus-active">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="contact-us">
            	<h1>Your queries, inquiries and feedbacks matter to us</h1>
         </div>
            <div id="content-wrapper">
            	
                <div class="formswrapper">
                	<img src="images/contact-ribbon.jpg" alt="" class="ribbonsettings" />
                    <div class="formswrapper-inner">
                    
                    	<h1 class="green" style="padding-left:220px;">
                        	Send us your inquires or Questions<br />
                        	<small class="red">Please note that fields marked with * are required</small>
                        </h1>
                        <form action="<?php $_SERVER['SCRIPT_NAME']?>#message" method="post">
                        	<div style="padding-left:160px;">
                            	<?php if (isset($errors) && count($errors) > 0): ?>
                                    <h1 id="message" class="red">Failed to submit because of the following errors:</h1>
                                    <ol>
                                        <?php foreach ($errors as $e): ?>
                                        <li><?php echo $e ?></li>
                                        <?php endforeach; ?>
                                    </ol>
                                    <?php elseif (isset($success)): ?>
                                    <h1 id="message" class="green"><?php echo $success ?></h1>
                                <?php endif ?>
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="5">
                                <tr>
                                    <td align="right" width="150"><strong>Firstname<span class="red">*</span></strong></td>
                                    <td align="left"><input type="text" name="first_name" size="30" value="<?php echo $form_vals['first_name']?>" /></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Lastname<span class="red">*</span> </strong></td>
                                    <td align="left"><input type="text" name="last_name" size="30" value="<?php echo $form_vals['last_name']?>" /></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Email address<span class="red">*</span></strong></td>
                                    <td align="left"><input type="text" name="email" value="<?php echo $form_vals['email']?>" /></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Phone</strong></td>
                                    <td align="left"><input type="text" value="<?php echo $form_vals['phone']?>" name="phone" /></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>City<span class="red">*</span></strong></td>
                                    <td align="left"><input type="text" name="city" value="<?php echo $form_vals['city']?>" /></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Country<span class="red">*</span></strong></td>
                                    <td align="left">
                                    	<select name="country">
                                            <option <?php if ($form_vals['country'] == '') echo 'selected="selected"'?> value="">---- Please choose your country -----</option>
                                            <option <?php if ($form_vals['country'] == 'United States') echo 'selected="selected"'?> value="United States">United States</option>
                                            <option <?php if ($form_vals['country'] == 'Afghanistan') echo 'selected="selected"'?> value="Afghanistan">Afghanistan</option>
                                            <option <?php if ($form_vals['country'] == 'Albania') echo 'selected="selected"'?> value="Albania">Albania</option>
                                            <option <?php if ($form_vals['country'] == 'Algeria') echo 'selected="selected"'?> value="Algeria">Algeria</option>
                                            <option <?php if ($form_vals['country'] == 'American Samoa') echo 'selected="selected"'?> value="American Samoa">American Samoa</option>
                                            <option <?php if ($form_vals['country'] == 'Andorra') echo 'selected="selected"'?> value="Andorra">Andorra</option>
                                            <option <?php if ($form_vals['country'] == 'Angola') echo 'selected="selected"'?> value="Angola">Angola</option>
                                            <option <?php if ($form_vals['country'] == 'AI') echo 'selected="selected"'?> value="AI">Anguilla</option>
                                            <option <?php if ($form_vals['country'] == 'Anguilla') echo 'selected="selected"'?> value="Anguilla">Antarctica</option>
                                            <option <?php if ($form_vals['country'] == 'Antigua and Barbuda') echo 'selected="selected"'?> value="Antigua and Barbuda">Antigua and Barbuda</option>
                                            <option <?php if ($form_vals['country'] == 'Argentina') echo 'selected="selected"'?> value="Argentina">Argentina</option>
                                            <option <?php if ($form_vals['country'] == 'Armenia') echo 'selected="selected"'?> value="Armenia">Armenia</option>
                                            <option <?php if ($form_vals['country'] == 'Aruba') echo 'selected="selected"'?> value="Aruba">Aruba</option>
                                            <option <?php if ($form_vals['country'] == 'Australia') echo 'selected="selected"'?> value="Australia">Australia</option>
                                            <option <?php if ($form_vals['country'] == 'Austria') echo 'selected="selected"'?> value="Austria">Austria</option>
                                            <option <?php if ($form_vals['country'] == 'Azerbaijan') echo 'selected="selected"'?> value="Azerbaijan">Azerbaijan</option>
                                            <option <?php if ($form_vals['country'] == 'Bahamas') echo 'selected="selected"'?> value="Bahamas">Bahamas</option>
                                            <option <?php if ($form_vals['country'] == 'Bahrain') echo 'selected="selected"'?> value="Bahrain">Bahrain</option>
                                            <option <?php if ($form_vals['country'] == 'Bangladesh') echo 'selected="selected"'?> value="Bangladesh">Bangladesh</option>
                                            <option <?php if ($form_vals['country'] == 'Barbados') echo 'selected="selected"'?> value="Barbados">Barbados</option>
                                            <option <?php if ($form_vals['country'] == 'Belarus') echo 'selected="selected"'?> value="Belarus">Belarus</option>
                                            <option <?php if ($form_vals['country'] == 'Belgium') echo 'selected="selected"'?> value="Belgium">Belgium</option>
                                            <option <?php if ($form_vals['country'] == 'Belize') echo 'selected="selected"'?> value="Belize">Belize</option>
                                            <option <?php if ($form_vals['country'] == 'Benin') echo 'selected="selected"'?> value="Benin">Benin</option>
                                            <option <?php if ($form_vals['country'] == 'Bermuda') echo 'selected="selected"'?> value="Bermuda">Bermuda</option>
                                            <option <?php if ($form_vals['country'] == 'Bhutan') echo 'selected="selected"'?> value="Bhutan">Bhutan</option>
                                            <option <?php if ($form_vals['country'] == 'Bolivia') echo 'selected="selected"'?> value="Bolivia">Bolivia</option>
                                            <option <?php if ($form_vals['country'] == 'Bosnia and Herzegowina') echo 'selected="selected"'?> value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                            <option <?php if ($form_vals['country'] == 'Botswana') echo 'selected="selected"'?> value="Botswana">Botswana</option>
                                            <option <?php if ($form_vals['country'] == 'Bouvet Island') echo 'selected="selected"'?> value="Bouvet Island">Bouvet Island</option>
                                            <option <?php if ($form_vals['country'] == 'Brazil') echo 'selected="selected"'?> value="Brazil">Brazil</option>
                                            <option <?php if ($form_vals['country'] == 'British Indian Ocean Territory') echo 'selected="selected"'?> value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                            <option <?php if ($form_vals['country'] == 'Brunei Darussalam') echo 'selected="selected"'?> value="Brunei Darussalam">Brunei Darussalam</option>
                                            <option <?php if ($form_vals['country'] == 'Bulgaria') echo 'selected="selected"'?> value="Bulgaria">Bulgaria</option>
                                            <option <?php if ($form_vals['country'] == 'Burkina Faso') echo 'selected="selected"'?> value="Burkina Faso">Burkina Faso</option>
                                            <option <?php if ($form_vals['country'] == 'Burundi') echo 'selected="selected"'?> value="Burundi">Burundi</option>
                                            <option <?php if ($form_vals['country'] == 'Cambodia') echo 'selected="selected"'?> value="Cambodia">Cambodia</option>
                                            <option <?php if ($form_vals['country'] == 'Cameroon') echo 'selected="selected"'?> value="Cameroon">Cameroon</option>
                                            <option <?php if ($form_vals['country'] == 'Canada') echo 'selected="selected"'?> value="Canada">Canada</option>
                                            <option <?php if ($form_vals['country'] == 'Cape Verde') echo 'selected="selected"'?> value="Cape Verde">Cape Verde</option>
                                            <option <?php if ($form_vals['country'] == 'Cayman Islands') echo 'selected="selected"'?> value="Cayman Islands">Cayman Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Central African Republic') echo 'selected="selected"'?> value="Central African Republic">Central African Republic</option>
                                            <option <?php if ($form_vals['country'] == 'Chad') echo 'selected="selected"'?> value="Chad">Chad</option>
                                            <option <?php if ($form_vals['country'] == 'Chile') echo 'selected="selected"'?> value="Chile">Chile</option>
                                            <option <?php if ($form_vals['country'] == 'China') echo 'selected="selected"'?> value="China">China</option>
                                            <option <?php if ($form_vals['country'] == 'Christmas Island') echo 'selected="selected"'?> value="Christmas Island">Christmas Island</option>
                                            <option <?php if ($form_vals['country'] == 'Cocoa (Keeling) Islands') echo 'selected="selected"'?> value="Cocoa (Keeling) Islands">Cocoa (Keeling) Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Colombia') echo 'selected="selected"'?> value="Colombia">Colombia</option>
                                            <option <?php if ($form_vals['country'] == 'Comoros') echo 'selected="selected"'?> value="Comoros">Comoros</option>
                                            <option <?php if ($form_vals['country'] == 'Congo') echo 'selected="selected"'?> value="Congo">Congo</option>
                                            <option <?php if ($form_vals['country'] == 'Cook Islands') echo 'selected="selected"'?> value="Cook Islands">Cook Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Costa Rica') echo 'selected="selected"'?> value="Costa Rica">Costa Rica</option>
                                            <option <?php if ($form_vals['country'] == 'Cote Divoire') echo 'selected="selected"'?> value="Cote Divoire">Cote Divoire</option>
                                            <option <?php if ($form_vals['country'] == 'Croatia (Hrvatska)') echo 'selected="selected"'?> value="Croatia (Hrvatska)">Croatia (Hrvatska)</option>
                                            <option <?php if ($form_vals['country'] == 'Cuba') echo 'selected="selected"'?> value="Cuba">Cuba</option>
                                            <option <?php if ($form_vals['country'] == 'Cyprus') echo 'selected="selected"'?> value="Cyprus">Cyprus</option>
                                            <option <?php if ($form_vals['country'] == 'Czech Republic') echo 'selected="selected"'?> value="Czech Republic">Czech Republic</option>
                                            <option <?php if ($form_vals['country'] == 'Denmark') echo 'selected="selected"'?> value="Denmark">Denmark</option>
                                            <option <?php if ($form_vals['country'] == 'Djibouti') echo 'selected="selected"'?> value="Djibouti">Djibouti</option>
                                            <option <?php if ($form_vals['country'] == 'Dominica') echo 'selected="selected"'?> value="Dominica">Dominica</option>
                                            <option <?php if ($form_vals['country'] == 'Dominican Republic') echo 'selected="selected"'?> value="Dominican Republic">Dominican Republic</option>
                                            <option <?php if ($form_vals['country'] == 'East Timor') echo 'selected="selected"'?> value="East Timor">East Timor</option>
                                            <option <?php if ($form_vals['country'] == 'Ecuador') echo 'selected="selected"'?> value="Ecuador">Ecuador</option>
                                            <option <?php if ($form_vals['country'] == 'Egypt') echo 'selected="selected"'?> value="Egypt">Egypt</option>
                                            <option <?php if ($form_vals['country'] == 'El Salvador') echo 'selected="selected"'?> value="El Salvador">El Salvador</option>
                                            <option <?php if ($form_vals['country'] == 'Equatorial Guinea') echo 'selected="selected"'?> value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option <?php if ($form_vals['country'] == 'Eritrea') echo 'selected="selected"'?> value="Eritrea">Eritrea</option>
                                            <option <?php if ($form_vals['country'] == 'Estonia') echo 'selected="selected"'?> value="Estonia">Estonia</option>
                                            <option <?php if ($form_vals['country'] == 'Ethiopia') echo 'selected="selected"'?> value="Ethiopia">Ethiopia</option>
                                            <option <?php if ($form_vals['country'] == 'Falkland Islands (Malvinas)') echo 'selected="selected"'?> value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                            <option <?php if ($form_vals['country'] == 'Faroe Islands') echo 'selected="selected"'?> value="Faroe Islands">Faroe Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Fiji') echo 'selected="selected"'?> value="Fiji">Fiji</option>
                                            <option <?php if ($form_vals['country'] == 'Finland') echo 'selected="selected"'?> value="Finland">Finland</option>
                                            <option <?php if ($form_vals['country'] == 'France') echo 'selected="selected"'?> value="France">France</option>
                                            <option <?php if ($form_vals['country'] == 'France, Metropolitan') echo 'selected="selected"'?> value="France, Metropolitan">France, Metropolitan</option>
                                            <option <?php if ($form_vals['country'] == 'French Guiana') echo 'selected="selected"'?> value="French Guiana">French Guiana</option>
                                            <option <?php if ($form_vals['country'] == 'French Polynesia') echo 'selected="selected"'?> value="French Polynesia">French Polynesia</option>
                                            <option <?php if ($form_vals['country'] == 'French Southern Territories') echo 'selected="selected"'?> value="French Southern Territories">French Southern Territories</option>
                                            <option <?php if ($form_vals['country'] == 'Gabon') echo 'selected="selected"'?> value="Gabon">Gabon</option>
                                            <option <?php if ($form_vals['country'] == 'Gambia') echo 'selected="selected"'?> value="Gambia">Gambia</option>
                                            <option <?php if ($form_vals['country'] == 'Georgia') echo 'selected="selected"'?> value="Georgia">Georgia</option>
                                            <option <?php if ($form_vals['country'] == 'Germany') echo 'selected="selected"'?> value="Germany">Germany</option>
                                            <option <?php if ($form_vals['country'] == 'Ghana') echo 'selected="selected"'?> value="Ghana">Ghana</option>
                                            <option <?php if ($form_vals['country'] == 'Gibraltar') echo 'selected="selected"'?> value="Gibraltar">Gibraltar</option>
                                            <option <?php if ($form_vals['country'] == 'Greece') echo 'selected="selected"'?> value="Greece">Greece</option>
                                            <option <?php if ($form_vals['country'] == 'Greenland') echo 'selected="selected"'?> value="Greenland">Greenland</option>
                                            <option <?php if ($form_vals['country'] == 'Grenada') echo 'selected="selected"'?> value="Grenada">Grenada</option>
                                            <option <?php if ($form_vals['country'] == 'Guadeloupe') echo 'selected="selected"'?> value="Guadeloupe">Guadeloupe</option>
                                            <option <?php if ($form_vals['country'] == 'Guam') echo 'selected="selected"'?> value="Guam">Guam</option>
                                            <option <?php if ($form_vals['country'] == 'Guatemala') echo 'selected="selected"'?> value="Guatemala">Guatemala</option>
                                            <option <?php if ($form_vals['country'] == 'Guinea') echo 'selected="selected"'?> value="Guinea">Guinea</option>
                                            <option <?php if ($form_vals['country'] == 'Guinea-Bissau') echo 'selected="selected"'?> value="Guinea-Bissau">Guinea-Bissau</option>
                                            <option <?php if ($form_vals['country'] == 'Guyana') echo 'selected="selected"'?> value="Guyana">Guyana</option>
                                            <option <?php if ($form_vals['country'] == 'Haiti') echo 'selected="selected"'?> value="Haiti">Haiti</option>
                                            <option <?php if ($form_vals['country'] == 'Heard and Mc Donald Islands') echo 'selected="selected"'?> value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Honduras') echo 'selected="selected"'?> value="Honduras">Honduras</option>
                                            <option <?php if ($form_vals['country'] == 'Hong Kong') echo 'selected="selected"'?> value="Hong Kong">Hong Kong</option>
                                            <option <?php if ($form_vals['country'] == 'Hungary') echo 'selected="selected"'?> value="Hungary">Hungary</option>
                                            <option <?php if ($form_vals['country'] == 'Iceland') echo 'selected="selected"'?> value="Iceland">Iceland</option>
                                            <option <?php if ($form_vals['country'] == 'India') echo 'selected="selected"'?> value="India">India</option>
                                            <option <?php if ($form_vals['country'] == 'Indonesia') echo 'selected="selected"'?> value="Indonesia">Indonesia</option>
                                            <option <?php if ($form_vals['country'] == 'Iran (Islamic Republic of)') echo 'selected="selected"'?> value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
                                            <option <?php if ($form_vals['country'] == 'Iraq') echo 'selected="selected"'?> value="Iraq">Iraq</option>
                                            <option <?php if ($form_vals['country'] == 'Ireland') echo 'selected="selected"'?> value="Ireland">Ireland</option>
                                            <option <?php if ($form_vals['country'] == 'Israel') echo 'selected="selected"'?> value="Israel">Israel</option>
                                            <option <?php if ($form_vals['country'] == 'Italy') echo 'selected="selected"'?> value="Italy">Italy</option>
                                            <option <?php if ($form_vals['country'] == 'Jamaica') echo 'selected="selected"'?> value="Jamaica">Jamaica</option>
                                            <option <?php if ($form_vals['country'] == 'Japan') echo 'selected="selected"'?> value="Japan">Japan</option>
                                            <option <?php if ($form_vals['country'] == 'Jordan') echo 'selected="selected"'?> value="Jordan">Jordan</option>
                                            <option <?php if ($form_vals['country'] == 'Kazakhstan') echo 'selected="selected"'?> value="Kazakhstan">Kazakhstan</option>
                                            <option <?php if ($form_vals['country'] == 'Kenya') echo 'selected="selected"'?> value="Kenya">Kenya</option>
                                            <option <?php if ($form_vals['country'] == 'Kiribati') echo 'selected="selected"'?> value="Kiribati">Kiribati</option>
                                            <option <?php if ($form_vals['country'] == 'Korea, Democratic Peoples Republic of') echo 'selected="selected"'?> value="Korea, Democratic Peoples Republic of">Korea, Democratic Peoples Republic of</option>
                                            <option <?php if ($form_vals['country'] == 'Korea, Republic of') echo 'selected="selected"'?> value="Korea, Republic of">Korea, Republic of</option>
                                            <option <?php if ($form_vals['country'] == 'Kuwait') echo 'selected="selected"'?> value="Kuwait">Kuwait</option>
                                            <option <?php if ($form_vals['country'] == 'Kyrgyzstan') echo 'selected="selected"'?> value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option <?php if ($form_vals['country'] == 'Lao Peoples Democratic Republic') echo 'selected="selected"'?> value="Lao Peoples Democratic Republic">Lao Peoples Democratic Republic</option>
                                            <option <?php if ($form_vals['country'] == 'Latvia') echo 'selected="selected"'?> value="Latvia">Latvia</option>
                                            <option <?php if ($form_vals['country'] == 'Lebanon') echo 'selected="selected"'?> value="Lebanon">Lebanon</option>
                                            <option <?php if ($form_vals['country'] == 'Lesotho') echo 'selected="selected"'?> value="Lesotho">Lesotho</option>
                                            <option <?php if ($form_vals['country'] == 'Liberia') echo 'selected="selected"'?> value="Liberia">Liberia</option>
                                            <option <?php if ($form_vals['country'] == 'Libyan Arab Jamahiriya') echo 'selected="selected"'?> value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                            <option <?php if ($form_vals['country'] == 'Liechtenstein') echo 'selected="selected"'?> value="Liechtenstein">Liechtenstein</option>
                                            <option <?php if ($form_vals['country'] == 'Lithuania') echo 'selected="selected"'?> value="Lithuania">Lithuania</option>
                                            <option <?php if ($form_vals['country'] == 'Luxembourg') echo 'selected="selected"'?> value="Luxembourg">Luxembourg</option>
                                            <option <?php if ($form_vals['country'] == 'Macau') echo 'selected="selected"'?> value="Macau">Macau</option>
                                            <option <?php if ($form_vals['country'] == 'Macedonia, The Former Yugoslav Republic of') echo 'selected="selected"'?> value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                            <option <?php if ($form_vals['country'] == 'Madagascar') echo 'selected="selected"'?> value="Madagascar">Madagascar</option>
                                            <option <?php if ($form_vals['country'] == 'Malawi') echo 'selected="selected"'?> value="Malawi">Malawi</option>
                                            <option <?php if ($form_vals['country'] == 'Malaysia') echo 'selected="selected"'?> value="Malaysia">Malaysia</option>
                                            <option <?php if ($form_vals['country'] == 'Maldives') echo 'selected="selected"'?> value="Maldives">Maldives</option>
                                            <option <?php if ($form_vals['country'] == 'Mali') echo 'selected="selected"'?> value="Mali">Mali</option>
                                            <option <?php if ($form_vals['country'] == 'Malta') echo 'selected="selected"'?> value="Malta">Malta</option>
                                            <option <?php if ($form_vals['country'] == 'Marshall Islands') echo 'selected="selected"'?> value="Marshall Islands">Marshall Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Martinique') echo 'selected="selected"'?> value="Martinique">Martinique</option>
                                            <option <?php if ($form_vals['country'] == 'Mauritania') echo 'selected="selected"'?> value="Mauritania">Mauritania</option>
                                            <option <?php if ($form_vals['country'] == 'Mauritius') echo 'selected="selected"'?> value="Mauritius">Mauritius</option>
                                            <option <?php if ($form_vals['country'] == 'Mayotte') echo 'selected="selected"'?> value="Mayotte">Mayotte</option>
                                            <option <?php if ($form_vals['country'] == 'Mexico') echo 'selected="selected"'?> value="Mexico">Mexico</option>
                                            <option <?php if ($form_vals['country'] == 'Micronesia, Federated States of') echo 'selected="selected"'?> value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                            <option <?php if ($form_vals['country'] == 'Moldova, Republic of') echo 'selected="selected"'?> value="Moldova, Republic of">Moldova, Republic of</option>
                                            <option <?php if ($form_vals['country'] == 'Monaco') echo 'selected="selected"'?> value="Monaco">Monaco</option>
                                            <option <?php if ($form_vals['country'] == 'Mongolia') echo 'selected="selected"'?> value="Mongolia">Mongolia</option>
                                            <option <?php if ($form_vals['country'] == 'Montserrat') echo 'selected="selected"'?> value="Montserrat">Montserrat</option>
                                            <option <?php if ($form_vals['country'] == 'Morocco') echo 'selected="selected"'?> value="Morocco">Morocco</option>
                                            <option <?php if ($form_vals['country'] == 'Mozambique') echo 'selected="selected"'?> value="Mozambique">Mozambique</option>
                                            <option <?php if ($form_vals['country'] == 'Myanmar') echo 'selected="selected"'?> value="Myanmar">Myanmar</option>
                                            <option <?php if ($form_vals['country'] == 'Namibia') echo 'selected="selected"'?> value="Namibia">Namibia</option>
                                            <option <?php if ($form_vals['country'] == 'Nauru') echo 'selected="selected"'?> value="Nauru">Nauru</option>
                                            <option <?php if ($form_vals['country'] == 'Nepal') echo 'selected="selected"'?> value="Nepal">Nepal</option>
                                            <option <?php if ($form_vals['country'] == 'Netherlands') echo 'selected="selected"'?> value="Netherlands">Netherlands</option>
                                            <option <?php if ($form_vals['country'] == 'Netherlands Antilles') echo 'selected="selected"'?> value="Netherlands Antilles">Netherlands Antilles</option>
                                            <option <?php if ($form_vals['country'] == 'New Caledonia') echo 'selected="selected"'?> value="New Caledonia">New Caledonia</option>
                                            <option <?php if ($form_vals['country'] == 'New Zealand') echo 'selected="selected"'?> value="New Zealand">New Zealand</option>
                                            <option <?php if ($form_vals['country'] == 'Nicaragua') echo 'selected="selected"'?> value="Nicaragua">Nicaragua</option>
                                            <option <?php if ($form_vals['country'] == 'Niger') echo 'selected="selected"'?> value="Niger">Niger</option>
                                            <option <?php if ($form_vals['country'] == 'Nigeria') echo 'selected="selected"'?> value="Nigeria">Nigeria</option>
                                            <option <?php if ($form_vals['country'] == 'Niue') echo 'selected="selected"'?> value="Niue">Niue</option>
                                            <option <?php if ($form_vals['country'] == 'Norfolk Island') echo 'selected="selected"'?> value="Norfolk Island">Norfolk Island</option>
                                            <option <?php if ($form_vals['country'] == 'Northern Mariana Islands') echo 'selected="selected"'?> value="Northern Mariana Islands">Northern Mariana Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Norway') echo 'selected="selected"'?> value="Norway">Norway</option>
                                            <option <?php if ($form_vals['country'] == 'Oman') echo 'selected="selected"'?> value="Oman">Oman</option>
                                            <option <?php if ($form_vals['country'] == 'Pakistan') echo 'selected="selected"'?> value="Pakistan">Pakistan</option>
                                            <option <?php if ($form_vals['country'] == 'Palau') echo 'selected="selected"'?> value="Palau">Palau</option>
                                            <option <?php if ($form_vals['country'] == 'Panama') echo 'selected="selected"'?> value="Panama">Panama</option>
                                            <option <?php if ($form_vals['country'] == 'Papua New Guinea') echo 'selected="selected"'?> value="Papua New Guinea">Papua New Guinea</option>
                                            <option <?php if ($form_vals['country'] == 'Paraguay') echo 'selected="selected"'?> value="Paraguay">Paraguay</option>
                                            <option <?php if ($form_vals['country'] == 'Peru') echo 'selected="selected"'?> value="Peru">Peru</option>
                                            <option <?php if ($form_vals['country'] == 'Philippines') echo 'selected="selected"'?> value="Philippines">Philippines</option>
                                            <option <?php if ($form_vals['country'] == 'Pitcairn') echo 'selected="selected"'?> value="Pitcairn">Pitcairn</option>
                                            <option <?php if ($form_vals['country'] == 'Poland') echo 'selected="selected"'?> value="Poland">Poland</option>
                                            <option <?php if ($form_vals['country'] == 'Portugal') echo 'selected="selected"'?> value="Portugal">Portugal</option>
                                            <option <?php if ($form_vals['country'] == 'PuertoRico') echo 'selected="selected"'?> value="PuertoRico">PuertoRico</option>
                                            <option <?php if ($form_vals['country'] == 'Qatar') echo 'selected="selected"'?> value="Qatar">Qatar</option>
                                            <option <?php if ($form_vals['country'] == 'Reunion') echo 'selected="selected"'?> value="Reunion">Reunion</option>
                                            <option <?php if ($form_vals['country'] == 'Romania') echo 'selected="selected"'?> value="Romania">Romania</option>
                                            <option <?php if ($form_vals['country'] == 'Russian Federation') echo 'selected="selected"'?> value="Russian Federation">Russian Federation</option>
                                            <option <?php if ($form_vals['country'] == 'Rwanda') echo 'selected="selected"'?> value="Rwanda">Rwanda</option>
                                            <option <?php if ($form_vals['country'] == 'Saint Kitts and Nevis') echo 'selected="selected"'?> value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                            <option <?php if ($form_vals['country'] == 'Saint Lucia') echo 'selected="selected"'?> value="Saint Lucia">Saint Lucia</option>
                                            <option <?php if ($form_vals['country'] == 'Saint Vincent and the Grenadines') echo 'selected="selected"'?> value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                            <option <?php if ($form_vals['country'] == 'Samoa') echo 'selected="selected"'?> value="Samoa">Samoa</option>
                                            <option <?php if ($form_vals['country'] == 'SanMarino') echo 'selected="selected"'?> value="SanMarino">SanMarino</option>
                                            <option <?php if ($form_vals['country'] == 'Sao Tome and Principe') echo 'selected="selected"'?> value="Sao Tome and Principe">Sao Tome and Principe</option>
                                            <option <?php if ($form_vals['country'] == 'Saudi Arabia') echo 'selected="selected"'?> value="Saudi Arabia">Saudi Arabia</option>
                                            <option <?php if ($form_vals['country'] == 'Senegal') echo 'selected="selected"'?> value="Senegal">Senegal</option>
                                            <option <?php if ($form_vals['country'] == 'Seychelles') echo 'selected="selected"'?> value="Seychelles">Seychelles</option>
                                            <option <?php if ($form_vals['country'] == 'Sierra Leone') echo 'selected="selected"'?> value="Sierra Leone">Sierra Leone</option>
                                            <option <?php if ($form_vals['country'] == 'Singapore') echo 'selected="selected"'?> value="Singapore">Singapore</option>
                                            <option <?php if ($form_vals['country'] == 'Slovakia (Slovak Republic)') echo 'selected="selected"'?> value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
                                            <option <?php if ($form_vals['country'] == 'Slovenia') echo 'selected="selected"'?> value="Slovenia">Slovenia</option>
                                            <option <?php if ($form_vals['country'] == 'Solomon Islands') echo 'selected="selected"'?> value="Solomon Islands">Solomon Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Somalia') echo 'selected="selected"'?> value="Somalia">Somalia</option>
                                            <option <?php if ($form_vals['country'] == 'South Africa') echo 'selected="selected"'?> value="South Africa">South Africa</option>
                                            <option <?php if ($form_vals['country'] == 'SouthGeorgia and the South Sandwich Islands') echo 'selected="selected"'?> value="SouthGeorgia and the South Sandwich Islands">SouthGeorgia and the South Sandwich Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Spain') echo 'selected="selected"'?> value="Spain">Spain</option>
                                            <option <?php if ($form_vals['country'] == 'Sri Lanka') echo 'selected="selected"'?> value="Sri Lanka">Sri Lanka</option>
                                            <option <?php if ($form_vals['country'] == 'St. Helena') echo 'selected="selected"'?> value="St. Helena">St. Helena</option>
                                            <option <?php if ($form_vals['country'] == 'St. Pierre andMiquelon') echo 'selected="selected"'?> value="St. Pierre andMiquelon">St. Pierre and Miquelon</option>
                                            <option <?php if ($form_vals['country'] == 'Sudan') echo 'selected="selected"'?> value="Sudan">Sudan</option>
                                            <option <?php if ($form_vals['country'] == 'Suriname') echo 'selected="selected"'?> value="Suriname">Suriname</option>
                                            <option <?php if ($form_vals['country'] == 'Svalbard and Jan Mayen Islands') echo 'selected="selected"'?> value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Swaziland') echo 'selected="selected"'?> value="Swaziland">Swaziland</option>
                                            <option <?php if ($form_vals['country'] == 'Sweden') echo 'selected="selected"'?> value="Sweden">Sweden</option>
                                            <option <?php if ($form_vals['country'] == 'Switzerland') echo 'selected="selected"'?> value="Switzerland">Switzerland</option>
                                            <option <?php if ($form_vals['country'] == 'Syrian ArabRepublic') echo 'selected="selected"'?> value="Syrian ArabRepublic">Syrian ArabRepublic</option>
                                            <option <?php if ($form_vals['country'] == 'Taiwan') echo 'selected="selected"'?> value="Taiwan">Taiwan</option>
                                            <option <?php if ($form_vals['country'] == 'Tajikistan') echo 'selected="selected"'?> value="Tajikistan">Tajikistan</option>
                                            <option <?php if ($form_vals['country'] == 'Tanzania, United Republic of') echo 'selected="selected"'?> value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                            <option <?php if ($form_vals['country'] == 'Thailand') echo 'selected="selected"'?> value="Thailand">Thailand</option>
                                            <option <?php if ($form_vals['country'] == 'Togo') echo 'selected="selected"'?> value="Togo">Togo</option>
                                            <option <?php if ($form_vals['country'] == 'Tokelau') echo 'selected="selected"'?> value="Tokelau">Tokelau</option>
                                            <option <?php if ($form_vals['country'] == 'Tonga') echo 'selected="selected"'?> value="Tonga">Tonga</option>
                                            <option <?php if ($form_vals['country'] == 'Trinidad and Tobago') echo 'selected="selected"'?> value="Trinidad and Tobago">Trinidad and Tobago</option>
                                            <option <?php if ($form_vals['country'] == 'Tunisia') echo 'selected="selected"'?> value="Tunisia">Tunisia</option>
                                            <option <?php if ($form_vals['country'] == 'Turkey') echo 'selected="selected"'?> value="Turkey">Turkey</option>
                                            <option <?php if ($form_vals['country'] == 'Turkmenistan') echo 'selected="selected"'?> value="Turkmenistan">Turkmenistan</option>
                                            <option <?php if ($form_vals['country'] == 'Turks and Caicos Islands') echo 'selected="selected"'?> value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Tuvalu') echo 'selected="selected"'?> value="Tuvalu">Tuvalu</option>
                                            <option <?php if ($form_vals['country'] == 'Uganda') echo 'selected="selected"'?> value="Uganda">Uganda</option>
                                            <option <?php if ($form_vals['country'] == 'Ukraine') echo 'selected="selected"'?> value="Ukraine">Ukraine</option>
                                            <option <?php if ($form_vals['country'] == 'United Arab Emirates') echo 'selected="selected"'?> value="United Arab Emirates">United Arab Emirates</option>
                                            <option <?php if ($form_vals['country'] == 'United Kingdom') echo 'selected="selected"'?> value="United Kingdom">United Kingdom</option>
                                            <option <?php if ($form_vals['country'] == 'United States') echo 'selected="selected"'?> value="United States">United States</option>
                                            <option <?php if ($form_vals['country'] == 'United States Minor Outlying Islands') echo 'selected="selected"'?> value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Uruguay') echo 'selected="selected"'?> value="Uruguay">Uruguay</option>
                                            <option <?php if ($form_vals['country'] == 'Uzbekistan') echo 'selected="selected"'?> value="Uzbekistan">Uzbekistan</option>
                                            <option <?php if ($form_vals['country'] == 'Vanuatu') echo 'selected="selected"'?> value="Vanuatu">Vanuatu</option>
                                            <option <?php if ($form_vals['country'] == 'Vatican City State(Holy See)') echo 'selected="selected"'?> value="Vatican City State(Holy See)">Vatican City State(Holy See)</option>
                                            <option <?php if ($form_vals['country'] == 'Venezuela') echo 'selected="selected"'?> value="Venezuela">Venezuela</option>
                                            <option <?php if ($form_vals['country'] == 'Viet Nam') echo 'selected="selected"'?> value="Viet Nam">Viet Nam</option>
                                            <option <?php if ($form_vals['country'] == 'Virgin Islands (British)') echo 'selected="selected"'?> value="Virgin Islands (British)">Virgin Islands (British)</option>
                                            <option <?php if ($form_vals['country'] == 'Virgin Islands (U.S.)') echo 'selected="selected"'?> value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                                            <option <?php if ($form_vals['country'] == 'Wallis and Futuna Islands') echo 'selected="selected"'?> value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                                            <option <?php if ($form_vals['country'] == 'Western Sahara') echo 'selected="selected"'?> value="Western Sahara">Western Sahara</option>
                                            <option <?php if ($form_vals['country'] == 'Yeman') echo 'selected="selected"'?> value="Yeman">Yeman</option>
                                            <option <?php if ($form_vals['country'] == 'Yugoslavia') echo 'selected="selected"'?> value="Yugoslavia">Yugoslavia</option>
                                            <option <?php if ($form_vals['country'] == 'Zaire') echo 'selected="selected"'?> value="Zaire">Zaire</option>
                                            <option <?php if ($form_vals['country'] == 'Zambia') echo 'selected="selected"'?> value="Zambia">Zambia</option>
                                            <option <?php if ($form_vals['country'] == 'Zimbabwe') echo 'selected="selected"'?> value="Zimbabwe">Zimbabwe</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Questions<span class="red">*</span></strong></td>
                                    <td align="left"><textarea name="questions" style="height:150px;"><?php echo $form_vals['questions'] ? $form_vals['questions'] : '' ?></textarea></td>
                                </tr>
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td align="center"><img border="0" id="captcha" src="php-include/captcha/image.php" alt="" /><br />
                                    <small>reload image: <a href="javascript:new_captcha();"> <img border="0" alt="" src="images/refresh.jpg" /></a></small></td>
                                </tr>
                                <tr>
                                    <td align="right"><strong>Verification code<span class="red">*</span></strong></td>
                                    <td align="left">
                                        <small class="lightgray">Enter text shown on image above</small>
                                        <input type="text" id="security_code" name="security_code" size="30" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td align="center"><input type="image" src="images/buttons/submit.jpg" style="width:80px; height:27px; border:none;" name="contact_submit" /></td>
                                </tr>
                            </table>
                    	</form>
                    </div><!-- end of .formswrapper-inner -->
                    
                </div><!-- end of .formswrapper -->
                
              	<div class="forms-rightpane" style="background:url(images/contact-holder.jpg) no-repeat;">
                    	
					<h2>Got something to share?</h2>
             		<p>
                    	Fill this form completely and let us know what you have in mind! We appreciate your interest in our company.
                    </p>
                    
              	</div>
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  
    
    <?php include_once("php-include/footer.php"); ?>
    
</body>
</html>
