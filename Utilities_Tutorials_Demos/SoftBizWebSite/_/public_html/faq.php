<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - FAQs</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="who-are-we">
            	<h1 align="right">Who we are and what we do</h1>
         </div>
            <div id="content-wrapper">
            	<div id="leftpane">
                	<h1 class="maintitle" id="faqs">Frequently <span class="green">Asked Questions</span></h1>
                    <p>These frequently asked questions let you know the answers to queries commonly asked by other people. If you don’t find your question here, <a href="contact-us.php" title="Contact us">let us know</a>.</p>
                    
                    <p class="hide">
                        <a href="#" onclick="Sexy.info('<p align=\'justify\'><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</strong><br /><br /><em>PDonec in sem mi, tincidunt consequat ipsum. Donec aliquam dolor vel justo fermentum consectetur. Donec pharetra, massa ut sagittis venenatis, ante lectus gravida arcu, quis egestas dolor nulla sit amet magna. Aliquam consectetur, tellus sed lacinia dignissim, quam nulla mollis urna, in tincidunt orci elit nec nulla. In nunc augue, aliquet in posuere sed, mattis in est. </em></p>');return false;">Info Test
                        </a>
                    </p>
                    
                    <div>
                    	<table width="100%" cellpadding="0" cellspacing="5">
                    	<tr>
                        	<td width="50%" valign="top">
                            	<h1 class="orange" id="question-group-1">Search Engine Optimization (SEO)</h1>
                                <ol>
                                	<li><a href="#seo1">What is SEO?</a></li>
                                    <li><a href="#seo2">What is linkbuilding?</a></li>
                                    <li><a href="#seo3">Is SEO important to my website?</a></li>
                                    <li><a href="#seo4">What is search engine?</a></li>
                                    <li><a href="#seo5">Is Pay Per Click (PPC) marketing good for e-commerce websites? </a></li>
                                    
                                   
                                </ol>
                          	</td>
                            <td valign="top">
                            	<h1 class="orange" id="question-group-2">Web Design &amp; Development</h1>
                                <ol>
                                
                                	<li><a href="#wdd1">Do I need a website for my business?</a></li>
                                	<li><a href="#wdd2">How much does a web site cost?</a></li>
                                    <li><a href="#wdd3">Do I need to hire a web designer near my area?</a></li>
                                    <li><a href="#wdd4">How long does it take to finish a website?</a></li>
                                </ol>
                          	</td>
                        </tr>
                        
                        <tr>
                        	<td valign="top">
                            	<h1 class="orange" id="question-group-3">Software Development</h1>
                                <ol>
                                	<li><a href="#sd1">What are the tools did you used for software development?</a></li>
                                    <li><a href="#sd2">Can you develop a software for keeping track of inventory? </a></li>
                                    
                              </ol>
                          	</td>
                            
                            <td valign="top">
                            	<h1 class="orange" id="question-group-4">Online Support</h1>
                                <ol>
                                	<li><a href="#os1">How will I contact CWS?</a></li>
                                    <li><a href="#os2">When can I contact Creative Web Solution?</a></li>
                                    
                                </ol>
                          	</td>
                            
                            
                        </tr>
                        <tr>
                        	<td valign="top">
                            	<h1 class="orange" id="question-group-5">Technical Terms</h1>
                                
                                <ol>
                                	<li><a href="#tt1">What is PHP?</a></li>
                                    <li><a href="#tt2">What is e-commerce?</a></li>
                                    <li><a href="#tt3">What is Page Rank (PR)?</a></li>
                              </ol>
                          	</td>
                            
                            <td valign="top">&nbsp;</td>
                            
                        </tr>
                        <tr>
                       	  <td colspan="2" valign="top">
                            	<h1 class="blue">Search Engine Optimization (SEO)</h1>
                                <ol>
                                	<li>
                                    	<p>
                                            <strong id="seo1">What is SEO?</strong><br />
                                            SEO or Search Engine Optimization is the process of making web pages visible on search engines by means of organic and inorganic listings. Organic listings are free and can be achieved through link building; inorganic listings are otherwise paid and are also known as pay-per-click or PPC.
                                        </p>
                                    </li>
                                    <li>
                                    	 <p>
                                            <strong id="seo2">What is linkbuilding?</strong><br />
                                           	Link building is a process in SEO. It simply means making links from other websites that lead to your website. There are many link building types such as article, blog posting, forum posting, blog commenting, social bookmarking, directory listing, et cetera. Link building makes use of your preferred keywords (the words that users type in search engines to lead to your page) and the URL of your page/s.
                                        </p>
                                    </li>
                                    
                                    <li>
                                    	<p>
                                            <strong id="seo3">Is SEO important to my website?</strong><br />
                                            SEO or Search Engine Optimization is the process of making web pages visible on search engines by means of organic and inorganic listings. Organic listings are free and can be achieved through link building; inorganic listings are otherwise paid and are also known as pay-per-click or PPC.
                                        </p>
                                    </li>
                                    <li>
                                    	 <p>
                                            <strong id="seo4">What is search engine?</strong><br />
                                            A web search engine is where Internet users place their queries and find results. The results may come up in web pages, images and other file types. The most popular web search engines are Google, Yahoo and Bing.
                                        </p>
                                    </li>
                                    <li>
                                    	 <p>
                                            <strong id="seo5">Is Pay Per Click (PPC) marketing good for e-commerce websites? </strong><br />
                                            PPC marketing is very important to e-commerce websites. Unlike online advertising, the cost of PPC only depends on its clicks. The ads are placed on websites with the same niche or category. The most common PPC providers are Google AdWords, Yahoo! Search Marketing and Microsoft AdCenter. 
                                            
                                            <br /><br />
                                            <a href="#faqs" title="Back to top">Back to top</a>
                                        </p>
                                    </li>
                                </ol>
                                
                               
                                
                                <h1 class="blue">Web Design &amp; Development</h1>
                               
                               	<ol>
                                	<li>
                                   	  	<p>
                                            <strong id="wdd1">Do I need a website for my business?</strong><br />
                                    		Yes, you do. Many people now make use of search engines to avail of something, so it is best for your business to be recognized in the Internet as well. 
                                        </p>
                                    </li>
                          			<li>
                                    	<p>
                                            <strong id="wdd2">How much does a web site cost?</strong><br />
                                       		The cost of building a website depends on many factors such as time frame, content, number of pages, et cetera. If you wish to have a clue on the cost, let us know and we will give you a free quote.
                                    </li>
                                    <li>
                                    	<p>
                                            <strong id="wdd3">Do I need to hire a web designer near my area?</strong><br />
                                            With the technology we use, there is no need to find and hire a web designer near your area since our online support and hotline are available.
                                        </p>
                                    </li>
                                    <li>
                                    	<p>
                                            <strong id="wdd4">How long does it take to finish a website?</strong><br />
                                            Time frame depends on the complexity of your website, along with its size and contents. We will inform you about your site’s progress from time to time.
                                       
                                        	<br /><br />
                                            <a href="#faqs" title="Back to top">Back to top</a>
                                        </p>
                                    </li>
                               	</ol>
                                
                                <h1 class="blue">Software Development</h1>
                               
                               	<ol>
                                	<li>
                                   	  	<p>
                                            <strong id="sd1">What are the tools did you used for software development?</strong><br />
                                    		Our programmers make use of the common languages such as Java, Delphi, Visual Basic .NET (C#). Our programmers depend on the usage of programming languages on the nature of the software to be developed.
                                        </p>
                                    </li>
                                    <li>
                                   	  	<p>
                                            <strong id="sd2">Can you develop a software for keeping track of inventory?</strong><br />
                                   		 	Yes we can. Inventory is one of the most common forms of software and our programmers can develop it. Kindly check our <a href="services.php">Services</a> page for more information. 
                                            
                                            <br /><br />
                                            <a href="#faqs" title="Back to top">Back to top</a>
                                        </p>
                                    </li>
                                    
								</ol>
                                
                                <h1 class="blue">Online Support</h1>
                               
                               	<ol>
                                	<li>
                                   	  	<p>
                                            <strong id="os1">How will I contact CWS?</strong><br />
                                   		We can be reached through our hotline (<strong>1-520-225-0169</strong>) or through our <a href="contact-us.php" title="Online Support">online support</a>. </p>
                                    </li>
                                    <li>
                                   	  	<p>
                                            <strong id="os2">When can I contact Creative Web Solution?</strong><br />
                                    		We are always available on business days.
                                            
                                            <br /><br />
                                            <a href="#faqs" title="Back to top">Back to top</a>
                                        </p>
                                    </li>
                                    
								</ol>
                                
                                <h1 class="blue">Technical Terms</h1>
                               
                               	<ol>
                                	<li>
                                   	  	<p>
                                            <strong id="tt1">What is PHP?</strong><br />
                                    		PHP, or Hypertext Preprocessor is a common scripting language used in creating dynamic web pages. These web pages have functions that are more than a typical HTML web page.
                                        </p>
                                    </li>
                                    <li>
                                   	  	<p>
                                            <strong id="tt2">What is e-commerce?</strong><br />
                                    		E-commerce, or electronic commerce is a buy-and-sell process that utilizes the Internet and other computer networks. The popular e-commerce websites include Amazon and eBay.
                                        </p>
                                    </li>
                                    
                                    <li>
                                   	  	<p>
                                            <strong id="tt3">What is Page Rank (PR)?</strong><br />
                                    		Page rank is the quantified importance of a web page. It is how Google measures the importance of a web page by means of ranking (1-10). 
                                            
                                            <br /><br />
                                            <a href="#faqs" title="Back to top">Back to top</a>
                                        </p>
                                    </li>
                                    
								</ol>
                                
                                <p>
                               		
                                Do not hesitate to <a href="contact-us.php" title="Contact us">contact us</a> if you have more questions in mind. We will display your questions here. </p>
                                
                               
                            </td>
                        </tr>
                    </table>
                    </div>
                   
                </div><!-- end of #leftpane-->
                <div id="rightpane">
                	
                    <?php include_once("php-include/contact-quote.php");?>
                    
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  
	
    <?php include_once("php-include/footer.php"); ?>
    
</body>
</html>
