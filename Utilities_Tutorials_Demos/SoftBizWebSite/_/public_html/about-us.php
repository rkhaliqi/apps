<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - About Company</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus aboutus-active">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="about-us">
            	<h1>Who We are and What We Do</h1>
         </div>
            <div id="content-wrapper">
            	<div id="leftpane">
                	<h1 class="maintitle">About <span class="green">Creative Web Solution!</span></h1>
                    <p>
                    	We are Creative Web Solution, a firm dedicated to information technology. We are advanced in the field of design, software and the Internet so, we are confident that we can give our clients the best output that will live up to their expectations. This confidence has a basis: our experience and expertise in the field. 
                    </p>
                    
                    <p>
                    	We are here to provide service to clients in need. In the business world, income matters the most.  So, we guarantee our clients that we can bring more income to the business through the most effective medium today, the Internet.
                    </p>
                    
                    <p>
                    	We have different teams working for our different services: web development and design, search engine optimization, internet marketing and software development. Each team has professionals whose skills and experience are superior beyond contest.
                    </p>
                    
                <h1>Services <span class="green">Offered:</span></h1>
                    
                    <table width="100%" cellpadding="0" cellspacing="5">
                    	<tr>
                        	<td width="50%" valign="top">
                            	<p>
                                	<strong class="blue">A. Web Design &amp; Development </strong>
                             	</p>
                                <ul>
                                    <li>Web Content and Writing Services</li>
                                    <li>eBay Store/Amazon Professional Services</li>
                                    <li>Search Engine Optimization</li>
                                    <li>Pay Per Click </li>
                                    <li>Internet Marketing </li>
                                    <li>Custom Website Design and Programming</li>
                                </ul>
                            </td>
                            <td width="50%" valign="top">
                            	<p>
                                	<strong class="blue">B. Software Development </strong>
                             	</p>
                                <ul>
                                	<li>Stragey and analysis</li>
                                    <li>Application Development</li>
                                    <li>System integration</li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    
                    <p>
                    	We are ready to render our services anytime. Deadlines strictly matter to us. Our <a href="online-support.php" title="Online Support">online support</a> shows how we value our clients.
                    </p>	
                    
                    <p>
                   		For detailed explanation of our services offered, click <a href="services.php" title="Services">here</a>. 
                    </p>
                    
                </div><!-- end of #leftpane-->
                <div id="rightpane">
                	
                    <div class="rightpane-header"> <h2>Explore more</h2></div><!-- end of .rightpane-header -->
                    
                     <ul class="rightnavigation">
                    	
                        <li><a href="#" title="About company" class="rightnav-active">About company</a></li>
                        <li><a href="online-support.php" title="Online Support">Online Support</a></li>
                        <li><a href="technology.php" title="Technology">Technology</a></li>
                        <li><a href="why-people-choose-us.php" title="Why People Choose Us">Why People Choose Us</a></li>
                        <li><a href="mission-vision.php" title="Mission Vision">Mission Vision</a></li>
                        <li><a href="learnmore.php" title="Learn more">Learn more</a></li>
                    </ul>
                    <br class="clear" /><!-- don't remove -->
                    
                    <?php include_once("php-include/contact-quote.php");?>
                    
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  

	<?php include_once("php-include/footer.php"); ?>

</body>
</html>
