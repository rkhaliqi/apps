<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - Our Works</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks ourworks-active">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="our-works">
            	<h1>Proof of our creativity and expertise in our services</h1>
         	</div>
            <div id="content-wrapper">
            	<div id="leftpane">
               		
                   	<table width="100%" cellpadding="0" cellspacing="5" style="border-right:1px solid #CCC;">
                   		<tr>
                        	<td colspan="2" valign="top">
                            	<p>
                                	Below is our gallery that gives you a hint of what we can do. Every website in this gallery is unique and tailor-made according to our clients' specific preferences and needs. Click the thumbnails to enlarge the images. Want a website like one of these? <a href="free-quote.php">Request a quote</a> from us now!
                                </p>
                            </td>
                        </tr>	
                        <tr>
                        	<td valign="top" align="center"><a href="images-content/our-works/detailed/samstrans.jpg" rel="lightbox[ourworks]" title="Sams Trans"><img src="images-content/our-works/thumbs/samstrans.jpg" alt="" /></a></td>
                            <td valign="middle">
                            	
                            <strong>Website name:<span class="green"> Sams Trans</span></strong><br />
                            Sam’s Trans is an online company that gives transportation services for airport trips and other events.<br />
                           
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td valign="top" align="center"><a href="images-content/our-works/detailed/batterieswholesale.jpg" rel="lightbox[ourworks]" title="Batteries Wholesale"><img src="images-content/our-works/thumbs/batterieswholesale.jpg" alt="" /></a></td>
                            <td valign="middle">
                           
                            <strong>Website name: <span class="green">Batteries Wholesale</span></strong><br />
                            Batteries Wholesale is an authority in replacement laptop batteries for Dell, Asus, Sony, Toshiba and other brands.<br />
                            
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                      		<td valign="top" align="center"><a href="images-content/our-works/detailed/tyroneprieto.jpg" rel="lightbox[ourworks]" title="Tyrone Prieto"><img src="images-content/our-works/thumbs/tyroneprieto.jpg" alt="" /></a></td>
                            <td valign="middle">
                          
                                <strong>Website name:<span class="green"> Tyrone Prieto</span></strong><br />
                            	Tyrone Prieto offers professional web design and development for different businesses using various platforms.<br />
                                
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2">&nbsp;</td>
                        </tr>
                        
                        <tr>
                      		<td valign="top" align="center"><a href="images-content/our-works/detailed/mybusinessdesk.jpg" rel="lightbox[ourworks]" title="myBusiness Desk"><img src="images-content/our-works/thumbs/mybusinessdesk.jpg" alt="" /></a></td>
                            <td valign="middle">
                          
                                <strong>Website name:<span class="green"> myBusiness Desk</span></strong><br />
                            	myBusinessDesk is a fully automated control panel designed for you to promote or sell your products/services to resellers, affiliates and end-users.  <br />
                              
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2">&nbsp;</td>
                        </tr>
                        
                        <tr>
                      		<td valign="top" align="center"><a href="images-content/our-works/detailed/cheaperpeepers.jpg" rel="lightbox[ourworks]" title="Cheaper Peepers"><img src="images-content/our-works/thumbs/cheaperpeepers.jpg" alt="" /></a></td>
                            <td valign="middle">
                          
                                <strong>Website name:<span class="green"> Cheaper Peepers</span></strong><br />
                                Cheaper Peepers is New York’s source for discount eyeglasses. The site offers low prices on eyewear and also gives a buy one take one offer.<br />
                               
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2">&nbsp;</td>
                        </tr>
                        
                        <tr>
                      		<td valign="top" align="center"><a href="images-content/our-works/detailed/blazintech-sublink.jpg" rel="lightbox[ourworks]" title="Blazintech Computer Services"><img src="images-content/our-works/thumbs/blazintech-sublink.jpg" alt="" /></a></td>
                            <td valign="middle">
                          
                                <strong>Website name:<span class="green"> Blazintech Computer Services</span></strong><br />
                                Blazintech Computer Services provide insured on-site and remote access computer repair services including professional computer technician for virus removal, software &amp; hardware problems.<br />
                               
                          </td>
                        </tr>
                        <tr>
                        	<td colspan="2">&nbsp;</td>
                        </tr>
                        
                       
                   </table>
                   
                   
              		<div class="pagination-wrapper">
                    	
                        <ul class="pagination">
                            <li>Page 1 of 5:</li>
                            <li><a href="#" title="Previous">&laquo;</a></li>
                            <li><a href="#" class="page-active">1</a></li>
                            <li><a href="#" title="2">2</a></li>
                            <li><a href="#" title="3">3</a></li>
                            <li><a href="#" title="Next">&raquo;</a></li>
                            <li><a href="#" title="Go to last page">last &raquo;</a></li>
                        </ul> 
                    </div><!-- end of .pagination-wrapper -->
                   
          		</div><!-- end of #leftpane-->
                <div id="rightpane">
                	
                    <div class="featured-prof-wrapper">
                    	<div class="featured-text">
                        	<p>Creative Web Solution really solved my problems. I badly needed to have my website finished but sadly time was not enough. Other companies would offer me almost twice the price of their package if they rush the project. Creative Web Solution gladly accepted the project and assured me that the deadline will be met – without putting a hefty price! Now my site is up and running like it should.</p>
                        	<p>If ever I will have another project, I will definitely choose Creative Web Solution again – rush or not!</p>	
                    	</div><!-- end of .featured-text-->
                        
                        <div class="featured-footer">
                        
                        	<img src="images/male-icon.jpg" alt="Lorem Ipsum" class="FL" />
                            <strong class="green">Ethan Henry</strong><br />
                            <em>Online Marketer</em>
                            
                        </div><!-- end of .featured-footer -->
                        
                    </div><!-- end of .featured-prof-wrapper -->
                    
                    
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  

	<?php include_once("php-include/footer.php"); ?>

</body>
</html>
