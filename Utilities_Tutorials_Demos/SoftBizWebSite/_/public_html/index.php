<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body>
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
                <ul>
                	<li><a href="." title="Home" class="home home-active">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
                
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
        <div id="featured-wrapper">
        	<div class="intro-text">
            	<p>
            		<img src="images/we-design-code.jpg" alt="We Design and Code beutiful websites." />
            	</p>
                <p>
                	Does your business need to have global exposure? Does it need a website designed and developed by webmaster professionals?
            	</p>
                <p>
                	At Creative Web Solution, we can make your business grow by building its corporate website – from developing, designing and up to internet marketing. We have a team of skilled and professional developers that will assure your website's top spot in the most popular search engines of today. <a href="learnmore.php" title="Learn more">+Learn more</a>
              </p>
                
               
                
              <ul class="intro-buttons">
           	    <li><a href="free-quote.php" title="Request a Quote"><img src="images/buttons/request-a-quote.jpg" alt="Request a Quote" /></a></li>
                    <li><a href="our-works.php" title="View our Works"><img src="images/buttons/view-our-works.jpg" alt="View our Works" /></a></li>
                </ul>
                
            </div><!-- end of .intro-text -->
            
            
           
            
            <div id="gallery-wrapper">
            	
                
                <div id="slider">
                	
                    <img src="images/featured-portfolio.jpg" alt="" class="hide" />
                    <img src="images/featured-portfolio2.jpg" alt="" class="hide" />
                    <img src="images/featured-portfolio3.jpg" alt="" class="hide"/>
                    <img src="images/featured-portfolio4.jpg" alt="" class="hide" />
                	<img src="images/featured-portfolio5.jpg" alt="" class="hide"/>
                
                    <img src="images/featured-portfolio.jpg" alt="" />
                    <img src="images/featured-portfolio2.jpg" alt="" />
                    <img src="images/featured-portfolio3.jpg" alt=""/>
                    <img src="images/featured-portfolio4.jpg" alt="" />
                	<img src="images/featured-portfolio5.jpg" alt=""/>
                </div>
               
            </div><!-- end of #gallery-wrapper-->
            
        </div><!-- end of #featured-wrapper -->
        
       
    </div><!-- end of #mainwrapper -->
    
    <br class="clear" /><!-- don'r remove -->
    
    
    <div id="services-wrapper">
    	<div id="services-inner">
        	<ul id="services-menu">
            	<li><a href="services.php#webdesign" class="custom-webdesign" title="Custom Webdesign">Custom Webdesign</a></li>
                <li><a href="services.php#webdesign-5" class="internet-marketing" title="Internet Marketing">Internet Marketing</a></li>
                <li><a href="services.php#webdesign-3" class="seo-ppc" title="SEO/PPC Marketing">SEO/PPC Marketing</a></li>
                <li><a href="services.php#software" class="software-development" title="Software Development">Software Development</a></li>
            </ul>
           <div class="index-content-body">
           
           		<div class="first-column">
                	<h1>Why people choose us</h1>
                    <p align="justify"> <a href="why-people-choose-us.php" class="FL"><img src="images-content/why-people.jpg" alt="" /></a>
   			      We at Creative Web Solution provide exceptional designs to meet our clients’ needs. Our designs and software are created by our team of professionals who have an eye for detail. We assure our clients that they get satisfactory results from our design, code, advertising and marketing, and software development. We take care of our clients by providing superior client support by means of our friendly customer service available on business days. <strong><a href="why-people-choose-us.php" title="read more">+read more</a></strong></p>
                    
                    <h1>Technology that we use</h1>
                    <p align="justify"> <a href="technology.php" class="FL"><img src="images-content/technology.jpg" alt="" /></a>
                   	To meet the clients’ needs, our teams use nothing but the state of the art tools and technology to assure not only the quality, but also the satisfaction of our clients as well. Our web developers use Web 2.0 and other platforms for dynamic output. Our SEO specialists strategically create organic and inorganic listings (PPC) to broaden the scope of each campaign, and finally our software developers make use of MVC programming and other techniques for the superior quality of their output. <strong><a href="technology.php" title="read more">+read more</a></strong> </p>
                    
                </div><!-- end of .first-column -->
                <div class="second-column">
                	<h1>Frequently Asked Questions</h1>
                    <p align="justify"> <a href="faq.php" class="FL"><img src="images-content/faq.jpg" alt=""/></a>
                    	Our Frequently Asked Questions page lets you learn more about our services through previously asked queries by other people. The FAQ page is a must for every business website since concise questions and answers will help potential clients learn more about the company. Feel free to browse through this page. <strong><a href="faq.php" title="read more">+read more</a></strong> 
                	</p>
                    
                    <h1>Online Support</h1>
                    <p align="justify"> <a href="online-support.php" class="FL"><img src="images-content/online-support.jpg" alt="" /></a>
                    	We provide online support for inquiries and customer assistance. Our team of friendly customer service representative explains every detail and makes sure that the information to and from the clients is relayed to the respective team of developers, designers, programmers and marketers. Our online support is available all throughout each day of the week. <strong><a href="online-support.php" title="read more">+read more</a></strong> 
                    </p>
                    
                </div><!-- end of .second-column -->
                
           </div><!-- end of .ervices-body -->
            
        </div><!-- end of #services-inner -->
        
    </div><!-- end of #services-wrapper -->
    
    <br class="clear" /><!-- don't remove-->
	
	<?php include_once("php-include/footer.php"); ?>
  
</body>
</html>
