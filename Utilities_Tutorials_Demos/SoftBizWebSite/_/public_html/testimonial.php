<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - Testimonial</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="testimonial">
            	<h1 align="right">What our clients say about us</h1>
         	</div>
            <div id="content-wrapper">
            	<div id="leftpane">
                	
                 	<h1 class="maintitle">We have 4 testimonials already!</h1>   
                   	
                    <div class="testi-text-wrapper">
                        <p>
                        	<img src="images/quotation-mark.jpg" class="FL" />I’m very pleased with how my website was remodeled, and I’m happy with the 100% business increase I’ve gotten through the design and marketing Creative Web Solution did. We’ve also gotten a nice increase to our eBay business. I’d recommend them to anyone who has an online business.</p>
                        <p>
                        	<strong class="green">Mr. Adil Khan, Arizona, USA</strong><br />
                      </p>
                        
               	  </div><!-- end of .testi-text-wrapper -->
                   
                    <div class="testi-text-wrapper">
                        
                    <p>
                            <img src="images/quotation-mark.jpg" class="FL" />
                            Creative Web did a great job on my website. It’s simple yet elegant and it has every customer needs. I’m pleased with the interface since it lets us browse around and find what we need easily. I’m really satisfied with their service especially when I needed to have some modifications in my page. I recommend Creative Web to those who want to have a great website. </p>
                       <p>
                           <strong class="green">Mr. Ethan Henry, Louisiana, USA</strong><br />
                       </p>
                        
                    </div><!-- end of .testi-text-wrapper -->
                    
                    <div class="testi-text-wrapper">
                        
                        <p>
                            <img src="images/quotation-mark.jpg" class="FL" />
                            Creative Web Solution did magic on my website. In a short time I saw my web site on Google and I’m delighted! According to my site statistics my traffic went up which means more people are visiting my website.    
                        </p>
                   	  <p>
                            <strong class="green">Mr. John Winner, Illinois, USA</strong><br />
                       </p>
                        
                    </div><!-- end of .testi-text-wrapper -->
                    
                    <div class="testi-text-wrapper">
                        
                        <p>
                            <img src="images/quotation-mark.jpg" class="FL" />Creative Web Solution never left me all  throughout the development of my software. They use all sorts of tools to make  sure the output is precise. They also made me understand how easy the interface  of the software is. I’m happy with the output of the project and I am looking  forward for my next one.</p>
                     	<p>
                            <strong class="green">Mr. David Markerson, California, USA</strong><br />
                        
                      </p>
                        
                  </div><!-- end of .testi-text-wrapper -->
                    
                </div><!-- end of #leftpane-->
                <div id="rightpane">
                	
                    <?php include_once("php-include/contact-quote.php");?>
                    
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  
   
   	<?php include_once("php-include/footer.php"); ?>
    
</body>
</html>
