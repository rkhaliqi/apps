<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Creative Web Solution - About Company</title>

<?php include_once("php-include/styles-js.php");?>

</head>

<body class="body-subpage">
	<div id="mainwrapper">
    	<div id="topwrapper">
        	
            <a href="." title="Creative Web Solution" class="FL"><img src="images/logo.jpg" alt="" /></a>
            
            <?php include_once("php-include/languages.php");?>
            
            <div id="mainmenu">
            	
            	<ul>
                	<li><a href="." title="Home" class="home">Home</a></li>
                    <li><a href="our-works.php" title="Our Works" class="ourworks">Our Works</a></li>
                    <li><a href="services.php" title="Services" class="services">Services</a></li>
                    <li><a href="free-quote.php" title="Free quote" class="freequote">Free quote</a></li>
                    <li><a href="about-us.php" title="About us" class="aboutus aboutus-active">About us</a></li>
                    <li><a href="blog.php" title="Blog" class="blog">Blog</a></li>
                    <li><a href="contact-us.php" title="Contact us" class="contactus">Contact us</a></li>
                </ul>
            </div><!-- end of #topwrapper -->
            
        </div><!-- end of #topwrapper -->
        
       <div id="subpage-content">
       		<div id="headers" class="about-us">
            	<h1>Who we are and what we do</h1>
         </div>
            <div id="content-wrapper">
            	<div id="leftpane">
                	<h1 class="maintitle">Techology <span class="green">that we use</span></h1>
                    
                    <p>We are equipped with state-of-the-art tools to meet the needs of our clients. We do not just manually work on every project; we also use technology as well.</p>
                    <p>We  do not solely rely on the expertise of our teams. We use updated tools to make  each project perfect and error-free.</p>
                  <p>Technology  such as Web 2.0, SEO and different tools for internet marketing are essential  for its users to make every job efficient, desirable and most importantly,  successful. We also assure every client that the tools we use are upgraded  day-to-day. That is one unwritten rule in information technology.</p>
<p>Our web  designers make use of different software for both design and coding. We use  codes beyond HTML and Javascript. They use modern languages for database, slideshows  and different functions a modern website should have. Moreover, the websites we  build are aesthetically pleasing, catchy and these websites have an interface  most suitable for the guests they target.</p>
<p>Our  SEO’s and internet marketers are equipped with tools for checking the search  engine results page (SERP), for indexing a webpage, and for creating many  backlinks during the project duration. We also have a wide variety of niches,  from article submission, to social bookmarking, to social media, et cetera. We  produce remarkable results and maintain them as well.</p>
<p>Our  software developers make use of programming languages for database and other  functions and debugger tools for testing the output. We also utilize project  management tools, which keep the entire project neat and organized from  planning to maintenance.</p>
<p>We have  already satisfied our clients with our outputs. We owe that to the technology  we use that allows us to simply summarize the superior output we give to each  client.</p>
</div><!-- end of #leftpane-->
                <div id="rightpane">
                	
                    <div class="rightpane-header"> <h2>Explore more</h2></div><!-- end of .rightpane-header -->
                    
                    <ul class="rightnavigation">
                    	
                        <li><a href="about-us.php" title="About company">About company</a></li>
                        <li><a href="online-support.php" title="Online Support">Online Support</a></li>
                        <li><a href="technology.php" title="Technology" class="rightnav-active">Technology</a></li>
                        <li><a href="why-people-choose-us.php" title="Why People Choose Us">Why People Choose Us</a></li>
                        <li><a href="mission-vision.php" title="Mission Vision">Mission Vision</a></li>
                        <li><a href="learnmore.php" title="Learn more">Learn more</a></li>
                    </ul>
                    <br class="clear" /><!-- don't remove -->
                    
                    <?php include_once("php-include/contact-quote.php");?>
                    
                </div><!-- end of #rightpane-->
                
                <br class="clear" /><!-- don't remove-->
                
            </div><!--end of #content-wrapper -->
            
       </div><!-- end of #subpage-content-->
	</div>  

	<?php include_once("php-include/footer.php"); ?>

</body>
</html>
