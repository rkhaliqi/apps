.class Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;
.super Landroid/os/AsyncTask;
.source "CreateFoodActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateFoodOnServerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1378
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1378
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;->doInBackground([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .parameter "params"

    .prologue
    .line 1382
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v3

    invoke-virtual {v3}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodWithServingsToCreateOrEdit()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    move-result-object v0

    .line 1384
    .local v0, foodsWithServingsArray:Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/medhelp/mydiet/http/MHHttpHandler;->createOrEditFood(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1386
    .local v1, response:Ljava/lang/String;
    invoke-static {v1}, Lorg/medhelp/mydiet/util/SyncUtil;->isResponseValid(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1387
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lorg/medhelp/mydiet/util/SyncUtil;->handleSyncNewFoodsResponse(Landroid/content/Context;Ljava/lang/String;)V

    .line 1390
    :cond_0
    const/4 v2, 0x0

    return-object v2
.end method
