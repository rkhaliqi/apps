.class Lorg/medhelp/mydiet/activity/SetupScreen4$2;
.super Ljava/lang/Object;
.source "SetupScreen4.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/SetupScreen4;->clickActivityLevel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

.field private final synthetic val$activityLevels:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/SetupScreen4;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$2;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$2;->val$activityLevels:[Ljava/lang/String;

    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 285
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$2;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$2;->val$activityLevels:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setActivityLevel(Landroid/content/Context;Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$2;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    #calls: Lorg/medhelp/mydiet/activity/SetupScreen4;->refreshViewContents()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->access$5(Lorg/medhelp/mydiet/activity/SetupScreen4;)V

    .line 287
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$2;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 288
    return-void
.end method
