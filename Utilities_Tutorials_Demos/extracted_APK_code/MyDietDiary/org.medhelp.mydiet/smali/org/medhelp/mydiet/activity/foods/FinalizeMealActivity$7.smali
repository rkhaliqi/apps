.class Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$7;
.super Ljava/lang/Object;
.source "FinalizeMealActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->refreshFoodItems()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$7;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 508
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$7;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    const-class v3, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 510
    .local v1, fooddetailsIntent:Landroid/content/Intent;
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$7;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v2

    iget-object v3, v2, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 511
    .local v0, foodKey:Ljava/lang/String;
    const-string v2, "transient_foods"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 512
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$7;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    const/16 v3, 0x3eb

    invoke-virtual {v2, v1, v3}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 513
    return-void
.end method
