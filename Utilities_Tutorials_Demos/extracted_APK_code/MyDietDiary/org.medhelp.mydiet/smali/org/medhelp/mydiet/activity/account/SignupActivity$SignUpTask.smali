.class Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;
.super Landroid/os/AsyncTask;
.source "SignupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/account/SignupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SignUpTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final SIGNUP_SUCCESS:I = 0x1


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/account/SignupActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 124
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/account/SignupActivity;Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;-><init>(Lorg/medhelp/mydiet/activity/account/SignupActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs declared-synchronized doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .parameter "params"

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mhHttpHandler:Lorg/medhelp/mydiet/http/MHHttpHandler;

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    new-instance v1, Lorg/medhelp/mydiet/http/MHHttpHandler;

    invoke-direct {v1}, Lorg/medhelp/mydiet/http/MHHttpHandler;-><init>()V

    iput-object v1, v0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mhHttpHandler:Lorg/medhelp/mydiet/http/MHHttpHandler;

    .line 136
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mhHttpHandler:Lorg/medhelp/mydiet/http/MHHttpHandler;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    const/4 v2, 0x1

    aget-object v2, p1, v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    const/4 v4, 0x3

    aget-object v4, p1, v4

    const/4 v5, 0x4

    aget-object v5, p1, v5

    invoke-virtual/range {v0 .. v5}, Lorg/medhelp/mydiet/http/MHHttpHandler;->signUp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 137
    .local v6, response:Ljava/lang/String;
    monitor-exit p0

    return-object v6

    .line 134
    .end local v6           #response:Ljava/lang/String;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 11
    .parameter "result"

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 151
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v7, v7, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 153
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    const v8, 0x7f0b0107

    invoke-virtual {v7, v8}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 155
    .local v5, tv:Landroid/widget/TextView;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 156
    :cond_0
    const-string v7, "#FF0000"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 157
    const-string v7, "Login failed. Please check your network connection and try again"

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    :goto_0
    return-void

    .line 160
    :cond_1
    const/4 v3, 0x0

    .line 163
    .local v3, resultObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    .end local v3           #resultObject:Lorg/json/JSONObject;
    .local v4, resultObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v7, "response_code"

    const/4 v8, -0x1

    invoke-virtual {v4, v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 166
    .local v2, responseCode:I
    const/4 v6, 0x0

    .line 168
    .local v6, userId:Ljava/lang/String;
    if-nez v2, :cond_2

    .line 171
    const-string v7, "#91cb56"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 172
    const-string v7, ""

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    const-string v7, "data"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/json/JSONObject;

    const-string v8, "user_id"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 176
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v8, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v8, v8, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mNicknameField:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserName(Landroid/content/Context;Ljava/lang/String;)V

    .line 177
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v8, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v8, v8, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mPasswordField:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setPassword(Landroid/content/Context;Ljava/lang/String;)V

    .line 178
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    invoke-static {v7, v6}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserId(Landroid/content/Context;Ljava/lang/String;)V

    .line 179
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    const-string v8, "medhelp"

    invoke-static {v7, v8}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserType(Landroid/content/Context;Ljava/lang/String;)V

    .line 181
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Integer;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v4

    .line 211
    .end local v2           #responseCode:I
    .end local v4           #resultObject:Lorg/json/JSONObject;
    .end local v6           #userId:Ljava/lang/String;
    .restart local v3       #resultObject:Lorg/json/JSONObject;
    :goto_1
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 182
    .end local v3           #resultObject:Lorg/json/JSONObject;
    .restart local v2       #responseCode:I
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    .restart local v6       #userId:Ljava/lang/String;
    :cond_2
    if-ne v2, v9, :cond_3

    .line 185
    :try_start_2
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    const-string v8, ""

    invoke-static {v7, v8}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserName(Landroid/content/Context;Ljava/lang/String;)V

    .line 186
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    const-string v8, ""

    invoke-static {v7, v8}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setPassword(Landroid/content/Context;Ljava/lang/String;)V

    .line 187
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    const-string v8, ""

    invoke-static {v7, v8}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserId(Landroid/content/Context;Ljava/lang/String;)V

    .line 189
    const-string v1, "Signup Failed : \nPlease Enter valid information"

    .line 191
    .local v1, errorText:Ljava/lang/String;
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v7, v7, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mCaptchaSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v7}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 194
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    #calls: Lorg/medhelp/mydiet/activity/account/SignupActivity;->refreshCaptcha()V
    invoke-static {v7}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->access$1(Lorg/medhelp/mydiet/activity/account/SignupActivity;)V

    move-object v3, v4

    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v3       #resultObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 196
    .end local v1           #errorText:Ljava/lang/String;
    .end local v3           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    :cond_3
    const/16 v7, -0xa

    if-ne v2, v7, :cond_4

    .line 197
    const-string v7, "Login failed. MedHelp server is down for maintainance. Please try logging in later. You may still use the application"

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v3, v4

    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v3       #resultObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 198
    .end local v3           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    :cond_4
    const/16 v7, 0x385

    if-eq v2, v7, :cond_5

    const/16 v7, 0x386

    if-ne v2, v7, :cond_6

    .line 200
    :cond_5
    const-string v7, "Login failed. Cannot connect to MedHelp Server. Please check your network connection or try later. You may still use the application"

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v3, v4

    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v3       #resultObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 203
    .end local v3           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    :cond_6
    const-string v7, "Login failed."

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v3, v4

    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v3       #resultObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 206
    .end local v2           #responseCode:I
    .end local v6           #userId:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 207
    .local v0, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 208
    const-string v7, "#FF0000"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 209
    const-string v7, "Login failed : invalid response"

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 206
    .end local v0           #e:Lorg/json/JSONException;
    .end local v3           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v3       #resultObject:Lorg/json/JSONObject;
    goto :goto_2
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 130
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 142
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 143
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 148
    :goto_0
    return-void

    .line 145
    :pswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    #calls: Lorg/medhelp/mydiet/activity/account/SignupActivity;->completeSignup()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->access$0(Lorg/medhelp/mydiet/activity/account/SignupActivity;)V

    goto :goto_0

    .line 143
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
