.class public interface abstract Lorg/medhelp/mydiet/C$units;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "units"
.end annotation


# static fields
.field public static final CM_TO_IN:D = 0.393700787

.field public static final FLOZ_TO_GLASSES:D = 0.125

.field public static final FLOZ_TO_ML:D = 29.5735295625

.field public static final GLASS_TO_FLOZ:D = 8.0

.field public static final GLASS_TO_ML:D = 236.5882365

.field public static final IN_TO_CM:D = 2.54

.field public static final KG_TO_LB:D = 2.20462262

.field public static final KM_TO_MI:D = 0.621371192

.field public static final LB_TO_KG:D = 0.45359237

.field public static final MAX_WEIGHT:D = 1500.0

.field public static final MI_TO_KM:D = 1.609344

.field public static final ML_TO_FLOZ:D = 0.033814022701843

.field public static final ML_TO_GLASSES:D = 0.004226752837730375

.field public static final OZ_TO_LB:D = 0.0625
