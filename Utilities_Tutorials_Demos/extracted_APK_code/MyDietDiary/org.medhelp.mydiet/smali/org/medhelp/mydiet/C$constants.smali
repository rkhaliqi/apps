.class public interface abstract Lorg/medhelp/mydiet/C$constants;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "constants"
.end annotation


# static fields
.field public static final ACTIVITY_MUL_EXTREMELY:D = 1.9

.field public static final ACTIVITY_MUL_LIGHTLY:D = 1.375

.field public static final ACTIVITY_MUL_MODERATELY:D = 1.55

.field public static final ACTIVITY_MUL_SEDENTARY:D = 1.2

.field public static final ACTIVITY_MUL_VERY:D = 1.725

.field public static final AL_EXTREMELY:Ljava/lang/String; = "Extremely Active"

.field public static final AL_LIGHT:Ljava/lang/String; = "Lightly Active"

.field public static final AL_MODERATE:Ljava/lang/String; = "Moderately Active"

.field public static final AL_SEDENTARY:Ljava/lang/String; = "Sedentary"

.field public static final AL_VERY:Ljava/lang/String; = "Very Active"

.field public static final GENDER_FEMALE:I = 0x0

.field public static final GENDER_MALE:I = 0x1

.field public static final HTTP_ACCEPT:Ljava/lang/String; = "Accept"

.field public static final HTTP_CONTENT_JSON:Ljava/lang/String; = "application/json"

.field public static final HTTP_CONTENT_TYPE:Ljava/lang/String; = "Content-Type"
