.class Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;
.super Ljava/lang/Object;
.source "EditExerciseActivity.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 5
    .parameter "view"
    .parameter "hourOfDay"
    .parameter "minute"

    .prologue
    .line 235
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 237
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v1, v1, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    .line 238
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v1, v1, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v2, v2, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    iput-wide v2, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    .line 240
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 241
    .local v0, c:Ljava/util/Calendar;
    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v2, v2, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 242
    const/16 v1, 0xb

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 243
    const/16 v1, 0xc

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 245
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v1, v1, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    invoke-static {v0}, Lorg/medhelp/mydiet/util/DateUtil;->calendarToDate(Ljava/util/Calendar;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    .line 246
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setExerciseTimeText()V
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$0(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    monitor-exit p0

    return-void

    .line 235
    .end local v0           #c:Ljava/util/Calendar;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
