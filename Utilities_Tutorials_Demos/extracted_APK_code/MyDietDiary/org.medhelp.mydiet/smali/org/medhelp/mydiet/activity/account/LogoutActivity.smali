.class public Lorg/medhelp/mydiet/activity/account/LogoutActivity;
.super Landroid/app/Activity;
.source "LogoutActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private clickLogout()V
    .locals 2

    .prologue
    .line 40
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->logout(Landroid/content/Context;)V

    .line 41
    const-string v0, "You are logged out"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 42
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/account/LogoutActivity;->finish()V

    .line 43
    return-void
.end method

.method private setMessage()V
    .locals 4

    .prologue
    .line 46
    const v2, 0x7f0b001e

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/account/LogoutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 47
    .local v0, tv:Landroid/widget/TextView;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, username:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "You are logged in as "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 32
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 37
    :goto_0
    return-void

    .line 34
    :pswitch_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/LogoutActivity;->clickLogout()V

    goto :goto_0

    .line 32
    :pswitch_data_0
    .packed-switch 0x7f0b00e3
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v0, 0x7f030025

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/LogoutActivity;->setContentView(I)V

    .line 26
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/LogoutActivity;->setMessage()V

    .line 27
    const v0, 0x7f0b00e3

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/LogoutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    return-void
.end method
