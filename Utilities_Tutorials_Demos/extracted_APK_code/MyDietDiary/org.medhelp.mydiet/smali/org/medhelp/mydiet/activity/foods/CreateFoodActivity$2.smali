.class Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$2;
.super Ljava/lang/Object;
.source "CreateFoodActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showServingNameEntryDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

.field private final synthetic val$servingNameEditor:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$2;->val$servingNameEditor:Landroid/widget/EditText;

    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 215
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$2;->val$servingNameEditor:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, caloriesString:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 217
    const-string v0, ""

    .line 219
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->addServingName(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$8(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Ljava/lang/String;)V

    .line 220
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$7(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 221
    return-void
.end method
