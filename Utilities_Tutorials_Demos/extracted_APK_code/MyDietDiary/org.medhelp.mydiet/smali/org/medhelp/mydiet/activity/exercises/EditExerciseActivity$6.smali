.class Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;
.super Ljava/lang/Object;
.source "EditExerciseActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->onClickDistance()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

.field private final synthetic val$editor:Landroid/widget/EditText;

.field private final synthetic val$units:Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;->val$editor:Landroid/widget/EditText;

    iput-object p3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;->val$units:Ljava/lang/String;

    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 387
    const/4 v0, 0x0

    .line 388
    .local v0, distance:F
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;->val$editor:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 389
    .local v1, distanceString:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_0

    .line 390
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 392
    :cond_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;->val$units:Ljava/lang/String;

    const-string v3, "km"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 393
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    float-to-double v3, v0

    const-wide v5, 0x3fe3e245d68a2112L

    mul-double/2addr v3, v5

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setDistance(D)V
    invoke-static {v2, v3, v4}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$13(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;D)V

    .line 397
    :goto_0
    return-void

    .line 395
    :cond_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    float-to-double v3, v0

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setDistance(D)V
    invoke-static {v2, v3, v4}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$13(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;D)V

    goto :goto_0
.end method
