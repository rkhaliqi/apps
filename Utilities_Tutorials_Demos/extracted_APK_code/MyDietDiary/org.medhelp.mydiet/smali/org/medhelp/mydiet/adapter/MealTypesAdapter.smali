.class public Lorg/medhelp/mydiet/adapter/MealTypesAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MealTypesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mMealTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p2, mealTypes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const v0, 0x7f030021

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 23
    iput-object p2, p0, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->mMealTypes:Ljava/util/List;

    .line 24
    iput-object p1, p0, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->mContext:Landroid/content/Context;

    .line 25
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 26
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->mMealTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/String;
    .locals 1
    .parameter "position"

    .prologue
    .line 34
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->mMealTypes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 38
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 44
    if-nez p2, :cond_0

    .line 45
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030021

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 47
    :cond_0
    const v2, 0x7f0b00d5

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 49
    .local v0, img:Landroid/widget/ImageView;
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Breakfast"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 50
    const v2, 0x7f02003e

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 59
    :cond_1
    :goto_0
    const v2, 0x7f0b000c

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 60
    .local v1, tv:Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    return-object p2

    .line 51
    .end local v1           #tv:Landroid/widget/TextView;
    :cond_2
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Lunch"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 52
    const v2, 0x7f020040

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 53
    :cond_3
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Dinner"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 54
    const v2, 0x7f02003f

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 55
    :cond_4
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Snack"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    const v2, 0x7f020041

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
