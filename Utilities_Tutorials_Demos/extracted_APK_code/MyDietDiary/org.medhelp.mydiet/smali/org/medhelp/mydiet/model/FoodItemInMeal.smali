.class public Lorg/medhelp/mydiet/model/FoodItemInMeal;
.super Ljava/lang/Object;
.source "FoodItemInMeal.java"


# instance fields
.field public amount:D

.field public caloriesInFood:D

.field public description:Ljava/lang/String;

.field public foodJSONString:Ljava/lang/String;

.field public foodSource:Ljava/lang/String;

.field public id:J

.field public inAppId:J

.field public isSelected:Z

.field public medhelpId:J

.field public name:Ljava/lang/String;

.field public servingId:J

.field public summary:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 26
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 37
    return-void
.end method

.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;JDDZ)V
    .locals 2
    .parameter "id"
    .parameter "medhelpId"
    .parameter "name"
    .parameter "summary"
    .parameter "servingId"
    .parameter "amount"
    .parameter "caloriesInFood"
    .parameter "isSelected"

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 26
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 57
    iput-wide p1, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 58
    iput-wide p3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    .line 59
    iput-object p5, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    .line 60
    iput-object p6, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 61
    iput-wide p7, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 62
    iput-wide p9, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 63
    iput-wide p11, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 64
    iput-boolean p13, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    .line 65
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JDDZ)V
    .locals 2
    .parameter "id"
    .parameter "name"
    .parameter "summary"
    .parameter "servingId"
    .parameter "amount"
    .parameter "caloriesInFood"
    .parameter "isSelected"

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 26
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 43
    iput-wide p1, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 44
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    .line 45
    iput-object p3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 47
    iput-wide p5, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 48
    iput-wide p7, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 49
    iput-wide p9, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 50
    iput-boolean p11, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    .line 51
    return-void
.end method


# virtual methods
.method public copyFoodItemTo(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V
    .locals 2
    .parameter "copyTo"

    .prologue
    .line 92
    if-nez p0, :cond_0

    .line 93
    const/4 p1, 0x0

    .line 111
    :goto_0
    return-void

    .line 97
    :cond_0
    if-nez p1, :cond_1

    .line 98
    new-instance p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .end local p1
    invoke-direct {p1}, Lorg/medhelp/mydiet/model/FoodItemInMeal;-><init>()V

    .line 101
    .restart local p1
    :cond_1
    iget-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    iput-wide v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 102
    iget-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    iput-wide v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    .line 103
    iget-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    iput-wide v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    .line 104
    iget-object v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    iput-object v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    .line 105
    iget-object v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    iput-object v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    .line 106
    iget-object v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    iput-object v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 107
    iget-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    iput-wide v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 108
    iget-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iput-wide v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 109
    iget-wide v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    iput-wide v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 110
    iget-boolean v0, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    iput-boolean v0, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 68
    if-nez p0, :cond_0

    const/4 v2, 0x0

    .line 87
    :goto_0
    return-object v2

    .line 70
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 73
    .local v1, foodObject:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "food_id"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 74
    const-string v2, "in_app_id"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 75
    const-string v2, "medhelp_id"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 76
    const-string v2, "food_name"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 77
    const-string v2, "food_description"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 78
    const-string v2, "food_summary"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 79
    const-string v2, "serving_id"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 80
    const-string v2, "amount"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 81
    const-string v2, "food_calories"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 82
    const-string v2, "is_selected"

    iget-boolean v3, p0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
