.class Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;
.super Landroid/os/AsyncTask;
.source "EditExerciseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadExerciseTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 722
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 722
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 4
    .parameter "params"

    .prologue
    .line 735
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 736
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v2, v2, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseMHKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getExercise(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Exercise;

    move-result-object v2

    iput-object v2, v1, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    .line 738
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #getter for: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDate:Ljava/util/Date;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$1(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getRecentWeightOnOrBefore(Ljava/util/Date;)D

    move-result-wide v2

    #setter for: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mWeight:D
    invoke-static {v1, v2, v3}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$2(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;D)V

    .line 739
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 758
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->publishProgress([Ljava/lang/Object;)V

    .line 759
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 760
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 729
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 730
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->publishProgress([Ljava/lang/Object;)V

    .line 731
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 744
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 745
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 754
    :goto_0
    return-void

    .line 747
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->showProgressDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$3(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    goto :goto_0

    .line 750
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->hideProgressDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$4(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    .line 751
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->refreshContentViews()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$5(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    goto :goto_0

    .line 745
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
