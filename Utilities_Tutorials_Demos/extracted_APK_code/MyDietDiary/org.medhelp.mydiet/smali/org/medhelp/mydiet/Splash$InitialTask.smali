.class Lorg/medhelp/mydiet/Splash$InitialTask;
.super Landroid/os/AsyncTask;
.source "Splash.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/Splash;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitialTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final USAGE_TRACKER_URL:Ljava/lang/String; = "http://www.medhelp.org/analytics/create"


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/Splash;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/Splash;)V
    .locals 0
    .parameter

    .prologue
    .line 156
    iput-object p1, p0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/Splash;Lorg/medhelp/mydiet/Splash$InitialTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 156
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/Splash$InitialTask;-><init>(Lorg/medhelp/mydiet/Splash;)V

    return-void
.end method

.method private isConnectedToNetwork()Z
    .locals 4

    .prologue
    .line 269
    iget-object v2, p0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/Splash;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 270
    .local v1, connectivityManager:Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 271
    .local v0, activeNetworkInfo:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isNetworkRoaming()Z
    .locals 4

    .prologue
    .line 275
    iget-object v2, p0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/Splash;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 276
    .local v1, connectivityManager:Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 277
    .local v0, activeNetworkInfo:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private sendUsageData()V
    .locals 28

    .prologue
    .line 179
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    move-object/from16 v25, v0

    const-string v26, "connectivity"

    invoke-virtual/range {v25 .. v26}, Lorg/medhelp/mydiet/Splash;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/ConnectivityManager;

    .line 180
    .local v8, cm:Landroid/net/ConnectivityManager;
    const/4 v6, 0x0

    .line 181
    .local v6, backgroundData:Z
    if-eqz v8, :cond_0

    .line 182
    invoke-virtual {v8}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v6

    .line 184
    :cond_0
    if-eqz v6, :cond_5

    .line 185
    invoke-direct/range {p0 .. p0}, Lorg/medhelp/mydiet/Splash$InitialTask;->isConnectedToNetwork()Z

    move-result v25

    if-eqz v25, :cond_5

    invoke-direct/range {p0 .. p0}, Lorg/medhelp/mydiet/Splash$InitialTask;->isNetworkRoaming()Z

    move-result v25

    if-nez v25, :cond_5

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lorg/medhelp/mydiet/Splash;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    const-string v26, "android_id"

    invoke-static/range {v25 .. v26}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 191
    .local v3, androidId:Ljava/lang/String;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    if-nez v25, :cond_3

    .line 192
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getSavedUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 193
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    if-nez v25, :cond_3

    .line 194
    :cond_2
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v0, v3}, Lorg/medhelp/mydiet/util/PreferenceUtil;->saveUUID(Landroid/content/Context;Ljava/lang/String;)V

    .line 199
    :cond_3
    const-string v4, "My Diet Diary"

    .line 200
    .local v4, applicationName:Ljava/lang/String;
    const-string v5, "1.2.2"

    .line 201
    .local v5, applicationVersion:Ljava/lang/String;
    const-string v16, "Android"

    .line 202
    .local v16, operatingSystem:Ljava/lang/String;
    sget-object v14, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 203
    .local v14, manufacturer:Ljava/lang/String;
    sget-object v15, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 204
    .local v15, model:Ljava/lang/String;
    sget-object v19, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 205
    .local v19, product:Ljava/lang/String;
    sget-object v7, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 206
    .local v7, brand:Ljava/lang/String;
    sget-object v20, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 207
    .local v20, release:Ljava/lang/String;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    sget v26, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 210
    .local v22, sdkVersion:Ljava/lang/String;
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 212
    .local v18, postParameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lorg/medhelp/mydiet/util/PreferenceUtil;->isFirstRunTracked(Landroid/content/Context;)Z

    move-result v25

    if-nez v25, :cond_4

    .line 214
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "first_use"

    const-string v27, "true"

    invoke-direct/range {v25 .. v27}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    :cond_4
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "uid"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "app"

    const-string v27, "My Diet Diary"

    invoke-direct/range {v25 .. v27}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "application_version"

    const-string v27, "1.2.2"

    invoke-direct/range {v25 .. v27}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "source"

    const-string v27, "Android"

    invoke-direct/range {v25 .. v27}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "os_version"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "manufacturer"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "model"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "product"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "brand"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "release"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUsageCount(Landroid/content/Context;)J

    move-result-wide v23

    .line 230
    .local v23, usageCount:J
    new-instance v25, Lorg/apache/http/message/BasicNameValuePair;

    const-string v26, "usage"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-wide/from16 v1, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v25 .. v27}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lorg/medhelp/mydiet/util/PreferenceUtil;->incrementUsageCount(Landroid/content/Context;)V

    .line 233
    new-instance v13, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v13}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 234
    .local v13, httpClient:Lorg/apache/http/client/HttpClient;
    new-instance v17, Lorg/apache/http/client/methods/HttpPost;

    const-string v25, "http://www.medhelp.org/analytics/create"

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 236
    .local v17, post:Lorg/apache/http/client/methods/HttpPost;
    const/16 v21, 0x0

    .line 238
    .local v21, response:Lorg/apache/http/HttpResponse;
    const/4 v11, 0x0

    .line 240
    .local v11, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_1
    new-instance v12, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 241
    .end local v11           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .local v12, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object v11, v12

    .line 247
    .end local v12           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v11       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :goto_0
    :try_start_3
    move-object/from16 v0, v17

    invoke-interface {v13, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v21

    .line 248
    new-instance v25, Lorg/apache/http/impl/client/BasicResponseHandler;

    invoke-direct/range {v25 .. v25}, Lorg/apache/http/impl/client/BasicResponseHandler;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/BasicResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 257
    :goto_1
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lorg/medhelp/mydiet/util/PreferenceUtil;->isFirstRunTracked(Landroid/content/Context;)Z

    move-result v25

    if-nez v25, :cond_5

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/Splash$InitialTask;->this$0:Lorg/medhelp/mydiet/Splash;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    invoke-static/range {v25 .. v26}, Lorg/medhelp/mydiet/util/PreferenceUtil;->markFirstRunTracked(Landroid/content/Context;Z)V

    .line 266
    .end local v3           #androidId:Ljava/lang/String;
    .end local v4           #applicationName:Ljava/lang/String;
    .end local v5           #applicationVersion:Ljava/lang/String;
    .end local v6           #backgroundData:Z
    .end local v7           #brand:Ljava/lang/String;
    .end local v8           #cm:Landroid/net/ConnectivityManager;
    .end local v11           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v13           #httpClient:Lorg/apache/http/client/HttpClient;
    .end local v14           #manufacturer:Ljava/lang/String;
    .end local v15           #model:Ljava/lang/String;
    .end local v16           #operatingSystem:Ljava/lang/String;
    .end local v17           #post:Lorg/apache/http/client/methods/HttpPost;
    .end local v18           #postParameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v19           #product:Ljava/lang/String;
    .end local v20           #release:Ljava/lang/String;
    .end local v21           #response:Lorg/apache/http/HttpResponse;
    .end local v22           #sdkVersion:Ljava/lang/String;
    .end local v23           #usageCount:J
    :cond_5
    :goto_2
    return-void

    .line 242
    .restart local v3       #androidId:Ljava/lang/String;
    .restart local v4       #applicationName:Ljava/lang/String;
    .restart local v5       #applicationVersion:Ljava/lang/String;
    .restart local v6       #backgroundData:Z
    .restart local v7       #brand:Ljava/lang/String;
    .restart local v8       #cm:Landroid/net/ConnectivityManager;
    .restart local v11       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v13       #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v14       #manufacturer:Ljava/lang/String;
    .restart local v15       #model:Ljava/lang/String;
    .restart local v16       #operatingSystem:Ljava/lang/String;
    .restart local v17       #post:Lorg/apache/http/client/methods/HttpPost;
    .restart local v18       #postParameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v19       #product:Ljava/lang/String;
    .restart local v20       #release:Ljava/lang/String;
    .restart local v21       #response:Lorg/apache/http/HttpResponse;
    .restart local v22       #sdkVersion:Ljava/lang/String;
    .restart local v23       #usageCount:J
    :catch_0
    move-exception v10

    .line 243
    .local v10, e1:Ljava/io/UnsupportedEncodingException;
    :goto_3
    invoke-virtual {v10}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 262
    .end local v3           #androidId:Ljava/lang/String;
    .end local v4           #applicationName:Ljava/lang/String;
    .end local v5           #applicationVersion:Ljava/lang/String;
    .end local v6           #backgroundData:Z
    .end local v7           #brand:Ljava/lang/String;
    .end local v8           #cm:Landroid/net/ConnectivityManager;
    .end local v10           #e1:Ljava/io/UnsupportedEncodingException;
    .end local v11           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v13           #httpClient:Lorg/apache/http/client/HttpClient;
    .end local v14           #manufacturer:Ljava/lang/String;
    .end local v15           #model:Ljava/lang/String;
    .end local v16           #operatingSystem:Ljava/lang/String;
    .end local v17           #post:Lorg/apache/http/client/methods/HttpPost;
    .end local v18           #postParameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v19           #product:Ljava/lang/String;
    .end local v20           #release:Ljava/lang/String;
    .end local v21           #response:Lorg/apache/http/HttpResponse;
    .end local v22           #sdkVersion:Ljava/lang/String;
    .end local v23           #usageCount:J
    :catch_1
    move-exception v9

    .line 264
    .local v9, e:Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 249
    .end local v9           #e:Ljava/lang/Exception;
    .restart local v3       #androidId:Ljava/lang/String;
    .restart local v4       #applicationName:Ljava/lang/String;
    .restart local v5       #applicationVersion:Ljava/lang/String;
    .restart local v6       #backgroundData:Z
    .restart local v7       #brand:Ljava/lang/String;
    .restart local v8       #cm:Landroid/net/ConnectivityManager;
    .restart local v11       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v13       #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v14       #manufacturer:Ljava/lang/String;
    .restart local v15       #model:Ljava/lang/String;
    .restart local v16       #operatingSystem:Ljava/lang/String;
    .restart local v17       #post:Lorg/apache/http/client/methods/HttpPost;
    .restart local v18       #postParameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .restart local v19       #product:Ljava/lang/String;
    .restart local v20       #release:Ljava/lang/String;
    .restart local v21       #response:Lorg/apache/http/HttpResponse;
    .restart local v22       #sdkVersion:Ljava/lang/String;
    .restart local v23       #usageCount:J
    :catch_2
    move-exception v9

    .line 250
    .local v9, e:Lorg/apache/http/client/ClientProtocolException;
    :try_start_5
    invoke-virtual {v9}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_1

    .line 251
    .end local v9           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_3
    move-exception v9

    .line 252
    .local v9, e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 242
    .end local v9           #e:Ljava/io/IOException;
    .end local v11           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v12       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :catch_4
    move-exception v10

    move-object v11, v12

    .end local v12           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v11       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    goto :goto_3
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/Splash$InitialTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs declared-synchronized doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 1
    .parameter "params"

    .prologue
    .line 167
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/Splash$InitialTask;->sendUsageData()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/Splash$InitialTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0
    .parameter "result"

    .prologue
    .line 173
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 174
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 162
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 163
    return-void
.end method
