.class public Lorg/medhelp/mydiet/activity/PreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "PreferencesActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;
    }
.end annotation


# instance fields
.field avatar:Landroid/preference/Preference;

.field birthdate:Landroid/preference/Preference;

.field bmr:Landroid/preference/Preference;

.field goals:Landroid/preference/Preference;

.field mDialog:Landroid/app/AlertDialog;

.field mWeight:D

.field medhelpAccount:Landroid/preference/Preference;

.field units:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mWeight:D

    .line 34
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 266
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->showAlertDialog()V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 281
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->hideAlertDialog()V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 97
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->updateBMRSummary()V

    return-void
.end method

.method private getCalculatedBMR()D
    .locals 12

    .prologue
    .line 156
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDateOfBirth(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getTodayMidnightCalendar()Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-static {v0, v8}, Lorg/medhelp/mydiet/util/DataUtil;->getAge(Ljava/util/Date;Ljava/util/Date;)D

    move-result-wide v5

    .line 158
    .local v5, age:D
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightInCm(Landroid/content/Context;)F

    move-result v0

    float-to-double v3, v0

    .line 159
    .local v3, height:D
    iget-wide v8, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mWeight:D

    const-wide v10, 0x3fdd07a84ab75e51L

    mul-double v1, v8, v10

    .line 160
    .local v1, weightInKg:D
    const-wide/16 v8, 0x0

    cmpg-double v0, v1, v8

    if-gtz v0, :cond_0

    .line 161
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInKG(Landroid/content/Context;)F

    move-result v0

    float-to-double v1, v0

    .line 163
    :cond_0
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v8, "male"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v7, 0x1

    .line 165
    .local v7, genderConstant:I
    :goto_0
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getActivityLevel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    invoke-static/range {v0 .. v8}, Lorg/medhelp/mydiet/util/DataUtil;->getBMRByActivityLevel(Landroid/content/Context;DDDILjava/lang/String;)D

    move-result-wide v8

    return-wide v8

    .line 163
    .end local v7           #genderConstant:I
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private hideAlertDialog()V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 285
    :cond_0
    return-void
.end method

.method private onCLickBMR()V
    .locals 13

    .prologue
    const-wide/16 v11, 0x0

    .line 169
    move-object v3, p0

    .line 170
    .local v3, ctx:Landroid/content/Context;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 171
    .local v2, builder:Landroid/app/AlertDialog$Builder;
    const-string v9, "layout_inflater"

    invoke-virtual {v3, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 172
    .local v6, inflater:Landroid/view/LayoutInflater;
    const v10, 0x7f03000a

    const v9, 0x7f0b0019

    invoke-virtual {p0, v9}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    invoke-virtual {v6, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 173
    .local v8, v:Landroid/view/View;
    invoke-virtual {v2, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 174
    const-string v9, "Enter BMR"

    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 176
    const v9, 0x7f0b0018

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    .line 178
    .local v5, editor:Landroid/widget/EditText;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserInputBMR(Landroid/content/Context;)F

    move-result v9

    float-to-double v0, v9

    .line 179
    .local v0, bmr:D
    cmpg-double v9, v0, v11

    if-gtz v9, :cond_0

    .line 180
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->getCalculatedBMR()D

    move-result-wide v0

    .line 183
    :cond_0
    cmpl-double v9, v0, v11

    if-lez v9, :cond_1

    .line 184
    new-instance v7, Ljava/text/DecimalFormatSymbols;

    new-instance v9, Ljava/util/Locale;

    const-string v10, "en"

    const-string v11, "en"

    invoke-direct {v9, v10, v11}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v7, v9}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 185
    .local v7, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v9, 0x2e

    invoke-virtual {v7, v9}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 186
    new-instance v4, Ljava/text/DecimalFormat;

    const-string v9, "#.#"

    invoke-direct {v4, v9, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 187
    .local v4, df:Ljava/text/DecimalFormat;
    invoke-virtual {v4, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 190
    .end local v4           #df:Ljava/text/DecimalFormat;
    .end local v7           #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_1
    new-instance v9, Lorg/medhelp/mydiet/activity/PreferencesActivity$1;

    invoke-direct {v9, p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity$1;-><init>(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V

    invoke-virtual {v5, v9}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 199
    const-string v9, "OK"

    new-instance v10, Lorg/medhelp/mydiet/activity/PreferencesActivity$2;

    invoke-direct {v10, p0, v5}, Lorg/medhelp/mydiet/activity/PreferencesActivity$2;-><init>(Lorg/medhelp/mydiet/activity/PreferencesActivity;Landroid/widget/EditText;)V

    invoke-virtual {v2, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 216
    const-string v9, "Cancel"

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 218
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    iput-object v9, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mDialog:Landroid/app/AlertDialog;

    .line 219
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    .line 220
    return-void
.end method

.method private showAlertDialog()V
    .locals 6

    .prologue
    .line 267
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 268
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v5, 0x7f03002c

    .line 269
    const v4, 0x7f0b0111

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 268
    invoke-virtual {v1, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 271
    .local v2, layout:Landroid/view/View;
    const v4, 0x7f0b001e

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 272
    .local v3, tv:Landroid/widget/TextView;
    const-string v4, "Loading preferences..."

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 275
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 276
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 277
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mDialog:Landroid/app/AlertDialog;

    .line 278
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 279
    return-void
.end method

.method private updateBMRSummary()V
    .locals 6

    .prologue
    .line 98
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserInputBMR(Landroid/content/Context;)F

    move-result v0

    .line 99
    .local v0, bodyMassRatio:F
    const/4 v3, 0x0

    cmpg-float v3, v0, v3

    if-gtz v3, :cond_0

    .line 100
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->getCalculatedBMR()D

    move-result-wide v3

    double-to-float v0, v3

    .line 102
    :cond_0
    new-instance v2, Ljava/text/DecimalFormatSymbols;

    new-instance v3, Ljava/util/Locale;

    const-string v4, "en"

    const-string v5, "en"

    invoke-direct {v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 103
    .local v2, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 104
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v3, "#.#"

    invoke-direct {v1, v3, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 105
    .local v1, df:Ljava/text/DecimalFormat;
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->bmr:Landroid/preference/Preference;

    float-to-double v4, v0

    invoke-virtual {v1, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 106
    return-void
.end method

.method private updateBirthdateSummary()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->birthdate:Landroid/preference/Preference;

    const-string v1, "MMMM dd, yyyy"

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDateOfBirthString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 95
    return-void
.end method

.method private updateMedHelpAccountSummary()V
    .locals 4

    .prologue
    .line 85
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, userType:Ljava/lang/String;
    const-string v1, "medhelp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->medhelpAccount:Landroid/preference/Preference;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Logged in as "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->medhelpAccount:Landroid/preference/Preference;

    const-string v2, "Login to sync/backup your data"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "mh_preferences"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 51
    const/high16 v0, 0x7f04

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->addPreferencesFromResource(I)V

    .line 53
    const-string v0, "medhelp_account"

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->medhelpAccount:Landroid/preference/Preference;

    .line 55
    const-string v0, "avatar"

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->avatar:Landroid/preference/Preference;

    .line 56
    const-string v0, "birthdate"

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->birthdate:Landroid/preference/Preference;

    .line 57
    const-string v0, "units"

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->units:Landroid/preference/Preference;

    .line 58
    const-string v0, "goals"

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->goals:Landroid/preference/Preference;

    .line 59
    const-string v0, "bmr"

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->bmr:Landroid/preference/Preference;

    .line 61
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->medhelpAccount:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 62
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->avatar:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 63
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->birthdate:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 64
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->units:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 65
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->goals:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 66
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->bmr:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 67
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 81
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 82
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1
    .parameter "preference"
    .parameter "newValue"

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .parameter "p"

    .prologue
    .line 110
    const-string v2, "medhelp_account"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, userType:Ljava/lang/String;
    const-string v2, "medhelp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/account/LogoutActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "edit_preferences"

    const-string v3, "edit_preferences"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    .line 152
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #userType:Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 118
    .restart local v1       #userType:Ljava/lang/String;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 119
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "edit_preferences"

    const-string v3, "edit_preferences"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 123
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #userType:Ljava/lang/String;
    :cond_2
    const-string v2, "avatar"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 125
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/SetupScreen1;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "edit_preferences"

    const-string v3, "edit_preferences"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 129
    .end local v0           #intent:Landroid/content/Intent;
    :cond_3
    const-string v2, "birthdate"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 131
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/SetupScreen2;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "edit_preferences"

    const-string v3, "edit_preferences"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 135
    .end local v0           #intent:Landroid/content/Intent;
    :cond_4
    const-string v2, "units"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 137
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 138
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "edit_preferences"

    const-string v3, "edit_preferences"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 141
    .end local v0           #intent:Landroid/content/Intent;
    :cond_5
    const-string v2, "goals"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 143
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/SetupScreen4;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 144
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "edit_preferences"

    const-string v3, "edit_preferences"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 147
    .end local v0           #intent:Landroid/content/Intent;
    :cond_6
    const-string v2, "bmr"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->onCLickBMR()V

    goto/16 :goto_0
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 71
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 72
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->updateMedHelpAccountSummary()V

    .line 73
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->updateBirthdateSummary()V

    .line 74
    new-instance v0, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;-><init>(Lorg/medhelp/mydiet/activity/PreferencesActivity;Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 75
    return-void
.end method
