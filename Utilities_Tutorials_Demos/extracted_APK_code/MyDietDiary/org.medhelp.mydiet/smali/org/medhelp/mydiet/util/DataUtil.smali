.class public Lorg/medhelp/mydiet/util/DataUtil;
.super Ljava/lang/Object;
.source "DataUtil.java"

# interfaces
.implements Lorg/medhelp/mydiet/C$jsonKeys;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAge(Ljava/util/Date;Ljava/util/Date;)D
    .locals 8
    .parameter "dateOfBirth"
    .parameter "onDate"

    .prologue
    .line 994
    if-nez p0, :cond_0

    .line 995
    const-wide/16 v4, 0x0

    .line 1007
    :goto_0
    return-wide v4

    .line 998
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_2

    .line 999
    :cond_1
    new-instance p1, Ljava/util/Date;

    .end local p1
    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    .line 1002
    .restart local p1
    :cond_2
    invoke-static {p0}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v0

    .line 1003
    .local v0, birthdayCal:Ljava/util/Calendar;
    invoke-static {p1}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v3

    .line 1005
    .local v3, onDateCal:Ljava/util/Calendar;
    invoke-static {v0, v3}, Lorg/medhelp/mydiet/util/DateUtil;->getDifferenceInDays(Ljava/util/Calendar;Ljava/util/Calendar;)J

    move-result-wide v4

    long-to-double v1, v4

    .line 1007
    .local v1, differenceInDays:D
    const-wide v4, 0x4076d00000000000L

    div-double v4, v1, v4

    goto :goto_0
.end method

.method private static getBMR(DDDI)D
    .locals 5
    .parameter "weightInKg"
    .parameter "heightInCM"
    .parameter "age"
    .parameter "gender"

    .prologue
    const/4 v4, 0x1

    .line 953
    if-eq p6, v4, :cond_0

    const/4 p6, 0x0

    .line 954
    :cond_0
    const-wide v0, 0x4023fae147ae147bL

    mul-double/2addr v0, p0

    const-wide/high16 v2, 0x4019

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    const-wide v2, 0x4013ae147ae147aeL

    mul-double/2addr v2, p4

    sub-double v2, v0, v2

    if-ne p6, v4, :cond_1

    const-wide/high16 v0, 0x4014

    :goto_0
    add-double/2addr v0, v2

    return-wide v0

    :cond_1
    const-wide v0, -0x3f9be00000000000L

    goto :goto_0
.end method

.method private static getBMRByActivityLevel(DDDILjava/lang/String;)D
    .locals 4
    .parameter "weightInKg"
    .parameter "heightInCM"
    .parameter "age"
    .parameter "gender"
    .parameter "activityLevel"

    .prologue
    .line 958
    const/4 v2, 0x1

    if-eq p6, v2, :cond_0

    const/4 p6, 0x0

    .line 959
    :cond_0
    invoke-static/range {p0 .. p6}, Lorg/medhelp/mydiet/util/DataUtil;->getBMR(DDDI)D

    move-result-wide v0

    .line 961
    .local v0, bmr:D
    if-nez p7, :cond_1

    const-string p7, "Sedentary"

    .line 963
    :cond_1
    const-string v2, "Moderately Active"

    invoke-virtual {p7, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 964
    const-wide v2, 0x3ff8cccccccccccdL

    mul-double/2addr v0, v2

    .line 975
    :goto_0
    return-wide v0

    .line 965
    :cond_2
    const-string v2, "Lightly Active"

    invoke-virtual {p7, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 966
    const-wide/high16 v2, 0x3ff6

    mul-double/2addr v0, v2

    goto :goto_0

    .line 967
    :cond_3
    const-string v2, "Very Active"

    invoke-virtual {p7, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 968
    const-wide v2, 0x3ffb99999999999aL

    mul-double/2addr v0, v2

    goto :goto_0

    .line 969
    :cond_4
    const-string v2, "Extremely Active"

    invoke-virtual {p7, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 970
    const-wide v2, 0x3ffe666666666666L

    mul-double/2addr v0, v2

    goto :goto_0

    .line 972
    :cond_5
    const-wide v2, 0x3ff3333333333333L

    mul-double/2addr v0, v2

    goto :goto_0
.end method

.method public static getBMRByActivityLevel(Landroid/content/Context;DDDILjava/lang/String;)D
    .locals 4
    .parameter "c"
    .parameter "weightInKg"
    .parameter "heightInCM"
    .parameter "age"
    .parameter "gender"
    .parameter "activityLevel"

    .prologue
    .line 979
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserInputBMR(Landroid/content/Context;)F

    move-result v2

    float-to-double v0, v2

    .line 980
    .local v0, bmr:D
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 983
    .end local v0           #bmr:D
    :goto_0
    return-wide v0

    .restart local v0       #bmr:D
    :cond_0
    invoke-static/range {p1 .. p8}, Lorg/medhelp/mydiet/util/DataUtil;->getBMRByActivityLevel(DDDILjava/lang/String;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D
    .locals 8
    .parameter "serving"
    .parameter "foodAmount"
    .parameter "nutrientValue"

    .prologue
    .line 440
    const-wide/16 v0, 0x0

    .line 441
    .local v0, equivalent:D
    const-wide/16 v4, 0x0

    .line 442
    .local v4, servingAmount:D
    if-eqz p0, :cond_0

    .line 443
    iget-wide v0, p0, Lorg/medhelp/mydiet/model/FoodServing;->equivalent:D

    .line 444
    iget-wide v4, p0, Lorg/medhelp/mydiet/model/FoodServing;->amount:D

    .line 446
    :cond_0
    invoke-static {p0, v0, v1, v4, v5}, Lorg/medhelp/mydiet/util/DataUtil;->getServingMultiplier(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v2

    .line 448
    .local v2, multiplier:D
    mul-double v6, p1, p3

    mul-double/2addr v6, v2

    return-wide v6
.end method

.method public static getCalorieBudget(DDD)D
    .locals 4
    .parameter "bmr"
    .parameter "changeInWeight"
    .parameter "daysToTarget"

    .prologue
    const-wide/16 v1, 0x0

    .line 1022
    cmpl-double v0, p4, v1

    if-nez v0, :cond_0

    const-wide/high16 p4, 0x3ff0

    .line 1023
    :cond_0
    cmpg-double v0, p4, v1

    if-gez v0, :cond_1

    const-wide/high16 v0, -0x4010

    .line 1025
    :goto_0
    return-wide v0

    :cond_1
    div-double v0, p2, p4

    const-wide v2, 0x40ab580000000000L

    mul-double/2addr v0, v2

    add-double/2addr v0, p0

    goto :goto_0
.end method

.method public static getCalorieBudget(Landroid/content/Context;DDLjava/util/Calendar;)D
    .locals 15
    .parameter "c"
    .parameter "weightInKg"
    .parameter "goalWeightChangeInLB"
    .parameter "referenceDay"

    .prologue
    .line 1029
    if-nez p0, :cond_1

    .line 1030
    const-wide/16 v8, 0x0

    .line 1057
    :cond_0
    :goto_0
    return-wide v8

    .line 1032
    :cond_1
    if-nez p5, :cond_2

    .line 1033
    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getTodayMidnightCalendar()Ljava/util/Calendar;

    move-result-object p5

    .line 1035
    :cond_2
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDateOfBirth(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual/range {p5 .. p5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/medhelp/mydiet/util/DataUtil;->getAge(Ljava/util/Date;Ljava/util/Date;)D

    move-result-wide v6

    .line 1039
    .local v6, age:D
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightInCm(Landroid/content/Context;)F

    move-result v1

    float-to-double v4, v1

    .line 1041
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Male"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v8, 0x1

    .line 1042
    :goto_1
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getActivityLevel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    move-object v1, p0

    move-wide/from16 v2, p1

    .line 1036
    invoke-static/range {v1 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getBMRByActivityLevel(Landroid/content/Context;DDDILjava/lang/String;)D

    move-result-wide v8

    .line 1044
    .local v8, calorieBudget:D
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGoal(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Maintain Weight"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1047
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDate(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v14

    .line 1048
    .local v14, targetDate:Ljava/util/Calendar;
    if-eqz v14, :cond_0

    move-object/from16 v0, p5

    invoke-virtual {v14, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1051
    move-object/from16 v0, p5

    invoke-static {v14, v0}, Lorg/medhelp/mydiet/util/DateUtil;->getDifferenceInDays(Ljava/util/Calendar;Ljava/util/Calendar;)J

    move-result-wide v1

    long-to-double v12, v1

    .line 1052
    .local v12, daysToTarget:D
    const-wide/16 v1, 0x0

    cmpg-double v1, v12, v1

    if-gtz v1, :cond_3

    .line 1053
    const-wide/high16 v12, 0x3ff0

    :cond_3
    move-wide/from16 v10, p3

    .line 1055
    invoke-static/range {v8 .. v13}, Lorg/medhelp/mydiet/util/DataUtil;->getCalorieBudget(DDD)D

    move-result-wide v8

    .line 1057
    goto :goto_0

    .line 1041
    .end local v8           #calorieBudget:D
    .end local v12           #daysToTarget:D
    .end local v14           #targetDate:Ljava/util/Calendar;
    :cond_4
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public static getCalorieCoefficient(Ljava/lang/String;)Lorg/medhelp/mydiet/model/CalorieCoefficientRange;
    .locals 6
    .parameter "calorieCoefficientJSON"

    .prologue
    .line 870
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 884
    :goto_0
    return-object v0

    .line 872
    :cond_1
    new-instance v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;

    invoke-direct {v0}, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;-><init>()V

    .line 875
    .local v0, ccRange:Lorg/medhelp/mydiet/model/CalorieCoefficientRange;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 877
    .local v1, ccRangeObject:Lorg/json/JSONObject;
    const-string v3, "calorie_coefficient"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    iput-wide v3, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->calorieCoefficient:D

    .line 878
    const-string v3, "max"

    const-wide/high16 v4, 0x4059

    invoke-virtual {v1, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    iput-wide v3, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->maxSpeed:D

    .line 879
    const-string v3, "min"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    iput-wide v3, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->minSpeed:D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 880
    .end local v1           #ccRangeObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 881
    .local v2, e:Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getCalorieCoefficientRanges(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .parameter "calorieCoefficientsJSON"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/CalorieCoefficientRange;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 888
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    move-object v1, v7

    .line 908
    :goto_0
    return-object v1

    .line 890
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 892
    .local v1, ccRanges:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/CalorieCoefficientRange;>;"
    const/4 v2, 0x0

    .line 893
    .local v2, ccRangesArray:Lorg/json/JSONArray;
    const/4 v0, 0x0

    .line 896
    .local v0, ccRangeObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 897
    .end local v2           #ccRangesArray:Lorg/json/JSONArray;
    .local v3, ccRangesArray:Lorg/json/JSONArray;
    if-eqz v3, :cond_2

    :try_start_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    move-object v1, v7

    goto :goto_0

    .line 899
    :cond_3
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 900
    .local v6, length:I
    const/4 v5, 0x0

    .local v5, i:I
    :goto_1
    if-lt v5, v6, :cond_4

    move-object v2, v3

    .end local v3           #ccRangesArray:Lorg/json/JSONArray;
    .restart local v2       #ccRangesArray:Lorg/json/JSONArray;
    goto :goto_0

    .line 901
    .end local v2           #ccRangesArray:Lorg/json/JSONArray;
    .restart local v3       #ccRangesArray:Lorg/json/JSONArray;
    :cond_4
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 902
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/medhelp/mydiet/util/DataUtil;->getCalorieCoefficient(Ljava/lang/String;)Lorg/medhelp/mydiet/model/CalorieCoefficientRange;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 900
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 904
    .end local v3           #ccRangesArray:Lorg/json/JSONArray;
    .end local v5           #i:I
    .end local v6           #length:I
    .restart local v2       #ccRangesArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v4

    .line 905
    .local v4, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 904
    .end local v2           #ccRangesArray:Lorg/json/JSONArray;
    .end local v4           #e:Lorg/json/JSONException;
    .restart local v3       #ccRangesArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v4

    move-object v2, v3

    .end local v3           #ccRangesArray:Lorg/json/JSONArray;
    .restart local v2       #ccRangesArray:Lorg/json/JSONArray;
    goto :goto_2
.end method

.method public static getDailyWeightChangeForTarget(DDDLjava/util/Date;)D
    .locals 6
    .parameter "bmr"
    .parameter "weightInLb"
    .parameter "goalWeight"
    .parameter "targetDate"

    .prologue
    .line 1015
    invoke-static {p6}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/medhelp/mydiet/util/DateUtil;->getDifferenceInDays(Ljava/util/Calendar;Ljava/util/Calendar;)J

    move-result-wide v4

    long-to-double v0, v4

    .line 1016
    .local v0, diffInDays:D
    sub-double v4, p4, p2

    div-double v2, v4, v0

    .line 1018
    .local v2, weightChangePerDay:D
    const-wide v4, 0x40ab580000000000L

    mul-double/2addr v4, v2

    add-double/2addr v4, p0

    return-wide v4
.end method

.method public static getEquivalentMultiplierToCreateOrEditFood(ZDD)D
    .locals 2
    .parameter "hasEquivalentUnitType"
    .parameter "servingAmount"
    .parameter "servingEquivalent"

    .prologue
    const-wide/16 v0, 0x0

    .line 462
    if-nez p0, :cond_1

    .line 463
    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    const-wide/high16 p1, 0x3ff0

    .line 464
    :cond_0
    const-wide/high16 v0, 0x3ff0

    div-double/2addr v0, p1

    .line 467
    :goto_0
    return-wide v0

    .line 466
    :cond_1
    cmpg-double v0, p3, v0

    if-gtz v0, :cond_2

    const-wide/high16 p3, 0x3ff0

    .line 467
    :cond_2
    const-wide/high16 v0, 0x4059

    div-double/2addr v0, p3

    goto :goto_0
.end method

.method public static getExercise(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Exercise;
    .locals 6
    .parameter "exerciseJSON"

    .prologue
    const/4 v1, 0x0

    .line 788
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "null"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 810
    :cond_0
    :goto_0
    return-object v1

    .line 790
    :cond_1
    new-instance v1, Lorg/medhelp/mydiet/model/Exercise;

    invoke-direct {v1}, Lorg/medhelp/mydiet/model/Exercise;-><init>()V

    .line 793
    .local v1, exercise:Lorg/medhelp/mydiet/model/Exercise;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 795
    .local v2, exerciseObject:Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lorg/medhelp/mydiet/model/Exercise;->medHelpId:Ljava/lang/String;

    .line 796
    const-string v3, "name"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lorg/medhelp/mydiet/model/Exercise;->name:Ljava/lang/String;

    .line 797
    const-string v3, "description"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lorg/medhelp/mydiet/model/Exercise;->description:Ljava/lang/String;

    .line 798
    iput-object p0, v1, Lorg/medhelp/mydiet/model/Exercise;->exerciseJSON:Ljava/lang/String;

    .line 799
    const-string v3, "has_distance"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v1, Lorg/medhelp/mydiet/model/Exercise;->hasDistance:Z

    .line 800
    const-string v3, "distance_max"

    const-wide v4, 0x409f400000000000L

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    iput-wide v3, v1, Lorg/medhelp/mydiet/model/Exercise;->maxDistance:D

    .line 801
    const-string v3, "speed_max"

    const-wide/high16 v4, 0x4059

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    iput-wide v3, v1, Lorg/medhelp/mydiet/model/Exercise;->maxSpeed:D

    .line 802
    const-string v3, "calorie_coefficient"

    const-wide/high16 v4, -0x4010

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    iput-wide v3, v1, Lorg/medhelp/mydiet/model/Exercise;->normalCalorieCoefficient:D

    .line 803
    const-string v3, "calorie_coefficient_ranges"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/medhelp/mydiet/util/DataUtil;->getCalorieCoefficientRanges(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v1, Lorg/medhelp/mydiet/model/Exercise;->ccRanges:Ljava/util/ArrayList;

    .line 804
    const-string v3, "types"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/medhelp/mydiet/util/DataUtil;->getExerciseTypes(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v1, Lorg/medhelp/mydiet/model/Exercise;->exerciseTypes:Ljava/util/ArrayList;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 806
    .end local v2           #exerciseObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 807
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getExerciseItem(Ljava/lang/String;)Lorg/medhelp/mydiet/model/ExerciseItem;
    .locals 6
    .parameter "exerciseItemJSON"

    .prologue
    .line 844
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 866
    :goto_0
    return-object v1

    .line 846
    :cond_1
    const/4 v2, 0x0

    .line 847
    .local v2, exerciseObject:Lorg/json/JSONObject;
    new-instance v1, Lorg/medhelp/mydiet/model/ExerciseItem;

    invoke-direct {v1}, Lorg/medhelp/mydiet/model/ExerciseItem;-><init>()V

    .line 850
    .local v1, exerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 851
    .end local v2           #exerciseObject:Lorg/json/JSONObject;
    .local v3, exerciseObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v4, "in_app_id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    .line 852
    const-string v4, "medhelp_id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseMHKey:Ljava/lang/String;

    .line 853
    const-string v4, "name"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    .line 854
    const-string v4, "exercise_type"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    .line 855
    const-string v4, "distance"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    .line 856
    const-string v4, "speed"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    .line 857
    const-string v4, "calories"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    .line 858
    const-string v4, "time"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    .line 859
    const-string v4, "time_period"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    .line 860
    const-string v4, "date"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .end local v3           #exerciseObject:Lorg/json/JSONObject;
    .restart local v2       #exerciseObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 862
    :catch_0
    move-exception v0

    .line 863
    .local v0, e:Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 862
    .end local v0           #e:Lorg/json/JSONException;
    .end local v2           #exerciseObject:Lorg/json/JSONObject;
    .restart local v3       #exerciseObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3           #exerciseObject:Lorg/json/JSONObject;
    .restart local v2       #exerciseObject:Lorg/json/JSONObject;
    goto :goto_1
.end method

.method public static getExerciseType(Ljava/lang/String;)Lorg/medhelp/mydiet/model/ExerciseType;
    .locals 6
    .parameter "exerciseJSON"

    .prologue
    .line 912
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const/4 v2, 0x0

    .line 925
    :goto_0
    return-object v2

    .line 914
    :cond_1
    new-instance v2, Lorg/medhelp/mydiet/model/ExerciseType;

    invoke-direct {v2}, Lorg/medhelp/mydiet/model/ExerciseType;-><init>()V

    .line 917
    .local v2, exerciseType:Lorg/medhelp/mydiet/model/ExerciseType;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 919
    .local v1, etObject:Lorg/json/JSONObject;
    const-string v3, "name"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lorg/medhelp/mydiet/model/ExerciseType;->name:Ljava/lang/String;

    .line 920
    const-string v3, "calorie_coefficient"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseType;->calorieCoefficient:D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 921
    .end local v1           #etObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 922
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getExerciseTypes(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .parameter "exerciseTypesJSON"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/ExerciseType;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 929
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    move-object v2, v7

    .line 949
    :goto_0
    return-object v2

    .line 931
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 933
    .local v2, exerciseTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/ExerciseType;>;"
    const/4 v3, 0x0

    .line 934
    .local v3, exerciseTypesArray:Lorg/json/JSONArray;
    const/4 v1, 0x0

    .line 937
    .local v1, exerciseTypeObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 938
    .end local v3           #exerciseTypesArray:Lorg/json/JSONArray;
    .local v4, exerciseTypesArray:Lorg/json/JSONArray;
    if-eqz v4, :cond_2

    :try_start_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    move-object v2, v7

    goto :goto_0

    .line 940
    :cond_3
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 941
    .local v6, length:I
    const/4 v5, 0x0

    .local v5, i:I
    :goto_1
    if-lt v5, v6, :cond_4

    move-object v3, v4

    .end local v4           #exerciseTypesArray:Lorg/json/JSONArray;
    .restart local v3       #exerciseTypesArray:Lorg/json/JSONArray;
    goto :goto_0

    .line 942
    .end local v3           #exerciseTypesArray:Lorg/json/JSONArray;
    .restart local v4       #exerciseTypesArray:Lorg/json/JSONArray;
    :cond_4
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 943
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/medhelp/mydiet/util/DataUtil;->getExerciseType(Ljava/lang/String;)Lorg/medhelp/mydiet/model/ExerciseType;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 941
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 945
    .end local v4           #exerciseTypesArray:Lorg/json/JSONArray;
    .end local v5           #i:I
    .end local v6           #length:I
    .restart local v3       #exerciseTypesArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 946
    .local v0, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 945
    .end local v0           #e:Lorg/json/JSONException;
    .end local v3           #exerciseTypesArray:Lorg/json/JSONArray;
    .restart local v4       #exerciseTypesArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4           #exerciseTypesArray:Lorg/json/JSONArray;
    .restart local v3       #exerciseTypesArray:Lorg/json/JSONArray;
    goto :goto_2
.end method

.method public static getExercises(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .parameter "exercisesJSON"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Exercise;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 814
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    move-object v3, v8

    .line 840
    :goto_0
    return-object v3

    .line 816
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 818
    .local v3, exercises:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Exercise;>;"
    const/4 v4, 0x0

    .line 819
    .local v4, exercisesArray:Lorg/json/JSONArray;
    const/4 v2, 0x0

    .line 822
    .local v2, exerciseObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 823
    .end local v4           #exercisesArray:Lorg/json/JSONArray;
    .local v5, exercisesArray:Lorg/json/JSONArray;
    if-eqz v5, :cond_2

    :try_start_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-nez v9, :cond_3

    :cond_2
    move-object v3, v8

    goto :goto_0

    .line 825
    :cond_3
    const/4 v1, 0x0

    .line 826
    .local v1, exercise:Lorg/medhelp/mydiet/model/Exercise;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    .line 827
    .local v7, length:I
    const/4 v6, 0x0

    .local v6, i:I
    :goto_1
    if-lt v6, v7, :cond_4

    move-object v4, v5

    .end local v5           #exercisesArray:Lorg/json/JSONArray;
    .restart local v4       #exercisesArray:Lorg/json/JSONArray;
    goto :goto_0

    .line 828
    .end local v4           #exercisesArray:Lorg/json/JSONArray;
    .restart local v5       #exercisesArray:Lorg/json/JSONArray;
    :cond_4
    const/4 v1, 0x0

    .line 830
    invoke-virtual {v5, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 832
    if-eqz v2, :cond_5

    .line 833
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lorg/medhelp/mydiet/util/DataUtil;->getExercise(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Exercise;

    move-result-object v1

    .line 834
    :cond_5
    if-eqz v1, :cond_6

    .line 835
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lorg/medhelp/mydiet/util/DataUtil;->getExercise(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Exercise;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 827
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 837
    .end local v1           #exercise:Lorg/medhelp/mydiet/model/Exercise;
    .end local v5           #exercisesArray:Lorg/json/JSONArray;
    .end local v6           #i:I
    .end local v7           #length:I
    .restart local v4       #exercisesArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 838
    .local v0, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 837
    .end local v0           #e:Lorg/json/JSONException;
    .end local v4           #exercisesArray:Lorg/json/JSONArray;
    .restart local v5       #exercisesArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v0

    move-object v4, v5

    .end local v5           #exercisesArray:Lorg/json/JSONArray;
    .restart local v4       #exercisesArray:Lorg/json/JSONArray;
    goto :goto_2
.end method

.method public static getFood(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Food;
    .locals 8
    .parameter "foodJSON"

    .prologue
    .line 34
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 56
    :goto_0
    return-object v1

    .line 36
    :cond_1
    new-instance v1, Lorg/medhelp/mydiet/model/Food;

    invoke-direct {v1}, Lorg/medhelp/mydiet/model/Food;-><init>()V

    .line 39
    .local v1, food:Lorg/medhelp/mydiet/model/Food;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 41
    .local v3, foodWithServingObject:Lorg/json/JSONObject;
    const-string v5, "food"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 42
    .local v2, foodObject:Lorg/json/JSONObject;
    const-string v5, "food_servings"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 44
    .local v4, servingsArray:Lorg/json/JSONArray;
    const-string v5, "external_id"

    const-wide/16 v6, -0x1

    invoke-virtual {v2, v5, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, v1, Lorg/medhelp/mydiet/model/Food;->inAppId:J

    .line 45
    const-string v5, "id"

    const-wide/16 v6, -0x1

    invoke-virtual {v2, v5, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, v1, Lorg/medhelp/mydiet/model/Food;->medHelpId:J

    .line 46
    const-string v5, "name"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lorg/medhelp/mydiet/model/Food;->name:Ljava/lang/String;

    .line 47
    const-string v5, "item_description"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    .line 48
    const-string v5, "calories"

    const-wide/16 v6, 0x0

    invoke-virtual {v2, v5, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v5

    iput-wide v5, v1, Lorg/medhelp/mydiet/model/Food;->calories:D

    .line 49
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    .line 50
    invoke-virtual {v4}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    .line 51
    const-string v5, "food_group_id"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v5

    iput-wide v5, v1, Lorg/medhelp/mydiet/model/Food;->groupId:J
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 52
    .end local v2           #foodObject:Lorg/json/JSONObject;
    .end local v3           #foodWithServingObject:Lorg/json/JSONObject;
    .end local v4           #servingsArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 53
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getFoodDetail(Ljava/lang/String;Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodDetail;
    .locals 7
    .parameter "foodJSONString"
    .parameter "servingsJSONString"

    .prologue
    .line 633
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 694
    :goto_0
    return-object v1

    .line 635
    :cond_1
    new-instance v1, Lorg/medhelp/mydiet/model/FoodDetail;

    invoke-direct {v1}, Lorg/medhelp/mydiet/model/FoodDetail;-><init>()V

    .line 636
    .local v1, foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    const/4 v2, 0x0

    .line 639
    .local v2, foodDetailJSONObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 641
    .end local v2           #foodDetailJSONObject:Lorg/json/JSONObject;
    .local v3, foodDetailJSONObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v4, "id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->medhelpId:J

    .line 643
    const-string v4, "food_group_id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->foodGroupId:I

    .line 644
    const-string v4, "status"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->status:Ljava/lang/String;

    .line 645
    const-string v4, "equivalent_unit_type"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->equivalentUnitType:Ljava/lang/String;

    .line 646
    const-string v4, "brand"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->brand:Ljava/lang/String;

    .line 647
    const-string v4, "manufacturer"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->manufacturer:Ljava/lang/String;

    .line 649
    const-string v4, "calories"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    .line 650
    const-string v4, "total_fat"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->totalFat:D

    .line 651
    const-string v4, "saturated_fatty_acids"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->saturatedFat:D

    .line 652
    const-string v4, "total_trans_fatty_acids"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->transFat:D

    .line 653
    const-string v4, "total_monounsaturated_fatty_acids"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->monoUnsaturatedFat:D

    .line 654
    const-string v4, "cholesterol"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->cholesterol:D

    .line 655
    const-string v4, "sodium"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->sodium:D

    .line 656
    const-string v4, "potassium"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->potassium:D

    .line 657
    const-string v4, "carbohydrate"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->totalCarbohydrate:D

    .line 658
    const-string v4, "fiber"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->dietaryFiber:D

    .line 659
    const-string v4, "total_sugars"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->sugars:D

    .line 660
    const-string v4, "protein"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->protein:D

    .line 661
    const-string v4, "vitamin_a"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminA:D

    .line 662
    const-string v4, "vitamin_c"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminC:D

    .line 663
    const-string v4, "vitamin_d"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminD:D

    .line 664
    const-string v4, "vitamin_e"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminE:D

    .line 665
    const-string v4, "vitamin_k"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminK:D

    .line 666
    const-string v4, "thiamin"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->thiamin:D

    .line 667
    const-string v4, "riboflavin"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->riboflavin:D

    .line 668
    const-string v4, "niacin"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->niacin:D

    .line 669
    const-string v4, "vitamin_b6"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB6:D

    .line 670
    const-string v4, "folate"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->folate:D

    .line 671
    const-string v4, "vitamin_b12"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB12:D

    .line 672
    const-string v4, "biotin"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->biotin:D

    .line 673
    const-string v4, "pantothenic_acid"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->pantothenicAcid:D

    .line 674
    const-string v4, "calcium"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->calcium:D

    .line 675
    const-string v4, "iron"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->iron:D

    .line 676
    const-string v4, "iodine"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->iodine:D

    .line 677
    const-string v4, "magnesium"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->magnesium:D

    .line 678
    const-string v4, "zinc"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->zinc:D

    .line 679
    const-string v4, "selenium"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->selenium:D

    .line 680
    const-string v4, "copper"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->copper:D

    .line 681
    const-string v4, "manganese"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->manganese:D

    .line 682
    const-string v4, "chromium"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->chromium:D

    .line 683
    const-string v4, "molybdenum"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->molybdenum:D

    .line 684
    const-string v4, "fluoride"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->fluoride:D

    .line 685
    const-string v4, "phosphorus"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->phosphorus:D

    .line 686
    const-string v4, "chloride"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->chloride:D

    .line 688
    invoke-static {p1}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodServings(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .end local v3           #foodDetailJSONObject:Lorg/json/JSONObject;
    .restart local v2       #foodDetailJSONObject:Lorg/json/JSONObject;
    goto/16 :goto_0

    .line 690
    :catch_0
    move-exception v0

    .line 691
    .local v0, e:Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 690
    .end local v0           #e:Lorg/json/JSONException;
    .end local v2           #foodDetailJSONObject:Lorg/json/JSONObject;
    .restart local v3       #foodDetailJSONObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3           #foodDetailJSONObject:Lorg/json/JSONObject;
    .restart local v2       #foodDetailJSONObject:Lorg/json/JSONObject;
    goto :goto_1
.end method

.method public static getFoodDetailToCreateOrEditFood(Lorg/medhelp/mydiet/model/FoodDetail;ZDD)Lorg/medhelp/mydiet/model/FoodDetail;
    .locals 11
    .parameter "rawFoodDetail"
    .parameter "hasEquivalentUnitType"
    .parameter "servingAmount"
    .parameter "servingEquivalent"

    .prologue
    .line 489
    if-nez p0, :cond_0

    const/4 v10, 0x0

    .line 606
    :goto_0
    return-object v10

    .line 491
    :cond_0
    move-object v10, p0

    .line 493
    .local v10, processedFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 492
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    .line 496
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->caloriesFromFat:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 495
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->caloriesFromFat:D

    .line 499
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->totalFat:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 498
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->totalFat:D

    .line 502
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->saturatedFat:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 501
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->saturatedFat:D

    .line 505
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->transFat:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 504
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->transFat:D

    .line 508
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->monoUnsaturatedFat:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 507
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->monoUnsaturatedFat:D

    .line 511
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->cholesterol:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 510
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->cholesterol:D

    .line 514
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->sodium:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 513
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->sodium:D

    .line 517
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->potassium:D

    const-wide v8, 0x40ab580000000000L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 516
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->potassium:D

    .line 520
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->totalCarbohydrate:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 519
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->totalCarbohydrate:D

    .line 523
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->dietaryFiber:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 522
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->dietaryFiber:D

    .line 526
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->sugars:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 525
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->sugars:D

    .line 529
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminA:D

    const-wide v8, 0x40b3880000000000L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 528
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminA:D

    .line 532
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminC:D

    const-wide/high16 v8, 0x404e

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 531
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminC:D

    .line 535
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminD:D

    const-wide/high16 v8, 0x4079

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 534
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminD:D

    .line 538
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminE:D

    const-wide/high16 v8, 0x4034

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 537
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminE:D

    .line 541
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminK:D

    const-wide/high16 v8, 0x4054

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 540
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminK:D

    .line 544
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->thiamin:D

    const-wide/high16 v8, 0x3ff8

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 543
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->thiamin:D

    .line 547
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->riboflavin:D

    const-wide v8, 0x3ffb333333333333L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 546
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->riboflavin:D

    .line 550
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->niacin:D

    const-wide/high16 v8, 0x4034

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 549
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->niacin:D

    .line 553
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB6:D

    const-wide/high16 v8, 0x4000

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 552
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB6:D

    .line 556
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->folate:D

    const-wide/high16 v8, 0x4079

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 555
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->folate:D

    .line 559
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB12:D

    const-wide/high16 v8, 0x4018

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 558
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB12:D

    .line 562
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->biotin:D

    const-wide v8, 0x4072c00000000000L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 561
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->biotin:D

    .line 565
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->pantothenicAcid:D

    const-wide/high16 v8, 0x4024

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 564
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->pantothenicAcid:D

    .line 568
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->calcium:D

    const-wide v8, 0x408f400000000000L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 567
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->calcium:D

    .line 571
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->iron:D

    const-wide/high16 v8, 0x4032

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 570
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->iron:D

    .line 574
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->iodine:D

    const-wide v8, 0x4062c00000000000L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 573
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->iodine:D

    .line 577
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->magnesium:D

    const-wide/high16 v8, 0x4079

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 576
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->magnesium:D

    .line 580
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->zinc:D

    const-wide/high16 v8, 0x402e

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 579
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->zinc:D

    .line 583
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->selenium:D

    const-wide v8, 0x4051800000000000L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 582
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->selenium:D

    .line 586
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->copper:D

    const-wide/high16 v8, 0x4000

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 585
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->copper:D

    .line 589
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->manganese:D

    const-wide/high16 v8, 0x4000

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 588
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->manganese:D

    .line 592
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->chromium:D

    const-wide/high16 v8, 0x405e

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 591
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->chromium:D

    .line 595
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->molybdenum:D

    const-wide v8, 0x4052c00000000000L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 594
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->molybdenum:D

    .line 598
    const/4 v0, 0x0

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->fluoride:D

    const-wide/16 v8, 0x0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 597
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->fluoride:D

    .line 601
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->phosphorus:D

    const-wide v8, 0x408f400000000000L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 600
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->phosphorus:D

    .line 604
    const/4 v0, 0x1

    iget-wide v6, p0, Lorg/medhelp/mydiet/model/FoodDetail;->chloride:D

    const-wide v8, 0x400b333333333333L

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 603
    invoke-static/range {v0 .. v9}, Lorg/medhelp/mydiet/util/DataUtil;->getNutrientWeightToCreateOrEditFood(ZZDDDD)D

    move-result-wide v0

    iput-wide v0, v10, Lorg/medhelp/mydiet/model/FoodDetail;->chloride:D

    goto/16 :goto_0
.end method

.method public static getFoodGroupId(Ljava/lang/String;Ljava/lang/String;)I
    .locals 8
    .parameter "foodGroupsStr"
    .parameter "foodGroup"

    .prologue
    const/4 v5, 0x0

    .line 749
    invoke-static {p0}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodGroups(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 751
    .local v1, foodGroups:Lorg/json/JSONArray;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-eqz v6, :cond_0

    if-nez p1, :cond_1

    .line 765
    :cond_0
    :goto_0
    return v5

    .line 753
    :cond_1
    const/4 v2, 0x0

    .line 754
    .local v2, group:Ljava/lang/String;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 755
    .local v4, size:I
    const/4 v3, 0x0

    .local v3, i:I
    :goto_1
    if-ge v3, v4, :cond_0

    .line 757
    :try_start_0
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "food_group"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "name"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 758
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 759
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "food_group"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "id"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    goto :goto_0

    .line 761
    :catch_0
    move-exception v0

    .line 762
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 755
    .end local v0           #e:Lorg/json/JSONException;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static getFoodGroups(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 8
    .parameter "foodGroupsStr"

    .prologue
    .line 698
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    const/4 v2, 0x0

    .line 714
    :goto_0
    return-object v2

    .line 700
    :cond_1
    const/4 v2, 0x0

    .line 702
    .local v2, foodGroupsArray:Lorg/json/JSONArray;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 703
    .end local v2           #foodGroupsArray:Lorg/json/JSONArray;
    .local v3, foodGroupsArray:Lorg/json/JSONArray;
    const/4 v1, 0x0

    .line 705
    .local v1, fg:Lorg/medhelp/mydiet/model/FoodGroup;
    :try_start_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    .line 706
    .local v5, size:I
    const/4 v4, 0x0

    .local v4, i:I
    :goto_1
    if-lt v4, v5, :cond_2

    move-object v2, v3

    .end local v3           #foodGroupsArray:Lorg/json/JSONArray;
    .restart local v2       #foodGroupsArray:Lorg/json/JSONArray;
    goto :goto_0

    .line 707
    .end local v2           #foodGroupsArray:Lorg/json/JSONArray;
    .restart local v3       #foodGroupsArray:Lorg/json/JSONArray;
    :cond_2
    new-instance v1, Lorg/medhelp/mydiet/model/FoodGroup;

    .end local v1           #fg:Lorg/medhelp/mydiet/model/FoodGroup;
    invoke-direct {v1}, Lorg/medhelp/mydiet/model/FoodGroup;-><init>()V

    .line 708
    .restart local v1       #fg:Lorg/medhelp/mydiet/model/FoodGroup;
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "food_group"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "id"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Lorg/medhelp/mydiet/model/FoodGroup;->id:I

    .line 709
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "food_group"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "name"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lorg/medhelp/mydiet/model/FoodGroup;->name:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 706
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 711
    .end local v1           #fg:Lorg/medhelp/mydiet/model/FoodGroup;
    .end local v3           #foodGroupsArray:Lorg/json/JSONArray;
    .end local v4           #i:I
    .end local v5           #size:I
    .restart local v2       #foodGroupsArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 712
    .local v0, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 711
    .end local v0           #e:Lorg/json/JSONException;
    .end local v2           #foodGroupsArray:Lorg/json/JSONArray;
    .restart local v3       #foodGroupsArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3           #foodGroupsArray:Lorg/json/JSONArray;
    .restart local v2       #foodGroupsArray:Lorg/json/JSONArray;
    goto :goto_2
.end method

.method public static getFoodGroupsArray(Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodGroup;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 718
    .local p0, foodGroups:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodGroup;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    const/4 v0, 0x0

    .line 726
    :cond_1
    return-object v0

    .line 720
    :cond_2
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    .line 721
    .local v0, groups:[Ljava/lang/String;
    const/4 v3, 0x0

    const-string v4, "None"

    aput-object v4, v0, v3

    .line 722
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v2, v3, 0x1

    .line 723
    .local v2, size:I
    const/4 v1, 0x1

    .local v1, i:I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 724
    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/medhelp/mydiet/model/FoodGroup;

    iget-object v3, v3, Lorg/medhelp/mydiet/model/FoodGroup;->name:Ljava/lang/String;

    aput-object v3, v0, v1

    .line 723
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getFoodGroupsArray(Lorg/json/JSONArray;)[Ljava/lang/String;
    .locals 7
    .parameter "foodGroups"

    .prologue
    const/4 v6, 0x0

    .line 730
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 745
    :goto_0
    return-object v1

    .line 732
    :cond_1
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 733
    .local v3, size:I
    add-int/lit8 v3, v3, 0x1

    .line 734
    new-array v1, v3, [Ljava/lang/String;

    .line 735
    .local v1, groups:[Ljava/lang/String;
    const-string v4, ""

    aput-object v4, v1, v6

    .line 736
    const/4 v2, 0x1

    .local v2, i:I
    :goto_1
    if-lt v2, v3, :cond_2

    .line 743
    invoke-static {v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 744
    const-string v4, "None"

    aput-object v4, v1, v6

    goto :goto_0

    .line 738
    :cond_2
    add-int/lit8 v4, v2, -0x1

    :try_start_0
    invoke-virtual {p0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "food_group"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 736
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 739
    :catch_0
    move-exception v0

    .line 740
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method

.method public static getFoodGroupsXML(Landroid/content/Context;I)Ljava/lang/String;
    .locals 6
    .parameter "c"
    .parameter "foodGroupsArrayResourceId"

    .prologue
    .line 769
    invoke-static {p0, p1}, Lorg/medhelp/mydiet/util/FileUtil;->readFile(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 770
    .local v2, foodGroupsString:Ljava/lang/String;
    invoke-static {v2}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodGroups(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 772
    .local v1, foodGroupsJSONArray:Lorg/json/JSONArray;
    invoke-static {v1}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodGroupsArray(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v0

    .line 774
    .local v0, foodGroups:[Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 775
    .local v4, s:Ljava/lang/StringBuilder;
    const-string v5, "<string-array name=\"food_groups\">\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 777
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    array-length v5, v0

    if-lt v3, v5, :cond_0

    .line 782
    const-string v5, "</string-array>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 784
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 778
    :cond_0
    const-string v5, "<item>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 779
    aget-object v5, v0, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 780
    const-string v5, "</item>\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 777
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static getFoodItemFromJSON(Ljava/lang/String;JDLjava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .locals 20
    .parameter "foodJSONWithServings"
    .parameter "servingId"
    .parameter "amount"
    .parameter "foodSource"

    .prologue
    .line 353
    const/4 v14, 0x0

    .line 354
    .local v14, foodJSONMain:Lorg/json/JSONObject;
    const/4 v12, 0x0

    .line 357
    .local v12, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :try_start_0
    new-instance v15, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    .end local v14           #foodJSONMain:Lorg/json/JSONObject;
    .local v15, foodJSONMain:Lorg/json/JSONObject;
    :try_start_1
    const-string v3, "food"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v16

    .line 360
    .local v16, foodObject:Lorg/json/JSONObject;
    if-nez v16, :cond_0

    const/4 v3, 0x0

    move-object v14, v15

    .line 427
    .end local v15           #foodJSONMain:Lorg/json/JSONObject;
    .end local v16           #foodObject:Lorg/json/JSONObject;
    .end local p3
    .restart local v14       #foodJSONMain:Lorg/json/JSONObject;
    :goto_0
    return-object v3

    .line 362
    .end local v14           #foodJSONMain:Lorg/json/JSONObject;
    .restart local v15       #foodJSONMain:Lorg/json/JSONObject;
    .restart local v16       #foodObject:Lorg/json/JSONObject;
    .restart local p3
    :cond_0
    new-instance v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-direct {v13}, Lorg/medhelp/mydiet/model/FoodItemInMeal;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 363
    .end local v12           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .local v13, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :try_start_2
    move-object/from16 v0, p0

    iput-object v0, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodJSONString:Ljava/lang/String;

    .line 364
    const-string v3, "id"

    const-wide/16 v4, -0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    .line 365
    const-string v3, "external_id"

    const-wide/16 v4, -0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    .line 366
    const-string v3, "name"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    .line 367
    const-string v3, "item_description"

    const-string v4, ""

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    .line 368
    iget-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 369
    const/4 v3, 0x0

    iput-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    .line 370
    :cond_1
    const-string v3, "brand"

    const-string v4, ""

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 371
    iget-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 372
    :cond_2
    const-string v3, "manufacturer"

    const-string v4, ""

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 373
    iget-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 374
    const-string v3, ""

    iput-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 377
    :cond_3
    move-object/from16 v0, p5

    iput-object v0, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodSource:Ljava/lang/String;

    .line 378
    const-string v3, "calories"

    const-wide/16 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v9

    .line 380
    .local v9, calories:D
    const/16 v18, 0x0

    .line 381
    .local v18, servings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodServing;>;"
    const-string v3, "food_servings"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 383
    .local v19, servingsJSON:Ljava/lang/String;
    if-eqz v19, :cond_4

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 384
    invoke-static/range {v19 .. v19}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodServings(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v18

    .line 385
    if-eqz v18, :cond_4

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 386
    invoke-static/range {v18 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 390
    :cond_4
    const/4 v2, 0x0

    .line 392
    .local v2, foodServing:Lorg/medhelp/mydiet/model/FoodServing;
    if-eqz v18, :cond_b

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_b

    .line 393
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2           #foodServing:Lorg/medhelp/mydiet/model/FoodServing;
    check-cast v2, Lorg/medhelp/mydiet/model/FoodServing;

    .line 395
    .restart local v2       #foodServing:Lorg/medhelp/mydiet/model/FoodServing;
    const-wide/16 v3, 0x0

    cmp-long v3, p1, v3

    if-lez v3, :cond_6

    .line 396
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_7

    .line 407
    :cond_6
    :goto_1
    if-eqz v2, :cond_9

    .line 408
    iget-wide v3, v2, Lorg/medhelp/mydiet/model/FoodServing;->medhelpId:J

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 409
    const-wide/16 v3, 0x0

    cmpl-double v3, p3, v3

    if-lez v3, :cond_8

    .end local p3
    :goto_2
    move-wide/from16 v0, p3

    iput-wide v0, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 410
    iget-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    invoke-static {v2, v3, v4, v9, v10}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 411
    iget-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    iget-wide v5, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v7, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemSummary(Lorg/medhelp/mydiet/model/FoodServing;DDLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    move-object v12, v13

    .end local v13           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v12       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    move-object v14, v15

    .end local v2           #foodServing:Lorg/medhelp/mydiet/model/FoodServing;
    .end local v9           #calories:D
    .end local v15           #foodJSONMain:Lorg/json/JSONObject;
    .end local v16           #foodObject:Lorg/json/JSONObject;
    .end local v18           #servings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodServing;>;"
    .end local v19           #servingsJSON:Ljava/lang/String;
    .restart local v14       #foodJSONMain:Lorg/json/JSONObject;
    :goto_3
    move-object v3, v12

    .line 427
    goto/16 :goto_0

    .line 396
    .end local v12           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v14           #foodJSONMain:Lorg/json/JSONObject;
    .restart local v2       #foodServing:Lorg/medhelp/mydiet/model/FoodServing;
    .restart local v9       #calories:D
    .restart local v13       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v15       #foodJSONMain:Lorg/json/JSONObject;
    .restart local v16       #foodObject:Lorg/json/JSONObject;
    .restart local v18       #servings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodServing;>;"
    .restart local v19       #servingsJSON:Ljava/lang/String;
    .restart local p3
    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/medhelp/mydiet/model/FoodServing;

    .line 397
    .local v17, serving:Lorg/medhelp/mydiet/model/FoodServing;
    if-eqz v17, :cond_5

    .line 400
    move-object/from16 v0, v17

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/FoodServing;->medhelpId:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_5

    .line 401
    move-object/from16 v2, v17

    .line 402
    goto :goto_1

    .line 409
    .end local v17           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    :cond_8
    iget-wide v0, v2, Lorg/medhelp/mydiet/model/FoodServing;->amount:D

    move-wide/from16 p3, v0

    goto :goto_2

    .line 413
    :cond_9
    const-wide/16 v3, 0x0

    cmpl-double v3, p3, v3

    if-lez v3, :cond_a

    .end local p3
    :goto_4
    move-wide/from16 v0, p3

    iput-wide v0, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 414
    iget-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    invoke-static {v2, v3, v4, v9, v10}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 415
    const/4 v3, 0x0

    iget-wide v4, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    iget-wide v6, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v8, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemSummary(Lorg/medhelp/mydiet/model/FoodServing;DDLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    move-object v12, v13

    .end local v13           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v12       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    move-object v14, v15

    .end local v15           #foodJSONMain:Lorg/json/JSONObject;
    .restart local v14       #foodJSONMain:Lorg/json/JSONObject;
    goto :goto_3

    .line 413
    .end local v12           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v14           #foodJSONMain:Lorg/json/JSONObject;
    .restart local v13       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v15       #foodJSONMain:Lorg/json/JSONObject;
    .restart local p3
    :cond_a
    const-wide/high16 p3, 0x3ff0

    goto :goto_4

    .line 418
    :cond_b
    const-wide/16 v3, 0x0

    cmpl-double v3, p3, v3

    if-lez v3, :cond_c

    .end local p3
    :goto_5
    move-wide/from16 v0, p3

    iput-wide v0, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 419
    iget-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    invoke-static {v2, v3, v4, v9, v10}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 420
    const/4 v3, 0x0

    iget-wide v4, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    iget-wide v6, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v8, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemSummary(Lorg/medhelp/mydiet/model/FoodServing;DDLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v12, v13

    .end local v13           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v12       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    move-object v14, v15

    .end local v15           #foodJSONMain:Lorg/json/JSONObject;
    .restart local v14       #foodJSONMain:Lorg/json/JSONObject;
    goto :goto_3

    .line 418
    .end local v12           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v14           #foodJSONMain:Lorg/json/JSONObject;
    .restart local v13       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v15       #foodJSONMain:Lorg/json/JSONObject;
    .restart local p3
    :cond_c
    const-wide/high16 p3, 0x3ff0

    goto :goto_5

    .line 423
    .end local v2           #foodServing:Lorg/medhelp/mydiet/model/FoodServing;
    .end local v9           #calories:D
    .end local v13           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v15           #foodJSONMain:Lorg/json/JSONObject;
    .end local v16           #foodObject:Lorg/json/JSONObject;
    .end local v18           #servings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodServing;>;"
    .end local v19           #servingsJSON:Ljava/lang/String;
    .restart local v12       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v14       #foodJSONMain:Lorg/json/JSONObject;
    :catch_0
    move-exception v11

    .line 424
    .end local p3
    .local v11, e:Lorg/json/JSONException;
    :goto_6
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    .line 423
    .end local v11           #e:Lorg/json/JSONException;
    .end local v14           #foodJSONMain:Lorg/json/JSONObject;
    .restart local v15       #foodJSONMain:Lorg/json/JSONObject;
    .restart local p3
    :catch_1
    move-exception v11

    move-object v14, v15

    .end local v15           #foodJSONMain:Lorg/json/JSONObject;
    .restart local v14       #foodJSONMain:Lorg/json/JSONObject;
    goto :goto_6

    .end local v12           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v14           #foodJSONMain:Lorg/json/JSONObject;
    .end local p3
    .restart local v13       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v15       #foodJSONMain:Lorg/json/JSONObject;
    .restart local v16       #foodObject:Lorg/json/JSONObject;
    :catch_2
    move-exception v11

    move-object v12, v13

    .end local v13           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v12       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    move-object v14, v15

    .end local v15           #foodJSONMain:Lorg/json/JSONObject;
    .restart local v14       #foodJSONMain:Lorg/json/JSONObject;
    goto :goto_6
.end method

.method public static getFoodItemInMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .locals 7
    .parameter "foodItemJSON"

    .prologue
    .line 60
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 83
    :goto_0
    return-object v1

    .line 62
    :cond_1
    const/4 v2, 0x0

    .line 63
    .local v2, foodObject:Lorg/json/JSONObject;
    new-instance v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-direct {v1}, Lorg/medhelp/mydiet/model/FoodItemInMeal;-><init>()V

    .line 66
    .local v1, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .end local v2           #foodObject:Lorg/json/JSONObject;
    .local v3, foodObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v4, "food_id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 69
    const-string v4, "in_app_id"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    .line 70
    const-string v4, "medhelp_id"

    const-wide/16 v5, -0x1

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    .line 71
    const-string v4, "food_name"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    .line 72
    const-string v4, "food_description"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    .line 73
    const-string v4, "food_summary"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 74
    const-string v4, "serving_id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 75
    const-string v4, "amount"

    const-wide/high16 v5, 0x3ff0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 76
    const-string v4, "food_calories"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 77
    const-string v4, "is_selected"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .end local v3           #foodObject:Lorg/json/JSONObject;
    .restart local v2       #foodObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, e:Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 79
    .end local v0           #e:Lorg/json/JSONException;
    .end local v2           #foodObject:Lorg/json/JSONObject;
    .restart local v3       #foodObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3           #foodObject:Lorg/json/JSONObject;
    .restart local v2       #foodObject:Lorg/json/JSONObject;
    goto :goto_1
.end method

.method public static getFoodItemSummary(Lorg/medhelp/mydiet/model/FoodServing;DDLjava/lang/String;)Ljava/lang/String;
    .locals 6
    .parameter "foodServing"
    .parameter "caloriesInFoodItem"
    .parameter "amount"
    .parameter "prefix"

    .prologue
    .line 610
    const-string v1, ""

    .line 611
    .local v1, summary:Ljava/lang/String;
    new-instance v2, Ljava/text/DecimalFormatSymbols;

    new-instance v3, Ljava/util/Locale;

    const-string v4, "en"

    const-string v5, "en"

    invoke-direct {v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 612
    .local v2, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 613
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v3, "0.0"

    invoke-direct {v0, v3, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 615
    .local v0, df1:Ljava/text/DecimalFormat;
    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 616
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 619
    :cond_0
    if-eqz p0, :cond_2

    .line 620
    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 621
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 622
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 626
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 627
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Calories"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 629
    return-object v1
.end method

.method public static getFoodItems(Ljava/lang/String;Z)Ljava/util/ArrayList;
    .locals 18
    .parameter "foodsJSON"
    .parameter "isMedHelpGenerated"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 269
    .local v10, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    if-eqz p0, :cond_0

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 270
    :cond_0
    const/4 v10, 0x0

    .line 334
    .end local v10           #foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    :goto_0
    return-object v10

    .line 272
    .restart local v10       #foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    :cond_1
    const/4 v11, 0x0

    .line 274
    .local v11, foodsJSONObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v12, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    .end local v11           #foodsJSONObject:Lorg/json/JSONObject;
    .local v12, foodsJSONObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v1, "mh_approved_foods"

    invoke-virtual {v12, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    .line 277
    .local v15, mhApprovedFoodsArray:Lorg/json/JSONArray;
    const-string v1, "user_foods"

    invoke-virtual {v12, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v17

    .line 278
    .local v17, usersFoodArray:Lorg/json/JSONArray;
    const-string v1, "my_foods"

    invoke-virtual {v12, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v16

    .line 281
    .local v16, myFoodsArray:Lorg/json/JSONArray;
    if-eqz v15, :cond_2

    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_4

    .line 282
    :cond_2
    if-eqz v17, :cond_3

    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_4

    .line 283
    :cond_3
    const/4 v10, 0x0

    goto :goto_0

    .line 285
    :cond_4
    const/4 v9, 0x0

    .line 287
    .local v9, foodItem:Lorg/json/JSONObject;
    const/4 v14, 0x0

    .line 288
    .local v14, length:I
    if-eqz v16, :cond_5

    .line 289
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONArray;->length()I

    move-result v14

    .line 290
    :cond_5
    const/4 v8, 0x0

    .line 291
    .local v8, food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const/4 v13, 0x0

    .local v13, i:I
    :goto_1
    if-lt v13, v14, :cond_8

    .line 301
    const/4 v14, 0x0

    .line 302
    if-eqz v15, :cond_6

    .line 303
    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v14

    .line 305
    :cond_6
    const/4 v13, 0x0

    :goto_2
    if-lt v13, v14, :cond_a

    .line 315
    const/4 v14, 0x0

    .line 316
    if-eqz v17, :cond_7

    .line 317
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->length()I

    move-result v14

    .line 319
    :cond_7
    const/4 v13, 0x0

    :goto_3
    if-lt v13, v14, :cond_c

    move-object v11, v12

    .end local v12           #foodsJSONObject:Lorg/json/JSONObject;
    .restart local v11       #foodsJSONObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 292
    .end local v11           #foodsJSONObject:Lorg/json/JSONObject;
    .restart local v12       #foodsJSONObject:Lorg/json/JSONObject;
    :cond_8
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 293
    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    const-string v6, "my_foods"

    invoke-static/range {v1 .. v6}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemFromJSON(Ljava/lang/String;JDLjava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v8

    .line 295
    iget-wide v1, v8, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_9

    .line 296
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, v8, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 298
    :cond_9
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 306
    :cond_a
    invoke-virtual {v15, v13}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 307
    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    const-string v6, "mh_approved_foods"

    invoke-static/range {v1 .. v6}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemFromJSON(Ljava/lang/String;JDLjava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v8

    .line 309
    iget-wide v1, v8, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_b

    .line 310
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, v8, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 312
    :cond_b
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 320
    :cond_c
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 321
    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    const-string v6, "user_foods"

    invoke-static/range {v1 .. v6}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemFromJSON(Ljava/lang/String;JDLjava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v8

    .line 323
    iget-wide v1, v8, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_d

    .line 324
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, v8, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 327
    :cond_d
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 319
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 330
    .end local v8           #food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v9           #foodItem:Lorg/json/JSONObject;
    .end local v12           #foodsJSONObject:Lorg/json/JSONObject;
    .end local v13           #i:I
    .end local v14           #length:I
    .end local v15           #mhApprovedFoodsArray:Lorg/json/JSONArray;
    .end local v16           #myFoodsArray:Lorg/json/JSONArray;
    .end local v17           #usersFoodArray:Lorg/json/JSONArray;
    .restart local v11       #foodsJSONObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v7

    .line 331
    .local v7, e:Lorg/json/JSONException;
    :goto_4
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 330
    .end local v7           #e:Lorg/json/JSONException;
    .end local v11           #foodsJSONObject:Lorg/json/JSONObject;
    .restart local v12       #foodsJSONObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v7

    move-object v11, v12

    .end local v12           #foodsJSONObject:Lorg/json/JSONObject;
    .restart local v11       #foodsJSONObject:Lorg/json/JSONObject;
    goto :goto_4
.end method

.method public static getFoodItemsInMeal(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .parameter "foodsInMealsJSON"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 154
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    move-object v1, v7

    .line 174
    :goto_0
    return-object v1

    .line 156
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 158
    .local v1, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    const/4 v3, 0x0

    .line 159
    .local v3, foodsArray:Lorg/json/JSONArray;
    const/4 v2, 0x0

    .line 162
    .local v2, foodObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .end local v3           #foodsArray:Lorg/json/JSONArray;
    .local v4, foodsArray:Lorg/json/JSONArray;
    if-eqz v4, :cond_2

    :try_start_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    move-object v1, v7

    goto :goto_0

    .line 165
    :cond_3
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 166
    .local v6, length:I
    const/4 v5, 0x0

    .local v5, i:I
    :goto_1
    if-lt v5, v6, :cond_4

    move-object v3, v4

    .end local v4           #foodsArray:Lorg/json/JSONArray;
    .restart local v3       #foodsArray:Lorg/json/JSONArray;
    goto :goto_0

    .line 167
    .end local v3           #foodsArray:Lorg/json/JSONArray;
    .restart local v4       #foodsArray:Lorg/json/JSONArray;
    :cond_4
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 168
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemInMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 166
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 170
    .end local v4           #foodsArray:Lorg/json/JSONArray;
    .end local v5           #i:I
    .end local v6           #length:I
    .restart local v3       #foodsArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 171
    .local v0, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 170
    .end local v0           #e:Lorg/json/JSONException;
    .end local v3           #foodsArray:Lorg/json/JSONArray;
    .restart local v4       #foodsArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4           #foodsArray:Lorg/json/JSONArray;
    .restart local v3       #foodsArray:Lorg/json/JSONArray;
    goto :goto_2
.end method

.method public static getFoodItemsInMealAsJSONString(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 178
    .local p0, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    const/4 v5, 0x0

    .line 192
    :goto_0
    return-object v5

    .line 180
    :cond_1
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 181
    .local v4, foodsArray:Lorg/json/JSONArray;
    const/4 v2, 0x0

    .line 184
    .local v2, foodObject:Lorg/json/JSONObject;
    :try_start_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    move-object v3, v2

    .end local v2           #foodObject:Lorg/json/JSONObject;
    .local v3, foodObject:Lorg/json/JSONObject;
    :goto_1
    :try_start_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    if-nez v6, :cond_2

    move-object v2, v3

    .line 192
    .end local v3           #foodObject:Lorg/json/JSONObject;
    .restart local v2       #foodObject:Lorg/json/JSONObject;
    :goto_2
    invoke-virtual {v4}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 184
    .end local v2           #foodObject:Lorg/json/JSONObject;
    .restart local v3       #foodObject:Lorg/json/JSONObject;
    :cond_2
    :try_start_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 185
    .local v1, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    new-instance v2, Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/FoodItemInMeal;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 186
    .end local v3           #foodObject:Lorg/json/JSONObject;
    .restart local v2       #foodObject:Lorg/json/JSONObject;
    :try_start_3
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-object v3, v2

    .end local v2           #foodObject:Lorg/json/JSONObject;
    .restart local v3       #foodObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 188
    .end local v1           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v3           #foodObject:Lorg/json/JSONObject;
    .restart local v2       #foodObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 189
    .local v0, e:Ljava/lang/Exception;
    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 188
    .end local v0           #e:Ljava/lang/Exception;
    .end local v2           #foodObject:Lorg/json/JSONObject;
    .restart local v3       #foodObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3           #foodObject:Lorg/json/JSONObject;
    .restart local v2       #foodObject:Lorg/json/JSONObject;
    goto :goto_3
.end method

.method public static getFoodJSONWithServings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "foodJSON"
    .parameter "foodServingsJSON"

    .prologue
    .line 339
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    .line 349
    :goto_0
    return-object v2

    .line 341
    :cond_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 343
    .local v1, foodObject:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "food"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 344
    const-string v2, "food_servings"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getFoodServings(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .parameter "servingsJSON"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodServing;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 224
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_1

    :cond_0
    move-object v6, v9

    .line 257
    :goto_0
    return-object v6

    .line 226
    :cond_1
    const/4 v7, 0x0

    .line 227
    .local v7, servingsArray:Lorg/json/JSONArray;
    const/4 v5, 0x0

    .line 228
    .local v5, servingObject:Lorg/json/JSONObject;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 229
    .local v6, servings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodServing;>;"
    new-instance v3, Lorg/medhelp/mydiet/model/FoodServing;

    invoke-direct {v3}, Lorg/medhelp/mydiet/model/FoodServing;-><init>()V

    .line 232
    .local v3, serving:Lorg/medhelp/mydiet/model/FoodServing;
    :try_start_0
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    .end local v7           #servingsArray:Lorg/json/JSONArray;
    .local v8, servingsArray:Lorg/json/JSONArray;
    if-eqz v8, :cond_2

    :try_start_1
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v10

    if-nez v10, :cond_3

    :cond_2
    move-object v6, v9

    .line 234
    goto :goto_0

    .line 236
    :cond_3
    const/16 v0, 0x64

    .line 237
    .local v0, GENERATED_PRIORITY_START:I
    const/4 v2, 0x0

    .local v2, i:I
    move-object v4, v3

    .end local v3           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .local v4, serving:Lorg/medhelp/mydiet/model/FoodServing;
    :goto_1
    :try_start_2
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lt v2, v9, :cond_4

    move-object v3, v4

    .end local v4           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .restart local v3       #serving:Lorg/medhelp/mydiet/model/FoodServing;
    move-object v7, v8

    .end local v8           #servingsArray:Lorg/json/JSONArray;
    .restart local v7       #servingsArray:Lorg/json/JSONArray;
    goto :goto_0

    .line 238
    .end local v3           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .end local v7           #servingsArray:Lorg/json/JSONArray;
    .restart local v4       #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .restart local v8       #servingsArray:Lorg/json/JSONArray;
    :cond_4
    new-instance v3, Lorg/medhelp/mydiet/model/FoodServing;

    invoke-direct {v3}, Lorg/medhelp/mydiet/model/FoodServing;-><init>()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 239
    .end local v4           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .restart local v3       #serving:Lorg/medhelp/mydiet/model/FoodServing;
    :try_start_3
    invoke-virtual {v8, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 241
    int-to-long v9, v2

    iput-wide v9, v3, Lorg/medhelp/mydiet/model/FoodServing;->id:J

    .line 242
    const-string v9, "id"

    const-wide/16 v10, -0x1

    invoke-virtual {v5, v9, v10, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v9

    iput-wide v9, v3, Lorg/medhelp/mydiet/model/FoodServing;->medhelpId:J

    .line 243
    const-string v9, "food_id"

    const-wide/16 v10, -0x1

    invoke-virtual {v5, v9, v10, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v9

    iput-wide v9, v3, Lorg/medhelp/mydiet/model/FoodServing;->foodId:J

    .line 244
    const-string v9, "name"

    const-string v10, ""

    invoke-virtual {v5, v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v3, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    .line 245
    const-string v9, "equivalent"

    const-wide/high16 v10, -0x4010

    invoke-virtual {v5, v9, v10, v11}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v9

    iput-wide v9, v3, Lorg/medhelp/mydiet/model/FoodServing;->equivalent:D

    .line 246
    const-string v9, "amount"

    const-wide/high16 v10, 0x3ff0

    invoke-virtual {v5, v9, v10, v11}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v9

    iput-wide v9, v3, Lorg/medhelp/mydiet/model/FoodServing;->amount:D

    .line 247
    const-string v9, "priority"

    add-int/lit8 v10, v2, 0x64

    invoke-virtual {v5, v9, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, v3, Lorg/medhelp/mydiet/model/FoodServing;->priority:I

    .line 248
    iget-wide v9, v3, Lorg/medhelp/mydiet/model/FoodServing;->amount:D

    const-wide/16 v11, 0x0

    cmpg-double v9, v9, v11

    if-gtz v9, :cond_5

    const-wide/high16 v9, 0x3ff0

    iput-wide v9, v3, Lorg/medhelp/mydiet/model/FoodServing;->amount:D

    .line 250
    :cond_5
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 237
    add-int/lit8 v2, v2, 0x1

    move-object v4, v3

    .end local v3           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .restart local v4       #serving:Lorg/medhelp/mydiet/model/FoodServing;
    goto :goto_1

    .line 253
    .end local v0           #GENERATED_PRIORITY_START:I
    .end local v2           #i:I
    .end local v4           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .end local v8           #servingsArray:Lorg/json/JSONArray;
    .restart local v3       #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .restart local v7       #servingsArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 254
    .local v1, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 253
    .end local v1           #e:Lorg/json/JSONException;
    .end local v7           #servingsArray:Lorg/json/JSONArray;
    .restart local v8       #servingsArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v1

    move-object v7, v8

    .end local v8           #servingsArray:Lorg/json/JSONArray;
    .restart local v7       #servingsArray:Lorg/json/JSONArray;
    goto :goto_2

    .end local v3           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .end local v7           #servingsArray:Lorg/json/JSONArray;
    .restart local v0       #GENERATED_PRIORITY_START:I
    .restart local v2       #i:I
    .restart local v4       #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .restart local v8       #servingsArray:Lorg/json/JSONArray;
    :catch_2
    move-exception v1

    move-object v3, v4

    .end local v4           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    .restart local v3       #serving:Lorg/medhelp/mydiet/model/FoodServing;
    move-object v7, v8

    .end local v8           #servingsArray:Lorg/json/JSONArray;
    .restart local v7       #servingsArray:Lorg/json/JSONArray;
    goto :goto_2
.end method

.method public static getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;
    .locals 6
    .parameter "mealJSON"

    .prologue
    .line 87
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 108
    :goto_0
    return-object v1

    .line 89
    :cond_1
    const/4 v2, 0x0

    .line 90
    .local v2, mealsObject:Lorg/json/JSONObject;
    new-instance v1, Lorg/medhelp/mydiet/model/Meal;

    invoke-direct {v1}, Lorg/medhelp/mydiet/model/Meal;-><init>()V

    .line 93
    .local v1, meal:Lorg/medhelp/mydiet/model/Meal;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v2           #mealsObject:Lorg/json/JSONObject;
    .local v3, mealsObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v4, "id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/Meal;->id:J

    .line 96
    const-string v4, "meal_type"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/medhelp/mydiet/model/Meal;->setMealType(Ljava/lang/String;)V

    .line 97
    const-string v4, "foods"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemsInMeal(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    .line 98
    const-string v4, "meal_time"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    .line 99
    const-string v4, "user_input_calories"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    .line 100
    const-string v4, "notes"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    .line 101
    const-string v4, "where"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    .line 102
    const-string v4, "with_whom"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .end local v3           #mealsObject:Lorg/json/JSONObject;
    .restart local v2       #mealsObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, e:Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 104
    .end local v0           #e:Lorg/json/JSONException;
    .end local v2           #mealsObject:Lorg/json/JSONObject;
    .restart local v3       #mealsObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3           #mealsObject:Lorg/json/JSONObject;
    .restart local v2       #mealsObject:Lorg/json/JSONObject;
    goto :goto_1
.end method

.method public static getMeals(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .parameter "mealsJson"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Meal;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 112
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    move-object v3, v7

    .line 132
    :goto_0
    return-object v3

    .line 114
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 116
    .local v3, meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    const/4 v4, 0x0

    .line 117
    .local v4, mealsArray:Lorg/json/JSONArray;
    const/4 v6, 0x0

    .line 120
    .local v6, mealsObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    .end local v4           #mealsArray:Lorg/json/JSONArray;
    .local v5, mealsArray:Lorg/json/JSONArray;
    if-eqz v5, :cond_2

    :try_start_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    move-object v3, v7

    goto :goto_0

    .line 123
    :cond_3
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 124
    .local v2, length:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    if-lt v1, v2, :cond_4

    move-object v4, v5

    .end local v5           #mealsArray:Lorg/json/JSONArray;
    .restart local v4       #mealsArray:Lorg/json/JSONArray;
    goto :goto_0

    .line 125
    .end local v4           #mealsArray:Lorg/json/JSONArray;
    .restart local v5       #mealsArray:Lorg/json/JSONArray;
    :cond_4
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 126
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 128
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v5           #mealsArray:Lorg/json/JSONArray;
    .restart local v4       #mealsArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 129
    .local v0, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 128
    .end local v0           #e:Lorg/json/JSONException;
    .end local v4           #mealsArray:Lorg/json/JSONArray;
    .restart local v5       #mealsArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v0

    move-object v4, v5

    .end local v5           #mealsArray:Lorg/json/JSONArray;
    .restart local v4       #mealsArray:Lorg/json/JSONArray;
    goto :goto_2
.end method

.method public static getMealsAsJSONArrayString(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Meal;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 136
    .local p0, meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    const/4 v5, 0x0

    .line 150
    :goto_0
    return-object v5

    .line 138
    :cond_1
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 139
    .local v2, mealsArray:Lorg/json/JSONArray;
    const/4 v3, 0x0

    .line 142
    .local v3, object:Lorg/json/JSONObject;
    :try_start_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    move-object v4, v3

    .end local v3           #object:Lorg/json/JSONObject;
    .local v4, object:Lorg/json/JSONObject;
    :goto_1
    :try_start_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    if-nez v6, :cond_2

    move-object v3, v4

    .line 150
    .end local v4           #object:Lorg/json/JSONObject;
    .restart local v3       #object:Lorg/json/JSONObject;
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 142
    .end local v3           #object:Lorg/json/JSONObject;
    .restart local v4       #object:Lorg/json/JSONObject;
    :cond_2
    :try_start_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/Meal;

    .line 143
    .local v1, meal:Lorg/medhelp/mydiet/model/Meal;
    new-instance v3, Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 144
    .end local v4           #object:Lorg/json/JSONObject;
    .restart local v3       #object:Lorg/json/JSONObject;
    :try_start_3
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-object v4, v3

    .end local v3           #object:Lorg/json/JSONObject;
    .restart local v4       #object:Lorg/json/JSONObject;
    goto :goto_1

    .line 146
    .end local v1           #meal:Lorg/medhelp/mydiet/model/Meal;
    .end local v4           #object:Lorg/json/JSONObject;
    .restart local v3       #object:Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 147
    .local v0, e:Ljava/lang/Exception;
    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 146
    .end local v0           #e:Ljava/lang/Exception;
    .end local v3           #object:Lorg/json/JSONObject;
    .restart local v4       #object:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4           #object:Lorg/json/JSONObject;
    .restart local v3       #object:Lorg/json/JSONObject;
    goto :goto_3
.end method

.method public static getNutrientWeightToCreateOrEditFood(ZZDDDD)D
    .locals 6
    .parameter "usePercent"
    .parameter "hasEquivalentUnitType"
    .parameter "servingAmount"
    .parameter "servingEquivalent"
    .parameter "nutrientValue"
    .parameter "nutrientDRI"

    .prologue
    .line 477
    invoke-static {p1, p2, p3, p4, p5}, Lorg/medhelp/mydiet/util/DataUtil;->getEquivalentMultiplierToCreateOrEditFood(ZDD)D

    move-result-wide v0

    .line 479
    .local v0, equivalentMultiplier:D
    move-wide v2, p6

    .line 480
    .local v2, weight:D
    if-eqz p0, :cond_0

    .line 481
    const-wide/high16 v4, 0x4059

    div-double v4, p6, v4

    mul-double v2, v4, p8

    .line 484
    :cond_0
    mul-double v4, v2, v0

    return-wide v4
.end method

.method public static getServingMultiplier(Lorg/medhelp/mydiet/model/FoodServing;DD)D
    .locals 5
    .parameter "foodServing"
    .parameter "equivalent"
    .parameter "servingAmount"

    .prologue
    const-wide/high16 v0, 0x3ff0

    const-wide/16 v3, 0x0

    .line 452
    if-nez p0, :cond_1

    .line 457
    :cond_0
    :goto_0
    return-wide v0

    .line 454
    :cond_1
    cmpg-double v2, p1, v3

    if-lez v2, :cond_0

    cmpg-double v2, p3, v3

    if-lez v2, :cond_0

    .line 457
    const-wide/high16 v0, 0x4059

    mul-double/2addr v0, p3

    div-double v0, p1, v0

    goto :goto_0
.end method

.method public static getWeightDifferenceInFourWeeks(DD)D
    .locals 4
    .parameter "bmr"
    .parameter "calorieInTake"

    .prologue
    .line 1011
    div-double v0, p2, p0

    const-wide/high16 v2, 0x403c

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static mergeFoodItems(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 10
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    .local p0, oldFoods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    .local p1, newFoods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 197
    const/4 p0, 0x0

    .line 220
    .end local p0           #oldFoods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    :cond_0
    :goto_0
    return-object p0

    .line 199
    .restart local p0       #oldFoods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    :cond_1
    if-nez p0, :cond_2

    move-object p0, p1

    goto :goto_0

    .line 200
    :cond_2
    if-eqz p1, :cond_0

    .line 201
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_3

    move-object p0, p1

    goto :goto_0

    .line 202
    :cond_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 204
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 205
    .local v0, food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const/4 v3, -0x1

    .line 206
    .local v3, position:I
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 207
    .local v2, oldFoodsSize:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_2
    if-lt v1, v2, :cond_4

    .line 213
    :goto_3
    if-ltz v3, :cond_6

    .line 214
    invoke-virtual {p0, v3, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 208
    :cond_4
    iget-wide v6, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v8, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    cmp-long v4, v6, v8

    if-nez v4, :cond_5

    iget-wide v6, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-ltz v4, :cond_5

    .line 209
    move v3, v1

    .line 210
    goto :goto_3

    .line 207
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 216
    :cond_6
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
