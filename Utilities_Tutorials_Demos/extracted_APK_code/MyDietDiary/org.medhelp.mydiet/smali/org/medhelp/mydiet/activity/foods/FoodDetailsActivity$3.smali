.class Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;
.super Ljava/lang/Object;
.source "FoodDetailsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->onClickServingsSize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

.field private final synthetic val$servingNames:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->val$servingNames:[Ljava/lang/String;

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .parameter "dialog"
    .parameter "item"

    .prologue
    .line 210
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->val$servingNames:[Ljava/lang/String;

    aget-object v1, v1, p2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 211
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v1

    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$3(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v0

    iget-object v0, v0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodServing;

    iget-wide v2, v0, Lorg/medhelp/mydiet/model/FoodServing;->medhelpId:J

    iput-wide v2, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 212
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v1

    .line 213
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$3(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v0

    iget-object v0, v0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodServing;

    .line 214
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v2

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 215
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$3(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v4

    iget-wide v4, v4, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    .line 212
    invoke-static {v0, v2, v3, v4, v5}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v2

    iput-wide v2, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 218
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 220
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->refreshViewContents()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$2(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)V

    .line 221
    return-void
.end method
