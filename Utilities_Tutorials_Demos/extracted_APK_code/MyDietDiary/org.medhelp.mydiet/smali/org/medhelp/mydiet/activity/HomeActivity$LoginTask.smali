.class Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;
.super Landroid/os/AsyncTask;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoginTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final CREATE_ANONYMOUS:I = 0xc8

.field private static final LOGIN_SUCCESS:I = 0x64

.field private static final START:I


# instance fields
.field private loginAttempts:I

.field final synthetic this$0:Lorg/medhelp/mydiet/activity/HomeActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/HomeActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 173
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 179
    const/4 v0, 0x0

    iput v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->loginAttempts:I

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/HomeActivity;Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;-><init>(Lorg/medhelp/mydiet/activity/HomeActivity;)V

    return-void
.end method

.method private login(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "username"
    .parameter "password"

    .prologue
    .line 222
    invoke-static {p1, p2}, Lorg/medhelp/mydiet/http/MHHttpHandler;->login(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->doInBackground([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .parameter "params"

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 189
    const/4 v1, 0x0

    .line 191
    .local v1, loggedIn:Z
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    iget v6, p0, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->loginAttempts:I

    const/4 v7, 0x5

    if-lt v6, v7, :cond_2

    .line 218
    :cond_1
    const/4 v6, 0x0

    return-object v6

    .line 192
    :cond_2
    iget v6, p0, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->loginAttempts:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->loginAttempts:I

    .line 193
    aget-object v6, p1, v10

    aget-object v7, p1, v9

    invoke-direct {p0, v6, v7}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->login(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 195
    .local v2, loginResult:Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 196
    const/4 v4, 0x0

    .line 199
    .local v4, resultObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 200
    .end local v4           #resultObject:Lorg/json/JSONObject;
    .local v5, resultObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v6, "response_code"

    const/4 v7, -0x1

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    .line 201
    .local v3, responseCode:I
    if-nez v3, :cond_3

    .line 202
    const/4 v1, 0x1

    .line 203
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Integer;

    const/4 v7, 0x0

    const/16 v8, 0x64

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v6}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 211
    .end local v3           #responseCode:I
    :catch_0
    move-exception v0

    move-object v4, v5

    .line 212
    .end local v5           #resultObject:Lorg/json/JSONObject;
    .local v0, e:Lorg/json/JSONException;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 204
    .end local v0           #e:Lorg/json/JSONException;
    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v3       #responseCode:I
    .restart local v5       #resultObject:Lorg/json/JSONObject;
    :cond_3
    if-ne v3, v9, :cond_0

    .line 205
    const/4 v6, 0x6

    :try_start_2
    iput v6, p0, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->loginAttempts:I

    .line 206
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v6}, Lorg/medhelp/mydiet/util/PreferenceUtil;->logout(Landroid/content/Context;)V

    .line 207
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Integer;

    const/4 v7, 0x0

    const/16 v8, 0xc8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v6}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 211
    .end local v3           #responseCode:I
    .end local v5           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 239
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 240
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->onPostExecute([Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute([Ljava/lang/String;)V
    .locals 0
    .parameter "result"

    .prologue
    .line 244
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 245
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 183
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 184
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->publishProgress([Ljava/lang/Object;)V

    .line 185
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 227
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 228
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 235
    :goto_0
    :sswitch_0
    return-void

    .line 232
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    #calls: Lorg/medhelp/mydiet/activity/HomeActivity;->startUserConfigsSync()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->access$0(Lorg/medhelp/mydiet/activity/HomeActivity;)V

    goto :goto_0

    .line 228
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
