.class public Lorg/medhelp/mydiet/activity/WelcomeActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "WelcomeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 32
    packed-switch p1, :pswitch_data_0

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 34
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 35
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/SetupScreen1;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WelcomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 36
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/WelcomeActivity;->finish()V

    goto :goto_0

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0xfa0
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 45
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 59
    :goto_0
    return-void

    .line 47
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xfa0

    invoke-virtual {p0, v0, v1}, Lorg/medhelp/mydiet/activity/WelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 50
    :pswitch_1
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->isSetupDone(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WelcomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 56
    :goto_1
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/WelcomeActivity;->finish()V

    goto :goto_0

    .line 53
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/SetupScreen1;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WelcomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 45
    :pswitch_data_0
    .packed-switch 0x7f0b0135
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 21
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const v0, 0x7f030036

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WelcomeActivity;->setContentView(I)V

    .line 24
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setLastLoginPromptTime(Landroid/content/Context;J)V

    .line 26
    const v0, 0x7f0b0135

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    const v0, 0x7f0b0136

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    return-void
.end method
