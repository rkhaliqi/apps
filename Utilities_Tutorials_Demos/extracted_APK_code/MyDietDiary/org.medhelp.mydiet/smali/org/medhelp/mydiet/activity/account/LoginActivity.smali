.class public Lorg/medhelp/mydiet/activity/account/LoginActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "LoginActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;,
        Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;,
        Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;
    }
.end annotation


# instance fields
.field loginButton:Landroid/widget/Button;

.field mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

.field mPasswordField:Landroid/widget/EditText;

.field mProgress:Landroid/widget/ProgressBar;

.field mUserField:Landroid/widget/EditText;

.field private startedFromSettings:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/account/LoginActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 126
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->syncUserSettings()V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/account/LoginActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 112
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->completeLogin()V

    return-void
.end method

.method private completeLogin()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 113
    iget-boolean v1, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->startedFromSettings:Z

    if-nez v1, :cond_0

    .line 114
    const-wide/16 v1, -0x1

    invoke-static {p0, v1, v2}, Lorg/medhelp/mydiet/util/SyncUtil;->setConfigsLastSyncTime(Landroid/content/Context;J)V

    .line 115
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_CLEAR:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 117
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 118
    .local v0, resultIntent:Landroid/content/Intent;
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 120
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->finish()V

    .line 124
    .end local v0           #resultIntent:Landroid/content/Intent;
    :goto_0
    return-void

    .line 122
    :cond_0
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->finish()V

    goto :goto_0
.end method

.method private onLoginClick()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 102
    const v1, 0x7f0b00d9

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 103
    .local v0, tv:Landroid/widget/TextView;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    invoke-static {p0}, Lorg/medhelp/mydiet/util/NetworkUtil;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lorg/medhelp/mydiet/util/NetworkUtil;->isNetworkRoaming(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    new-instance v1, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;-><init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mUserField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mPasswordField:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    const-string v1, "Network Unavailable"

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private populateFields()V
    .locals 3

    .prologue
    .line 69
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, userType:Ljava/lang/String;
    const-string v1, "medhelp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mUserField:Landroid/widget/EditText;

    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mPasswordField:Landroid/widget/EditText;

    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserPassword(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 75
    :cond_0
    return-void
.end method

.method private syncUserSettings()V
    .locals 4

    .prologue
    .line 127
    new-instance v0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;-><init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 128
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 91
    packed-switch p1, :pswitch_data_0

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 93
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 94
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->completeLogin()V

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0xfa1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 87
    :goto_0
    :pswitch_0
    return-void

    .line 81
    :pswitch_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->onLoginClick()V

    goto :goto_0

    .line 84
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/account/SignupActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xfa1

    invoke-virtual {p0, v0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b00d8
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 44
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v2, 0x7f030022

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->setContentView(I)V

    .line 47
    const v2, 0x7f0b00d8

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->loginButton:Landroid/widget/Button;

    .line 49
    const v2, 0x7f0b00d6

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mUserField:Landroid/widget/EditText;

    .line 50
    const v2, 0x7f0b00d7

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mPasswordField:Landroid/widget/EditText;

    .line 52
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 53
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 54
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "edit_preferences"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, editPreferences:Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "edit_preferences"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->startedFromSettings:Z

    .line 60
    .end local v0           #editPreferences:Ljava/lang/String;
    :cond_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->populateFields()V

    .line 62
    const v2, 0x7f0b0001

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 64
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->loginButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    const v2, 0x7f0b00da

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    return-void
.end method
