.class Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;
.super Landroid/os/AsyncTask;
.source "ExercisesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncUserDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x7d0


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 314
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 314
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V

    return-void
.end method

.method private syncExercises()V
    .locals 22

    .prologue
    .line 351
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncExercisesStatus(Landroid/content/Context;Z)V

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    #getter for: Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDate:Ljava/util/Date;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->access$0(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)Ljava/util/Date;

    move-result-object v2

    invoke-static {v2}, Lorg/medhelp/mydiet/util/DateUtil;->getSelectedMonthFirstDayMidnight(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v17

    .line 354
    .local v17, startCal:Ljava/util/Calendar;
    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v18

    .line 355
    .local v18, time:J
    new-instance v2, Ljava/util/Date;

    move-wide/from16 v0, v18

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v16

    .line 356
    .local v16, endCal:Ljava/util/Calendar;
    const/4 v2, 0x2

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 358
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 359
    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "month_year_timestamp=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v9

    const/4 v7, 0x0

    .line 358
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 361
    .local v15, c:Landroid/database/Cursor;
    const-wide/16 v7, -0x1

    .line 362
    .local v7, lastSyncTime:J
    if-eqz v15, :cond_0

    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 363
    const-string v2, "last_sync_exercises"

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 366
    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    .line 367
    .local v11, currentTime:J
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual/range {v16 .. v16}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static/range {v2 .. v8}, Lorg/medhelp/mydiet/http/MHHttpHandler;->requestSyncExercises(Landroid/content/Context;JJJ)Ljava/lang/String;

    move-result-object v10

    .line 369
    .local v10, response:Ljava/lang/String;
    invoke-static {v10}, Lorg/medhelp/mydiet/util/SyncUtil;->isResponseValid(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 370
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v13

    invoke-static/range {v9 .. v14}, Lorg/medhelp/mydiet/util/SyncUtil;->handleExerciseItemsSyncResponse(Landroid/content/Context;Ljava/lang/String;JJ)V

    .line 373
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncExercisesStatus(Landroid/content/Context;Z)V

    .line 374
    return-void
.end method

.method private syncUserData()V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    invoke-static {v0}, Lorg/medhelp/mydiet/util/SyncUtil;->getSyncExercisesStatus(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    invoke-static {v0}, Lorg/medhelp/mydiet/util/SyncUtil;->getExercisesLastSyncTime(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/SyncUtil;->isLastSyncTimeWithinThreshhold(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 346
    :cond_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->syncExercises()V

    .line 348
    :cond_1
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs declared-synchronized doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "params"

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->syncUserData()V

    .line 320
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x7d0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 340
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 341
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0
    .parameter "result"

    .prologue
    .line 335
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 336
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 326
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 327
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 331
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
