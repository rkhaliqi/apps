.class public Lorg/medhelp/mydiet/activity/TermsActivity;
.super Landroid/app/Activity;
.source "TermsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private sharedPreferences:Landroid/content/SharedPreferences;

.field private termsText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private getTerms()Ljava/lang/CharSequence;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/TermsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050003

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 45
    .local v1, inputStream:Ljava/io/InputStream;
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 46
    .local v3, reader:Ljava/io/BufferedReader;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .local v0, builder:Ljava/lang/StringBuilder;
    :goto_0
    :try_start_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .local v2, line:Ljava/lang/String;
    if-nez v2, :cond_0

    .line 54
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 56
    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    return-object v4

    .line 50
    :cond_0
    :try_start_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 53
    .end local v2           #line:Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 54
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 55
    throw v4
.end method

.method private startApp()V
    .locals 2

    .prologue
    .line 82
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/WelcomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/TermsActivity;->startActivity(Landroid/content/Intent;)V

    .line 83
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/TermsActivity;->finish()V

    .line 84
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    const/4 v3, 0x0

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 79
    :goto_0
    return-void

    .line 63
    :pswitch_0
    const-string v2, "mh_preferences"

    invoke-virtual {p0, v2, v3}, Lorg/medhelp/mydiet/activity/TermsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/TermsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 64
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TermsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 65
    .local v0, prefEditor1:Landroid/content/SharedPreferences$Editor;
    const-string v2, "terms_agreed"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 66
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 68
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TermsActivity;->startApp()V

    goto :goto_0

    .line 72
    .end local v0           #prefEditor1:Landroid/content/SharedPreferences$Editor;
    :pswitch_1
    const-string v2, "mh_preferences"

    invoke-virtual {p0, v2, v3}, Lorg/medhelp/mydiet/activity/TermsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/TermsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 73
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TermsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 74
    .local v1, prefEditor2:Landroid/content/SharedPreferences$Editor;
    const-string v2, "mh_preferences"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 75
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 76
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/TermsActivity;->finish()V

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x7f0b012c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v1, 0x7f030031

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/TermsActivity;->setContentView(I)V

    .line 31
    const v1, 0x7f0b012b

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/TermsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/TermsActivity;->termsText:Landroid/widget/TextView;

    .line 33
    :try_start_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TermsActivity;->termsText:Landroid/widget/TextView;

    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TermsActivity;->getTerms()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    const v1, 0x7f0b012c

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/TermsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    const v1, 0x7f0b012d

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/TermsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
