.class Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;
.super Landroid/os/AsyncTask;
.source "AddFoodActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveFoodTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
        "Ljava/lang/Integer;",
        "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 700
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 700
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->doInBackground([Lorg/medhelp/mydiet/model/FoodItemInMeal;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lorg/medhelp/mydiet/model/FoodItemInMeal;)Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .locals 6
    .parameter "params"

    .prologue
    const/4 v5, 0x0

    .line 713
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-virtual {v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 714
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    const/4 v1, 0x0

    .line 715
    .local v1, food:Lorg/medhelp/mydiet/model/Food;
    aget-object v4, p1, v5

    if-eqz v4, :cond_0

    aget-object v4, p1, v5

    iget-object v4, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodJSONString:Ljava/lang/String;

    if-eqz v4, :cond_0

    aget-object v4, p1, v5

    iget-object v4, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodJSONString:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 716
    aget-object v4, p1, v5

    iget-object v4, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodJSONString:Ljava/lang/String;

    invoke-static {v4}, Lorg/medhelp/mydiet/util/DataUtil;->getFood(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Food;

    move-result-object v1

    .line 717
    iput-boolean v5, v1, Lorg/medhelp/mydiet/model/Food;->isNewAccess:Z

    .line 719
    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->insertFood(Lorg/medhelp/mydiet/model/Food;)J

    move-result-wide v2

    .line 720
    .local v2, foodInAppId:J
    aget-object v4, p1, v5

    iput-wide v2, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    .line 722
    .end local v2           #foodInAppId:J
    :cond_0
    aget-object v4, p1, v5

    return-object v4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->onPostExecute(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    return-void
.end method

.method protected onPostExecute(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V
    .locals 5
    .parameter "result"

    .prologue
    .line 740
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->publishProgress([Ljava/lang/Object;)V

    .line 741
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const-class v3, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 742
    .local v1, fooddetailsIntent:Landroid/content/Intent;
    invoke-static {p1}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 743
    .local v0, foodKey:Ljava/lang/String;
    const-string v2, "transient_foods"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 744
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const/16 v3, 0x3eb

    invoke-virtual {v2, v1, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 745
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 746
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 707
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 708
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->publishProgress([Ljava/lang/Object;)V

    .line 709
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 727
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 728
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 736
    :goto_0
    return-void

    .line 730
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->showSavingFoodsDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$24(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    goto :goto_0

    .line 733
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->hideSavingFoodsDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$25(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    goto :goto_0

    .line 728
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
