.class Lorg/medhelp/mydiet/Splash$LoadDBTask;
.super Landroid/os/AsyncTask;
.source "Splash.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/Splash;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadDBTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/Splash;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/Splash;)V
    .locals 0
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Lorg/medhelp/mydiet/Splash$LoadDBTask;->this$0:Lorg/medhelp/mydiet/Splash;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/Splash;Lorg/medhelp/mydiet/Splash$LoadDBTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/Splash$LoadDBTask;-><init>(Lorg/medhelp/mydiet/Splash;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/Splash$LoadDBTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 3
    .parameter "params"

    .prologue
    .line 121
    iget-object v1, p0, Lorg/medhelp/mydiet/Splash$LoadDBTask;->this$0:Lorg/medhelp/mydiet/Splash;

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 122
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->initTables()J

    .line 124
    iget-object v1, p0, Lorg/medhelp/mydiet/Splash$LoadDBTask;->this$0:Lorg/medhelp/mydiet/Splash;

    invoke-static {v1}, Lorg/medhelp/mydiet/util/DBSetupUtil;->getOldDBVersion(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_0

    .line 125
    iget-object v1, p0, Lorg/medhelp/mydiet/Splash$LoadDBTask;->this$0:Lorg/medhelp/mydiet/Splash;

    invoke-static {v1}, Lorg/medhelp/mydiet/util/DBSetupUtil;->getExercisesMHKeyStatus(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 126
    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->updateExerciseKeysToMedhelpKeys()V

    .line 130
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/Splash$LoadDBTask;->this$0:Lorg/medhelp/mydiet/Splash;

    invoke-static {v1}, Lorg/medhelp/mydiet/util/DBSetupUtil;->isDBLoadedWithExercises(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 131
    iget-object v1, p0, Lorg/medhelp/mydiet/Splash$LoadDBTask;->this$0:Lorg/medhelp/mydiet/Splash;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/Splash;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->populateExercises(Landroid/content/Context;)V

    .line 134
    :cond_1
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/Splash$LoadDBTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 150
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/Splash$LoadDBTask;->publishProgress([Ljava/lang/Object;)V

    .line 151
    iget-object v0, p0, Lorg/medhelp/mydiet/Splash$LoadDBTask;->this$0:Lorg/medhelp/mydiet/Splash;

    #calls: Lorg/medhelp/mydiet/Splash;->showHome()V
    invoke-static {v0}, Lorg/medhelp/mydiet/Splash;->access$0(Lorg/medhelp/mydiet/Splash;)V

    .line 152
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 153
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 116
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/Splash$LoadDBTask;->publishProgress([Ljava/lang/Object;)V

    .line 117
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 139
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 140
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 146
    :pswitch_0
    return-void

    .line 140
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/Splash$LoadDBTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
