.class public Lorg/medhelp/mydiet/model/Meal;
.super Ljava/lang/Object;
.source "Meal.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/medhelp/mydiet/model/Meal;",
        ">;"
    }
.end annotation


# static fields
.field public static final BREAKFAST:I = 0x3

.field public static final DINNER:I = 0x1

.field public static final LUNCH:I = 0x2

.field public static final SNACK:I = 0x0

.field public static final TYPE_BREAKFAST:Ljava/lang/String; = "Breakfast"

.field public static final TYPE_DINNER:Ljava/lang/String; = "Dinner"

.field public static final TYPE_LUNCH:Ljava/lang/String; = "Lunch"

.field public static final TYPE_SNACK:Ljava/lang/String; = "Snack"


# instance fields
.field public foods:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation
.end field

.field public id:J

.field public mealTime:J

.field public mealType:I

.field public note:Ljava/lang/String;

.field public userInputCalories:D

.field public where:Ljava/lang/String;

.field public withWhom:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 39
    .local v0, now:J
    iput-wide v0, p0, Lorg/medhelp/mydiet/model/Meal;->id:J

    .line 40
    const/4 v2, 0x0

    iput v2, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    .line 41
    const-wide/high16 v2, -0x4010

    iput-wide v2, p0, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    .line 42
    iput-wide v0, p0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    .line 43
    return-void
.end method

.method public constructor <init>(JLjava/util/ArrayList;JDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "id"
    .parameter
    .parameter "mealTime"
    .parameter "userInputCalories"
    .parameter "note"
    .parameter "where"
    .parameter "withWhom"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;JD",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    .local p3, foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-wide p1, p0, Lorg/medhelp/mydiet/model/Meal;->id:J

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    .line 51
    iput-object p3, p0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    .line 52
    iput-wide p4, p0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    .line 53
    iput-wide p6, p0, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    .line 54
    iput-object p8, p0, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    .line 55
    iput-object p9, p0, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    .line 56
    iput-object p10, p0, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lorg/medhelp/mydiet/model/Meal;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/model/Meal;->compareTo(Lorg/medhelp/mydiet/model/Meal;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/medhelp/mydiet/model/Meal;)I
    .locals 9
    .parameter "otherMeal"

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 135
    const/4 v1, -0x1

    .line 136
    .local v1, BEFORE:I
    const/4 v2, 0x0

    .line 137
    .local v2, EQUAL:I
    const/4 v0, 0x1

    .line 139
    .local v0, AFTER:I
    iget v5, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    iget v6, p1, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    if-le v5, v6, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v3

    .line 141
    :cond_1
    iget v5, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    iget v6, p1, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    if-ge v5, v6, :cond_2

    move v3, v4

    .line 142
    goto :goto_0

    .line 144
    :cond_2
    iget-wide v5, p0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    iget-wide v7, p1, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    cmp-long v5, v5, v7

    if-ltz v5, :cond_0

    .line 146
    iget-wide v5, p0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    iget-wide v7, p1, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    cmp-long v3, v5, v7

    if-lez v3, :cond_3

    move v3, v4

    .line 147
    goto :goto_0

    .line 149
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCopy()Lorg/medhelp/mydiet/model/Meal;
    .locals 3

    .prologue
    .line 154
    new-instance v0, Lorg/medhelp/mydiet/model/Meal;

    invoke-direct {v0}, Lorg/medhelp/mydiet/model/Meal;-><init>()V

    .line 155
    .local v0, meal:Lorg/medhelp/mydiet/model/Meal;
    iget-wide v1, p0, Lorg/medhelp/mydiet/model/Meal;->id:J

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/Meal;->id:J

    .line 156
    iget v1, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    iput v1, v0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    .line 157
    iget-object v1, p0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    iput-object v1, v0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    .line 158
    iget-wide v1, p0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    .line 159
    iget-wide v1, p0, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    .line 160
    iget-object v1, p0, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    iput-object v1, v0, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    .line 161
    iget-object v1, p0, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    iput-object v1, v0, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    .line 162
    iget-object v1, p0, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;

    iput-object v1, v0, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;

    .line 164
    return-object v0
.end method

.method public getFoodTitles()Ljava/lang/String;
    .locals 4

    .prologue
    .line 83
    iget-object v2, p0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    .line 84
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 85
    .local v1, titles:Ljava/lang/StringBuffer;
    iget-object v2, p0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 93
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 95
    .end local v1           #titles:Ljava/lang/StringBuffer;
    :goto_1
    return-object v2

    .line 85
    .restart local v1       #titles:Ljava/lang/StringBuffer;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 86
    .local v0, food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-object v3, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 87
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 88
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 90
    :cond_2
    iget-object v3, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 95
    .end local v0           #food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v1           #titles:Ljava/lang/StringBuffer;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getMealCalories()D
    .locals 8

    .prologue
    const-wide/16 v3, 0x0

    .line 99
    const-wide/16 v0, 0x0

    .line 100
    .local v0, calories:D
    iget-object v5, p0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-wide v3

    .line 101
    :cond_1
    iget-object v5, p0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    move-wide v3, v0

    .line 106
    goto :goto_0

    .line 101
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 102
    .local v2, food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-wide v6, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    cmpl-double v6, v6, v3

    if-ltz v6, :cond_2

    .line 103
    iget-wide v6, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    add-double/2addr v0, v6

    goto :goto_1
.end method

.method public getMealTypeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    packed-switch v0, :pswitch_data_0

    .line 118
    const-string v0, "Snack"

    :goto_0
    return-object v0

    .line 112
    :pswitch_0
    const-string v0, "Breakfast"

    goto :goto_0

    .line 114
    :pswitch_1
    const-string v0, "Lunch"

    goto :goto_0

    .line 116
    :pswitch_2
    const-string v0, "Dinner"

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setMealType(Ljava/lang/String;)V
    .locals 1
    .parameter "mealTypeString"

    .prologue
    .line 122
    const-string v0, "Breakfast"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x3

    iput v0, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    .line 131
    :goto_0
    return-void

    .line 124
    :cond_0
    const-string v0, "Lunch"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    const/4 v0, 0x2

    iput v0, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    goto :goto_0

    .line 126
    :cond_1
    const-string v0, "Dinner"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 127
    const/4 v0, 0x1

    iput v0, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    goto :goto_0

    .line 129
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 61
    if-nez p0, :cond_0

    const/4 v2, 0x0

    .line 79
    :goto_0
    return-object v2

    .line 63
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 66
    .local v1, mealObject:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "id"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/Meal;->id:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 67
    const-string v2, "meal_type"

    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/Meal;->getMealTypeString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 68
    const-string v2, "foods"

    new-instance v3, Lorg/json/JSONArray;

    iget-object v4, p0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-static {v4}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemsInMealAsJSONString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 69
    const-string v2, "meal_time"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 70
    const-string v2, "where"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 71
    const-string v2, "with_whom"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 72
    const-string v2, "user_input_calories"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 73
    const-string v2, "calories"

    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/Meal;->getMealCalories()D

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 74
    const-string v2, "notes"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
