.class public Lorg/medhelp/mydiet/activity/AboutActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "AboutActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    .line 25
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 42
    :goto_0
    :pswitch_0
    return-void

    .line 27
    :pswitch_1
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 28
    .local v2, medhelpIntent:Landroid/content/Intent;
    const-string v3, "about"

    const-string v4, "medhelp"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 29
    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/AboutActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 32
    .end local v2           #medhelpIntent:Landroid/content/Intent;
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    .local v0, geIntent:Landroid/content/Intent;
    const-string v3, "about"

    const-string v4, "ge"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 34
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/AboutActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 37
    .end local v0           #geIntent:Landroid/content/Intent;
    :pswitch_3
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 38
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "market://search?q=pub:MedHelp"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 39
    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/AboutActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x7f0b0002
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 15
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    const/high16 v0, 0x7f03

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/AboutActivity;->setContentView(I)V

    .line 18
    const v0, 0x7f0b0002

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    const v0, 0x7f0b0004

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    const v0, 0x7f0b0005

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    return-void
.end method
