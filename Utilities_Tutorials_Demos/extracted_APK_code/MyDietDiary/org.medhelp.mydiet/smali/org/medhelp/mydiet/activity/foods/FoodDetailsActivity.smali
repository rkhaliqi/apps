.class public Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "FoodDetailsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lorg/medhelp/mydiet/C$jsonKeys;


# instance fields
.field private mDialog:Landroid/app/AlertDialog;

.field private mFood:Lorg/medhelp/mydiet/model/Food;

.field private mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

.field private mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Landroid/app/AlertDialog;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    return-object v0
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 242
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->refreshViewContents()V

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Lorg/medhelp/mydiet/model/FoodDetail;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    return-object v0
.end method

.method private initializeContent()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    .line 89
    invoke-static {p0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 90
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    invoke-virtual {v0, v1, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodWithInAppId(J)Lorg/medhelp/mydiet/model/Food;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFood:Lorg/medhelp/mydiet/model/Food;

    .line 92
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFood:Lorg/medhelp/mydiet/model/Food;

    if-nez v1, :cond_1

    .line 93
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    invoke-virtual {v0, v1, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodWithMedHelpId(J)Lorg/medhelp/mydiet/model/Food;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFood:Lorg/medhelp/mydiet/model/Food;

    .line 96
    :cond_1
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFood:Lorg/medhelp/mydiet/model/Food;

    iget-object v1, v1, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFood:Lorg/medhelp/mydiet/model/Food;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    invoke-static {v1, v2}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodDetail(Ljava/lang/String;Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    .line 98
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    if-nez v1, :cond_3

    .line 99
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    invoke-virtual {v0, v1, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->deleteFood(J)I

    .line 100
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->finish()V

    .line 115
    :cond_2
    :goto_0
    return-void

    .line 104
    :cond_3
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v1, v1, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v1, v1, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 105
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v1, v1, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 107
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    .line 108
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v1, v1, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/FoodServing;

    iget-wide v3, v1, Lorg/medhelp/mydiet/model/FoodServing;->id:J

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 112
    :cond_4
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-nez v1, :cond_2

    .line 113
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    const-wide/high16 v2, 0x3ff0

    iput-wide v2, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    goto :goto_0
.end method

.method private onClickDone()V
    .locals 4

    .prologue
    .line 231
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 232
    .local v1, resultIntent:Landroid/content/Intent;
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    .line 234
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/model/FoodItemInMeal;->toString()Ljava/lang/String;

    .line 236
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-static {v2}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, foodKey:Ljava/lang/String;
    const-string v2, "transient_foods"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->setResult(ILandroid/content/Intent;)V

    .line 239
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->finish()V

    .line 240
    return-void
.end method

.method private onClickNumberOfServings()V
    .locals 10

    .prologue
    .line 140
    move-object v1, p0

    .line 141
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 142
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v7, "layout_inflater"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 144
    .local v4, inflater:Landroid/view/LayoutInflater;
    const v8, 0x7f03000a

    const v7, 0x7f0b0019

    invoke-virtual {p0, v7}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v4, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 145
    .local v5, layout:Landroid/view/View;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 146
    const-string v7, "Number of Servings"

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 148
    const v7, 0x7f0b0018

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 150
    .local v3, etServings:Landroid/widget/EditText;
    new-instance v7, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$1;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$1;-><init>(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)V

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 159
    new-instance v6, Ljava/text/DecimalFormatSymbols;

    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v6, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 160
    .local v6, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v6, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 161
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v7, "0.##"

    invoke-direct {v2, v7, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 163
    .local v2, df:Ljava/text/DecimalFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v8, v8, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    invoke-virtual {v2, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 165
    const-string v7, "OK"

    new-instance v8, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$2;

    invoke-direct {v8, p0, v3}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$2;-><init>(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 183
    const-string v7, "Cancel"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 184
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mDialog:Landroid/app/AlertDialog;

    .line 185
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 186
    return-void
.end method

.method private onClickServingsSize()V
    .locals 9

    .prologue
    .line 189
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 190
    :cond_0
    const-string v5, "Food Servings are not available for this food at this time."

    const/4 v6, 0x0

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 228
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 196
    .local v4, servingsSize:I
    const/4 v2, 0x0

    .line 197
    .local v2, selectedServing:I
    new-array v3, v4, [Ljava/lang/String;

    .line 199
    .local v3, servingNames:[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    if-lt v1, v4, :cond_2

    .line 206
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 207
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v5, "Pick a serving size"

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 208
    new-instance v5, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;

    invoke-direct {v5, p0, v3}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$3;-><init>(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;[Ljava/lang/String;)V

    invoke-virtual {v0, v3, v2, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 224
    const-string v5, "Cancel"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 226
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mDialog:Landroid/app/AlertDialog;

    .line 227
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 200
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :cond_2
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/medhelp/mydiet/model/FoodServing;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    aput-object v5, v3, v1

    .line 201
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/medhelp/mydiet/model/FoodServing;

    iget-wide v5, v5, Lorg/medhelp/mydiet/model/FoodServing;->medhelpId:J

    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v7, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    cmp-long v5, v5, v7

    if-nez v5, :cond_3

    .line 202
    move v2, v1

    .line 199
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private declared-synchronized refreshViewContents()V
    .locals 15

    .prologue
    .line 243
    monitor-enter p0

    const/4 v12, 0x0

    .line 245
    .local v12, tv:Landroid/widget/TextView;
    :try_start_0
    new-instance v11, Ljava/text/DecimalFormatSymbols;

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "en"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v11, v2}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 246
    .local v11, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v2, 0x2e

    invoke-virtual {v11, v2}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 247
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v2, "#.#"

    invoke-direct {v7, v2, v11}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 248
    .local v7, df1:Ljava/text/DecimalFormat;
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v2, "0.00"

    invoke-direct {v8, v2, v11}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 250
    .local v8, df2:Ljava/text/DecimalFormat;
    const v2, 0x7f0b000c

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 251
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    const v2, 0x7f0b007e

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 254
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    invoke-virtual {v7, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    const v2, 0x7f0b0080

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 257
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v9, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 258
    .local v9, servingId:J
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFood:Lorg/medhelp/mydiet/model/Food;

    invoke-virtual {v2, v9, v10}, Lorg/medhelp/mydiet/model/Food;->getFoodServing(J)Lorg/medhelp/mydiet/model/FoodServing;

    move-result-object v1

    .line 260
    .local v1, selectedServing:Lorg/medhelp/mydiet/model/FoodServing;
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 400
    :goto_0
    monitor-exit p0

    return-void

    .line 264
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v6, v2, Lorg/medhelp/mydiet/model/FoodDetail;->brand:Ljava/lang/String;

    .line 265
    .local v6, foodSummary:Ljava/lang/String;
    if-eqz v6, :cond_1

    const-string v2, "null"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 266
    :cond_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v6, v2, Lorg/medhelp/mydiet/model/FoodDetail;->manufacturer:Ljava/lang/String;

    .line 268
    :cond_2
    if-eqz v6, :cond_3

    const-string v2, "null"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 269
    :cond_3
    const-string v6, ""

    .line 272
    :cond_4
    if-eqz v1, :cond_5

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v1, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    :goto_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 280
    iget-object v13, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 282
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->caloriesInFood:D

    .line 283
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v4, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 280
    invoke-static/range {v1 .. v6}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemSummary(Lorg/medhelp/mydiet/model/FoodServing;DDLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 287
    const v2, 0x7f0b0083

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 288
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v4, v4, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    invoke-static {v1, v2, v3, v4, v5}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v2

    invoke-virtual {v8, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    const v2, 0x7f0b0084

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 291
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->totalFat:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    const v2, 0x7f0b0085

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 294
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->saturatedFat:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    const v2, 0x7f0b0086

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 297
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->transFat:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    const v2, 0x7f0b0087

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 300
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->monoUnsaturatedFat:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    const v2, 0x7f0b0088

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 303
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->cholesterol:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    const v2, 0x7f0b0089

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 306
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->sodium:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    const v2, 0x7f0b008a

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 309
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->potassium:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    const v2, 0x7f0b008b

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 312
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->totalCarbohydrate:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    const v2, 0x7f0b008c

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 315
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->dietaryFiber:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    const v2, 0x7f0b008d

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 318
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->sugars:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    const v2, 0x7f0b008e

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 321
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->protein:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    const v2, 0x7f0b008f

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 324
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminA:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "IU"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    const v2, 0x7f0b0090

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 327
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminC:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 329
    const v2, 0x7f0b0091

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 330
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminD:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "IU"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 332
    const v2, 0x7f0b0092

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 333
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminE:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    const v2, 0x7f0b0093

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 336
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminK:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mcg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    const v2, 0x7f0b0094

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 339
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->thiamin:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    const v2, 0x7f0b0095

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 342
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->riboflavin:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 344
    const v2, 0x7f0b0096

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 345
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->niacin:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 347
    const v2, 0x7f0b0097

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 348
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB6:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 350
    const v2, 0x7f0b0098

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 351
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->folate:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mcg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    const v2, 0x7f0b0099

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 354
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB12:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 356
    const v2, 0x7f0b009a

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 357
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->biotin:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mcg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    const v2, 0x7f0b009b

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 360
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->pantothenicAcid:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    const v2, 0x7f0b009c

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 363
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->calcium:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    const v2, 0x7f0b009d

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 366
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->iron:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    const v2, 0x7f0b009e

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 369
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->iodine:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mcg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    const v2, 0x7f0b009f

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 372
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->magnesium:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    const v2, 0x7f0b00a0

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 375
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->zinc:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    const v2, 0x7f0b00a1

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 378
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->selenium:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mcg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 380
    const v2, 0x7f0b00a2

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 381
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->copper:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    const v2, 0x7f0b00a3

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 384
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->manganese:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 386
    const v2, 0x7f0b00a4

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 387
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->chromium:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mcg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 389
    const v2, 0x7f0b00a5

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 390
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->molybdenum:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mcg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    const v2, 0x7f0b00a6

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 393
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->fluoride:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mcg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    const v2, 0x7f0b00a7

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 396
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->phosphorus:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 398
    const v2, 0x7f0b00a8

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v12, v0

    .line 399
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v13, v5, Lorg/medhelp/mydiet/model/FoodDetail;->chloride:D

    invoke-static {v1, v3, v4, v13, v14}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "g"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 243
    .end local v1           #selectedServing:Lorg/medhelp/mydiet/model/FoodServing;
    .end local v6           #foodSummary:Ljava/lang/String;
    .end local v7           #df1:Ljava/text/DecimalFormat;
    .end local v8           #df2:Ljava/text/DecimalFormat;
    .end local v9           #servingId:J
    .end local v11           #symbols:Ljava/text/DecimalFormatSymbols;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 275
    .restart local v1       #selectedServing:Lorg/medhelp/mydiet/model/FoodServing;
    .restart local v6       #foodSummary:Ljava/lang/String;
    .restart local v7       #df1:Ljava/text/DecimalFormat;
    .restart local v8       #df2:Ljava/text/DecimalFormat;
    .restart local v9       #servingId:J
    .restart local v11       #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_5
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/FoodDetail;->equivalentUnitType:Ljava/lang/String;

    const-string v4, "volume"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "mL"

    :goto_2
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_6
    const-string v2, "g"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 137
    :goto_0
    :pswitch_0
    monitor-exit p0

    return-void

    .line 128
    :pswitch_1
    :try_start_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->onClickNumberOfServings()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 131
    :pswitch_2
    :try_start_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->onClickServingsSize()V

    goto :goto_0

    .line 134
    :pswitch_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->onClickDone()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b007d
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter "savedInstanceState"

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v5, 0x7f030015

    invoke-virtual {p0, v5}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->setContentView(I)V

    .line 53
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020027

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 54
    .local v2, tileBitMap:Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v3, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 55
    .local v3, tiledBitMap:Landroid/graphics/drawable/BitmapDrawable;
    sget-object v5, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    sget-object v6, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v3, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 57
    const v5, 0x7f0b0082

    invoke-virtual {p0, v5}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 58
    .local v0, llNutrients:Landroid/widget/LinearLayout;
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 60
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "transient_foods"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 62
    .local v4, transientFoodsKey:Ljava/lang/String;
    invoke-static {v4}, Lorg/medhelp/mydiet/model/TransientDataStore;->retrieveData(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 64
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 65
    const-string v5, "food_item"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemInMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v1

    .line 67
    .end local v1           #o:Ljava/lang/Object;
    :cond_0
    check-cast v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 69
    const v5, 0x7f0b007d

    invoke-virtual {p0, v5}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v5, 0x7f0b007f

    invoke-virtual {p0, v5}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    const v5, 0x7f0b0081

    invoke-virtual {p0, v5}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->initializeContent()V

    .line 74
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->refreshViewContents()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit p0

    return-void

    .line 50
    .end local v0           #llNutrients:Landroid/widget/LinearLayout;
    .end local v2           #tileBitMap:Landroid/graphics/Bitmap;
    .end local v3           #tiledBitMap:Landroid/graphics/drawable/BitmapDrawable;
    .end local v4           #transientFoodsKey:Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 121
    :cond_0
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 122
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 79
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 80
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 84
    const-string v0, "food_item"

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/FoodItemInMeal;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 86
    return-void
.end method
