.class public Lorg/medhelp/mydiet/http/MHHttpClient;
.super Lorg/apache/http/impl/client/DefaultHttpClient;
.source "MHHttpClient.java"


# static fields
.field private static mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    return-void
.end method

.method private constructor <init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V
    .locals 0
    .parameter "conman"
    .parameter "params"

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 16
    return-void
.end method

.method public static getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;
    .locals 5

    .prologue
    .line 21
    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    if-nez v2, :cond_0

    .line 22
    new-instance v2, Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-direct {v2}, Lorg/medhelp/mydiet/http/MHHttpClient;-><init>()V

    sput-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 23
    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/http/MHHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    .line 24
    .local v0, mgr:Lorg/apache/http/conn/ClientConnectionManager;
    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/http/MHHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    .line 26
    .local v1, params:Lorg/apache/http/params/HttpParams;
    new-instance v2, Lorg/medhelp/mydiet/http/MHHttpClient;

    new-instance v3, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .line 27
    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 26
    invoke-direct {v2, v3, v1}, Lorg/medhelp/mydiet/http/MHHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    sput-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 28
    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/http/MHHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.useragent"

    const-string v4, "My Diet Diary/1.2.2 Android"

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 30
    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 32
    .end local v0           #mgr:Lorg/apache/http/conn/ClientConnectionManager;
    .end local v1           #params:Lorg/apache/http/params/HttpParams;
    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    goto :goto_0
.end method

.method public static resetMHHttpClient()V
    .locals 5

    .prologue
    .line 37
    new-instance v2, Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-direct {v2}, Lorg/medhelp/mydiet/http/MHHttpClient;-><init>()V

    sput-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 38
    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/http/MHHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    .line 39
    .local v0, mgr:Lorg/apache/http/conn/ClientConnectionManager;
    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/http/MHHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    .line 41
    .local v1, params:Lorg/apache/http/params/HttpParams;
    new-instance v2, Lorg/medhelp/mydiet/http/MHHttpClient;

    new-instance v3, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .line 42
    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 41
    invoke-direct {v2, v3, v1}, Lorg/medhelp/mydiet/http/MHHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    sput-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 43
    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpClient;->mhHttpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/http/MHHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.useragent"

    const-string v4, "My Diet Diary/1.2.2 Android"

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 44
    return-void
.end method
