.class public Lorg/medhelp/mydiet/activity/WebViewSimple;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "WebViewSimple.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;
    }
.end annotation


# instance fields
.field mWebView:Landroid/webkit/WebView;

.field progressLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x1

    .line 20
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    const v0, 0x7f030033

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WebViewSimple;->setContentView(I)V

    .line 23
    const v0, 0x7f0b012f

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WebViewSimple;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple;->progressLayout:Landroid/widget/LinearLayout;

    .line 25
    const v0, 0x7f0b0131

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WebViewSimple;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple;->mWebView:Landroid/webkit/WebView;

    .line 26
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 27
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;-><init>(Lorg/medhelp/mydiet/activity/WebViewSimple;Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 28
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple;->mWebView:Landroid/webkit/WebView;

    const/high16 v1, 0x200

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    .line 29
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 30
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple;->mWebView:Landroid/webkit/WebView;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->FORUMS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public declared-synchronized onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 54
    monitor-enter p0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    const/4 v0, 0x1

    .line 58
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-super {p0, p1, p2}, Lorg/medhelp/mydiet/activity/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
