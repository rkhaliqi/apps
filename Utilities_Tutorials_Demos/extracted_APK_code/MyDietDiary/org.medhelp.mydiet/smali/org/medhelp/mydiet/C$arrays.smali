.class public interface abstract Lorg/medhelp/mydiet/C$arrays;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "arrays"
.end annotation


# static fields
.field public static final activityLevels:[Ljava/lang/String;

.field public static final femaleAvatarPositionOnMedHelp:[I

.field public static final femaleAvatars:[I

.field public static final maleAvatarPositionOnMedHelp:[I

.field public static final maleAvatars:[I

.field public static final servingNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xc

    .line 747
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 748
    const-string v1, "Sedentary"

    aput-object v1, v0, v4

    .line 749
    const-string v1, "Lightly Active"

    aput-object v1, v0, v5

    .line 750
    const-string v1, "Moderately Active"

    aput-object v1, v0, v6

    .line 751
    const-string v1, "Very Active"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    .line 752
    const-string v2, "Extremely Active"

    aput-object v2, v0, v1

    .line 747
    sput-object v0, Lorg/medhelp/mydiet/C$arrays;->activityLevels:[Ljava/lang/String;

    .line 754
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/medhelp/mydiet/C$arrays;->maleAvatars:[I

    .line 769
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/medhelp/mydiet/C$arrays;->femaleAvatars:[I

    .line 784
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, Lorg/medhelp/mydiet/C$arrays;->maleAvatarPositionOnMedHelp:[I

    .line 799
    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, Lorg/medhelp/mydiet/C$arrays;->femaleAvatarPositionOnMedHelp:[I

    .line 814
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/String;

    .line 815
    const-string v1, "cubic inch"

    aput-object v1, v0, v4

    .line 816
    const-string v1, "cup"

    aput-object v1, v0, v5

    .line 817
    const-string v1, "cup, chopped"

    aput-object v1, v0, v6

    .line 818
    const-string v1, "cup, chopped or diced"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    .line 819
    const-string v2, "cup, cubes"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "cup, diced"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 820
    const-string v2, "cup, shredded"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 821
    const-string v2, "cup, sliced"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 822
    const-string v2, "fillet"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 823
    const-string v2, "fl oz"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 824
    const-string v2, "item"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 825
    const-string v2, "jar"

    aput-object v2, v0, v1

    .line 826
    const-string v1, "large"

    aput-object v1, v0, v3

    const/16 v1, 0xd

    .line 827
    const-string v2, "lb"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 828
    const-string v2, "medium"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 829
    const-string v2, "oz"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 830
    const-string v2, "package"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 831
    const-string v2, "packet"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 832
    const-string v2, "patty"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 833
    const-string v2, "pie"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 834
    const-string v2, "piece"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 835
    const-string v2, "serving"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 836
    const-string v2, "slice"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 837
    const-string v2, "small"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 838
    const-string v2, "steak"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 839
    const-string v2, "tbsp"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 840
    const-string v2, "tsp"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 841
    const-string v2, "other"

    aput-object v2, v0, v1

    .line 814
    sput-object v0, Lorg/medhelp/mydiet/C$arrays;->servingNames:[Ljava/lang/String;

    .line 746
    return-void

    .line 754
    nop

    :array_0
    .array-data 0x4
        0x0t 0x0t 0x2t 0x7ft
        0x4t 0x0t 0x2t 0x7ft
        0x5t 0x0t 0x2t 0x7ft
        0x6t 0x0t 0x2t 0x7ft
        0x7t 0x0t 0x2t 0x7ft
        0x8t 0x0t 0x2t 0x7ft
        0x9t 0x0t 0x2t 0x7ft
        0xat 0x0t 0x2t 0x7ft
        0xbt 0x0t 0x2t 0x7ft
        0x1t 0x0t 0x2t 0x7ft
        0x2t 0x0t 0x2t 0x7ft
        0x3t 0x0t 0x2t 0x7ft
    .end array-data

    .line 769
    :array_1
    .array-data 0x4
        0xct 0x0t 0x2t 0x7ft
        0x10t 0x0t 0x2t 0x7ft
        0x11t 0x0t 0x2t 0x7ft
        0x12t 0x0t 0x2t 0x7ft
        0x13t 0x0t 0x2t 0x7ft
        0x14t 0x0t 0x2t 0x7ft
        0x15t 0x0t 0x2t 0x7ft
        0x16t 0x0t 0x2t 0x7ft
        0x17t 0x0t 0x2t 0x7ft
        0xdt 0x0t 0x2t 0x7ft
        0xet 0x0t 0x2t 0x7ft
        0xft 0x0t 0x2t 0x7ft
    .end array-data

    .line 784
    :array_2
    .array-data 0x4
        0x14t 0x0t 0x0t 0x0t
        0xbt 0x0t 0x0t 0x0t
        0xct 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0xet 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0x10t 0x0t 0x0t 0x0t
        0x11t 0x0t 0x0t 0x0t
        0x12t 0x0t 0x0t 0x0t
        0x13t 0x0t 0x0t 0x0t
        0x15t 0x0t 0x0t 0x0t
        0x16t 0x0t 0x0t 0x0t
    .end array-data

    .line 799
    :array_3
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
        0xat 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
        0xfft 0xfft 0xfft 0xfft
    .end array-data
.end method
