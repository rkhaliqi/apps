.class public Lorg/medhelp/mydiet/model/FoodDetail;
.super Ljava/lang/Object;
.source "FoodDetail.java"

# interfaces
.implements Lorg/medhelp/mydiet/C$jsonKeys;


# instance fields
.field public amount:D

.field public biotin:D

.field public brand:Ljava/lang/String;

.field public calcium:D

.field public calories:D

.field public caloriesFromFat:D

.field public chloride:D

.field public cholesterol:D

.field public chromium:D

.field public copper:D

.field public createdAt:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public dietaryFiber:D

.field public equivalentUnitType:Ljava/lang/String;

.field public fluoride:D

.field public folate:D

.field public foodGroupId:I

.field public foodName:Ljava/lang/String;

.field public id:J

.field public inAppId:J

.field public iodine:D

.field public iron:D

.field public magnesium:D

.field public manganese:D

.field public manufacturer:Ljava/lang/String;

.field public medhelpId:J

.field public molybdenum:D

.field public monoUnsaturatedFat:D

.field public niacin:D

.field public pantothenicAcid:D

.field public phosphorus:D

.field public potassium:D

.field public protein:D

.field public riboflavin:D

.field public saturatedFat:D

.field public selenium:D

.field public servingId:I

.field public servings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodServing;",
            ">;"
        }
    .end annotation
.end field

.field public sodium:D

.field public status:Ljava/lang/String;

.field public sugars:D

.field public thiamin:D

.field public totalCarbohydrate:D

.field public totalFat:D

.field public transFat:D

.field public vitaminA:D

.field public vitaminB12:D

.field public vitaminB6:D

.field public vitaminC:D

.field public vitaminD:D

.field public vitaminE:D

.field public vitaminK:D

.field public zinc:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFoodJSON()Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 127
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 129
    .local v1, food:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "name"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->foodName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 130
    const-string v2, "manufacturer"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->manufacturer:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 131
    const-string v2, "food_group_id"

    iget v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->foodGroupId:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 132
    const-string v2, "equivalent_unit_type"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->equivalentUnitType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 133
    const-string v2, "calories"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 134
    const-string v2, "calories_from_fat"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->caloriesFromFat:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 135
    const-string v2, "total_fat"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->totalFat:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 136
    const-string v2, "saturated_fatty_acids"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->saturatedFat:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 137
    const-string v2, "total_trans_fatty_acids"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->transFat:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 138
    const-string v2, "total_monounsaturated_fatty_acids"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->monoUnsaturatedFat:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 139
    const-string v2, "cholesterol"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->cholesterol:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 140
    const-string v2, "sodium"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->sodium:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 141
    const-string v2, "potassium"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->potassium:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 142
    const-string v2, "carbohydrate"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->totalCarbohydrate:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 143
    const-string v2, "fiber"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->dietaryFiber:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 144
    const-string v2, "total_sugars"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->sugars:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 145
    const-string v2, "protein"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->protein:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 146
    const-string v2, "vitamin_a"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminA:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 147
    const-string v2, "vitamin_c"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminC:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 148
    const-string v2, "vitamin_d"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminD:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 149
    const-string v2, "vitamin_e"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminE:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 150
    const-string v2, "vitamin_k"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminK:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 151
    const-string v2, "thiamin"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->thiamin:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 152
    const-string v2, "riboflavin"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->riboflavin:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 153
    const-string v2, "niacin"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->niacin:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 154
    const-string v2, "vitamin_b6"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB6:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 155
    const-string v2, "folate"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->folate:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 156
    const-string v2, "vitamin_b12"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB12:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 157
    const-string v2, "biotin"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->biotin:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 158
    const-string v2, "pantothenic_acid"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->pantothenicAcid:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 159
    const-string v2, "calcium"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->calcium:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 160
    const-string v2, "iron"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->iron:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 161
    const-string v2, "iodine"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->iodine:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 162
    const-string v2, "magnesium"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->magnesium:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 163
    const-string v2, "zinc"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->zinc:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 164
    const-string v2, "selenium"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->selenium:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 165
    const-string v2, "copper"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->copper:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 166
    const-string v2, "manganese"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->manganese:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 167
    const-string v2, "chromium"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->chromium:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 168
    const-string v2, "molybdenum"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->molybdenum:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 169
    const-string v2, "fluoride"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->fluoride:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 170
    const-string v2, "phosphorus"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->phosphorus:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 171
    const-string v2, "chloride"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->chloride:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 173
    const-string v2, "external_id"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->inAppId:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_0
    return-object v1

    .line 174
    :catch_0
    move-exception v0

    .line 175
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFoodServingToCreateFood()Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 111
    iget-object v2, p0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 112
    :cond_0
    const/4 v1, 0x0

    .line 123
    :goto_0
    return-object v1

    .line 114
    :cond_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 116
    .local v1, foodServing:Lorg/json/JSONObject;
    :try_start_0
    const-string v3, "name"

    iget-object v2, p0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/FoodServing;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 117
    const-string v3, "equivalent"

    iget-object v2, p0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/FoodServing;

    iget-wide v4, v2, Lorg/medhelp/mydiet/model/FoodServing;->equivalent:D

    invoke-virtual {v1, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 118
    const-string v3, "amount"

    iget-object v2, p0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/FoodServing;

    iget-wide v4, v2, Lorg/medhelp/mydiet/model/FoodServing;->amount:D

    invoke-virtual {v1, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 119
    const-string v2, "external_id"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFoodServings()Lorg/json/JSONArray;
    .locals 4

    .prologue
    .line 88
    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    const/4 v1, 0x0

    .line 95
    :cond_1
    return-object v1

    .line 90
    :cond_2
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 91
    .local v1, servingsArray:Lorg/json/JSONArray;
    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 92
    .local v2, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 93
    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/medhelp/mydiet/model/FoodServing;

    invoke-virtual {v3}, Lorg/medhelp/mydiet/model/FoodServing;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getFoodWithServingsAsFromSearch()Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 75
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 76
    .local v2, foodWithServings:Lorg/json/JSONObject;
    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodJSON()Lorg/json/JSONObject;

    move-result-object v1

    .line 79
    .local v1, food:Lorg/json/JSONObject;
    :try_start_0
    const-string v3, "food"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 80
    const-string v3, "food_servings"

    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodServings()Lorg/json/JSONArray;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-object v2

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFoodWithServingsToCreateOrEdit()Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 99
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 100
    .local v2, foodWithServings:Lorg/json/JSONObject;
    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodJSON()Lorg/json/JSONObject;

    move-result-object v1

    .line 102
    .local v1, food:Lorg/json/JSONObject;
    :try_start_0
    const-string v3, "food"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 103
    const-string v3, "food_serving"

    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodServingToCreateFood()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    return-object v2

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
