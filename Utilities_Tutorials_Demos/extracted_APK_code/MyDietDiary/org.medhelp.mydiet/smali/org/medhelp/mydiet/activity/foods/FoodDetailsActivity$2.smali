.class Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$2;
.super Ljava/lang/Object;
.source "FoodDetailsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->onClickNumberOfServings()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

.field private final synthetic val$etServings:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$2;->val$etServings:Landroid/widget/EditText;

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 169
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, amount:F
    new-instance v2, Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$2;->val$etServings:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 172
    .local v1, amountString:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 173
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 175
    :cond_0
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 176
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->mFoodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v2

    float-to-double v3, v0

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 178
    :cond_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->refreshViewContents()V
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;->access$2(Lorg/medhelp/mydiet/activity/foods/FoodDetailsActivity;)V

    .line 181
    .end local v0           #amount:F
    .end local v1           #amountString:Ljava/lang/String;
    :cond_2
    return-void
.end method
