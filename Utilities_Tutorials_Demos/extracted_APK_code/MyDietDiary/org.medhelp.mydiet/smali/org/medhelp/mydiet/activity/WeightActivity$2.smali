.class Lorg/medhelp/mydiet/activity/WeightActivity$2;
.super Ljava/lang/Object;
.source "WeightActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/WeightActivity;->onUpdateClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

.field private final synthetic val$editor:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/WeightActivity;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->val$editor:Landroid/widget/EditText;

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v6, 0x1

    .line 161
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    #setter for: Lorg/medhelp/mydiet/activity/WeightActivity;->hasEdits:Z
    invoke-static {v3, v6}, Lorg/medhelp/mydiet/activity/WeightActivity;->access$5(Lorg/medhelp/mydiet/activity/WeightActivity;Z)V

    .line 163
    const-wide/16 v0, 0x0

    .line 164
    .local v0, weight:D
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->val$editor:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 165
    .local v2, weightString:Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v6, :cond_0

    .line 166
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "0"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 169
    :cond_0
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "kg"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 170
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    const-wide v4, 0x4001a3112f275febL

    mul-double/2addr v4, v0

    iput-wide v4, v3, Lorg/medhelp/mydiet/activity/WeightActivity;->mWeight:D

    .line 175
    :goto_0
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    iget-object v3, v3, Lorg/medhelp/mydiet/activity/WeightActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 176
    new-instance v3, Lorg/medhelp/mydiet/activity/WeightActivity$UpdateWeightTask;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lorg/medhelp/mydiet/activity/WeightActivity$UpdateWeightTask;-><init>(Lorg/medhelp/mydiet/activity/WeightActivity;Lorg/medhelp/mydiet/activity/WeightActivity$UpdateWeightTask;)V

    new-array v4, v6, [Ljava/lang/Long;

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    iget-object v6, v6, Lorg/medhelp/mydiet/activity/WeightActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/activity/WeightActivity$UpdateWeightTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 177
    return-void

    .line 172
    :cond_1
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    iput-wide v0, v3, Lorg/medhelp/mydiet/activity/WeightActivity;->mWeight:D

    goto :goto_0
.end method
