.class public interface abstract Lorg/medhelp/mydiet/C$extras;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "extras"
.end annotation


# static fields
.field public static final ABOUT:Ljava/lang/String; = "about"

.field public static final ADD_MORE_FOODS:Ljava/lang/String; = "add_more_foods"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final EDIT_PREFERENCES:Ljava/lang/String; = "edit_preferences"

.field public static final FOOD_AMOUNT:Ljava/lang/String; = "amount"

.field public static final FOOD_SERVING_ID:Ljava/lang/String; = "serving_id"

.field public static final MEALS_WHERE:Ljava/lang/String; = "where"

.field public static final MEALS_WITH_WHOM:Ljava/lang/String; = "with_whom"

.field public static final MEAL_ID:Ljava/lang/String; = "meal_id"

.field public static final MEAL_TYPE:Ljava/lang/String; = "meal_type"

.field public static final NOTES:Ljava/lang/String; = "notes"

.field public static final PASSWORD:Ljava/lang/String; = "password"

.field public static final TRANSIENT_EI_KEY:Ljava/lang/String; = "transient_exercise_item"

.field public static final TRANSIENT_FOODS_KEY:Ljava/lang/String; = "transient_foods"

.field public static final TRANSIENT_MEAL_KEY:Ljava/lang/String; = "transient_meal"

.field public static final USER_ID:Ljava/lang/String; = "user_id"

.field public static final USER_NAME:Ljava/lang/String; = "username"

.field public static final VALUE_GE:Ljava/lang/String; = "ge"

.field public static final VALUE_MEDHELP:Ljava/lang/String; = "medhelp"
