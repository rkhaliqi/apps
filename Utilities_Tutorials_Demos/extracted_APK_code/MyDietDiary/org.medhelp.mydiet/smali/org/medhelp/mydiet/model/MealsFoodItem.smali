.class public Lorg/medhelp/mydiet/model/MealsFoodItem;
.super Ljava/lang/Object;
.source "MealsFoodItem.java"


# instance fields
.field public amount:D

.field public foodName:Ljava/lang/String;

.field public foodSummary:Ljava/lang/String;

.field public id:J

.field public servingId:J


# direct methods
.method public constructor <init>(JJDLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "id"
    .parameter "servingId"
    .parameter "amount"
    .parameter "foodName"
    .parameter "foodSummary"

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-wide p1, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->id:J

    .line 19
    iput-wide p3, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->servingId:J

    .line 20
    iput-wide p5, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->amount:D

    .line 21
    iput-object p7, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->foodName:Ljava/lang/String;

    .line 22
    iput-object p8, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->foodSummary:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public toJSONString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 27
    .local v0, foodItem:Lorg/json/JSONObject;
    if-eqz p0, :cond_0

    .line 29
    :try_start_0
    const-string v1, "food_id"

    iget-wide v2, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->id:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 30
    const-string v1, "serving_id"

    iget-wide v2, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->servingId:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 31
    const-string v1, "amount"

    iget-wide v2, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->amount:D

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 32
    const-string v1, "food_name"

    iget-object v2, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->foodName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 33
    const-string v1, "food_summary"

    iget-object v2, p0, Lorg/medhelp/mydiet/model/MealsFoodItem;->foodSummary:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 41
    :goto_1
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 35
    :catch_0
    move-exception v1

    goto :goto_0
.end method
