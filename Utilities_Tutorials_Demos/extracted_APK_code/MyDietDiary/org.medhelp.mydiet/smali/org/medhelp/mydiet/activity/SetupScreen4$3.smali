.class Lorg/medhelp/mydiet/activity/SetupScreen4$3;
.super Ljava/lang/Object;
.source "SetupScreen4.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/SetupScreen4;->clickGoals()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/SetupScreen4;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$3;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 321
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$3;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/activity/SetupScreen4;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v0, v1, p2

    .line 322
    .local v0, units:Ljava/lang/String;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$3;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    invoke-static {v1, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGoal(Landroid/content/Context;Ljava/lang/String;)V

    .line 323
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$3;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    #calls: Lorg/medhelp/mydiet/activity/SetupScreen4;->refreshViewContents()V
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/SetupScreen4;->access$5(Lorg/medhelp/mydiet/activity/SetupScreen4;)V

    .line 324
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$3;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    iget-object v1, v1, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 325
    return-void
.end method
