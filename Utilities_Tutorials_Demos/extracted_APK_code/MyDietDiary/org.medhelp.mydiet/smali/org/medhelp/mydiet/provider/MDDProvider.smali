.class public Lorg/medhelp/mydiet/provider/MDDProvider;
.super Landroid/content/ContentProvider;
.source "MDDProvider.java"

# interfaces
.implements Lorg/medhelp/mydiet/C$provider;
.implements Lorg/medhelp/mydiet/C$data;


# static fields
.field private static final DAY_EXERCISE_ITEMS:I = 0xd3

.field private static final DAY_MEALS:I = 0xb

.field private static final DAY_WATER:I = 0x1f5

.field private static final DAY_WEIGHT:I = 0x259

.field private static final EXERCISES:I = 0xc9

.field public static final EXERCISES_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.dirvnd.org.medhelp.mydiet.exercises"

.field private static final EXERCISE_BY_ID:I = 0xca

.field private static final EXERCISE_BY_MH_ID:I = 0xcb

.field public static final EXERCISE_ENTRY_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.itemvnd.org.medhelp.mydiet.exercises"

.field private static final EXERCISE_ITEMS:I = 0xd2

.field public static final EXERCISE_ITEMS_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.dirvnd.org.medhelp.mydiet.exerciseitems"

.field private static final EXERCISE_ITEMS_TO_SYNC:I = 0xd4

.field public static final EXERCISE_ITEM_ENTRY_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.itemvnd.org.medhelp.mydiet.exerciseitems"

.field private static final FOODS:I = 0x64

.field private static final FOODS_FREQUENT:I = 0x68

.field public static final FOODS_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.dirvnd.org.medhelp.mydiet.foods"

.field private static final FOODS_RECENT:I = 0x67

.field private static final FOOD_BY_ID:I = 0x65

.field private static final FOOD_BY_MH_ID:I = 0x66

.field public static final FOOD_ENTRY_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.itemvnd.org.medhelp.mydiet.foods"

.field private static final MEALS:I = 0xa

.field public static final MEAL_ENTRY_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.itemvnd.org.medhelp.mydiet.meals"

.field private static final SYNC_CLEAR:I = 0x270e

.field public static final SYNC_DIR_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.dirvnd.org.medhelp.mydiet.sync"

.field public static final SYNC_ENTRY_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.itemvnd.org.medhelp.mydiet.sync"

.field private static final SYNC_STATUS:I = 0x270f

.field public static final URI_DAY_EXERCISE_ITEMS:Landroid/net/Uri; = null

.field public static final URI_DAY_MEALS:Landroid/net/Uri; = null

.field public static final URI_DAY_WATER:Landroid/net/Uri; = null

.field public static final URI_DAY_WEIGHT:Landroid/net/Uri; = null

.field public static final URI_EXERCISES:Landroid/net/Uri; = null

.field public static final URI_EXERCISE_BY_ID:Landroid/net/Uri; = null

.field public static final URI_EXERCISE_BY_MH_ID:Landroid/net/Uri; = null

.field public static final URI_EXERCISE_ITEMS:Landroid/net/Uri; = null

.field public static final URI_EXERCISE_ITEMS_TO_SYNC:Landroid/net/Uri; = null

.field public static final URI_FOODS:Landroid/net/Uri; = null

.field public static final URI_FOODS_FREQUENT:Landroid/net/Uri; = null

.field public static final URI_FOODS_RECENT:Landroid/net/Uri; = null

.field public static final URI_FOOD_BY_ID:Landroid/net/Uri; = null

.field public static final URI_FOOD_BY_MH_ID:Landroid/net/Uri; = null

.field public static final URI_MEALS:Landroid/net/Uri; = null

.field public static final URI_SYNC_CLEAR:Landroid/net/Uri; = null

.field public static final URI_SYNC_STATUS:Landroid/net/Uri; = null

.field public static final URI_WATER:Landroid/net/Uri; = null

.field public static final URI_WEIGHT:Landroid/net/Uri; = null

.field private static final WATER:I = 0x1f4

.field public static final WATER_ENTRY_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.itemvnd.org.medhelp.mydiet.water"

.field public static final WATER_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.dirvnd.org.medhelp.mydiet.water"

.field private static final WEIGHT:I = 0x258

.field public static final WEIGHT_ENTRY_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.itemvnd.org.medhelp.mydiet.weight"

.field public static final WEIGHT_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.dirvnd.org.medhelp.mydiet.weight"

.field private static final sURIMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/meals"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_MEALS:Landroid/net/Uri;

    .line 84
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/meals/date"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_DAY_MEALS:Landroid/net/Uri;

    .line 86
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/foods/_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOOD_BY_ID:Landroid/net/Uri;

    .line 87
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/foods/medhelp_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOOD_BY_MH_ID:Landroid/net/Uri;

    .line 88
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/foods/recent"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS_RECENT:Landroid/net/Uri;

    .line 89
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/foods/frequent"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS_FREQUENT:Landroid/net/Uri;

    .line 90
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/foods"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    .line 92
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/exercises"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISES:Landroid/net/Uri;

    .line 93
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/exercises/_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISE_BY_ID:Landroid/net/Uri;

    .line 94
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/exercises/medhelp_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISE_BY_MH_ID:Landroid/net/Uri;

    .line 95
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/exercise_items"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISE_ITEMS:Landroid/net/Uri;

    .line 96
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/exercise_items/date"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_DAY_EXERCISE_ITEMS:Landroid/net/Uri;

    .line 97
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/exercise_items/sync"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISE_ITEMS_TO_SYNC:Landroid/net/Uri;

    .line 99
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/water"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WATER:Landroid/net/Uri;

    .line 100
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/water/date"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_DAY_WATER:Landroid/net/Uri;

    .line 102
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/weight"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WEIGHT:Landroid/net/Uri;

    .line 103
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/weight/date"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_DAY_WEIGHT:Landroid/net/Uri;

    .line 105
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/sync_status"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    .line 106
    const-string v0, "content://org.medhelp.mydiet.provider.mddprovider/sync_status/clear"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_CLEAR:Landroid/net/Uri;

    .line 135
    invoke-static {}, Lorg/medhelp/mydiet/provider/MDDProvider;->buildUriMatcher()Landroid/content/UriMatcher;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->sURIMatcher:Landroid/content/UriMatcher;

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private static buildUriMatcher()Landroid/content/UriMatcher;
    .locals 6

    .prologue
    const/16 v5, 0x68

    const/16 v4, 0x67

    .line 141
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 143
    .local v0, uriMatcher:Landroid/content/UriMatcher;
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "meals/date/*"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 144
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "meals"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 146
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "foods/_id/#"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 147
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "foods/medhelp_id/#"

    const/16 v3, 0x66

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 148
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "foods/recent"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 149
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "foods/frequent"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 150
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "foods/recent?like=*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 151
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "foods/frequent?like=*"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 152
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "foods"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 154
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "exercises"

    const/16 v3, 0xc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 155
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "exercises/_id/#"

    const/16 v3, 0xca

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 156
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "exercises/medhelp_id/#"

    const/16 v3, 0xcb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 158
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "exercise_items"

    const/16 v3, 0xd2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 159
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "exercise_items/date/*"

    const/16 v3, 0xd3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 160
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "exercise_items/sync"

    const/16 v3, 0xd4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 162
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "water/date/*"

    const/16 v3, 0x1f5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 163
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "water"

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 165
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "weight/date/*"

    const/16 v3, 0x259

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 166
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "weight"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 168
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "sync_status/clear"

    const/16 v3, 0x270e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 169
    const-string v1, "org.medhelp.mydiet.provider.mddprovider"

    const-string v2, "sync_status"

    const/16 v3, 0x270f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 171
    return-object v0
.end method


# virtual methods
.method public createOrUpdateExerciseItem(Lorg/medhelp/mydiet/model/ExerciseItem;)J
    .locals 16
    .parameter "exerciseItem"

    .prologue
    .line 687
    if-nez p1, :cond_1

    const-wide/16 v14, -0x1

    .line 728
    :cond_0
    :goto_0
    return-wide v14

    .line 689
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 690
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "exercise_items"

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 692
    .local v9, c:Landroid/database/Cursor;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 693
    .local v11, cv:Landroid/content/ContentValues;
    const-string v2, "date"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 694
    move-object/from16 v0, p1

    iget-wide v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    .line 695
    move-object/from16 v0, p1

    iget-wide v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    move-object/from16 v0, p1

    iput-wide v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    .line 697
    :cond_2
    const-string v2, "medhelp_id"

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseMHKey:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    const-string v2, "name"

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    if-eqz v2, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_6

    .line 700
    :cond_3
    const-string v2, "exercise_type"

    const-string v3, ""

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    :goto_1
    const-string v2, "distance"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 705
    const-string v2, "speed"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 706
    const-string v2, "time"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 707
    const-string v2, "calories"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 708
    const-string v2, "time_period"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 709
    const-string v2, "last_updated_time"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 711
    const-wide/16 v14, -0x1

    .line 712
    .local v14, returnValue:J
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_7

    .line 715
    :cond_4
    const-string v2, "exercise_items"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v12

    .line 716
    .local v12, id:J
    move-wide v14, v12

    .line 724
    .end local v12           #id:J
    :cond_5
    :goto_2
    if-eqz v9, :cond_0

    .line 725
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 702
    .end local v14           #returnValue:J
    :cond_6
    const-string v2, "exercise_type"

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 718
    .restart local v14       #returnValue:J
    :cond_7
    const-string v2, "exercise_items"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v11, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 719
    .local v10, count:I
    if-lez v10, :cond_5

    .line 720
    move-object/from16 v0, p1

    iget-wide v14, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    goto :goto_2
.end method

.method public createOrUpdateMeal(Lorg/medhelp/mydiet/model/Meal;Ljava/util/Date;)J
    .locals 21
    .parameter "meal"
    .parameter "date"

    .prologue
    .line 466
    if-nez p1, :cond_1

    const-wide/16 v14, 0x0

    .line 549
    :cond_0
    :goto_0
    return-wide v14

    .line 468
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v3}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 469
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "meals"

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "date = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 471
    .local v10, c:Landroid/database/Cursor;
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 472
    .local v12, cv:Landroid/content/ContentValues;
    const-string v3, "date"

    invoke-virtual/range {p2 .. p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 473
    const-string v3, "last_updated_time"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 475
    const-wide/16 v14, 0x0

    .line 477
    .local v14, id:J
    if-eqz v10, :cond_2

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_4

    .line 479
    :cond_2
    move-object/from16 v0, p1

    iget v3, v0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    packed-switch v3, :pswitch_data_0

    .line 503
    :goto_1
    const-string v3, "meals"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v12}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v14

    .line 545
    :cond_3
    :goto_2
    if-eqz v10, :cond_0

    .line 546
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 481
    :pswitch_0
    const-string v3, "breakfast"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 484
    :pswitch_1
    const-string v3, "lunch"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 487
    :pswitch_2
    const-string v3, "dinner"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 490
    :pswitch_3
    new-instance v19, Lorg/json/JSONArray;

    invoke-direct/range {v19 .. v19}, Lorg/json/JSONArray;-><init>()V

    .line 491
    .local v19, mealsArray:Lorg/json/JSONArray;
    const/16 v16, 0x0

    .line 494
    .local v16, mealObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v17, Lorg/json/JSONObject;

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    .end local v16           #mealObject:Lorg/json/JSONObject;
    .local v17, mealObject:Lorg/json/JSONObject;
    :try_start_1
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v16, v17

    .line 500
    .end local v17           #mealObject:Lorg/json/JSONObject;
    .restart local v16       #mealObject:Lorg/json/JSONObject;
    :goto_3
    const-string v3, "snacks"

    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 496
    :catch_0
    move-exception v13

    .line 497
    .local v13, e:Lorg/json/JSONException;
    :goto_4
    invoke-virtual {v13}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    .line 506
    .end local v13           #e:Lorg/json/JSONException;
    .end local v16           #mealObject:Lorg/json/JSONObject;
    .end local v19           #mealsArray:Lorg/json/JSONArray;
    :cond_4
    const-string v3, "_id"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 508
    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/Meal;->id:J

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v3, v4, v1}, Lorg/medhelp/mydiet/provider/MDDProvider;->deleteMeal(JLjava/util/Date;)V

    .line 510
    move-object/from16 v0, p1

    iget v3, v0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    packed-switch v3, :pswitch_data_1

    .line 539
    :goto_5
    const-string v3, "meals"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "date = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v12, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    .line 541
    .local v11, count:I
    const/4 v3, 0x1

    if-eq v11, v3, :cond_3

    .line 542
    const-wide/16 v14, 0x0

    goto/16 :goto_2

    .line 512
    .end local v11           #count:I
    :pswitch_4
    const-string v3, "breakfast"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 515
    :pswitch_5
    const-string v3, "lunch"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 518
    :pswitch_6
    const-string v3, "dinner"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 521
    :pswitch_7
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 522
    const-string v3, "meals"

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "date = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 524
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 525
    const-string v3, "snacks"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 527
    .local v20, oldSnacksJSON:Ljava/lang/String;
    const/16 v18, 0x0

    .line 529
    .local v18, meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    if-eqz v20, :cond_5

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_5

    .line 530
    invoke-static/range {v20 .. v20}, Lorg/medhelp/mydiet/util/DataUtil;->getMeals(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v18

    .line 532
    :cond_5
    if-nez v18, :cond_6

    new-instance v18, Ljava/util/ArrayList;

    .end local v18           #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 533
    .restart local v18       #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_6
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 535
    const-string v3, "snacks"

    invoke-static/range {v18 .. v18}, Lorg/medhelp/mydiet/util/DataUtil;->getMealsAsJSONArrayString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 496
    .end local v18           #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    .end local v20           #oldSnacksJSON:Ljava/lang/String;
    .restart local v17       #mealObject:Lorg/json/JSONObject;
    .restart local v19       #mealsArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v13

    move-object/from16 v16, v17

    .end local v17           #mealObject:Lorg/json/JSONObject;
    .restart local v16       #mealObject:Lorg/json/JSONObject;
    goto/16 :goto_4

    .line 479
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 510
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 15
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    .line 176
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 177
    .local v3, lastPathSegment:Ljava/lang/String;
    iget-object v10, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v10}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 179
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v10, Lorg/medhelp/mydiet/provider/MDDProvider;->sURIMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    .line 225
    const/4 v10, 0x0

    :goto_0
    return v10

    .line 181
    :sswitch_0
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_0

    .line 182
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 183
    .local v4, mealsDate:J
    const-string v10, "meals"

    const-string v11, "date=?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v2, v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    goto :goto_0

    .line 185
    .end local v4           #mealsDate:J
    :cond_0
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Invalid Uri. Valid date is required to delete meals "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 190
    :sswitch_1
    const/4 v10, 0x0

    goto :goto_0

    .line 193
    :sswitch_2
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Invalid Uri. Unsupported at this time "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 197
    :sswitch_3
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Invalid Uri. Unsupported at this time "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 201
    :sswitch_4
    if-eqz p2, :cond_1

    move-object/from16 v0, p3

    array-length v10, v0

    if-lez v10, :cond_1

    .line 202
    const-string v10, "exercise_items"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v2, v10, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_0

    .line 204
    :cond_1
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Invalid Uri. Date, selection and selectionArgs are required to delete exercise items "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 205
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 204
    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 208
    :sswitch_5
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_2

    .line 209
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 210
    .local v6, waterDate:J
    const-string v10, "water"

    const-string v11, "date=?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v2, v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_0

    .line 212
    .end local v6           #waterDate:J
    :cond_2
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Invalid Uri. Valid date is required to delete water "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 216
    :sswitch_6
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_3

    .line 217
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 218
    .local v8, weightDate:J
    const-string v10, "weight"

    const-string v11, "date=?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v2, v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_0

    .line 220
    .end local v8           #weightDate:J
    :cond_3
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Invalid Uri. Valid date is required to delete weight "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 223
    :sswitch_7
    const-string v10, "sync_tracker"

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v2, v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_0

    .line 179
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_1
        0xc9 -> :sswitch_2
        0xca -> :sswitch_3
        0xcb -> :sswitch_3
        0xd2 -> :sswitch_4
        0x1f5 -> :sswitch_5
        0x259 -> :sswitch_6
        0x270e -> :sswitch_7
    .end sparse-switch
.end method

.method public deleteMeal(JLjava/util/Date;)V
    .locals 18
    .parameter "mealId"
    .parameter "date"

    .prologue
    .line 553
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 554
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "meals"

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "date = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p3 .. p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 556
    .local v9, c:Landroid/database/Cursor;
    if-eqz v9, :cond_8

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 557
    const/4 v10, 0x0

    .line 559
    .local v10, cv:Landroid/content/ContentValues;
    const-string v2, "breakfast"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 560
    .local v14, mealString:Ljava/lang/String;
    const/4 v13, 0x0

    .line 561
    .local v13, meal:Lorg/medhelp/mydiet/model/Meal;
    if-eqz v14, :cond_1

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 562
    invoke-static {v14}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v13

    .line 563
    iget-wide v2, v13, Lorg/medhelp/mydiet/model/Meal;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    .line 564
    if-nez v10, :cond_0

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 565
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_0
    const-string v2, "breakfast"

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 569
    :cond_1
    const-string v2, "lunch"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 570
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 571
    invoke-static {v14}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v13

    .line 572
    iget-wide v2, v13, Lorg/medhelp/mydiet/model/Meal;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_3

    .line 573
    if-nez v10, :cond_2

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 574
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_2
    const-string v2, "lunch"

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 578
    :cond_3
    const-string v2, "dinner"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 579
    if-eqz v14, :cond_5

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 580
    invoke-static {v14}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v13

    .line 581
    iget-wide v2, v13, Lorg/medhelp/mydiet/model/Meal;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_5

    .line 582
    if-nez v10, :cond_4

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 583
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_4
    const-string v2, "dinner"

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 587
    :cond_5
    const-string v2, "snacks"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 588
    if-eqz v14, :cond_7

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_7

    .line 589
    invoke-static {v14}, Lorg/medhelp/mydiet/util/DataUtil;->getMeals(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    .line 591
    .local v17, snacksList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    if-eqz v17, :cond_7

    .line 592
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 593
    .local v15, size:I
    move v12, v15

    .line 595
    .local v12, initialSize:I
    const/4 v11, 0x0

    .local v11, i:I
    :goto_0
    if-lt v11, v15, :cond_a

    .line 603
    if-ge v15, v12, :cond_7

    .line 604
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_c

    .line 605
    if-nez v10, :cond_6

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 606
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_6
    const-string v2, "snacks"

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 616
    .end local v11           #i:I
    .end local v12           #initialSize:I
    .end local v15           #size:I
    .end local v17           #snacksList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_7
    :goto_1
    if-eqz v10, :cond_8

    .line 617
    const-string v2, "last_updated_time"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 618
    const-string v2, "meals"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p3 .. p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v10, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 622
    .end local v10           #cv:Landroid/content/ContentValues;
    .end local v13           #meal:Lorg/medhelp/mydiet/model/Meal;
    .end local v14           #mealString:Ljava/lang/String;
    :cond_8
    if-eqz v9, :cond_9

    .line 623
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 625
    :cond_9
    return-void

    .line 596
    .restart local v10       #cv:Landroid/content/ContentValues;
    .restart local v11       #i:I
    .restart local v12       #initialSize:I
    .restart local v13       #meal:Lorg/medhelp/mydiet/model/Meal;
    .restart local v14       #mealString:Ljava/lang/String;
    .restart local v15       #size:I
    .restart local v17       #snacksList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_a
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/Meal;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/Meal;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_b

    .line 597
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 598
    add-int/lit8 v11, v11, -0x1

    .line 599
    add-int/lit8 v15, v15, -0x1

    .line 595
    :cond_b
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 608
    :cond_c
    invoke-static/range {v17 .. v17}, Lorg/medhelp/mydiet/util/DataUtil;->getMealsAsJSONArrayString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v16

    .line 609
    .local v16, snacksJSON:Ljava/lang/String;
    if-nez v10, :cond_d

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 610
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_d
    const-string v2, "snacks"

    move-object/from16 v0, v16

    invoke-virtual {v10, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getFoodCursorWithInAppId(J)Landroid/database/Cursor;
    .locals 8
    .parameter "id"

    .prologue
    const/4 v2, 0x0

    .line 737
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 738
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "foods"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public getFoodCursorWithMedHelpID(J)Landroid/database/Cursor;
    .locals 8
    .parameter "id"

    .prologue
    const/4 v2, 0x0

    .line 732
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 733
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "foods"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "medhelp_id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public getFrequentFoods(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .parameter "like"

    .prologue
    const/4 v2, 0x0

    .line 759
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 760
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .local v3, selection:Ljava/lang/String;
    move-object v4, v2

    .line 761
    check-cast v4, [Ljava/lang/String;

    .line 762
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v8, 0x1

    .line 764
    .local v8, MIN_ACCESS_COUNT:I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 765
    :cond_0
    const-string p1, ""

    .line 766
    const-string v3, "access_count >= \'1\'"

    .line 772
    :goto_0
    const-string v1, "foods"

    const-string v7, "access_count DESC"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    .line 768
    :cond_1
    const-string v3, "name LIKE ? AND access_count >= \'1\'"

    .line 769
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    .end local v4           #selectionArgs:[Ljava/lang/String;
    const/4 v1, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "%"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .restart local v4       #selectionArgs:[Ljava/lang/String;
    goto :goto_0
.end method

.method public getMealsCursor(Ljava/util/Date;)Landroid/database/Cursor;
    .locals 8
    .parameter "date"

    .prologue
    const/4 v2, 0x0

    .line 776
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 777
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "meals"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public getRecentFoods(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .parameter "like"

    .prologue
    const/4 v2, 0x0

    .line 742
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 743
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .local v3, selection:Ljava/lang/String;
    move-object v4, v2

    .line 744
    check-cast v4, [Ljava/lang/String;

    .line 745
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v8, 0x1

    .line 747
    .local v8, MIN_ACCESS_COUNT:I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 748
    :cond_0
    const-string p1, ""

    .line 749
    const-string v3, "last_accessed_time>=\'0\' AND access_count >= \'1\'"

    .line 755
    :goto_0
    const-string v1, "foods"

    const-string v7, "last_accessed_time DESC"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    .line 751
    :cond_1
    const-string v3, "name LIKE ? AND last_accessed_time>=\'0\' AND access_count >= \'1\'"

    .line 752
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    .end local v4           #selectionArgs:[Ljava/lang/String;
    const/4 v1, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "%"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .restart local v4       #selectionArgs:[Ljava/lang/String;
    goto :goto_0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .parameter "uri"

    .prologue
    .line 230
    sget-object v0, Lorg/medhelp/mydiet/provider/MDDProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 272
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :sswitch_0
    const-string v0, "vnd.android.cursor.itemvnd.org.medhelp.mydiet.meals"

    .line 269
    :goto_0
    return-object v0

    .line 236
    :sswitch_1
    const-string v0, "vnd.android.cursor.itemvnd.org.medhelp.mydiet.foods"

    goto :goto_0

    .line 240
    :sswitch_2
    const-string v0, "vnd.android.cursor.dirvnd.org.medhelp.mydiet.foods"

    goto :goto_0

    .line 243
    :sswitch_3
    const-string v0, "vnd.android.cursor.dirvnd.org.medhelp.mydiet.exercises"

    goto :goto_0

    .line 247
    :sswitch_4
    const-string v0, "vnd.android.cursor.itemvnd.org.medhelp.mydiet.exercises"

    goto :goto_0

    .line 251
    :sswitch_5
    const-string v0, "vnd.android.cursor.dirvnd.org.medhelp.mydiet.exerciseitems"

    goto :goto_0

    .line 254
    :sswitch_6
    const-string v0, "vnd.android.cursor.dirvnd.org.medhelp.mydiet.water"

    goto :goto_0

    .line 257
    :sswitch_7
    const-string v0, "vnd.android.cursor.itemvnd.org.medhelp.mydiet.water"

    goto :goto_0

    .line 260
    :sswitch_8
    const-string v0, "vnd.android.cursor.dirvnd.org.medhelp.mydiet.weight"

    goto :goto_0

    .line 263
    :sswitch_9
    const-string v0, "vnd.android.cursor.itemvnd.org.medhelp.mydiet.weight"

    goto :goto_0

    .line 266
    :sswitch_a
    const-string v0, "vnd.android.cursor.dirvnd.org.medhelp.mydiet.sync"

    goto :goto_0

    .line 269
    :sswitch_b
    const-string v0, "vnd.android.cursor.dirvnd.org.medhelp.mydiet.sync"

    goto :goto_0

    .line 230
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_1
        0x67 -> :sswitch_2
        0x68 -> :sswitch_2
        0xc9 -> :sswitch_3
        0xca -> :sswitch_4
        0xcb -> :sswitch_4
        0xd3 -> :sswitch_5
        0xd4 -> :sswitch_5
        0x1f4 -> :sswitch_6
        0x1f5 -> :sswitch_7
        0x258 -> :sswitch_8
        0x259 -> :sswitch_9
        0x270e -> :sswitch_a
        0x270f -> :sswitch_b
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 24
    .parameter "uri"
    .parameter "values"

    .prologue
    .line 283
    const-wide/16 v8, 0x0

    .line 284
    .local v8, id:J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 286
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v20, "last_updated_time"

    new-instance v21, Ljava/util/Date;

    invoke-direct/range {v21 .. v21}, Ljava/util/Date;-><init>()V

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->getTime()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 288
    sget-object v20, Lorg/medhelp/mydiet/provider/MDDProvider;->sURIMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v20

    sparse-switch v20, :sswitch_data_0

    .line 360
    new-instance v20, Ljava/lang/IllegalArgumentException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Inserting using Unknown URI : "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 290
    :sswitch_0
    const-string v20, "meal"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 291
    .local v11, mealString:Ljava/lang/String;
    invoke-static {v11}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v10

    .line 292
    .local v10, meal:Lorg/medhelp/mydiet/model/Meal;
    new-instance v3, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 293
    .local v3, date:Ljava/util/Date;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v3}, Lorg/medhelp/mydiet/provider/MDDProvider;->createOrUpdateMeal(Lorg/medhelp/mydiet/model/Meal;Ljava/util/Date;)J

    move-result-wide v8

    .line 295
    const-wide/16 v20, 0x0

    cmp-long v20, v8, v20

    if-lez v20, :cond_0

    .line 296
    sget-object v20, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_MEALS:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    .line 297
    .local v12, mealsUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lorg/medhelp/mydiet/provider/MDDProvider;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v12, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 354
    .end local v3           #date:Ljava/util/Date;
    .end local v10           #meal:Lorg/medhelp/mydiet/model/Meal;
    .end local v11           #mealString:Ljava/lang/String;
    .end local v12           #mealsUri:Landroid/net/Uri;
    :goto_0
    return-object v12

    .line 301
    .restart local v3       #date:Ljava/util/Date;
    .restart local v10       #meal:Lorg/medhelp/mydiet/model/Meal;
    .restart local v11       #mealString:Ljava/lang/String;
    :cond_0
    new-instance v20, Landroid/database/SQLException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Failed to insert meal "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 305
    .end local v3           #date:Ljava/util/Date;
    .end local v10           #meal:Lorg/medhelp/mydiet/model/Meal;
    .end local v11           #mealString:Ljava/lang/String;
    :sswitch_1
    const-string v20, "food"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 306
    .local v6, foodJSON:Ljava/lang/String;
    invoke-static {v6}, Lorg/medhelp/mydiet/util/DataUtil;->getFood(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Food;

    move-result-object v5

    .line 307
    .local v5, food:Lorg/medhelp/mydiet/model/Food;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/medhelp/mydiet/provider/MDDProvider;->insertFood(Lorg/medhelp/mydiet/model/Food;)J

    move-result-wide v8

    .line 309
    const-wide/16 v20, 0x0

    cmp-long v20, v8, v20

    if-lez v20, :cond_1

    .line 310
    sget-object v20, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    .line 311
    .local v7, foodUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lorg/medhelp/mydiet/provider/MDDProvider;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move-object v12, v7

    .line 313
    goto :goto_0

    .line 315
    .end local v7           #foodUri:Landroid/net/Uri;
    :cond_1
    new-instance v20, Landroid/database/SQLException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Failed to insert food "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 319
    .end local v5           #food:Lorg/medhelp/mydiet/model/Food;
    .end local v6           #foodJSON:Ljava/lang/String;
    :sswitch_2
    const/4 v12, 0x0

    goto :goto_0

    .line 322
    :sswitch_3
    const-string v20, "exercise_items"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p2

    invoke-virtual {v4, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 324
    const-wide/16 v20, 0x0

    cmp-long v20, v8, v20

    if-lez v20, :cond_2

    .line 325
    sget-object v20, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISE_BY_ID:Landroid/net/Uri;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    goto/16 :goto_0

    .line 327
    :cond_2
    new-instance v20, Landroid/database/SQLException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Failed to insert Exercise Item with uri "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 331
    :sswitch_4
    const-string v20, "water"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p2

    invoke-virtual {v4, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 332
    const-string v20, "date"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 333
    .local v14, waterDate:J
    const-wide/16 v20, 0x0

    cmp-long v20, v8, v20

    if-lez v20, :cond_3

    .line 334
    sget-object v20, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_DAY_WATER:Landroid/net/Uri;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .local v16, waterUri:Landroid/net/Uri;
    move-object/from16 v12, v16

    .line 335
    goto/16 :goto_0

    .line 337
    .end local v16           #waterUri:Landroid/net/Uri;
    :cond_3
    new-instance v20, Landroid/database/SQLException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Failed to insert Water on "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/util/Date;

    move-object/from16 v0, v22

    invoke-direct {v0, v14, v15}, Ljava/util/Date;-><init>(J)V

    const/16 v23, 0x0

    invoke-static/range {v22 .. v23}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 341
    .end local v14           #waterDate:J
    :sswitch_5
    const-string v20, "weight"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p2

    invoke-virtual {v4, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 342
    const-string v20, "date"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    .line 343
    .local v17, weightDate:J
    const-wide/16 v20, 0x0

    cmp-long v20, v8, v20

    if-lez v20, :cond_4

    .line 344
    sget-object v20, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_DAY_WEIGHT:Landroid/net/Uri;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .local v19, weightUri:Landroid/net/Uri;
    move-object/from16 v12, v19

    .line 345
    goto/16 :goto_0

    .line 347
    .end local v19           #weightUri:Landroid/net/Uri;
    :cond_4
    new-instance v20, Landroid/database/SQLException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Failed to insert Weight on "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/util/Date;

    move-object/from16 v0, v22

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const/16 v23, 0x0

    invoke-static/range {v22 .. v23}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 351
    .end local v17           #weightDate:J
    :sswitch_6
    const-string v20, "sync_tracker"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p2

    invoke-virtual {v4, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 352
    const-wide/16 v20, 0x0

    cmp-long v20, v8, v20

    if-lez v20, :cond_5

    .line 353
    sget-object v20, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v13

    .local v13, syncStatusUri:Landroid/net/Uri;
    move-object v12, v13

    .line 354
    goto/16 :goto_0

    .line 356
    .end local v13           #syncStatusUri:Landroid/net/Uri;
    :cond_5
    new-instance v20, Landroid/database/SQLException;

    const-string v21, "Failed to insert sync Status "

    invoke-direct/range {v20 .. v21}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 288
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x64 -> :sswitch_1
        0xc9 -> :sswitch_2
        0xd2 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x270f -> :sswitch_6
    .end sparse-switch
.end method

.method public insertFood(Lorg/medhelp/mydiet/model/Food;)J
    .locals 20
    .parameter "food"

    .prologue
    .line 633
    if-nez p1, :cond_0

    const-wide/16 v14, -0x1

    .line 683
    :goto_0
    return-wide v14

    .line 635
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v3}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 636
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 638
    .local v13, cv:Landroid/content/ContentValues;
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    .line 640
    .local v16, now:J
    const-string v3, "medhelp_id"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->medHelpId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 641
    const-string v3, "name"

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/medhelp/mydiet/model/Food;->name:Ljava/lang/String;

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    const-string v3, "summary"

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    const-string v3, "food_group_id"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->groupId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 644
    const-string v3, "food"

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    const-string v3, "servings"

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    const-string v3, "calories"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->calories:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 647
    const-string v3, "last_updated_time"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 648
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/medhelp/mydiet/model/Food;->isNewAccess:Z

    if-eqz v3, :cond_1

    .line 649
    const-string v3, "last_accessed_time"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 652
    :cond_1
    const-string v3, "foods"

    const/4 v4, 0x0

    .line 653
    const-string v5, "(medhelp_id > 0 AND medhelp_id = ?)  OR (_id > 0 AND _id = ?)"

    .line 655
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Food;->medHelpId:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Food;->inAppId:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 652
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 657
    .local v12, c:Landroid/database/Cursor;
    const-wide/16 v14, -0x1

    .line 658
    .local v14, insertedFoodId:J
    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_5

    .line 659
    :cond_2
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/medhelp/mydiet/model/Food;->isNewAccess:Z

    if-eqz v3, :cond_3

    .line 660
    const-string v3, "access_count"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 661
    const-string v3, "serving_id"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->servingId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 662
    const-string v3, "amount"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->amount:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 665
    :cond_3
    const-string v3, "foods"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v13}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v14

    .line 681
    :cond_4
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 667
    :cond_5
    if-eqz v12, :cond_4

    .line 668
    const-string v3, "access_count"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 669
    .local v10, accessCount:J
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/medhelp/mydiet/model/Food;->isNewAccess:Z

    if-eqz v3, :cond_6

    .line 670
    const-wide/16 v3, 0x1

    add-long/2addr v10, v3

    .line 671
    const-string v3, "access_count"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 672
    const-string v3, "serving_id"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->servingId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 673
    const-string v3, "amount"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->amount:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 676
    :cond_6
    const-string v3, "_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 677
    const-string v3, "foods"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v13, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 366
    invoke-virtual {p0}, Lorg/medhelp/mydiet/provider/MDDProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/medhelp/mydiet/model/DBHelper;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DBHelper;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    .line 368
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    .prologue
    const/4 v5, 0x0

    .line 374
    const/4 v0, 0x0

    .line 376
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v1, Lorg/medhelp/mydiet/provider/MDDProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 422
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal Argument exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 379
    :sswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, v1, v2}, Lorg/medhelp/mydiet/provider/MDDProvider;->getFoodCursorWithInAppId(J)Landroid/database/Cursor;

    move-result-object v1

    .line 419
    :goto_0
    return-object v1

    .line 381
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, v1, v2}, Lorg/medhelp/mydiet/provider/MDDProvider;->getFoodCursorWithMedHelpID(J)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 383
    :sswitch_2
    const-string v1, "like"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/provider/MDDProvider;->getRecentFoods(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 385
    :sswitch_3
    const-string v1, "like"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/provider/MDDProvider;->getFrequentFoods(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 387
    :sswitch_4
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 388
    const-string v1, "foods"

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 391
    :sswitch_5
    new-instance v1, Ljava/util/Date;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/provider/MDDProvider;->getMealsCursor(Ljava/util/Date;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 394
    :sswitch_6
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 395
    const-string v1, "meals"

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 398
    :sswitch_7
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 399
    const-string v1, "exercises"

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 402
    :sswitch_8
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 403
    const-string v1, "exercise_items"

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 406
    :sswitch_9
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 407
    invoke-virtual {v0, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto/16 :goto_0

    .line 410
    :sswitch_a
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 411
    const-string v1, "water"

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto/16 :goto_0

    .line 414
    :sswitch_b
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 415
    const-string v1, "weight"

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto/16 :goto_0

    .line 418
    :sswitch_c
    iget-object v1, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 419
    const-string v1, "sync_tracker"

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto/16 :goto_0

    .line 376
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_6
        0xb -> :sswitch_5
        0x64 -> :sswitch_4
        0x65 -> :sswitch_0
        0x66 -> :sswitch_1
        0x67 -> :sswitch_2
        0x68 -> :sswitch_3
        0xc9 -> :sswitch_7
        0xd2 -> :sswitch_8
        0xd4 -> :sswitch_9
        0x1f4 -> :sswitch_a
        0x258 -> :sswitch_b
        0x270f -> :sswitch_c
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 429
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 430
    .local v1, lastPathSegment:Ljava/lang/String;
    iget-object v7, p0, Lorg/medhelp/mydiet/provider/MDDProvider;->mDBHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v7}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 432
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v7, Lorg/medhelp/mydiet/provider/MDDProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v7, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 462
    :goto_0
    return v6

    .line 434
    :sswitch_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 435
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 436
    .local v2, waterDate:J
    const-string v7, "water"

    const-string v8, "date=?"

    new-array v9, v9, [Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-virtual {v0, v7, p2, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    .line 438
    .end local v2           #waterDate:J
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid Uri. Valid date is required to update water "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 441
    :sswitch_1
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 442
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 443
    .local v4, weightDate:J
    const-string v7, "weight"

    const-string v8, "date=?"

    new-array v9, v9, [Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-virtual {v0, v7, p2, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    .line 445
    .end local v4           #weightDate:J
    :cond_1
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid Uri. Valid date is required to update weight "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 449
    :sswitch_2
    if-eqz p3, :cond_2

    array-length v6, p4

    if-lez v6, :cond_2

    .line 450
    const-string v6, "meals"

    invoke-virtual {v0, v6, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    .line 452
    :cond_2
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid Uri. Date, selection and selectionArgs are required to delete meals "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 453
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 452
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 455
    :sswitch_3
    if-eqz p3, :cond_3

    if-eqz p4, :cond_3

    array-length v6, p4

    if-lez v6, :cond_3

    .line 456
    const-string v6, "foods"

    invoke-virtual {v0, v6, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    .line 459
    :cond_3
    :sswitch_4
    const-string v6, "sync_tracker"

    invoke-virtual {v0, v6, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    .line 432
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_2
        0x64 -> :sswitch_3
        0x1f5 -> :sswitch_0
        0x259 -> :sswitch_1
        0x270f -> :sswitch_4
    .end sparse-switch
.end method
