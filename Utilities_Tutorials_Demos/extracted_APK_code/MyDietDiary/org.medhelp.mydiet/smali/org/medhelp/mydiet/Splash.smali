.class public Lorg/medhelp/mydiet/Splash;
.super Landroid/app/Activity;
.source "Splash.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/Splash$InitialTask;,
        Lorg/medhelp/mydiet/Splash$LoadDBTask;
    }
.end annotation


# static fields
.field private static final SPLASH_TIME:J = 0x5dcL

.field private static isSplashActive:Z


# instance fields
.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    sput-boolean v0, Lorg/medhelp/mydiet/Splash;->isSplashActive:Z

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/Splash;)V
    .locals 0
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lorg/medhelp/mydiet/Splash;->showHome()V

    return-void
.end method

.method static synthetic access$1()Z
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lorg/medhelp/mydiet/Splash;->isSplashActive:Z

    return v0
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/Splash;)V
    .locals 0
    .parameter

    .prologue
    .line 85
    invoke-direct {p0}, Lorg/medhelp/mydiet/Splash;->startApplication()V

    return-void
.end method

.method private showHome()V
    .locals 4

    .prologue
    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lorg/medhelp/mydiet/Splash$1;

    invoke-direct {v1, p0}, Lorg/medhelp/mydiet/Splash$1;-><init>(Lorg/medhelp/mydiet/Splash;)V

    .line 77
    const-wide/16 v2, 0x5dc

    .line 69
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 78
    return-void
.end method

.method private startApplication()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 86
    const-string v1, "mh_preferences"

    invoke-virtual {p0, v1, v3}, Lorg/medhelp/mydiet/Splash;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/Splash;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 87
    iget-object v1, p0, Lorg/medhelp/mydiet/Splash;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "terms_agreed"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 89
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/TermsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/Splash;->startActivity(Landroid/content/Intent;)V

    .line 106
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->isSetupDone(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 92
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, userType:Ljava/lang/String;
    const-string v1, "medhelp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 94
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDaysSinceLastLogin(Landroid/content/Context;)J

    move-result-wide v1

    const-wide/16 v3, 0xe

    cmp-long v1, v1, v3

    if-ltz v1, :cond_1

    .line 95
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/WelcomeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/Splash;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 97
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/Splash;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 100
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/Splash;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 103
    .end local v0           #userType:Ljava/lang/String;
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/WelcomeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/Splash;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/Splash;->setContentView(I)V

    .line 60
    const/4 v0, 0x1

    sput-boolean v0, Lorg/medhelp/mydiet/Splash;->isSplashActive:Z

    .line 62
    new-instance v0, Lorg/medhelp/mydiet/Splash$InitialTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/Splash$InitialTask;-><init>(Lorg/medhelp/mydiet/Splash;Lorg/medhelp/mydiet/Splash$InitialTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/Splash$InitialTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 64
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->incrementUsageCount(Landroid/content/Context;)V

    .line 65
    new-instance v0, Lorg/medhelp/mydiet/Splash$LoadDBTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/Splash$LoadDBTask;-><init>(Lorg/medhelp/mydiet/Splash;Lorg/medhelp/mydiet/Splash$LoadDBTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/Splash$LoadDBTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit p0

    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 82
    const/4 v0, 0x0

    sput-boolean v0, Lorg/medhelp/mydiet/Splash;->isSplashActive:Z

    .line 83
    return-void
.end method
