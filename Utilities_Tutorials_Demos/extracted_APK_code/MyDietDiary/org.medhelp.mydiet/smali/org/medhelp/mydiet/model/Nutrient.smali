.class public Lorg/medhelp/mydiet/model/Nutrient;
.super Ljava/lang/Object;
.source "Nutrient.java"


# instance fields
.field public displayName:Ljava/lang/String;

.field public isMicroNutrient:Z

.field public medhelpId:Ljava/lang/String;

.field public units:Ljava/lang/String;

.field public usePercentageValue:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .parameter "medhelpId"
    .parameter "displayName"
    .parameter "units"
    .parameter "usePercentageValue"
    .parameter "isMicroNutrient"

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lorg/medhelp/mydiet/model/Nutrient;->medhelpId:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lorg/medhelp/mydiet/model/Nutrient;->displayName:Ljava/lang/String;

    .line 14
    iput-object p3, p0, Lorg/medhelp/mydiet/model/Nutrient;->units:Ljava/lang/String;

    .line 15
    iput-boolean p4, p0, Lorg/medhelp/mydiet/model/Nutrient;->usePercentageValue:Z

    .line 16
    iput-boolean p5, p0, Lorg/medhelp/mydiet/model/Nutrient;->isMicroNutrient:Z

    .line 17
    return-void
.end method
