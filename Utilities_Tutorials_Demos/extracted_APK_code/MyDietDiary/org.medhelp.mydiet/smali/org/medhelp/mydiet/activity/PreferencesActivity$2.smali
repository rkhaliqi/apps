.class Lorg/medhelp/mydiet/activity/PreferencesActivity$2;
.super Ljava/lang/Object;
.source "PreferencesActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/PreferencesActivity;->onCLickBMR()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

.field private final synthetic val$editor:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/PreferencesActivity;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$2;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$2;->val$editor:Landroid/widget/EditText;

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 204
    const/4 v0, 0x0

    .line 205
    .local v0, bmr:F
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$2;->val$editor:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 206
    .local v1, bmrString:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_0

    .line 207
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 210
    :cond_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$2;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    invoke-static {v2, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserInputBMR(Landroid/content/Context;F)V

    .line 211
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$2;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    #calls: Lorg/medhelp/mydiet/activity/PreferencesActivity;->updateBMRSummary()V
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->access$2(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V

    .line 212
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$2;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    iget-object v2, v2, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 213
    return-void
.end method
