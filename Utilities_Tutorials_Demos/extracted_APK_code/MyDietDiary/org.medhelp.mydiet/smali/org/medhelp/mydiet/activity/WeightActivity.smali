.class public Lorg/medhelp/mydiet/activity/WeightActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "WeightActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;,
        Lorg/medhelp/mydiet/activity/WeightActivity$SyncUserDataTask;,
        Lorg/medhelp/mydiet/activity/WeightActivity$UpdateWeightTask;
    }
.end annotation


# instance fields
.field private hasEdits:Z

.field mDate:Ljava/util/Date;

.field mDialog:Landroid/app/AlertDialog;

.field mProgress:Landroid/widget/ProgressBar;

.field mWeight:D


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->hasEdits:Z

    .line 34
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/WeightActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 264
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WeightActivity;->showProgressBar()V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/WeightActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 268
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WeightActivity;->hideProgressBar()V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/WeightActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WeightActivity;->refreshViewContent()V

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/WeightActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 272
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WeightActivity;->showAlertDialog()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/WeightActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 287
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WeightActivity;->hideAlertDialog()V

    return-void
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/WeightActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-boolean p1, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->hasEdits:Z

    return-void
.end method

.method private hideAlertDialog()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 291
    :cond_0
    return-void
.end method

.method private hideProgressBar()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 270
    return-void
.end method

.method private onUpdateClick()V
    .locals 13

    .prologue
    .line 123
    move-object v1, p0

    .line 124
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 125
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v10, "layout_inflater"

    invoke-virtual {v1, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 126
    .local v6, inflater:Landroid/view/LayoutInflater;
    const v11, 0x7f030035

    const v10, 0x7f0b0133

    invoke-virtual {p0, v10}, Lorg/medhelp/mydiet/activity/WeightActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    invoke-virtual {v6, v11, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 127
    .local v9, v:Landroid/view/View;
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 128
    const-string v10, "Enter Weight"

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 130
    const v10, 0x7f0b0134

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    .line 132
    .local v5, editor:Landroid/widget/EditText;
    const v10, 0x7f0b001a

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 133
    .local v8, tv:Landroid/widget/TextView;
    const-string v10, " lb"

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-wide v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mWeight:D

    .line 136
    .local v3, displayWeight:D
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "kg"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 137
    const-wide v10, 0x3fdd07a84ab75e51L

    mul-double/2addr v3, v10

    .line 138
    const-string v10, " kg"

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    :cond_0
    const-wide/16 v10, 0x0

    cmpl-double v10, v3, v10

    if-lez v10, :cond_1

    .line 142
    new-instance v7, Ljava/text/DecimalFormatSymbols;

    new-instance v10, Ljava/util/Locale;

    const-string v11, "en"

    const-string v12, "en"

    invoke-direct {v10, v11, v12}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v7, v10}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 143
    .local v7, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v10, 0x2e

    invoke-virtual {v7, v10}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 144
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v10, "#.#"

    invoke-direct {v2, v10, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 145
    .local v2, df:Ljava/text/DecimalFormat;
    invoke-virtual {v2, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 148
    .end local v2           #df:Ljava/text/DecimalFormat;
    .end local v7           #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_1
    new-instance v10, Lorg/medhelp/mydiet/activity/WeightActivity$1;

    invoke-direct {v10, p0}, Lorg/medhelp/mydiet/activity/WeightActivity$1;-><init>(Lorg/medhelp/mydiet/activity/WeightActivity;)V

    invoke-virtual {v5, v10}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 157
    const-string v10, "OK"

    new-instance v11, Lorg/medhelp/mydiet/activity/WeightActivity$2;

    invoke-direct {v11, p0, v5}, Lorg/medhelp/mydiet/activity/WeightActivity$2;-><init>(Lorg/medhelp/mydiet/activity/WeightActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 180
    const-string v10, "Cancel"

    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 182
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    iput-object v10, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDialog:Landroid/app/AlertDialog;

    .line 183
    iget-object v10, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v10}, Landroid/app/AlertDialog;->show()V

    .line 184
    return-void
.end method

.method private refreshViewContent()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 92
    iget-wide v6, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mWeight:D

    const-wide/high16 v8, -0x4010

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_0

    .line 93
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v6

    float-to-double v6, v6

    iput-wide v6, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mWeight:D

    .line 96
    :cond_0
    new-instance v1, Ljava/text/DecimalFormatSymbols;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "en"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 97
    .local v1, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v6, 0x2e

    invoke-virtual {v1, v6}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 98
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v6, "#.#"

    invoke-direct {v0, v6, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 100
    .local v0, df:Ljava/text/DecimalFormat;
    const/4 v5, 0x0

    .line 101
    .local v5, weightString:Ljava/lang/String;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "lb"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 102
    iget-wide v6, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mWeight:D

    cmpl-double v6, v6, v10

    if-lez v6, :cond_2

    .line 103
    new-instance v6, Ljava/lang/StringBuilder;

    iget-wide v7, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mWeight:D

    invoke-virtual {v0, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " lb"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 116
    :goto_0
    if-eqz v5, :cond_1

    .line 117
    const v6, 0x7f0b012e

    invoke-virtual {p0, v6}, Lorg/medhelp/mydiet/activity/WeightActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 118
    .local v2, tv:Landroid/widget/TextView;
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    .end local v2           #tv:Landroid/widget/TextView;
    :cond_1
    return-void

    .line 105
    :cond_2
    const-string v5, ""

    goto :goto_0

    .line 108
    :cond_3
    iget-wide v6, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mWeight:D

    const-wide v8, 0x3fdd07a84ab75e51L

    mul-double v3, v6, v8

    .line 109
    .local v3, weightInKG:D
    cmpl-double v6, v3, v10

    if-lez v6, :cond_4

    .line 110
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " kg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 112
    :cond_4
    const-string v5, ""

    goto :goto_0
.end method

.method private showAlertDialog()V
    .locals 6

    .prologue
    .line 273
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/WeightActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 274
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v5, 0x7f03002c

    .line 275
    const v4, 0x7f0b0111

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/WeightActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 274
    invoke-virtual {v1, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 277
    .local v2, layout:Landroid/view/View;
    const v4, 0x7f0b001e

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 278
    .local v3, tv:Landroid/widget/TextView;
    const-string v4, "Updating weight..."

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 281
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 282
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 283
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDialog:Landroid/app/AlertDialog;

    .line 284
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 285
    return-void
.end method

.method private showProgressBar()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 266
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 81
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 89
    :goto_0
    return-void

    .line 83
    :sswitch_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WeightActivity;->onUpdateClick()V

    goto :goto_0

    .line 86
    :sswitch_1
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/WeightActivity;->finish()V

    goto :goto_0

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x7f0b0081 -> :sswitch_1
        0x7f0b0132 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    .line 47
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v3, 0x7f030034

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WeightActivity;->setContentView(I)V

    .line 50
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/WeightActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "date"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 52
    .local v0, timeInMillis:J
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-lez v3, :cond_0

    .line 53
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDate:Ljava/util/Date;

    .line 58
    :goto_0
    const v3, 0x7f0b0001

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WeightActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 59
    const v3, 0x7f0b0081

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WeightActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v3, 0x7f0b0132

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WeightActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    new-instance v3, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;-><init>(Lorg/medhelp/mydiet/activity/WeightActivity;Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 64
    const v3, 0x7f0b0036

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WeightActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 65
    .local v2, tvDate:Landroid/widget/TextView;
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDate:Ljava/util/Date;

    const-string v4, "EEEE MMM d, yyyy"

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    return-void

    .line 55
    .end local v2           #tvDate:Landroid/widget/TextView;
    :cond_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDate:Ljava/util/Date;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 72
    :cond_0
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity;->hasEdits:Z

    if-eqz v0, :cond_1

    .line 74
    new-instance v0, Lorg/medhelp/mydiet/activity/WeightActivity$SyncUserDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/WeightActivity$SyncUserDataTask;-><init>(Lorg/medhelp/mydiet/activity/WeightActivity;Lorg/medhelp/mydiet/activity/WeightActivity$SyncUserDataTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/WeightActivity$SyncUserDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 76
    :cond_1
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 77
    return-void
.end method
