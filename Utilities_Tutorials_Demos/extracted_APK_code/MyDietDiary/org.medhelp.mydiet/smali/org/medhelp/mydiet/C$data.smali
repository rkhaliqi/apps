.class public interface abstract Lorg/medhelp/mydiet/C$data;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "data"
.end annotation


# static fields
.field public static final ACCESS_COUNT:Ljava/lang/String; = "access_count"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DB_PATH:Ljava/lang/String; = "/data/data/org.medhelp.mydiet/databases"

.field public static final DIET_DIARY_DB:Ljava/lang/String; = "diet_diary.sqlite"

.field public static final EI_CALORIES:Ljava/lang/String; = "calories"

.field public static final EI_DISTANCE:Ljava/lang/String; = "distance"

.field public static final EI_INAPP_EXERCISE_ID:Ljava/lang/String; = "exercise_id"

.field public static final EI_MEDHELP_ID:Ljava/lang/String; = "medhelp_id"

.field public static final EI_NAME:Ljava/lang/String; = "name"

.field public static final EI_SPEED:Ljava/lang/String; = "speed"

.field public static final EI_TIME:Ljava/lang/String; = "time"

.field public static final EI_TIME_PERIOD:Ljava/lang/String; = "time_period"

.field public static final EI_TYPE:Ljava/lang/String; = "exercise_type"

.field public static final EXERCISES_JSON:Ljava/lang/String; = "exercise_json"

.field public static final EXERCISES_MEDHELP_ID:Ljava/lang/String; = "medhelp_id"

.field public static final EXERCISES_NAME:Ljava/lang/String; = "name"

.field public static final FOOD_AMOUNT:Ljava/lang/String; = "amount"

.field public static final FOOD_CALORIES:Ljava/lang/String; = "calories"

.field public static final FOOD_FOOD_JSON:Ljava/lang/String; = "food"

.field public static final FOOD_GROUP_ID:Ljava/lang/String; = "food_group_id"

.field public static final FOOD_MEDHELP_ID:Ljava/lang/String; = "medhelp_id"

.field public static final FOOD_NAME:Ljava/lang/String; = "name"

.field public static final FOOD_SERVINGS_JSON:Ljava/lang/String; = "servings"

.field public static final FOOD_SERVING_ID:Ljava/lang/String; = "serving_id"

.field public static final FOOD_SUMMARY:Ljava/lang/String; = "summary"

.field public static final LAST_ACCESSED_TIME:Ljava/lang/String; = "last_accessed_time"

.field public static final LAST_UPDATED_TIME:Ljava/lang/String; = "last_updated_time"

.field public static final MEAL_BREAKFAST:Ljava/lang/String; = "breakfast"

.field public static final MEAL_DINNER:Ljava/lang/String; = "dinner"

.field public static final MEAL_LUNCH:Ljava/lang/String; = "lunch"

.field public static final MEAL_SNACKS:Ljava/lang/String; = "snacks"

.field public static final STATUS_EI_MEDHELP_KEY:Ljava/lang/String; = "status_ei_medhelp_key"

.field public static final STATUS_EXERCISES_INSERT:Ljava/lang/String; = "status_insert_exercises"

.field public static final STATUS_OLD_DB_VERSION:Ljava/lang/String; = "status_old_db_version"

.field public static final SYNC_LAST_SYNC_EXERCISES:Ljava/lang/String; = "last_sync_exercises"

.field public static final SYNC_LAST_SYNC_MEALS:Ljava/lang/String; = "last_sync_meals"

.field public static final SYNC_LAST_SYNC_TIME:Ljava/lang/String; = "last_sync_time"

.field public static final SYNC_LAST_SYNC_WATER:Ljava/lang/String; = "last_sync_water"

.field public static final SYNC_LAST_SYNC_WEIGHT:Ljava/lang/String; = "last_sync_weight"

.field public static final SYNC_MONTH_YEAR:Ljava/lang/String; = "month_year_timestamp"

.field public static final TABLE_EXERCISES:Ljava/lang/String; = "exercises"

.field public static final TABLE_EXERCISE_ITEMS:Ljava/lang/String; = "exercise_items"

.field public static final TABLE_FOODS:Ljava/lang/String; = "foods"

.field public static final TABLE_MEALS:Ljava/lang/String; = "meals"

.field public static final TABLE_SYNC_TRAC:Ljava/lang/String; = "sync_tracker"

.field public static final TABLE_WATER:Ljava/lang/String; = "water"

.field public static final TABLE_WEIGHT:Ljava/lang/String; = "weight"

.field public static final UPDATED_AT:Ljava/lang/String; = "updated_at"

.field public static final WATER_AMOUNT:Ljava/lang/String; = "amount"

.field public static final WEIGHT_WEIGHT:Ljava/lang/String; = "weight"

.field public static final _ID:Ljava/lang/String; = "_id"
