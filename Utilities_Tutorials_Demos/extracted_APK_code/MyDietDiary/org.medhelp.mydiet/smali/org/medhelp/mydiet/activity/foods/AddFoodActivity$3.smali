.class Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;
.super Ljava/lang/Object;
.source "AddFoodActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    .line 957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .parameter "s"

    .prologue
    .line 960
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    .prologue
    .line 963
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 966
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 967
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->cancelAllTasks()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$9(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    .line 968
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    new-instance v1, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {v1, v2, v5}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;)V

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchRecentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;
    invoke-static {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$10(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;)V

    .line 969
    if-nez p1, :cond_0

    const-string p1, ""

    .line 971
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchRecentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$11(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 980
    :cond_1
    :goto_0
    return-void

    .line 972
    :cond_2
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 973
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->cancelAllTasks()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$9(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    .line 975
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    new-instance v1, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {v1, v2, v5}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;)V

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchFrequentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;
    invoke-static {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$12(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;)V

    .line 976
    if-nez p1, :cond_3

    const-string p1, ""

    .line 978
    :cond_3
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchFrequentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$13(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
