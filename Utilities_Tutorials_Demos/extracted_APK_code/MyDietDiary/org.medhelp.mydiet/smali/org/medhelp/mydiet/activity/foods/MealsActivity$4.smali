.class Lorg/medhelp/mydiet/activity/foods/MealsActivity$4;
.super Ljava/lang/Object;
.source "MealsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/MealsActivity;->onAddMealClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$4;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 383
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$4;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->access$6(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    move-result-object v2

    invoke-virtual {v2, p2}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v1

    .line 385
    .local v1, mealtype:Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$4;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    const-class v3, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 386
    .local v0, addFoodsIntent:Landroid/content/Intent;
    const-string v2, "date"

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$4;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDate:Ljava/util/Date;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 387
    const-string v2, "meal_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 388
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$4;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    invoke-virtual {v2, v0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->startActivity(Landroid/content/Intent;)V

    .line 390
    return-void
.end method
