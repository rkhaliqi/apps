.class public interface abstract Lorg/medhelp/mydiet/C$nutrients;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "nutrients"
.end annotation


# static fields
.field public static final DRI_BIOTIN:D = 300.0

.field public static final DRI_CALCIUM:D = 1000.0

.field public static final DRI_CHLORIDE:D = 3.4

.field public static final DRI_CHROMIUM:D = 120.0

.field public static final DRI_COPPER:D = 2.0

.field public static final DRI_FOLATE:D = 400.0

.field public static final DRI_IODINE:D = 150.0

.field public static final DRI_IRON:D = 18.0

.field public static final DRI_MACRO_NUTRIENTS:D = 25.0

.field public static final DRI_MAGNESIUM:D = 400.0

.field public static final DRI_MANGANESE:D = 2.0

.field public static final DRI_MOLYBDENUM:D = 75.0

.field public static final DRI_NIACIN:D = 20.0

.field public static final DRI_PANTOTHENIC_ACID:D = 10.0

.field public static final DRI_PHOSPHORUS:D = 1000.0

.field public static final DRI_POTASSIUM:D = 3500.0

.field public static final DRI_RIBOFLAVIN:D = 1.7

.field public static final DRI_SELENIUM:D = 70.0

.field public static final DRI_SODIUM:D = 2400.0

.field public static final DRI_THIAMIN:D = 1.5

.field public static final DRI_VITAMIN_A:D = 5000.0

.field public static final DRI_VITAMIN_B12:D = 6.0

.field public static final DRI_VITAMIN_B6:D = 2.0

.field public static final DRI_VITAMIN_C:D = 60.0

.field public static final DRI_VITAMIN_D:D = 400.0

.field public static final DRI_VITAMIN_E:D = 20.0

.field public static final DRI_VITAMIN_K:D = 80.0

.field public static final DRI_ZINC:D = 15.0
