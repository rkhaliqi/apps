.class public Lorg/medhelp/mydiet/model/Food;
.super Ljava/lang/Object;
.source "Food.java"


# instance fields
.field public accessCount:J

.field public amount:D

.field public calories:D

.field public foodJSON:Ljava/lang/String;

.field public foodServingsJSON:Ljava/lang/String;

.field public groupId:J

.field public inAppId:J

.field public isNewAccess:Z

.field public lastAccessedTime:J

.field public medHelpId:J

.field public name:Ljava/lang/String;

.field public servingId:J

.field public summary:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFoodDetail()Lorg/medhelp/mydiet/model/FoodDetail;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    iget-object v1, p0, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodDetail(Ljava/lang/String;Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v0

    return-object v0
.end method

.method public getFoodServing()Lorg/medhelp/mydiet/model/FoodServing;
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/Food;->getFoodServings()Ljava/util/ArrayList;

    move-result-object v0

    .line 46
    .local v0, servings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodServing;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 49
    :goto_0
    return-object v1

    .line 48
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 49
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/FoodServing;

    goto :goto_0
.end method

.method public getFoodServing(J)Lorg/medhelp/mydiet/model/FoodServing;
    .locals 5
    .parameter "servingId"

    .prologue
    .line 58
    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/Food;->getFoodServings()Ljava/util/ArrayList;

    move-result-object v1

    .line 59
    .local v1, servings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodServing;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    .line 61
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 66
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/FoodServing;

    move-object v0, v2

    goto :goto_0

    .line 61
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodServing;

    .line 62
    .local v0, serving:Lorg/medhelp/mydiet/model/FoodServing;
    iget-wide v3, v0, Lorg/medhelp/mydiet/model/FoodServing;->medhelpId:J

    cmp-long v3, v3, p1

    if-nez v3, :cond_2

    goto :goto_0
.end method

.method public getFoodServings()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodServing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    invoke-static {v0}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodServings(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    return-object v0
.end method
