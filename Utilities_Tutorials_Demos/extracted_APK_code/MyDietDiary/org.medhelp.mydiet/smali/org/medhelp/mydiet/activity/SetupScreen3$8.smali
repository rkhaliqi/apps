.class Lorg/medhelp/mydiet/activity/SetupScreen3$8;
.super Ljava/lang/Object;
.source "SetupScreen3.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/SetupScreen3;->clickHeight()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

.field private final synthetic val$heightField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/SetupScreen3;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$8;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$8;->val$heightField:Landroid/widget/EditText;

    .line 351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 355
    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    .line 356
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$8;->val$heightField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 357
    .local v1, s:Ljava/lang/String;
    const/4 v0, 0x0

    .line 358
    .local v0, height:F
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_0

    .line 359
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 361
    :cond_0
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_2

    const/high16 v2, 0x4397

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_2

    .line 362
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$8;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-static {v2, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightInCm(Landroid/content/Context;F)V

    .line 366
    :goto_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$8;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    #calls: Lorg/medhelp/mydiet/activity/SetupScreen3;->updateHeightDisplay()V
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/SetupScreen3;->access$3(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    .line 369
    .end local v0           #height:F
    .end local v1           #s:Ljava/lang/String;
    :cond_1
    return-void

    .line 364
    .restart local v0       #height:F
    .restart local v1       #s:Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$8;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    const-string v3, "Invalid height"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
