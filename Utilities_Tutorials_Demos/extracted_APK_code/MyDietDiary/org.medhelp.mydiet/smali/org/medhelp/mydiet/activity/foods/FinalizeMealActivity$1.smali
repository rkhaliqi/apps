.class Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;
.super Ljava/lang/Object;
.source "FinalizeMealActivity.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    .line 397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 5
    .parameter "view"
    .parameter "hourOfDay"
    .parameter "minute"

    .prologue
    .line 401
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 403
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v1

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    .line 404
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v1

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, v1, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    .line 406
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 407
    .local v0, c:Ljava/util/Calendar;
    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v2

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 408
    const/16 v1, 0xb

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 409
    const/16 v1, 0xc

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 411
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v1

    invoke-static {v0}, Lorg/medhelp/mydiet/util/DateUtil;->calendarToDate(Ljava/util/Calendar;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, v1, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    .line 412
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setMealTimeText()V
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$2(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    monitor-exit p0

    return-void

    .line 401
    .end local v0           #c:Ljava/util/Calendar;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
