.class Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;
.super Ljava/lang/Object;
.source "EditExerciseActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->onClickType()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

.field private final synthetic val$types:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->val$types:[Ljava/lang/String;

    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 275
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->val$types:[Ljava/lang/String;

    aget-object v1, v1, p2

    iput-object v1, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    .line 276
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->val$types:[Ljava/lang/String;

    aget-object v1, v1, p2

    iput-object v1, v0, Lorg/medhelp/mydiet/model/Exercise;->exerciseType:Ljava/lang/String;

    .line 278
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    const v1, 0x7f0b0027

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 279
    .local v9, tv:Landroid/widget/TextView;
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v0, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    invoke-static {v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v0

    float-to-double v3, v0

    .line 282
    .local v3, weight:D
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->val$types:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/model/Exercise;->getCalorieCoefficientForType(Ljava/lang/String;)D

    move-result-wide v5

    .line 284
    .local v5, calorieCoefficient:D
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v1, v1, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->getCaloriesBurned(DDD)D
    invoke-static/range {v0 .. v6}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$10(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;DDD)D

    move-result-wide v7

    .line 285
    .local v7, calories:D
    const-wide/16 v0, 0x0

    cmpg-double v0, v7, v0

    if-gez v0, :cond_0

    .line 286
    const-wide/16 v7, 0x0

    .line 287
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setCalories(D)V
    invoke-static {v0, v7, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$11(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;D)V

    .line 289
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 290
    return-void
.end method
