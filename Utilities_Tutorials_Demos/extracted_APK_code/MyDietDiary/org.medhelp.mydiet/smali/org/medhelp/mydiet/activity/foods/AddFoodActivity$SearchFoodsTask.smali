.class Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;
.super Landroid/os/AsyncTask;
.source "AddFoodActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchFoodsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final PROGRESS_IS_ROAMING:I = 0x3

.field private static final PROGRESS_NO_NETWORK:I = 0x2

.field private static final PROGRESS_REFRESH_RESULTS:I = 0x1

.field private static final PROGRESS_START_SEARCH:I


# instance fields
.field private _foodItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation
.end field

.field private _searchString:Ljava/lang/String;

.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 506
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 506
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    return-void
.end method

.method private declared-synchronized addNewResultsToFoods()V
    .locals 3

    .prologue
    .line 602
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_0

    .line 603
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v1, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$20(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Ljava/util/ArrayList;)V

    .line 604
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 606
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->_foodItems:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 607
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v1

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->_foodItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->markSelectedFoodItems(Ljava/util/ArrayList;)V

    .line 609
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->_foodItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 614
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->_foodItems:Ljava/util/ArrayList;

    .line 615
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPage:I
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$2(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPage:I
    invoke-static {v1, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$22(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 616
    monitor-exit p0

    return-void

    .line 609
    :cond_2
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 610
    .local v0, item:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 602
    .end local v0           #item:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized searchAndDisplayFoods(Ljava/lang/String;II)V
    .locals 5
    .parameter "searchString"
    .parameter "limit"
    .parameter "offset"

    .prologue
    .line 583
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->isConnectedToNetwork()Z
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$18(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 584
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->publishProgress([Ljava/lang/Object;)V

    .line 585
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 599
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 586
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->isNetworkRoaming()Z
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$19(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 587
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->publishProgress([Ljava/lang/Object;)V

    .line 588
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 583
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 590
    :cond_2
    int-to-long v1, p2

    int-to-long v3, p3

    :try_start_2
    invoke-static {p1, v1, v2, v3, v4}, Lorg/medhelp/mydiet/http/MHHttpHandler;->searchFoods(Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v0

    .line 591
    .local v0, foods:Ljava/lang/String;
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 592
    invoke-static {}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$8()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 594
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItems(Ljava/lang/String;Z)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->_foodItems:Ljava/util/ArrayList;

    .line 595
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 3
    .parameter "params"

    .prologue
    .line 526
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->_searchString:Ljava/lang/String;

    .line 527
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 528
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->_searchString:Ljava/lang/String;

    const/16 v1, 0x14

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPage:I
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$2(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x14

    invoke-direct {p0, v0, v1, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->searchAndDisplayFoods(Ljava/lang/String;II)V

    .line 529
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 578
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const/4 v1, 0x0

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mIsLoadingItems:Z
    invoke-static {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$14(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Z)V

    .line 579
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 580
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 572
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const/4 v1, 0x0

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mIsLoadingItems:Z
    invoke-static {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$14(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Z)V

    .line 573
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 574
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 518
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 519
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mIsLoadingItems:Z
    invoke-static {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$14(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Z)V

    .line 520
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->showLoadingDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$15(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    .line 521
    new-array v0, v1, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->publishProgress([Ljava/lang/Object;)V

    .line 522
    return-void
.end method

.method protected varargs declared-synchronized onProgressUpdate([Ljava/lang/Integer;)V
    .locals 5
    .parameter "values"

    .prologue
    .line 534
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 535
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 568
    :goto_0
    :pswitch_0
    monitor-exit p0

    return-void

    .line 539
    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const-string v3, "You are not connected to network"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 534
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 542
    :pswitch_2
    :try_start_2
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const-string v3, "Network is unavailable(roaming)"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 545
    :pswitch_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->addNewResultsToFoods()V

    .line 547
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 548
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->_searchString:Ljava/lang/String;

    invoke-static {}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$8()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 549
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPreviousPageItemsCount:I
    invoke-static {v2, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$16(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;I)V

    .line 565
    :cond_0
    :goto_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->hideLoadingDialog()V
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$17(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    goto :goto_0

    .line 552
    :cond_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const v3, 0x7f0b0040

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 554
    .local v0, text:Ljava/lang/String;
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const v3, 0x7f0b0042

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 555
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const v3, 0x7f0b0043

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 557
    .local v1, tv:Landroid/widget/TextView;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 558
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "There are no foods that matched "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Create a new food to add it."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 561
    :cond_2
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 535
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
