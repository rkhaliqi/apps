.class public Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "AddFoodActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;,
        Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;,
        Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;,
        Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;,
        Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;
    }
.end annotation


# static fields
.field private static final ALL_ENTRIES:I = 0x0

.field private static final FREQUENT:I = 0x2

.field private static final LIMIT:I = 0x14

.field private static final RECENT:I = 0x1

.field private static mCurrentSearchString:Ljava/lang/String;


# instance fields
.field private mAddingMoreFoods:J

.field private mCurrentResults:I

.field private mDate:Ljava/util/Date;

.field private mDialog:Landroid/app/AlertDialog;

.field private mFoods:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation
.end field

.field private mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

.field private mFoodsFromPreviousSearch:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation
.end field

.field private mFoodsList:Landroid/widget/ListView;

.field private mFooterView:Landroid/widget/LinearLayout;

.field private mIsLoadingItems:Z

.field private mMealType:Ljava/lang/String;

.field private mPage:I

.field private mPreviousPageItemsCount:I

.field private mSearchActive:Z

.field private mSearchFrequentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

.field private mSearchRecentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

.field private mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

.field private mSearchText:Landroid/widget/EditText;

.field mTransientFoodPosition:I

.field private onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private textWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 74
    iput v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPage:I

    .line 83
    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchActive:Z

    .line 85
    iput v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I

    .line 100
    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mIsLoadingItems:Z

    .line 115
    const/16 v0, 0x15

    iput v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPreviousPageItemsCount:I

    .line 425
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$1;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$1;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 435
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 957
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$3;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->textWatcher:Landroid/text/TextWatcher;

    .line 44
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 83
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchActive:Z

    return v0
.end method

.method static synthetic access$10(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchRecentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

    return-void
.end method

.method static synthetic access$11(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;
    .locals 1
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchRecentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

    return-object v0
.end method

.method static synthetic access$12(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchFrequentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

    return-void
.end method

.method static synthetic access$13(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;
    .locals 1
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchFrequentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

    return-object v0
.end method

.method static synthetic access$14(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 100
    iput-boolean p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mIsLoadingItems:Z

    return-void
.end method

.method static synthetic access$15(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 207
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->showLoadingDialog()V

    return-void
.end method

.method static synthetic access$16(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 115
    iput p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPreviousPageItemsCount:I

    return-void
.end method

.method static synthetic access$17(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 211
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->hideLoadingDialog()V

    return-void
.end method

.method static synthetic access$18(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 619
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->isConnectedToNetwork()Z

    move-result v0

    return v0
.end method

.method static synthetic access$19(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 625
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->isNetworkRoaming()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 74
    iget v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPage:I

    return v0
.end method

.method static synthetic access$20(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    return-object v0
.end method

.method static synthetic access$22(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    iput p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPage:I

    return-void
.end method

.method static synthetic access$23(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 749
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->saveFoodsToDB(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$24(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 768
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->showSavingFoodsDialog()V

    return-void
.end method

.method static synthetic access$25(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 780
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->hideSavingFoodsDialog()V

    return-void
.end method

.method static synthetic access$26(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 118
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mMealType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$27(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)J
    .locals 2
    .parameter

    .prologue
    .line 120
    iget-wide v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mAddingMoreFoods:J

    return-wide v0
.end method

.method static synthetic access$28(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/Date;
    .locals 1
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$29(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 240
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->refreshCount()V

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 115
    iget v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPreviousPageItemsCount:I

    return v0
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 100
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mIsLoadingItems:Z

    return v0
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 85
    iget v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I

    return v0
.end method

.method static synthetic access$6(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 106
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    return-void
.end method

.method static synthetic access$7(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;
    .locals 1
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    return-object v0
.end method

.method static synthetic access$8()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentSearchString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 983
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->cancelAllTasks()V

    return-void
.end method

.method private cancelAllTasks()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 984
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    if-eqz v0, :cond_0

    .line 985
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->cancel(Z)Z

    .line 987
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchRecentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

    if-eqz v0, :cond_1

    .line 988
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchRecentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->cancel(Z)Z

    .line 990
    :cond_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchFrequentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

    if-eqz v0, :cond_2

    .line 991
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchFrequentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;->cancel(Z)Z

    .line 993
    :cond_2
    return-void
.end method

.method private finishAndShowDetails()V
    .locals 1

    .prologue
    .line 421
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->setResult(I)V

    .line 422
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->finish()V

    .line 423
    return-void
.end method

.method private hideLoadingDialog()V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFooterView:Landroid/widget/LinearLayout;

    const v1, 0x7f0b00d3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    return-void
.end method

.method private hideSavingFoodsDialog()V
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 782
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 784
    :cond_0
    return-void
.end method

.method private hideSoftKeyboard()V
    .locals 3

    .prologue
    .line 385
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 386
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 387
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 388
    :cond_0
    return-void
.end method

.method private initializeEmptyListView()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    .line 177
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$4;

    .line 179
    const v3, 0x7f030012

    .line 180
    const v4, 0x7f0b0049

    .line 181
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$4;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Landroid/content/Context;IILjava/util/ArrayList;)V

    .line 177
    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    .line 194
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 195
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 196
    const v1, 0x7f03001f

    invoke-virtual {v0, v1, v7, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 194
    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFooterView:Landroid/widget/LinearLayout;

    .line 197
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsList:Landroid/widget/ListView;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFooterView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v7, v6}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 199
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->hideLoadingDialog()V

    .line 201
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsList:Landroid/widget/ListView;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 203
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsList:Landroid/widget/ListView;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 204
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsList:Landroid/widget/ListView;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 205
    return-void
.end method

.method private isConnectedToNetwork()Z
    .locals 3

    .prologue
    .line 620
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 621
    .local v1, connectivityManager:Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 622
    .local v0, activeNetworkInfo:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isNetworkRoaming()Z
    .locals 3

    .prologue
    .line 626
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 627
    .local v1, connectivityManager:Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 628
    .local v0, activeNetworkInfo:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onAllEntriesClick()V
    .locals 7

    .prologue
    const v6, 0x7f0b0042

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 321
    const v2, 0x7f0b003f

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 322
    .local v0, btn:Landroid/widget/Button;
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 323
    iput v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I

    .line 325
    invoke-virtual {p0, v6}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 327
    const v2, 0x7f0b003d

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 328
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 329
    const v2, 0x7f0b003e

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 330
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 332
    const v2, 0x7f0b0041

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 333
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 335
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->cancelAllTasks()V

    .line 336
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 337
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 339
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsFromPreviousSearch:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsFromPreviousSearch:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 340
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    .line 341
    :cond_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsFromPreviousSearch:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->markSelectedFoodItems(Ljava/util/ArrayList;)V

    .line 342
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsFromPreviousSearch:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 343
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 350
    :goto_0
    return-void

    .line 345
    :cond_1
    invoke-virtual {p0, v6}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 346
    const v2, 0x7f0b0043

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 348
    .local v1, tv:Landroid/widget/TextView;
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private declared-synchronized onFinalizeClick()V
    .locals 4

    .prologue
    .line 381
    monitor-enter p0

    :try_start_0
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    monitor-exit p0

    return-void

    .line 381
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private onFrequentClick()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 286
    const v3, 0x7f0b0040

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 287
    .local v2, searchText:Landroid/widget/EditText;
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, s:Ljava/lang/String;
    const v3, 0x7f0b0042

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 291
    iget v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I

    if-nez v3, :cond_0

    .line 292
    sput-object v1, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentSearchString:Ljava/lang/String;

    .line 294
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 295
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsFromPreviousSearch:Ljava/util/ArrayList;

    .line 296
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsFromPreviousSearch:Ljava/util/ArrayList;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 300
    :cond_0
    const v3, 0x7f0b003e

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 301
    .local v0, btn:Landroid/widget/Button;
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 302
    const/4 v3, 0x2

    iput v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I

    .line 305
    const v3, 0x7f0b003d

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 306
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 307
    const v3, 0x7f0b003f

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 308
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 310
    const v3, 0x7f0b0041

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 311
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 313
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->cancelAllTasks()V

    .line 314
    new-instance v3, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;)V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchFrequentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

    .line 315
    if-nez v1, :cond_1

    const-string v1, ""

    .line 317
    :cond_1
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchFrequentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;

    new-array v4, v5, [Ljava/lang/String;

    aput-object v1, v4, v6

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFrequentFoodsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 318
    return-void
.end method

.method private onRecentClick()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 252
    const v3, 0x7f0b0040

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 253
    .local v2, searchText:Landroid/widget/EditText;
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 255
    .local v1, s:Ljava/lang/String;
    const v3, 0x7f0b0042

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 257
    iget v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I

    if-nez v3, :cond_0

    .line 258
    sput-object v1, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentSearchString:Ljava/lang/String;

    .line 260
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 261
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsFromPreviousSearch:Ljava/util/ArrayList;

    .line 262
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsFromPreviousSearch:Ljava/util/ArrayList;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 266
    :cond_0
    const v3, 0x7f0b003d

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 267
    .local v0, btn:Landroid/widget/Button;
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 268
    iput v5, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I

    .line 270
    const v3, 0x7f0b003e

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 271
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 272
    const v3, 0x7f0b003f

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 273
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 275
    const v3, 0x7f0b0041

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 276
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 278
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->cancelAllTasks()V

    .line 279
    new-instance v3, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;)V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchRecentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

    .line 280
    if-nez v1, :cond_1

    const-string v1, ""

    .line 282
    :cond_1
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchRecentFoodsTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;

    new-array v4, v5, [Ljava/lang/String;

    aput-object v1, v4, v6

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 283
    return-void
.end method

.method private declared-synchronized onSearchClick(Ljava/lang/String;)V
    .locals 2
    .parameter "searchString"

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    sget-object v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentSearchString:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentSearchString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 354
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 355
    const-string v0, "Please Enter a food to search"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    :goto_0
    monitor-exit p0

    return-void

    .line 358
    :cond_1
    const v0, 0x7f0b0042

    :try_start_1
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 360
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->startSearch()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private refreshCount()V
    .locals 4

    .prologue
    .line 241
    const v2, 0x7f0b003b

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 242
    .local v1, tv:Landroid/widget/TextView;
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 246
    .local v0, count:I
    if-lez v0, :cond_0

    .line 247
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    :cond_0
    return-void
.end method

.method private saveFoodsToDB(Ljava/util/ArrayList;)V
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 750
    .local p1, foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 766
    :cond_0
    return-void

    .line 752
    :cond_1
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 753
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    const/4 v1, 0x0

    .line 754
    .local v1, food:Lorg/medhelp/mydiet/model/Food;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 755
    .local v4, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-object v6, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodJSONString:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodJSONString:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 756
    iget-object v6, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodJSONString:Ljava/lang/String;

    invoke-static {v6}, Lorg/medhelp/mydiet/util/DataUtil;->getFood(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Food;

    move-result-object v1

    .line 757
    const/4 v6, 0x1

    iput-boolean v6, v1, Lorg/medhelp/mydiet/model/Food;->isNewAccess:Z

    .line 758
    iget-wide v6, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    iput-wide v6, v1, Lorg/medhelp/mydiet/model/Food;->servingId:J

    .line 759
    iget-wide v6, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    iput-wide v6, v1, Lorg/medhelp/mydiet/model/Food;->amount:D

    .line 760
    iget-object v6, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    iput-object v6, v1, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    .line 762
    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->insertFood(Lorg/medhelp/mydiet/model/Food;)J

    move-result-wide v2

    .line 763
    .local v2, foodInAppId:J
    iput-wide v2, v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    goto :goto_0
.end method

.method private showLoadingDialog()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFooterView:Landroid/widget/LinearLayout;

    const v1, 0x7f0b00d3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 209
    return-void
.end method

.method private showSavingFoodsDialog()V
    .locals 5

    .prologue
    .line 769
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 770
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v4, 0x7f03002c

    .line 771
    const v3, 0x7f0b0111

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 770
    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 773
    .local v2, layout:Landroid/view/View;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 774
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 775
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 776
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mDialog:Landroid/app/AlertDialog;

    .line 777
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 778
    return-void
.end method

.method private startSearch()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 364
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->hideSoftKeyboard()V

    .line 366
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->cancel(Z)Z

    .line 370
    :cond_0
    iput v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPage:I

    .line 371
    const/16 v0, 0x15

    iput v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPreviousPageItemsCount:I

    .line 372
    iput-boolean v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchActive:Z

    .line 373
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 374
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 376
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    .line 377
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    new-array v1, v2, [Ljava/lang/String;

    sget-object v2, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentSearchString:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 378
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v2, -0x1

    .line 394
    sparse-switch p1, :sswitch_data_0

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 396
    :sswitch_0
    if-ne p2, v2, :cond_0

    .line 397
    const-string v2, "transient_foods"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    .local v0, foodKey:Ljava/lang/String;
    invoke-static {v0}, Lorg/medhelp/mydiet/model/TransientDataStore;->retrieveData(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 399
    .local v1, resultFood:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;

    iget v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mTransientFoodPosition:I

    invoke-virtual {v2, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 400
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 401
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v2, v1}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->selectFoodItem(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    goto :goto_0

    .line 405
    .end local v0           #foodKey:Ljava/lang/String;
    .end local v1           #resultFood:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :sswitch_1
    if-ne p2, v2, :cond_0

    .line 406
    const-string v2, "transient_foods"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 407
    .restart local v0       #foodKey:Ljava/lang/String;
    invoke-static {v0}, Lorg/medhelp/mydiet/model/TransientDataStore;->retrieveData(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 408
    .restart local v1       #resultFood:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->refreshCount()V

    .line 410
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 411
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v2, v1}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->selectFoodItem(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    goto :goto_0

    .line 415
    .end local v0           #foodKey:Ljava/lang/String;
    .end local v1           #resultFood:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :sswitch_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->finishAndShowDetails()V

    goto :goto_0

    .line 394
    :sswitch_data_0
    .sparse-switch
        0x3eb -> :sswitch_0
        0x3ed -> :sswitch_1
        0xbb9 -> :sswitch_2
    .end sparse-switch
.end method

.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 217
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 238
    :goto_0
    :pswitch_0
    monitor-exit p0

    return-void

    .line 219
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentSearchString:Ljava/lang/String;

    .line 220
    sget-object v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentSearchString:Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->onSearchClick(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 223
    :pswitch_2
    :try_start_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->onFinalizeClick()V

    goto :goto_0

    .line 226
    :pswitch_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->onRecentClick()V

    goto :goto_0

    .line 229
    :pswitch_4
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->onFrequentClick()V

    goto :goto_0

    .line 232
    :pswitch_5
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->onAllEntriesClick()V

    goto :goto_0

    .line 235
    :pswitch_6
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3ed

    invoke-virtual {p0, v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x7f0b003c
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .parameter "savedInstanceState"

    .prologue
    const v8, 0x7f0b0040

    const/4 v5, 0x0

    .line 124
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 125
    const v4, 0x7f030011

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->setContentView(I)V

    .line 127
    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchText:Landroid/widget/EditText;

    .line 128
    const v4, 0x7f0b0044

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsList:Landroid/widget/ListView;

    .line 129
    const v4, 0x7f0b0041

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 130
    .local v0, btn:Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 133
    const v4, 0x7f0b0045

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    const v4, 0x7f0b003d

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    const v4, 0x7f0b003e

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    const v4, 0x7f0b003f

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #btn:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 137
    .restart local v0       #btn:Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 140
    const v4, 0x7f0b003c

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->initializeEmptyListView()V

    .line 144
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "date"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 146
    .local v2, timeInMillis:J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 147
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mDate:Ljava/util/Date;

    .line 152
    :goto_0
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "meal_type"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mMealType:Ljava/lang/String;

    .line 153
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mMealType:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 154
    const-string v4, "Snack"

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mMealType:Ljava/lang/String;

    .line 157
    :cond_0
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "add_more_foods"

    const-wide/16 v6, -0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mAddingMoreFoods:J

    .line 159
    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 160
    .local v1, searchText:Landroid/widget/EditText;
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 161
    return-void

    .line 149
    .end local v1           #searchText:Landroid/widget/EditText;
    :cond_1
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->finish()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 172
    :cond_0
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 173
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onStart()V

    .line 165
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 166
    return-void
.end method
