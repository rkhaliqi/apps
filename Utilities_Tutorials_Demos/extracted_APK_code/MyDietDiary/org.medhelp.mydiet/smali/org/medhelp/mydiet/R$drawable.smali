.class public final Lorg/medhelp/mydiet/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final av_boy1:I = 0x7f020000

.field public static final av_boy10:I = 0x7f020001

.field public static final av_boy11:I = 0x7f020002

.field public static final av_boy12:I = 0x7f020003

.field public static final av_boy2:I = 0x7f020004

.field public static final av_boy3:I = 0x7f020005

.field public static final av_boy4:I = 0x7f020006

.field public static final av_boy5:I = 0x7f020007

.field public static final av_boy6:I = 0x7f020008

.field public static final av_boy7:I = 0x7f020009

.field public static final av_boy8:I = 0x7f02000a

.field public static final av_boy9:I = 0x7f02000b

.field public static final av_girl1:I = 0x7f02000c

.field public static final av_girl10:I = 0x7f02000d

.field public static final av_girl11:I = 0x7f02000e

.field public static final av_girl12:I = 0x7f02000f

.field public static final av_girl2:I = 0x7f020010

.field public static final av_girl3:I = 0x7f020011

.field public static final av_girl4:I = 0x7f020012

.field public static final av_girl5:I = 0x7f020013

.field public static final av_girl6:I = 0x7f020014

.field public static final av_girl7:I = 0x7f020015

.field public static final av_girl8:I = 0x7f020016

.field public static final av_girl9:I = 0x7f020017

.field public static final avt_dot_off:I = 0x7f020018

.field public static final avt_dot_on:I = 0x7f020019

.field public static final bar_top_branding:I = 0x7f02001a

.field public static final bar_top_branding_line:I = 0x7f02001b

.field public static final bar_top_branding_logo:I = 0x7f02001c

.field public static final bg_date:I = 0x7f02001d

.field public static final bg_details_paper_top:I = 0x7f02001e

.field public static final bg_dialog_1:I = 0x7f02001f

.field public static final bg_et_invalid_value_normal:I = 0x7f020020

.field public static final bg_green_radial:I = 0x7f020021

.field public static final bg_grey_bar:I = 0x7f020022

.field public static final bg_main_actions:I = 0x7f020023

.field public static final bg_nodata:I = 0x7f020024

.field public static final bg_note_bottom:I = 0x7f020025

.field public static final bg_nutrition_label_bottom:I = 0x7f020026

.field public static final bg_nutrition_label_mid:I = 0x7f020027

.field public static final bg_nutrition_label_top:I = 0x7f020028

.field public static final bg_panel_bottom:I = 0x7f020029

.field public static final bg_panel_top:I = 0x7f02002a

.field public static final bg_paper:I = 0x7f02002b

.field public static final bg_progress_grey:I = 0x7f02002c

.field public static final bg_progress_orange:I = 0x7f02002d

.field public static final bg_progress_red:I = 0x7f02002e

.field public static final bg_settings:I = 0x7f02002f

.field public static final bg_silver_panel:I = 0x7f020030

.field public static final bg_spiral_ring_tiles:I = 0x7f020031

.field public static final bg_splash:I = 0x7f020032

.field public static final bg_summary_green:I = 0x7f020033

.field public static final bg_tab:I = 0x7f020034

.field public static final bg_tiled:I = 0x7f020035

.field public static final bg_tiled_notes:I = 0x7f020036

.field public static final bg_titlebar:I = 0x7f020037

.field public static final bg_top_branding_bar:I = 0x7f020038

.field public static final bg_weight_water:I = 0x7f020039

.field public static final bt_arrow_left:I = 0x7f02003a

.field public static final bt_arrow_right:I = 0x7f02003b

.field public static final bt_dark_normal:I = 0x7f02003c

.field public static final bt_dark_selected:I = 0x7f02003d

.field public static final bt_select_meal_bfast:I = 0x7f02003e

.field public static final bt_select_meal_dinner:I = 0x7f02003f

.field public static final bt_select_meal_lunch:I = 0x7f020040

.field public static final bt_select_meal_snack:I = 0x7f020041

.field public static final btn_arrow_left:I = 0x7f020042

.field public static final btn_arrow_right:I = 0x7f020043

.field public static final btn_avt_left:I = 0x7f020044

.field public static final btn_avt_right:I = 0x7f020045

.field public static final btn_control_normal:I = 0x7f020046

.field public static final btn_create_food:I = 0x7f020047

.field public static final btn_create_food_normal:I = 0x7f020048

.field public static final btn_create_food_selected:I = 0x7f020049

.field public static final btn_dark:I = 0x7f02004a

.field public static final btn_dark_normal:I = 0x7f02004b

.field public static final btn_dark_selected:I = 0x7f02004c

.field public static final btn_default:I = 0x7f02004d

.field public static final btn_default_normal:I = 0x7f02004e

.field public static final btn_default_selected:I = 0x7f02004f

.field public static final btn_details:I = 0x7f020050

.field public static final btn_details_normal:I = 0x7f020051

.field public static final btn_details_selected:I = 0x7f020052

.field public static final btn_dropdown_normal:I = 0x7f020053

.field public static final btn_edit:I = 0x7f020054

.field public static final btn_grey_dark:I = 0x7f020055

.field public static final btn_grey_dark_normal:I = 0x7f020056

.field public static final btn_grey_dark_selected:I = 0x7f020057

.field public static final btn_grey_normal:I = 0x7f020058

.field public static final btn_grey_selected:I = 0x7f020059

.field public static final btn_grey_toggle:I = 0x7f02005a

.field public static final btn_grey_toggle_normal:I = 0x7f02005b

.field public static final btn_grey_toggle_selected:I = 0x7f02005c

.field public static final btn_main_actions:I = 0x7f02005d

.field public static final btn_minus:I = 0x7f02005e

.field public static final btn_minus_normal:I = 0x7f02005f

.field public static final btn_minus_selected:I = 0x7f020060

.field public static final btn_plus:I = 0x7f020061

.field public static final btn_plus_normal:I = 0x7f020062

.field public static final btn_plus_selected:I = 0x7f020063

.field public static final btn_question:I = 0x7f020064

.field public static final btn_question_normal:I = 0x7f020065

.field public static final btn_question_selected:I = 0x7f020066

.field public static final btn_search:I = 0x7f020067

.field public static final btn_search_normal:I = 0x7f020068

.field public static final btn_search_selected:I = 0x7f020069

.field public static final btn_settings:I = 0x7f02006a

.field public static final btn_summary:I = 0x7f02006b

.field public static final btn_summary_normal:I = 0x7f02006c

.field public static final btn_summary_selected:I = 0x7f02006d

.field public static final btn_tbb_ge:I = 0x7f02006e

.field public static final btn_tbb_medhelp:I = 0x7f02006f

.field public static final btn_white:I = 0x7f020070

.field public static final btn_white_normal:I = 0x7f020071

.field public static final btn_white_selected:I = 0x7f020072

.field public static final et_search:I = 0x7f020073

.field public static final et_search_focussed:I = 0x7f020074

.field public static final et_search_normal:I = 0x7f020075

.field public static final et_search_selected:I = 0x7f020076

.field public static final et_transparent:I = 0x7f020077

.field public static final ic_btn_exercise:I = 0x7f020078

.field public static final ic_btn_food:I = 0x7f020079

.field public static final ic_btn_nav_arrow_right:I = 0x7f02007a

.field public static final ic_btn_water:I = 0x7f02007b

.field public static final ic_btn_weight:I = 0x7f02007c

.field public static final ic_calories_carbs:I = 0x7f02007d

.field public static final ic_calories_fat:I = 0x7f02007e

.field public static final ic_calories_protein:I = 0x7f02007f

.field public static final ic_edit_normal:I = 0x7f020080

.field public static final ic_edit_selected:I = 0x7f020081

.field public static final ic_ge_normal:I = 0x7f020082

.field public static final ic_help_apple:I = 0x7f020083

.field public static final ic_help_dumbell:I = 0x7f020084

.field public static final ic_help_minus:I = 0x7f020085

.field public static final ic_help_plus:I = 0x7f020086

.field public static final ic_launcher:I = 0x7f020087

.field public static final ic_logo_market:I = 0x7f020088

.field public static final ic_logos_ge:I = 0x7f020089

.field public static final ic_logos_mh:I = 0x7f02008a

.field public static final ic_menu_info:I = 0x7f02008b

.field public static final ic_menu_key:I = 0x7f02008c

.field public static final ic_menu_question:I = 0x7f02008d

.field public static final ic_menu_settings:I = 0x7f02008e

.field public static final ic_mh_normal:I = 0x7f02008f

.field public static final ic_mh_verified:I = 0x7f020090

.field public static final ic_my_food:I = 0x7f020091

.field public static final ic_servings_info_normal:I = 0x7f020092

.field public static final ic_spiral_ring:I = 0x7f020093

.field public static final ic_summary_apple:I = 0x7f020094

.field public static final ic_summary_help:I = 0x7f020095

.field public static final ic_summary_jog:I = 0x7f020096

.field public static final ic_summary_scale:I = 0x7f020097

.field public static final ic_tab_forums:I = 0x7f020098

.field public static final ic_tab_forums_normal:I = 0x7f020099

.field public static final ic_tab_forums_selected:I = 0x7f02009a

.field public static final ic_tab_track_it:I = 0x7f02009b

.field public static final ic_tab_track_it_normal:I = 0x7f02009c

.field public static final ic_tab_track_it_selected:I = 0x7f02009d

.field public static final ic_tbb_ge:I = 0x7f02009e

.field public static final ic_tbb_ge_selected:I = 0x7f02009f

.field public static final ic_tbb_medhelp:I = 0x7f0200a0

.field public static final ic_tbb_medhelp_selected:I = 0x7f0200a1

.field public static final ic_user_foods:I = 0x7f0200a2

.field public static final ic_water:I = 0x7f0200a3

.field public static final ic_weight:I = 0x7f0200a4

.field public static final ic_welcome_devices:I = 0x7f0200a5

.field public static final ic_welcome_lock:I = 0x7f0200a6

.field public static final ic_welcome_tracker:I = 0x7f0200a7

.field public static final img_serving_info:I = 0x7f0200a8

.field public static final logo_gehealthymagination:I = 0x7f0200a9

.field public static final logo_medhelp:I = 0x7f0200aa

.field public static final logo_medhelp_white:I = 0x7f0200ab

.field public static final tab_bottom_bar:I = 0x7f0200ac

.field public static final tab_normal:I = 0x7f0200ad

.field public static final tab_selected:I = 0x7f0200ae

.field public static final tab_text_color:I = 0x7f0200af

.field public static final tbb_line:I = 0x7f0200b0

.field public static final text_grey_toggle_btn:I = 0x7f0200b1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
