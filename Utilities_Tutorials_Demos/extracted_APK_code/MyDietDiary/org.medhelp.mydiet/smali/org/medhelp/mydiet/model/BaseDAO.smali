.class public abstract Lorg/medhelp/mydiet/model/BaseDAO;
.super Ljava/lang/Object;
.source "BaseDAO.java"


# instance fields
.field private listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/medhelp/mydiet/model/OnDataChangedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/medhelp/mydiet/model/BaseDAO;->listeners:Ljava/util/List;

    .line 11
    return-void
.end method


# virtual methods
.method public addOnDataChangedListener(Lorg/medhelp/mydiet/model/OnDataChangedListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 14
    iget-object v0, p0, Lorg/medhelp/mydiet/model/BaseDAO;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 15
    iget-object v0, p0, Lorg/medhelp/mydiet/model/BaseDAO;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    return-void
.end method

.method protected notifyDataChangedListeners()V
    .locals 3

    .prologue
    .line 23
    iget-object v1, p0, Lorg/medhelp/mydiet/model/BaseDAO;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 28
    return-void

    .line 23
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/OnDataChangedListener;

    .line 24
    .local v0, listener:Lorg/medhelp/mydiet/model/OnDataChangedListener;
    if-eqz v0, :cond_0

    .line 25
    invoke-interface {v0}, Lorg/medhelp/mydiet/model/OnDataChangedListener;->onDataChanged()V

    goto :goto_0
.end method

.method public removeOnDataChangedListener(Lorg/medhelp/mydiet/model/OnDataChangedListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 19
    iget-object v0, p0, Lorg/medhelp/mydiet/model/BaseDAO;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 20
    return-void
.end method
