.class public Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "PickExerciseActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;
    }
.end annotation


# static fields
.field private static final tag:Ljava/lang/String; = "############ PickExerciseActivity "


# instance fields
.field private mDate:Ljava/util/Date;

.field private mExercises:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Exercise;",
            ">;"
        }
    .end annotation
.end field

.field private mList:Landroid/widget/ListView;

.field mProgress:Landroid/widget/ProgressBar;

.field private onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 54
    new-instance v0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$1;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$1;-><init>(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 26
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mExercises:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)Ljava/util/Date;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mExercises:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 142
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->showSavingDialog()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 146
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->hideSavingDialog()V

    return-void
.end method

.method private finishAndShowDetails()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->setResult(I)V

    .line 99
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->finish()V

    .line 100
    return-void
.end method

.method private hideSavingDialog()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 148
    return-void
.end method

.method private showSavingDialog()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 144
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 87
    sparse-switch p1, :sswitch_data_0

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 90
    :sswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 91
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->finishAndShowDetails()V

    goto :goto_0

    .line 87
    nop

    :sswitch_data_0
    .sparse-switch
        0x7d0 -> :sswitch_0
        0xbb9 -> :sswitch_0
    .end sparse-switch
.end method

.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit p0

    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    .line 37
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v2, 0x7f030010

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->setContentView(I)V

    .line 40
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "date"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 41
    .local v0, timeInMillis:J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 42
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mDate:Ljava/util/Date;

    .line 47
    :goto_0
    const v2, 0x7f0b0001

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 48
    const v2, 0x7f0b003a

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mList:Landroid/widget/ListView;

    .line 49
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mList:Landroid/widget/ListView;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 51
    new-instance v2, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, ""

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 52
    return-void

    .line 44
    :cond_0
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->finish()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onStart()V

    .line 75
    return-void
.end method
