.class public Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "FinalizeMealActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;,
        Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SyncUserDataTask;
    }
.end annotation


# static fields
.field private static final TIME_PICKER:I


# instance fields
.field private mDate:Ljava/util/Date;

.field private mDialog:Landroid/app/AlertDialog;

.field private mMeal:Lorg/medhelp/mydiet/model/Meal;

.field private mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

.field private mRLMealType:Landroid/widget/RelativeLayout;

.field private mSelectFoodsLayout:Landroid/widget/LinearLayout;

.field private mTVCaloriesSummary:Landroid/widget/TextView;

.field private mTVMealType:Landroid/widget/TextView;

.field private mTVNotesSummary:Landroid/widget/TextView;

.field private mTVTimeResult:Landroid/widget/TextView;

.field private mTVWhere:Landroid/widget/TextView;

.field private mTVWithWhom:Landroid/widget/TextView;

.field private mTvDate:Landroid/widget/TextView;

.field private timeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 397
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$1;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->timeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    .line 60
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;
    .locals 1
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    return-object v0
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Ljava/util/Date;
    .locals 1
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$10(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 321
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setCaloriesText()V

    return-void
.end method

.method static synthetic access$11(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mSelectFoodsLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$12(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 458
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->refreshFoodItems()V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 303
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setMealTimeText()V

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 583
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->showSavingDialog()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 595
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->hideSavingDialog()V

    return-void
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 440
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->finishAndShowDetails()V

    return-void
.end method

.method static synthetic access$6(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 579
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->startUserDataSync()V

    return-void
.end method

.method static synthetic access$7(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Landroid/app/AlertDialog;
    .locals 1
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$8(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/adapter/MealTypesAdapter;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    return-object v0
.end method

.method static synthetic access$9(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVMealType:Landroid/widget/TextView;

    return-object v0
.end method

.method private finishAndShowDetails()V
    .locals 1

    .prologue
    .line 441
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setResult(I)V

    .line 442
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->finish()V

    .line 443
    return-void
.end method

.method private hideSavingDialog()V
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 599
    :cond_0
    return-void
.end method

.method private onAddAFoodClick()V
    .locals 4

    .prologue
    .line 417
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 418
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "date"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 419
    const-string v1, "meal_type"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/model/Meal;->getMealTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 420
    const-string v1, "add_more_foods"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 421
    const/16 v1, 0x3ec

    invoke-virtual {p0, v0, v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 422
    return-void
.end method

.method private onCaloriesClick()V
    .locals 11

    .prologue
    .line 250
    move-object v2, p0

    .line 251
    .local v2, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 252
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v7, "layout_inflater"

    invoke-virtual {v2, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 253
    .local v4, inflater:Landroid/view/LayoutInflater;
    const v8, 0x7f030009

    const v7, 0x7f0b0017

    invoke-virtual {p0, v7}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v4, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 254
    .local v6, v:Landroid/view/View;
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 255
    const-string v7, "Enter Calories"

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 257
    const v7, 0x7f0b0018

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 259
    .local v1, caloriesEditor:Landroid/widget/EditText;
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-wide v7, v7, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    const-wide/16 v9, 0x0

    cmpl-double v7, v7, v9

    if-lez v7, :cond_0

    .line 260
    new-instance v5, Ljava/text/DecimalFormatSymbols;

    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 261
    .local v5, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v5, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 262
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v7, "#"

    invoke-direct {v3, v7, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 263
    .local v3, df:Ljava/text/DecimalFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-wide v8, v8, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    invoke-virtual {v3, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 266
    .end local v3           #df:Ljava/text/DecimalFormat;
    .end local v5           #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_0
    new-instance v7, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$4;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$4;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 275
    const-string v7, "OK"

    new-instance v8, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$5;

    invoke-direct {v8, p0, v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$5;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 290
    const-string v7, "Cancel"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 292
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    .line 293
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 294
    return-void
.end method

.method private onLocationClick()V
    .locals 3

    .prologue
    .line 243
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 244
    .local v0, locationIntent:Landroid/content/Intent;
    const-string v1, "where"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const-string v1, "with_whom"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 247
    return-void
.end method

.method private onMealTypeClick()V
    .locals 8

    .prologue
    .line 200
    invoke-static {p0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v1

    .line 201
    .local v1, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v1, v6}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getAvailableMealTypesForDay(Ljava/util/Date;)Ljava/util/ArrayList;

    move-result-object v5

    .line 202
    .local v5, mealTypesSavedForDay:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v5, :cond_0

    new-instance v5, Ljava/util/ArrayList;

    .end local v5           #mealTypesSavedForDay:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 204
    .restart local v5       #mealTypesSavedForDay:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 205
    .local v4, mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "Breakfast"

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    const-string v6, "Lunch"

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    const-string v6, "Dinner"

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 217
    const-string v6, "Snack"

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    new-instance v6, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    invoke-direct {v6, p0, v4}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    .line 221
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 222
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    new-instance v7, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$3;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$3;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 231
    const-string v6, "Select a meal type"

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 232
    const-string v6, "cancel"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 234
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    .line 235
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 236
    return-void

    .line 209
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 210
    .local v3, mealTypeSaved:Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 211
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 212
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 210
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private onSaveClick()V
    .locals 6

    .prologue
    .line 425
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 427
    .local v1, foodsToSave:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 433
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 434
    new-instance v2, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, ""

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 438
    :goto_1
    return-void

    .line 427
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 428
    .local v0, food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-boolean v3, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    if-eqz v3, :cond_0

    .line 429
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 436
    .end local v0           #food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :cond_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->showDeleteMealDialog()V

    goto :goto_1
.end method

.method private onTimeClick()V
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->showDialog(I)V

    .line 240
    return-void
.end method

.method private refreshFoodItems()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 460
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mSelectFoodsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 462
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    .line 463
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 464
    .local v3, inflater:Landroid/view/LayoutInflater;
    const/4 v4, 0x0

    .line 465
    .local v4, llFoodItem:Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    .line 467
    .local v1, divider:Landroid/view/View;
    const/4 v6, 0x0

    .line 468
    .local v6, tv:Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 470
    .local v0, button:Landroid/widget/ImageButton;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v2, v7, :cond_1

    .line 523
    .end local v0           #button:Landroid/widget/ImageButton;
    .end local v1           #divider:Landroid/view/View;
    .end local v2           #i:I
    .end local v3           #inflater:Landroid/view/LayoutInflater;
    .end local v4           #llFoodItem:Landroid/widget/LinearLayout;
    .end local v6           #tv:Landroid/widget/TextView;
    :cond_0
    return-void

    .line 471
    .restart local v0       #button:Landroid/widget/ImageButton;
    .restart local v1       #divider:Landroid/view/View;
    .restart local v2       #i:I
    .restart local v3       #inflater:Landroid/view/LayoutInflater;
    .restart local v4       #llFoodItem:Landroid/widget/LinearLayout;
    .restart local v6       #tv:Landroid/widget/TextView;
    :cond_1
    const v7, 0x7f030019

    invoke-virtual {v3, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .end local v4           #llFoodItem:Landroid/widget/LinearLayout;
    check-cast v4, Landroid/widget/LinearLayout;

    .line 472
    .restart local v4       #llFoodItem:Landroid/widget/LinearLayout;
    const/16 v7, 0x10

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 474
    const v7, 0x7f0b000c

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6           #tv:Landroid/widget/TextView;
    check-cast v6, Landroid/widget/TextView;

    .line 476
    .restart local v6       #tv:Landroid/widget/TextView;
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-object v5, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    .line 477
    .local v5, title:Ljava/lang/String;
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 478
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 480
    :cond_2
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 481
    const v7, 0x7f0b000d

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6           #tv:Landroid/widget/TextView;
    check-cast v6, Landroid/widget/TextView;

    .line 482
    .restart local v6       #tv:Landroid/widget/TextView;
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-object v7, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 484
    const v7, 0x7f0b00c1

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #button:Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 485
    .restart local v0       #button:Landroid/widget/ImageButton;
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 486
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 488
    new-instance v7, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$6;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$6;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 504
    new-instance v7, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$7;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$7;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 516
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mSelectFoodsLayout:Landroid/widget/LinearLayout;

    mul-int/lit8 v8, v2, 0x2

    invoke-virtual {v7, v4, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 518
    const v7, 0x7f03001d

    invoke-virtual {v3, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 519
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, 0x1

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 520
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mSelectFoodsLayout:Landroid/widget/LinearLayout;

    mul-int/lit8 v8, v2, 0x2

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 470
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method private refreshMealsDetails()V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setMealTimeText()V

    .line 298
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setLocationContent()V

    .line 299
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setCaloriesText()V

    .line 300
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setNotesText()V

    .line 301
    return-void
.end method

.method private replaceFoodInlist(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V
    .locals 6
    .parameter "food"

    .prologue
    .line 446
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 447
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 449
    .local v1, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 456
    .end local v0           #i:I
    .end local v1           #size:I
    :cond_0
    return-void

    .line 450
    .restart local v0       #i:I
    .restart local v1       #size:I
    :cond_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    iget-wide v4, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 451
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 452
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v2, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 449
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setCaloriesText()V
    .locals 6

    .prologue
    .line 322
    new-instance v1, Ljava/text/DecimalFormatSymbols;

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "en"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 323
    .local v1, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 324
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v2, "#"

    invoke-direct {v0, v2, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 326
    .local v0, df:Ljava/text/DecimalFormat;
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 327
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVCaloriesSummary:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-wide v4, v4, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " calories"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVCaloriesSummary:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 333
    :goto_0
    return-void

    .line 330
    :cond_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVCaloriesSummary:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVCaloriesSummary:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setLocationContent()V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVWhere:Landroid/widget/TextView;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v1, v1, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVWithWhom:Landroid/widget/TextView;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v1, v1, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVWhere:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVWithWhom:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setMealTimeText()V
    .locals 4

    .prologue
    .line 304
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 305
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVTimeResult:Landroid/widget/TextView;

    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    const-string v2, "h:mm aa"

    invoke-static {v1, v2}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    :goto_0
    return-void

    .line 307
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVTimeResult:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setNotesText()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVNotesSummary:Landroid/widget/TextView;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v1, v1, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    :goto_0
    return-void

    .line 339
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVNotesSummary:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private showDeleteMealDialog()V
    .locals 3

    .prologue
    .line 602
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 603
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v1, "Delete Meal?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 605
    const-string v1, "You have not selected any foods. Do you want to delete this meal?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 607
    const-string v1, "Yes"

    new-instance v2, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$8;

    invoke-direct {v2, p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$8;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 616
    const-string v1, "Cancel"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 618
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    .line 619
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 620
    return-void
.end method

.method private showSavingDialog()V
    .locals 5

    .prologue
    .line 584
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 585
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v4, 0x7f03002c

    .line 586
    const v3, 0x7f0b0111

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 585
    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 588
    .local v2, layout:Landroid/view/View;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 589
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 590
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 591
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    .line 592
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 593
    return-void
.end method

.method private startUserDataSync()V
    .locals 4

    .prologue
    .line 580
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SyncUserDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SyncUserDataTask;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SyncUserDataTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SyncUserDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 581
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v6, -0x1

    .line 345
    packed-switch p1, :pswitch_data_0

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 347
    :pswitch_0
    if-ne p2, v6, :cond_0

    .line 348
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    const-string v7, "notes"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    .line 349
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setNotesText()V

    goto :goto_0

    .line 353
    :pswitch_1
    if-ne p2, v6, :cond_0

    .line 354
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    const-string v7, "where"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    .line 355
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    const-string v7, "with_whom"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;

    .line 356
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setLocationContent()V

    goto :goto_0

    .line 360
    :pswitch_2
    if-ne p2, v6, :cond_0

    .line 361
    const-string v6, "transient_foods"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 362
    .local v0, foodKey:Ljava/lang/String;
    invoke-static {v0}, Lorg/medhelp/mydiet/model/TransientDataStore;->retrieveData(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 364
    .local v4, resultFood:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    invoke-direct {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->replaceFoodInlist(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    .line 365
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->refreshFoodItems()V

    goto :goto_0

    .line 369
    .end local v0           #foodKey:Ljava/lang/String;
    .end local v4           #resultFood:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :pswitch_3
    if-ne p2, v6, :cond_0

    .line 370
    const-string v6, "transient_meal"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 371
    .local v5, transientMealKey:Ljava/lang/String;
    invoke-static {v5}, Lorg/medhelp/mydiet/model/TransientDataStore;->retrieveData(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 372
    .local v1, mealObject:Ljava/lang/Object;
    if-eqz v1, :cond_0

    move-object v3, v1

    .line 373
    check-cast v3, Lorg/medhelp/mydiet/model/Meal;

    .line 374
    .local v3, newMeal:Lorg/medhelp/mydiet/model/Meal;
    const/4 v1, 0x0

    .line 375
    iget-object v6, v3, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 376
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v6, v6, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    iget-object v7, v3, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Lorg/medhelp/mydiet/util/DataUtil;->mergeFoodItems(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 377
    .local v2, newFoods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iput-object v2, v6, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    .line 378
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->refreshFoodItems()V

    goto :goto_0

    .line 345
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 180
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 181
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v1, "Your changes haven\'t been saved. Do you want to continue?"

    .line 182
    .local v1, message:Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 184
    const-string v2, "Yes"

    new-instance v3, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$2;

    invoke-direct {v3, p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$2;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 193
    const-string v2, "Cancel"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 195
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    .line 196
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 197
    return-void
.end method

.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 177
    :goto_0
    monitor-exit p0

    return-void

    .line 153
    :sswitch_0
    :try_start_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->onMealTypeClick()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 157
    :sswitch_1
    :try_start_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->onAddAFoodClick()V

    goto :goto_0

    .line 160
    :sswitch_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->onTimeClick()V

    goto :goto_0

    .line 163
    :sswitch_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->onLocationClick()V

    goto :goto_0

    .line 166
    :sswitch_4
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->onCaloriesClick()V

    goto :goto_0

    .line 169
    :sswitch_5
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/NotesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 170
    .local v0, notesIntent:Landroid/content/Intent;
    const-string v1, "notes"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVNotesSummary:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 174
    .end local v0           #notesIntent:Landroid/content/Intent;
    :sswitch_6
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->onSaveClick()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 151
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0035 -> :sswitch_6
        0x7f0b00a9 -> :sswitch_0
        0x7f0b00ac -> :sswitch_1
        0x7f0b00ad -> :sswitch_1
        0x7f0b00ae -> :sswitch_2
        0x7f0b00b0 -> :sswitch_3
        0x7f0b00b3 -> :sswitch_4
        0x7f0b00b5 -> :sswitch_5
    .end sparse-switch
.end method

.method public declared-synchronized onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    const v4, 0x7f030016

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setContentView(I)V

    .line 88
    const v4, 0x7f0b00ab

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mSelectFoodsLayout:Landroid/widget/LinearLayout;

    .line 89
    const v4, 0x7f0b00a9

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mRLMealType:Landroid/widget/RelativeLayout;

    .line 90
    const v4, 0x7f0b00aa

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVMealType:Landroid/widget/TextView;

    .line 91
    const v4, 0x7f0b00af

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVTimeResult:Landroid/widget/TextView;

    .line 92
    const v4, 0x7f0b00b4

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVCaloriesSummary:Landroid/widget/TextView;

    .line 93
    const v4, 0x7f0b00b6

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVNotesSummary:Landroid/widget/TextView;

    .line 94
    const v4, 0x7f0b00b2

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVWhere:Landroid/widget/TextView;

    .line 95
    const v4, 0x7f0b00b1

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVWithWhom:Landroid/widget/TextView;

    .line 97
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mRLMealType:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const v4, 0x7f0b00b5

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v4, 0x7f0b00b3

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    const v4, 0x7f0b00b0

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    const v4, 0x7f0b00ae

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v4, 0x7f0b0035

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    const v4, 0x7f0b00ad

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    const v4, 0x7f0b00ac

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "transient_meal"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 108
    .local v3, transientMealKey:Ljava/lang/String;
    invoke-static {v3}, Lorg/medhelp/mydiet/model/TransientDataStore;->retrieveData(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 110
    .local v0, mealObject:Ljava/lang/Object;
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "date"

    const-wide/16 v6, -0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    .line 111
    .local v1, timeInMillis:J
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-lez v4, :cond_0

    .line 112
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;

    .line 115
    :cond_0
    if-nez v0, :cond_1

    .line 116
    const-string v4, "meal"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v0

    .line 117
    .local v0, mealObject:Lorg/medhelp/mydiet/model/Meal;
    new-instance v4, Ljava/util/Date;

    const-string v5, "date"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;

    .line 119
    .end local v0           #mealObject:Lorg/medhelp/mydiet/model/Meal;
    :cond_1
    check-cast v0, Lorg/medhelp/mydiet/model/Meal;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/Meal;->getCopy()Lorg/medhelp/mydiet/model/Meal;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    .line 121
    const v4, 0x7f0b0036

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTvDate:Landroid/widget/TextView;

    .line 122
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTvDate:Landroid/widget/TextView;

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;

    const-string v6, "EEEE MMM d, yyyy"

    invoke-static {v5, v6}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVMealType:Landroid/widget/TextView;

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    invoke-virtual {v5}, Lorg/medhelp/mydiet/model/Meal;->getMealTypeString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->refreshFoodItems()V

    .line 126
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->refreshMealsDetails()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-void

    .line 85
    .end local v1           #timeInMillis:J
    .end local v3           #transientMealKey:Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .parameter "id"

    .prologue
    .line 388
    packed-switch p1, :pswitch_data_0

    .line 394
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 390
    :pswitch_0
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v6

    .line 391
    .local v6, c:Ljava/util/Calendar;
    new-instance v0, Landroid/app/TimePickerDialog;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->timeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    const/16 v1, 0xb

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 392
    const/16 v1, 0xc

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x0

    move-object v1, p0

    .line 391
    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    goto :goto_0

    .line 388
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 146
    :cond_0
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 147
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 131
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 132
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter "outState"

    .prologue
    .line 136
    const-string v0, "meal"

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v0, "date"

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 139
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 140
    return-void
.end method
