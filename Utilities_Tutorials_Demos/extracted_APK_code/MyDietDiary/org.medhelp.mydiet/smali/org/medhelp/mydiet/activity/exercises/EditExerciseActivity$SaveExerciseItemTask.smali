.class Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;
.super Landroid/os/AsyncTask;
.source "EditExerciseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveExerciseItemTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 771
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 771
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 7
    .parameter "params"

    .prologue
    .line 784
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 786
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v3, v3, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v4, v4, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v4, v4, Lorg/medhelp/mydiet/model/Exercise;->name:Ljava/lang/String;

    iput-object v4, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    .line 788
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v3, v3, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    invoke-virtual {v0, v3}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->createOrUpdateExerciseItem(Lorg/medhelp/mydiet/model/ExerciseItem;)J

    move-result-wide v1

    .line 789
    .local v1, id:J
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v3, v3, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    .line 790
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    iget-object v3, v3, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iput-wide v1, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    .line 792
    :cond_0
    const/4 v3, 0x0

    return-object v3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 812
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->publishProgress([Ljava/lang/Object;)V

    .line 813
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->startUserDataSync()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$9(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    .line 814
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 815
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 778
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 779
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->publishProgress([Ljava/lang/Object;)V

    .line 780
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 797
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 798
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 808
    :goto_0
    return-void

    .line 800
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->showSavingDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$6(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    goto :goto_0

    .line 803
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->hideSavingDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$7(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    .line 804
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->refreshContentViews()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$5(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    .line 805
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->finishAndShowDetails()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->access$8(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    goto :goto_0

    .line 798
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
