.class Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;
.super Landroid/os/AsyncTask;
.source "PreferencesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/PreferencesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadWeightTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 227
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/PreferencesActivity;Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 227
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;-><init>(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->doInBackground([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 4
    .parameter "params"

    .prologue
    .line 240
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 241
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getRecentWeightOnOrBefore(Ljava/util/Date;)D

    move-result-wide v2

    iput-wide v2, v1, Lorg/medhelp/mydiet/activity/PreferencesActivity;->mWeight:D

    .line 242
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 260
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->publishProgress([Ljava/lang/Object;)V

    .line 261
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    #calls: Lorg/medhelp/mydiet/activity/PreferencesActivity;->updateBMRSummary()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->access$2(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V

    .line 262
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 263
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 234
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 235
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->publishProgress([Ljava/lang/Object;)V

    .line 236
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 247
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 248
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 256
    :goto_0
    return-void

    .line 250
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    #calls: Lorg/medhelp/mydiet/activity/PreferencesActivity;->showAlertDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->access$0(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V

    goto :goto_0

    .line 253
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/PreferencesActivity;

    #calls: Lorg/medhelp/mydiet/activity/PreferencesActivity;->hideAlertDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/PreferencesActivity;->access$1(Lorg/medhelp/mydiet/activity/PreferencesActivity;)V

    goto :goto_0

    .line 248
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/PreferencesActivity$LoadWeightTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
