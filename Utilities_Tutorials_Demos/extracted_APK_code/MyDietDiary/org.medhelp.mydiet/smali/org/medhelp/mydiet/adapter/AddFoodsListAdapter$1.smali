.class Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;
.super Ljava/lang/Object;
.source "AddFoodsListAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

.field private final synthetic val$foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;Lorg/medhelp/mydiet/model/FoodItemInMeal;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;->this$0:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    iput-object p2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;->val$foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .parameter "buttonView"
    .parameter "isChecked"

    .prologue
    .line 82
    if-eqz p2, :cond_0

    .line 83
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;->this$0:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;->val$foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->selectFoodItem(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    .line 84
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;->this$0:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->checkedFoodItem()V

    .line 89
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;->this$0:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;->val$foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->unselectFoodItem(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    .line 87
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;->this$0:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->uncheckedFoodItem()V

    goto :goto_0
.end method
