.class public Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "EditExerciseActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;,
        Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;,
        Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SyncUserDataTask;
    }
.end annotation


# static fields
.field private static final TIME_PICKER:I


# instance fields
.field private mDate:Ljava/util/Date;

.field mDialog:Landroid/app/AlertDialog;

.field mDistanceLayout:Landroid/widget/LinearLayout;

.field mExercise:Lorg/medhelp/mydiet/model/Exercise;

.field mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

.field mProgress:Landroid/widget/ProgressBar;

.field mTypeLayout:Landroid/widget/LinearLayout;

.field private mWeight:D

.field private timeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 231
    new-instance v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$1;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->timeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    .line 44
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 500
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setExerciseTimeText()V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)Ljava/util/Date;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$10(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;DDD)D
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 718
    invoke-direct/range {p0 .. p6}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->getCaloriesBurned(DDD)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$11(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;D)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 509
    invoke-direct {p0, p1, p2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setCalories(D)V

    return-void
.end method

.method static synthetic access$12(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;D)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 643
    invoke-direct {p0, p1, p2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setMinutes(D)V

    return-void
.end method

.method static synthetic access$13(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;D)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 524
    invoke-direct {p0, p1, p2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setDistance(D)V

    return-void
.end method

.method static synthetic access$14(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;D)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 584
    invoke-direct {p0, p1, p2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setSpeed(D)V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;D)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58
    iput-wide p1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mWeight:D

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 763
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->showProgressDialog()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 767
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->hideProgressDialog()V

    return-void
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 119
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->refreshContentViews()V

    return-void
.end method

.method static synthetic access$6(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 822
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->showSavingDialog()V

    return-void
.end method

.method static synthetic access$7(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 837
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->hideSavingDialog()V

    return-void
.end method

.method static synthetic access$8(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 114
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->finishAndShowDetails()V

    return-void
.end method

.method static synthetic access$9(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 818
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->startUserDataSync()V

    return-void
.end method

.method private finishAndShowDetails()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setResult(I)V

    .line 116
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->finish()V

    .line 117
    return-void
.end method

.method private getCaloriesBurned(DDD)D
    .locals 4
    .parameter "minutes"
    .parameter "weightInLb"
    .parameter "calorieCoefficient"

    .prologue
    .line 719
    mul-double v0, p1, p3

    mul-double/2addr v0, p5

    const-wide/high16 v2, 0x404e

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private hideProgressDialog()V
    .locals 2

    .prologue
    .line 768
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 769
    return-void
.end method

.method private hideSavingDialog()V
    .locals 1

    .prologue
    .line 838
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 839
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 841
    :cond_0
    return-void
.end method

.method private onClickCalories()V
    .locals 11

    .prologue
    .line 454
    move-object v1, p0

    .line 455
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 456
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v7, "layout_inflater"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 457
    .local v4, inflater:Landroid/view/LayoutInflater;
    const v8, 0x7f030009

    const v7, 0x7f0b0017

    invoke-virtual {p0, v7}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v4, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 458
    .local v6, v:Landroid/view/View;
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 459
    const-string v7, "Enter Calories"

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 461
    const v7, 0x7f0b0018

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 462
    .local v3, editor:Landroid/widget/EditText;
    const-string v7, "calories"

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 464
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v7, v7, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    const-wide/16 v9, 0x0

    cmpl-double v7, v7, v9

    if-lez v7, :cond_0

    .line 465
    new-instance v5, Ljava/text/DecimalFormatSymbols;

    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 466
    .local v5, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v5, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 467
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v7, "#.#"

    invoke-direct {v2, v7, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 468
    .local v2, df:Ljava/text/DecimalFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v8, v8, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    invoke-virtual {v2, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 471
    .end local v2           #df:Ljava/text/DecimalFormat;
    .end local v5           #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_0
    new-instance v7, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$9;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$9;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 480
    const-string v7, "OK"

    new-instance v8, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$10;

    invoke-direct {v8, p0, v3}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$10;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 494
    const-string v7, "Cancel"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 496
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    .line 497
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 498
    return-void
.end method

.method private onClickDistance()V
    .locals 14

    .prologue
    .line 349
    move-object v1, p0

    .line 350
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 351
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v9, "layout_inflater"

    invoke-virtual {v1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 352
    .local v4, inflater:Landroid/view/LayoutInflater;
    const v10, 0x7f03000a

    const v9, 0x7f0b0019

    invoke-virtual {p0, v9}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    invoke-virtual {v4, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 353
    .local v8, v:Landroid/view/View;
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 354
    const-string v9, "Enter Distance"

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 356
    const v9, 0x7f0b0018

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 357
    .local v3, editor:Landroid/widget/EditText;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDistanceUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 359
    .local v7, units:Ljava/lang/String;
    const v9, 0x7f0b001a

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 360
    .local v6, tv:Landroid/widget/TextView;
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v9, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    const-wide/16 v11, 0x0

    cmpl-double v9, v9, v11

    if-lez v9, :cond_0

    .line 363
    new-instance v5, Ljava/text/DecimalFormatSymbols;

    new-instance v9, Ljava/util/Locale;

    const-string v10, "en"

    const-string v11, "en"

    invoke-direct {v9, v10, v11}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v5, v9}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 364
    .local v5, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v9, 0x2e

    invoke-virtual {v5, v9}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 365
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v9, "#.#"

    invoke-direct {v2, v9, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 366
    .local v2, df:Ljava/text/DecimalFormat;
    const-string v9, "km"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 367
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v10, v10, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    const-wide v12, 0x3ff9bfdf7e8038a0L

    mul-double/2addr v10, v12

    invoke-virtual {v2, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 373
    .end local v2           #df:Ljava/text/DecimalFormat;
    .end local v5           #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_0
    :goto_0
    new-instance v9, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$5;

    invoke-direct {v9, p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$5;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 382
    const-string v9, "OK"

    new-instance v10, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;

    invoke-direct {v10, p0, v3, v7}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$6;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Landroid/widget/EditText;Ljava/lang/String;)V

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 399
    const-string v9, "Cancel"

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 401
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    iput-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    .line 402
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    .line 403
    return-void

    .line 369
    .restart local v2       #df:Ljava/text/DecimalFormat;
    .restart local v5       #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v10, v10, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    invoke-virtual {v2, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private onClickMinutes()V
    .locals 11

    .prologue
    .line 301
    move-object v1, p0

    .line 302
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 303
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v7, "layout_inflater"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 304
    .local v4, inflater:Landroid/view/LayoutInflater;
    const v8, 0x7f030009

    const v7, 0x7f0b0017

    invoke-virtual {p0, v7}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v4, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 305
    .local v6, v:Landroid/view/View;
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 306
    const-string v7, "Enter Minutes"

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 308
    const v7, 0x7f0b0018

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 310
    .local v3, editor:Landroid/widget/EditText;
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v7, v7, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    const-wide/16 v9, 0x0

    cmpl-double v7, v7, v9

    if-lez v7, :cond_0

    .line 311
    new-instance v5, Ljava/text/DecimalFormatSymbols;

    .line 312
    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    invoke-direct {v5, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 313
    .local v5, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v5, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 314
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v7, "#.#"

    invoke-direct {v2, v7, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 315
    .local v2, df:Ljava/text/DecimalFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v8, v8, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v2, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 318
    .end local v2           #df:Ljava/text/DecimalFormat;
    .end local v5           #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_0
    new-instance v7, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$3;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$3;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 327
    const-string v7, "OK"

    new-instance v8, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$4;

    invoke-direct {v8, p0, v3}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$4;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 342
    const-string v7, "Cancel"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 344
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    .line 345
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 346
    return-void
.end method

.method private onClickSpeed()V
    .locals 11

    .prologue
    .line 406
    move-object v1, p0

    .line 407
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 408
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v7, "layout_inflater"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 409
    .local v4, inflater:Landroid/view/LayoutInflater;
    const v8, 0x7f030009

    const v7, 0x7f0b0017

    invoke-virtual {p0, v7}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v4, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 410
    .local v6, v:Landroid/view/View;
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 411
    const-string v7, "Enter Speed"

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 413
    const v7, 0x7f0b0018

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 414
    .local v3, editor:Landroid/widget/EditText;
    const-string v7, "mph"

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 416
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v7, v7, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    const-wide/16 v9, 0x0

    cmpl-double v7, v7, v9

    if-lez v7, :cond_0

    .line 417
    new-instance v5, Ljava/text/DecimalFormatSymbols;

    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 418
    .local v5, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v5, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 419
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v7, "#.#"

    invoke-direct {v2, v7, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 420
    .local v2, df:Ljava/text/DecimalFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v8, v8, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-virtual {v2, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 423
    .end local v2           #df:Ljava/text/DecimalFormat;
    .end local v5           #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_0
    new-instance v7, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$7;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$7;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;)V

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 432
    const-string v7, "OK"

    new-instance v8, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$8;

    invoke-direct {v8, p0, v3}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$8;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 447
    const-string v7, "Cancel"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 449
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    .line 450
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 451
    return-void
.end method

.method private onClickTime()V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->showDialog(I)V

    .line 252
    return-void
.end method

.method private onClickType()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 255
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 257
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/Exercise;->exerciseTypes:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 258
    .local v3, size:I
    new-array v4, v3, [Ljava/lang/String;

    .line 260
    .local v4, types:[Ljava/lang/String;
    const/4 v2, 0x0

    .line 262
    .local v2, selectedItem:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-lt v1, v3, :cond_0

    .line 269
    invoke-virtual {v0, v4, v7}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 271
    new-instance v5, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;

    invoke-direct {v5, p0, v4}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$2;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;[Ljava/lang/String;)V

    invoke-virtual {v0, v4, v2, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 293
    const-string v5, "Select Exercise Type"

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 295
    const-string v5, "Cancel"

    invoke-virtual {v0, v5, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 296
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    .line 297
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 298
    return-void

    .line 263
    :cond_0
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/Exercise;->exerciseTypes:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/medhelp/mydiet/model/ExerciseType;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/ExerciseType;->name:Ljava/lang/String;

    aput-object v5, v4, v1

    .line 264
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v5, v5, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    aget-object v6, v4, v1

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 265
    move v2, v1

    .line 262
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private refreshContentViews()V
    .locals 13

    .prologue
    .line 120
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    if-nez v8, :cond_0

    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->finish()V

    .line 122
    :cond_0
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v9, v9, Lorg/medhelp/mydiet/model/Exercise;->name:Ljava/lang/String;

    iput-object v9, v8, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    .line 123
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    invoke-virtual {v9}, Lorg/medhelp/mydiet/model/Exercise;->getCCType()I

    move-result v9

    iput v9, v8, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    .line 125
    new-instance v6, Ljava/text/DecimalFormatSymbols;

    new-instance v8, Ljava/util/Locale;

    const-string v9, "en"

    const-string v10, "en"

    invoke-direct {v8, v9, v10}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v6, v8}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 126
    .local v6, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v8, 0x2e

    invoke-virtual {v6, v8}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 127
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v8, "#.#"

    invoke-direct {v2, v8, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 129
    .local v2, df:Ljava/text/DecimalFormat;
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v8, v8, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-gtz v8, :cond_1

    .line 130
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 131
    .local v0, c:Ljava/util/Calendar;
    new-instance v8, Ljava/util/Date;

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v9, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    invoke-direct {v8, v9, v10}, Ljava/util/Date;-><init>(J)V

    invoke-static {v8}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v1

    .line 132
    .local v1, c1:Ljava/util/Calendar;
    const/16 v8, 0xb

    const/16 v9, 0xb

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 133
    const/16 v8, 0xc

    const/16 v9, 0xc

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 135
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    .line 137
    .end local v0           #c:Ljava/util/Calendar;
    .end local v1           #c1:Ljava/util/Calendar;
    :cond_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setExerciseTimeText()V

    .line 139
    const v8, 0x7f0b0021

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 140
    .local v7, tv:Landroid/widget/TextView;
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v8, v8, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget v8, v8, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_4

    .line 143
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mTypeLayout:Landroid/widget/LinearLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 144
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDistanceLayout:Landroid/widget/LinearLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 146
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDistanceUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 148
    .local v3, distanceUnits:Ljava/lang/String;
    const v8, 0x7f0b002e

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7           #tv:Landroid/widget/TextView;
    check-cast v7, Landroid/widget/TextView;

    .line 149
    .restart local v7       #tv:Landroid/widget/TextView;
    const-string v8, "km"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 150
    new-instance v8, Ljava/lang/StringBuilder;

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v9, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    const-wide v11, 0x3ff9bfdf7e8038a0L

    mul-double/2addr v9, v11

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    :goto_0
    const-string v5, "mi/hr"

    .line 156
    .local v5, speedUnits:Ljava/lang/String;
    const v8, 0x7f0b0031

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7           #tv:Landroid/widget/TextView;
    check-cast v7, Landroid/widget/TextView;

    .line 157
    .restart local v7       #tv:Landroid/widget/TextView;
    const-string v8, "km"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 158
    const-string v5, "km/hr"

    .line 159
    new-instance v8, Ljava/lang/StringBuilder;

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v9, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    const-wide v11, 0x3ff9bfdf7e8038a0L

    mul-double/2addr v9, v11

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    .end local v3           #distanceUnits:Ljava/lang/String;
    .end local v5           #speedUnits:Ljava/lang/String;
    :goto_1
    const v8, 0x7f0b002a

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7           #tv:Landroid/widget/TextView;
    check-cast v7, Landroid/widget/TextView;

    .line 183
    .restart local v7       #tv:Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v9, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " min"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    const v8, 0x7f0b0034

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7           #tv:Landroid/widget/TextView;
    check-cast v7, Landroid/widget/TextView;

    .line 186
    .restart local v7       #tv:Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v9, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " calories"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    return-void

    .line 152
    .restart local v3       #distanceUnits:Ljava/lang/String;
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v9, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 161
    .restart local v5       #speedUnits:Ljava/lang/String;
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v9, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 164
    .end local v3           #distanceUnits:Ljava/lang/String;
    .end local v5           #speedUnits:Ljava/lang/String;
    :cond_4
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget v8, v8, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_6

    .line 165
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mTypeLayout:Landroid/widget/LinearLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 166
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDistanceLayout:Landroid/widget/LinearLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 168
    const v8, 0x7f0b0027

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7           #tv:Landroid/widget/TextView;
    check-cast v7, Landroid/widget/TextView;

    .line 169
    .restart local v7       #tv:Landroid/widget/TextView;
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v4, v8, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    .line 170
    .local v4, exerciseType:Ljava/lang/String;
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_5

    .line 171
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 173
    :cond_5
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v8, v8, Lorg/medhelp/mydiet/model/Exercise;->exerciseTypes:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/medhelp/mydiet/model/ExerciseType;

    iget-object v4, v8, Lorg/medhelp/mydiet/model/ExerciseType;->name:Ljava/lang/String;

    .line 174
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iput-object v4, v8, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    .line 175
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 178
    .end local v4           #exerciseType:Ljava/lang/String;
    :cond_6
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mTypeLayout:Landroid/widget/LinearLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 179
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDistanceLayout:Landroid/widget/LinearLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method private setCalories(D)V
    .locals 6
    .parameter "calories"

    .prologue
    .line 510
    new-instance v1, Ljava/text/DecimalFormatSymbols;

    new-instance v3, Ljava/util/Locale;

    const-string v4, "en"

    const-string v5, "en"

    invoke-direct {v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 511
    .local v1, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v3, 0x2e

    invoke-virtual {v1, v3}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 512
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v3, "#.#"

    invoke-direct {v0, v3, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 514
    .local v0, df:Ljava/text/DecimalFormat;
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iput-wide p1, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    .line 516
    const v3, 0x7f0b0034

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 517
    .local v2, tv:Landroid/widget/TextView;
    const-wide/16 v3, 0x0

    cmpl-double v3, p1, v3

    if-ltz v3, :cond_0

    .line 518
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " calories"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    :goto_0
    return-void

    .line 520
    :cond_0
    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setDistance(D)V
    .locals 18
    .parameter "distanceInMiles"

    .prologue
    .line 525
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    move-wide/from16 v0, p1

    iput-wide v0, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    .line 527
    new-instance v14, Ljava/text/DecimalFormatSymbols;

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "en"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v14, v2}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 528
    .local v14, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v2, 0x2e

    invoke-virtual {v14, v2}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 529
    new-instance v11, Ljava/text/DecimalFormat;

    const-string v2, "#.#"

    invoke-direct {v11, v2, v14}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 531
    .local v11, df:Ljava/text/DecimalFormat;
    const v2, 0x7f0b002e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 533
    .local v15, tv:Landroid/widget/TextView;
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDistanceUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 534
    .local v12, distanceUnits:Ljava/lang/String;
    const-string v13, "mi/hr"

    .line 536
    .local v13, speedUnits:Ljava/lang/String;
    const-string v2, "km"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 537
    const-string v13, "km/hr"

    .line 538
    new-instance v2, Ljava/lang/StringBuilder;

    const-wide v3, 0x3ff9bfdf7e8038a0L

    mul-double v3, v3, p1

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 543
    :goto_0
    const-wide/16 v2, 0x0

    cmpl-double v2, p1, v2

    if-nez v2, :cond_1

    .line 544
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    const-wide/16 v3, 0x0

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    .line 545
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    const-wide/16 v3, 0x0

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    .line 547
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    const-wide/16 v3, 0x0

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/Exercise;->distance:D

    .line 548
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    const-wide/16 v3, 0x0

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/Exercise;->speed:D

    .line 550
    const v2, 0x7f0b0031

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .end local v15           #tv:Landroid/widget/TextView;
    check-cast v15, Landroid/widget/TextView;

    .line 551
    .restart local v15       #tv:Landroid/widget/TextView;
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 553
    const v2, 0x7f0b002a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .end local v15           #tv:Landroid/widget/TextView;
    check-cast v15, Landroid/widget/TextView;

    .line 554
    .restart local v15       #tv:Landroid/widget/TextView;
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " min"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 582
    :goto_1
    return-void

    .line 540
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    move-wide/from16 v0, p1

    invoke-virtual {v11, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 559
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    const-wide/16 v16, 0x0

    cmpl-double v2, v2, v16

    if-lez v2, :cond_4

    .line 560
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    div-double v3, p1, v3

    const-wide/high16 v16, 0x404e

    mul-double v3, v3, v16

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    .line 561
    const v2, 0x7f0b0031

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .end local v15           #tv:Landroid/widget/TextView;
    check-cast v15, Landroid/widget/TextView;

    .line 563
    .restart local v15       #tv:Landroid/widget/TextView;
    const-string v2, "km"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 564
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    const-wide v16, 0x3ff9bfdf7e8038a0L

    mul-double v3, v3, v16

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 575
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/Exercise;->speed:D

    .line 577
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v2

    float-to-double v5, v2

    .line 578
    .local v5, weight:D
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/model/Exercise;->getCalorieCoefficient()D

    move-result-wide v7

    .line 579
    .local v7, calorieCoefficient:D
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->getCaloriesBurned(DDD)D

    move-result-wide v9

    .line 581
    .local v9, calories:D
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setCalories(D)V

    goto/16 :goto_1

    .line 566
    .end local v5           #weight:D
    .end local v7           #calorieCoefficient:D
    .end local v9           #calories:D
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 568
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    const-wide/16 v16, 0x0

    cmpl-double v2, v2, v16

    if-lez v2, :cond_2

    .line 569
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    div-double v3, p1, v3

    const-wide/high16 v16, 0x404e

    mul-double v3, v3, v16

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    .line 570
    const v2, 0x7f0b002a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .end local v15           #tv:Landroid/widget/TextView;
    check-cast v15, Landroid/widget/TextView;

    .line 571
    .restart local v15       #tv:Landroid/widget/TextView;
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " min"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method private setExerciseTimeText()V
    .locals 5

    .prologue
    .line 501
    const v1, 0x7f0b0024

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 502
    .local v0, tv:Landroid/widget/TextView;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 503
    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    const-string v2, "h:mm aa"

    invoke-static {v1, v2}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 507
    :goto_0
    return-void

    .line 505
    :cond_0
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setMinutes(D)V
    .locals 14
    .parameter "minutes"

    .prologue
    .line 644
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iput-wide p1, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    .line 646
    new-instance v12, Ljava/text/DecimalFormatSymbols;

    new-instance v0, Ljava/util/Locale;

    const-string v1, "en"

    const-string v2, "en"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v12, v0}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 647
    .local v12, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v0, 0x2e

    invoke-virtual {v12, v0}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 648
    new-instance v9, Ljava/text/DecimalFormat;

    const-string v0, "#.#"

    invoke-direct {v9, v0, v12}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 650
    .local v9, df:Ljava/text/DecimalFormat;
    const v0, 0x7f0b002a

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 651
    .local v13, tv:Landroid/widget/TextView;
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v9, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " min"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 653
    iget-wide v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mWeight:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 654
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mWeight:D

    .line 656
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/Exercise;->getCCType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 657
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/Exercise;->getCalorieCoefficient()D

    move-result-wide v5

    .line 658
    .local v5, calorieCoefficient:D
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    iget-wide v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mWeight:D

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->getCaloriesBurned(DDD)D

    move-result-wide v7

    .line 660
    .local v7, calories:D
    invoke-direct {p0, v7, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setCalories(D)V

    .line 716
    .end local v5           #calorieCoefficient:D
    .end local v7           #calories:D
    :goto_0
    return-void

    .line 661
    :cond_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/Exercise;->getCCType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 662
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/model/Exercise;->getCalorieCoefficientForType(Ljava/lang/String;)D

    move-result-wide v5

    .line 663
    .restart local v5       #calorieCoefficient:D
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    iget-wide v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mWeight:D

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->getCaloriesBurned(DDD)D

    move-result-wide v7

    .line 665
    .restart local v7       #calories:D
    invoke-direct {p0, v7, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setCalories(D)V

    goto :goto_0

    .line 666
    .end local v5           #calorieCoefficient:D
    .end local v7           #calories:D
    :cond_2
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/Exercise;->getCCType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_9

    .line 667
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDistanceUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 668
    .local v10, distanceUnits:Ljava/lang/String;
    const-string v11, "mi/hr"

    .line 669
    .local v11, speedUnits:Ljava/lang/String;
    const-string v0, "km"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 670
    const-string v11, "km/hr"

    .line 673
    :cond_3
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_4

    .line 674
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    .line 675
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    .line 677
    const v0, 0x7f0b002e

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .end local v13           #tv:Landroid/widget/TextView;
    check-cast v13, Landroid/widget/TextView;

    .line 678
    .restart local v13       #tv:Landroid/widget/TextView;
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    invoke-virtual {v9, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 680
    const v0, 0x7f0b0031

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .end local v13           #tv:Landroid/widget/TextView;
    check-cast v13, Landroid/widget/TextView;

    .line 681
    .restart local v13       #tv:Landroid/widget/TextView;
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-virtual {v9, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 686
    :cond_4
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_7

    .line 687
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    div-double/2addr v1, v3

    const-wide/high16 v3, 0x404e

    mul-double/2addr v1, v3

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    .line 689
    const v0, 0x7f0b0031

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .end local v13           #tv:Landroid/widget/TextView;
    check-cast v13, Landroid/widget/TextView;

    .line 690
    .restart local v13       #tv:Landroid/widget/TextView;
    const-string v0, "km"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 691
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    const-wide v3, 0x3ff9bfdf7e8038a0L

    mul-double/2addr v1, v3

    invoke-virtual {v9, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 707
    :cond_5
    :goto_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/Exercise;->speed:D

    .line 709
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/Exercise;->getCalorieCoefficient()D

    move-result-wide v5

    .line 710
    .restart local v5       #calorieCoefficient:D
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    iget-wide v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mWeight:D

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->getCaloriesBurned(DDD)D

    move-result-wide v7

    .line 712
    .restart local v7       #calories:D
    invoke-direct {p0, v7, v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setCalories(D)V

    goto/16 :goto_0

    .line 693
    .end local v5           #calorieCoefficient:D
    .end local v7           #calories:D
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-virtual {v9, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 695
    :cond_7
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_5

    .line 696
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    mul-double/2addr v1, v3

    const-wide/high16 v3, 0x404e

    div-double/2addr v1, v3

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    .line 698
    const v0, 0x7f0b002e

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .end local v13           #tv:Landroid/widget/TextView;
    check-cast v13, Landroid/widget/TextView;

    .line 699
    .restart local v13       #tv:Landroid/widget/TextView;
    const-string v0, "km"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 700
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    const-wide v3, 0x3ff9bfdf7e8038a0L

    mul-double/2addr v1, v3

    invoke-virtual {v9, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 702
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v1, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    invoke-virtual {v9, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 714
    .end local v10           #distanceUnits:Ljava/lang/String;
    .end local v11           #speedUnits:Ljava/lang/String;
    :cond_9
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setCalories(D)V

    goto/16 :goto_0
.end method

.method private setSpeed(D)V
    .locals 18
    .parameter "milesPerHour"

    .prologue
    .line 585
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    move-wide/from16 v0, p1

    iput-wide v0, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    .line 587
    new-instance v14, Ljava/text/DecimalFormatSymbols;

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "en"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v14, v2}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 588
    .local v14, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v2, 0x2e

    invoke-virtual {v14, v2}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 589
    new-instance v11, Ljava/text/DecimalFormat;

    const-string v2, "#.#"

    invoke-direct {v11, v2, v14}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 591
    .local v11, df:Ljava/text/DecimalFormat;
    const v2, 0x7f0b0031

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 593
    .local v15, tv:Landroid/widget/TextView;
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDistanceUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 594
    .local v12, distanceUnits:Ljava/lang/String;
    const-string v13, "mi/hr"

    .line 596
    .local v13, speedUnits:Ljava/lang/String;
    const-string v2, "km"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 597
    const-string v13, "km/hr"

    .line 598
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    const-wide v16, 0x3ff9bfdf7e8038a0L

    mul-double v3, v3, v16

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    :goto_0
    const-wide/16 v2, 0x0

    cmpl-double v2, p1, v2

    if-nez v2, :cond_1

    .line 605
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    const-wide/16 v3, 0x0

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    .line 606
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    const-wide/16 v3, 0x0

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    .line 608
    const v2, 0x7f0b002e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .end local v15           #tv:Landroid/widget/TextView;
    check-cast v15, Landroid/widget/TextView;

    .line 609
    .restart local v15       #tv:Landroid/widget/TextView;
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 611
    const v2, 0x7f0b002a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .end local v15           #tv:Landroid/widget/TextView;
    check-cast v15, Landroid/widget/TextView;

    .line 612
    .restart local v15       #tv:Landroid/widget/TextView;
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " min"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 641
    :goto_1
    return-void

    .line 600
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 617
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    const-wide/16 v16, 0x0

    cmpl-double v2, v2, v16

    if-lez v2, :cond_4

    .line 618
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    mul-double v3, v3, p1

    const-wide/high16 v16, 0x404e

    div-double v3, v3, v16

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    .line 620
    const v2, 0x7f0b002e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .end local v15           #tv:Landroid/widget/TextView;
    check-cast v15, Landroid/widget/TextView;

    .line 621
    .restart local v15       #tv:Landroid/widget/TextView;
    const-string v2, "km"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 622
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    const-wide v16, 0x3ff9bfdf7e8038a0L

    mul-double v3, v3, v16

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 634
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/Exercise;->speed:D

    .line 636
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v2

    float-to-double v5, v2

    .line 637
    .local v5, weight:D
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExercise:Lorg/medhelp/mydiet/model/Exercise;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/model/Exercise;->getCalorieCoefficient()D

    move-result-wide v7

    .line 638
    .local v7, calorieCoefficient:D
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->getCaloriesBurned(DDD)D

    move-result-wide v9

    .line 640
    .local v9, calories:D
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setCalories(D)V

    goto/16 :goto_1

    .line 624
    .end local v5           #weight:D
    .end local v7           #calorieCoefficient:D
    .end local v9           #calories:D
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 626
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    const-wide/16 v16, 0x0

    cmpl-double v2, v2, v16

    if-lez v2, :cond_2

    .line 627
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    move-wide/from16 v16, v0

    div-double v3, v3, v16

    const-wide/high16 v16, 0x404e

    mul-double v3, v3, v16

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    .line 629
    const v2, 0x7f0b002a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .end local v15           #tv:Landroid/widget/TextView;
    check-cast v15, Landroid/widget/TextView;

    .line 630
    .restart local v15       #tv:Landroid/widget/TextView;
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v11, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " min"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method private showProgressDialog()V
    .locals 2

    .prologue
    .line 764
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 765
    return-void
.end method

.method private showSavingDialog()V
    .locals 6

    .prologue
    .line 823
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 824
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v5, 0x7f03002c

    .line 825
    const v4, 0x7f0b0111

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 824
    invoke-virtual {v1, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 827
    .local v2, layout:Landroid/view/View;
    const v4, 0x7f0b001e

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 828
    .local v3, tv:Landroid/widget/TextView;
    const-string v4, "Saving..."

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 830
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 831
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 832
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 833
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    .line 834
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 835
    return-void
.end method

.method private startUserDataSync()V
    .locals 4

    .prologue
    .line 819
    new-instance v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SyncUserDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SyncUserDataTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SyncUserDataTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SyncUserDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 820
    return-void
.end method


# virtual methods
.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 215
    :goto_0
    monitor-exit p0

    return-void

    .line 194
    :sswitch_0
    :try_start_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->onClickTime()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 197
    :sswitch_1
    :try_start_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->onClickType()V

    goto :goto_0

    .line 200
    :sswitch_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->onClickMinutes()V

    goto :goto_0

    .line 203
    :sswitch_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->onClickDistance()V

    goto :goto_0

    .line 206
    :sswitch_4
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->onClickSpeed()V

    goto :goto_0

    .line 209
    :sswitch_5
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->onClickCalories()V

    goto :goto_0

    .line 212
    :sswitch_6
    new-instance v0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$SaveExerciseItemTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 192
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0022 -> :sswitch_0
        0x7f0b0026 -> :sswitch_1
        0x7f0b0028 -> :sswitch_2
        0x7f0b002c -> :sswitch_3
        0x7f0b002f -> :sswitch_4
        0x7f0b0032 -> :sswitch_5
        0x7f0b0035 -> :sswitch_6
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    .line 64
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    const v2, 0x7f03000e

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->setContentView(I)V

    .line 67
    const v2, 0x7f0b0001

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 69
    const v2, 0x7f0b0025

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mTypeLayout:Landroid/widget/LinearLayout;

    .line 70
    const v2, 0x7f0b002b

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDistanceLayout:Landroid/widget/LinearLayout;

    .line 72
    const v2, 0x7f0b0022

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    const v2, 0x7f0b0026

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    const v2, 0x7f0b0028

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    const v2, 0x7f0b002c

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    const v2, 0x7f0b002f

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    const v2, 0x7f0b0032

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    const v2, 0x7f0b0035

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "transient_exercise_item"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, transientEIKey:Ljava/lang/String;
    invoke-static {v1}, Lorg/medhelp/mydiet/model/TransientDataStore;->retrieveData(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 82
    .local v0, obj:Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 83
    const-string v2, "exercise_item"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/medhelp/mydiet/util/DataUtil;->getExerciseItem(Ljava/lang/String;)Lorg/medhelp/mydiet/model/ExerciseItem;

    move-result-object v0

    .line 85
    .end local v0           #obj:Ljava/lang/Object;
    :cond_0
    check-cast v0, Lorg/medhelp/mydiet/model/ExerciseItem;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    .line 87
    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDate:Ljava/util/Date;

    .line 88
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDate:Ljava/util/Date;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    .line 89
    :cond_1
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->finish()V

    .line 92
    :cond_2
    new-instance v2, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, ""

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity$LoadExerciseTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 93
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 9
    .parameter "id"

    .prologue
    .line 218
    packed-switch p1, :pswitch_data_0

    .line 228
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 220
    :pswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v7, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    .line 221
    .local v7, time:J
    const-wide/16 v0, 0x0

    cmp-long v0, v7, v0

    if-gtz v0, :cond_0

    .line 222
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    .line 224
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v6

    .line 225
    .local v6, c:Ljava/util/Calendar;
    new-instance v0, Landroid/app/TimePickerDialog;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->timeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    const/16 v1, 0xb

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 226
    const/16 v1, 0xc

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x0

    move-object v1, p0

    .line 225
    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    goto :goto_0

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 101
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 105
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 106
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 110
    const-string v0, "exercise_item"

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;->mExerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/ExerciseItem;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onStart()V

    .line 97
    return-void
.end method
