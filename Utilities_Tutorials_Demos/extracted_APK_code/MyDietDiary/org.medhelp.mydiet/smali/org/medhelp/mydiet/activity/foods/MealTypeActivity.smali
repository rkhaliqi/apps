.class public Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "MealTypeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field mDate:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 68
    :goto_0
    const/4 v4, -0x1

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->setResult(I)V

    .line 69
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->finish()V

    .line 70
    return-void

    .line 44
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v4, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {v0, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 45
    .local v0, addBreakFastIntent:Landroid/content/Intent;
    const-string v4, "date"

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 46
    const-string v4, "meal_type"

    const-string v5, "Breakfast"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 50
    .end local v0           #addBreakFastIntent:Landroid/content/Intent;
    :pswitch_1
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 51
    .local v2, addLunchIntent:Landroid/content/Intent;
    const-string v4, "date"

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 52
    const-string v4, "meal_type"

    const-string v5, "Lunch"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 56
    .end local v2           #addLunchIntent:Landroid/content/Intent;
    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 57
    .local v1, addDinnerIntent:Landroid/content/Intent;
    const-string v4, "date"

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 58
    const-string v4, "meal_type"

    const-string v5, "Dinner"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 62
    .end local v1           #addDinnerIntent:Landroid/content/Intent;
    :pswitch_3
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 63
    .local v3, addSnackIntent:Landroid/content/Intent;
    const-string v4, "date"

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 64
    const-string v4, "meal_type"

    const-string v5, "Snack"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b00b7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 23
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v2, 0x7f030017

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->setContentView(I)V

    .line 26
    const v2, 0x7f0b00b7

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    const v2, 0x7f0b00b8

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    const v2, 0x7f0b00b9

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    const v2, 0x7f0b00ba

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "date"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 33
    .local v0, timeInMillis:J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 34
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->mDate:Ljava/util/Date;

    .line 38
    :goto_0
    return-void

    .line 36
    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getTodayMidnightInMilliseconds()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;->mDate:Ljava/util/Date;

    goto :goto_0
.end method
