.class public Lorg/medhelp/mydiet/model/DBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DBHelper.java"

# interfaces
.implements Lorg/medhelp/mydiet/C$data;


# static fields
.field private static final DB_NAME:Ljava/lang/String; = "diet_diary.sqlite"

.field private static final DB_VERSION:I = 0x4

.field private static dbHelper:Lorg/medhelp/mydiet/model/DBHelper;


# instance fields
.field private final CREATE_EXERCISES_TABLE:Ljava/lang/String;

.field private final CREATE_EXERCISE_ITEMS_TABLE:Ljava/lang/String;

.field private final CREATE_FOODS_TABLE:Ljava/lang/String;

.field private final CREATE_MEALS_TABLE:Ljava/lang/String;

.field private final CREATE_SYNC_TRAC_TABLE:Ljava/lang/String;

.field private final CREATE_WATER_TABLE:Ljava/lang/String;

.field private final CREATE_WEIGHT_TABLE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lorg/medhelp/mydiet/model/DBHelper;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    .line 11
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    .line 111
    const-string v0, "diet_diary.sqlite"

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 38
    const-string v0, "CREATE TABLE IF NOT EXISTS meals ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date INTEGER NOT NULL, breakfast TEXT, lunch TEXT, dinner TEXT, snacks TEXT, updated_at TEXT, last_updated_time INTEGER NOT NULL);"

    iput-object v0, p0, Lorg/medhelp/mydiet/model/DBHelper;->CREATE_MEALS_TABLE:Ljava/lang/String;

    .line 48
    const-string v0, "CREATE TABLE IF NOT EXISTS foods ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, medhelp_id INTEGER, name TEXT NOT NULL, summary TEXT, food_group_id INTEGER, food TEXT, servings TEXT, calories REAL, serving_id INTEGER NOT NULL DEFAULT 0, amount REAL NOT NULL DEFAULT 0, updated_at TEXT, last_accessed_time INTEGER NOT NULL DEFAULT 0, access_count INTEGER NOT NULL DEFAULT 0, last_updated_time INTEGER NOT NULL );"

    iput-object v0, p0, Lorg/medhelp/mydiet/model/DBHelper;->CREATE_FOODS_TABLE:Ljava/lang/String;

    .line 65
    const-string v0, "CREATE TABLE IF NOT EXISTS exercise_items ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date INTEGER NOT NULL, exercise_id INTEGER, name TEXT, medhelp_id TEXT, exercise_type TEXT, distance REAL, speed REAL, calories REAL, time INTEGER, time_period REAL, updated_at TEXT, last_updated_time INTEGER NOT NULL);"

    iput-object v0, p0, Lorg/medhelp/mydiet/model/DBHelper;->CREATE_EXERCISE_ITEMS_TABLE:Ljava/lang/String;

    .line 80
    const-string v0, "CREATE TABLE IF NOT EXISTS exercises ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, medhelp_id TEXT, name TEXT NOT NULL, exercise_json TEXT, updated_at TEXT, last_updated_time INTEGER NOT NULL );"

    iput-object v0, p0, Lorg/medhelp/mydiet/model/DBHelper;->CREATE_EXERCISES_TABLE:Ljava/lang/String;

    .line 88
    const-string v0, "CREATE TABLE IF NOT EXISTS weight ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date INTEGER NOT NULL, weight REAL NOT NULL, last_updated_time INTEGER NOT NULL );"

    iput-object v0, p0, Lorg/medhelp/mydiet/model/DBHelper;->CREATE_WEIGHT_TABLE:Ljava/lang/String;

    .line 94
    const-string v0, "CREATE TABLE IF NOT EXISTS water ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date INTEGER NOT NULL, amount REAL NOT NULL, last_updated_time INTEGER NOT NULL );"

    iput-object v0, p0, Lorg/medhelp/mydiet/model/DBHelper;->CREATE_WATER_TABLE:Ljava/lang/String;

    .line 100
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_tracker ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, month_year_timestamp INTEGER NOT NULL DEFAULT 0, last_sync_time INTEGER NOT NULL DEFAULT 0, last_sync_meals INTEGER NOT NULL DEFAULT 0, last_sync_exercises INTEGER NOT NULL DEFAULT 0, last_sync_water INTEGER NOT NULL DEFAULT 0, last_sync_weight INTEGER NOT NULL DEFAULT 0, last_updated_time INTEGER NOT NULL );"

    iput-object v0, p0, Lorg/medhelp/mydiet/model/DBHelper;->CREATE_SYNC_TRAC_TABLE:Ljava/lang/String;

    .line 112
    iput-object p1, p0, Lorg/medhelp/mydiet/model/DBHelper;->mContext:Landroid/content/Context;

    .line 113
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DBHelper;
    .locals 2
    .parameter "context"

    .prologue
    .line 116
    const-class v1, Lorg/medhelp/mydiet/model/DBHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/medhelp/mydiet/model/DBHelper;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lorg/medhelp/mydiet/model/DBHelper;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/model/DBHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/medhelp/mydiet/model/DBHelper;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    .line 120
    :cond_0
    sget-object v0, Lorg/medhelp/mydiet/model/DBHelper;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected clearTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 2
    .parameter "db"
    .parameter "table"

    .prologue
    .line 168
    const-string v0, "1"

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .parameter "db"

    .prologue
    .line 157
    const-string v0, "CREATE TABLE IF NOT EXISTS meals ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date INTEGER NOT NULL, breakfast TEXT, lunch TEXT, dinner TEXT, snacks TEXT, updated_at TEXT, last_updated_time INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 158
    const-string v0, "CREATE TABLE IF NOT EXISTS foods ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, medhelp_id INTEGER, name TEXT NOT NULL, summary TEXT, food_group_id INTEGER, food TEXT, servings TEXT, calories REAL, serving_id INTEGER NOT NULL DEFAULT 0, amount REAL NOT NULL DEFAULT 0, updated_at TEXT, last_accessed_time INTEGER NOT NULL DEFAULT 0, access_count INTEGER NOT NULL DEFAULT 0, last_updated_time INTEGER NOT NULL );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 159
    const-string v0, "CREATE TABLE IF NOT EXISTS exercise_items ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date INTEGER NOT NULL, exercise_id INTEGER, name TEXT, medhelp_id TEXT, exercise_type TEXT, distance REAL, speed REAL, calories REAL, time INTEGER, time_period REAL, updated_at TEXT, last_updated_time INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 160
    const-string v0, "CREATE TABLE IF NOT EXISTS exercises ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, medhelp_id TEXT, name TEXT NOT NULL, exercise_json TEXT, updated_at TEXT, last_updated_time INTEGER NOT NULL );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 161
    const-string v0, "CREATE TABLE IF NOT EXISTS weight ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date INTEGER NOT NULL, weight REAL NOT NULL, last_updated_time INTEGER NOT NULL );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 162
    const-string v0, "CREATE TABLE IF NOT EXISTS water ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date INTEGER NOT NULL, amount REAL NOT NULL, last_updated_time INTEGER NOT NULL );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 164
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_tracker ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, month_year_timestamp INTEGER NOT NULL DEFAULT 0, last_sync_time INTEGER NOT NULL DEFAULT 0, last_sync_meals INTEGER NOT NULL DEFAULT 0, last_sync_exercises INTEGER NOT NULL DEFAULT 0, last_sync_water INTEGER NOT NULL DEFAULT 0, last_sync_weight INTEGER NOT NULL DEFAULT 0, last_updated_time INTEGER NOT NULL );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 165
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .parameter "db"

    .prologue
    .line 125
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/model/DBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 126
    iget-object v0, p0, Lorg/medhelp/mydiet/model/DBHelper;->mContext:Landroid/content/Context;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/DBSetupUtil;->setOldDBVersion(Landroid/content/Context;I)V

    .line 127
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 7
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    const/4 v6, 0x2

    .line 131
    iget-object v5, p0, Lorg/medhelp/mydiet/model/DBHelper;->mContext:Landroid/content/Context;

    invoke-static {v5, p2}, Lorg/medhelp/mydiet/util/DBSetupUtil;->setOldDBVersion(Landroid/content/Context;I)V

    .line 133
    const/4 v5, 0x1

    if-ne p2, v5, :cond_0

    if-lt p3, v6, :cond_0

    .line 134
    const-string v1, "ALTER TABLE foods ADD COLUMN last_accessed_time INTEGER NOT NULL DEFAULT 0;"

    .line 135
    .local v1, ALTER_FOODS_TABLE_1:Ljava/lang/String;
    const-string v2, "ALTER TABLE foods ADD COLUMN access_count INTEGER NOT NULL DEFAULT 0;"

    .line 136
    .local v2, ALTER_FOODS_TABLE_2:Ljava/lang/String;
    const-string v3, "ALTER TABLE foods ADD COLUMN serving_id INTEGER NOT NULL DEFAULT 0;"

    .line 137
    .local v3, ALTER_FOODS_TABLE_3:Ljava/lang/String;
    const-string v4, "ALTER TABLE foods ADD COLUMN amount REAL NOT NULL DEFAULT 0;"

    .line 139
    .local v4, ALTER_FOODS_TABLE_4:Ljava/lang/String;
    const-string v5, "ALTER TABLE foods ADD COLUMN last_accessed_time INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 140
    const-string v5, "ALTER TABLE foods ADD COLUMN access_count INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 141
    const-string v5, "ALTER TABLE foods ADD COLUMN serving_id INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 142
    const-string v5, "ALTER TABLE foods ADD COLUMN amount REAL NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 145
    .end local v1           #ALTER_FOODS_TABLE_1:Ljava/lang/String;
    .end local v2           #ALTER_FOODS_TABLE_2:Ljava/lang/String;
    .end local v3           #ALTER_FOODS_TABLE_3:Ljava/lang/String;
    .end local v4           #ALTER_FOODS_TABLE_4:Ljava/lang/String;
    :cond_0
    if-gt p2, v6, :cond_1

    if-le p3, v6, :cond_1

    .line 146
    const-string v5, "CREATE TABLE IF NOT EXISTS sync_tracker ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, month_year_timestamp INTEGER NOT NULL DEFAULT 0, last_sync_time INTEGER NOT NULL DEFAULT 0, last_sync_meals INTEGER NOT NULL DEFAULT 0, last_sync_exercises INTEGER NOT NULL DEFAULT 0, last_sync_water INTEGER NOT NULL DEFAULT 0, last_sync_weight INTEGER NOT NULL DEFAULT 0, last_updated_time INTEGER NOT NULL );"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 149
    :cond_1
    const/4 v5, 0x3

    if-gt p2, v5, :cond_2

    const/4 v5, 0x4

    if-lt p3, v5, :cond_2

    .line 150
    const-string v0, "ALTER TABLE exercise_items ADD COLUMN medhelp_id TEXT;"

    .line 152
    .local v0, ALTER_EI_TABLE:Ljava/lang/String;
    const-string v5, "ALTER TABLE exercise_items ADD COLUMN medhelp_id TEXT;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 154
    .end local v0           #ALTER_EI_TABLE:Ljava/lang/String;
    :cond_2
    return-void
.end method
