.class public interface abstract Lorg/medhelp/mydiet/C$requestCodes;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "requestCodes"
.end annotation


# static fields
.field public static final ADD_MORE_FOODS:I = 0x3ec

.field public static final CREATE_FOOD:I = 0x3ed

.field public static final EDIT_FOOD:I = 0x3ee

.field public static final FOOD_DETAIL:I = 0x3eb

.field public static final LOGIN:I = 0xfa0

.field public static final MEAL_LOCATION:I = 0x3ea

.field public static final NOTES:I = 0x3e9

.field public static final PICK_EXERCISE:I = 0x7d0

.field public static final SHOW_DETAILS:I = 0xbb9

.field public static final SIGNUP:I = 0xfa1
