.class public Lorg/medhelp/mydiet/model/Exercise;
.super Ljava/lang/Object;
.source "Exercise.java"


# static fields
.field public static final CC_EXERCISE_TYPE:I = 0x2

.field public static final CC_NONE:I = 0x0

.field public static final CC_NORMAL:I = 0x1

.field public static final CC_RANGE:I = 0x3


# instance fields
.field public calorieCoefficientType:I

.field public ccRanges:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/CalorieCoefficientRange;",
            ">;"
        }
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public distance:D

.field public exerciseJSON:Ljava/lang/String;

.field public exerciseType:Ljava/lang/String;

.field public exerciseTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/ExerciseType;",
            ">;"
        }
    .end annotation
.end field

.field public hasDistance:Z

.field public id:J

.field public inAppId:J

.field public maxDistance:D

.field public maxSpeed:D

.field public medHelpId:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public normalCalorieCoefficient:D

.field public speed:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCCType()I
    .locals 4

    .prologue
    .line 39
    iget-boolean v0, p0, Lorg/medhelp/mydiet/model/Exercise;->hasDistance:Z

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x3

    iput v0, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    .line 51
    :goto_0
    iget v0, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    return v0

    .line 42
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/model/Exercise;->exerciseTypes:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/medhelp/mydiet/model/Exercise;->exerciseTypes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 43
    const/4 v0, 0x2

    iput v0, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    goto :goto_0

    .line 44
    :cond_1
    iget-wide v0, p0, Lorg/medhelp/mydiet/model/Exercise;->normalCalorieCoefficient:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_2

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    goto :goto_0

    .line 47
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    goto :goto_0
.end method

.method public getCalorieCoefficient()D
    .locals 9

    .prologue
    const-wide/high16 v2, -0x4010

    .line 65
    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/Exercise;->getCCType()I

    move-result v4

    iput v4, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    .line 67
    iget v4, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 68
    iget-wide v2, p0, Lorg/medhelp/mydiet/model/Exercise;->normalCalorieCoefficient:D

    .line 98
    :cond_0
    :goto_0
    return-wide v2

    .line 71
    :cond_1
    iget v4, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 72
    iget-object v4, p0, Lorg/medhelp/mydiet/model/Exercise;->exerciseType:Ljava/lang/String;

    if-nez v4, :cond_2

    const-wide/16 v2, 0x0

    goto :goto_0

    .line 74
    :cond_2
    iget-object v4, p0, Lorg/medhelp/mydiet/model/Exercise;->exerciseTypes:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_7

    .line 81
    :cond_4
    iget v4, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_6

    .line 82
    iget-object v4, p0, Lorg/medhelp/mydiet/model/Exercise;->ccRanges:Ljava/util/ArrayList;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lorg/medhelp/mydiet/model/Exercise;->ccRanges:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_b

    .line 83
    iget-object v4, p0, Lorg/medhelp/mydiet/model/Exercise;->ccRanges:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_8

    .line 94
    :cond_6
    iget v4, p0, Lorg/medhelp/mydiet/model/Exercise;->calorieCoefficientType:I

    if-nez v4, :cond_0

    goto :goto_0

    .line 74
    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/ExerciseType;

    .line 75
    .local v1, eType:Lorg/medhelp/mydiet/model/ExerciseType;
    iget-object v5, p0, Lorg/medhelp/mydiet/model/Exercise;->exerciseType:Ljava/lang/String;

    iget-object v6, v1, Lorg/medhelp/mydiet/model/ExerciseType;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 76
    iget-wide v2, v1, Lorg/medhelp/mydiet/model/ExerciseType;->calorieCoefficient:D

    goto :goto_0

    .line 83
    .end local v1           #eType:Lorg/medhelp/mydiet/model/ExerciseType;
    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;

    .line 84
    .local v0, ccRange:Lorg/medhelp/mydiet/model/CalorieCoefficientRange;
    iget-boolean v5, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->isOutOfRange:Z

    if-eqz v5, :cond_a

    .line 85
    iget-wide v5, p0, Lorg/medhelp/mydiet/model/Exercise;->speed:D

    iget-wide v7, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->minSpeed:D

    cmpg-double v5, v5, v7

    if-ltz v5, :cond_9

    iget-wide v5, p0, Lorg/medhelp/mydiet/model/Exercise;->speed:D

    iget-wide v7, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->maxSpeed:D

    cmpl-double v5, v5, v7

    if-lez v5, :cond_a

    :cond_9
    iget-wide v2, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->calorieCoefficient:D

    goto :goto_0

    .line 87
    :cond_a
    iget-wide v5, p0, Lorg/medhelp/mydiet/model/Exercise;->speed:D

    iget-wide v7, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->minSpeed:D

    cmpl-double v5, v5, v7

    if-ltz v5, :cond_5

    iget-wide v5, p0, Lorg/medhelp/mydiet/model/Exercise;->speed:D

    iget-wide v7, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->maxSpeed:D

    cmpg-double v5, v5, v7

    if-gtz v5, :cond_5

    iget-wide v2, v0, Lorg/medhelp/mydiet/model/CalorieCoefficientRange;->calorieCoefficient:D

    goto :goto_0

    .line 90
    .end local v0           #ccRange:Lorg/medhelp/mydiet/model/CalorieCoefficientRange;
    :cond_b
    iget-wide v2, p0, Lorg/medhelp/mydiet/model/Exercise;->normalCalorieCoefficient:D

    goto :goto_0
.end method

.method public getCalorieCoefficientForType(Ljava/lang/String;)D
    .locals 5
    .parameter "exerciseType"

    .prologue
    const-wide/16 v1, 0x0

    .line 55
    if-nez p1, :cond_1

    .line 61
    :cond_0
    :goto_0
    return-wide v1

    .line 56
    :cond_1
    iget-object v3, p0, Lorg/medhelp/mydiet/model/Exercise;->exerciseTypes:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/ExerciseType;

    .line 57
    .local v0, eType:Lorg/medhelp/mydiet/model/ExerciseType;
    iget-object v4, v0, Lorg/medhelp/mydiet/model/ExerciseType;->name:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 58
    iget-wide v1, v0, Lorg/medhelp/mydiet/model/ExerciseType;->calorieCoefficient:D

    goto :goto_0
.end method
