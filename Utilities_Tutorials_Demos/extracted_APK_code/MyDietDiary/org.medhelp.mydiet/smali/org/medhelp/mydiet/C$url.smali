.class public final Lorg/medhelp/mydiet/C$url;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "url"
.end annotation


# static fields
.field public static final FORUMS:Ljava/lang/String; = null

.field public static final SERVER_URL:Ljava/lang/String; = null

.field public static final SERVER_URL_ACHIE:Ljava/lang/String; = "http://192.168.5.75:3000"

.field public static final SERVER_URL_ACHIE_HOME:Ljava/lang/String; = "http://192.168.1.4:3000"

.field public static final SERVER_URL_CERT:Ljava/lang/String; = "http://www.medhelp.org"

.field public static final SERVER_URL_CERT_LOGIN:Ljava/lang/String; = "https://android.medhelp.org"

.field public static final SERVER_URL_LOGIN:Ljava/lang/String; = null

.field public static final SERVER_URL_PRODUCTION:Ljava/lang/String; = "http://www.medhelp.org"

.field public static final SERVER_URL_PRODUCTION_LOGIN:Ljava/lang/String; = "https://android.medhelp.org"

.field public static final SERVER_URL_SKYE:Ljava/lang/String; = "http://192.168.5.30:3000"

.field public static final SERVER_URL_SKYE_CUSTOM:Ljava/lang/String; = "http://10.1.10.202:3000"

.field public static final SERVER_URL_STAGING_1:Ljava/lang/String; = "http://staging1.medhelp.ws"

.field public static final SERVER_URL_STAGING_1_LOGIN:Ljava/lang/String; = "https://staging1.medhelp.ws"

.field public static final SERVER_URL_STAGING_3:Ljava/lang/String; = "http://staging3.medhelp.ws"

.field public static final SERVER_URL_STAGING_3_LOGIN:Ljava/lang/String; = "https://staging3.medhelp.ws"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 571
    invoke-static {}, Lorg/medhelp/mydiet/C$url;->getServerUrl()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/C$url;->SERVER_URL:Ljava/lang/String;

    .line 572
    invoke-static {}, Lorg/medhelp/mydiet/C$url;->getServerUrlForLogin()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/C$url;->SERVER_URL_LOGIN:Ljava/lang/String;

    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->SERVER_URL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/forums/list?app=diet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/C$url;->FORUMS:Ljava/lang/String;

    .line 570
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getServerUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 595
    const-string v0, "http://www.medhelp.org"

    return-object v0
.end method

.method private static getServerUrlForLogin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 603
    const-string v0, "https://android.medhelp.org"

    return-object v0
.end method
