.class public Lorg/medhelp/mydiet/activity/SetupScreen3;
.super Lorg/medhelp/mydiet/activity/BackClickAlertActivity;
.source "SetupScreen3.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private btnHeight:Landroid/widget/Button;

.field private btnHeightUnits:Landroid/widget/Button;

.field private btnWeight:Landroid/widget/Button;

.field private btnWeightUnits:Landroid/widget/Button;

.field mDialog:Landroid/app/AlertDialog;

.field private mDistanceUnitsLayout:Landroid/widget/RelativeLayout;

.field private mWaterUnitsLayout:Landroid/widget/RelativeLayout;

.field private startedFromSettings:Z

.field private tvDistanceUnits:Landroid/widget/TextView;

.field private tvWaterUnits:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/SetupScreen3;)V
    .locals 0
    .parameter

    .prologue
    .line 489
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateWeightDisplay()V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/SetupScreen3;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnWeightUnits:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/SetupScreen3;)V
    .locals 0
    .parameter

    .prologue
    .line 513
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateWeightUnitsDisplay()V

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/SetupScreen3;)V
    .locals 0
    .parameter

    .prologue
    .line 471
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateHeightDisplay()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/SetupScreen3;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnHeightUnits:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/SetupScreen3;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->tvDistanceUnits:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6(Lorg/medhelp/mydiet/activity/SetupScreen3;)V
    .locals 0
    .parameter

    .prologue
    .line 517
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateDistanceUnitsDisplay()V

    return-void
.end method

.method static synthetic access$7(Lorg/medhelp/mydiet/activity/SetupScreen3;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->tvWaterUnits:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$8(Lorg/medhelp/mydiet/activity/SetupScreen3;)V
    .locals 0
    .parameter

    .prologue
    .line 521
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateWaterUnitsDisplay()V

    return-void
.end method

.method private clickDistanceUnits()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f060002

    .line 406
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 407
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 409
    const/4 v1, 0x0

    .line 410
    .local v1, checkedItem:I
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDistanceUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "km"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 411
    const/4 v1, 0x1

    .line 414
    :cond_0
    new-instance v2, Lorg/medhelp/mydiet/activity/SetupScreen3$10;

    invoke-direct {v2, p0}, Lorg/medhelp/mydiet/activity/SetupScreen3$10;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    invoke-virtual {v0, v4, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 426
    const-string v2, "Select Units"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 428
    const-string v2, "Cancel"

    invoke-virtual {v0, v2, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 429
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    .line 430
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 431
    return-void
.end method

.method private clickHeight()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v13, 0x0

    .line 247
    move-object v1, p0

    .line 248
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 249
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v11, "layout_inflater"

    invoke-virtual {v1, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/LayoutInflater;

    .line 251
    .local v9, inflater:Landroid/view/LayoutInflater;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "in"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 253
    const v12, 0x7f03001b

    const v11, 0x7f0b00c5

    invoke-virtual {p0, v11}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    invoke-virtual {v9, v12, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 254
    .local v10, layout:Landroid/view/View;
    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 255
    const-string v11, "Enter your height"

    invoke-virtual {v0, v11}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 257
    const v11, 0x7f0b00c6

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 258
    .local v2, feetField:Landroid/widget/EditText;
    const v11, 0x7f0b00c8

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    .line 260
    .local v8, inchesField:Landroid/widget/EditText;
    new-instance v11, Lorg/medhelp/mydiet/activity/SetupScreen3$4;

    invoke-direct {v11, p0}, Lorg/medhelp/mydiet/activity/SetupScreen3$4;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    invoke-virtual {v2, v11}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 269
    new-instance v11, Lorg/medhelp/mydiet/activity/SetupScreen3$5;

    invoke-direct {v11, p0, v8}, Lorg/medhelp/mydiet/activity/SetupScreen3$5;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;Landroid/widget/EditText;)V

    invoke-virtual {v8, v11}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 290
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightInInches(Landroid/content/Context;)F

    move-result v6

    .line 291
    .local v6, heightInInches:F
    const/high16 v11, 0x4140

    div-float v11, v6, v11

    float-to-int v3, v11

    .line 292
    .local v3, heightFeet:I
    mul-int/lit8 v11, v3, 0xc

    int-to-float v11, v11

    sub-float v11, v6, v11

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 293
    .local v7, heightInches:I
    const/16 v11, 0xc

    if-ne v7, v11, :cond_0

    .line 294
    add-int/lit8 v3, v3, 0x1

    .line 295
    const/4 v7, 0x0

    .line 297
    :cond_0
    cmpl-float v11, v6, v13

    if-lez v11, :cond_1

    .line 298
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 299
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 302
    :cond_1
    const-string v11, "OK"

    new-instance v12, Lorg/medhelp/mydiet/activity/SetupScreen3$6;

    invoke-direct {v12, p0, v2, v8}, Lorg/medhelp/mydiet/activity/SetupScreen3$6;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v0, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 328
    const-string v11, "Cancel"

    invoke-virtual {v0, v11, v14}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 373
    .end local v2           #feetField:Landroid/widget/EditText;
    .end local v3           #heightFeet:I
    .end local v6           #heightInInches:F
    .end local v7           #heightInches:I
    .end local v8           #inchesField:Landroid/widget/EditText;
    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    iput-object v11, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    .line 374
    iget-object v11, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v11}, Landroid/app/AlertDialog;->show()V

    .line 375
    return-void

    .line 331
    .end local v10           #layout:Landroid/view/View;
    :cond_2
    const v12, 0x7f03001a

    const v11, 0x7f0b00c2

    invoke-virtual {p0, v11}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    invoke-virtual {v9, v12, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 332
    .restart local v10       #layout:Landroid/view/View;
    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 333
    const-string v11, "Enter your height"

    invoke-virtual {v0, v11}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 335
    const v11, 0x7f0b00c3

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 336
    .local v4, heightField:Landroid/widget/EditText;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightInCm(Landroid/content/Context;)F

    move-result v5

    .line 338
    .local v5, heightInCm:F
    new-instance v11, Lorg/medhelp/mydiet/activity/SetupScreen3$7;

    invoke-direct {v11, p0}, Lorg/medhelp/mydiet/activity/SetupScreen3$7;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    invoke-virtual {v4, v11}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 347
    cmpl-float v11, v5, v13

    if-lez v11, :cond_3

    .line 348
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 351
    :cond_3
    const-string v11, "OK"

    new-instance v12, Lorg/medhelp/mydiet/activity/SetupScreen3$8;

    invoke-direct {v12, p0, v4}, Lorg/medhelp/mydiet/activity/SetupScreen3$8;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;Landroid/widget/EditText;)V

    invoke-virtual {v0, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 371
    const-string v11, "Cancel"

    invoke-virtual {v0, v11, v14}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private clickHeightUnits()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f060001

    .line 378
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 379
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 381
    const/4 v1, 0x0

    .line 382
    .local v1, checkedItem:I
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 383
    const/4 v1, 0x1

    .line 386
    :cond_0
    new-instance v2, Lorg/medhelp/mydiet/activity/SetupScreen3$9;

    invoke-direct {v2, p0}, Lorg/medhelp/mydiet/activity/SetupScreen3$9;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    invoke-virtual {v0, v4, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 398
    const-string v2, "Select Units"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 400
    const-string v2, "Cancel"

    invoke-virtual {v0, v2, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 401
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    .line 402
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 403
    return-void
.end method

.method private clickWaterUnits()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f060003

    .line 434
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 435
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 437
    const/4 v1, -0x1

    .line 439
    .local v1, checkedItem:I
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 441
    .local v3, waterUnits:[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    array-length v4, v3

    if-lt v2, v4, :cond_1

    .line 448
    :goto_1
    if-gez v1, :cond_0

    .line 449
    const/4 v1, 0x0

    .line 452
    :cond_0
    new-instance v4, Lorg/medhelp/mydiet/activity/SetupScreen3$11;

    invoke-direct {v4, p0}, Lorg/medhelp/mydiet/activity/SetupScreen3$11;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    invoke-virtual {v0, v6, v1, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 464
    const-string v4, "Select Units"

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 466
    const-string v4, "Cancel"

    invoke-virtual {v0, v4, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 467
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    .line 468
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 469
    return-void

    .line 442
    :cond_1
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aget-object v5, v3, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 443
    move v1, v2

    .line 444
    goto :goto_1

    .line 441
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private clickWeight()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 139
    move-object v1, p0

    .line 140
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 141
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v9, "layout_inflater"

    invoke-virtual {v1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 143
    .local v2, inflater:Landroid/view/LayoutInflater;
    const v10, 0x7f030035

    const v9, 0x7f0b0133

    invoke-virtual {p0, v9}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    invoke-virtual {v2, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 144
    .local v3, layout:Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 145
    const-string v9, "Enter your Weight"

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 147
    const v9, 0x7f0b0134

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    .line 148
    .local v6, weightField:Landroid/widget/EditText;
    const v9, 0x7f0b001a

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 150
    .local v4, tvUnits:Landroid/widget/TextView;
    new-instance v9, Lorg/medhelp/mydiet/activity/SetupScreen3$1;

    invoke-direct {v9, p0}, Lorg/medhelp/mydiet/activity/SetupScreen3$1;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 159
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 161
    .local v5, units:Ljava/lang/String;
    const-string v9, "lb"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 164
    const-string v9, "lb"

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v8

    .line 166
    .local v8, weightInLb:F
    cmpl-float v9, v8, v11

    if-lez v9, :cond_0

    .line 167
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 181
    .end local v8           #weightInLb:F
    :goto_0
    const-string v9, "OK"

    new-instance v10, Lorg/medhelp/mydiet/activity/SetupScreen3$2;

    invoke-direct {v10, p0, v6, v5}, Lorg/medhelp/mydiet/activity/SetupScreen3$2;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;Landroid/widget/EditText;Ljava/lang/String;)V

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 211
    const-string v9, "Cancel"

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 212
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    iput-object v9, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    .line 213
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    .line 214
    return-void

    .line 169
    .restart local v8       #weightInLb:F
    :cond_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->resetWeightToDefault()V

    goto :goto_0

    .line 173
    .end local v8           #weightInLb:F
    :cond_1
    const-string v9, "kg"

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInKG(Landroid/content/Context;)F

    move-result v7

    .line 175
    .local v7, weightInKG:F
    cmpl-float v9, v7, v11

    if-lez v9, :cond_2

    .line 176
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 178
    :cond_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->resetWeightToDefault()V

    goto :goto_0
.end method

.method private clickWeightUnits()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x7f06

    .line 217
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 219
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 221
    const/4 v1, 0x0

    .line 222
    .local v1, checkedItem:I
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "kg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 223
    const/4 v1, 0x1

    .line 226
    :cond_0
    new-instance v2, Lorg/medhelp/mydiet/activity/SetupScreen3$3;

    invoke-direct {v2, p0}, Lorg/medhelp/mydiet/activity/SetupScreen3$3;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    invoke-virtual {v0, v4, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 239
    const-string v2, "Select Units"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 241
    const-string v2, "Cancel"

    invoke-virtual {v0, v2, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 242
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    .line 243
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 244
    return-void
.end method

.method private onClickNext()V
    .locals 2

    .prologue
    .line 130
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->startedFromSettings:Z

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->finish()V

    .line 136
    :goto_0
    return-void

    .line 133
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/SetupScreen4;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->startActivity(Landroid/content/Intent;)V

    .line 134
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->finish()V

    goto :goto_0
.end method

.method private refreshViewContents()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateHeightDisplay()V

    .line 95
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateHeightUnitsDisplay()V

    .line 96
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateWeightDisplay()V

    .line 97
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateWeightUnitsDisplay()V

    .line 98
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateDistanceUnitsDisplay()V

    .line 99
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateWaterUnitsDisplay()V

    .line 100
    return-void
.end method

.method private resetHeightToDefault()V
    .locals 2

    .prologue
    .line 538
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 540
    .local v0, gender:Ljava/lang/String;
    const-string v1, "Male"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 541
    const/high16 v1, 0x428c

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightInInches(Landroid/content/Context;F)V

    .line 546
    :goto_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateHeightDisplay()V

    .line 547
    return-void

    .line 543
    :cond_0
    const/high16 v1, 0x4280

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightInInches(Landroid/content/Context;F)V

    goto :goto_0
.end method

.method private resetWeightToDefault()V
    .locals 2

    .prologue
    .line 526
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 528
    .local v0, gender:Ljava/lang/String;
    const-string v1, "Male"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 529
    const/high16 v1, 0x4334

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWeightInLb(Landroid/content/Context;F)V

    .line 534
    :goto_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->updateWeightDisplay()V

    .line 535
    return-void

    .line 531
    :cond_0
    const/high16 v1, 0x4316

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWeightInLb(Landroid/content/Context;F)V

    goto :goto_0
.end method

.method private updateDistanceUnitsDisplay()V
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->tvDistanceUnits:Landroid/widget/TextView;

    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDistanceUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 519
    return-void
.end method

.method private updateHeightDisplay()V
    .locals 3

    .prologue
    .line 472
    const/4 v0, 0x0

    .line 473
    .local v0, heightString:Ljava/lang/String;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "in"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 474
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 478
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 479
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnHeight:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 483
    :goto_1
    return-void

    .line 476
    :cond_0
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 481
    :cond_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->resetHeightToDefault()V

    goto :goto_1
.end method

.method private updateHeightUnitsDisplay()V
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnHeightUnits:Landroid/widget/Button;

    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 487
    return-void
.end method

.method private updateWaterUnitsDisplay()V
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->tvWaterUnits:Landroid/widget/TextView;

    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 523
    return-void
.end method

.method private updateWeightDisplay()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 490
    const/4 v2, 0x0

    .line 491
    .local v2, weightString:Ljava/lang/String;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "lb"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 492
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v1

    .line 493
    .local v1, weightInLb:F
    cmpl-float v3, v1, v5

    if-lez v3, :cond_0

    .line 494
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " lb"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 506
    .end local v1           #weightInLb:F
    :goto_0
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 507
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnWeight:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 511
    :goto_1
    return-void

    .line 496
    .restart local v1       #weightInLb:F
    :cond_0
    const-string v2, ""

    goto :goto_0

    .line 499
    .end local v1           #weightInLb:F
    :cond_1
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInKG(Landroid/content/Context;)F

    move-result v0

    .line 500
    .local v0, weightInKG:F
    cmpl-float v3, v0, v5

    if-lez v3, :cond_2

    .line 501
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " kg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 503
    :cond_2
    const-string v2, ""

    goto :goto_0

    .line 509
    .end local v0           #weightInKG:F
    :cond_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->resetWeightToDefault()V

    goto :goto_1
.end method

.method private updateWeightUnitsDisplay()V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnWeightUnits:Landroid/widget/Button;

    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 515
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 104
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 127
    :goto_0
    :pswitch_0
    return-void

    .line 106
    :pswitch_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->clickWeight()V

    goto :goto_0

    .line 109
    :pswitch_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->clickWeightUnits()V

    goto :goto_0

    .line 112
    :pswitch_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->clickHeight()V

    goto :goto_0

    .line 115
    :pswitch_4
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->clickHeightUnits()V

    goto :goto_0

    .line 118
    :pswitch_5
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->clickDistanceUnits()V

    goto :goto_0

    .line 121
    :pswitch_6
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->clickWaterUnits()V

    goto :goto_0

    .line 124
    :pswitch_7
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->onClickNext()V

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x7f0b00e9
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 47
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v3, 0x7f030028

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->setContentView(I)V

    .line 50
    const v3, 0x7f0b00eb

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnWeight:Landroid/widget/Button;

    .line 51
    const v3, 0x7f0b00ec

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnWeightUnits:Landroid/widget/Button;

    .line 52
    const v3, 0x7f0b00ed

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnHeight:Landroid/widget/Button;

    .line 53
    const v3, 0x7f0b00ee

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnHeightUnits:Landroid/widget/Button;

    .line 54
    const v3, 0x7f0b00ef

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDistanceUnitsLayout:Landroid/widget/RelativeLayout;

    .line 55
    const v3, 0x7f0b00f2

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mWaterUnitsLayout:Landroid/widget/RelativeLayout;

    .line 56
    const v3, 0x7f0b00f1

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->tvDistanceUnits:Landroid/widget/TextView;

    .line 57
    const v3, 0x7f0b00f4

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->tvWaterUnits:Landroid/widget/TextView;

    .line 59
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnWeight:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnWeightUnits:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnHeight:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnHeightUnits:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDistanceUnitsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mWaterUnitsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v3, 0x7f0b00e9

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 67
    .local v0, btn:Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 70
    .local v2, extras:Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 71
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "edit_preferences"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, editPreferences:Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "edit_preferences"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->startedFromSettings:Z

    .line 74
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen3;->setShowBackAlert(Z)V

    .line 75
    const-string v3, "Save"

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->btnWeight:Landroid/widget/Button;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 79
    .end local v1           #editPreferences:Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 90
    :cond_0
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->onDestroy()V

    .line 91
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->onStart()V

    .line 83
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen3;->refreshViewContents()V

    .line 84
    return-void
.end method
