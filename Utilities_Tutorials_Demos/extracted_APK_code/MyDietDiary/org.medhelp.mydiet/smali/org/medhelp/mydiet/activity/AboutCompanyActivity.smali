.class public Lorg/medhelp/mydiet/activity/AboutCompanyActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "AboutCompanyActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    .line 13
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 14
    const v4, 0x7f030001

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->setContentView(I)V

    .line 16
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "about"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 18
    .local v0, about:Ljava/lang/String;
    const v4, 0x7f0b0006

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 19
    .local v2, logo:Landroid/widget/ImageView;
    const/high16 v4, 0x7f0b

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 20
    .local v3, title:Landroid/widget/TextView;
    const v4, 0x7f0b0003

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 22
    .local v1, aboutText:Landroid/widget/TextView;
    if-nez v0, :cond_0

    .line 23
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->finish()V

    .line 25
    :cond_0
    const-string v4, "ge"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 26
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200a9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 28
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08000e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    :cond_1
    :goto_0
    return-void

    .line 29
    :cond_2
    const-string v4, "medhelp"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 30
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08000b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200aa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 32
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08000c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
