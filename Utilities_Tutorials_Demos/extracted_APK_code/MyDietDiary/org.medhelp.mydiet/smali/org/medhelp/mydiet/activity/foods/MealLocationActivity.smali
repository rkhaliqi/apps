.class public Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "MealLocationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mDialog:Landroid/app/AlertDialog;

.field private mTVWhere:Landroid/widget/TextView;

.field private mTVWithWhom:Landroid/widget/TextView;

.field private mWhere:Ljava/lang/String;

.field private mWithWhom:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWhere:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->refreshWhereContent()V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;)Landroid/app/AlertDialog;
    .locals 1
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWithWhom:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->refreshWithWhomContent()V

    return-void
.end method

.method private onWhereClick()V
    .locals 4

    .prologue
    .line 87
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 89
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const/4 v1, 0x0

    .line 90
    .local v1, checkedItem:I
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "kg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    const/4 v1, 0x1

    .line 94
    :cond_0
    const v2, 0x7f060007

    new-instance v3, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$1;

    invoke-direct {v3, p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$1;-><init>(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;)V

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 104
    const-string v2, "Record where you ate"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 106
    const-string v2, "Cancel"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 107
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mDialog:Landroid/app/AlertDialog;

    .line 108
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 109
    return-void
.end method

.method private onWithWhomClick()V
    .locals 4

    .prologue
    .line 112
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 114
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const/4 v1, 0x0

    .line 116
    .local v1, checkedItem:I
    const v2, 0x7f060008

    new-instance v3, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$2;

    invoke-direct {v3, p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$2;-><init>(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;)V

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 126
    const-string v2, "With whom?"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 128
    const-string v2, "Cancel"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 129
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mDialog:Landroid/app/AlertDialog;

    .line 130
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 131
    return-void
.end method

.method private refreshWhereContent()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWhere:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWhere:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 48
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mTVWhere:Landroid/widget/TextView;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWhere:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mTVWhere:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private refreshWithWhomContent()V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWithWhom:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWithWhom:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 56
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mTVWithWhom:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "With "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWithWhom:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mTVWhere:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 84
    :goto_0
    monitor-exit p0

    return-void

    .line 66
    :sswitch_0
    :try_start_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->onWhereClick()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 69
    :sswitch_1
    :try_start_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->onWithWhomClick()V

    goto :goto_0

    .line 72
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 73
    .local v0, resultIntent:Landroid/content/Intent;
    const-string v1, "where"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWhere:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    const-string v1, "with_whom"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWithWhom:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->setResult(ILandroid/content/Intent;)V

    .line 77
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->finish()V

    goto :goto_0

    .line 81
    .end local v0           #resultIntent:Landroid/content/Intent;
    :sswitch_3
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 64
    :sswitch_data_0
    .sparse-switch
        0x7f0b0081 -> :sswitch_2
        0x7f0b00db -> :sswitch_0
        0x7f0b00de -> :sswitch_1
        0x7f0b00e2 -> :sswitch_3
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 29
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const v0, 0x7f030023

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->setContentView(I)V

    .line 32
    const v0, 0x7f0b00dd

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mTVWhere:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f0b00e0

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mTVWithWhom:Landroid/widget/TextView;

    .line 35
    const v0, 0x7f0b00db

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    const v0, 0x7f0b00de

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    const v0, 0x7f0b0081

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "where"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWhere:Ljava/lang/String;

    .line 40
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "with_whom"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWithWhom:Ljava/lang/String;

    .line 42
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->refreshWhereContent()V

    .line 43
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->refreshWithWhomContent()V

    .line 44
    return-void
.end method
