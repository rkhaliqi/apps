.class Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;
.super Landroid/os/AsyncTask;
.source "PickExerciseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadExercisesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 3
    .parameter "params"

    .prologue
    .line 115
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 116
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getExercises()Ljava/util/ArrayList;

    move-result-object v2

    #setter for: Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mExercises:Ljava/util/ArrayList;
    invoke-static {v1, v2}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->access$2(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;Ljava/util/ArrayList;)V

    .line 117
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 137
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->publishProgress([Ljava/lang/Object;)V

    .line 138
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 139
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 110
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->publishProgress([Ljava/lang/Object;)V

    .line 111
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 5
    .parameter "values"

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 123
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 133
    :goto_0
    return-void

    .line 125
    :sswitch_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->showSavingDialog()V
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->access$3(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)V

    goto :goto_0

    .line 128
    :sswitch_1
    new-instance v0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    const v2, 0x7f030020

    const v3, 0x7f0b000c

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    #getter for: Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mExercises:Ljava/util/ArrayList;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->access$0(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;-><init>(Landroid/content/Context;IILjava/util/ArrayList;)V

    .line 129
    .local v0, adapter:Lorg/medhelp/mydiet/adapter/ExercisesAdapter;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    #getter for: Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mList:Landroid/widget/ListView;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->access$4(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 130
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    #calls: Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->hideSavingDialog()V
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->access$5(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)V

    goto :goto_0

    .line 123
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$LoadExercisesTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
