.class Lorg/medhelp/mydiet/activity/SetupScreen3$11;
.super Ljava/lang/Object;
.source "SetupScreen3.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/SetupScreen3;->clickWaterUnits()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/SetupScreen3;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$11;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    .line 452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 456
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$11;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/activity/SetupScreen3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v0, v1, p2

    .line 457
    .local v0, units:Ljava/lang/String;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$11;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-static {v1, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWaterUnits(Landroid/content/Context;Ljava/lang/String;)V

    .line 458
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$11;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    #getter for: Lorg/medhelp/mydiet/activity/SetupScreen3;->tvWaterUnits:Landroid/widget/TextView;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/SetupScreen3;->access$7(Lorg/medhelp/mydiet/activity/SetupScreen3;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$11;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-static {v2}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$11;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    #calls: Lorg/medhelp/mydiet/activity/SetupScreen3;->updateWaterUnitsDisplay()V
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/SetupScreen3;->access$8(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    .line 460
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$11;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    iget-object v1, v1, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 461
    return-void
.end method
