.class public Lorg/medhelp/mydiet/adapter/ExercisesAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ExercisesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lorg/medhelp/mydiet/model/Exercise;",
        ">;"
    }
.end annotation


# instance fields
.field private holder:Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;

.field private mExercises:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/medhelp/mydiet/model/Exercise;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/util/ArrayList;)V
    .locals 1
    .parameter "context"
    .parameter "resource"
    .parameter "textViewResourceId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Exercise;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p4, objects:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Exercise;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 30
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 31
    iput-object p4, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->mExercises:Ljava/util/List;

    .line 32
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->mExercises:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->getItem(I)Lorg/medhelp/mydiet/model/Exercise;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lorg/medhelp/mydiet/model/Exercise;
    .locals 1
    .parameter "position"

    .prologue
    .line 39
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->mExercises:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/Exercise;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 52
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->mExercises:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/Exercise;

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Exercise;->id:J

    return-wide v0
.end method

.method public getPosition(Lorg/medhelp/mydiet/model/FoodItemInMeal;)I
    .locals 1
    .parameter "item"

    .prologue
    .line 48
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->mExercises:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->getItem(I)Lorg/medhelp/mydiet/model/Exercise;

    move-result-object v0

    .line 59
    .local v0, exercise:Lorg/medhelp/mydiet/model/Exercise;
    if-nez p2, :cond_1

    .line 60
    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030020

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 61
    new-instance v1, Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;

    invoke-direct {v1}, Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;-><init>()V

    iput-object v1, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->holder:Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;

    .line 62
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->holder:Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;

    const v1, 0x7f0b000c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;->tvFoodItemTitle:Landroid/widget/TextView;

    .line 63
    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->holder:Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 68
    :goto_0
    if-eqz v0, :cond_0

    .line 69
    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->holder:Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;

    iget-object v1, v1, Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;->tvFoodItemTitle:Landroid/widget/TextView;

    iget-object v2, v0, Lorg/medhelp/mydiet/model/Exercise;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    :cond_0
    return-object p2

    .line 65
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;

    iput-object v1, p0, Lorg/medhelp/mydiet/adapter/ExercisesAdapter;->holder:Lorg/medhelp/mydiet/adapter/ExercisesAdapter$ViewHolder;

    goto :goto_0
.end method
