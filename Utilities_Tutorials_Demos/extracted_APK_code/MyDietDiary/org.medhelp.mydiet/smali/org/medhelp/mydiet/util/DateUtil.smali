.class public Lorg/medhelp/mydiet/util/DateUtil;
.super Ljava/lang/Object;
.source "DateUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calendarToDate(Ljava/util/Calendar;)Ljava/util/Date;
    .locals 8
    .parameter "c"

    .prologue
    .line 27
    new-instance v7, Ljava/util/Date;

    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-direct {v7, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 28
    .local v7, d:Ljava/util/Date;
    invoke-static {v7}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v0

    .line 29
    .local v0, c1:Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 30
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 31
    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 32
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 33
    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 34
    const/16 v6, 0xd

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 29
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 35
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v1
.end method

.method public static dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;
    .locals 1
    .parameter "date"

    .prologue
    .line 15
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 16
    .local v0, cal:Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 17
    return-object v0
.end method

.method public static formatDate(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "c"
    .parameter "format"

    .prologue
    .line 92
    if-nez p0, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 95
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "date"
    .parameter "format"

    .prologue
    .line 99
    if-nez p0, :cond_0

    .line 100
    const/4 v1, 0x0

    .line 106
    :goto_0
    return-object v1

    .line 102
    :cond_0
    if-nez p1, :cond_1

    .line 103
    const-string p1, "MMMM dd, yyyy"

    .line 105
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 106
    .local v0, dateFormat:Ljava/text/SimpleDateFormat;
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "date"
    .parameter "format"

    .prologue
    .line 110
    if-nez p0, :cond_0

    .line 111
    const/4 v1, 0x0

    .line 118
    :goto_0
    return-object v1

    .line 113
    :cond_0
    if-nez p1, :cond_1

    .line 114
    const-string p1, "MMMM dd, yyyy"

    .line 116
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 117
    .local v0, dateFormat:Ljava/text/SimpleDateFormat;
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 118
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getCurrentMonthFirstDayMidnight()Ljava/util/Calendar;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 82
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 83
    .local v0, c:Ljava/util/Calendar;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-direct {v1, v2, v3, v4}, Ljava/util/GregorianCalendar;-><init>(III)V

    return-object v1
.end method

.method public static getDateAtMidnightInMilliseconds(Ljava/util/Date;)J
    .locals 3
    .parameter "startDate"

    .prologue
    const/4 v2, 0x0

    .line 71
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 72
    .local v0, gcal:Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p0}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 73
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 74
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 75
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 76
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 78
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    return-wide v1
.end method

.method public static getDateFromString(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .parameter "dateString"
    .parameter "format"

    .prologue
    .line 122
    if-nez p1, :cond_0

    .line 123
    const-string p1, "MMMM dd, yyyy"

    .line 125
    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 127
    .local v1, df:Ljava/text/SimpleDateFormat;
    const/4 v0, 0x0

    .line 130
    .local v0, date:Ljava/util/Date;
    :try_start_0
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 135
    :goto_0
    return-object v0

    .line 131
    :catch_0
    move-exception v2

    .line 132
    .local v2, e:Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDifferenceInDays(Ljava/util/Calendar;Ljava/util/Calendar;)J
    .locals 10
    .parameter "c1"
    .parameter "c2"

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x5

    const/4 v7, 0x1

    .line 39
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const-wide/16 v2, 0x0

    .line 55
    :cond_1
    :goto_0
    return-wide v2

    .line 41
    :cond_2
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-virtual {p0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {p0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {p0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-direct {v0, v4, v5, v6}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 42
    .local v0, calA:Ljava/util/Calendar;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {p1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {p1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-direct {v1, v4, v5, v6}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 44
    .local v1, calB:Ljava/util/Calendar;
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 46
    invoke-static {v1, v0}, Lorg/medhelp/mydiet/util/DateUtil;->getDifferenceInDays(Ljava/util/Calendar;Ljava/util/Calendar;)J

    move-result-wide v2

    goto :goto_0

    .line 48
    :cond_3
    const-wide/16 v2, 0x0

    .line 50
    .local v2, differenceInDays:J
    :goto_1
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 51
    invoke-virtual {v0, v8, v7}, Ljava/util/Calendar;->add(II)V

    .line 52
    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    goto :goto_1
.end method

.method public static getDifferenceInMinutes(JJ)J
    .locals 4
    .parameter "d1"
    .parameter "d2"

    .prologue
    .line 163
    cmp-long v0, p0, p2

    if-gez v0, :cond_0

    .line 164
    invoke-static {p2, p3, p0, p1}, Lorg/medhelp/mydiet/util/DateUtil;->getDifferenceInMinutes(JJ)J

    move-result-wide v0

    .line 167
    :goto_0
    return-wide v0

    :cond_0
    sub-long v0, p0, p2

    const-wide/32 v2, 0xea60

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public static getLocalDateFromUTC(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .parameter "utcDate"
    .parameter "format"

    .prologue
    .line 139
    if-nez p1, :cond_0

    .line 140
    const-string p1, "MMMM dd, yyyy"

    .line 142
    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 143
    .local v1, df:Ljava/text/SimpleDateFormat;
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 145
    const/4 v0, 0x0

    .line 148
    .local v0, date:Ljava/util/Date;
    :try_start_0
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    .line 149
    :catch_0
    move-exception v2

    .line 150
    .local v2, e:Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSelectedMonthFirstDayMidnight(Ljava/util/Date;)Ljava/util/Calendar;
    .locals 5
    .parameter "dateInMonth"

    .prologue
    const/4 v4, 0x1

    .line 87
    invoke-static {p0}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v0

    .line 88
    .local v0, c:Ljava/util/Calendar;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-direct {v1, v2, v3, v4}, Ljava/util/GregorianCalendar;-><init>(III)V

    return-object v1
.end method

.method public static getTodayMidnightCalendar()Ljava/util/Calendar;
    .locals 5

    .prologue
    .line 60
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 61
    .local v0, c:Ljava/util/Calendar;
    new-instance v1, Ljava/util/GregorianCalendar;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Ljava/util/GregorianCalendar;-><init>(III)V

    return-object v1
.end method

.method public static getTodayMidnightInMilliseconds()J
    .locals 5

    .prologue
    .line 65
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 66
    .local v0, c:Ljava/util/Calendar;
    new-instance v1, Ljava/util/GregorianCalendar;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 67
    .end local v0           #c:Ljava/util/Calendar;
    .local v1, c:Ljava/util/Calendar;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method
