.class Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$5;
.super Ljava/lang/Object;
.source "FinalizeMealActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->onCaloriesClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

.field private final synthetic val$caloriesEditor:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$5;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$5;->val$caloriesEditor:Landroid/widget/EditText;

    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 280
    const/4 v0, 0x0

    .line 281
    .local v0, calories:F
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$5;->val$caloriesEditor:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 282
    .local v1, caloriesString:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_0

    .line 283
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 286
    :cond_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$5;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v2

    float-to-double v3, v0

    iput-wide v3, v2, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    .line 287
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$5;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->setCaloriesText()V
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$10(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    .line 288
    return-void
.end method
