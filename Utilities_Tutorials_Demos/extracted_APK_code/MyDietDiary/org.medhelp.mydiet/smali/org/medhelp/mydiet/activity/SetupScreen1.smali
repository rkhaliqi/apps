.class public Lorg/medhelp/mydiet/activity/SetupScreen1;
.super Lorg/medhelp/mydiet/activity/BackClickAlertActivity;
.source "SetupScreen1.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field btnLeft:Landroid/widget/Button;

.field btnNext:Landroid/widget/Button;

.field btnRight:Landroid/widget/Button;

.field ivAvatar:Landroid/widget/ImageView;

.field rbFemale:Landroid/widget/RadioButton;

.field rbMale:Landroid/widget/RadioButton;

.field private startedFromSettings:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;-><init>()V

    return-void
.end method

.method private declared-synchronized clickLeft()V
    .locals 2

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getAvatarPosition(Landroid/content/Context;)I

    move-result v0

    .line 135
    .local v0, avatarImagePosition:I
    if-lez v0, :cond_0

    .line 136
    add-int/lit8 v0, v0, -0x1

    .line 137
    invoke-static {p0, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setAvatarPosition(Landroid/content/Context;I)V

    .line 138
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->refreshAvatar()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :cond_0
    monitor-exit p0

    return-void

    .line 134
    .end local v0           #avatarImagePosition:I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private clickNext()V
    .locals 2

    .prologue
    .line 152
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->startedFromSettings:Z

    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->finish()V

    .line 158
    :goto_0
    return-void

    .line 155
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/SetupScreen2;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->startActivity(Landroid/content/Intent;)V

    .line 156
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->finish()V

    goto :goto_0
.end method

.method private declared-synchronized clickRight()V
    .locals 2

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getAvatarPosition(Landroid/content/Context;)I

    move-result v0

    .line 144
    .local v0, avatarImagePosition:I
    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 145
    add-int/lit8 v0, v0, 0x1

    .line 146
    invoke-static {p0, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setAvatarPosition(Landroid/content/Context;I)V

    .line 147
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->refreshAvatar()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    :cond_0
    monitor-exit p0

    return-void

    .line 143
    .end local v0           #avatarImagePosition:I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private initViews()V
    .locals 2

    .prologue
    .line 101
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, gender:Ljava/lang/String;
    const-string v1, "Male"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->setGenderMale()V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->setGenderFemale()V

    goto :goto_0
.end method

.method private refreshAvatar()V
    .locals 4

    .prologue
    .line 124
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getAvatarPosition(Landroid/content/Context;)I

    move-result v0

    .line 125
    .local v0, avatarImagePosition:I
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 126
    .local v1, gender:Ljava/lang/String;
    const-string v2, "Male"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->ivAvatar:Landroid/widget/ImageView;

    sget-object v3, Lorg/medhelp/mydiet/C$arrays;->maleAvatars:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->ivAvatar:Landroid/widget/ImageView;

    sget-object v3, Lorg/medhelp/mydiet/C$arrays;->femaleAvatars:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private setGenderFemale()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->rbFemale:Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 111
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->rbMale:Landroid/widget/RadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 112
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGenderAsFemale(Landroid/content/Context;)V

    .line 113
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->refreshAvatar()V

    .line 114
    return-void
.end method

.method private setGenderMale()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->rbMale:Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 118
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->rbFemale:Landroid/widget/RadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 119
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGenderAsMale(Landroid/content/Context;)V

    .line 120
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->refreshAvatar()V

    .line 121
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .parameter "buttonView"
    .parameter "isChecked"

    .prologue
    .line 86
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 88
    :pswitch_0
    if-eqz p2, :cond_0

    .line 89
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->setGenderFemale()V

    goto :goto_0

    .line 93
    :pswitch_1
    if-eqz p2, :cond_0

    .line 94
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->setGenderMale()V

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x7f0b00e4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 82
    :goto_0
    :pswitch_0
    return-void

    .line 73
    :pswitch_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->clickLeft()V

    goto :goto_0

    .line 76
    :pswitch_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->clickRight()V

    goto :goto_0

    .line 79
    :pswitch_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->clickNext()V

    goto :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x7f0b00e6
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 39
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v2, 0x7f030026

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/SetupScreen1;->setContentView(I)V

    .line 42
    const v2, 0x7f0b00e4

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/SetupScreen1;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->rbFemale:Landroid/widget/RadioButton;

    .line 43
    const v2, 0x7f0b00e5

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/SetupScreen1;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->rbMale:Landroid/widget/RadioButton;

    .line 44
    const v2, 0x7f0b00e7

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/SetupScreen1;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->ivAvatar:Landroid/widget/ImageView;

    .line 45
    const v2, 0x7f0b00e6

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/SetupScreen1;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->btnLeft:Landroid/widget/Button;

    .line 46
    const v2, 0x7f0b00e8

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/SetupScreen1;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->btnRight:Landroid/widget/Button;

    .line 47
    const v2, 0x7f0b00e9

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/SetupScreen1;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->btnNext:Landroid/widget/Button;

    .line 49
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 50
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "edit_preferences"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, editPreferences:Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "edit_preferences"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->startedFromSettings:Z

    .line 54
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/SetupScreen1;->setShowBackAlert(Z)V

    .line 55
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->btnNext:Landroid/widget/Button;

    const-string v3, "Save"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 59
    .end local v0           #editPreferences:Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->rbFemale:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 60
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->rbMale:Landroid/widget/RadioButton;

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 62
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->btnLeft:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->btnRight:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen1;->btnNext:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen1;->initViews()V

    .line 67
    return-void
.end method
