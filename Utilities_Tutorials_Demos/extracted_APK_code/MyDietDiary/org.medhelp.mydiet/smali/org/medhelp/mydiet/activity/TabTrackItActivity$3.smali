.class Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;
.super Ljava/lang/Object;
.source "TabTrackItActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshMealsContent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

.field private final synthetic val$meal:Lorg/medhelp/mydiet/model/Meal;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/model/Meal;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;->val$meal:Lorg/medhelp/mydiet/model/Meal;

    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    .line 395
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;->val$meal:Lorg/medhelp/mydiet/model/Meal;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;->val$meal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v3, v3, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 396
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;->val$meal:Lorg/medhelp/mydiet/model/Meal;

    iget-object v3, v3, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 401
    :cond_0
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;->val$meal:Lorg/medhelp/mydiet/model/Meal;

    invoke-static {v3}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 403
    .local v2, mealsKey:Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    const-class v4, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 404
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "transient_meal"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    const-string v3, "date"

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #getter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$1(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 406
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    const/16 v4, 0xbb9

    invoke-virtual {v3, v1, v4}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 407
    return-void

    .line 396
    .end local v1           #intent:Landroid/content/Intent;
    .end local v2           #mealsKey:Ljava/lang/String;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 397
    .local v0, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const/4 v4, 0x1

    iput-boolean v4, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    goto :goto_0
.end method
