.class Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$1;
.super Ljava/lang/Object;
.source "ExercisesActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->refreshExercisesContent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    .line 157
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 158
    .local v3, position:I
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    iget-object v4, v4, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/ExerciseItem;

    .line 159
    .local v2, item:Lorg/medhelp/mydiet/model/ExerciseItem;
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    #getter for: Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDate:Ljava/util/Date;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->access$0(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iput-wide v4, v2, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    .line 161
    invoke-static {v2}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, eiKey:Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    const-class v5, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 164
    .local v1, intent:Landroid/content/Intent;
    const-string v4, "transient_exercise_item"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    invoke-virtual {v4, v1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->startActivity(Landroid/content/Intent;)V

    .line 166
    return-void
.end method
