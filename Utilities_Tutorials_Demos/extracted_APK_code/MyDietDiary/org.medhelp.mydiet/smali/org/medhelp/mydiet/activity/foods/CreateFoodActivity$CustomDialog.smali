.class public Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;
.super Landroid/app/Dialog;
.source "CreateFoodActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CustomDialog"
.end annotation


# instance fields
.field _context:Landroid/content/Context;

.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;


# direct methods
.method public constructor <init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Landroid/content/Context;)V
    .locals 1
    .parameter
    .parameter "context"

    .prologue
    .line 1226
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    .line 1227
    const v0, 0x7f09000c

    invoke-direct {p0, p2, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 1228
    const v0, 0x7f03000c

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->setContentView(I)V

    .line 1229
    iput-object p2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->_context:Landroid/content/Context;

    .line 1230
    return-void
.end method


# virtual methods
.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 1238
    const v1, 0x7f0b001e

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1239
    .local v0, tv:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1240
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2
    .parameter "title"

    .prologue
    .line 1233
    const v1, 0x7f0b000c

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1234
    .local v0, tv:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1235
    return-void
.end method
