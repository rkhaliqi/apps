.class Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$2;
.super Ljava/lang/Object;
.source "ExercisesActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->refreshExercisesContent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    .line 174
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 175
    .local v0, position:I
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$2;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    iget-object v1, v1, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/ExerciseItem;

    iget-wide v3, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    #calls: Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->deleteExerciseItem(IJ)V
    invoke-static {v2, v0, v3, v4}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->access$6(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;IJ)V

    .line 176
    return-void
.end method
