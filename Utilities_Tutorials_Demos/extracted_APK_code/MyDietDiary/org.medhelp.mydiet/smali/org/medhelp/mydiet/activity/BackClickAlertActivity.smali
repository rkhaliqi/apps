.class public Lorg/medhelp/mydiet/activity/BackClickAlertActivity;
.super Landroid/app/Activity;
.source "BackClickAlertActivity.java"


# instance fields
.field private mBackAlertMessage:Ljava/lang/String;

.field private mRootDialog:Landroid/app/AlertDialog;

.field private showAlert:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 18
    const-string v0, "Changes haven\'t been saved. Are you sure you want to exit?"

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->mBackAlertMessage:Ljava/lang/String;

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->showAlert:Z

    .line 12
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/BackClickAlertActivity;)Landroid/app/AlertDialog;
    .locals 1
    .parameter

    .prologue
    .line 16
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->mRootDialog:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 35
    iget-boolean v1, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->showAlert:Z

    if-eqz v1, :cond_0

    .line 36
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 37
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->mBackAlertMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 39
    const-string v1, "Yes"

    new-instance v2, Lorg/medhelp/mydiet/activity/BackClickAlertActivity$1;

    invoke-direct {v2, p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity$1;-><init>(Lorg/medhelp/mydiet/activity/BackClickAlertActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 48
    const-string v1, "Cancel"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->mRootDialog:Landroid/app/AlertDialog;

    .line 51
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->mRootDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 55
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 24
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->mRootDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->mRootDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 31
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 32
    return-void
.end method

.method public setBackAlertMessage(Ljava/lang/String;)V
    .locals 0
    .parameter "backAlertMessage"

    .prologue
    .line 58
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->mBackAlertMessage:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setShowBackAlert(Z)V
    .locals 0
    .parameter "showAlert"

    .prologue
    .line 62
    iput-boolean p1, p0, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->showAlert:Z

    .line 63
    return-void
.end method
