.class public final Lorg/medhelp/mydiet/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final about_logo:I = 0x7f0b0006

.field public static final about_text:I = 0x7f0b0003

.field public static final btn_accept:I = 0x7f0b012c

.field public static final btn_add_exercise:I = 0x7f0b0039

.field public static final btn_add_food:I = 0x7f0b00ad

.field public static final btn_add_meal:I = 0x7f0b007c

.field public static final btn_all_entries:I = 0x7f0b003f

.field public static final btn_breakfast:I = 0x7f0b00b7

.field public static final btn_cancel:I = 0x7f0b00e2

.field public static final btn_create_food:I = 0x7f0b003c

.field public static final btn_date:I = 0x7f0b0115

.field public static final btn_decline:I = 0x7f0b012d

.field public static final btn_delete_food:I = 0x7f0b00c1

.field public static final btn_delete_item:I = 0x7f0b000a

.field public static final btn_delete_meal:I = 0x7f0b00bc

.field public static final btn_details:I = 0x7f0b0116

.field public static final btn_dinner:I = 0x7f0b00b9

.field public static final btn_done:I = 0x7f0b0081

.field public static final btn_edit:I = 0x7f0b0010

.field public static final btn_exercise:I = 0x7f0b0129

.field public static final btn_finalize_meal:I = 0x7f0b0045

.field public static final btn_finish:I = 0x7f0b0104

.field public static final btn_food:I = 0x7f0b0128

.field public static final btn_frequent:I = 0x7f0b003e

.field public static final btn_height:I = 0x7f0b00ed

.field public static final btn_height_units:I = 0x7f0b00ee

.field public static final btn_info:I = 0x7f0b00f5

.field public static final btn_left:I = 0x7f0b00e6

.field public static final btn_login:I = 0x7f0b00d8

.field public static final btn_logout:I = 0x7f0b00e3

.field public static final btn_lunch:I = 0x7f0b00b8

.field public static final btn_next:I = 0x7f0b00e9

.field public static final btn_ok:I = 0x7f0b001c

.field public static final btn_recent:I = 0x7f0b003d

.field public static final btn_right:I = 0x7f0b00e8

.field public static final btn_save:I = 0x7f0b0035

.field public static final btn_search:I = 0x7f0b0041

.field public static final btn_signin:I = 0x7f0b0135

.field public static final btn_signup:I = 0x7f0b0110

.field public static final btn_skip:I = 0x7f0b0136

.field public static final btn_snack:I = 0x7f0b00ba

.field public static final btn_water:I = 0x7f0b012a

.field public static final btn_weight:I = 0x7f0b00eb

.field public static final btn_weight_units:I = 0x7f0b00ec

.field public static final cb_add_food_item:I = 0x7f0b0047

.field public static final decimal_entry_dialog:I = 0x7f0b0019

.field public static final dialog_calorie_calculation:I = 0x7f0b0011

.field public static final dialog_calories:I = 0x7f0b0017

.field public static final et_amount:I = 0x7f0b0018

.field public static final et_biotin:I = 0x7f0b006b

.field public static final et_calcium:I = 0x7f0b006d

.field public static final et_calories:I = 0x7f0b0053

.field public static final et_calories_from_fat:I = 0x7f0b0054

.field public static final et_captcha:I = 0x7f0b010e

.field public static final et_chloride:I = 0x7f0b0079

.field public static final et_cholesterol:I = 0x7f0b0059

.field public static final et_chromium:I = 0x7f0b0075

.field public static final et_cm:I = 0x7f0b00c3

.field public static final et_copper:I = 0x7f0b0073

.field public static final et_dietary_fiber:I = 0x7f0b005d

.field public static final et_email:I = 0x7f0b0109

.field public static final et_fluoride:I = 0x7f0b0077

.field public static final et_folate:I = 0x7f0b0069

.field public static final et_food_name:I = 0x7f0b004b

.field public static final et_ft:I = 0x7f0b00c6

.field public static final et_in:I = 0x7f0b00c8

.field public static final et_iodine:I = 0x7f0b006f

.field public static final et_iron:I = 0x7f0b006e

.field public static final et_magnesium:I = 0x7f0b0070

.field public static final et_manganese:I = 0x7f0b0074

.field public static final et_manufacturer:I = 0x7f0b004c

.field public static final et_molybdenum:I = 0x7f0b0076

.field public static final et_monounsaturated_fat:I = 0x7f0b0058

.field public static final et_niacin:I = 0x7f0b0067

.field public static final et_nickname:I = 0x7f0b0108

.field public static final et_notes:I = 0x7f0b00e1

.field public static final et_pantothenic_acid:I = 0x7f0b006c

.field public static final et_password:I = 0x7f0b00d7

.field public static final et_password_confirm:I = 0x7f0b010a

.field public static final et_phosphorus:I = 0x7f0b0078

.field public static final et_potassium:I = 0x7f0b005b

.field public static final et_protein:I = 0x7f0b005f

.field public static final et_riboflavin:I = 0x7f0b0066

.field public static final et_saturated_fat:I = 0x7f0b0056

.field public static final et_search:I = 0x7f0b0040

.field public static final et_selenium:I = 0x7f0b0072

.field public static final et_serving_amount:I = 0x7f0b004f

.field public static final et_serving_equivalent_amount:I = 0x7f0b0051

.field public static final et_sodium:I = 0x7f0b005a

.field public static final et_sugars:I = 0x7f0b005e

.field public static final et_thiamin:I = 0x7f0b0065

.field public static final et_total_carbohydrate:I = 0x7f0b005c

.field public static final et_total_fat:I = 0x7f0b0055

.field public static final et_trans_fat:I = 0x7f0b0057

.field public static final et_username:I = 0x7f0b00d6

.field public static final et_value:I = 0x7f0b0020

.field public static final et_vitamin_a:I = 0x7f0b0060

.field public static final et_vitamin_b12:I = 0x7f0b006a

.field public static final et_vitamin_b6:I = 0x7f0b0068

.field public static final et_vitamin_c:I = 0x7f0b0061

.field public static final et_vitamin_d:I = 0x7f0b0062

.field public static final et_vitamin_e:I = 0x7f0b0063

.field public static final et_vitamin_k:I = 0x7f0b0064

.field public static final et_weight:I = 0x7f0b0134

.field public static final et_zinc:I = 0x7f0b0071

.field public static final height_dialog_cm:I = 0x7f0b00c2

.field public static final height_dialog_ft:I = 0x7f0b00c5

.field public static final home_btn_ge:I = 0x7f0b00ca

.field public static final home_btn_medhelp:I = 0x7f0b00cc

.field public static final home_tbb_seperator:I = 0x7f0b00cb

.field public static final home_tbb_seperator_2:I = 0x7f0b00cd

.field public static final home_tbb_title:I = 0x7f0b00ce

.field public static final image_dialog:I = 0x7f0b001b

.field public static final item_about:I = 0x7f0b0139

.field public static final item_settings:I = 0x7f0b0137

.field public static final item_support:I = 0x7f0b0138

.field public static final iv_avatar:I = 0x7f0b00e7

.field public static final iv_cal_progress_bg:I = 0x7f0b0120

.field public static final iv_cal_progress_excess_bg:I = 0x7f0b0122

.field public static final iv_cal_progress_value:I = 0x7f0b0121

.field public static final iv_captcha:I = 0x7f0b010d

.field public static final iv_food_source:I = 0x7f0b0048

.field public static final iv_icon:I = 0x7f0b00d5

.field public static final iv_nav_arrow:I = 0x7f0b000b

.field public static final iv_no_entries_image:I = 0x7f0b0118

.field public static final ll_about_ge:I = 0x7f0b0004

.field public static final ll_about_medhelp:I = 0x7f0b0002

.field public static final ll_activity_level_info:I = 0x7f0b0007

.field public static final ll_add_exercise:I = 0x7f0b0038

.field public static final ll_add_food:I = 0x7f0b00ac

.field public static final ll_add_meal:I = 0x7f0b007b

.field public static final ll_breakdown_content_items:I = 0x7f0b0008

.field public static final ll_calorie_mealtype_breakdown:I = 0x7f0b011c

.field public static final ll_calorie_progress:I = 0x7f0b011d

.field public static final ll_calorie_source_breakdown:I = 0x7f0b011b

.field public static final ll_calories:I = 0x7f0b00b3

.field public static final ll_captcha:I = 0x7f0b010c

.field public static final ll_create_food_layout:I = 0x7f0b00d0

.field public static final ll_details:I = 0x7f0b0117

.field public static final ll_details_header_item:I = 0x7f0b000e

.field public static final ll_distance:I = 0x7f0b002b

.field public static final ll_exercises:I = 0x7f0b0037

.field public static final ll_food_select_food_item:I = 0x7f0b00c0

.field public static final ll_list_footer:I = 0x7f0b00d2

.field public static final ll_list_item_1:I = 0x7f0b00d4

.field public static final ll_loading:I = 0x7f0b00d3

.field public static final ll_location:I = 0x7f0b00b0

.field public static final ll_meals:I = 0x7f0b007a

.field public static final ll_no_items:I = 0x7f0b0042

.field public static final ll_notes:I = 0x7f0b00b5

.field public static final ll_nutritients:I = 0x7f0b0082

.field public static final ll_other_apps:I = 0x7f0b0005

.field public static final ll_progress_dialog:I = 0x7f0b0111

.field public static final ll_select_foods:I = 0x7f0b00ab

.field public static final ll_servings_amount:I = 0x7f0b007d

.field public static final ll_servings_name:I = 0x7f0b007f

.field public static final ll_time:I = 0x7f0b00ae

.field public static final ll_type:I = 0x7f0b0025

.field public static final ll_water:I = 0x7f0b0119

.field public static final ll_weight:I = 0x7f0b0132

.field public static final ll_weight_goal:I = 0x7f0b00fc

.field public static final ll_weight_status:I = 0x7f0b011a

.field public static final lv_exercises:I = 0x7f0b003a

.field public static final lv_foods:I = 0x7f0b0044

.field public static final pb_1:I = 0x7f0b010f

.field public static final pb_loading:I = 0x7f0b001d

.field public static final pb_progress:I = 0x7f0b0001

.field public static final progressBar:I = 0x7f0b0130

.field public static final progressLayout:I = 0x7f0b012f

.field public static final rbtn_female:I = 0x7f0b00e4

.field public static final rbtn_male:I = 0x7f0b00e5

.field public static final rl_activity_level:I = 0x7f0b00f6

.field public static final rl_calories:I = 0x7f0b0032

.field public static final rl_desired_weight:I = 0x7f0b00fd

.field public static final rl_distance:I = 0x7f0b002c

.field public static final rl_distance_units:I = 0x7f0b00ef

.field public static final rl_expecting:I = 0x7f0b0105

.field public static final rl_food_add_food_list_item:I = 0x7f0b0046

.field public static final rl_goals:I = 0x7f0b00f9

.field public static final rl_item_2:I = 0x7f0b0009

.field public static final rl_meal_type:I = 0x7f0b00a9

.field public static final rl_meals_list_item:I = 0x7f0b00bb

.field public static final rl_minutes:I = 0x7f0b0028

.field public static final rl_speed:I = 0x7f0b002f

.field public static final rl_target_date:I = 0x7f0b0100

.field public static final rl_time:I = 0x7f0b0022

.field public static final rl_type:I = 0x7f0b0026

.field public static final rl_water_units:I = 0x7f0b00f2

.field public static final rl_where:I = 0x7f0b00db

.field public static final rl_with_whom:I = 0x7f0b00de

.field public static final spin_food_groups:I = 0x7f0b004d

.field public static final spin_serving_equivalent_units:I = 0x7f0b0052

.field public static final spin_serving_names:I = 0x7f0b0050

.field public static final tab_iv:I = 0x7f0b0113

.field public static final tab_tv:I = 0x7f0b0114

.field public static final tabhost:I = 0x7f0b00cf

.field public static final tabwidget:I = 0x7f0b0112

.field public static final text_entry_dialog:I = 0x7f0b001f

.field public static final titleTextView:I = 0x7f0b0000

.field public static final tv_activity_level_content:I = 0x7f0b00f8

.field public static final tv_activity_level_title:I = 0x7f0b00f7

.field public static final tv_amount:I = 0x7f0b012e

.field public static final tv_biotin_result:I = 0x7f0b009a

.field public static final tv_budget_value:I = 0x7f0b0012

.field public static final tv_calcium_result:I = 0x7f0b009c

.field public static final tv_calories:I = 0x7f0b011e

.field public static final tv_calories_content:I = 0x7f0b0034

.field public static final tv_calories_end:I = 0x7f0b0124

.field public static final tv_calories_remaining:I = 0x7f0b011f

.field public static final tv_calories_result:I = 0x7f0b0083

.field public static final tv_calories_start:I = 0x7f0b0123

.field public static final tv_calories_summary:I = 0x7f0b00b4

.field public static final tv_calories_title:I = 0x7f0b0033

.field public static final tv_chloride_result:I = 0x7f0b00a8

.field public static final tv_cholesterol_result:I = 0x7f0b0088

.field public static final tv_chromium_result:I = 0x7f0b00a4

.field public static final tv_cm:I = 0x7f0b00c4

.field public static final tv_copper_result:I = 0x7f0b00a2

.field public static final tv_count:I = 0x7f0b003b

.field public static final tv_create_food:I = 0x7f0b00d1

.field public static final tv_date:I = 0x7f0b0036

.field public static final tv_desired_weight_content:I = 0x7f0b00ff

.field public static final tv_desired_weight_title:I = 0x7f0b00fe

.field public static final tv_dietary_fiber_result:I = 0x7f0b008c

.field public static final tv_distance_content:I = 0x7f0b002e

.field public static final tv_distance_title:I = 0x7f0b002d

.field public static final tv_distance_units_content:I = 0x7f0b00f1

.field public static final tv_distance_units_title:I = 0x7f0b00f0

.field public static final tv_dob:I = 0x7f0b00ea

.field public static final tv_exercise_value:I = 0x7f0b0014

.field public static final tv_expecting:I = 0x7f0b0106

.field public static final tv_fluoride_result:I = 0x7f0b00a6

.field public static final tv_folate_result:I = 0x7f0b0098

.field public static final tv_food_calories:I = 0x7f0b00be

.field public static final tv_food_description:I = 0x7f0b004a

.field public static final tv_food_summary:I = 0x7f0b00bf

.field public static final tv_food_time:I = 0x7f0b00bd

.field public static final tv_food_title:I = 0x7f0b0049

.field public static final tv_food_value:I = 0x7f0b0013

.field public static final tv_ft:I = 0x7f0b00c7

.field public static final tv_goal_message:I = 0x7f0b0103

.field public static final tv_goals_content:I = 0x7f0b00fb

.field public static final tv_goals_title:I = 0x7f0b00fa

.field public static final tv_in:I = 0x7f0b00c9

.field public static final tv_iodine_result:I = 0x7f0b009e

.field public static final tv_iron_result:I = 0x7f0b009d

.field public static final tv_location_where:I = 0x7f0b00b2

.field public static final tv_location_with_whom:I = 0x7f0b00b1

.field public static final tv_login_response:I = 0x7f0b00d9

.field public static final tv_magnesium_result:I = 0x7f0b009f

.field public static final tv_manganese_result:I = 0x7f0b00a3

.field public static final tv_meal_type:I = 0x7f0b00aa

.field public static final tv_message:I = 0x7f0b001e

.field public static final tv_minutes_content:I = 0x7f0b002a

.field public static final tv_minutes_title:I = 0x7f0b0029

.field public static final tv_molybdenum_result:I = 0x7f0b00a5

.field public static final tv_mono_unsaturated_fat_result:I = 0x7f0b0087

.field public static final tv_niacin_result:I = 0x7f0b0096

.field public static final tv_no_items:I = 0x7f0b0043

.field public static final tv_notes_summary:I = 0x7f0b00b6

.field public static final tv_pantothenic_acid_result:I = 0x7f0b009b

.field public static final tv_phosphorus_result:I = 0x7f0b00a7

.field public static final tv_potassium_result:I = 0x7f0b008a

.field public static final tv_protein_result:I = 0x7f0b008e

.field public static final tv_remaining_calories_title:I = 0x7f0b0015

.field public static final tv_remaining_calories_value:I = 0x7f0b0016

.field public static final tv_riboflavin_result:I = 0x7f0b0095

.field public static final tv_saturated_fat_result:I = 0x7f0b0085

.field public static final tv_selenium_result:I = 0x7f0b00a1

.field public static final tv_servings_result:I = 0x7f0b007e

.field public static final tv_servings_size_result:I = 0x7f0b0080

.field public static final tv_servings_title:I = 0x7f0b004e

.field public static final tv_sign_up:I = 0x7f0b00da

.field public static final tv_signup_response:I = 0x7f0b0107

.field public static final tv_sodium_result:I = 0x7f0b0089

.field public static final tv_speed_content:I = 0x7f0b0031

.field public static final tv_speed_title:I = 0x7f0b0030

.field public static final tv_sugars_result:I = 0x7f0b008d

.field public static final tv_summ_exercise_calories:I = 0x7f0b0126

.field public static final tv_summ_food_calories:I = 0x7f0b0125

.field public static final tv_summ_weight:I = 0x7f0b0127

.field public static final tv_summary:I = 0x7f0b000d

.field public static final tv_target_date_content:I = 0x7f0b0102

.field public static final tv_target_date_title:I = 0x7f0b0101

.field public static final tv_terms:I = 0x7f0b012b

.field public static final tv_thiamin_result:I = 0x7f0b0094

.field public static final tv_time_content:I = 0x7f0b0024

.field public static final tv_time_result:I = 0x7f0b00af

.field public static final tv_time_title:I = 0x7f0b0023

.field public static final tv_title:I = 0x7f0b000c

.field public static final tv_title_main:I = 0x7f0b0021

.field public static final tv_total_carbohydrate_result:I = 0x7f0b008b

.field public static final tv_total_fat_result:I = 0x7f0b0084

.field public static final tv_trans_fat_result:I = 0x7f0b0086

.field public static final tv_type_title:I = 0x7f0b0027

.field public static final tv_units:I = 0x7f0b001a

.field public static final tv_value:I = 0x7f0b000f

.field public static final tv_vitamin_a_result:I = 0x7f0b008f

.field public static final tv_vitamin_b12_result:I = 0x7f0b0099

.field public static final tv_vitamin_b6_result:I = 0x7f0b0097

.field public static final tv_vitamin_c_result:I = 0x7f0b0090

.field public static final tv_vitamin_d_result:I = 0x7f0b0091

.field public static final tv_vitamin_e_result:I = 0x7f0b0092

.field public static final tv_vitamin_k_result:I = 0x7f0b0093

.field public static final tv_water_units_content:I = 0x7f0b00f4

.field public static final tv_water_units_title:I = 0x7f0b00f3

.field public static final tv_where_content:I = 0x7f0b00dd

.field public static final tv_where_title:I = 0x7f0b00dc

.field public static final tv_with_whom_content:I = 0x7f0b00e0

.field public static final tv_with_whom_title:I = 0x7f0b00df

.field public static final tv_zinc_result:I = 0x7f0b00a0

.field public static final vs_captcha:I = 0x7f0b010b

.field public static final webview:I = 0x7f0b0131

.field public static final weight_dialog:I = 0x7f0b0133


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
