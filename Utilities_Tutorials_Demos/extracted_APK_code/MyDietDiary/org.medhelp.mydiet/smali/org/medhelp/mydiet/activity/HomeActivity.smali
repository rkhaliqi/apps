.class public Lorg/medhelp/mydiet/activity/HomeActivity;
.super Landroid/app/ActivityGroup;
.source "HomeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;,
        Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;,
        Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;,
        Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;
    }
.end annotation


# static fields
.field private static final TAB_FORUMS:Ljava/lang/String; = "Forums"

.field private static final TAB_STATE:Ljava/lang/String; = "tab_state"

.field private static final TAB_TRACK_IT:Ljava/lang/String; = "Track It"


# instance fields
.field private configChanges:I

.field private tabTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/ActivityGroup;-><init>()V

    .line 46
    const-string v0, "Track It"

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->tabTag:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->configChanges:I

    .line 39
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/HomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 165
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/HomeActivity;->startUserConfigsSync()V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/HomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 169
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/HomeActivity;->startUserDataSync()V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/HomeActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->tabTag:Ljava/lang/String;

    return-void
.end method

.method private addTab(Ljava/lang/String;ILandroid/content/Intent;)V
    .locals 11
    .parameter "label"
    .parameter "drawableId"
    .parameter "intent"

    .prologue
    const/4 v10, -0x2

    .line 88
    const v7, 0x7f0b00cf

    invoke-virtual {p0, v7}, Lorg/medhelp/mydiet/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TabHost;

    .line 89
    .local v5, tabHost:Landroid/widget/TabHost;
    invoke-virtual {v5, p1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v4

    .line 91
    .local v4, spec:Landroid/widget/TabHost$TabSpec;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 93
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v7, 0x7f030030

    invoke-virtual {v5}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v1, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 94
    .local v3, ll:Landroid/widget/LinearLayout;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x3f80

    invoke-direct {v2, v10, v10, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 95
    .local v2, layoutParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    const v7, 0x7f0b0114

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 98
    .local v6, title:Landroid/widget/TextView;
    invoke-virtual {v6, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    const v7, 0x7f0b0113

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 100
    .local v0, icon:Landroid/widget/ImageView;
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 102
    invoke-virtual {v4, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    .line 103
    invoke-virtual {v4, p3}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    .line 104
    invoke-virtual {v5, v4}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 106
    new-instance v7, Lorg/medhelp/mydiet/activity/HomeActivity$1;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/HomeActivity$1;-><init>(Lorg/medhelp/mydiet/activity/HomeActivity;)V

    invoke-virtual {v5, v7}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 113
    return-void
.end method

.method private createAnonymousUser()V
    .locals 4

    .prologue
    .line 162
    new-instance v0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;-><init>(Lorg/medhelp/mydiet/activity/HomeActivity;Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 163
    return-void
.end method

.method private initializeSync()V
    .locals 6

    .prologue
    .line 143
    invoke-static {p0}, Lorg/medhelp/mydiet/util/NetworkUtil;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p0}, Lorg/medhelp/mydiet/util/NetworkUtil;->isNetworkRoaming(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 144
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 146
    .local v2, userType:Ljava/lang/String;
    const-string v3, "medhelp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 147
    const-string v3, "anonymous"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 148
    :cond_0
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, userName:Ljava/lang/String;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserPassword(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, password:Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 151
    new-instance v3, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;-><init>(Lorg/medhelp/mydiet/activity/HomeActivity;Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;)V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/activity/HomeActivity$LoginTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 159
    .end local v0           #password:Ljava/lang/String;
    .end local v1           #userName:Ljava/lang/String;
    .end local v2           #userType:Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 153
    .restart local v0       #password:Ljava/lang/String;
    .restart local v1       #userName:Ljava/lang/String;
    .restart local v2       #userType:Ljava/lang/String;
    :cond_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/HomeActivity;->createAnonymousUser()V

    goto :goto_0

    .line 156
    .end local v0           #password:Ljava/lang/String;
    .end local v1           #userName:Ljava/lang/String;
    :cond_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/HomeActivity;->createAnonymousUser()V

    goto :goto_0
.end method

.method private setTabs()V
    .locals 4

    .prologue
    .line 75
    const v2, 0x7f0b00cf

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TabHost;

    .line 76
    .local v1, tabHost:Landroid/widget/TabHost;
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/HomeActivity;->getLocalActivityManager()Landroid/app/LocalActivityManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->setup(Landroid/app/LocalActivityManager;)V

    .line 78
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 79
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "Track It"

    const v3, 0x7f02009b

    invoke-direct {p0, v2, v3, v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->addTab(Ljava/lang/String;ILandroid/content/Intent;)V

    .line 81
    new-instance v0, Landroid/content/Intent;

    .end local v0           #intent:Landroid/content/Intent;
    const-class v2, Lorg/medhelp/mydiet/activity/WebViewSimple;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v2, "Forums"

    const v3, 0x7f020098

    invoke-direct {p0, v2, v3, v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->addTab(Ljava/lang/String;ILandroid/content/Intent;)V

    .line 84
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->tabTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method private startUserConfigsSync()V
    .locals 4

    .prologue
    .line 166
    new-instance v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;-><init>(Lorg/medhelp/mydiet/activity/HomeActivity;Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 167
    return-void
.end method

.method private startUserDataSync()V
    .locals 4

    .prologue
    .line 170
    new-instance v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;-><init>(Lorg/medhelp/mydiet/activity/HomeActivity;Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 171
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 127
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 139
    :goto_0
    :pswitch_0
    return-void

    .line 129
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 130
    .local v0, geIntent:Landroid/content/Intent;
    const-string v2, "about"

    const-string v3, "ge"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 134
    .end local v0           #geIntent:Landroid/content/Intent;
    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lorg/medhelp/mydiet/activity/AboutCompanyActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 135
    .local v1, medhelpIntent:Landroid/content/Intent;
    const-string v2, "about"

    const-string v3, "medhelp"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 127
    :pswitch_data_0
    .packed-switch 0x7f0b00ca
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f03001c

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->setContentView(I)V

    .line 57
    const v0, 0x7f0b00ca

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    const v0, 0x7f0b00cc

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    if-eqz p1, :cond_1

    .line 61
    const-string v0, "tab_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->tabTag:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->tabTag:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "Track It"

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->tabTag:Ljava/lang/String;

    .line 64
    :cond_0
    const-string v0, "config_changes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->configChanges:I

    .line 67
    :cond_1
    iget v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->configChanges:I

    if-nez v0, :cond_2

    .line 68
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/HomeActivity;->initializeSync()V

    .line 71
    :cond_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/HomeActivity;->setTabs()V

    .line 72
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 123
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 116
    const-string v0, "tab_state"

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->tabTag:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v0, "config_changes"

    iget v1, p0, Lorg/medhelp/mydiet/activity/HomeActivity;->configChanges:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 119
    return-void
.end method
