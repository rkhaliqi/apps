.class public Lorg/medhelp/mydiet/activity/WaterActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "WaterActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;,
        Lorg/medhelp/mydiet/activity/WaterActivity$SyncUserDataTask;,
        Lorg/medhelp/mydiet/activity/WaterActivity$UpdateWaterTask;
    }
.end annotation


# instance fields
.field private hasEdits:Z

.field mDate:Ljava/util/Date;

.field mDialog:Landroid/app/AlertDialog;

.field mProgress:Landroid/widget/ProgressBar;

.field mWater:D


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->hasEdits:Z

    .line 34
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/WaterActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 280
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WaterActivity;->showProgressBar()V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/WaterActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 284
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WaterActivity;->hideProgressBar()V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/WaterActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WaterActivity;->refreshViewContent()V

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/WaterActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 288
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WaterActivity;->showAlertDialog()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/WaterActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 303
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WaterActivity;->hideAlertDialog()V

    return-void
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/WaterActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-boolean p1, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->hasEdits:Z

    return-void
.end method

.method private hideAlertDialog()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 307
    :cond_0
    return-void
.end method

.method private hideProgressBar()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 286
    return-void
.end method

.method private onUpdateClick()V
    .locals 13

    .prologue
    .line 131
    move-object v1, p0

    .line 132
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 133
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v10, "layout_inflater"

    invoke-virtual {v1, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 134
    .local v6, inflater:Landroid/view/LayoutInflater;
    const v11, 0x7f03000a

    const v10, 0x7f0b0019

    invoke-virtual {p0, v10}, Lorg/medhelp/mydiet/activity/WaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    invoke-virtual {v6, v11, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 135
    .local v9, v:Landroid/view/View;
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 136
    const-string v10, "Enter Water"

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 138
    const v10, 0x7f0b0018

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    .line 140
    .local v5, editor:Landroid/widget/EditText;
    const v10, 0x7f0b001a

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 141
    .local v8, tv:Landroid/widget/TextView;
    const-string v10, " glasses"

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-wide v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    .line 144
    .local v3, displayWater:D
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "mL"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 145
    const-wide v10, 0x406d92d2d55a3a08L

    mul-double/2addr v3, v10

    .line 146
    const-string v10, " mL"

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :cond_0
    :goto_0
    const-wide/16 v10, 0x0

    cmpl-double v10, v3, v10

    if-lez v10, :cond_1

    .line 153
    new-instance v7, Ljava/text/DecimalFormatSymbols;

    new-instance v10, Ljava/util/Locale;

    const-string v11, "en"

    const-string v12, "en"

    invoke-direct {v10, v11, v12}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v7, v10}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 154
    .local v7, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v10, 0x2e

    invoke-virtual {v7, v10}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 155
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v10, "#.#"

    invoke-direct {v2, v10, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 156
    .local v2, df:Ljava/text/DecimalFormat;
    invoke-virtual {v2, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 159
    .end local v2           #df:Ljava/text/DecimalFormat;
    .end local v7           #symbols:Ljava/text/DecimalFormatSymbols;
    :cond_1
    new-instance v10, Lorg/medhelp/mydiet/activity/WaterActivity$1;

    invoke-direct {v10, p0}, Lorg/medhelp/mydiet/activity/WaterActivity$1;-><init>(Lorg/medhelp/mydiet/activity/WaterActivity;)V

    invoke-virtual {v5, v10}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 168
    const-string v10, "OK"

    new-instance v11, Lorg/medhelp/mydiet/activity/WaterActivity$2;

    invoke-direct {v11, p0, v5}, Lorg/medhelp/mydiet/activity/WaterActivity$2;-><init>(Lorg/medhelp/mydiet/activity/WaterActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 195
    const-string v10, "Cancel"

    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 197
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    iput-object v10, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDialog:Landroid/app/AlertDialog;

    .line 198
    iget-object v10, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v10}, Landroid/app/AlertDialog;->show()V

    .line 199
    return-void

    .line 147
    :cond_2
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "fl oz"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 148
    const-wide/high16 v10, 0x4020

    mul-double/2addr v3, v10

    .line 149
    const-string v10, " fl oz"

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private refreshViewContent()V
    .locals 15

    .prologue
    const-wide/16 v13, 0x0

    .line 92
    new-instance v1, Ljava/text/DecimalFormatSymbols;

    new-instance v9, Ljava/util/Locale;

    const-string v10, "en"

    const-string v11, "en"

    invoke-direct {v9, v10, v11}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v9}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 93
    .local v1, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v9, 0x2e

    invoke-virtual {v1, v9}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 94
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v9, "#.#"

    invoke-direct {v0, v9, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 96
    .local v0, df:Ljava/text/DecimalFormat;
    const/4 v8, 0x0

    .line 97
    .local v8, waterString:Ljava/lang/String;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "mL"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 98
    iget-wide v9, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    const-wide v11, 0x406d92d2d55a3a08L

    mul-double v6, v9, v11

    .line 99
    .local v6, waterInML:D
    cmpl-double v9, v6, v13

    if-lez v9, :cond_1

    .line 100
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " mL"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 124
    .end local v6           #waterInML:D
    :goto_0
    if-eqz v8, :cond_0

    .line 125
    const v9, 0x7f0b012e

    invoke-virtual {p0, v9}, Lorg/medhelp/mydiet/activity/WaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 126
    .local v2, tv:Landroid/widget/TextView;
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    .end local v2           #tv:Landroid/widget/TextView;
    :cond_0
    return-void

    .line 102
    .restart local v6       #waterInML:D
    :cond_1
    const-string v8, ""

    goto :goto_0

    .line 104
    .end local v6           #waterInML:D
    :cond_2
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "fl oz"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 105
    iget-wide v9, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    const-wide/high16 v11, 0x4020

    mul-double v4, v9, v11

    .line 106
    .local v4, waterInFLOZ:D
    cmpl-double v9, v4, v13

    if-lez v9, :cond_3

    .line 107
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " fl oz"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 109
    :cond_3
    const-string v8, ""

    goto :goto_0

    .line 112
    .end local v4           #waterInFLOZ:D
    :cond_4
    iget-wide v9, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    cmpl-double v9, v9, v13

    if-lez v9, :cond_7

    .line 113
    iget-wide v9, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    invoke-virtual {v0, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, waterAmountString:Ljava/lang/String;
    const-string v9, "1"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_5

    const-string v9, "1.0"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 115
    :cond_5
    const-string v8, "1 glass"

    goto :goto_0

    .line 117
    :cond_6
    new-instance v9, Ljava/lang/StringBuilder;

    iget-wide v10, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    invoke-virtual {v0, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " glasses"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 120
    .end local v3           #waterAmountString:Ljava/lang/String;
    :cond_7
    const-string v8, ""

    goto :goto_0
.end method

.method private showAlertDialog()V
    .locals 6

    .prologue
    .line 289
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/WaterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 290
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v5, 0x7f03002c

    .line 291
    const v4, 0x7f0b0111

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/WaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 290
    invoke-virtual {v1, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 293
    .local v2, layout:Landroid/view/View;
    const v4, 0x7f0b001e

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 294
    .local v3, tv:Landroid/widget/TextView;
    const-string v4, "Updating water..."

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 297
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 298
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 299
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDialog:Landroid/app/AlertDialog;

    .line 300
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 301
    return-void
.end method

.method private showProgressBar()V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 282
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 78
    :goto_0
    return-void

    .line 72
    :sswitch_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/WaterActivity;->onUpdateClick()V

    goto :goto_0

    .line 75
    :sswitch_1
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/WaterActivity;->finish()V

    goto :goto_0

    .line 70
    :sswitch_data_0
    .sparse-switch
        0x7f0b0081 -> :sswitch_1
        0x7f0b0119 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    .line 47
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v3, 0x7f030032

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WaterActivity;->setContentView(I)V

    .line 50
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/WaterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "date"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 52
    .local v0, timeInMillis:J
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-lez v3, :cond_0

    .line 53
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDate:Ljava/util/Date;

    .line 58
    :goto_0
    const v3, 0x7f0b0001

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 59
    const v3, 0x7f0b0081

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v3, 0x7f0b0119

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    new-instance v3, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;-><init>(Lorg/medhelp/mydiet/activity/WaterActivity;Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 64
    const v3, 0x7f0b0036

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/WaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 65
    .local v2, tvDate:Landroid/widget/TextView;
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDate:Ljava/util/Date;

    const-string v4, "EEEE MMM d, yyyy"

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    return-void

    .line 55
    .end local v2           #tvDate:Landroid/widget/TextView;
    :cond_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDate:Ljava/util/Date;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 81
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 84
    :cond_0
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity;->hasEdits:Z

    if-eqz v0, :cond_1

    .line 86
    new-instance v0, Lorg/medhelp/mydiet/activity/WaterActivity$SyncUserDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/WaterActivity$SyncUserDataTask;-><init>(Lorg/medhelp/mydiet/activity/WaterActivity;Lorg/medhelp/mydiet/activity/WaterActivity$SyncUserDataTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/WaterActivity$SyncUserDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 88
    :cond_1
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 89
    return-void
.end method
