.class public Lorg/medhelp/mydiet/model/NutrientDAO;
.super Ljava/lang/Object;
.source "NutrientDAO.java"


# instance fields
.field public nutrients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/medhelp/mydiet/model/Nutrient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    invoke-direct {p0}, Lorg/medhelp/mydiet/model/NutrientDAO;->getNutrients()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/model/NutrientDAO;->nutrients:Ljava/util/HashMap;

    .line 5
    return-void
.end method

.method private getNutrients()Ljava/util/HashMap;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/medhelp/mydiet/model/Nutrient;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    .line 10
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 12
    .local v11, nutrients:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lorg/medhelp/mydiet/model/Nutrient;>;"
    const-string v6, "calories"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "calories"

    const-string v2, "Calories"

    const-string v3, "Cal"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    const-string v6, "calories_from_fat"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "calories_from_fat"

    const-string v2, "Calories from fat"

    const-string v3, "Cal"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    const-string v6, "total_fat"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "total_fat"

    const-string v2, "Total Fat"

    const-string v3, "g"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    const-string v6, "saturated_fatty_acids"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "saturated_fatty_acids"

    const-string v2, "Saturated Fat"

    const-string v3, "g"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    const-string v6, "total_trans_fatty_acids"

    .line 17
    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "total_trans_fatty_acids"

    const-string v2, "Trans fat"

    const-string v3, "g"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 16
    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    const-string v6, "total_monounsaturated_fatty_acids"

    .line 19
    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "total_monounsaturated_fatty_acids"

    const-string v2, "Monounsaturated fat"

    const-string v3, "g"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 18
    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    const-string v6, "cholesterol"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "cholesterol"

    const-string v2, "Cholesterol"

    const-string v3, "mg"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    const-string v6, "sodium"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "sodium"

    const-string v2, "Sodium"

    const-string v3, "mg"

    move v5, v9

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    const-string v0, "potassium"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "potassium"

    const-string v7, "Potassium"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    const-string v6, "carbohydrate"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "carbohydrate"

    const-string v2, "Total Carbohydrate"

    const-string v3, "g"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    const-string v6, "fiber"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "fiber"

    const-string v2, "Dietary Fiber"

    const-string v3, "g"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    const-string v6, "total_sugars"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "total_sugars"

    const-string v2, "Sugars"

    const-string v3, "g"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    const-string v6, "protein"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "protein"

    const-string v2, "Protein"

    const-string v3, "g"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    const-string v0, "vitamin_a"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "vitamin_a"

    const-string v7, "Vitamin A"

    const-string v8, "IU"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    const-string v0, "vitamin_c"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "vitamin_c"

    const-string v7, "Vitamin C"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    const-string v0, "vitamin_d"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "vitamin_d"

    const-string v7, "Vitamin D"

    const-string v8, "IU"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    const-string v0, "vitamin_e"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "vitamin_e"

    const-string v7, "Vitamin E"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    const-string v0, "vitamin_k"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "vitamin_k"

    const-string v7, "Vitamin K"

    const-string v8, "mcg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const-string v0, "thiamin"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "thiamin"

    const-string v7, "Thiamin"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const-string v0, "riboflavin"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "riboflavin"

    const-string v7, "Riboflavin"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const-string v0, "vitamin_b6"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "vitamin_b6"

    const-string v7, "Vitamin B-6"

    const-string v8, "mh"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    const-string v0, "folate"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "folate"

    const-string v7, "Folate"

    const-string v8, "mcg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const-string v0, "vitamin_b12"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "vitamin_b12"

    const-string v7, "Vitamin B-12"

    const-string v8, "mcg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const-string v0, "biotin"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "biotin"

    const-string v7, "Biotin"

    const-string v8, "mcg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    const-string v0, "pantothenic_acid"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "pantothenic_acid"

    const-string v7, "Pantothenic Acid"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    const-string v0, "calcium"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "calcium"

    const-string v7, "Calcium"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    const-string v0, "iron"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "iron"

    const-string v7, "Iron"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string v0, "iodine"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "iodine"

    const-string v7, "Iodine"

    const-string v8, "mcg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string v0, "magnesium"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "magnesium"

    const-string v7, "Magnesium"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    const-string v0, "zinc"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "zinc"

    const-string v7, "Zinc"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const-string v0, "selenium"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "selenium"

    const-string v7, "Selenium"

    const-string v8, "mcg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const-string v0, "copper"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "copper"

    const-string v7, "Copper"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const-string v0, "manganese"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "manganese"

    const-string v7, "Manganese"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v0, "chromium"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "chromium"

    const-string v7, "Chromium"

    const-string v8, "mcg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v0, "molybdenum"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "molybdenum"

    const-string v7, "Molybdenum"

    const-string v8, "mcg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v6, "fluoride"

    new-instance v0, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v1, "fluoride"

    const-string v2, "Fluoride"

    const-string v3, "mcg"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v0, "phosphorus"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "phosphorus"

    const-string v7, "Phosphorus"

    const-string v8, "mg"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v0, "chloride"

    new-instance v5, Lorg/medhelp/mydiet/model/Nutrient;

    const-string v6, "chloride"

    const-string v7, "Chloride"

    const-string v8, "g"

    move v10, v9

    invoke-direct/range {v5 .. v10}, Lorg/medhelp/mydiet/model/Nutrient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v11, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-object v11
.end method
