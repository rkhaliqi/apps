.class public final Lorg/medhelp/mydiet/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final about:I = 0x7f030000

.field public static final about_text:I = 0x7f030001

.field public static final activity_level_info:I = 0x7f030002

.field public static final black_divider:I = 0x7f030003

.field public static final break_down_content_items:I = 0x7f030004

.field public static final delete_item_2:I = 0x7f030005

.field public static final details_content_item:I = 0x7f030006

.field public static final details_header_item:I = 0x7f030007

.field public static final dialog_calorie_calculation:I = 0x7f030008

.field public static final dialog_calories:I = 0x7f030009

.field public static final dialog_decimal_entry_field:I = 0x7f03000a

.field public static final dialog_image:I = 0x7f03000b

.field public static final dialog_loading_1:I = 0x7f03000c

.field public static final dialog_text_entry:I = 0x7f03000d

.field public static final exercise_edit:I = 0x7f03000e

.field public static final exercises_day_edit:I = 0x7f03000f

.field public static final exercises_list:I = 0x7f030010

.field public static final food_add_food:I = 0x7f030011

.field public static final food_add_food_list_item:I = 0x7f030012

.field public static final food_create:I = 0x7f030013

.field public static final food_day_meals:I = 0x7f030014

.field public static final food_details:I = 0x7f030015

.field public static final food_meal_finalize:I = 0x7f030016

.field public static final food_meal_type:I = 0x7f030017

.field public static final food_meals_list_item:I = 0x7f030018

.field public static final food_select_food_item:I = 0x7f030019

.field public static final height_dialog_cm:I = 0x7f03001a

.field public static final height_dialog_ft:I = 0x7f03001b

.field public static final home:I = 0x7f03001c

.field public static final list_divider:I = 0x7f03001d

.field public static final list_footer_create_food_item:I = 0x7f03001e

.field public static final list_footer_item:I = 0x7f03001f

.field public static final list_item_1:I = 0x7f030020

.field public static final list_item_mealtype:I = 0x7f030021

.field public static final login:I = 0x7f030022

.field public static final meal_location_details:I = 0x7f030023

.field public static final note_editor:I = 0x7f030024

.field public static final settings_logout:I = 0x7f030025

.field public static final setup_screen_1:I = 0x7f030026

.field public static final setup_screen_2:I = 0x7f030027

.field public static final setup_screen_3:I = 0x7f030028

.field public static final setup_screen_4:I = 0x7f030029

.field public static final setup_screen_5:I = 0x7f03002a

.field public static final signup:I = 0x7f03002b

.field public static final simple_progress_dialog:I = 0x7f03002c

.field public static final splash:I = 0x7f03002d

.field public static final tab_forums:I = 0x7f03002e

.field public static final tab_track_it:I = 0x7f03002f

.field public static final tabwidget:I = 0x7f030030

.field public static final terms:I = 0x7f030031

.field public static final water:I = 0x7f030032

.field public static final webview:I = 0x7f030033

.field public static final weight:I = 0x7f030034

.field public static final weight_dialog:I = 0x7f030035

.field public static final welcome:I = 0x7f030036


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
