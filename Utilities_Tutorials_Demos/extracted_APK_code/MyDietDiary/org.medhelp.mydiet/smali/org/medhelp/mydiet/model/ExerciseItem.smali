.class public Lorg/medhelp/mydiet/model/ExerciseItem;
.super Ljava/lang/Object;
.source "ExerciseItem.java"


# instance fields
.field public calories:D

.field public date:J

.field public distance:D

.field public exerciseMHKey:Ljava/lang/String;

.field public exerciseType:Ljava/lang/String;

.field public inAppId:J

.field public lastUpdatedTime:J

.field public name:Ljava/lang/String;

.field public speed:D

.field public time:J

.field public timePeriod:D


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const-wide/16 v0, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-wide v2, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    .line 16
    iput-wide v0, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    .line 17
    iput-wide v0, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    .line 18
    iput-wide v0, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    .line 19
    iput-wide v2, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    .line 20
    iput-wide v0, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    .line 10
    return-void
.end method


# virtual methods
.method public getSummary()Ljava/lang/String;
    .locals 5

    .prologue
    .line 27
    new-instance v1, Ljava/text/DecimalFormatSymbols;

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "en"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 28
    .local v1, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 29
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v2, "0"

    invoke-direct {v0, v2, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 31
    .local v0, df0:Ljava/text/DecimalFormat;
    new-instance v2, Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " minutes "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " calories burned"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 35
    if-nez p0, :cond_0

    const/4 v2, 0x0

    .line 54
    :goto_0
    return-object v2

    .line 37
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 40
    .local v1, exerciseObject:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "in_app_id"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 41
    const-string v2, "medhelp_id"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseMHKey:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 42
    const-string v2, "name"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 43
    const-string v2, "exercise_type"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 44
    const-string v2, "distance"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 45
    const-string v2, "speed"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 46
    const-string v2, "calories"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 47
    const-string v2, "time"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 48
    const-string v2, "time_period"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 49
    const-string v2, "date"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
