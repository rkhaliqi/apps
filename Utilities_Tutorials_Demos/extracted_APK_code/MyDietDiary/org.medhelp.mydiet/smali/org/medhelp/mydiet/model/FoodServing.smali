.class public Lorg/medhelp/mydiet/model/FoodServing;
.super Ljava/lang/Object;
.source "FoodServing.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/medhelp/mydiet/model/FoodServing;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:D

.field public equivalent:D

.field public foodId:J

.field public id:J

.field public medhelpId:J

.field public name:Ljava/lang/String;

.field public priority:I

.field public updatedAt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IJJDDLjava/lang/String;I)V
    .locals 2
    .parameter "id"
    .parameter "medhelpId"
    .parameter "foodId"
    .parameter "equivalent"
    .parameter "servingAmount"
    .parameter "name"
    .parameter "priority"

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    int-to-long v0, p1

    iput-wide v0, p0, Lorg/medhelp/mydiet/model/FoodServing;->id:J

    .line 24
    iput-wide p2, p0, Lorg/medhelp/mydiet/model/FoodServing;->medhelpId:J

    .line 25
    iput-wide p4, p0, Lorg/medhelp/mydiet/model/FoodServing;->foodId:J

    .line 26
    iput-object p10, p0, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    .line 27
    iput-wide p6, p0, Lorg/medhelp/mydiet/model/FoodServing;->equivalent:D

    .line 28
    iput-wide p8, p0, Lorg/medhelp/mydiet/model/FoodServing;->amount:D

    .line 29
    iput p11, p0, Lorg/medhelp/mydiet/model/FoodServing;->priority:I

    .line 30
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lorg/medhelp/mydiet/model/FoodServing;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/model/FoodServing;->compareTo(Lorg/medhelp/mydiet/model/FoodServing;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/medhelp/mydiet/model/FoodServing;)I
    .locals 2
    .parameter "foodServing"

    .prologue
    .line 55
    iget v0, p0, Lorg/medhelp/mydiet/model/FoodServing;->priority:I

    iget v1, p1, Lorg/medhelp/mydiet/model/FoodServing;->priority:I

    if-le v0, v1, :cond_0

    .line 56
    const/4 v0, 0x1

    .line 60
    :goto_0
    return v0

    .line 57
    :cond_0
    iget v0, p0, Lorg/medhelp/mydiet/model/FoodServing;->priority:I

    iget v1, p1, Lorg/medhelp/mydiet/model/FoodServing;->priority:I

    if-ge v0, v1, :cond_1

    .line 58
    const/4 v0, -0x1

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    iget-object v1, p1, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public toJSONObject()Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 37
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 40
    .local v1, serving:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "id"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodServing;->medhelpId:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 41
    const-string v2, "food_id"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodServing;->foodId:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 42
    const-string v2, "equivalent"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodServing;->equivalent:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 43
    const-string v2, "amount"

    iget-wide v3, p0, Lorg/medhelp/mydiet/model/FoodServing;->amount:D

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 44
    const-string v2, "name"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 45
    const-string v2, "priority"

    iget v3, p0, Lorg/medhelp/mydiet/model/FoodServing;->priority:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 46
    const-string v2, "updated_at"

    iget-object v3, p0, Lorg/medhelp/mydiet/model/FoodServing;->updatedAt:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_0
    return-object v1

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    return-object v0
.end method
