.class public abstract Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AddFoodsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
        ">;"
    }
.end annotation


# instance fields
.field private holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

.field private mFoods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field public mSelectedFoods:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/util/ArrayList;)V
    .locals 1
    .parameter "context"
    .parameter "resource"
    .parameter "textViewResourceId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p4, objects:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 36
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 37
    iput-object p4, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mFoods:Ljava/util/List;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    .line 39
    return-void
.end method


# virtual methods
.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->add(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    return-void
.end method

.method public add(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V
    .locals 0
    .parameter "foodItem"

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 130
    return-void
.end method

.method public abstract checkedFoodItem()V
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mFoods:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->getItem(I)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .locals 1
    .parameter "position"

    .prologue
    .line 46
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mFoods:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 59
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mFoods:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    return-wide v0
.end method

.method public bridge synthetic getPosition(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->getPosition(Lorg/medhelp/mydiet/model/FoodItemInMeal;)I

    move-result v0

    return v0
.end method

.method public getPosition(Lorg/medhelp/mydiet/model/FoodItemInMeal;)I
    .locals 1
    .parameter "item"

    .prologue
    .line 55
    iget-object v0, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mFoods:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 65
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->getItem(I)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v0

    .line 66
    .local v0, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    if-nez p2, :cond_2

    .line 67
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030012

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 68
    new-instance v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    invoke-direct {v2}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;-><init>()V

    iput-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    .line 69
    iget-object v3, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    const v2, 0x7f0b0049

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->tvFoodItemTitle:Landroid/widget/TextView;

    .line 70
    iget-object v3, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    const v2, 0x7f0b004a

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->tvFoodItemDescription:Landroid/widget/TextView;

    .line 71
    iget-object v3, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    const v2, 0x7f0b0047

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v3, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->cbFoodItemChecker:Landroid/widget/CheckBox;

    .line 72
    iget-object v3, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    const v2, 0x7f0b0048

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->ivFoodItemSource:Landroid/widget/ImageView;

    .line 74
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 78
    :goto_0
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->cbFoodItemChecker:Landroid/widget/CheckBox;

    new-instance v3, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;

    invoke-direct {v3, p0, v0}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$1;-><init>(Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 93
    if-eqz v0, :cond_1

    .line 94
    iget-object v1, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    .line 95
    .local v1, title:Ljava/lang/String;
    iget-object v2, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 96
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 97
    :cond_0
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->tvFoodItemTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v2, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodSource:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 100
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->ivFoodItemSource:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    iget-object v2, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodSource:Ljava/lang/String;

    const-string v3, "my_foods"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 102
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->ivFoodItemSource:Landroid/widget/ImageView;

    const v3, 0x7f020091

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 114
    :goto_1
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->tvFoodItemDescription:Landroid/widget/TextView;

    iget-object v3, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->cbFoodItemChecker:Landroid/widget/CheckBox;

    iget-boolean v3, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 117
    .end local v1           #title:Ljava/lang/String;
    :cond_1
    return-object p2

    .line 76
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iput-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    goto :goto_0

    .line 103
    .restart local v1       #title:Ljava/lang/String;
    :cond_3
    iget-object v2, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodSource:Ljava/lang/String;

    const-string v3, "mh_approved_foods"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 104
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->ivFoodItemSource:Landroid/widget/ImageView;

    const v3, 0x7f020090

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 105
    :cond_4
    iget-object v2, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->foodSource:Ljava/lang/String;

    const-string v3, "user_foods"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 106
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->ivFoodItemSource:Landroid/widget/ImageView;

    const v3, 0x7f0200a2

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 108
    :cond_5
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->ivFoodItemSource:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 111
    :cond_6
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->holder:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter$ViewHolder;->ivFoodItemSource:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public markSelectedFoodItems(Ljava/util/ArrayList;)V
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, foodItemsToDisplay:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 152
    :cond_0
    return-void

    .line 142
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 146
    .local v0, displayItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-object v3, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 147
    .local v1, selectedItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-wide v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    iget-wide v6, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 148
    invoke-virtual {v1, v0}, Lorg/medhelp/mydiet/model/FoodItemInMeal;->copyFoodItemTo(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 175
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 176
    return-void
.end method

.method public selectFoodItem(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V
    .locals 6
    .parameter "foodItem"

    .prologue
    .line 155
    if-nez p1, :cond_0

    .line 171
    :goto_0
    return-void

    .line 158
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    .line 160
    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 161
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    .line 163
    :cond_1
    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 170
    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 164
    .local v0, selectedItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-wide v2, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    iget-wide v4, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 165
    invoke-virtual {p1, v0}, Lorg/medhelp/mydiet/model/FoodItemInMeal;->copyFoodItemTo(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V

    goto :goto_0
.end method

.method public abstract uncheckedFoodItem()V
.end method

.method public unselectFoodItem(Lorg/medhelp/mydiet/model/FoodItemInMeal;)V
    .locals 6
    .parameter "foodItem"

    .prologue
    .line 179
    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    if-eqz p1, :cond_0

    .line 185
    const/4 v1, 0x0

    iput-boolean v1, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    .line 187
    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 188
    .local v0, selectedItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-wide v2, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    iget-wide v4, p1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 189
    iget-object v1, p0, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
