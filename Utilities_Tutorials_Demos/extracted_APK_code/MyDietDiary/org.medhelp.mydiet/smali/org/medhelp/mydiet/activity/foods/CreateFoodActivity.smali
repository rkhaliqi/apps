.class public Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "CreateFoodActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;,
        Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;,
        Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingNameSelectedListener;,
        Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;,
        Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;
    }
.end annotation


# instance fields
.field mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

.field private mDialog:Landroid/app/AlertDialog;

.field private mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

.field private mItemSelectedByUser:Z

.field mServingsInfoDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mItemSelectedByUser:Z

    .line 47
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mItemSelectedByUser:Z

    return v0
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showServingNameEntryDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    iput-boolean p1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mItemSelectedByUser:Z

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 229
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showSavingDialog()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 501
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->validateAllFields()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Lorg/medhelp/mydiet/model/FoodDetail;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    return-object v0
.end method

.method static synthetic access$6(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Lorg/medhelp/mydiet/model/FoodDetail;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    return-void
.end method

.method static synthetic access$7(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Landroid/app/AlertDialog;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$8(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->addServingName(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$9(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1197
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->requestFocusOnView(I)V

    return-void
.end method

.method private addServingName(Ljava/lang/String;)V
    .locals 6
    .parameter "servingName"

    .prologue
    .line 146
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    const v4, 0x7f0b0050

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 150
    .local v1, s:Landroid/widget/Spinner;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v2, servingNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/CharSequence;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    sget-object v4, Lorg/medhelp/mydiet/C$arrays;->servingNames:[Ljava/lang/String;

    array-length v4, v4

    if-lt v0, v4, :cond_2

    .line 156
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " (other)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x1090008

    invoke-direct {v3, p0, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 159
    .local v3, servingsAdapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v1, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 160
    const v4, 0x1090009

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 161
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mItemSelectedByUser:Z

    .line 162
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    .line 152
    .end local v3           #servingsAdapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    :cond_2
    sget-object v4, Lorg/medhelp/mydiet/C$arrays;->servingNames:[Ljava/lang/String;

    aget-object v4, v4, v0

    const-string v5, "other"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 153
    sget-object v4, Lorg/medhelp/mydiet/C$arrays;->servingNames:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private initializeFoodGroups()V
    .locals 4

    .prologue
    .line 123
    const v2, 0x7f0b004d

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 126
    .local v1, s:Landroid/widget/Spinner;
    const v2, 0x7f060009

    const v3, 0x1090008

    .line 125
    invoke-static {p0, v2, v3}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 127
    .local v0, adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 128
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 129
    return-void
.end method

.method private initializeServingNames()V
    .locals 4

    .prologue
    .line 132
    const v2, 0x7f0b0050

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 134
    .local v1, s:Landroid/widget/Spinner;
    new-instance v0, Landroid/widget/ArrayAdapter;

    .line 135
    const v2, 0x1090008

    .line 136
    sget-object v3, Lorg/medhelp/mydiet/C$arrays;->servingNames:[Ljava/lang/String;

    .line 134
    invoke-direct {v0, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 137
    .local v0, adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 139
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 140
    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 142
    new-instance v2, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingNameSelectedListener;

    invoke-direct {v2, p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingNameSelectedListener;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 143
    return-void
.end method

.method private onServingsInfoClick()V
    .locals 2

    .prologue
    .line 117
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;

    invoke-direct {v0, p0, p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mServingsInfoDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;

    .line 118
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mServingsInfoDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;

    const-string v1, "Test"

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mServingsInfoDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;->show()V

    .line 120
    return-void
.end method

.method private declared-synchronized requestFocusOnView(I)V
    .locals 1
    .parameter "id"

    .prologue
    .line 1203
    monitor-enter p0

    const v0, 0x7f0b004b

    :try_start_0
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1204
    const v0, 0x7f0b004c

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1205
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1206
    monitor-exit p0

    return-void

    .line 1203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private resetFoodDetail()V
    .locals 5

    .prologue
    .line 1209
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    if-nez v4, :cond_0

    .line 1210
    new-instance v4, Lorg/medhelp/mydiet/model/FoodDetail;

    invoke-direct {v4}, Lorg/medhelp/mydiet/model/FoodDetail;-><init>()V

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    .line 1212
    :cond_0
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v0, v4, Lorg/medhelp/mydiet/model/FoodDetail;->inAppId:J

    .line 1213
    .local v0, inAppId:J
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iget-wide v2, v4, Lorg/medhelp/mydiet/model/FoodDetail;->medhelpId:J

    .line 1214
    .local v2, medhelpId:J
    new-instance v4, Lorg/medhelp/mydiet/model/FoodDetail;

    invoke-direct {v4}, Lorg/medhelp/mydiet/model/FoodDetail;-><init>()V

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    .line 1215
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iput-wide v0, v4, Lorg/medhelp/mydiet/model/FoodDetail;->inAppId:J

    .line 1216
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iput-wide v2, v4, Lorg/medhelp/mydiet/model/FoodDetail;->medhelpId:J

    .line 1217
    return-void
.end method

.method private setEditTexts()V
    .locals 2

    .prologue
    .line 239
    const/4 v0, 0x0

    .line 241
    .local v0, et:Landroid/widget/EditText;
    const v1, 0x7f0b0053

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 242
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 244
    const v1, 0x7f0b0054

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 245
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 247
    const v1, 0x7f0b0055

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 248
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 250
    const v1, 0x7f0b0056

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 251
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 253
    const v1, 0x7f0b0057

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 254
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 256
    const v1, 0x7f0b0058

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 257
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 259
    const v1, 0x7f0b0059

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 260
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 262
    const v1, 0x7f0b005a

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 263
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 265
    const v1, 0x7f0b005b

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 266
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 268
    const v1, 0x7f0b005c

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 269
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 271
    const v1, 0x7f0b005d

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 272
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 274
    const v1, 0x7f0b005e

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 275
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 277
    const v1, 0x7f0b005f

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 278
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 280
    const v1, 0x7f0b0060

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 281
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 283
    const v1, 0x7f0b0061

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 284
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 286
    const v1, 0x7f0b0062

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 287
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 289
    const v1, 0x7f0b0063

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 290
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 292
    const v1, 0x7f0b0064

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 293
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 295
    const v1, 0x7f0b0065

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 296
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 298
    const v1, 0x7f0b0066

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 299
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 301
    const v1, 0x7f0b0067

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 302
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 304
    const v1, 0x7f0b0068

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 305
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 307
    const v1, 0x7f0b0069

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 308
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 310
    const v1, 0x7f0b006a

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 311
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 313
    const v1, 0x7f0b006b

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 314
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 316
    const v1, 0x7f0b006c

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 317
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 319
    const v1, 0x7f0b006d

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 320
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 322
    const v1, 0x7f0b006e

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 323
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 325
    const v1, 0x7f0b006f

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 326
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 328
    const v1, 0x7f0b0070

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 329
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 331
    const v1, 0x7f0b0071

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 332
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 334
    const v1, 0x7f0b0072

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 335
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 337
    const v1, 0x7f0b0073

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 338
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 340
    const v1, 0x7f0b0074

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 341
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 343
    const v1, 0x7f0b0075

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 344
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 346
    const v1, 0x7f0b0076

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 347
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 349
    const v1, 0x7f0b0077

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 350
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 352
    const v1, 0x7f0b0078

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 353
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 355
    const v1, 0x7f0b0079

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 356
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 357
    return-void
.end method

.method private showSavingDialog()V
    .locals 2

    .prologue
    .line 230
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    invoke-direct {v0, p0, p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    .line 231
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    const-string v1, "Saving food"

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    const-string v1, "Saving your food into database"

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->setCancelable(Z)V

    .line 235
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->show()V

    .line 236
    return-void
.end method

.method private showServingNameEntryDialog(Ljava/lang/String;)V
    .locals 7
    .parameter "servingName"

    .prologue
    .line 187
    move-object v1, p0

    .line 188
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 189
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v5, "layout_inflater"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 190
    .local v2, inflater:Landroid/view/LayoutInflater;
    const v6, 0x7f03000d

    const v5, 0x7f0b001f

    invoke-virtual {p0, v5}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v2, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 191
    .local v4, v:Landroid/view/View;
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 192
    const-string v5, "Enter Serving Name"

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 194
    const v5, 0x7f0b0020

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 197
    .local v3, servingNameEditor:Landroid/widget/EditText;
    if-eqz p1, :cond_0

    const-string v5, ""

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 198
    :cond_0
    const-string p1, ""

    .line 200
    :cond_1
    invoke-virtual {v3, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 202
    new-instance v5, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$1;

    invoke-direct {v5, p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$1;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)V

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 211
    const-string v5, "OK"

    new-instance v6, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$2;

    invoke-direct {v6, p0, v3}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$2;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 223
    const-string v5, "Cancel"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 225
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;

    .line 226
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 227
    return-void
.end method

.method private showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "title"
    .parameter "message"

    .prologue
    .line 1169
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1170
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1171
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1172
    const-string v1, "OK"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1173
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;

    .line 1174
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1175
    return-void
.end method

.method private showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .parameter "title"
    .parameter "message"
    .parameter "viewToFocus"

    .prologue
    .line 1178
    const/4 v1, -0x1

    if-ne p3, v1, :cond_0

    .line 1179
    invoke-direct {p0, p1, p2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1195
    :goto_0
    return-void

    .line 1183
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1184
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1185
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1186
    const-string v1, "OK"

    new-instance v2, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$3;

    invoke-direct {v2, p0, p3}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$3;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1193
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;

    .line 1194
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private validateAllFields()Z
    .locals 15

    .prologue
    const v14, 0x7f020077

    const v13, 0x7f020020

    const/4 v7, 0x0

    const/high16 v12, -0x1

    const/high16 v11, -0x100

    .line 502
    const v8, 0x7f0b004b

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 503
    .local v0, et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 505
    .local v6, string:Ljava/lang/String;
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->resetFoodDetail()V

    .line 507
    new-instance v3, Lorg/medhelp/mydiet/model/FoodServing;

    invoke-direct {v3}, Lorg/medhelp/mydiet/model/FoodServing;-><init>()V

    .line 508
    .local v3, foodServing:Lorg/medhelp/mydiet/model/FoodServing;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 510
    .local v4, servings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodServing;>;"
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    .line 511
    :cond_0
    const-string v8, "Invalid Food Name"

    .line 512
    const-string v9, "Please enter a valid food name"

    .line 513
    const v10, 0x7f0b004b

    .line 511
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 514
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 515
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1165
    :goto_0
    return v7

    .line 518
    :cond_1
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->foodName:Ljava/lang/String;

    .line 519
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 520
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 523
    const v8, 0x7f0b004c

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 524
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 525
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_3

    .line 526
    :cond_2
    const-string v6, ""

    .line 528
    :cond_3
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iput-object v6, v8, Lorg/medhelp/mydiet/model/FoodDetail;->manufacturer:Ljava/lang/String;

    .line 530
    const v8, 0x7f0b004d

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    .line 531
    .local v5, spinner:Landroid/widget/Spinner;
    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 532
    const v8, 0x7f050001

    invoke-static {p0, v8}, Lorg/medhelp/mydiet/util/FileUtil;->readFile(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 533
    .local v2, foodGroupsString:Ljava/lang/String;
    invoke-static {v2}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodGroups(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 534
    .local v1, foodGroupsJSONArray:Lorg/json/JSONArray;
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v6}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodGroupId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->foodGroupId:I

    .line 536
    const v8, 0x7f0b004f

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 537
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 538
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 539
    :cond_4
    const-string v8, "Invalid Serving Amount"

    .line 540
    const-string v9, "Please enter a valid serving amount or enter 1"

    .line 541
    const v10, 0x7f0b004f

    .line 539
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 542
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 543
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 546
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "0"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iput-wide v8, v3, Lorg/medhelp/mydiet/model/FoodServing;->amount:D

    .line 547
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 548
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 551
    const v8, 0x7f0b0050

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5           #spinner:Landroid/widget/Spinner;
    check-cast v5, Landroid/widget/Spinner;

    .line 552
    .restart local v5       #spinner:Landroid/widget/Spinner;
    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 553
    iput-object v6, v3, Lorg/medhelp/mydiet/model/FoodServing;->name:Ljava/lang/String;

    .line 555
    const v8, 0x7f0b0051

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 556
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 557
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_6

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 558
    const-string v8, "Invalid Serving equivalent"

    .line 559
    const-string v9, "Please enter a valid serving equivalent or remove it"

    .line 560
    const v10, 0x7f0b0051

    .line 558
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 561
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 562
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 565
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "0"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iput-wide v8, v3, Lorg/medhelp/mydiet/model/FoodServing;->equivalent:D

    .line 566
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 567
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 570
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 571
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    iput-object v4, v8, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    .line 573
    const v8, 0x7f0b0052

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5           #spinner:Landroid/widget/Spinner;
    check-cast v5, Landroid/widget/Spinner;

    .line 574
    .restart local v5       #spinner:Landroid/widget/Spinner;
    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 575
    const-string v8, "ml"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 576
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    const-string v9, "volume"

    iput-object v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->equivalentUnitType:Ljava/lang/String;

    .line 581
    :goto_1
    const v8, 0x7f0b0053

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 582
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 583
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_8

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 584
    const-string v8, "Invalid Calories"

    .line 585
    const-string v9, "Please enter a valid amount for calories or remove it"

    .line 586
    const v10, 0x7f0b0053

    .line 584
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 587
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 588
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 578
    :cond_7
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    const-string v9, "weight"

    iput-object v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->equivalentUnitType:Ljava/lang/String;

    goto :goto_1

    .line 591
    :cond_8
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    .line 592
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 593
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 596
    const v8, 0x7f0b0054

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 597
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 598
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_9

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_9

    .line 599
    const-string v8, "Invalid Calories from fat"

    .line 600
    const-string v9, "Please enter a valid amount for calories from fat or remove it"

    .line 601
    const v10, 0x7f0b0054

    .line 599
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 602
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 603
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 606
    :cond_9
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->caloriesFromFat:D

    .line 607
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 608
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 611
    const v8, 0x7f0b0055

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 612
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 613
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_a

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 614
    const-string v8, "Invalid Total Fat"

    .line 615
    const-string v9, "Please enter a valid amount for total fat or remove it"

    .line 616
    const v10, 0x7f0b0055

    .line 614
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 617
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 618
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 621
    :cond_a
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->totalFat:D

    .line 622
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 623
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 626
    const v8, 0x7f0b0056

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 627
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 628
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_b

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 629
    const-string v8, "Invalid Saturated Fat"

    .line 630
    const-string v9, "Please enter a valid amount for Saturated Fat or remove it"

    .line 631
    const v10, 0x7f0b0056

    .line 629
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 632
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 633
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 636
    :cond_b
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->saturatedFat:D

    .line 637
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 638
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 641
    const v8, 0x7f0b0057

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 642
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 643
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_c

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_c

    .line 644
    const-string v8, "Invalid Trans Fat"

    .line 645
    const-string v9, "Please enter a valid amount for Trans Fat or remove it"

    .line 646
    const v10, 0x7f0b0057

    .line 644
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 647
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 648
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 651
    :cond_c
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->transFat:D

    .line 652
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 653
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 656
    const v8, 0x7f0b0058

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 657
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 658
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_d

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_d

    .line 659
    const-string v8, "Invalid Monounsaturated fat"

    .line 660
    const-string v9, "Please enter a valid amount for monounsaturated fat or remove it"

    .line 661
    const v10, 0x7f0b0058

    .line 659
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 662
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 663
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 666
    :cond_d
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->monoUnsaturatedFat:D

    .line 667
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 668
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 671
    const v8, 0x7f0b0059

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 672
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 673
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_e

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_e

    .line 674
    const-string v8, "Invalid Cholesterol"

    .line 675
    const-string v9, "Please enter a valid amount for cholesterol or remove it"

    .line 676
    const v10, 0x7f0b0059

    .line 674
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 677
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 678
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 681
    :cond_e
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->cholesterol:D

    .line 682
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 683
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 686
    const v8, 0x7f0b005a

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 687
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 688
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_f

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_f

    .line 689
    const-string v8, "Invalid Sodium"

    .line 690
    const-string v9, "Please enter a valid amount for sodium or remove it"

    .line 691
    const v10, 0x7f0b005a

    .line 689
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 692
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 693
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 696
    :cond_f
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->sodium:D

    .line 697
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 698
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 701
    const v8, 0x7f0b005b

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 702
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 703
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_10

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_10

    .line 704
    const-string v8, "Invalid Potassium"

    .line 705
    const-string v9, "Please enter a valid amount for Potassium or remove it"

    .line 706
    const v10, 0x7f0b005b

    .line 704
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 707
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 708
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 711
    :cond_10
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->potassium:D

    .line 712
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 713
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 716
    const v8, 0x7f0b005c

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 717
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 718
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_11

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_11

    .line 719
    const-string v8, "Invalid Total Carbohydrates"

    .line 720
    const-string v9, "Please enter a valid amount for total carbohydrates or remove it"

    .line 721
    const v10, 0x7f0b005c

    .line 719
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 722
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 723
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 726
    :cond_11
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->totalCarbohydrate:D

    .line 727
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 728
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 731
    const v8, 0x7f0b005d

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 732
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 733
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_12

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_12

    .line 734
    const-string v8, "Invalid Dietary Fiber"

    .line 735
    const-string v9, "Please enter a valid amount for dietary fiber or remove it"

    .line 736
    const v10, 0x7f0b005d

    .line 734
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 737
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 738
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 741
    :cond_12
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->dietaryFiber:D

    .line 742
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 743
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 746
    const v8, 0x7f0b005e

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 747
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 748
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_13

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_13

    .line 749
    const-string v8, "Invalid Sugars"

    .line 750
    const-string v9, "Please enter a valid amount for sugars or remove it"

    .line 751
    const v10, 0x7f0b005e

    .line 749
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 752
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 753
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 756
    :cond_13
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->sugars:D

    .line 757
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 758
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 761
    const v8, 0x7f0b005f

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 762
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 763
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_14

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_14

    .line 764
    const-string v8, "Invalid Protein"

    .line 765
    const-string v9, "Please enter a valid amount for protein or remove it"

    .line 766
    const v10, 0x7f0b005f

    .line 764
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 767
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 768
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 771
    :cond_14
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->protein:D

    .line 772
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 773
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 776
    const v8, 0x7f0b0060

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 777
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 778
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_15

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_15

    .line 779
    const-string v8, "Invalid Vitamin A"

    .line 780
    const-string v9, "Please enter a valid amount for vitamin A or remove it"

    .line 781
    const v10, 0x7f0b0060

    .line 779
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 782
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 783
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 786
    :cond_15
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminA:D

    .line 787
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 788
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 791
    const v8, 0x7f0b0061

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 792
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 793
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_16

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_16

    .line 794
    const-string v8, "Invalid Vitamin C"

    .line 795
    const-string v9, "Please enter a valid amount for vitamin C or remove it"

    .line 796
    const v10, 0x7f0b0061

    .line 794
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 797
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 798
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 801
    :cond_16
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminC:D

    .line 802
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 803
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 806
    const v8, 0x7f0b0062

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 807
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 808
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_17

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_17

    .line 809
    const-string v8, "Invalid Vitamin D"

    .line 810
    const-string v9, "Please enter a valid amount for vitamin D or remove it"

    .line 811
    const v10, 0x7f0b0062

    .line 809
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 812
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 813
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 816
    :cond_17
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminD:D

    .line 817
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 818
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 821
    const v8, 0x7f0b0063

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 822
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 823
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_18

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_18

    .line 824
    const-string v8, "Invalid Vitamin E"

    .line 825
    const-string v9, "Please enter a valid amount for vitamin E or remove it"

    .line 826
    const v10, 0x7f0b0063

    .line 824
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 827
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 828
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 831
    :cond_18
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminE:D

    .line 832
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 833
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 836
    const v8, 0x7f0b0064

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 837
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 838
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_19

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_19

    .line 839
    const-string v8, "Invalid Vitamin K"

    .line 840
    const-string v9, "Please enter a valid amount for vitamin K or remove it"

    .line 841
    const v10, 0x7f0b0064

    .line 839
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 842
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 843
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 846
    :cond_19
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 847
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 850
    const v8, 0x7f0b0065

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 851
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 852
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_1a

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1a

    .line 853
    const-string v8, "Invalid Thiamin"

    .line 854
    const-string v9, "Please enter a valid amount for thiamin or remove it"

    .line 855
    const v10, 0x7f0b0065

    .line 853
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 856
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 857
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 860
    :cond_1a
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->thiamin:D

    .line 861
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 862
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 865
    const v8, 0x7f0b0066

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 866
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 867
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_1b

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1b

    .line 868
    const-string v8, "Invalid Riboflavin"

    .line 869
    const-string v9, "Please enter a valid amount for Riboflavin or remove it"

    .line 870
    const v10, 0x7f0b0066

    .line 868
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 871
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 872
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 875
    :cond_1b
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->riboflavin:D

    .line 876
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 877
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 880
    const v8, 0x7f0b0067

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 881
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 882
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_1c

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1c

    .line 883
    const-string v8, "Invalid Niacin"

    .line 884
    const-string v9, "Please enter a valid amount for niacin or remove it"

    .line 885
    const v10, 0x7f0b0067

    .line 883
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 886
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 887
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 890
    :cond_1c
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->niacin:D

    .line 891
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 892
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 895
    const v8, 0x7f0b0068

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 896
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 897
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_1d

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1d

    .line 898
    const-string v8, "Invalid Vitamin B6"

    .line 899
    const-string v9, "Please enter a valid amount for vitamin B6 or remove it"

    .line 900
    const v10, 0x7f0b0068

    .line 898
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 901
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 902
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 905
    :cond_1d
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB6:D

    .line 906
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 907
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 910
    const v8, 0x7f0b0069

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 911
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 912
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_1e

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1e

    .line 913
    const-string v8, "Invalid Folate"

    .line 914
    const-string v9, "Please enter a valid amount for folate or remove it"

    .line 915
    const v10, 0x7f0b0069

    .line 913
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 916
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 917
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 920
    :cond_1e
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->folate:D

    .line 921
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 922
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 925
    const v8, 0x7f0b006a

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 926
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 927
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_1f

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1f

    .line 928
    const-string v8, "Invalid Vitamin B12"

    .line 929
    const-string v9, "Please enter a valid amount for vitamin B12 or remove it"

    .line 930
    const v10, 0x7f0b006a

    .line 928
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 931
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 932
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 935
    :cond_1f
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->vitaminB12:D

    .line 936
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 937
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 940
    const v8, 0x7f0b006b

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 941
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 942
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_20

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_20

    .line 943
    const-string v8, "Invalid Biotin"

    .line 944
    const-string v9, "Please enter a valid amount for biotin or remove it"

    .line 945
    const v10, 0x7f0b006b

    .line 943
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 946
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 947
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 950
    :cond_20
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->biotin:D

    .line 951
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 952
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 955
    const v8, 0x7f0b006c

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 956
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 957
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_21

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_21

    .line 958
    const-string v8, "Invalid Pantothenic Acid"

    .line 959
    const-string v9, "Please enter a valid amount for Pantothenic Acid or remove it"

    .line 960
    const v10, 0x7f0b006c

    .line 958
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 961
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 962
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 965
    :cond_21
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->pantothenicAcid:D

    .line 966
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 967
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 970
    const v8, 0x7f0b006d

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 971
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 972
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_22

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_22

    .line 973
    const-string v8, "Invalid Calcium"

    .line 974
    const-string v9, "Please enter a valid amount for calcium or remove it"

    .line 975
    const v10, 0x7f0b006d

    .line 973
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 976
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 977
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 980
    :cond_22
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->calcium:D

    .line 981
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 982
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 985
    const v8, 0x7f0b006e

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 986
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 987
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_23

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_23

    .line 988
    const-string v8, "Invalid Iron"

    .line 989
    const-string v9, "Please enter a valid amount for iron or remove it"

    .line 990
    const v10, 0x7f0b006e

    .line 988
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 991
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 992
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 995
    :cond_23
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->iron:D

    .line 996
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 997
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1000
    const v8, 0x7f0b006f

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1001
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1002
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_24

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_24

    .line 1003
    const-string v8, "Invalid Iodine"

    .line 1004
    const-string v9, "Please enter a valid amount for iodine or remove it"

    .line 1005
    const v10, 0x7f0b006f

    .line 1003
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1006
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1007
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1010
    :cond_24
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->iodine:D

    .line 1011
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1012
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1015
    const v8, 0x7f0b0070

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1016
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1017
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_25

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_25

    .line 1018
    const-string v8, "Invalid Magnesium"

    .line 1019
    const-string v9, "Please enter a valid amount for magnesium or remove it"

    .line 1020
    const v10, 0x7f0b0070

    .line 1018
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1021
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1022
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1025
    :cond_25
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->magnesium:D

    .line 1026
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1027
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1030
    const v8, 0x7f0b0071

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1031
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1032
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_26

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_26

    .line 1033
    const-string v8, "Invalid Zinc"

    .line 1034
    const-string v9, "Please enter a valid amount for zinc or remove it"

    .line 1035
    const v10, 0x7f0b0071

    .line 1033
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1036
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1037
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1040
    :cond_26
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->zinc:D

    .line 1041
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1042
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1045
    const v8, 0x7f0b0072

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1046
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1047
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_27

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_27

    .line 1048
    const-string v8, "Invalid Selenium"

    .line 1049
    const-string v9, "Please enter a valid amount for selenium or remove it"

    .line 1050
    const v10, 0x7f0b0072

    .line 1048
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1051
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1052
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1055
    :cond_27
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->selenium:D

    .line 1056
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1057
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1060
    const v8, 0x7f0b0073

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1061
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1062
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_28

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_28

    .line 1063
    const-string v8, "Invalid Copper"

    .line 1064
    const-string v9, "Please enter a valid amount for copper or remove it"

    .line 1065
    const v10, 0x7f0b0073

    .line 1063
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1066
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1067
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1070
    :cond_28
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->copper:D

    .line 1071
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1072
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1075
    const v8, 0x7f0b0074

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1076
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1077
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_29

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_29

    .line 1078
    const-string v8, "Invalid Manganese"

    .line 1079
    const-string v9, "Please enter a valid amount for manganese or remove it"

    .line 1080
    const v10, 0x7f0b0074

    .line 1078
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1081
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1082
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1085
    :cond_29
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->manganese:D

    .line 1086
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1087
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1090
    const v8, 0x7f0b0075

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1091
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1092
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2a

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2a

    .line 1093
    const-string v8, "Invalid Chromium"

    .line 1094
    const-string v9, "Please enter a valid amount for chromium or remove it"

    .line 1095
    const v10, 0x7f0b0075

    .line 1093
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1096
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1097
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1100
    :cond_2a
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->chromium:D

    .line 1101
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1102
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1105
    const v8, 0x7f0b0076

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1106
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1107
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2b

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2b

    .line 1108
    const-string v8, "Invalid Molybdenum"

    .line 1109
    const-string v9, "Please enter a valid amount for molybdenum or remove it"

    .line 1110
    const v10, 0x7f0b0076

    .line 1108
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1111
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1112
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1115
    :cond_2b
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->molybdenum:D

    .line 1116
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1117
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1120
    const v8, 0x7f0b0077

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1121
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1122
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2c

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2c

    .line 1123
    const-string v8, "Invalid Fluoride"

    .line 1124
    const-string v9, "Please enter a valid amount for fluoride or remove it"

    .line 1125
    const v10, 0x7f0b0077

    .line 1123
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1126
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1127
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1130
    :cond_2c
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->fluoride:D

    .line 1131
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1132
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1135
    const v8, 0x7f0b0078

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1136
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1137
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2d

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2d

    .line 1138
    const-string v8, "Invalid Phosphorus"

    .line 1139
    const-string v9, "Please enter a valid amount for phosphorus or remove it"

    .line 1140
    const v10, 0x7f0b0078

    .line 1138
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1141
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1142
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1145
    :cond_2d
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    iput-wide v9, v8, Lorg/medhelp/mydiet/model/FoodDetail;->phosphorus:D

    .line 1146
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1147
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1150
    const v8, 0x7f0b0079

    invoke-virtual {p0, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1151
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1152
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2e

    invoke-static {v6}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2e

    .line 1153
    const-string v8, "Invalid Chloride"

    .line 1154
    const-string v9, "Please enter a valid amount for chloride or remove it"

    .line 1155
    const v10, 0x7f0b0079

    .line 1153
    invoke-direct {p0, v8, v9, v10}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showValidationErrorDialog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1156
    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1157
    invoke-virtual {v0, v13}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1160
    :cond_2e
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "0"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iput-wide v8, v7, Lorg/medhelp/mydiet/model/FoodDetail;->chloride:D

    .line 1161
    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1162
    invoke-virtual {v0, v14}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 1165
    const/4 v7, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 114
    :goto_0
    monitor-exit p0

    return-void

    .line 108
    :sswitch_0
    :try_start_1
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 111
    :sswitch_1
    :try_start_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->onServingsInfoClick()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 106
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0035 -> :sswitch_0
        0x7f0b004e -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 56
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    const v0, 0x7f030013

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->setContentView(I)V

    .line 59
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->initializeFoodGroups()V

    .line 60
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->initializeServingNames()V

    .line 62
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->setEditTexts()V

    .line 64
    const v0, 0x7f0b0035

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    const v0, 0x7f0b004e

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 94
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mServingsInfoDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mServingsInfoDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingsInfoDialog;->dismiss()V

    .line 97
    :cond_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    if-eqz v0, :cond_2

    .line 98
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->dismiss()V

    .line 100
    :cond_2
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 101
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .parameter "v"
    .parameter "hasFocus"

    .prologue
    .line 362
    const/4 v0, 0x0

    .line 363
    .local v0, et:Landroid/widget/EditText;
    const/4 v1, 0x0

    .line 365
    .local v1, text:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 491
    :goto_0
    :pswitch_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 492
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    invoke-static {v1}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 493
    const/high16 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 494
    const v2, 0x7f020020

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 499
    :goto_1
    return-void

    .line 367
    :pswitch_1
    const v2, 0x7f0b004f

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 368
    .restart local v0       #et:Landroid/widget/EditText;
    goto :goto_0

    .line 370
    :pswitch_2
    const v2, 0x7f0b0051

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 371
    .restart local v0       #et:Landroid/widget/EditText;
    goto :goto_0

    .line 373
    :pswitch_3
    const v2, 0x7f0b0053

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 374
    .restart local v0       #et:Landroid/widget/EditText;
    goto :goto_0

    .line 376
    :pswitch_4
    const v2, 0x7f0b0054

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 377
    .restart local v0       #et:Landroid/widget/EditText;
    goto :goto_0

    .line 379
    :pswitch_5
    const v2, 0x7f0b0055

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 380
    .restart local v0       #et:Landroid/widget/EditText;
    goto :goto_0

    .line 382
    :pswitch_6
    const v2, 0x7f0b0056

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 383
    .restart local v0       #et:Landroid/widget/EditText;
    goto :goto_0

    .line 385
    :pswitch_7
    const v2, 0x7f0b0057

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 386
    .restart local v0       #et:Landroid/widget/EditText;
    goto :goto_0

    .line 388
    :pswitch_8
    const v2, 0x7f0b0058

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 389
    .restart local v0       #et:Landroid/widget/EditText;
    goto :goto_0

    .line 391
    :pswitch_9
    const v2, 0x7f0b0059

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 392
    .restart local v0       #et:Landroid/widget/EditText;
    goto :goto_0

    .line 394
    :pswitch_a
    const v2, 0x7f0b005a

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 395
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 397
    :pswitch_b
    const v2, 0x7f0b005b

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 398
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 400
    :pswitch_c
    const v2, 0x7f0b005c

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 401
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 403
    :pswitch_d
    const v2, 0x7f0b005d

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 404
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 406
    :pswitch_e
    const v2, 0x7f0b005e

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 407
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 409
    :pswitch_f
    const v2, 0x7f0b005f

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 410
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 412
    :pswitch_10
    const v2, 0x7f0b0060

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 413
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 415
    :pswitch_11
    const v2, 0x7f0b0061

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 416
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 418
    :pswitch_12
    const v2, 0x7f0b0062

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 419
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 421
    :pswitch_13
    const v2, 0x7f0b0063

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 422
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 424
    :pswitch_14
    const v2, 0x7f0b0064

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 425
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 427
    :pswitch_15
    const v2, 0x7f0b0065

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 428
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 430
    :pswitch_16
    const v2, 0x7f0b0066

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 431
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 433
    :pswitch_17
    const v2, 0x7f0b0067

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 434
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 436
    :pswitch_18
    const v2, 0x7f0b0068

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 437
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 439
    :pswitch_19
    const v2, 0x7f0b0069

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 440
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 442
    :pswitch_1a
    const v2, 0x7f0b006a

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 443
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 445
    :pswitch_1b
    const v2, 0x7f0b006b

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 446
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 448
    :pswitch_1c
    const v2, 0x7f0b006c

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 449
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 451
    :pswitch_1d
    const v2, 0x7f0b006d

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 452
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 454
    :pswitch_1e
    const v2, 0x7f0b006e

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 455
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 457
    :pswitch_1f
    const v2, 0x7f0b006f

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 458
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 460
    :pswitch_20
    const v2, 0x7f0b0070

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 461
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 463
    :pswitch_21
    const v2, 0x7f0b0071

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 464
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 466
    :pswitch_22
    const v2, 0x7f0b0072

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 467
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 469
    :pswitch_23
    const v2, 0x7f0b0073

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 470
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 472
    :pswitch_24
    const v2, 0x7f0b0074

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 473
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 475
    :pswitch_25
    const v2, 0x7f0b0075

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 476
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 478
    :pswitch_26
    const v2, 0x7f0b0076

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 479
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 481
    :pswitch_27
    const v2, 0x7f0b0077

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 482
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 484
    :pswitch_28
    const v2, 0x7f0b0078

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 485
    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 487
    :pswitch_29
    const v2, 0x7f0b0079

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .restart local v0       #et:Landroid/widget/EditText;
    goto/16 :goto_0

    .line 496
    :cond_0
    const/high16 v2, -0x100

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 497
    const v2, 0x7f020077

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 365
    :pswitch_data_0
    .packed-switch 0x7f0b004f
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
    .end packed-switch
.end method
