.class Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "WebViewSimple.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/WebViewSimple;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/WebViewSimple;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/WebViewSimple;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;->this$0:Lorg/medhelp/mydiet/activity/WebViewSimple;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/WebViewSimple;Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;-><init>(Lorg/medhelp/mydiet/activity/WebViewSimple;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .parameter "view"
    .parameter "url"

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;->this$0:Lorg/medhelp/mydiet/activity/WebViewSimple;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/WebViewSimple;->progressLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 44
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .parameter "view"
    .parameter "url"
    .parameter "favicon"

    .prologue
    .line 48
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WebViewSimple$SimpleWebViewClient;->this$0:Lorg/medhelp/mydiet/activity/WebViewSimple;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/WebViewSimple;->progressLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 49
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 50
    return-void
.end method

.method public declared-synchronized shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .parameter "view"
    .parameter "url"

    .prologue
    .line 37
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method
