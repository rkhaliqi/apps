.class Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$6;
.super Ljava/lang/Object;
.source "FinalizeMealActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->refreshFoodItems()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$6;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    .line 488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    const/16 v4, 0x8

    .line 492
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 493
    .local v1, foodPosition:I
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit8 v0, v2, 0x2

    .line 494
    .local v0, childPosition:I
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$6;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mSelectFoodsLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$11(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 495
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$6;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mSelectFoodsLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$11(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 497
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$6;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v2

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    .line 499
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$6;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v2

    iget-object v2, v2, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 500
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$6;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->refreshFoodItems()V
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$12(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    .line 501
    return-void
.end method
