.class public Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;
.super Landroid/app/Dialog;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/account/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LoginDialog"
.end annotation


# instance fields
.field _context:Landroid/content/Context;

.field final synthetic this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;


# direct methods
.method public constructor <init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;Landroid/content/Context;)V
    .locals 1
    .parameter
    .parameter "context"

    .prologue
    .line 295
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    .line 296
    const v0, 0x7f09000c

    invoke-direct {p0, p2, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 297
    const v0, 0x7f03000c

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->setContentView(I)V

    .line 298
    iput-object p2, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->_context:Landroid/content/Context;

    .line 299
    return-void
.end method


# virtual methods
.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 307
    const v1, 0x7f0b001e

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 308
    .local v0, tv:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2
    .parameter "title"

    .prologue
    .line 302
    const v1, 0x7f0b000c

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 303
    .local v0, tv:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    return-void
.end method
