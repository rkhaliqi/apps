.class public Lorg/medhelp/mydiet/activity/account/SignupActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "SignupActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;,
        Lorg/medhelp/mydiet/activity/account/SignupActivity$ImageDownloader;,
        Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;
    }
.end annotation


# static fields
.field private static final CAPTCHA_URL:Ljava/lang/String;


# instance fields
.field mCaptchaLoadingLayout:Landroid/widget/LinearLayout;

.field mCaptchaSwitcher:Landroid/widget/ViewSwitcher;

.field mCaptchaView:Landroid/widget/ImageView;

.field mCapthaField:Landroid/widget/EditText;

.field mEMailField:Landroid/widget/EditText;

.field mNicknameField:Landroid/widget/EditText;

.field mPasswordConfirmField:Landroid/widget/EditText;

.field mPasswordField:Landroid/widget/EditText;

.field mProgress:Landroid/widget/ProgressBar;

.field mhHttpHandler:Lorg/medhelp/mydiet/http/MHHttpHandler;

.field signUpButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->SERVER_URL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/noisy_image/create?key=account_signup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->CAPTCHA_URL:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/account/SignupActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->completeSignup()V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/account/SignupActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->refreshCaptcha()V

    return-void
.end method

.method private completeSignup()V
    .locals 2

    .prologue
    .line 107
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 108
    .local v0, resultIntent:Landroid/content/Intent;
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->setResult(ILandroid/content/Intent;)V

    .line 110
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->finish()V

    .line 111
    return-void
.end method

.method private onSignupClick()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 91
    const v1, 0x7f0b0107

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 92
    .local v0, tv:Landroid/widget/TextView;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 94
    invoke-static {p0}, Lorg/medhelp/mydiet/util/NetworkUtil;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lorg/medhelp/mydiet/util/NetworkUtil;->isNetworkRoaming(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 95
    new-instance v1, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;-><init>(Lorg/medhelp/mydiet/activity/account/SignupActivity;Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    .line 96
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mNicknameField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    .line 97
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mEMailField:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 98
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mPasswordField:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 99
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mPasswordConfirmField:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 100
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mCapthaField:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 95
    invoke-virtual {v1, v2}, Lorg/medhelp/mydiet/activity/account/SignupActivity$SignUpTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    const-string v1, "Network Unavailable"

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private refreshCaptcha()V
    .locals 3

    .prologue
    .line 77
    new-instance v0, Lorg/medhelp/mydiet/activity/account/SignupActivity$ImageDownloader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/account/SignupActivity$ImageDownloader;-><init>(Lorg/medhelp/mydiet/activity/account/SignupActivity;Lorg/medhelp/mydiet/activity/account/SignupActivity$ImageDownloader;)V

    .line 78
    .local v0, downloader:Lorg/medhelp/mydiet/activity/account/SignupActivity$ImageDownloader;
    sget-object v1, Lorg/medhelp/mydiet/activity/account/SignupActivity;->CAPTCHA_URL:Ljava/lang/String;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mCaptchaView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v2}, Lorg/medhelp/mydiet/activity/account/SignupActivity$ImageDownloader;->downloadImage(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 79
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 83
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 88
    :goto_0
    return-void

    .line 85
    :pswitch_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->onSignupClick()V

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x7f0b0110
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 52
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v0, 0x7f03002b

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->setContentView(I)V

    .line 55
    const v0, 0x7f0b0110

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->signUpButton:Landroid/widget/Button;

    .line 57
    const v0, 0x7f0b0108

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mNicknameField:Landroid/widget/EditText;

    .line 58
    const v0, 0x7f0b0109

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mEMailField:Landroid/widget/EditText;

    .line 59
    const v0, 0x7f0b00d7

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mPasswordField:Landroid/widget/EditText;

    .line 60
    const v0, 0x7f0b010a

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mPasswordConfirmField:Landroid/widget/EditText;

    .line 61
    const v0, 0x7f0b010e

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mCapthaField:Landroid/widget/EditText;

    .line 63
    const v0, 0x7f0b010f

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 64
    const v0, 0x7f0b010b

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mCaptchaSwitcher:Landroid/widget/ViewSwitcher;

    .line 65
    const v0, 0x7f0b010c

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mCaptchaLoadingLayout:Landroid/widget/LinearLayout;

    .line 66
    const v0, 0x7f0b010d

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mCaptchaView:Landroid/widget/ImageView;

    .line 68
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity;->signUpButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onStart()V

    .line 73
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/account/SignupActivity;->refreshCaptcha()V

    .line 74
    return-void
.end method
