.class public Lorg/medhelp/mydiet/activity/SetupScreen2;
.super Lorg/medhelp/mydiet/activity/BackClickAlertActivity;
.source "SetupScreen2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DATE_PICKER:I


# instance fields
.field private dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private mDOBDay:I

.field private mDOBMonth:I

.field private mDOBYear:I

.field private startedFromSettings:Z

.field tvDob:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;-><init>()V

    .line 133
    new-instance v0, Lorg/medhelp/mydiet/activity/SetupScreen2$1;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/activity/SetupScreen2$1;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen2;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 24
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/SetupScreen2;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 141
    invoke-direct {p0, p1, p2, p3}, Lorg/medhelp/mydiet/activity/SetupScreen2;->setDateOfBirth(III)V

    return-void
.end method

.method private onClickNext()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    iget v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBYear:I

    if-ltz v0, :cond_1

    iget v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBMonth:I

    if-ltz v0, :cond_1

    iget v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBDay:I

    if-ltz v0, :cond_1

    .line 93
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->startedFromSettings:Z

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->finish()V

    .line 103
    :goto_0
    return-void

    .line 96
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->startActivity(Landroid/content/Intent;)V

    .line 97
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->finish()V

    goto :goto_0

    .line 100
    :cond_1
    const-string v0, "Please set your date of birth"

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 101
    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/SetupScreen2;->showDialog(I)V

    goto :goto_0
.end method

.method private refreshViewContents()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->tvDob:Landroid/widget/TextView;

    const-string v1, "MMMM dd, yyyy"

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDateOfBirthString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void
.end method

.method private setDateOfBirth(III)V
    .locals 1
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    .prologue
    .line 142
    iput p3, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBDay:I

    .line 143
    iput p2, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBMonth:I

    .line 144
    iput p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBYear:I

    .line 145
    invoke-static {p0, p1, p2, p3}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setDateOfBirth(Landroid/content/Context;III)V

    .line 146
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->refreshViewContents()V

    .line 148
    const v0, 0x7f0b00e9

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 149
    return-void
.end method


# virtual methods
.method public init()V
    .locals 1

    .prologue
    .line 64
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDateOfBirthYear(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBYear:I

    .line 65
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDateOfBirthMonth(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBMonth:I

    .line 66
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDateOfBirthDay(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBDay:I

    .line 67
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 81
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 89
    :goto_0
    return-void

    .line 83
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->showDialog(I)V

    goto :goto_0

    .line 86
    :pswitch_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->onClickNext()V

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b00e9
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x0

    .line 40
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v3, 0x7f030027

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen2;->setContentView(I)V

    .line 43
    const v3, 0x7f0b00ea

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->tvDob:Landroid/widget/TextView;

    .line 45
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->tvDob:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    const v3, 0x7f0b00e9

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 47
    .local v0, btn:Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->init()V

    .line 49
    invoke-virtual {p0, v5}, Lorg/medhelp/mydiet/activity/SetupScreen2;->showDialog(I)V

    .line 51
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 52
    .local v2, extras:Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 53
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "edit_preferences"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, editPreferences:Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "edit_preferences"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->startedFromSettings:Z

    .line 56
    invoke-virtual {p0, v5}, Lorg/medhelp/mydiet/activity/SetupScreen2;->setShowBackAlert(Z)V

    .line 57
    const-string v3, "Save"

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 61
    .end local v1           #editPreferences:Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "id"

    .prologue
    .line 107
    packed-switch p1, :pswitch_data_0

    .line 117
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 109
    :pswitch_0
    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBYear:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBMonth:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBDay:I

    if-ltz v1, :cond_0

    .line 110
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    iget v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBYear:I

    iget v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBMonth:I

    iget v5, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBDay:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 111
    .local v0, datePickerDialog:Landroid/app/DatePickerDialog;
    goto :goto_0

    .line 113
    .end local v0           #datePickerDialog:Landroid/app/DatePickerDialog;
    :cond_0
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    const/16 v3, 0x7b9

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 114
    .restart local v0       #datePickerDialog:Landroid/app/DatePickerDialog;
    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 3
    .parameter "id"
    .parameter "dialog"

    .prologue
    .line 122
    packed-switch p1, :pswitch_data_0

    .line 131
    .end local p2
    :goto_0
    return-void

    .line 124
    .restart local p2
    :pswitch_0
    iget v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBYear:I

    if-ltz v0, :cond_0

    iget v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBMonth:I

    if-ltz v0, :cond_0

    iget v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBDay:I

    if-ltz v0, :cond_0

    .line 125
    check-cast p2, Landroid/app/DatePickerDialog;

    .end local p2
    iget v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBYear:I

    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBMonth:I

    iget v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen2;->mDOBDay:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/app/DatePickerDialog;->updateDate(III)V

    goto :goto_0

    .line 127
    .restart local p2
    :cond_0
    check-cast p2, Landroid/app/DatePickerDialog;

    .end local p2
    const/16 v0, 0x7b9

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p2, v0, v1, v2}, Landroid/app/DatePickerDialog;->updateDate(III)V

    goto :goto_0

    .line 122
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->onStart()V

    .line 71
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->init()V

    .line 72
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen2;->refreshViewContents()V

    .line 73
    return-void
.end method
