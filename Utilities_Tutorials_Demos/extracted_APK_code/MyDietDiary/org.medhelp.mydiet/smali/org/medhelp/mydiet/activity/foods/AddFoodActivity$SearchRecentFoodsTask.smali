.class Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;
.super Landroid/os/AsyncTask;
.source "AddFoodActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchRecentFoodsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final PROGRESS_REFRESH_RESULTS:I = 0x1

.field private static final PROGRESS_START_SEARCH:I


# instance fields
.field private _foodItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 786
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 786
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    return-void
.end method

.method private searchAndDisplayRecentFoods(Ljava/lang/String;)V
    .locals 2
    .parameter "like"

    .prologue
    .line 868
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 869
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    invoke-virtual {v0, p1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getRecentFoodItems(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->_foodItems:Ljava/util/ArrayList;

    .line 870
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 1
    .parameter "params"

    .prologue
    .line 800
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 801
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->searchAndDisplayRecentFoods(Ljava/lang/String;)V

    .line 803
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 864
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 865
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    const/4 v2, 0x1

    .line 858
    new-array v0, v2, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->publishProgress([Ljava/lang/Object;)V

    .line 859
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 860
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 794
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 795
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->publishProgress([Ljava/lang/Object;)V

    .line 796
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 7
    .parameter "values"

    .prologue
    const v6, 0x7f0b0042

    const/4 v5, 0x0

    .line 808
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 809
    aget-object v3, p1, v5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 811
    :pswitch_0
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->showLoadingDialog()V
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$15(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    goto :goto_0

    .line 814
    :pswitch_1
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->hideLoadingDialog()V
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$17(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    .line 816
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 817
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->_foodItems:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->_foodItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 818
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-virtual {v3, v6}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 820
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v3

    if-nez v3, :cond_1

    .line 821
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v3, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$20(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Ljava/util/ArrayList;)V

    .line 822
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 825
    :cond_1
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 826
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 828
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v3

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->_foodItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->markSelectedFoodItems(Ljava/util/ArrayList;)V

    .line 830
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->_foodItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 831
    .local v0, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 832
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    goto :goto_1

    .line 836
    .end local v0           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :cond_2
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 837
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->notifyDataSetChanged()V

    .line 839
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const v4, 0x7f0b0040

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 841
    .local v1, text:Ljava/lang/String;
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-virtual {v3, v6}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 842
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 844
    .local v2, tv:Landroid/widget/TextView;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 845
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "There are no foods that matched "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Create a new food to add it."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 848
    :cond_3
    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 809
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchRecentFoodsTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
