.class public Lorg/medhelp/mydiet/activity/SetupScreen4;
.super Lorg/medhelp/mydiet/activity/BackClickAlertActivity;
.source "SetupScreen4.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/SetupScreen4$LoadWeightTask;
    }
.end annotation


# static fields
.field private static final DATE_PICKER:I


# instance fields
.field private dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

.field llGoal:Landroid/widget/LinearLayout;

.field mDialog:Landroid/app/AlertDialog;

.field mProgress:Landroid/widget/ProgressBar;

.field private mTargetDateDay:I

.field private mTargetDateMonth:I

.field private mTargetDateYear:I

.field private mWeight:D

.field rlActivityLevel:Landroid/widget/RelativeLayout;

.field rlDesiredWeight:Landroid/widget/RelativeLayout;

.field rlGoals:Landroid/widget/RelativeLayout;

.field rlTargetDate:Landroid/widget/RelativeLayout;

.field private startedFromSettings:Z

.field tvActivityLevel:Landroid/widget/TextView;

.field tvDesiredWeight:Landroid/widget/TextView;

.field tvGoals:Landroid/widget/TextView;

.field tvTargetDate:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;-><init>()V

    .line 441
    new-instance v0, Lorg/medhelp/mydiet/activity/SetupScreen4$1;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/activity/SetupScreen4$1;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen4;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 44
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/SetupScreen4;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 449
    invoke-direct {p0, p1, p2, p3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->setTargetDate(III)V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/SetupScreen4;D)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62
    iput-wide p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mWeight:D

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/SetupScreen4;)V
    .locals 0
    .parameter

    .prologue
    .line 544
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->showProgressBar()V

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/SetupScreen4;)V
    .locals 0
    .parameter

    .prologue
    .line 548
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->hideProgressBar()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/SetupScreen4;)V
    .locals 0
    .parameter

    .prologue
    .line 457
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->refreshGoalMessage()V

    return-void
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/SetupScreen4;)V
    .locals 0
    .parameter

    .prologue
    .line 122
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->refreshViewContents()V

    return-void
.end method

.method private clickActivityLevel()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f060005

    .line 265
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 266
    .local v1, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 268
    const/4 v2, -0x1

    .line 269
    .local v2, checkedItem:I
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, activityLevels:[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    array-length v4, v0

    if-lt v3, v4, :cond_1

    .line 277
    :goto_1
    if-gez v2, :cond_0

    .line 278
    const/4 v2, 0x0

    .line 281
    :cond_0
    new-instance v4, Lorg/medhelp/mydiet/activity/SetupScreen4$2;

    invoke-direct {v4, p0, v0}, Lorg/medhelp/mydiet/activity/SetupScreen4$2;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen4;[Ljava/lang/String;)V

    invoke-virtual {v1, v6, v2, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 291
    const-string v4, "Select an Activity Level"

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 292
    const-string v4, "Cancel"

    invoke-virtual {v1, v4, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 293
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    .line 294
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 295
    return-void

    .line 271
    :cond_1
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getActivityLevel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aget-object v5, v0, v3

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 272
    move v2, v3

    .line 273
    goto :goto_1

    .line 270
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private clickDesiredWeight()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 336
    move-object v1, p0

    .line 337
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 338
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v9, "layout_inflater"

    invoke-virtual {v1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 340
    .local v2, inflater:Landroid/view/LayoutInflater;
    const v10, 0x7f030035

    const v9, 0x7f0b0133

    invoke-virtual {p0, v9}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    invoke-virtual {v2, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 341
    .local v3, layout:Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 342
    const-string v9, "Enter Desired Weight"

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 344
    const v9, 0x7f0b0134

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    .line 345
    .local v6, weightField:Landroid/widget/EditText;
    const v9, 0x7f0b001a

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 347
    .local v4, tvUnits:Landroid/widget/TextView;
    new-instance v9, Lorg/medhelp/mydiet/activity/SetupScreen4$4;

    invoke-direct {v9, p0}, Lorg/medhelp/mydiet/activity/SetupScreen4$4;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen4;)V

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 356
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 358
    .local v5, units:Ljava/lang/String;
    const-string v9, "lb"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 361
    const-string v9, "lb"

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDesiredWeightInLb(Landroid/content/Context;)F

    move-result v8

    .line 363
    .local v8, weightInLb:F
    cmpl-float v9, v8, v11

    if-lez v9, :cond_0

    .line 364
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 374
    .end local v8           #weightInLb:F
    :cond_0
    :goto_0
    const-string v9, "OK"

    new-instance v10, Lorg/medhelp/mydiet/activity/SetupScreen4$5;

    invoke-direct {v10, p0, v6, v5}, Lorg/medhelp/mydiet/activity/SetupScreen4$5;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen4;Landroid/widget/EditText;Ljava/lang/String;)V

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 402
    const-string v9, "Cancel"

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 403
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    iput-object v9, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    .line 404
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    .line 405
    return-void

    .line 368
    :cond_1
    const-string v9, "kg"

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDesiredWeightInKG(Landroid/content/Context;)F

    move-result v7

    .line 370
    .local v7, weightInKG:F
    cmpl-float v9, v7, v11

    if-lez v9, :cond_0

    .line 371
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private clickGoals()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f060006

    .line 298
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 299
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 301
    const/4 v1, -0x1

    .line 303
    .local v1, checkedItem:I
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 305
    .local v2, goals:[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    array-length v4, v2

    if-lt v3, v4, :cond_1

    .line 312
    :goto_1
    if-gez v1, :cond_0

    .line 313
    const/4 v1, 0x0

    .line 317
    :cond_0
    new-instance v4, Lorg/medhelp/mydiet/activity/SetupScreen4$3;

    invoke-direct {v4, p0}, Lorg/medhelp/mydiet/activity/SetupScreen4$3;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen4;)V

    invoke-virtual {v0, v6, v1, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 328
    const-string v4, "Select a Goal"

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 330
    const-string v4, "Cancel"

    invoke-virtual {v0, v4, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 331
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    .line 332
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 333
    return-void

    .line 306
    :cond_1
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGoal(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aget-object v5, v2, v3

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 307
    move v1, v3

    .line 308
    goto :goto_1

    .line 305
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private clickTargetDate()V
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->showDialog(I)V

    .line 409
    return-void
.end method

.method private hideProgressBar()V
    .locals 2

    .prologue
    .line 549
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 550
    return-void
.end method

.method private onActivityLevelInfoClick()V
    .locals 6

    .prologue
    .line 251
    move-object v1, p0

    .line 252
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 253
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v4, "layout_inflater"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 255
    .local v2, inflater:Landroid/view/LayoutInflater;
    const v5, 0x7f030002

    const v4, 0x7f0b0007

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v2, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 256
    .local v3, layout:Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 257
    const-string v4, "Activity Levels"

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 258
    const-string v4, "OK"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 260
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    .line 261
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 262
    return-void
.end method

.method private onFinishClick()V
    .locals 2

    .prologue
    .line 198
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->validateGoals()Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-boolean v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->startedFromSettings:Z

    if-eqz v1, :cond_1

    .line 202
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->finish()V

    goto :goto_0

    .line 204
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 205
    .local v0, home:Landroid/content/Intent;
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setSetupDone(Landroid/content/Context;Z)V

    .line 206
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->startActivity(Landroid/content/Intent;)V

    .line 207
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->finish()V

    goto :goto_0
.end method

.method private refreshGoalMessage()V
    .locals 27

    .prologue
    .line 458
    const v3, 0x7f0b0103

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 460
    .local v17, tv:Landroid/widget/TextView;
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDesiredWeightInLb(Landroid/content/Context;)F

    move-result v3

    const/4 v8, 0x0

    cmpg-float v3, v3, v8

    if-lez v3, :cond_0

    .line 461
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDate(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 462
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDate(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v3

    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getTodayMidnightCalendar()Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 463
    :cond_0
    const-string v3, ""

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 503
    :goto_0
    return-void

    .line 467
    :cond_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mWeight:D

    move-wide/from16 v23, v0

    const-wide v25, 0x3fdd07a84ab75e51L

    mul-double v4, v23, v25

    .line 468
    .local v4, weightInKg:D
    const-wide/16 v23, 0x0

    cmpg-double v3, v4, v23

    if-gtz v3, :cond_2

    .line 469
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v23, v0

    move-wide/from16 v0, v23

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/SetupScreen4;->mWeight:D

    .line 470
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mWeight:D

    move-wide/from16 v23, v0

    const-wide v25, 0x3fdd07a84ab75e51L

    mul-double v4, v23, v25

    .line 473
    :cond_2
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDesiredWeightInLb(Landroid/content/Context;)F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v23, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mWeight:D

    move-wide/from16 v25, v0

    sub-double v6, v23, v25

    .line 474
    .local v6, changeInWeight:D
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDate(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v3

    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getTodayMidnightCalendar()Ljava/util/Calendar;

    move-result-object v8

    invoke-static {v3, v8}, Lorg/medhelp/mydiet/util/DateUtil;->getDifferenceInDays(Ljava/util/Calendar;Ljava/util/Calendar;)J

    move-result-wide v23

    move-wide/from16 v0, v23

    long-to-double v11, v0

    .line 475
    .local v11, daysToTarget:D
    const-wide/high16 v23, 0x401c

    div-double v19, v11, v23

    .line 476
    .local v19, weeksToTarget:D
    const-wide/16 v23, 0x0

    cmpg-double v3, v19, v23

    if-gtz v3, :cond_3

    const-wide/high16 v19, 0x3ff0

    .line 477
    :cond_3
    div-double v21, v6, v19

    .line 479
    .local v21, weightChangePerWeek:D
    new-instance v16, Ljava/text/DecimalFormatSymbols;

    new-instance v3, Ljava/util/Locale;

    const-string v8, "en"

    const-string v23, "en"

    move-object/from16 v0, v23

    invoke-direct {v3, v8, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 480
    .local v16, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v3, 0x2e

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 481
    new-instance v14, Ljava/text/DecimalFormat;

    const-string v3, "#.#"

    move-object/from16 v0, v16

    invoke-direct {v14, v3, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 482
    .local v14, df1:Ljava/text/DecimalFormat;
    new-instance v13, Ljava/text/DecimalFormat;

    const-string v3, "#"

    move-object/from16 v0, v16

    invoke-direct {v13, v3, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 484
    .local v13, df0:Ljava/text/DecimalFormat;
    const-string v18, "lb"

    .line 485
    .local v18, units:Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "kg"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 486
    const-string v18, "kg"

    .line 487
    const-wide v23, 0x3fdd07a84ab75e51L

    mul-double v21, v21, v23

    .line 490
    :cond_4
    const-string v15, ""

    .line 491
    .local v15, displayText:Ljava/lang/String;
    const-wide/16 v23, 0x0

    cmpl-double v3, v21, v23

    if-lez v3, :cond_6

    .line 492
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "To reach this goal you should aim to gain "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v21

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " per week."

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 497
    :goto_1
    const-wide/16 v23, 0x0

    cmpg-double v3, v11, v23

    if-gtz v3, :cond_5

    const-wide/high16 v11, 0x3ff0

    .line 499
    :cond_5
    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getTodayMidnightCalendar()Ljava/util/Calendar;

    move-result-object v8

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v8}, Lorg/medhelp/mydiet/util/DataUtil;->getCalorieBudget(Landroid/content/Context;DDLjava/util/Calendar;)D

    move-result-wide v9

    .line 501
    .local v9, calorieBudget:D
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " Your calorie budget is "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v13, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " calories per day."

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 502
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 494
    .end local v9           #calorieBudget:D
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "To reach this goal you should aim to lose "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/high16 v23, -0x4010

    mul-double v23, v23, v21

    move-wide/from16 v0, v23

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " per week."

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_1
.end method

.method private refreshViewContents()V
    .locals 5

    .prologue
    .line 123
    new-instance v0, Lorg/medhelp/mydiet/activity/SetupScreen4$LoadWeightTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/SetupScreen4$LoadWeightTask;-><init>(Lorg/medhelp/mydiet/activity/SetupScreen4;Lorg/medhelp/mydiet/activity/SetupScreen4$LoadWeightTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/SetupScreen4$LoadWeightTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 125
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->tvActivityLevel:Landroid/widget/TextView;

    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getActivityLevel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->updateGoalsDisplay()V

    .line 127
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->updateDesiredWeightDisplay()V

    .line 128
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->tvTargetDate:Landroid/widget/TextView;

    const-string v1, "MMMM dd, yyyy"

    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDateString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    return-void
.end method

.method private setTargetDate(III)V
    .locals 0
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    .prologue
    .line 450
    iput p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateYear:I

    .line 451
    iput p2, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateMonth:I

    .line 452
    iput p3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateDay:I

    .line 453
    invoke-static {p0, p1, p2, p3}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setTargetDate(Landroid/content/Context;III)V

    .line 454
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->refreshViewContents()V

    .line 455
    return-void
.end method

.method private showInvalidGoalsDialog(Ljava/lang/String;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 241
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 242
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v1, "Please set your goals"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 243
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 244
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 245
    const-string v1, "OK"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 246
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    .line 247
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 248
    return-void
.end method

.method private showProgressBar()V
    .locals 2

    .prologue
    .line 545
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 546
    return-void
.end method

.method private updateDesiredWeightDisplay()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 152
    const/4 v2, 0x0

    .line 153
    .local v2, weightString:Ljava/lang/String;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "lb"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 154
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDesiredWeightInLb(Landroid/content/Context;)F

    move-result v1

    .line 155
    .local v1, weightInLb:F
    cmpl-float v3, v1, v5

    if-lez v3, :cond_1

    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " lb"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 168
    .end local v1           #weightInLb:F
    :goto_0
    if-eqz v2, :cond_0

    .line 169
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->tvDesiredWeight:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    :cond_0
    return-void

    .line 158
    .restart local v1       #weightInLb:F
    :cond_1
    const-string v2, ""

    goto :goto_0

    .line 161
    .end local v1           #weightInLb:F
    :cond_2
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDesiredWeightInKG(Landroid/content/Context;)F

    move-result v0

    .line 162
    .local v0, weightInKG:F
    cmpl-float v3, v0, v5

    if-lez v3, :cond_3

    .line 163
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " kg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 165
    :cond_3
    const-string v2, ""

    goto :goto_0
.end method

.method private updateGoalsDisplay()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 139
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGoal(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, goal:Ljava/lang/String;
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->tvGoals:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, goals:[Ljava/lang/String;
    aget-object v2, v1, v4

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->llGoal:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->llGoal:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private validateGoals()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v6, -0x1

    .line 212
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGoal(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 214
    .local v0, goal:Ljava/lang/String;
    const-string v5, "Maintain Weight"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 215
    invoke-static {p0, v6, v6, v6}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setTargetDate(Landroid/content/Context;III)V

    .line 237
    :cond_0
    :goto_0
    return v3

    .line 219
    :cond_1
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDate(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v1

    .line 220
    .local v1, targetDate:Ljava/util/Calendar;
    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getTodayMidnightCalendar()Ljava/util/Calendar;

    move-result-object v2

    .line 222
    .local v2, today:Ljava/util/Calendar;
    if-nez v1, :cond_2

    .line 223
    const-string v3, "Please pick a target date"

    invoke-direct {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->showInvalidGoalsDialog(Ljava/lang/String;)V

    move v3, v4

    .line 224
    goto :goto_0

    .line 227
    :cond_2
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 228
    const-string v3, "Your target date should be after today"

    invoke-direct {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->showInvalidGoalsDialog(Ljava/lang/String;)V

    move v3, v4

    .line 229
    goto :goto_0

    .line 232
    :cond_3
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDesiredWeightInLb(Landroid/content/Context;)F

    move-result v5

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_0

    .line 233
    const-string v3, "Please enter your desired weight."

    invoke-direct {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->showInvalidGoalsDialog(Ljava/lang/String;)V

    move v3, v4

    .line 234
    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 195
    :goto_0
    return-void

    .line 177
    :sswitch_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->clickActivityLevel()V

    goto :goto_0

    .line 180
    :sswitch_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->clickGoals()V

    goto :goto_0

    .line 183
    :sswitch_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->clickDesiredWeight()V

    goto :goto_0

    .line 186
    :sswitch_3
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->clickTargetDate()V

    goto :goto_0

    .line 189
    :sswitch_4
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->onFinishClick()V

    goto :goto_0

    .line 192
    :sswitch_5
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->onActivityLevelInfoClick()V

    goto :goto_0

    .line 175
    :sswitch_data_0
    .sparse-switch
        0x7f0b00f5 -> :sswitch_5
        0x7f0b00f6 -> :sswitch_0
        0x7f0b00f9 -> :sswitch_1
        0x7f0b00fd -> :sswitch_2
        0x7f0b0100 -> :sswitch_3
        0x7f0b0104 -> :sswitch_4
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 75
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const v3, 0x7f030029

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->setContentView(I)V

    .line 78
    const v3, 0x7f0b00f6

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->rlActivityLevel:Landroid/widget/RelativeLayout;

    .line 79
    const v3, 0x7f0b00f9

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->rlGoals:Landroid/widget/RelativeLayout;

    .line 80
    const v3, 0x7f0b00fd

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->rlDesiredWeight:Landroid/widget/RelativeLayout;

    .line 81
    const v3, 0x7f0b0100

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->rlTargetDate:Landroid/widget/RelativeLayout;

    .line 83
    const v3, 0x7f0b00fc

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->llGoal:Landroid/widget/LinearLayout;

    .line 85
    const v3, 0x7f0b00f8

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->tvActivityLevel:Landroid/widget/TextView;

    .line 86
    const v3, 0x7f0b00fb

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->tvGoals:Landroid/widget/TextView;

    .line 87
    const v3, 0x7f0b00ff

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->tvDesiredWeight:Landroid/widget/TextView;

    .line 88
    const v3, 0x7f0b0102

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->tvTargetDate:Landroid/widget/TextView;

    .line 89
    const v3, 0x7f0b0001

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mProgress:Landroid/widget/ProgressBar;

    .line 91
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->rlActivityLevel:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->rlGoals:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->rlDesiredWeight:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->rlTargetDate:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    const v3, 0x7f0b00f5

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const v3, 0x7f0b0104

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 99
    .local v0, btn:Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 104
    .local v2, extras:Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 105
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "edit_preferences"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 106
    .local v1, editPreferences:Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "edit_preferences"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->startedFromSettings:Z

    .line 108
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/SetupScreen4;->setShowBackAlert(Z)V

    .line 109
    const-string v3, "Save"

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 112
    .end local v1           #editPreferences:Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .parameter "id"

    .prologue
    .line 413
    packed-switch p1, :pswitch_data_0

    .line 424
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 415
    :pswitch_0
    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateYear:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateMonth:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateDay:I

    if-ltz v1, :cond_0

    .line 416
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    iget v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateYear:I

    iget v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateMonth:I

    iget v5, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateDay:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 417
    .local v0, datePickerDialog:Landroid/app/DatePickerDialog;
    goto :goto_0

    .line 419
    .end local v0           #datePickerDialog:Landroid/app/DatePickerDialog;
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 420
    .local v6, c:Ljava/util/Calendar;
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v1, 0x2

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v1, 0x5

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 421
    .restart local v0       #datePickerDialog:Landroid/app/DatePickerDialog;
    goto :goto_0

    .line 413
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 135
    :cond_0
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->onDestroy()V

    .line 136
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 4
    .parameter "id"
    .parameter "dialog"

    .prologue
    .line 429
    packed-switch p1, :pswitch_data_0

    .line 439
    .end local p2
    :goto_0
    return-void

    .line 431
    .restart local p2
    :pswitch_0
    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateYear:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateMonth:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateDay:I

    if-ltz v1, :cond_0

    .line 432
    check-cast p2, Landroid/app/DatePickerDialog;

    .end local p2
    iget v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateYear:I

    iget v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateMonth:I

    iget v3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateDay:I

    invoke-virtual {p2, v1, v2, v3}, Landroid/app/DatePickerDialog;->updateDate(III)V

    goto :goto_0

    .line 434
    .restart local p2
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 435
    .local v0, c:Ljava/util/Calendar;
    check-cast p2, Landroid/app/DatePickerDialog;

    .end local p2
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {p2, v1, v2, v3}, Landroid/app/DatePickerDialog;->updateDate(III)V

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BackClickAlertActivity;->onStart()V

    .line 116
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDateYear(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateYear:I

    .line 117
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDateMonth(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateMonth:I

    .line 118
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDateDay(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lorg/medhelp/mydiet/activity/SetupScreen4;->mTargetDateDay:I

    .line 119
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/SetupScreen4;->refreshViewContents()V

    .line 120
    return-void
.end method
