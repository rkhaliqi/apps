.class Lorg/medhelp/mydiet/activity/foods/MealsActivity$1;
.super Ljava/lang/Object;
.source "MealsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/MealsActivity;->refreshMealsContent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    .line 208
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    const-class v4, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 209
    .local v1, intent:Landroid/content/Intent;
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    iget-object v4, v3, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMeals:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/medhelp/mydiet/model/Meal;

    iget-object v3, v3, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 213
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    iget-object v4, v3, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMeals:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 214
    .local v2, mealsKey:Ljava/lang/String;
    const-string v3, "transient_meal"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    const-string v3, "date"

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDate:Ljava/util/Date;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 216
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    const/16 v4, 0xbb9

    invoke-virtual {v3, v1, v4}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 217
    return-void

    .line 209
    .end local v2           #mealsKey:Ljava/lang/String;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 210
    .local v0, foodInMeal:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const/4 v4, 0x1

    iput-boolean v4, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    goto :goto_0
.end method
