.class Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;
.super Landroid/os/AsyncTask;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncUserSettingsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x7d0


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/HomeActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/HomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 328
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/HomeActivity;Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 328
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;-><init>(Lorg/medhelp/mydiet/activity/HomeActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs declared-synchronized doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "params"

    .prologue
    .line 339
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/http/MHHttpHandler;->requestUserSettingsSync(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 340
    .local v0, response:Ljava/lang/String;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v1, v0}, Lorg/medhelp/mydiet/util/SyncUtil;->loadUserConfigs(Landroid/content/Context;Ljava/lang/String;)V

    .line 341
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0x7d0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    const/4 v1, 0x0

    monitor-exit p0

    return-object v1

    .line 339
    .end local v0           #response:Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 363
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 365
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0
    .parameter "result"

    .prologue
    .line 357
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 359
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 334
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 335
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 347
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 348
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 353
    :goto_0
    return-void

    .line 350
    :pswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    #calls: Lorg/medhelp/mydiet/activity/HomeActivity;->startUserDataSync()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->access$1(Lorg/medhelp/mydiet/activity/HomeActivity;)V

    goto :goto_0

    .line 348
    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserSettingsTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
