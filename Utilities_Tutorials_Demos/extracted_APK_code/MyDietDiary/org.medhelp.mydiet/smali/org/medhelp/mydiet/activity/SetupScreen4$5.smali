.class Lorg/medhelp/mydiet/activity/SetupScreen4$5;
.super Ljava/lang/Object;
.source "SetupScreen4.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/SetupScreen4;->clickDesiredWeight()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

.field private final synthetic val$units:Ljava/lang/String;

.field private final synthetic val$weightField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/SetupScreen4;Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->val$weightField:Landroid/widget/EditText;

    iput-object p3, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->val$units:Ljava/lang/String;

    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 11
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v10, 0x0

    const-wide v8, 0x4097700000000000L

    .line 378
    const/4 v4, -0x1

    if-ne p2, v4, :cond_1

    .line 379
    const/4 v0, 0x0

    .line 380
    .local v0, weight:F
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->val$weightField:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 381
    .local v3, weightString:Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-lt v4, v5, :cond_0

    .line 382
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 384
    :cond_0
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->val$units:Ljava/lang/String;

    const-string v5, "lb"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 385
    const/4 v4, 0x0

    cmpl-float v4, v0, v4

    if-ltz v4, :cond_2

    float-to-double v4, v0

    cmpg-double v4, v4, v8

    if-gtz v4, :cond_2

    .line 386
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    invoke-static {v4, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setDesiredWeightInLb(Landroid/content/Context;F)V

    .line 398
    :goto_0
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    #calls: Lorg/medhelp/mydiet/activity/SetupScreen4;->refreshViewContents()V
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/SetupScreen4;->access$5(Lorg/medhelp/mydiet/activity/SetupScreen4;)V

    .line 400
    .end local v0           #weight:F
    .end local v3           #weightString:Ljava/lang/String;
    :cond_1
    return-void

    .line 388
    .restart local v0       #weight:F
    .restart local v3       #weightString:Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    const-string v5, "Invalid weight"

    invoke-static {v4, v5, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 391
    :cond_3
    float-to-double v4, v0

    const-wide v6, 0x4001a3112f275febL

    mul-double v1, v4, v6

    .line 392
    .local v1, weightInLb:D
    const-wide/16 v4, 0x0

    cmpl-double v4, v1, v4

    if-ltz v4, :cond_4

    cmpg-double v4, v1, v8

    if-gtz v4, :cond_4

    .line 393
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    invoke-static {v4, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setDesiredWeightInKG(Landroid/content/Context;F)V

    goto :goto_0

    .line 395
    :cond_4
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/SetupScreen4$5;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen4;

    const-string v5, "Invalid weight"

    invoke-static {v4, v5, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
