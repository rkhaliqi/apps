.class public interface abstract Lorg/medhelp/mydiet/C$userSyncJsonKeys;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "userSyncJsonKeys"
.end annotation


# static fields
.field public static final APP_NAME:Ljava/lang/String; = "app_name"

.field public static final APP_NAME_VALUE:Ljava/lang/String; = "mdd_1.2.2"

.field public static final CLEAR_ALL:Ljava/lang/String; = "clear_all"

.field public static final CONFIG_DATA:Ljava/lang/String; = "config_data"

.field public static final CREATED_AT:Ljava/lang/String; = "created_at"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DATE_TIME_STRING:Ljava/lang/String; = "date_time_string"

.field public static final DAYS_TO_SYNC:Ljava/lang/String; = "days_to_sync"

.field public static final END_DATE:Ljava/lang/String; = "end_date"

.field public static final E_CALORIES:Ljava/lang/String; = "calories"

.field public static final E_DISTANCE:Ljava/lang/String; = "distance"

.field public static final E_SPEED:Ljava/lang/String; = "speed"

.field public static final E_TIME_AT:Ljava/lang/String; = "exercise_time"

.field public static final E_TIME_PERIOD:Ljava/lang/String; = "time"

.field public static final E_TYPE:Ljava/lang/String; = "type"

.field public static final FOODS:Ljava/lang/String; = "foods"

.field public static final FOOD_SERVINGS:Ljava/lang/String; = "food_servings"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IN_APP_ID:Ljava/lang/String; = "in_app_id"

.field public static final LAST_SYNC:Ljava/lang/String; = "last_sync"

.field public static final LIMIT:Ljava/lang/String; = "limit"

.field public static final MEAL_AMOUNT:Ljava/lang/String; = "amount"

.field public static final MEAL_FOODS:Ljava/lang/String; = "foods"

.field public static final MEAL_LOCATION:Ljava/lang/String; = "location"

.field public static final MEAL_LOCATION_TYPE:Ljava/lang/String; = "location_type"

.field public static final MEAL_NOTES:Ljava/lang/String; = "quick_note"

.field public static final MEAL_SERVING_ID:Ljava/lang/String; = "serving_id"

.field public static final MEAL_TIME:Ljava/lang/String; = "meal_time"

.field public static final MEAL_TYPE:Ljava/lang/String; = "meal_type"

.field public static final MEAL_USER_INPUT_CALORIES:Ljava/lang/String; = "user_input_calories"

.field public static final MEAL_WITH:Ljava/lang/String; = "company_type"

.field public static final OFFSET:Ljava/lang/String; = "offset"

.field public static final OS:Ljava/lang/String; = "os"

.field public static final OS_VALUE:Ljava/lang/String; = "Android"

.field public static final OS_VERSION:Ljava/lang/String; = "os_version"

.field public static final OS_VERSION_VALUE:Ljava/lang/String; = null

.field public static final RESPONSE_CAUSE:Ljava/lang/String; = "cause"

.field public static final RESPONSE_CONFIG:Ljava/lang/String; = "config"

.field public static final RESPONSE_DATA:Ljava/lang/String; = "data"

.field public static final RESPONSE_MESSAGE:Ljava/lang/String; = "message"

.field public static final RESPONSE_PASSWORD:Ljava/lang/String; = "password"

.field public static final RESPONSE_RESPONSE_CODE:Ljava/lang/String; = "response_code"

.field public static final RESPONSE_STATUS_CODE:Ljava/lang/String; = "status_code"

.field public static final RESPONSE_USERNAME:Ljava/lang/String; = "username"

.field public static final RESPONSE_USER_ID:Ljava/lang/String; = "user_id"

.field public static final SK_MEAL:Ljava/lang/String; = "Meal"

.field public static final SK_WATER:Ljava/lang/String; = "Water"

.field public static final SK_WEIGHT:Ljava/lang/String; = "Weight"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final SOURCE_VALUE:Ljava/lang/String; = "My Diet Diary"

.field public static final START_DATE:Ljava/lang/String; = "start_date"

.field public static final SUPPORTED_CONFIG_KEYS:Ljava/lang/String; = "supported_config_keys"

.field public static final SUPPORTED_KEYS:Ljava/lang/String; = "supported_keys"

.field public static final UNITS_CM:Ljava/lang/String; = "centimeters"

.field public static final UNITS_DISTANCE:Ljava/lang/String; = "unit_distance"

.field public static final UNITS_FL_OZ:Ljava/lang/String; = "fluid_ounces"

.field public static final UNITS_GLASSES:Ljava/lang/String; = "glasses"

.field public static final UNITS_IN:Ljava/lang/String; = "inches"

.field public static final UNITS_KG:Ljava/lang/String; = "kilograms"

.field public static final UNITS_KM:Ljava/lang/String; = "kilometers"

.field public static final UNITS_LB:Ljava/lang/String; = "pounds"

.field public static final UNITS_LENGTH:Ljava/lang/String; = "unit_length"

.field public static final UNITS_MI:Ljava/lang/String; = "miles"

.field public static final UNITS_ML:Ljava/lang/String; = "milliliters"

.field public static final UNITS_WATER:Ljava/lang/String; = "unit_volume"

.field public static final UNITS_WEIGHT:Ljava/lang/String; = "unit_weight"

.field public static final UPDATED_AT:Ljava/lang/String; = "updated_at"

.field public static final UPDATED_FOODS:Ljava/lang/String; = "updated_foods"

.field public static final USER_ACTIVITY_LEVEL:Ljava/lang/String; = "activity_level"

.field public static final USER_AVATAR:Ljava/lang/String; = "avatar"

.field public static final USER_BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final USER_DATA:Ljava/lang/String; = "user_data"

.field public static final USER_GENDER:Ljava/lang/String; = "gender"

.field public static final USER_HEIGHT:Ljava/lang/String; = "last_height"

.field public static final USER_WEIGHT:Ljava/lang/String; = "last_weight"

.field public static final VERSION:Ljava/lang/String; = "version"

.field public static final VERSION_VALUE:Ljava/lang/String; = "4.0"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 470
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sput-object v0, Lorg/medhelp/mydiet/C$userSyncJsonKeys;->OS_VERSION_VALUE:Ljava/lang/String;

    .line 453
    return-void
.end method
