.class public interface abstract Lorg/medhelp/mydiet/C$preferenceData;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "preferenceData"
.end annotation


# static fields
.field public static final ACTIVITY_LEVEL:Ljava/lang/String; = "activity_level"

.field public static final AVATAR_POSITION:Ljava/lang/String; = "avatar_position"

.field public static final CM:I = 0x2

.field public static final DESIRED_WEIGHT:Ljava/lang/String; = "desired_weight"

.field public static final DESIRED_WEIGHT_KG:Ljava/lang/String; = "desired_weight_in_kg"

.field public static final DESIRED_WEIGHT_LB:Ljava/lang/String; = "desired_weight_in_lb"

.field public static final DISTANCE_UNITS:Ljava/lang/String; = "units_distance"

.field public static final DOB_DAY:Ljava/lang/String; = "dob_day"

.field public static final DOB_MONTH:Ljava/lang/String; = "dob_month"

.field public static final DOB_YEAR:Ljava/lang/String; = "dob_year"

.field public static final GENDER:Ljava/lang/String; = "user_gender"

.field public static final GENDER_FEMALE:Ljava/lang/String; = "Female"

.field public static final GENDER_MALE:Ljava/lang/String; = "Male"

.field public static final HEIGHT_CM:Ljava/lang/String; = "height_in_cm"

.field public static final HEIGHT_IN:Ljava/lang/String; = "height_in_inches"

.field public static final HEIGHT_UNITS:Ljava/lang/String; = "units_height"

.field public static final INCHES:I = 0x1

.field public static final KG:I = 0xc

.field public static final LB:I = 0xb

.field public static final PREFERENCES_FILE:Ljava/lang/String; = "mh_preferences"

.field public static final SETUP_DONE:Ljava/lang/String; = "setup_done"

.field public static final TARGET_DATE:Ljava/lang/String; = "target_date"

.field public static final TARGET_DATE_DAY:Ljava/lang/String; = "target_date_day"

.field public static final TARGET_DATE_MONTH:Ljava/lang/String; = "target_date_month"

.field public static final TARGET_DATE_YEAR:Ljava/lang/String; = "target_date_year"

.field public static final TERMS_AGREED:Ljava/lang/String; = "terms_agreed"

.field public static final USER_INPUT_BMR:Ljava/lang/String; = "user_input_bmr"

.field public static final WATER_UNITS:Ljava/lang/String; = "units_water"

.field public static final WEIGHT_GOAL:Ljava/lang/String; = "weight_goal"

.field public static final WEIGHT_KG:Ljava/lang/String; = "weight_in_kg"

.field public static final WEIGHT_LB:Ljava/lang/String; = "weight_in_lb"

.field public static final WEIGHT_UNITS:Ljava/lang/String; = "units_weight"
