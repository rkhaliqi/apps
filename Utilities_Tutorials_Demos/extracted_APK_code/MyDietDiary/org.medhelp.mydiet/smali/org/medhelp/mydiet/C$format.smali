.class public interface abstract Lorg/medhelp/mydiet/C$format;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "format"
.end annotation


# static fields
.field public static final DATE_SLASH_FORMAT_LONG:Ljava/lang/String; = "EEE, MM/dd/yyyy"

.field public static final DATE_SPACE_FORMAT_LONG:Ljava/lang/String; = "EEEE MMM d, yyyy"

.field public static final DATE_SUMMARY_FORMAT:Ljava/lang/String; = "MMMM dd, yyyy"

.field public static final FORMAT_COUNTRY:Ljava/lang/String; = "US"

.field public static final FORMAT_LANGUAGE:Ljava/lang/String; = "en"

.field public static final FORMAT_SYNC_DATE_ONLY:Ljava/lang/String; = "yyyy-MM-dd"

.field public static final FORMAT_SYNC_DATE_TIME:Ljava/lang/String; = "yyyy-MM-dd HH:mm:ss"

.field public static final TIME_AM_PM_FORMAT:Ljava/lang/String; = "h:mm aa"
