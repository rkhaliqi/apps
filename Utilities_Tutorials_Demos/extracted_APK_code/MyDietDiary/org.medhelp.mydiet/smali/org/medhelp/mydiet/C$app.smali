.class public final Lorg/medhelp/mydiet/C$app;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "app"
.end annotation


# static fields
.field public static final APPLICATION_NAME:Ljava/lang/String; = "My Diet Diary"

.field public static final DEBUG:Z = false

.field public static final FIRST_USE:Ljava/lang/String; = "first_use"

.field public static final GENERATED_UUID:Ljava/lang/String; = "generated_uuid"

.field public static final LOGGED_FIRST_USE:Ljava/lang/String; = "logged_first_use"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "org.medhelp.mydiet"

.field public static final USAGE_COUNT:Ljava/lang/String; = "usage_count"

.field public static final USE_PRODUCTION_SERVER:Z = true

.field public static final VERSION_CODE:Ljava/lang/String; = "1.2.2"

.field public static final VERSION_NUMBER:I = 0x9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .parameter "context"

    .prologue
    .line 59
    if-nez p1, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f08

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
