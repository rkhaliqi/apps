.class Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;
.super Landroid/os/AsyncTask;
.source "CreateFoodActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveFoodTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final PROGRESS_SYNC_FOOD:I

.field private _hasEquivalentUnitType:Z

.field private _isDataValid:Z

.field private _servingAmount:D

.field private _servingEquivalent:D

.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 1264
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1265
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_isDataValid:Z

    .line 1266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_hasEquivalentUnitType:Z

    .line 1270
    const/16 v0, 0x65

    iput v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->PROGRESS_SYNC_FOOD:I

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1264
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)V

    return-void
.end method

.method private insertFood()V
    .locals 10

    .prologue
    .line 1356
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 1359
    .local v4, foodWithServings:Lorg/json/JSONObject;
    :try_start_0
    const-string v8, "food"

    iget-object v9, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v9}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v9

    invoke-virtual {v9}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodJSON()Lorg/json/JSONObject;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1360
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 1361
    .local v7, servings:Lorg/json/JSONArray;
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v8

    invoke-virtual {v8}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodServingToCreateFood()Lorg/json/JSONObject;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1362
    const-string v8, "food_servings"

    invoke-virtual {v4, v8, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1367
    .end local v7           #servings:Lorg/json/JSONArray;
    :goto_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1368
    .local v0, cv:Landroid/content/ContentValues;
    const-string v8, "food"

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1370
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    invoke-virtual {v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    invoke-virtual {v8, v9, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 1371
    .local v3, foodUri:Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 1373
    .local v2, foodIdStr:Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 1374
    .local v5, id:J
    iget-object v8, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v8

    iput-wide v5, v8, Lorg/medhelp/mydiet/model/FoodDetail;->inAppId:J

    .line 1375
    return-void

    .line 1363
    .end local v0           #cv:Landroid/content/ContentValues;
    .end local v2           #foodIdStr:Ljava/lang/String;
    .end local v3           #foodUri:Landroid/net/Uri;
    .end local v5           #id:J
    :catch_0
    move-exception v1

    .line 1364
    .local v1, e:Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private saveFood()V
    .locals 0

    .prologue
    .line 1351
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->insertFood()V

    .line 1353
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->doInBackground([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs declared-synchronized doInBackground([Ljava/lang/String;)[Ljava/lang/String;
    .locals 7
    .parameter "params"

    .prologue
    .line 1302
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_isDataValid:Z

    if-eqz v0, :cond_0

    .line 1304
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v0

    iget-boolean v1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_hasEquivalentUnitType:Z

    iget-wide v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_servingAmount:D

    iget-wide v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_servingEquivalent:D

    invoke-static/range {v0 .. v5}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodDetailToCreateOrEditFood(Lorg/medhelp/mydiet/model/FoodDetail;ZDD)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v0

    #setter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v6, v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$6(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Lorg/medhelp/mydiet/model/FoodDetail;)V

    .line 1306
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->saveFood()V

    .line 1307
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x65

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1309
    :cond_0
    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    .line 1302
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 1338
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    if-eqz v0, :cond_0

    .line 1339
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->dismiss()V

    .line 1340
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 1341
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->onPostExecute([Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute([Ljava/lang/String;)V
    .locals 1
    .parameter "result"

    .prologue
    .line 1345
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    if-eqz v0, :cond_0

    .line 1346
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->dismiss()V

    .line 1347
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1348
    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1273
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showSavingDialog()V
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$3(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)V

    .line 1274
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->validateAllFields()Z
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$4(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1275
    iput-boolean v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_isDataValid:Z

    .line 1280
    :goto_0
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    const v3, 0x7f0b0051

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1281
    .local v0, et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1283
    .local v1, text:Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1284
    iput-boolean v4, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_hasEquivalentUnitType:Z

    .line 1285
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iput-wide v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_servingEquivalent:D

    .line 1290
    :goto_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    const v3, 0x7f0b004f

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #et:Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 1291
    .restart local v0       #et:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1292
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/medhelp/mydiet/util/NumberUtil;->isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1293
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iput-wide v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_servingAmount:D

    .line 1298
    :goto_2
    return-void

    .line 1277
    .end local v0           #et:Landroid/widget/EditText;
    .end local v1           #text:Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_isDataValid:Z

    goto :goto_0

    .line 1287
    .restart local v0       #et:Landroid/widget/EditText;
    .restart local v1       #text:Ljava/lang/String;
    :cond_1
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_servingEquivalent:D

    goto :goto_1

    .line 1295
    :cond_2
    const-wide/high16 v2, 0x3ff0

    iput-wide v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->_servingAmount:D

    goto :goto_2
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 10
    .parameter "values"

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    .line 1314
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 1316
    aget-object v0, p1, v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1334
    :goto_0
    return-void

    .line 1318
    :pswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    if-eqz v0, :cond_0

    .line 1319
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mCustomDialog:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$CustomDialog;->dismiss()V

    .line 1320
    :cond_0
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;-><init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;)V

    new-array v1, v9, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$UpdateFoodOnServerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1322
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Saved new food."

    invoke-static {v0, v1, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1323
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1325
    .local v8, resultIntent:Landroid/content/Intent;
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v0

    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodWithServingsAsFromSearch()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x0

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mFoodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v3

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/FoodDetail;->amount:D

    const-string v5, "my_foods"

    .line 1324
    invoke-static/range {v0 .. v5}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemFromJSON(Ljava/lang/String;JDLjava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v6

    .line 1326
    .local v6, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iput-boolean v9, v6, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    .line 1327
    invoke-static {v6}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1328
    .local v7, foodKey:Ljava/lang/String;
    const-string v0, "transient_foods"

    invoke-virtual {v8, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1329
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v8}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->setResult(ILandroid/content/Intent;)V

    .line 1331
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->finish()V

    goto :goto_0

    .line 1316
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$SaveFoodTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
