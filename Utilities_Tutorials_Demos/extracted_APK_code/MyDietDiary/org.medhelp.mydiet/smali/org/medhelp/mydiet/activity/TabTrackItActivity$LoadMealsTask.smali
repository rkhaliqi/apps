.class Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;
.super Landroid/os/AsyncTask;
.source "TabTrackItActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/TabTrackItActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadMealsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 745
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 745
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;-><init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->doInBackground([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 3
    .parameter "params"

    .prologue
    .line 758
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 759
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #getter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$1(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getMealsList(Ljava/util/Date;)Ljava/util/ArrayList;

    move-result-object v2

    #setter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mMeals:Ljava/util/ArrayList;
    invoke-static {v1, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$2(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Ljava/util/ArrayList;)V

    .line 760
    const/4 v1, 0x0

    return-object v1
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 784
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 785
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #calls: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->hideProgressBar()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$5(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    .line 786
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 790
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->publishProgress([Ljava/lang/Object;)V

    .line 791
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 792
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 752
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 753
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->publishProgress([Ljava/lang/Object;)V

    .line 754
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 6
    .parameter "values"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 765
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 766
    aget-object v0, p1, v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 780
    :goto_0
    return-void

    .line 768
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #calls: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->showProgressBar()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$3(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    goto :goto_0

    .line 771
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #calls: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshMealsContent()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$4(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    .line 772
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #calls: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->hideProgressBar()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$5(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    .line 773
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #getter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadExerciseItemsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$6(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #getter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadExerciseItemsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$6(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;

    move-result-object v0

    invoke-virtual {v0, v5}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;->cancel(Z)Z

    .line 776
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    new-instance v1, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;-><init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;)V

    #setter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadExerciseItemsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;
    invoke-static {v0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$7(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;)V

    .line 777
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #getter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadExerciseItemsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$6(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Long;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #getter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$1(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 766
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
