.class Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$2;
.super Ljava/lang/Object;
.source "MealLocationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->onWithWhomClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 120
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p2

    #setter for: Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mWithWhom:Ljava/lang/String;
    invoke-static {v0, v1}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->access$3(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->refreshWithWhomContent()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->access$4(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;)V

    .line 122
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;->access$2(Lorg/medhelp/mydiet/activity/foods/MealLocationActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 123
    return-void
.end method
