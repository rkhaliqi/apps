.class Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$1;
.super Ljava/lang/Object;
.source "AddFoodActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    .line 425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter "arg1"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 429
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    iput p3, v0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mTransientFoodPosition:I

    .line 431
    new-instance v1, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;

    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;)V

    const/4 v0, 0x1

    new-array v2, v0, [Lorg/medhelp/mydiet/model/FoodItemInMeal;

    const/4 v3, 0x0

    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$1;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 432
    return-void
.end method
