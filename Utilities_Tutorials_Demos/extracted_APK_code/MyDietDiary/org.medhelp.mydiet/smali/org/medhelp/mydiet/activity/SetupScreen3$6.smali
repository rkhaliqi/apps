.class Lorg/medhelp/mydiet/activity/SetupScreen3$6;
.super Ljava/lang/Object;
.source "SetupScreen3.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/SetupScreen3;->clickHeight()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

.field private final synthetic val$feetField:Landroid/widget/EditText;

.field private final synthetic val$inchesField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/SetupScreen3;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$6;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$6;->val$feetField:Landroid/widget/EditText;

    iput-object p3, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$6;->val$inchesField:Landroid/widget/EditText;

    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v6, 0x1

    .line 306
    const/4 v5, -0x1

    if-ne p2, v5, :cond_2

    .line 307
    const/4 v4, 0x0

    .line 308
    .local v4, inches:F
    const/4 v0, 0x0

    .line 309
    .local v0, feet:I
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$6;->val$feetField:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 310
    .local v1, ft:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v6, :cond_0

    .line 311
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$6;->val$feetField:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 313
    :cond_0
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$6;->val$inchesField:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 314
    .local v3, in:Ljava/lang/String;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v6, :cond_1

    .line 315
    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 317
    :cond_1
    int-to-float v5, v0

    const/high16 v6, 0x4140

    mul-float/2addr v5, v6

    add-float v2, v5, v4

    .line 318
    .local v2, heightInInches:F
    const/4 v5, 0x0

    cmpl-float v5, v2, v5

    if-lez v5, :cond_3

    .line 319
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$6;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-static {v5, v2}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightInInches(Landroid/content/Context;F)V

    .line 323
    :goto_0
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$6;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    #calls: Lorg/medhelp/mydiet/activity/SetupScreen3;->updateHeightDisplay()V
    invoke-static {v5}, Lorg/medhelp/mydiet/activity/SetupScreen3;->access$3(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    .line 326
    .end local v0           #feet:I
    .end local v1           #ft:Ljava/lang/String;
    .end local v2           #heightInInches:F
    .end local v3           #in:Ljava/lang/String;
    .end local v4           #inches:F
    :cond_2
    return-void

    .line 321
    .restart local v0       #feet:I
    .restart local v1       #ft:Ljava/lang/String;
    .restart local v2       #heightInInches:F
    .restart local v3       #in:Ljava/lang/String;
    .restart local v4       #inches:F
    :cond_3
    iget-object v5, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$6;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    const-string v6, "Invalid height"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
