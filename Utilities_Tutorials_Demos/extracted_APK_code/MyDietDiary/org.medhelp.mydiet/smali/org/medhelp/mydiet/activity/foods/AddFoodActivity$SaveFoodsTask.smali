.class Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;
.super Landroid/os/AsyncTask;
.source "AddFoodActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveFoodsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 631
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 631
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 4
    .parameter "params"

    .prologue
    .line 644
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 645
    .local v0, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v2

    iget-object v2, v2, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 651
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->saveFoodsToDB(Ljava/util/ArrayList;)V
    invoke-static {v2, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$23(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Ljava/util/ArrayList;)V

    .line 652
    const/4 v2, 0x0

    return-object v2

    .line 645
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 646
    .local v1, item:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-boolean v3, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    if-eqz v3, :cond_0

    .line 647
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 8
    .parameter "result"

    .prologue
    .line 670
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Integer;

    const/4 v5, 0x0

    const/16 v6, 0x64

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->publishProgress([Ljava/lang/Object;)V

    .line 672
    new-instance v1, Lorg/medhelp/mydiet/model/Meal;

    invoke-direct {v1}, Lorg/medhelp/mydiet/model/Meal;-><init>()V

    .line 673
    .local v1, meal:Lorg/medhelp/mydiet/model/Meal;
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoodsAdapter:Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$21(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;

    move-result-object v4

    iget-object v4, v4, Lorg/medhelp/mydiet/adapter/AddFoodsListAdapter;->mSelectedFoods:Ljava/util/ArrayList;

    iput-object v4, v1, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    .line 674
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mMealType:Ljava/lang/String;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$26(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/medhelp/mydiet/model/Meal;->setMealType(Ljava/lang/String;)V

    .line 676
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mAddingMoreFoods:J
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$27(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 679
    invoke-static {v1}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 680
    .local v2, mealKey:Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 681
    .local v3, resultIntent:Landroid/content/Intent;
    const-string v4, "transient_meal"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 682
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const/4 v5, -0x1

    invoke-virtual {v4, v5, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->setResult(ILandroid/content/Intent;)V

    .line 683
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-virtual {v4}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->finish()V

    .line 696
    .end local v3           #resultIntent:Landroid/content/Intent;
    :goto_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 697
    return-void

    .line 686
    .end local v2           #mealKey:Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    .line 687
    invoke-static {v1}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 689
    .restart local v2       #mealKey:Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const-class v5, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 690
    .local v0, finalizeMealIntent:Landroid/content/Intent;
    const-string v4, "date"

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mDate:Ljava/util/Date;
    invoke-static {v5}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$28(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 691
    const-string v4, "transient_meal"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 693
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const/16 v5, 0xbb9

    invoke-virtual {v4, v0, v5}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 638
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 639
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->publishProgress([Ljava/lang/Object;)V

    .line 640
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 657
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 658
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 666
    :goto_0
    return-void

    .line 660
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->showSavingFoodsDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$24(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    goto :goto_0

    .line 663
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->hideSavingFoodsDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$25(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V

    goto :goto_0

    .line 658
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SaveFoodsTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
