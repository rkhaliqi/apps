.class Lorg/medhelp/mydiet/activity/WaterActivity$2;
.super Ljava/lang/Object;
.source "WaterActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/WaterActivity;->onUpdateClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

.field private final synthetic val$editor:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/WaterActivity;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->val$editor:Landroid/widget/EditText;

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v6, 0x1

    .line 172
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    #setter for: Lorg/medhelp/mydiet/activity/WaterActivity;->hasEdits:Z
    invoke-static {v3, v6}, Lorg/medhelp/mydiet/activity/WaterActivity;->access$5(Lorg/medhelp/mydiet/activity/WaterActivity;Z)V

    .line 174
    const-wide/16 v0, 0x0

    .line 175
    .local v0, water:D
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->val$editor:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 178
    .local v2, waterString:Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v6, :cond_0

    .line 179
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "0"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 182
    :cond_0
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "mL"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 183
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    const-wide v4, 0x3f715012534d1db4L

    mul-double/2addr v4, v0

    iput-wide v4, v3, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    .line 190
    :goto_0
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    iget-object v3, v3, Lorg/medhelp/mydiet/activity/WaterActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 191
    new-instance v3, Lorg/medhelp/mydiet/activity/WaterActivity$UpdateWaterTask;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lorg/medhelp/mydiet/activity/WaterActivity$UpdateWaterTask;-><init>(Lorg/medhelp/mydiet/activity/WaterActivity;Lorg/medhelp/mydiet/activity/WaterActivity$UpdateWaterTask;)V

    new-array v4, v6, [Ljava/lang/Long;

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    iget-object v6, v6, Lorg/medhelp/mydiet/activity/WaterActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lorg/medhelp/mydiet/activity/WaterActivity$UpdateWaterTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 192
    return-void

    .line 184
    :cond_1
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "fl oz"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 185
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    const-wide/high16 v4, 0x3fc0

    mul-double/2addr v4, v0

    iput-wide v4, v3, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    goto :goto_0

    .line 187
    :cond_2
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/WaterActivity$2;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    iput-wide v0, v3, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    goto :goto_0
.end method
