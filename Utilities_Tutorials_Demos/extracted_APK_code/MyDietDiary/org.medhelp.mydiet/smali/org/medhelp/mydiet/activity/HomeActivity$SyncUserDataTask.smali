.class Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;
.super Landroid/os/AsyncTask;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncUserDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x7d0


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/HomeActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/HomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 368
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/HomeActivity;Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 368
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;-><init>(Lorg/medhelp/mydiet/activity/HomeActivity;)V

    return-void
.end method

.method private syncExercises(I)V
    .locals 22
    .parameter "syncMonthOffsetFromNow"

    .prologue
    .line 494
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncExercisesStatus(Landroid/content/Context;Z)V

    .line 496
    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getCurrentMonthFirstDayMidnight()Ljava/util/Calendar;

    move-result-object v17

    .line 497
    .local v17, startCal:Ljava/util/Calendar;
    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v18

    .line 498
    .local v18, time:J
    new-instance v2, Ljava/util/Date;

    move-wide/from16 v0, v18

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v16

    .line 499
    .local v16, endCal:Ljava/util/Calendar;
    const/4 v2, 0x2

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 502
    const/4 v2, 0x2

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 503
    const/4 v2, 0x2

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 505
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 506
    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "month_year_timestamp=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v9

    const/4 v7, 0x0

    .line 505
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 507
    .local v15, c:Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2, v15}, Lorg/medhelp/mydiet/activity/HomeActivity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 508
    const-wide/16 v7, -0x1

    .line 509
    .local v7, lastSyncTime:J
    if-eqz v15, :cond_0

    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 510
    const-string v2, "last_sync_exercises"

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 513
    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    .line 514
    .local v11, currentTime:J
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual/range {v16 .. v16}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static/range {v2 .. v8}, Lorg/medhelp/mydiet/http/MHHttpHandler;->requestSyncExercises(Landroid/content/Context;JJJ)Ljava/lang/String;

    move-result-object v10

    .line 516
    .local v10, response:Ljava/lang/String;
    invoke-static {v10}, Lorg/medhelp/mydiet/util/SyncUtil;->isResponseValid(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 517
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v13

    invoke-static/range {v9 .. v14}, Lorg/medhelp/mydiet/util/SyncUtil;->handleExerciseItemsSyncResponse(Landroid/content/Context;Ljava/lang/String;JJ)V

    .line 519
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncExercisesStatus(Landroid/content/Context;Z)V

    .line 520
    return-void
.end method

.method private syncMeals(I)V
    .locals 22
    .parameter "syncMonthOffsetFromNow"

    .prologue
    .line 523
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncMealsStatus(Landroid/content/Context;Z)V

    .line 525
    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getCurrentMonthFirstDayMidnight()Ljava/util/Calendar;

    move-result-object v17

    .line 526
    .local v17, startCal:Ljava/util/Calendar;
    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v18

    .line 527
    .local v18, time:J
    new-instance v2, Ljava/util/Date;

    move-wide/from16 v0, v18

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v16

    .line 528
    .local v16, endCal:Ljava/util/Calendar;
    const/4 v2, 0x2

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 531
    const/4 v2, 0x2

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 532
    const/4 v2, 0x2

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 534
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 535
    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "month_year_timestamp=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v9

    const/4 v7, 0x0

    .line 534
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 536
    .local v15, c:Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2, v15}, Lorg/medhelp/mydiet/activity/HomeActivity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 537
    const-wide/16 v7, -0x1

    .line 538
    .local v7, lastSyncTime:J
    if-eqz v15, :cond_0

    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 539
    const-string v2, "last_sync_meals"

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 542
    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    .line 543
    .local v11, currentTime:J
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual/range {v16 .. v16}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static/range {v2 .. v8}, Lorg/medhelp/mydiet/http/MHHttpHandler;->requestSyncMeals(Landroid/content/Context;JJJ)Ljava/lang/String;

    move-result-object v10

    .line 545
    .local v10, response:Ljava/lang/String;
    invoke-static {v10}, Lorg/medhelp/mydiet/util/SyncUtil;->isResponseValid(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 546
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v13

    invoke-static/range {v9 .. v14}, Lorg/medhelp/mydiet/util/SyncUtil;->handleMealsSyncResponse(Landroid/content/Context;Ljava/lang/String;JJ)V

    .line 548
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncMealsStatus(Landroid/content/Context;Z)V

    .line 549
    return-void
.end method

.method private syncNewFoods()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 552
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    const-string v3, "medhelp_id < 1"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 554
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 555
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 556
    const/4 v7, 0x0

    .line 557
    .local v7, foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    const/4 v10, 0x0

    .line 559
    .local v10, foodsWithServingsArray:Lorg/json/JSONArray;
    :cond_0
    const-string v0, "food"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 560
    .local v8, foodJSON:Ljava/lang/String;
    const-string v0, "servings"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 562
    .local v9, foodServingsJSON:Ljava/lang/String;
    invoke-static {v8, v9}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodDetail(Ljava/lang/String;Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v7

    .line 564
    if-eqz v7, :cond_1

    .line 565
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v7, Lorg/medhelp/mydiet/model/FoodDetail;->inAppId:J

    .line 566
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v7}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodWithServingsToCreateOrEdit()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    move-result-object v10

    .line 567
    invoke-virtual {v10}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/medhelp/mydiet/http/MHHttpHandler;->createOrEditFood(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 568
    .local v11, response:Ljava/lang/String;
    invoke-static {v11}, Lorg/medhelp/mydiet/util/SyncUtil;->isResponseValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 569
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v11}, Lorg/medhelp/mydiet/util/SyncUtil;->handleSyncNewFoodsResponse(Landroid/content/Context;Ljava/lang/String;)V

    .line 572
    .end local v11           #response:Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    .line 558
    if-nez v0, :cond_0

    .line 574
    .end local v7           #foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    .end local v8           #foodJSON:Ljava/lang/String;
    .end local v9           #foodServingsJSON:Ljava/lang/String;
    .end local v10           #foodsWithServingsArray:Lorg/json/JSONArray;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 576
    :cond_3
    return-void
.end method

.method private syncUserData()V
    .locals 5

    .prologue
    .line 403
    const-wide/16 v1, 0x3

    .line 405
    .local v1, months:J
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->syncNewFoods()V

    .line 407
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    int-to-long v3, v0

    cmp-long v3, v3, v1

    if-ltz v3, :cond_0

    .line 429
    return-void

    .line 408
    :cond_0
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/SyncUtil;->getSyncWeightStatus(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 409
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/SyncUtil;->getWeightLastSyncTime(Landroid/content/Context;)J

    move-result-wide v3

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/SyncUtil;->isLastSyncTimeWithinThreshhold(J)Z

    move-result v3

    if-nez v3, :cond_2

    .line 410
    :cond_1
    neg-int v3, v0

    invoke-direct {p0, v3}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->syncWeight(I)V

    .line 413
    :cond_2
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/SyncUtil;->getSyncExercisesStatus(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 414
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/SyncUtil;->getExercisesLastSyncTime(Landroid/content/Context;)J

    move-result-wide v3

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/SyncUtil;->isLastSyncTimeWithinThreshhold(J)Z

    move-result v3

    if-nez v3, :cond_4

    .line 416
    :cond_3
    neg-int v3, v0

    invoke-direct {p0, v3}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->syncExercises(I)V

    .line 419
    :cond_4
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/SyncUtil;->getSyncMealsStatus(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 420
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/SyncUtil;->getMealsLastSyncTime(Landroid/content/Context;)J

    move-result-wide v3

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/SyncUtil;->isLastSyncTimeWithinThreshhold(J)Z

    move-result v3

    if-nez v3, :cond_6

    .line 421
    :cond_5
    neg-int v3, v0

    invoke-direct {p0, v3}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->syncMeals(I)V

    .line 424
    :cond_6
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/SyncUtil;->getSyncWaterStatus(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 425
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/util/SyncUtil;->getWaterLastSyncTime(Landroid/content/Context;)J

    move-result-wide v3

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/SyncUtil;->isLastSyncTimeWithinThreshhold(J)Z

    move-result v3

    if-nez v3, :cond_8

    .line 426
    :cond_7
    neg-int v3, v0

    invoke-direct {p0, v3}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->syncWater(I)V

    .line 407
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private syncWater(I)V
    .locals 22
    .parameter "syncMonthOffsetFromNow"

    .prologue
    .line 432
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncWaterStatus(Landroid/content/Context;Z)V

    .line 434
    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getCurrentMonthFirstDayMidnight()Ljava/util/Calendar;

    move-result-object v17

    .line 435
    .local v17, startCal:Ljava/util/Calendar;
    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v18

    .line 436
    .local v18, time:J
    new-instance v2, Ljava/util/Date;

    move-wide/from16 v0, v18

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v16

    .line 437
    .local v16, endCal:Ljava/util/Calendar;
    const/4 v2, 0x2

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 440
    const/4 v2, 0x2

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 441
    const/4 v2, 0x2

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 443
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 444
    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "month_year_timestamp=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v9

    const/4 v7, 0x0

    .line 443
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 445
    .local v15, c:Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2, v15}, Lorg/medhelp/mydiet/activity/HomeActivity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 446
    const-wide/16 v7, -0x1

    .line 447
    .local v7, lastSyncTime:J
    if-eqz v15, :cond_0

    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 448
    const-string v2, "last_sync_water"

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 451
    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    .line 453
    .local v11, currentTime:J
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 454
    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual/range {v16 .. v16}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    .line 453
    invoke-static/range {v2 .. v8}, Lorg/medhelp/mydiet/http/MHHttpHandler;->requestSyncWater(Landroid/content/Context;JJJ)Ljava/lang/String;

    move-result-object v10

    .line 456
    .local v10, response:Ljava/lang/String;
    invoke-static {v10}, Lorg/medhelp/mydiet/util/SyncUtil;->isResponseValid(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 457
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v13

    invoke-static/range {v9 .. v14}, Lorg/medhelp/mydiet/util/SyncUtil;->handleWaterSyncResponse(Landroid/content/Context;Ljava/lang/String;JJ)V

    .line 460
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncWaterStatus(Landroid/content/Context;Z)V

    .line 461
    return-void
.end method

.method private syncWeight(I)V
    .locals 22
    .parameter "syncMonthOffsetFromNow"

    .prologue
    .line 464
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncWeightStatus(Landroid/content/Context;Z)V

    .line 466
    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getCurrentMonthFirstDayMidnight()Ljava/util/Calendar;

    move-result-object v17

    .line 467
    .local v17, startCal:Ljava/util/Calendar;
    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v18

    .line 468
    .local v18, time:J
    new-instance v2, Ljava/util/Date;

    move-wide/from16 v0, v18

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v16

    .line 469
    .local v16, endCal:Ljava/util/Calendar;
    const/4 v2, 0x2

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 472
    const/4 v2, 0x2

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 473
    const/4 v2, 0x2

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 475
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 476
    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "month_year_timestamp=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v9

    const/4 v7, 0x0

    .line 475
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 477
    .local v15, c:Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2, v15}, Lorg/medhelp/mydiet/activity/HomeActivity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 478
    const-wide/16 v7, -0x1

    .line 479
    .local v7, lastSyncTime:J
    if-eqz v15, :cond_0

    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 480
    const-string v2, "last_sync_weight"

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 483
    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    .line 484
    .local v11, currentTime:J
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual/range {v16 .. v16}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static/range {v2 .. v8}, Lorg/medhelp/mydiet/http/MHHttpHandler;->requestSyncWeight(Landroid/content/Context;JJJ)Ljava/lang/String;

    move-result-object v10

    .line 486
    .local v10, response:Ljava/lang/String;
    invoke-static {v10}, Lorg/medhelp/mydiet/util/SyncUtil;->isResponseValid(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 487
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/activity/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual/range {v17 .. v17}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v13

    invoke-static/range {v9 .. v14}, Lorg/medhelp/mydiet/util/SyncUtil;->handleWeightSyncResponse(Landroid/content/Context;Ljava/lang/String;JJ)V

    .line 490
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->setSyncWeightStatus(Landroid/content/Context;Z)V

    .line 491
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs declared-synchronized doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "params"

    .prologue
    .line 377
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->syncUserData()V

    .line 378
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x7d0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    .line 377
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 398
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 399
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0
    .parameter "result"

    .prologue
    .line 393
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 394
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 372
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 373
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 384
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 385
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 389
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$SyncUserDataTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
