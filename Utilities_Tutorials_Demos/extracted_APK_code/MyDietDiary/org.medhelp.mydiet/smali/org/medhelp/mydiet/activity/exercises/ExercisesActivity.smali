.class public Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "ExercisesActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$DeleteExerciseItemTask;,
        Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$LoadExerciseItemsTask;,
        Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;
    }
.end annotation


# instance fields
.field private hasEdits:Z

.field private mDate:Ljava/util/Date;

.field mDialog:Landroid/app/AlertDialog;

.field private mExercisesLayout:Landroid/widget/LinearLayout;

.field mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/ExerciseItem;",
            ">;"
        }
    .end annotation
.end field

.field mProgress:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->hasEdits:Z

    .line 37
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)Ljava/util/Date;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 226
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->showProgressDialog()V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 230
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->hideProgressDialog()V

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 125
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->refreshExercisesContent()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 295
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->showAlertDialog()V

    return-void
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 307
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->hideAlertDialog()V

    return-void
.end method

.method static synthetic access$6(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;IJ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 234
    invoke-direct {p0, p1, p2, p3}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->deleteExerciseItem(IJ)V

    return-void
.end method

.method static synthetic access$7(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-boolean p1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->hasEdits:Z

    return-void
.end method

.method private deleteExerciseItem(IJ)V
    .locals 3
    .parameter "itemPosition"
    .parameter "itemId"

    .prologue
    .line 235
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 236
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v1, "Delete Exercise?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 238
    const-string v1, "Do you want to delete this Exercise?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 240
    const-string v1, "Yes"

    new-instance v2, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;

    invoke-direct {v2, p0, p2, p3, p1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;-><init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;JI)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 250
    const-string v1, "Cancel"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 252
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDialog:Landroid/app/AlertDialog;

    .line 253
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 254
    return-void
.end method

.method private finishAndShowDetails()V
    .locals 1

    .prologue
    .line 121
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->setResult(I)V

    .line 122
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->finish()V

    .line 123
    return-void
.end method

.method private hideAlertDialog()V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 311
    :cond_0
    return-void
.end method

.method private hideProgressDialog()V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 232
    return-void
.end method

.method private refreshExercisesContent()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 126
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mExercisesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 128
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mItems:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 185
    :cond_0
    return-void

    .line 132
    :cond_1
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 133
    .local v3, inflater:Landroid/view/LayoutInflater;
    const/4 v5, 0x0

    .line 134
    .local v5, rlItem:Landroid/widget/RelativeLayout;
    const/4 v1, 0x0

    .line 136
    .local v1, divider:Landroid/view/View;
    const/4 v6, 0x0

    .line 137
    .local v6, tv:Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 139
    .local v0, button:Landroid/widget/ImageButton;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 140
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/medhelp/mydiet/model/ExerciseItem;

    .line 142
    .local v4, item:Lorg/medhelp/mydiet/model/ExerciseItem;
    const v7, 0x7f030005

    invoke-virtual {v3, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .end local v5           #rlItem:Landroid/widget/RelativeLayout;
    check-cast v5, Landroid/widget/RelativeLayout;

    .line 144
    .restart local v5       #rlItem:Landroid/widget/RelativeLayout;
    const v7, 0x7f0b000c

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6           #tv:Landroid/widget/TextView;
    check-cast v6, Landroid/widget/TextView;

    .line 145
    .restart local v6       #tv:Landroid/widget/TextView;
    iget-object v7, v4, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    const v7, 0x7f0b000d

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6           #tv:Landroid/widget/TextView;
    check-cast v6, Landroid/widget/TextView;

    .line 147
    .restart local v6       #tv:Landroid/widget/TextView;
    invoke-virtual {v4}, Lorg/medhelp/mydiet/model/ExerciseItem;->getSummary()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    const v7, 0x7f0b000a

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #button:Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 150
    .restart local v0       #button:Landroid/widget/ImageButton;
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 151
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 153
    new-instance v7, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$1;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$1;-><init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    new-instance v7, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$2;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$2;-><init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mExercisesLayout:Landroid/widget/LinearLayout;

    mul-int/lit8 v8, v2, 0x2

    invoke-virtual {v7, v5, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 181
    const v7, 0x7f03001d

    invoke-virtual {v3, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 182
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, 0x1

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    iget-object v7, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mExercisesLayout:Landroid/widget/LinearLayout;

    mul-int/lit8 v8, v2, 0x2

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 139
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method private showAlertDialog()V
    .locals 5

    .prologue
    .line 296
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 297
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v4, 0x7f03002c

    .line 298
    const v3, 0x7f0b0111

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 297
    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 300
    .local v2, layout:Landroid/view/View;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 301
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 302
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 303
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDialog:Landroid/app/AlertDialog;

    .line 304
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 305
    return-void
.end method

.method private showProgressDialog()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 228
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 106
    packed-switch p1, :pswitch_data_0

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 108
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 109
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->finishAndShowDetails()V

    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0xbb9
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 102
    :goto_0
    monitor-exit p0

    return-void

    .line 97
    :pswitch_0
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "date"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 99
    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 94
    .end local v0           #intent:Landroid/content/Intent;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0038
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 53
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v3, 0x7f03000f

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->setContentView(I)V

    .line 56
    const v3, 0x7f0b0001

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 58
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "date"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 59
    .local v0, timeInMillis:J
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-lez v3, :cond_0

    .line 60
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDate:Ljava/util/Date;

    .line 65
    :goto_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->hasEdits:Z

    .line 67
    const v3, 0x7f0b0037

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mExercisesLayout:Landroid/widget/LinearLayout;

    .line 69
    const v3, 0x7f0b0038

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v3, 0x7f0b0039

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    const v3, 0x7f0b0036

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 73
    .local v2, tvDate:Landroid/widget/TextView;
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDate:Ljava/util/Date;

    const-string v4, "EEEE MMM d, yyyy"

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    return-void

    .line 62
    .end local v2           #tvDate:Landroid/widget/TextView;
    :cond_0
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->finish()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 85
    :cond_0
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->hasEdits:Z

    if-eqz v0, :cond_1

    .line 87
    new-instance v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$SyncUserDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 89
    :cond_1
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 90
    return-void
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 77
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onStart()V

    .line 78
    new-instance v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$LoadExerciseItemsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$LoadExerciseItemsTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$LoadExerciseItemsTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$LoadExerciseItemsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 79
    return-void
.end method
