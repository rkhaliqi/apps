.class Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;
.super Landroid/os/AsyncTask;
.source "WaterActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/WaterActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadWaterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/WaterActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/WaterActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 201
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/WaterActivity;Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;-><init>(Lorg/medhelp/mydiet/activity/WaterActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->doInBackground([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 6
    .parameter "params"

    .prologue
    const-wide/16 v4, 0x0

    .line 214
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 215
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    iget-object v2, v2, Lorg/medhelp/mydiet/activity/WaterActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getWater(Ljava/util/Date;)D

    move-result-wide v2

    iput-wide v2, v1, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    .line 216
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    iget-wide v1, v1, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    cmpg-double v1, v1, v4

    if-gez v1, :cond_0

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    iput-wide v4, v1, Lorg/medhelp/mydiet/activity/WaterActivity;->mWater:D

    .line 217
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 235
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->publishProgress([Ljava/lang/Object;)V

    .line 236
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    #calls: Lorg/medhelp/mydiet/activity/WaterActivity;->refreshViewContent()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/WaterActivity;->access$2(Lorg/medhelp/mydiet/activity/WaterActivity;)V

    .line 237
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 238
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 209
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->publishProgress([Ljava/lang/Object;)V

    .line 210
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 222
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 223
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 231
    :goto_0
    return-void

    .line 225
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    #calls: Lorg/medhelp/mydiet/activity/WaterActivity;->showProgressBar()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/WaterActivity;->access$0(Lorg/medhelp/mydiet/activity/WaterActivity;)V

    goto :goto_0

    .line 228
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->this$0:Lorg/medhelp/mydiet/activity/WaterActivity;

    #calls: Lorg/medhelp/mydiet/activity/WaterActivity;->hideProgressBar()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/WaterActivity;->access$1(Lorg/medhelp/mydiet/activity/WaterActivity;)V

    goto :goto_0

    .line 223
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/WaterActivity$LoadWaterTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
