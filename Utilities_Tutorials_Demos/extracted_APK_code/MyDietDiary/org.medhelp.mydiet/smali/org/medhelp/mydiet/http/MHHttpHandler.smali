.class public Lorg/medhelp/mydiet/http/MHHttpHandler;
.super Ljava/lang/Object;
.source "MHHttpHandler.java"


# static fields
.field private static final FOOD_SEARCH:Ljava/lang/String;

.field private static final URL_ANON:Ljava/lang/String;

.field private static final URL_FOODS:Ljava/lang/String;

.field private static final URL_FOOD_EDIT:Ljava/lang/String;

.field private static final URL_LOGIN:Ljava/lang/String;

.field private static final URL_SIGNUP:Ljava/lang/String;

.field private static final URL_SYNC:Ljava/lang/String;

.field private static httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->SERVER_URL_LOGIN:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/account/login.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_LOGIN:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->SERVER_URL_LOGIN:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/account/signup.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_SIGNUP:Ljava/lang/String;

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->SERVER_URL_LOGIN:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/apis/accounts/anon_create.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_ANON:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->SERVER_URL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/user_data/sync.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_SYNC:Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->SERVER_URL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/apis/foods/search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/http/MHHttpHandler;->FOOD_SEARCH:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->SERVER_URL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/apis/foods/show.json?id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_FOODS:Ljava/lang/String;

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lorg/medhelp/mydiet/C$url;->SERVER_URL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/apis/foods/edit.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_FOOD_EDIT:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createAnonymousUser()Ljava/lang/String;
    .locals 8

    .prologue
    .line 205
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v5

    sput-object v5, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 206
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    sget-object v5, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_ANON:Ljava/lang/String;

    invoke-direct {v3, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 208
    .local v3, post:Lorg/apache/http/client/methods/HttpPost;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 209
    .local v4, postParameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "app_name"

    const-string v7, "mdd_1.1.1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    const/4 v1, 0x0

    .line 213
    .local v1, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_0
    new-instance v2, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v2, v4}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    .end local v1           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .local v2, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_1
    invoke-virtual {v3, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    .line 219
    .end local v2           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v1       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :goto_0
    invoke-static {v3}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, e1:Ljava/io/UnsupportedEncodingException;
    :goto_1
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 215
    .end local v0           #e1:Ljava/io/UnsupportedEncodingException;
    .end local v1           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v2       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v1       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    goto :goto_1
.end method

.method public static createOrEditFood(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .parameter "foodsArray"

    .prologue
    .line 223
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v5

    sput-object v5, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 224
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    sget-object v5, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_FOOD_EDIT:Ljava/lang/String;

    invoke-direct {v3, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 226
    .local v3, post:Lorg/apache/http/client/methods/HttpPost;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 227
    .local v4, postParameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "app_name"

    const-string v7, "mdd_1.1.1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "version"

    const-string v7, "2.0"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "foods"

    invoke-direct {v5, v6, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    const/4 v1, 0x0

    .line 233
    .local v1, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_0
    new-instance v2, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v2, v4}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    .end local v1           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .local v2, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_1
    invoke-virtual {v3, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    .line 239
    .end local v2           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v1       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :goto_0
    invoke-static {v3}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 235
    :catch_0
    move-exception v0

    .line 236
    .local v0, e1:Ljava/io/UnsupportedEncodingException;
    :goto_1
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 235
    .end local v0           #e1:Ljava/io/UnsupportedEncodingException;
    .end local v1           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v2       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v1       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    goto :goto_1
.end method

.method public static doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;
    .locals 11
    .parameter "post"

    .prologue
    .line 81
    const/4 v7, 0x0

    .line 82
    .local v7, response:Lorg/apache/http/HttpResponse;
    const/4 v2, 0x0

    .line 83
    .local v2, entity:Lorg/apache/http/HttpEntity;
    const/4 v3, 0x0

    .line 84
    .local v3, inputStream:Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 85
    .local v5, reader:Ljava/io/BufferedReader;
    const-string v8, ""

    .line 87
    .local v8, result:Ljava/lang/String;
    :try_start_0
    sget-object v9, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v9, p0}, Lorg/medhelp/mydiet/http/MHHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    .line 88
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 89
    if-eqz v2, :cond_0

    .line 90
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 92
    :cond_0
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v10, 0x1f40

    invoke-direct {v6, v9, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 93
    .end local v5           #reader:Ljava/io/BufferedReader;
    .local v6, reader:Ljava/io/BufferedReader;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v9, ""

    invoke-direct {v0, v9}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 94
    .local v0, builder:Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    .line 95
    .local v4, line:Ljava/lang/String;
    :goto_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 98
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 100
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    move-result-object v8

    .line 106
    if-eqz v3, :cond_4

    .line 108
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    move-object v5, v6

    .line 115
    .end local v0           #builder:Ljava/lang/StringBuffer;
    .end local v4           #line:Ljava/lang/String;
    .end local v6           #reader:Ljava/io/BufferedReader;
    .restart local v5       #reader:Ljava/io/BufferedReader;
    :cond_1
    :goto_1
    return-object v8

    .line 96
    .end local v5           #reader:Ljava/io/BufferedReader;
    .restart local v0       #builder:Ljava/lang/StringBuffer;
    .restart local v4       #line:Ljava/lang/String;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    :cond_2
    :try_start_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    goto :goto_0

    .line 101
    .end local v0           #builder:Ljava/lang/StringBuffer;
    .end local v4           #line:Ljava/lang/String;
    :catch_0
    move-exception v1

    move-object v5, v6

    .line 102
    .end local v6           #reader:Ljava/io/BufferedReader;
    .local v1, e:Lorg/apache/http/client/ClientProtocolException;
    .restart local v5       #reader:Ljava/io/BufferedReader;
    :goto_2
    :try_start_4
    invoke-virtual {v1}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 106
    if-eqz v3, :cond_1

    .line 108
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 109
    :catch_1
    move-exception v1

    .line 110
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 103
    .end local v1           #e:Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 104
    .restart local v1       #e:Ljava/io/IOException;
    :goto_3
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 106
    if-eqz v3, :cond_1

    .line 108
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 109
    :catch_3
    move-exception v1

    .line 110
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 105
    .end local v1           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 106
    :goto_4
    if-eqz v3, :cond_3

    .line 108
    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 113
    :cond_3
    :goto_5
    throw v9

    .line 109
    :catch_4
    move-exception v1

    .line 110
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 109
    .end local v1           #e:Ljava/io/IOException;
    .end local v5           #reader:Ljava/io/BufferedReader;
    .restart local v0       #builder:Ljava/lang/StringBuffer;
    .restart local v4       #line:Ljava/lang/String;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    :catch_5
    move-exception v1

    .line 110
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .end local v1           #e:Ljava/io/IOException;
    :cond_4
    move-object v5, v6

    .end local v6           #reader:Ljava/io/BufferedReader;
    .restart local v5       #reader:Ljava/io/BufferedReader;
    goto :goto_1

    .line 105
    .end local v0           #builder:Ljava/lang/StringBuffer;
    .end local v4           #line:Ljava/lang/String;
    .end local v5           #reader:Ljava/io/BufferedReader;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    :catchall_1
    move-exception v9

    move-object v5, v6

    .end local v6           #reader:Ljava/io/BufferedReader;
    .restart local v5       #reader:Ljava/io/BufferedReader;
    goto :goto_4

    .line 103
    .end local v5           #reader:Ljava/io/BufferedReader;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    :catch_6
    move-exception v1

    move-object v5, v6

    .end local v6           #reader:Ljava/io/BufferedReader;
    .restart local v5       #reader:Ljava/io/BufferedReader;
    goto :goto_3

    .line 101
    :catch_7
    move-exception v1

    goto :goto_2
.end method

.method public static downloadBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .parameter "url"

    .prologue
    const/4 v6, 0x0

    .line 171
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v7

    sput-object v7, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 174
    :try_start_0
    invoke-static {p0}, Lorg/medhelp/mydiet/http/MHHttpHandler;->getRequest(Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 175
    .local v4, response:Lorg/apache/http/HttpResponse;
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    .line 176
    .local v5, statusCode:I
    const/16 v7, 0xc8

    if-eq v5, v7, :cond_0

    move-object v0, v6

    .line 201
    .end local v4           #response:Lorg/apache/http/HttpResponse;
    .end local v5           #statusCode:I
    :goto_0
    return-object v0

    .line 180
    .restart local v4       #response:Lorg/apache/http/HttpResponse;
    .restart local v5       #statusCode:I
    :cond_0
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .line 181
    .local v2, entity:Lorg/apache/http/HttpEntity;
    if-eqz v2, :cond_2

    .line 182
    const/4 v3, 0x0

    .line 184
    .local v3, inputStream:Ljava/io/InputStream;
    :try_start_1
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 185
    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 188
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v3, :cond_1

    .line 189
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 191
    :cond_1
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 194
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v2           #entity:Lorg/apache/http/HttpEntity;
    .end local v3           #inputStream:Ljava/io/InputStream;
    .end local v4           #response:Lorg/apache/http/HttpResponse;
    .end local v5           #statusCode:I
    :catch_0
    move-exception v1

    .line 195
    .local v1, e:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .end local v1           #e:Ljava/lang/IllegalStateException;
    :cond_2
    :goto_1
    move-object v0, v6

    .line 201
    goto :goto_0

    .line 187
    .restart local v2       #entity:Lorg/apache/http/HttpEntity;
    .restart local v3       #inputStream:Ljava/io/InputStream;
    .restart local v4       #response:Lorg/apache/http/HttpResponse;
    .restart local v5       #statusCode:I
    :catchall_0
    move-exception v7

    .line 188
    if-eqz v3, :cond_3

    .line 189
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 191
    :cond_3
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 192
    throw v7
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 196
    .end local v2           #entity:Lorg/apache/http/HttpEntity;
    .end local v3           #inputStream:Ljava/io/InputStream;
    .end local v4           #response:Lorg/apache/http/HttpResponse;
    .end local v5           #statusCode:I
    :catch_1
    move-exception v1

    .line 197
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 198
    .end local v1           #e:Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 199
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static getBasicSyncJSONObject(JJ)Lorg/json/JSONObject;
    .locals 6
    .parameter "startDate"
    .parameter "endDate"

    .prologue
    const-wide/16 v4, 0x0

    .line 464
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 466
    .local v1, postObject:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "os"

    const-string v3, "Android"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 467
    const-string v2, "os_version"

    sget-object v3, Lorg/medhelp/mydiet/C$userSyncJsonKeys;->OS_VERSION_VALUE:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 468
    const-string v2, "app_name"

    const-string v3, "mdd_1.2.2"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 469
    const-string v2, "source"

    const-string v3, "My Diet Diary"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 470
    const-string v2, "version"

    const-string v3, "4.0"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 471
    const-string v2, "supported_config_keys"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 472
    const-string v2, "supported_keys"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 473
    const-string v2, "user_data"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 475
    cmp-long v2, p0, v4

    if-lez v2, :cond_0

    cmp-long v2, p2, v4

    if-lez v2, :cond_0

    .line 476
    const-string v2, "start_date"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 477
    const-string v4, "yyyy-MM-dd"

    .line 476
    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 478
    const-string v2, "end_date"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, p2, p3}, Ljava/util/Date;-><init>(J)V

    .line 479
    const-string v4, "yyyy-MM-dd"

    .line 478
    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    :cond_0
    :goto_0
    return-object v1

    .line 481
    :catch_0
    move-exception v0

    .line 482
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getClientProtocolExceptionResultObject()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 500
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 502
    .local v1, resultObject:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "response_code"

    const/16 v3, 0x386

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 503
    const-string v2, "message"

    const-string v3, "Cannot connect to server"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 504
    const-string v2, "cause"

    const-string v3, "IOException"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    :goto_0
    return-object v1

    .line 505
    :catch_0
    move-exception v0

    .line 506
    .local v0, e1:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getIOExceptionResultObject()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 488
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 490
    .local v1, resultObject:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "response_code"

    const/16 v3, 0x385

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 491
    const-string v2, "message"

    const-string v3, "Cannot connect to server"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 492
    const-string v2, "cause"

    const-string v3, "IOException"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 496
    :goto_0
    return-object v1

    .line 493
    :catch_0
    move-exception v0

    .line 494
    .local v0, e1:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getRequest(Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 4
    .parameter "url"

    .prologue
    .line 51
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, p0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 53
    .local v1, getRequest:Lorg/apache/http/client/methods/HttpGet;
    const/4 v2, 0x0

    .line 55
    .local v2, response:Lorg/apache/http/HttpResponse;
    :try_start_0
    sget-object v3, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v3, v1}, Lorg/medhelp/mydiet/http/MHHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 62
    :goto_0
    return-object v2

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 58
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_1
    move-exception v0

    .line 59
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static login(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .parameter "username"
    .parameter "password"

    .prologue
    .line 129
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v5

    sput-object v5, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 130
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    sget-object v5, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_LOGIN:Ljava/lang/String;

    invoke-direct {v3, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 132
    .local v3, post:Lorg/apache/http/client/methods/HttpPost;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v4, postParameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "login"

    invoke-direct {v5, v6, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "password"

    invoke-direct {v5, v6, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "version"

    const-string v7, "3"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    const/4 v1, 0x0

    .line 139
    .local v1, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_0
    new-instance v2, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v2, v4}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    .end local v1           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .local v2, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_1
    invoke-virtual {v3, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    .line 145
    .end local v2           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v1       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :goto_0
    invoke-static {v3}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, e1:Ljava/io/UnsupportedEncodingException;
    :goto_1
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 141
    .end local v0           #e1:Ljava/io/UnsupportedEncodingException;
    .end local v1           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v2       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v1       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    goto :goto_1
.end method

.method public static postRequest(Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 4
    .parameter "url"

    .prologue
    .line 66
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, p0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 68
    .local v1, postResuest:Lorg/apache/http/client/methods/HttpPost;
    const/4 v2, 0x0

    .line 70
    .local v2, response:Lorg/apache/http/HttpResponse;
    :try_start_0
    sget-object v3, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v3, v1}, Lorg/medhelp/mydiet/http/MHHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 77
    :goto_0
    return-object v2

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 73
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_1
    move-exception v0

    .line 74
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static requestFoods(Landroid/content/Context;Lorg/json/JSONArray;)Ljava/lang/String;
    .locals 12
    .parameter "c"
    .parameter "foodIdsArray"

    .prologue
    .line 421
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-nez v10, :cond_2

    :cond_0
    const/4 v9, 0x0

    .line 460
    :cond_1
    :goto_0
    return-object v9

    .line 423
    :cond_2
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v10

    sput-object v10, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 424
    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    new-instance v10, Ljava/lang/StringBuilder;

    sget-object v11, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_FOODS:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 426
    .local v3, get:Lorg/apache/http/client/methods/HttpGet;
    const/4 v8, 0x0

    .line 427
    .local v8, response:Lorg/apache/http/HttpResponse;
    const/4 v2, 0x0

    .line 428
    .local v2, entity:Lorg/apache/http/HttpEntity;
    const/4 v4, 0x0

    .line 429
    .local v4, inputStream:Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 430
    .local v6, reader:Ljava/io/BufferedReader;
    const-string v9, ""

    .line 432
    .local v9, result:Ljava/lang/String;
    :try_start_0
    sget-object v10, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    invoke-virtual {v10, v3}, Lorg/medhelp/mydiet/http/MHHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 433
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 434
    if-eqz v2, :cond_3

    .line 435
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    .line 437
    :cond_3
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-direct {v10, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v11, 0x1f40

    invoke-direct {v7, v10, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 438
    .end local v6           #reader:Ljava/io/BufferedReader;
    .local v7, reader:Ljava/io/BufferedReader;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v10, ""

    invoke-direct {v0, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 439
    .local v0, builder:Ljava/lang/StringBuffer;
    const/4 v5, 0x0

    .line 440
    .local v5, line:Ljava/lang/String;
    :goto_1
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_4

    .line 443
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 444
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    move-result-object v9

    .line 452
    if-eqz v4, :cond_1

    .line 454
    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 455
    :catch_0
    move-exception v1

    .line 456
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 441
    .end local v1           #e:Ljava/io/IOException;
    :cond_4
    :try_start_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    goto :goto_1

    .line 445
    .end local v0           #builder:Ljava/lang/StringBuffer;
    .end local v5           #line:Ljava/lang/String;
    :catch_1
    move-exception v1

    move-object v6, v7

    .line 446
    .end local v7           #reader:Ljava/io/BufferedReader;
    .local v1, e:Lorg/apache/http/client/ClientProtocolException;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    :goto_2
    :try_start_4
    invoke-virtual {v1}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 447
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpHandler;->getClientProtocolExceptionResultObject()Lorg/json/JSONObject;

    move-result-object v10

    invoke-virtual {v10}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v9

    .line 452
    .end local v9           #result:Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 454
    :try_start_5
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 455
    :catch_2
    move-exception v1

    .line 456
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 448
    .end local v1           #e:Ljava/io/IOException;
    .restart local v9       #result:Ljava/lang/String;
    :catch_3
    move-exception v1

    .line 449
    .restart local v1       #e:Ljava/io/IOException;
    :goto_3
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 450
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpHandler;->getIOExceptionResultObject()Lorg/json/JSONObject;

    move-result-object v10

    invoke-virtual {v10}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v9

    .line 452
    .end local v9           #result:Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 454
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_0

    .line 455
    :catch_4
    move-exception v1

    .line 456
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 451
    .end local v1           #e:Ljava/io/IOException;
    .restart local v9       #result:Ljava/lang/String;
    :catchall_0
    move-exception v10

    .line 452
    :goto_4
    if-eqz v4, :cond_5

    .line 454
    :try_start_8
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 459
    :cond_5
    :goto_5
    throw v10

    .line 455
    :catch_5
    move-exception v1

    .line 456
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 451
    .end local v1           #e:Ljava/io/IOException;
    .end local v6           #reader:Ljava/io/BufferedReader;
    .restart local v7       #reader:Ljava/io/BufferedReader;
    :catchall_1
    move-exception v10

    move-object v6, v7

    .end local v7           #reader:Ljava/io/BufferedReader;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    goto :goto_4

    .line 448
    .end local v6           #reader:Ljava/io/BufferedReader;
    .restart local v7       #reader:Ljava/io/BufferedReader;
    :catch_6
    move-exception v1

    move-object v6, v7

    .end local v7           #reader:Ljava/io/BufferedReader;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    goto :goto_3

    .line 445
    :catch_7
    move-exception v1

    goto :goto_2
.end method

.method public static requestSyncExercises(Landroid/content/Context;JJJ)Ljava/lang/String;
    .locals 16
    .parameter "c"
    .parameter "startDate"
    .parameter "endDate"
    .parameter "lastSyncTime"

    .prologue
    .line 342
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v2

    sput-object v2, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 343
    new-instance v11, Lorg/apache/http/client/methods/HttpPost;

    sget-object v2, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_SYNC:Ljava/lang/String;

    invoke-direct {v11, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 344
    .local v11, post:Lorg/apache/http/client/methods/HttpPost;
    const-string v2, "Content-Type"

    const-string v3, "application/json"

    invoke-virtual {v11, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    const-string v2, "Accept"

    const-string v3, "application/json"

    invoke-virtual {v11, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    invoke-static/range {p1 .. p4}, Lorg/medhelp/mydiet/http/MHHttpHandler;->getBasicSyncJSONObject(JJ)Lorg/json/JSONObject;

    move-result-object v12

    .line 349
    .local v12, postObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v15, Lorg/json/JSONArray;

    invoke-direct {v15}, Lorg/json/JSONArray;-><init>()V

    .line 350
    .local v15, supportedKeys:Lorg/json/JSONArray;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISES:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "name"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 351
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 352
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 354
    :cond_0
    const-string v2, "medhelp_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 355
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    .line 353
    if-nez v2, :cond_0

    .line 357
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 359
    :cond_2
    const-string v2, "supported_keys"

    invoke-virtual {v12, v2, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 361
    const-string v2, "last_sync"

    .line 362
    new-instance v3, Ljava/util/Date;

    move-wide/from16 v0, p5

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    const-string v4, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 360
    invoke-virtual {v12, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 364
    invoke-static/range {p0 .. p6}, Lorg/medhelp/mydiet/util/SyncUtil;->getExercisesToSync(Landroid/content/Context;JJJ)Lorg/json/JSONObject;

    move-result-object v10

    .line 365
    .local v10, obj:Lorg/json/JSONObject;
    if-eqz v10, :cond_3

    .line 366
    const-string v2, "user_data"

    invoke-virtual {v12, v2, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    .end local v8           #cursor:Landroid/database/Cursor;
    .end local v10           #obj:Lorg/json/JSONObject;
    .end local v15           #supportedKeys:Lorg/json/JSONArray;
    :cond_3
    :goto_0
    const/4 v13, 0x0

    .line 374
    .local v13, se:Lorg/apache/http/entity/StringEntity;
    :try_start_1
    new-instance v14, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v14, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v13           #se:Lorg/apache/http/entity/StringEntity;
    .local v14, se:Lorg/apache/http/entity/StringEntity;
    move-object v13, v14

    .line 378
    .end local v14           #se:Lorg/apache/http/entity/StringEntity;
    .restart local v13       #se:Lorg/apache/http/entity/StringEntity;
    :goto_1
    invoke-virtual {v11, v13}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 380
    invoke-static {v11}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 368
    .end local v13           #se:Lorg/apache/http/entity/StringEntity;
    :catch_0
    move-exception v9

    .line 369
    .local v9, e2:Lorg/json/JSONException;
    invoke-virtual {v9}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 375
    .end local v9           #e2:Lorg/json/JSONException;
    .restart local v13       #se:Lorg/apache/http/entity/StringEntity;
    :catch_1
    move-exception v9

    .line 376
    .local v9, e2:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v9}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method

.method public static requestSyncMeals(Landroid/content/Context;JJJ)Ljava/lang/String;
    .locals 12
    .parameter "c"
    .parameter "startDate"
    .parameter "endDate"
    .parameter "lastSyncTime"

    .prologue
    .line 384
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v9

    sput-object v9, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 385
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    sget-object v9, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_SYNC:Ljava/lang/String;

    invoke-direct {v4, v9}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 386
    .local v4, post:Lorg/apache/http/client/methods/HttpPost;
    const-string v9, "Content-Type"

    const-string v10, "application/json"

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    const-string v9, "Accept"

    const-string v10, "application/json"

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    invoke-static/range {p1 .. p4}, Lorg/medhelp/mydiet/http/MHHttpHandler;->getBasicSyncJSONObject(JJ)Lorg/json/JSONObject;

    move-result-object v5

    .line 391
    .local v5, postObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 392
    .local v8, skArray:Lorg/json/JSONArray;
    const-string v9, "Meal"

    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 394
    const-string v9, "supported_keys"

    invoke-virtual {v5, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 396
    const-string v9, "last_sync"

    .line 397
    new-instance v10, Ljava/util/Date;

    move-wide/from16 v0, p5

    invoke-direct {v10, v0, v1}, Ljava/util/Date;-><init>(J)V

    const-string v11, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v10, v11}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 395
    invoke-virtual {v5, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 399
    invoke-static/range {p0 .. p6}, Lorg/medhelp/mydiet/util/SyncUtil;->getMealsToSync(Landroid/content/Context;JJJ)Lorg/json/JSONObject;

    move-result-object v3

    .line 400
    .local v3, obj:Lorg/json/JSONObject;
    if-eqz v3, :cond_0

    .line 401
    const-string v9, "user_data"

    invoke-virtual {v5, v9, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 409
    .end local v3           #obj:Lorg/json/JSONObject;
    .end local v8           #skArray:Lorg/json/JSONArray;
    :goto_0
    const/4 v6, 0x0

    .line 411
    .local v6, se:Lorg/apache/http/entity/StringEntity;
    :try_start_1
    new-instance v7, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v6           #se:Lorg/apache/http/entity/StringEntity;
    .local v7, se:Lorg/apache/http/entity/StringEntity;
    move-object v6, v7

    .line 415
    .end local v7           #se:Lorg/apache/http/entity/StringEntity;
    .restart local v6       #se:Lorg/apache/http/entity/StringEntity;
    :goto_1
    invoke-virtual {v4, v6}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 417
    invoke-static {v4}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 403
    .end local v6           #se:Lorg/apache/http/entity/StringEntity;
    .restart local v3       #obj:Lorg/json/JSONObject;
    .restart local v8       #skArray:Lorg/json/JSONArray;
    :cond_0
    :try_start_2
    const-string v9, "user_data"

    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v5, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 405
    .end local v3           #obj:Lorg/json/JSONObject;
    .end local v8           #skArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 406
    .local v2, e2:Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 412
    .end local v2           #e2:Lorg/json/JSONException;
    .restart local v6       #se:Lorg/apache/http/entity/StringEntity;
    :catch_1
    move-exception v2

    .line 413
    .local v2, e2:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method

.method public static requestSyncWater(Landroid/content/Context;JJJ)Ljava/lang/String;
    .locals 12
    .parameter "c"
    .parameter "startDate"
    .parameter "endDate"
    .parameter "lastSyncTime"

    .prologue
    .line 269
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v9

    sput-object v9, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 271
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    sget-object v9, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_SYNC:Ljava/lang/String;

    invoke-direct {v4, v9}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 272
    .local v4, post:Lorg/apache/http/client/methods/HttpPost;
    const-string v9, "Content-Type"

    const-string v10, "application/json"

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string v9, "Accept"

    const-string v10, "application/json"

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-static/range {p1 .. p4}, Lorg/medhelp/mydiet/http/MHHttpHandler;->getBasicSyncJSONObject(JJ)Lorg/json/JSONObject;

    move-result-object v5

    .line 277
    .local v5, postObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 278
    .local v8, wArray:Lorg/json/JSONArray;
    const-string v9, "Water"

    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 279
    const-string v9, "supported_keys"

    invoke-virtual {v5, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 282
    const-string v9, "last_sync"

    .line 283
    new-instance v10, Ljava/util/Date;

    move-wide/from16 v0, p5

    invoke-direct {v10, v0, v1}, Ljava/util/Date;-><init>(J)V

    const-string v11, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v10, v11}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 281
    invoke-virtual {v5, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 285
    invoke-static/range {p0 .. p6}, Lorg/medhelp/mydiet/util/SyncUtil;->getWaterToSync(Landroid/content/Context;JJJ)Lorg/json/JSONObject;

    move-result-object v3

    .line 286
    .local v3, obj:Lorg/json/JSONObject;
    if-eqz v3, :cond_0

    .line 287
    const-string v9, "user_data"

    invoke-virtual {v5, v9, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    .end local v3           #obj:Lorg/json/JSONObject;
    .end local v8           #wArray:Lorg/json/JSONArray;
    :goto_0
    const/4 v6, 0x0

    .line 297
    .local v6, se:Lorg/apache/http/entity/StringEntity;
    :try_start_1
    new-instance v7, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v6           #se:Lorg/apache/http/entity/StringEntity;
    .local v7, se:Lorg/apache/http/entity/StringEntity;
    move-object v6, v7

    .line 301
    .end local v7           #se:Lorg/apache/http/entity/StringEntity;
    .restart local v6       #se:Lorg/apache/http/entity/StringEntity;
    :goto_1
    invoke-virtual {v4, v6}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 303
    invoke-static {v4}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 289
    .end local v6           #se:Lorg/apache/http/entity/StringEntity;
    .restart local v3       #obj:Lorg/json/JSONObject;
    .restart local v8       #wArray:Lorg/json/JSONArray;
    :cond_0
    :try_start_2
    const-string v9, "user_data"

    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v5, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 291
    .end local v3           #obj:Lorg/json/JSONObject;
    .end local v8           #wArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 292
    .local v2, e2:Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 298
    .end local v2           #e2:Lorg/json/JSONException;
    .restart local v6       #se:Lorg/apache/http/entity/StringEntity;
    :catch_1
    move-exception v2

    .line 299
    .local v2, e2:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method

.method public static requestSyncWeight(Landroid/content/Context;JJJ)Ljava/lang/String;
    .locals 12
    .parameter "c"
    .parameter "startDate"
    .parameter "endDate"
    .parameter "lastSyncTime"

    .prologue
    .line 307
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v9

    sput-object v9, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 308
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    sget-object v9, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_SYNC:Ljava/lang/String;

    invoke-direct {v4, v9}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 309
    .local v4, post:Lorg/apache/http/client/methods/HttpPost;
    const-string v9, "Content-Type"

    const-string v10, "application/json"

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    const-string v9, "Accept"

    const-string v10, "application/json"

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    invoke-static/range {p1 .. p4}, Lorg/medhelp/mydiet/http/MHHttpHandler;->getBasicSyncJSONObject(JJ)Lorg/json/JSONObject;

    move-result-object v5

    .line 314
    .local v5, postObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 315
    .local v8, wArray:Lorg/json/JSONArray;
    const-string v9, "Weight"

    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 316
    const-string v9, "supported_keys"

    invoke-virtual {v5, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 319
    const-string v9, "last_sync"

    .line 320
    new-instance v10, Ljava/util/Date;

    move-wide/from16 v0, p5

    invoke-direct {v10, v0, v1}, Ljava/util/Date;-><init>(J)V

    const-string v11, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v10, v11}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 318
    invoke-virtual {v5, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 322
    invoke-static/range {p0 .. p6}, Lorg/medhelp/mydiet/util/SyncUtil;->getWeightToSync(Landroid/content/Context;JJJ)Lorg/json/JSONObject;

    move-result-object v3

    .line 323
    .local v3, obj:Lorg/json/JSONObject;
    if-eqz v3, :cond_0

    .line 324
    const-string v9, "user_data"

    invoke-virtual {v5, v9, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    .end local v3           #obj:Lorg/json/JSONObject;
    .end local v8           #wArray:Lorg/json/JSONArray;
    :cond_0
    :goto_0
    const/4 v6, 0x0

    .line 332
    .local v6, se:Lorg/apache/http/entity/StringEntity;
    :try_start_1
    new-instance v7, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v6           #se:Lorg/apache/http/entity/StringEntity;
    .local v7, se:Lorg/apache/http/entity/StringEntity;
    move-object v6, v7

    .line 336
    .end local v7           #se:Lorg/apache/http/entity/StringEntity;
    .restart local v6       #se:Lorg/apache/http/entity/StringEntity;
    :goto_1
    invoke-virtual {v4, v6}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 338
    invoke-static {v4}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 326
    .end local v6           #se:Lorg/apache/http/entity/StringEntity;
    :catch_0
    move-exception v2

    .line 327
    .local v2, e2:Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 333
    .end local v2           #e2:Lorg/json/JSONException;
    .restart local v6       #se:Lorg/apache/http/entity/StringEntity;
    :catch_1
    move-exception v2

    .line 334
    .local v2, e2:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method

.method public static requestUserSettingsSync(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .parameter "c"

    .prologue
    const-wide/16 v8, 0x0

    .line 243
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v6

    sput-object v6, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 244
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    sget-object v6, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_SYNC:Ljava/lang/String;

    invoke-direct {v2, v6}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 245
    .local v2, post:Lorg/apache/http/client/methods/HttpPost;
    const-string v6, "Content-Type"

    const-string v7, "application/json"

    invoke-virtual {v2, v6, v7}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v6, "Accept"

    const-string v7, "application/json"

    invoke-virtual {v2, v6, v7}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-static {v8, v9, v8, v9}, Lorg/medhelp/mydiet/http/MHHttpHandler;->getBasicSyncJSONObject(JJ)Lorg/json/JSONObject;

    move-result-object v3

    .line 250
    .local v3, postObject:Lorg/json/JSONObject;
    :try_start_0
    invoke-static {p0}, Lorg/medhelp/mydiet/util/SyncUtil;->getUserConfigsToSync(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v0

    .line 251
    .local v0, configsObj:Lorg/json/JSONObject;
    const-string v6, "supported_config_keys"

    const-string v7, "supported_config_keys"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 252
    const-string v6, "config_data"

    const-string v7, "config_data"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    .end local v0           #configsObj:Lorg/json/JSONObject;
    :goto_0
    const/4 v4, 0x0

    .line 259
    .local v4, se:Lorg/apache/http/entity/StringEntity;
    :try_start_1
    new-instance v5, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v4           #se:Lorg/apache/http/entity/StringEntity;
    .local v5, se:Lorg/apache/http/entity/StringEntity;
    move-object v4, v5

    .line 263
    .end local v5           #se:Lorg/apache/http/entity/StringEntity;
    .restart local v4       #se:Lorg/apache/http/entity/StringEntity;
    :goto_1
    invoke-virtual {v2, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 265
    invoke-static {v2}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 253
    .end local v4           #se:Lorg/apache/http/entity/StringEntity;
    :catch_0
    move-exception v1

    .line 254
    .local v1, e2:Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 260
    .end local v1           #e2:Lorg/json/JSONException;
    .restart local v4       #se:Lorg/apache/http/entity/StringEntity;
    :catch_1
    move-exception v1

    .line 261
    .local v1, e2:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method

.method public static searchFoods(Ljava/lang/String;JJ)Ljava/lang/String;
    .locals 4
    .parameter "food"
    .parameter "limit"
    .parameter "offset"

    .prologue
    .line 119
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpClient;->getInstance()Lorg/medhelp/mydiet/http/MHHttpClient;

    move-result-object v2

    sput-object v2, Lorg/medhelp/mydiet/http/MHHttpHandler;->httpClient:Lorg/medhelp/mydiet/http/MHHttpClient;

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lorg/medhelp/mydiet/http/MHHttpHandler;->FOOD_SEARCH:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "?q="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&limit="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&offset="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, url:Ljava/lang/String;
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, post:Lorg/apache/http/client/methods/HttpPost;
    const-string v2, "Content-Type"

    const-string v3, "application/json"

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v2, "Accept"

    const-string v3, "application/json"

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-static {v0}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public signUp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .parameter "nickname"
    .parameter "email"
    .parameter "password"
    .parameter "passwordConfirmation"
    .parameter "captcha"

    .prologue
    .line 149
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    sget-object v5, Lorg/medhelp/mydiet/http/MHHttpHandler;->URL_SIGNUP:Ljava/lang/String;

    invoke-direct {v3, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 151
    .local v3, post:Lorg/apache/http/client/methods/HttpPost;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 152
    .local v4, postParameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "user[login]"

    invoke-direct {v5, v6, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "user[email]"

    invoke-direct {v5, v6, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "user[password]"

    invoke-direct {v5, v6, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "user[password_confirmation]"

    invoke-direct {v5, v6, p4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "verification_code"

    invoke-direct {v5, v6, p5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "version"

    const-string v7, "3"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    const/4 v1, 0x0

    .line 161
    .local v1, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_0
    new-instance v2, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v2, v4}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v1           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .local v2, formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_1
    invoke-virtual {v3, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    .line 167
    .end local v2           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v1       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :goto_0
    invoke-static {v3}, Lorg/medhelp/mydiet/http/MHHttpHandler;->doPost(Lorg/apache/http/client/methods/HttpPost;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, e1:Ljava/io/UnsupportedEncodingException;
    :goto_1
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 163
    .end local v0           #e1:Ljava/io/UnsupportedEncodingException;
    .end local v1           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v2       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2           #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .restart local v1       #formEntity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    goto :goto_1
.end method
