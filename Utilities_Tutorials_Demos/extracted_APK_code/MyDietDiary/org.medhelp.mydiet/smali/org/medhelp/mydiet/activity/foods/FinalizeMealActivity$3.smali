.class Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$3;
.super Ljava/lang/Object;
.source "FinalizeMealActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->onMealTypeClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 226
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$8(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, mealtype:Ljava/lang/String;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/medhelp/mydiet/model/Meal;->setMealType(Ljava/lang/String;)V

    .line 228
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mTVMealType:Landroid/widget/TextView;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$9(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v2

    invoke-virtual {v2}, Lorg/medhelp/mydiet/model/Meal;->getMealTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    return-void
.end method
