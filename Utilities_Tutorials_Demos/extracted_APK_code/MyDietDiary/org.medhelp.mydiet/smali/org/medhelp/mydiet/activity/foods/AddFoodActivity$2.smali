.class Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;
.super Ljava/lang/Object;
.source "AddFoodActivity.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    .line 435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private canRequestMoreItems()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 457
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchActive:Z
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 497
    :cond_0
    :goto_0
    return v0

    .line 462
    :cond_1
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mFoods:Ljava/util/ArrayList;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 470
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPage:I
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$2(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 479
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mPreviousPageItemsCount:I
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$3(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I

    move-result v1

    const/16 v2, 0x14

    if-lt v1, v2, :cond_0

    .line 487
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mIsLoadingItems:Z
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$4(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 492
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mCurrentResults:I
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)I

    move-result v1

    if-nez v1, :cond_0

    .line 497
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private loadMoreItems()V
    .locals 1

    .prologue
    .line 450
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->canRequestMoreItems()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->requestMoreItems()V

    .line 453
    :cond_0
    return-void
.end method

.method private requestMoreItems()V
    .locals 4

    .prologue
    .line 501
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    new-instance v1, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;)V

    #setter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;
    invoke-static {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$6(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;)V

    .line 502
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->this$0:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->mSearchTask:Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$7(Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;)Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;->access$8()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$SearchFoodsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 503
    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .parameter "view"
    .parameter "firstVisibleItem"
    .parameter "visibleItemCount"
    .parameter "totalItemCount"

    .prologue
    .line 443
    add-int v0, p2, p3

    .line 444
    .local v0, lastVisibleItem:I
    add-int/lit8 v1, p4, -0x1

    if-lt v0, v1, :cond_0

    .line 445
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity$2;->loadMoreItems()V

    .line 447
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .parameter "view"
    .parameter "scrollState"

    .prologue
    .line 438
    return-void
.end method
