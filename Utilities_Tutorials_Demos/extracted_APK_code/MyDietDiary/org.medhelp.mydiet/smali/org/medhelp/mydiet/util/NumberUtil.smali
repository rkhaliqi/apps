.class public Lorg/medhelp/mydiet/util/NumberUtil;
.super Ljava/lang/Object;
.source "NumberUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isValidDecimalNumber(Ljava/lang/String;)Z
    .locals 2
    .parameter "string"

    .prologue
    .line 7
    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 8
    :catch_0
    move-exception v0

    .line 9
    .local v0, e:Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValidInteger(Ljava/lang/String;)Z
    .locals 2
    .parameter "string"

    .prologue
    .line 16
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 17
    :catch_0
    move-exception v0

    .line 18
    .local v0, e:Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValidNonNegativeDecimalNumber(Ljava/lang/String;)Z
    .locals 6
    .parameter "string"

    .prologue
    const/4 v3, 0x0

    .line 25
    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 26
    .local v0, d:D
    const-wide/16 v4, 0x0

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_0

    .line 27
    const/4 v3, 0x1

    .line 32
    .end local v0           #d:D
    :cond_0
    :goto_0
    return v3

    .line 29
    :catch_0
    move-exception v2

    .line 30
    .local v2, e:Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public static isValidNonNegativeInteger(Ljava/lang/String;)Z
    .locals 6
    .parameter "string"

    .prologue
    const/4 v3, 0x0

    .line 37
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    .line 38
    .local v1, number:J
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-ltz v4, :cond_0

    .line 39
    const/4 v3, 0x1

    .line 44
    .end local v1           #number:J
    :cond_0
    :goto_0
    return v3

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_0
.end method
