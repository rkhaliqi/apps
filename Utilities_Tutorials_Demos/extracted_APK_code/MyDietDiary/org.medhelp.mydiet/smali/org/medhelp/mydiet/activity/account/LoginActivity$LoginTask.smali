.class Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;
.super Landroid/os/AsyncTask;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/account/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoginTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGIN_SUCCESS:I = 0x64


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 130
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;-><init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->doInBackground([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs declared-synchronized doInBackground([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .parameter "params"

    .prologue
    .line 146
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    aget-object v2, p1, v2

    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-static {v2, v3}, Lorg/medhelp/mydiet/http/MHHttpHandler;->login(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, response:Ljava/lang/String;
    const/4 v2, 0x3

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-object v3, p1, v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x1

    aget-object v3, p1, v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    .local v1, result:[Ljava/lang/String;
    monitor-exit p0

    return-object v1

    .line 146
    .end local v0           #response:Ljava/lang/String;
    .end local v1           #result:[Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->dismiss()V

    .line 165
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 166
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->onPostExecute([Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute([Ljava/lang/String;)V
    .locals 14
    .parameter "result"

    .prologue
    const/4 v11, 0x1

    const/4 v13, 0x0

    .line 170
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 172
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v9, v9, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 173
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const v10, 0x7f0b00d9

    invoke-virtual {v9, v10}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 174
    .local v7, tv:Landroid/widget/TextView;
    const/4 v9, 0x2

    aget-object v2, p1, v9

    .line 175
    .local v2, response:Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_2

    .line 176
    :cond_0
    const-string v9, "#FF0000"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 177
    const-string v9, "Login failed. Please check your network connection and try again"

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 238
    :goto_0
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v9, v9, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    if-eqz v9, :cond_1

    .line 239
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v9, v9, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    invoke-virtual {v9}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->dismiss()V

    .line 240
    :cond_1
    return-void

    .line 180
    :cond_2
    const/4 v4, 0x0

    .line 182
    .local v4, resultObject:Lorg/json/JSONObject;
    const-string v9, "#FF0000"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 184
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    .end local v4           #resultObject:Lorg/json/JSONObject;
    .local v5, resultObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v9, "response_code"

    const/4 v10, -0x1

    invoke-virtual {v5, v9, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    .line 187
    .local v3, responseCode:I
    const/4 v8, 0x0

    .line 190
    .local v8, userId:Ljava/lang/String;
    if-nez v3, :cond_3

    .line 193
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const-wide/16 v10, -0x1

    invoke-static {v9, v10, v11}, Lorg/medhelp/mydiet/util/SyncUtil;->setConfigsLastSyncTime(Landroid/content/Context;J)V

    .line 194
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-virtual {v9}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_CLEAR:Landroid/net/Uri;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v10, v11, v12}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 196
    const-string v9, "#91cb56"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 197
    const-string v9, "Login success."

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    const-string v9, "data"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/json/JSONObject;

    const-string v10, "user_id"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 201
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const/4 v10, 0x0

    aget-object v10, p1, v10

    invoke-static {v9, v10}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserName(Landroid/content/Context;Ljava/lang/String;)V

    .line 202
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const/4 v10, 0x1

    aget-object v10, p1, v10

    invoke-static {v9, v10}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setPassword(Landroid/content/Context;Ljava/lang/String;)V

    .line 203
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-static {v9, v8}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserId(Landroid/content/Context;Ljava/lang/String;)V

    .line 204
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const-string v10, "medhelp"

    invoke-static {v9, v10}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserType(Landroid/content/Context;Ljava/lang/String;)V

    .line 206
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Integer;

    const/4 v10, 0x0

    const/16 v11, 0x64

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v4, v5

    .line 235
    .end local v3           #responseCode:I
    .end local v5           #resultObject:Lorg/json/JSONObject;
    .end local v8           #userId:Ljava/lang/String;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    :goto_1
    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 207
    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v3       #responseCode:I
    .restart local v5       #resultObject:Lorg/json/JSONObject;
    .restart local v8       #userId:Ljava/lang/String;
    :cond_3
    if-ne v3, v11, :cond_4

    .line 210
    :try_start_2
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const-string v10, "mh_user"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 211
    .local v6, sharedPreferences:Landroid/content/SharedPreferences;
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 212
    .local v1, prefEditor:Landroid/content/SharedPreferences$Editor;
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const-string v10, ""

    invoke-static {v9, v10}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserName(Landroid/content/Context;Ljava/lang/String;)V

    .line 213
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const-string v10, ""

    invoke-static {v9, v10}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setPassword(Landroid/content/Context;Ljava/lang/String;)V

    .line 214
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const-string v10, ""

    invoke-static {v9, v10}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserId(Landroid/content/Context;Ljava/lang/String;)V

    .line 215
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    const-string v10, "none"

    invoke-static {v9, v10}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserType(Landroid/content/Context;Ljava/lang/String;)V

    .line 217
    const-string v9, "Login failed. Please check your username and password and try again"

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-object v4, v5

    .end local v5           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 220
    .end local v1           #prefEditor:Landroid/content/SharedPreferences$Editor;
    .end local v4           #resultObject:Lorg/json/JSONObject;
    .end local v6           #sharedPreferences:Landroid/content/SharedPreferences;
    .restart local v5       #resultObject:Lorg/json/JSONObject;
    :cond_4
    const/16 v9, -0xa

    if-ne v3, v9, :cond_5

    .line 221
    const-string v9, "Login failed. MedHelp server is down for maintainance. Please try logging in later. You may still use the application"

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v4, v5

    .end local v5           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 222
    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v5       #resultObject:Lorg/json/JSONObject;
    :cond_5
    const/16 v9, 0x385

    if-eq v3, v9, :cond_6

    const/16 v9, 0x386

    if-ne v3, v9, :cond_7

    .line 224
    :cond_6
    const-string v9, "Login failed. Cannot connect to MedHelp Server. Please check your network connection or try later. You may still use the application"

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v4, v5

    .end local v5           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 227
    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v5       #resultObject:Lorg/json/JSONObject;
    :cond_7
    const-string v9, "Login failed."

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v4, v5

    .end local v5           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 230
    .end local v3           #responseCode:I
    .end local v8           #userId:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 231
    .local v0, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 232
    const-string v9, "#FF0000"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 233
    const-string v9, "Login failed : invalid response"

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 230
    .end local v0           #e:Lorg/json/JSONException;
    .end local v4           #resultObject:Lorg/json/JSONObject;
    .restart local v5       #resultObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v4, v5

    .end local v5           #resultObject:Lorg/json/JSONObject;
    .restart local v4       #resultObject:Lorg/json/JSONObject;
    goto :goto_2
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    new-instance v1, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-direct {v1, v2, v3}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;-><init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;Landroid/content/Context;)V

    iput-object v1, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    .line 138
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    const-string v1, "Logging in"

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    const-string v1, "This may take only a few seconds depending on your network"

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->show()V

    .line 142
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 154
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 159
    :goto_0
    return-void

    .line 156
    :pswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    #calls: Lorg/medhelp/mydiet/activity/account/LoginActivity;->syncUserSettings()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->access$0(Lorg/medhelp/mydiet/activity/account/LoginActivity;)V

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
