.class Lorg/medhelp/mydiet/activity/SetupScreen3$9;
.super Ljava/lang/Object;
.source "SetupScreen3.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/SetupScreen3;->clickHeightUnits()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/SetupScreen3;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$9;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 390
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$9;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/activity/SetupScreen3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v0, v1, p2

    .line 391
    .local v0, units:Ljava/lang/String;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$9;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-static {v1, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightUnits(Landroid/content/Context;Ljava/lang/String;)V

    .line 392
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$9;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    #getter for: Lorg/medhelp/mydiet/activity/SetupScreen3;->btnHeightUnits:Landroid/widget/Button;
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/SetupScreen3;->access$4(Lorg/medhelp/mydiet/activity/SetupScreen3;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$9;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    invoke-static {v2}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 393
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$9;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    #calls: Lorg/medhelp/mydiet/activity/SetupScreen3;->updateHeightDisplay()V
    invoke-static {v1}, Lorg/medhelp/mydiet/activity/SetupScreen3;->access$3(Lorg/medhelp/mydiet/activity/SetupScreen3;)V

    .line 394
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/SetupScreen3$9;->this$0:Lorg/medhelp/mydiet/activity/SetupScreen3;

    iget-object v1, v1, Lorg/medhelp/mydiet/activity/SetupScreen3;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 395
    return-void
.end method
