.class Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;
.super Landroid/os/AsyncTask;
.source "TabTrackItActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/TabTrackItActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadWeightTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 839
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 839
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;-><init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->doInBackground([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 4
    .parameter "params"

    .prologue
    .line 852
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 853
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #getter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$1(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getRecentWeightOnOrBefore(Ljava/util/Date;)D

    move-result-wide v2

    iput-wide v2, v1, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mWeight:D

    .line 854
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 872
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->publishProgress([Ljava/lang/Object;)V

    .line 873
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #calls: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshWeightContent()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$12(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    .line 874
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 875
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 846
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 847
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->publishProgress([Ljava/lang/Object;)V

    .line 848
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 859
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 860
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 868
    :goto_0
    return-void

    .line 862
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #calls: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->showProgressBar()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$3(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    goto :goto_0

    .line 865
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #calls: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->hideProgressBar()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$5(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    goto :goto_0

    .line 860
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
