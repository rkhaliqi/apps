.class Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;
.super Landroid/os/AsyncTask;
.source "MealsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/MealsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteMealTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 299
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;-><init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->doInBackground([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 4
    .parameter "params"

    .prologue
    .line 312
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 313
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDate:Ljava/util/Date;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->deleteMeal(JLjava/util/Date;)V

    .line 314
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 332
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->publishProgress([Ljava/lang/Object;)V

    .line 333
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/MealsActivity;->refreshMealsContent()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->access$3(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V

    .line 334
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 335
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 306
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 307
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->publishProgress([Ljava/lang/Object;)V

    .line 308
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 319
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 320
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 328
    :goto_0
    return-void

    .line 322
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/MealsActivity;->showAlertDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V

    goto :goto_0

    .line 325
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/MealsActivity;->hideAlertDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->access$2(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V

    goto :goto_0

    .line 320
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
