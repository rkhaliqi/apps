.class public interface abstract Lorg/medhelp/mydiet/C$jsonKeys;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "jsonKeys"
.end annotation


# static fields
.field public static final CCR_CALORIE_COEFFICIENT:Ljava/lang/String; = "calorie_coefficient"

.field public static final CCR_MAX_SPEED:Ljava/lang/String; = "max"

.field public static final CCR_MIN_SPEED:Ljava/lang/String; = "min"

.field public static final EI_CALORIES:Ljava/lang/String; = "calories"

.field public static final EI_DATE:Ljava/lang/String; = "date"

.field public static final EI_DISTANCE:Ljava/lang/String; = "distance"

.field public static final EI_EXERCISE_TYPE:Ljava/lang/String; = "exercise_type"

.field public static final EI_IN_APP_EXERCISE_ID:Ljava/lang/String; = "in_app_exercise_id"

.field public static final EI_IN_APP_ID:Ljava/lang/String; = "in_app_id"

.field public static final EI_MEDHELP_ID:Ljava/lang/String; = "medhelp_id"

.field public static final EI_NAME:Ljava/lang/String; = "name"

.field public static final EI_SPEED:Ljava/lang/String; = "speed"

.field public static final EI_TIME:Ljava/lang/String; = "time"

.field public static final EI_TIME_PERIOD:Ljava/lang/String; = "time_period"

.field public static final ET_CALORIE_COEFFICIENT:Ljava/lang/String; = "calorie_coefficient"

.field public static final ET_NAME:Ljava/lang/String; = "name"

.field public static final EXERCISE_CC:Ljava/lang/String; = "calorie_coefficient"

.field public static final EXERCISE_CC_RANGES:Ljava/lang/String; = "calorie_coefficient_ranges"

.field public static final EXERCISE_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final EXERCISE_HAS_DISTANCE:Ljava/lang/String; = "has_distance"

.field public static final EXERCISE_ITEM:Ljava/lang/String; = "exercise_item"

.field public static final EXERCISE_MAX_DISTANCE:Ljava/lang/String; = "distance_max"

.field public static final EXERCISE_MAX_SPEED:Ljava/lang/String; = "speed_max"

.field public static final EXERCISE_NAME:Ljava/lang/String; = "name"

.field public static final EXERCISE_TYPES:Ljava/lang/String; = "types"

.field public static final EXTERNAL_ID:Ljava/lang/String; = "external_id"

.field public static final FOOD:Ljava/lang/String; = "food"

.field public static final FOOD_BIOTIN:Ljava/lang/String; = "biotin"

.field public static final FOOD_BRAND:Ljava/lang/String; = "brand"

.field public static final FOOD_CALCIUM:Ljava/lang/String; = "calcium"

.field public static final FOOD_CALORIES:Ljava/lang/String; = "calories"

.field public static final FOOD_CALORIES_FROM_FAT:Ljava/lang/String; = "calories_from_fat"

.field public static final FOOD_CHLORIDE:Ljava/lang/String; = "chloride"

.field public static final FOOD_CHOLESTEROL:Ljava/lang/String; = "cholesterol"

.field public static final FOOD_CHROMIUM:Ljava/lang/String; = "chromium"

.field public static final FOOD_COPPER:Ljava/lang/String; = "copper"

.field public static final FOOD_DESCRIPTION:Ljava/lang/String; = "item_description"

.field public static final FOOD_DIETARY_FIBER:Ljava/lang/String; = "fiber"

.field public static final FOOD_EQUIVALENT_UNIT_TYPE:Ljava/lang/String; = "equivalent_unit_type"

.field public static final FOOD_FLUORIDE:Ljava/lang/String; = "fluoride"

.field public static final FOOD_FOLATE:Ljava/lang/String; = "folate"

.field public static final FOOD_GROUP:Ljava/lang/String; = "food_group"

.field public static final FOOD_GROUP_ID:Ljava/lang/String; = "food_group_id"

.field public static final FOOD_IODINE:Ljava/lang/String; = "iodine"

.field public static final FOOD_IRON:Ljava/lang/String; = "iron"

.field public static final FOOD_MAGNESIUM:Ljava/lang/String; = "magnesium"

.field public static final FOOD_MANGANESE:Ljava/lang/String; = "manganese"

.field public static final FOOD_MANUFACTURER:Ljava/lang/String; = "manufacturer"

.field public static final FOOD_MEDHELP_ID:Ljava/lang/String; = "id"

.field public static final FOOD_MOLYBDENUM:Ljava/lang/String; = "molybdenum"

.field public static final FOOD_MONO_UNSATURATED_FAT:Ljava/lang/String; = "total_monounsaturated_fatty_acids"

.field public static final FOOD_NAME:Ljava/lang/String; = "name"

.field public static final FOOD_NIACIN:Ljava/lang/String; = "niacin"

.field public static final FOOD_PANTOTHENIC_ACID:Ljava/lang/String; = "pantothenic_acid"

.field public static final FOOD_PHOSPHORUS:Ljava/lang/String; = "phosphorus"

.field public static final FOOD_POTASSIUM:Ljava/lang/String; = "potassium"

.field public static final FOOD_PROTEIN:Ljava/lang/String; = "protein"

.field public static final FOOD_RIBOFLAVIN:Ljava/lang/String; = "riboflavin"

.field public static final FOOD_SATURATED_FAT:Ljava/lang/String; = "saturated_fatty_acids"

.field public static final FOOD_SELENIUM:Ljava/lang/String; = "selenium"

.field public static final FOOD_SERVINGS:Ljava/lang/String; = "food_servings"

.field public static final FOOD_SODIUM:Ljava/lang/String; = "sodium"

.field public static final FOOD_STATUS:Ljava/lang/String; = "status"

.field public static final FOOD_SUGARS:Ljava/lang/String; = "total_sugars"

.field public static final FOOD_THIAMIN:Ljava/lang/String; = "thiamin"

.field public static final FOOD_TOTAL_CARBOHYDRATE:Ljava/lang/String; = "carbohydrate"

.field public static final FOOD_TOTAL_FAT:Ljava/lang/String; = "total_fat"

.field public static final FOOD_TRANS_FAT:Ljava/lang/String; = "total_trans_fatty_acids"

.field public static final FOOD_UPDATED_AT:Ljava/lang/String; = "updated_at"

.field public static final FOOD_VITAMIN_A:Ljava/lang/String; = "vitamin_a"

.field public static final FOOD_VITAMIN_B12:Ljava/lang/String; = "vitamin_b12"

.field public static final FOOD_VITAMIN_B6:Ljava/lang/String; = "vitamin_b6"

.field public static final FOOD_VITAMIN_C:Ljava/lang/String; = "vitamin_c"

.field public static final FOOD_VITAMIN_D:Ljava/lang/String; = "vitamin_d"

.field public static final FOOD_VITAMIN_E:Ljava/lang/String; = "vitamin_e"

.field public static final FOOD_VITAMIN_K:Ljava/lang/String; = "vitamin_k"

.field public static final FOOD_ZINC:Ljava/lang/String; = "zinc"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IN_APP_ID:Ljava/lang/String; = "in_app_id"

.field public static final LOGIN_DATA:Ljava/lang/String; = "data"

.field public static final LOGIN_RESPONSE_CODE:Ljava/lang/String; = "response_code"

.field public static final LOGIN_USER_ID:Ljava/lang/String; = "user_id"

.field public static final MEAL:Ljava/lang/String; = "meal"

.field public static final MEAL_CALORIES:Ljava/lang/String; = "calories"

.field public static final MEAL_FOODS:Ljava/lang/String; = "foods"

.field public static final MEAL_FOOD_AMOUNT:Ljava/lang/String; = "amount"

.field public static final MEAL_FOOD_CALORIES:Ljava/lang/String; = "food_calories"

.field public static final MEAL_FOOD_DESCRIPTION:Ljava/lang/String; = "food_description"

.field public static final MEAL_FOOD_ID:Ljava/lang/String; = "food_id"

.field public static final MEAL_FOOD_IS_SELECTED:Ljava/lang/String; = "is_selected"

.field public static final MEAL_FOOD_NAME:Ljava/lang/String; = "food_name"

.field public static final MEAL_FOOD_SERVING_ID:Ljava/lang/String; = "serving_id"

.field public static final MEAL_FOOD_SUMMARY:Ljava/lang/String; = "food_summary"

.field public static final MEAL_MEAL_ID:Ljava/lang/String; = "meal_id"

.field public static final MEAL_MEAL_TYPE:Ljava/lang/String; = "meal_type"

.field public static final MEAL_NOTES:Ljava/lang/String; = "notes"

.field public static final MEAL_SUMMARY:Ljava/lang/String; = "summary"

.field public static final MEAL_TIME:Ljava/lang/String; = "meal_time"

.field public static final MEAL_USER_INPUT_CALORIES:Ljava/lang/String; = "user_input_calories"

.field public static final MEAL_WHERE:Ljava/lang/String; = "where"

.field public static final MEAL_WITH_WHOM:Ljava/lang/String; = "with_whom"

.field public static final MEDHELP_ID:Ljava/lang/String; = "medhelp_id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final SERVING_AMOUNT:Ljava/lang/String; = "amount"

.field public static final SERVING_EQUIVALENT:Ljava/lang/String; = "equivalent"

.field public static final SERVING_FOOD_ID:Ljava/lang/String; = "food_id"

.field public static final SERVING_NAME:Ljava/lang/String; = "name"

.field public static final SERVING_PRIORITY:Ljava/lang/String; = "priority"

.field public static final SOURCE_MEDHELP_VERIFIED:Ljava/lang/String; = "mh_approved_foods"

.field public static final SOURCE_USER_OTHER:Ljava/lang/String; = "user_foods"

.field public static final SOURCE_USER_SELF:Ljava/lang/String; = "my_foods"

.field public static final WATER:Ljava/lang/String; = "water"

.field public static final WEIGHT:Ljava/lang/String; = "weight"
