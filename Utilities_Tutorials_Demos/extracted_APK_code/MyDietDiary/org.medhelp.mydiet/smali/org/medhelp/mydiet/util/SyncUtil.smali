.class public Lorg/medhelp/mydiet/util/SyncUtil;
.super Ljava/lang/Object;
.source "SyncUtil.java"

# interfaces
.implements Lorg/medhelp/mydiet/C$userSyncJsonKeys;


# static fields
.field private static SYNC_THRESHHOLD_MINUTES:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-wide/16 v0, 0x14

    sput-wide v0, Lorg/medhelp/mydiet/util/SyncUtil;->SYNC_THRESHHOLD_MINUTES:J

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static deleteDayMealsIfNecessary(Landroid/content/Context;JLorg/json/JSONArray;J)V
    .locals 18
    .parameter "c"
    .parameter "currentSyncStartTime"
    .parameter "itemsJSONArray"
    .parameter "dateTimeInMillis"

    .prologue
    .line 1326
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-nez v12, :cond_1

    .line 1368
    :cond_0
    :goto_0
    return-void

    .line 1328
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lorg/json/JSONArray;->length()I

    move-result v11

    .line 1330
    .local v11, size:I
    const/4 v7, 0x0

    .line 1331
    .local v7, mealItem:Lorg/json/JSONObject;
    const/4 v8, 0x0

    .line 1332
    .local v8, mealObject:Lorg/json/JSONObject;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1334
    .local v10, mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1335
    .local v3, cv:Landroid/content/ContentValues;
    const/4 v5, 0x0

    .line 1337
    .local v5, hasValues:Z
    const/4 v6, 0x0

    .local v6, i:I
    :goto_1
    if-lt v6, v11, :cond_2

    .line 1365
    if-eqz v5, :cond_0

    .line 1366
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    sget-object v13, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_MEALS:Landroid/net/Uri;

    const-string v14, "date = ?"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    move-wide/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v12, v13, v3, v14, v15}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 1339
    :cond_2
    :try_start_0
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 1340
    const-string v12, "Meal"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 1342
    if-eqz v8, :cond_3

    .line 1344
    const-string v12, "meal_type"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1345
    .local v9, mealType:Ljava/lang/String;
    const-string v12, "meal_type"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1346
    const-string v12, "Breakfast"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1347
    const/4 v5, 0x1

    .line 1348
    const-string v12, "breakfast"

    invoke-virtual {v3, v12}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1337
    .end local v9           #mealType:Ljava/lang/String;
    :cond_3
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1349
    .restart local v9       #mealType:Ljava/lang/String;
    :cond_4
    const-string v12, "Lunch"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1350
    const/4 v5, 0x1

    .line 1351
    const-string v12, "lunch"

    invoke-virtual {v3, v12}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1360
    .end local v9           #mealType:Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 1361
    .local v4, e:Lorg/json/JSONException;
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 1352
    .end local v4           #e:Lorg/json/JSONException;
    .restart local v9       #mealType:Ljava/lang/String;
    :cond_5
    :try_start_1
    const-string v12, "Dinner"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1353
    const/4 v5, 0x1

    .line 1354
    const-string v12, "dinner"

    invoke-virtual {v3, v12}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_2

    .line 1355
    :cond_6
    const-string v12, "Snack"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1356
    const/4 v5, 0x1

    .line 1357
    const-string v12, "snacks"

    invoke-virtual {v3, v12}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private static deleteExerciseItemsForDay(Landroid/content/Context;JLorg/json/JSONArray;Ljava/lang/String;)V
    .locals 17
    .parameter "c"
    .parameter "currentSyncStartTime"
    .parameter "itemsJSONArray"
    .parameter "dateString"

    .prologue
    .line 1038
    if-nez p3, :cond_1

    .line 1076
    :cond_0
    return-void

    .line 1039
    :cond_1
    const-string v9, "yyyy-MM-dd"

    move-object/from16 v0, p4

    invoke-static {v0, v9}, Lorg/medhelp/mydiet/util/DateUtil;->getDateFromString(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 1041
    .local v4, keyDate:Ljava/util/Date;
    invoke-virtual/range {p3 .. p3}, Lorg/json/JSONArray;->length()I

    move-result v7

    .line 1043
    .local v7, size:I
    const/4 v2, 0x0

    .line 1044
    .local v2, exercisesItem:Lorg/json/JSONObject;
    const/4 v6, 0x0

    .line 1045
    .local v6, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1047
    .local v5, keyList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    if-lt v3, v7, :cond_2

    .line 1067
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1068
    if-eqz v7, :cond_0

    .line 1070
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v7, :cond_0

    .line 1071
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    .line 1072
    sget-object v11, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISE_ITEMS:Landroid/net/Uri;

    .line 1073
    const-string v12, "date = ? AND medhelp_id =  ?"

    .line 1074
    const/4 v9, 0x2

    new-array v13, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v9

    const/4 v14, 0x1

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v13, v14

    .line 1071
    invoke-virtual {v10, v11, v12, v13}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1070
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1049
    :cond_2
    :try_start_0
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 1051
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v6

    .line 1053
    if-eqz v6, :cond_4

    .line 1054
    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_5

    .line 1047
    :cond_4
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1055
    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1056
    .local v8, str:Ljava/lang/String;
    if-eqz v8, :cond_3

    const-string v9, "updated_at"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1057
    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1062
    .end local v8           #str:Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1063
    .local v1, e:Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3
.end method

.method public static getConfigsLastSyncTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 35
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 36
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "configs_last_sync_time"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getExercisesLastSyncTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 85
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 86
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "last_sync_exercises"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static declared-synchronized getExercisesToSync(Landroid/content/Context;JJJ)Lorg/json/JSONObject;
    .locals 52
    .parameter "c"
    .parameter "startDate"
    .parameter "endDate"
    .parameter "lastSyncTime"

    .prologue
    .line 741
    const-class v49, Lorg/medhelp/mydiet/util/SyncUtil;

    monitor-enter v49

    :try_start_0
    new-instance v48, Lorg/json/JSONObject;

    invoke-direct/range {v48 .. v48}, Lorg/json/JSONObject;-><init>()V

    .line 743
    .local v48, userDataObject:Lorg/json/JSONObject;
    const-string v9, "date"

    .line 744
    .local v9, _DATE:Ljava/lang/String;
    const-string v13, "medhelp_id"

    .line 745
    .local v13, _EI_MEDHELP_KEY:Ljava/lang/String;
    const-string v17, "exercise_type"

    .line 746
    .local v17, _EI_TYPE:Ljava/lang/String;
    const-string v11, "distance"

    .line 747
    .local v11, _EI_DISTANCE:Ljava/lang/String;
    const-string v14, "speed"

    .line 748
    .local v14, _EI_SPEED:Ljava/lang/String;
    const-string v15, "time"

    .line 749
    .local v15, _EI_TIME:Ljava/lang/String;
    const-string v16, "time_period"

    .line 750
    .local v16, _EI_TIME_PERIOD:Ljava/lang/String;
    const-string v10, "calories"

    .line 751
    .local v10, _EI_CALORIES:Ljava/lang/String;
    const-string v12, "last_updated_time"

    .line 753
    .local v12, _EI_LAST_UPDATED_AT:Ljava/lang/String;
    const-string v6, "SELECT  ei.date AS date,  ei.medhelp_id AS medhelp_id,  ei.exercise_type AS exercise_type,  ei.distance AS distance,  ei.speed AS speed,  ei.time AS time,  ei.time_period AS time_period,  ei.calories AS calories,  ei.last_updated_time AS last_updated_time FROM exercise_items AS ei  INNER JOIN exercises AS e ON ei.medhelp_id = e.medhelp_id WHERE ei.date > ? AND ei.date < ? AND ei.medhelp_id IN ( SELECT DISTINCT medhelp_id  FROM exercise_items  WHERE date > ? AND date < ? AND last_updated_time > ?)  ORDER BY  ei.date, ei.medhelp_id;"

    .line 771
    .local v6, exerciseSql:Ljava/lang/String;
    const/4 v3, 0x5

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {p5 .. p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    .line 774
    .local v7, selectionArgs:[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISE_ITEMS_TO_SYNC:Landroid/net/Uri;

    const/4 v5, 0x0

    .line 775
    const/4 v8, 0x0

    .line 774
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 777
    .local v23, cursor:Landroid/database/Cursor;
    const/16 v29, 0x0

    .line 779
    .local v29, eItem:Lorg/json/JSONObject;
    if-eqz v23, :cond_3

    .line 780
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 781
    const/16 v37, 0x0

    .line 782
    .local v37, exerciseType:Ljava/lang/String;
    const-wide/16 v40, -0x3e7

    .line 783
    .local v40, noEntryTime:J
    const-wide/16 v42, -0x3e7

    .line 784
    .local v42, previousDateTime:J
    const-wide/16 v18, 0x0

    .line 785
    .local v18, currentDateTime:J
    const/16 v24, 0x0

    .line 786
    .local v24, dayDataObject:Lorg/json/JSONObject;
    const/16 v26, 0x0

    .line 787
    .local v26, dayExercisesObject:Lorg/json/JSONObject;
    const/16 v31, 0x0

    .line 788
    .local v31, eiArray:Lorg/json/JSONArray;
    const-wide/16 v33, 0x0

    .line 789
    .local v33, exerciseTime:J
    const-wide/16 v38, -0x1

    .line 790
    .local v38, lastUpdatedAt:J
    const-string v20, ""

    .line 791
    .local v20, currentEID:Ljava/lang/String;
    const-string v44, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 792
    .local v44, previousEID:Ljava/lang/String;
    const/16 v45, 0x0

    .local v45, previousExerciseKey:Ljava/lang/String;
    move-object/from16 v32, v31

    .end local v31           #eiArray:Lorg/json/JSONArray;
    .local v32, eiArray:Lorg/json/JSONArray;
    move-object/from16 v27, v26

    .end local v26           #dayExercisesObject:Lorg/json/JSONObject;
    .local v27, dayExercisesObject:Lorg/json/JSONObject;
    move-object/from16 v25, v24

    .end local v24           #dayDataObject:Lorg/json/JSONObject;
    .local v25, dayDataObject:Lorg/json/JSONObject;
    move-object/from16 v30, v29

    .line 796
    .end local v29           #eItem:Lorg/json/JSONObject;
    .local v30, eItem:Lorg/json/JSONObject;
    :goto_0
    :try_start_1
    const-string v3, "date"

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 797
    const-string v3, "medhelp_id"

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 799
    new-instance v29, Lorg/json/JSONObject;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONObject;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 800
    .end local v30           #eItem:Lorg/json/JSONObject;
    .restart local v29       #eItem:Lorg/json/JSONObject;
    :try_start_2
    const-string v3, "calories"

    const-string v4, "calories"

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 801
    const-string v3, "distance"

    const-string v4, "distance"

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 802
    const-string v3, "speed"

    const-string v4, "speed"

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 803
    const-string v3, "time"

    const-string v4, "time_period"

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 804
    const-string v3, "exercise_type"

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 805
    if-eqz v37, :cond_0

    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 806
    const-string v3, "type"

    move-object/from16 v0, v29

    move-object/from16 v1, v37

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 809
    :cond_0
    const-string v3, "time"

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v33

    .line 810
    new-instance v3, Ljava/util/Date;

    move-wide/from16 v0, v33

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v46

    .line 811
    .local v46, timeAtMidnight:J
    sub-long v3, v33, v46

    const-wide/16 v50, 0x3e8

    div-long v35, v3, v50

    .line 813
    .local v35, exerciseTimeOfDay:J
    const-string v3, "exercise_time"

    move-object/from16 v0, v29

    move-wide/from16 v1, v35

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 815
    cmp-long v3, v18, v42

    if-nez v3, :cond_6

    .line 816
    const-string v3, "last_updated_time"

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    .line 817
    .local v21, currentItemLastUpdatedAt:J
    cmp-long v3, v21, v38

    if-lez v3, :cond_1

    .line 818
    move-wide/from16 v38, v21

    .line 823
    :cond_1
    move-object/from16 v0, v20

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 825
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v31, v32

    .line 837
    .end local v32           #eiArray:Lorg/json/JSONArray;
    .restart local v31       #eiArray:Lorg/json/JSONArray;
    :goto_1
    :try_start_3
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v44

    .line 838
    const-string v3, "medhelp_id"

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v45

    move-object/from16 v26, v27

    .end local v27           #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v26       #dayExercisesObject:Lorg/json/JSONObject;
    move-object/from16 v24, v25

    .line 868
    .end local v21           #currentItemLastUpdatedAt:J
    .end local v25           #dayDataObject:Lorg/json/JSONObject;
    .restart local v24       #dayDataObject:Lorg/json/JSONObject;
    :goto_2
    :try_start_4
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    .line 795
    if-nez v3, :cond_9

    .line 873
    if-eqz v31, :cond_2

    invoke-virtual/range {v31 .. v31}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 874
    if-eqz v45, :cond_2

    invoke-virtual/range {v45 .. v45}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 875
    move-object/from16 v0, v26

    move-object/from16 v1, v45

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 876
    const-string v3, "data"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 877
    const-string v3, "updated_at"

    .line 878
    new-instance v4, Ljava/util/Date;

    move-wide/from16 v0, v38

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 879
    const-string v5, "yyyy-MM-dd HH:mm:ss"

    .line 877
    invoke-static {v4, v5}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 880
    new-instance v3, Ljava/util/Date;

    move-wide/from16 v0, v42

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    const-string v4, "yyyy-MM-dd"

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    .line 887
    .end local v18           #currentDateTime:J
    .end local v20           #currentEID:Ljava/lang/String;
    .end local v24           #dayDataObject:Lorg/json/JSONObject;
    .end local v26           #dayExercisesObject:Lorg/json/JSONObject;
    .end local v31           #eiArray:Lorg/json/JSONArray;
    .end local v33           #exerciseTime:J
    .end local v35           #exerciseTimeOfDay:J
    .end local v37           #exerciseType:Ljava/lang/String;
    .end local v38           #lastUpdatedAt:J
    .end local v40           #noEntryTime:J
    .end local v42           #previousDateTime:J
    .end local v44           #previousEID:Ljava/lang/String;
    .end local v45           #previousExerciseKey:Ljava/lang/String;
    .end local v46           #timeAtMidnight:J
    :cond_2
    :goto_3
    :try_start_5
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 890
    :cond_3
    monitor-exit v49

    return-object v48

    .line 828
    .restart local v18       #currentDateTime:J
    .restart local v20       #currentEID:Ljava/lang/String;
    .restart local v21       #currentItemLastUpdatedAt:J
    .restart local v25       #dayDataObject:Lorg/json/JSONObject;
    .restart local v27       #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v32       #eiArray:Lorg/json/JSONArray;
    .restart local v33       #exerciseTime:J
    .restart local v35       #exerciseTimeOfDay:J
    .restart local v37       #exerciseType:Ljava/lang/String;
    .restart local v38       #lastUpdatedAt:J
    .restart local v40       #noEntryTime:J
    .restart local v42       #previousDateTime:J
    .restart local v44       #previousEID:Ljava/lang/String;
    .restart local v45       #previousExerciseKey:Ljava/lang/String;
    .restart local v46       #timeAtMidnight:J
    :cond_4
    if-eqz v32, :cond_5

    :try_start_6
    invoke-virtual/range {v32 .. v32}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_5

    .line 829
    move-object/from16 v0, v27

    move-object/from16 v1, v45

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 833
    :cond_5
    new-instance v31, Lorg/json/JSONArray;

    invoke-direct/range {v31 .. v31}, Lorg/json/JSONArray;-><init>()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_2

    .line 834
    .end local v32           #eiArray:Lorg/json/JSONArray;
    .restart local v31       #eiArray:Lorg/json/JSONArray;
    :try_start_7
    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_1

    .line 883
    .end local v21           #currentItemLastUpdatedAt:J
    :catch_0
    move-exception v28

    move-object/from16 v26, v27

    .end local v27           #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v26       #dayExercisesObject:Lorg/json/JSONObject;
    move-object/from16 v24, v25

    .line 884
    .end local v25           #dayDataObject:Lorg/json/JSONObject;
    .end local v35           #exerciseTimeOfDay:J
    .end local v46           #timeAtMidnight:J
    .restart local v24       #dayDataObject:Lorg/json/JSONObject;
    .local v28, e:Lorg/json/JSONException;
    :goto_4
    :try_start_8
    invoke-virtual/range {v28 .. v28}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 741
    .end local v6           #exerciseSql:Ljava/lang/String;
    .end local v7           #selectionArgs:[Ljava/lang/String;
    .end local v9           #_DATE:Ljava/lang/String;
    .end local v10           #_EI_CALORIES:Ljava/lang/String;
    .end local v11           #_EI_DISTANCE:Ljava/lang/String;
    .end local v12           #_EI_LAST_UPDATED_AT:Ljava/lang/String;
    .end local v13           #_EI_MEDHELP_KEY:Ljava/lang/String;
    .end local v14           #_EI_SPEED:Ljava/lang/String;
    .end local v15           #_EI_TIME:Ljava/lang/String;
    .end local v16           #_EI_TIME_PERIOD:Ljava/lang/String;
    .end local v17           #_EI_TYPE:Ljava/lang/String;
    .end local v18           #currentDateTime:J
    .end local v20           #currentEID:Ljava/lang/String;
    .end local v23           #cursor:Landroid/database/Cursor;
    .end local v24           #dayDataObject:Lorg/json/JSONObject;
    .end local v26           #dayExercisesObject:Lorg/json/JSONObject;
    .end local v28           #e:Lorg/json/JSONException;
    .end local v29           #eItem:Lorg/json/JSONObject;
    .end local v31           #eiArray:Lorg/json/JSONArray;
    .end local v33           #exerciseTime:J
    .end local v37           #exerciseType:Ljava/lang/String;
    .end local v38           #lastUpdatedAt:J
    .end local v40           #noEntryTime:J
    .end local v42           #previousDateTime:J
    .end local v44           #previousEID:Ljava/lang/String;
    .end local v45           #previousExerciseKey:Ljava/lang/String;
    .end local v48           #userDataObject:Lorg/json/JSONObject;
    :catchall_0
    move-exception v3

    monitor-exit v49

    throw v3

    .line 842
    .restart local v6       #exerciseSql:Ljava/lang/String;
    .restart local v7       #selectionArgs:[Ljava/lang/String;
    .restart local v9       #_DATE:Ljava/lang/String;
    .restart local v10       #_EI_CALORIES:Ljava/lang/String;
    .restart local v11       #_EI_DISTANCE:Ljava/lang/String;
    .restart local v12       #_EI_LAST_UPDATED_AT:Ljava/lang/String;
    .restart local v13       #_EI_MEDHELP_KEY:Ljava/lang/String;
    .restart local v14       #_EI_SPEED:Ljava/lang/String;
    .restart local v15       #_EI_TIME:Ljava/lang/String;
    .restart local v16       #_EI_TIME_PERIOD:Ljava/lang/String;
    .restart local v17       #_EI_TYPE:Ljava/lang/String;
    .restart local v18       #currentDateTime:J
    .restart local v20       #currentEID:Ljava/lang/String;
    .restart local v23       #cursor:Landroid/database/Cursor;
    .restart local v25       #dayDataObject:Lorg/json/JSONObject;
    .restart local v27       #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v29       #eItem:Lorg/json/JSONObject;
    .restart local v32       #eiArray:Lorg/json/JSONArray;
    .restart local v33       #exerciseTime:J
    .restart local v35       #exerciseTimeOfDay:J
    .restart local v37       #exerciseType:Ljava/lang/String;
    .restart local v38       #lastUpdatedAt:J
    .restart local v40       #noEntryTime:J
    .restart local v42       #previousDateTime:J
    .restart local v44       #previousEID:Ljava/lang/String;
    .restart local v45       #previousExerciseKey:Ljava/lang/String;
    .restart local v46       #timeAtMidnight:J
    .restart local v48       #userDataObject:Lorg/json/JSONObject;
    :cond_6
    if-eqz v45, :cond_8

    :try_start_9
    invoke-virtual/range {v45 .. v45}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_8

    .line 844
    if-eqz v32, :cond_7

    invoke-virtual/range {v32 .. v32}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_7

    .line 845
    move-object/from16 v0, v27

    move-object/from16 v1, v45

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 846
    const-string v3, "data"

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 847
    const-string v3, "updated_at"

    .line 848
    new-instance v4, Ljava/util/Date;

    move-wide/from16 v0, v38

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 849
    const-string v5, "yyyy-MM-dd HH:mm:ss"

    .line 847
    invoke-static {v4, v5}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 852
    :cond_7
    new-instance v3, Ljava/util/Date;

    move-wide/from16 v0, v42

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    const-string v4, "yyyy-MM-dd"

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v48

    move-object/from16 v1, v25

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 854
    :cond_8
    const-string v3, "last_updated_time"

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v38

    .line 857
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v44

    .line 859
    new-instance v31, Lorg/json/JSONArray;

    invoke-direct/range {v31 .. v31}, Lorg/json/JSONArray;-><init>()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_2

    .line 860
    .end local v32           #eiArray:Lorg/json/JSONArray;
    .restart local v31       #eiArray:Lorg/json/JSONArray;
    :try_start_a
    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 862
    new-instance v24, Lorg/json/JSONObject;

    invoke-direct/range {v24 .. v24}, Lorg/json/JSONObject;-><init>()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_0

    .line 863
    .end local v25           #dayDataObject:Lorg/json/JSONObject;
    .restart local v24       #dayDataObject:Lorg/json/JSONObject;
    :try_start_b
    new-instance v26, Lorg/json/JSONObject;

    invoke-direct/range {v26 .. v26}, Lorg/json/JSONObject;-><init>()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_3

    .line 864
    .end local v27           #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v26       #dayExercisesObject:Lorg/json/JSONObject;
    move-wide/from16 v42, v18

    .line 865
    :try_start_c
    const-string v3, "medhelp_id"

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_4

    move-result-object v45

    goto/16 :goto_2

    .line 883
    .end local v24           #dayDataObject:Lorg/json/JSONObject;
    .end local v26           #dayExercisesObject:Lorg/json/JSONObject;
    .end local v29           #eItem:Lorg/json/JSONObject;
    .end local v31           #eiArray:Lorg/json/JSONArray;
    .end local v35           #exerciseTimeOfDay:J
    .end local v46           #timeAtMidnight:J
    .restart local v25       #dayDataObject:Lorg/json/JSONObject;
    .restart local v27       #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v30       #eItem:Lorg/json/JSONObject;
    .restart local v32       #eiArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v28

    move-object/from16 v31, v32

    .end local v32           #eiArray:Lorg/json/JSONArray;
    .restart local v31       #eiArray:Lorg/json/JSONArray;
    move-object/from16 v26, v27

    .end local v27           #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v26       #dayExercisesObject:Lorg/json/JSONObject;
    move-object/from16 v24, v25

    .end local v25           #dayDataObject:Lorg/json/JSONObject;
    .restart local v24       #dayDataObject:Lorg/json/JSONObject;
    move-object/from16 v29, v30

    .end local v30           #eItem:Lorg/json/JSONObject;
    .restart local v29       #eItem:Lorg/json/JSONObject;
    goto/16 :goto_4

    .end local v24           #dayDataObject:Lorg/json/JSONObject;
    .end local v26           #dayExercisesObject:Lorg/json/JSONObject;
    .end local v31           #eiArray:Lorg/json/JSONArray;
    .restart local v25       #dayDataObject:Lorg/json/JSONObject;
    .restart local v27       #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v32       #eiArray:Lorg/json/JSONArray;
    :catch_2
    move-exception v28

    move-object/from16 v31, v32

    .end local v32           #eiArray:Lorg/json/JSONArray;
    .restart local v31       #eiArray:Lorg/json/JSONArray;
    move-object/from16 v26, v27

    .end local v27           #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v26       #dayExercisesObject:Lorg/json/JSONObject;
    move-object/from16 v24, v25

    .end local v25           #dayDataObject:Lorg/json/JSONObject;
    .restart local v24       #dayDataObject:Lorg/json/JSONObject;
    goto/16 :goto_4

    .end local v26           #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v27       #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v35       #exerciseTimeOfDay:J
    .restart local v46       #timeAtMidnight:J
    :catch_3
    move-exception v28

    move-object/from16 v26, v27

    .end local v27           #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v26       #dayExercisesObject:Lorg/json/JSONObject;
    goto/16 :goto_4

    :catch_4
    move-exception v28

    goto/16 :goto_4

    :cond_9
    move-object/from16 v32, v31

    .end local v31           #eiArray:Lorg/json/JSONArray;
    .restart local v32       #eiArray:Lorg/json/JSONArray;
    move-object/from16 v27, v26

    .end local v26           #dayExercisesObject:Lorg/json/JSONObject;
    .restart local v27       #dayExercisesObject:Lorg/json/JSONObject;
    move-object/from16 v25, v24

    .end local v24           #dayDataObject:Lorg/json/JSONObject;
    .restart local v25       #dayDataObject:Lorg/json/JSONObject;
    move-object/from16 v30, v29

    .end local v29           #eItem:Lorg/json/JSONObject;
    .restart local v30       #eItem:Lorg/json/JSONObject;
    goto/16 :goto_0
.end method

.method public static getFoodItemInMealFromSync(Landroid/content/Context;Ljava/lang/String;I)Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .locals 23
    .parameter "c"
    .parameter "foodItemJSON"
    .parameter "uniqueId"

    .prologue
    .line 1565
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const/4 v13, 0x0

    .line 1610
    :goto_0
    return-object v13

    .line 1567
    :cond_1
    const/4 v14, 0x0

    .line 1568
    .local v14, foodItemFromSync:Lorg/json/JSONObject;
    new-instance v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    invoke-direct {v13}, Lorg/medhelp/mydiet/model/FoodItemInMeal;-><init>()V

    .line 1571
    .local v13, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    :try_start_0
    new-instance v15, Lorg/json/JSONObject;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1573
    .end local v14           #foodItemFromSync:Lorg/json/JSONObject;
    .local v15, foodItemFromSync:Lorg/json/JSONObject;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    const/4 v5, 0x0

    .line 1574
    const-string v6, "medhelp_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "id"

    const-wide/16 v21, -0x1

    move-object/from16 v0, v20

    move-wide/from16 v1, v21

    invoke-virtual {v15, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v20

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v7, v8

    const/4 v8, 0x0

    .line 1573
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1576
    .local v9, cursor:Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 1577
    .local v16, foodJSON:Ljava/lang/String;
    const/16 v18, 0x0

    .line 1578
    .local v18, servingsJSON:Ljava/lang/String;
    const-wide/16 v11, 0x0

    .line 1580
    .local v11, foodInAppId:J
    if-eqz v9, :cond_2

    .line 1581
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1582
    const-string v3, "food"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1583
    const-string v3, "servings"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 1584
    const-string v3, "_id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 1588
    :cond_2
    new-instance v17, Lorg/json/JSONObject;

    invoke-direct/range {v17 .. v17}, Lorg/json/JSONObject;-><init>()V

    .line 1589
    .local v17, foodObject:Lorg/json/JSONObject;
    const-string v3, "food"

    new-instance v4, Lorg/json/JSONObject;

    move-object/from16 v0, v16

    invoke-direct {v4, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1590
    const-string v3, "food_servings"

    new-instance v4, Lorg/json/JSONArray;

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1592
    if-eqz v9, :cond_3

    .line 1593
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1596
    :cond_3
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1597
    const-string v4, "serving_id"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1598
    const-string v6, "amount"

    const-wide/high16 v7, 0x3ff0

    invoke-virtual {v15, v6, v7, v8}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v6

    const/4 v8, 0x0

    .line 1596
    invoke-static/range {v3 .. v8}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemFromJSON(Ljava/lang/String;JDLjava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v13

    .line 1600
    move/from16 v0, p2

    int-to-long v3, v0

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 1601
    const-string v3, "id"

    const-wide/16 v4, -0x1

    invoke-virtual {v15, v3, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    .line 1602
    const-string v3, "serving_id"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 1603
    const-string v3, "amount"

    const-wide/high16 v4, 0x3ff0

    invoke-virtual {v15, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    iput-wide v3, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 1604
    iput-wide v11, v13, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v14, v15

    .end local v15           #foodItemFromSync:Lorg/json/JSONObject;
    .restart local v14       #foodItemFromSync:Lorg/json/JSONObject;
    goto/16 :goto_0

    .line 1606
    .end local v9           #cursor:Landroid/database/Cursor;
    .end local v11           #foodInAppId:J
    .end local v16           #foodJSON:Ljava/lang/String;
    .end local v17           #foodObject:Lorg/json/JSONObject;
    .end local v18           #servingsJSON:Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 1607
    .local v10, e:Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 1606
    .end local v10           #e:Lorg/json/JSONException;
    .end local v14           #foodItemFromSync:Lorg/json/JSONObject;
    .restart local v15       #foodItemFromSync:Lorg/json/JSONObject;
    :catch_1
    move-exception v10

    move-object v14, v15

    .end local v15           #foodItemFromSync:Lorg/json/JSONObject;
    .restart local v14       #foodItemFromSync:Lorg/json/JSONObject;
    goto :goto_1
.end method

.method public static getFoodItemsInMealFromSync(Landroid/content/Context;Lorg/json/JSONArray;)Ljava/util/ArrayList;
    .locals 6
    .parameter "c"
    .parameter "foodItemsInMealJSON"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1545
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    const/4 v1, 0x0

    .line 1561
    :cond_1
    :goto_0
    return-object v1

    .line 1547
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1549
    .local v1, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    const/4 v2, 0x0

    .line 1552
    .local v2, foodObject:Lorg/json/JSONObject;
    :try_start_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 1553
    .local v4, length:I
    const/4 v3, 0x0

    .local v3, i:I
    :goto_1
    if-ge v3, v4, :cond_1

    .line 1554
    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 1555
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->getFoodItemInMealFromSync(Landroid/content/Context;Ljava/lang/String;I)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1553
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1557
    .end local v3           #i:I
    .end local v4           #length:I
    :catch_0
    move-exception v0

    .line 1558
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getMealFromServerMeal(Landroid/content/Context;Ljava/lang/String;J)Lorg/medhelp/mydiet/model/Meal;
    .locals 8
    .parameter "c"
    .parameter "mealJSON"
    .parameter "mealDateTime"

    .prologue
    .line 1522
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 1541
    :goto_0
    return-object v1

    .line 1524
    :cond_1
    const/4 v2, 0x0

    .line 1525
    .local v2, mealsObject:Lorg/json/JSONObject;
    new-instance v1, Lorg/medhelp/mydiet/model/Meal;

    invoke-direct {v1}, Lorg/medhelp/mydiet/model/Meal;-><init>()V

    .line 1528
    .local v1, meal:Lorg/medhelp/mydiet/model/Meal;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1530
    .end local v2           #mealsObject:Lorg/json/JSONObject;
    .local v3, mealsObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v4, "meal_type"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/medhelp/mydiet/model/Meal;->setMealType(Ljava/lang/String;)V

    .line 1531
    const-string v4, "foods"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    invoke-static {p0, v4}, Lorg/medhelp/mydiet/util/SyncUtil;->getFoodItemsInMealFromSync(Landroid/content/Context;Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    .line 1532
    const-wide/16 v4, 0x3e8

    const-string v6, "meal_time"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    mul-long/2addr v4, v6

    add-long/2addr v4, p2

    iput-wide v4, v1, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    .line 1533
    const-string v4, "quick_note"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    .line 1534
    const-string v4, "location"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    .line 1535
    const-string v4, "company_type"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .end local v3           #mealsObject:Lorg/json/JSONObject;
    .restart local v2       #mealsObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 1537
    :catch_0
    move-exception v0

    .line 1538
    .local v0, e:Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 1537
    .end local v0           #e:Lorg/json/JSONException;
    .end local v2           #mealsObject:Lorg/json/JSONObject;
    .restart local v3       #mealsObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3           #mealsObject:Lorg/json/JSONObject;
    .restart local v2       #mealsObject:Lorg/json/JSONObject;
    goto :goto_1
.end method

.method private static getMealJSONToSync(Lorg/medhelp/mydiet/model/Meal;)Lorg/json/JSONObject;
    .locals 17
    .parameter "meal"

    .prologue
    .line 1162
    if-nez p0, :cond_1

    .line 1163
    const/4 v7, 0x0

    .line 1211
    :cond_0
    :goto_0
    return-object v7

    .line 1164
    :cond_1
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 1166
    .local v7, mealObject:Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    .line 1168
    .local v3, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    const/4 v4, 0x0

    .line 1171
    .local v4, foodObject:Lorg/json/JSONObject;
    :try_start_0
    const-string v12, "meal_type"

    invoke-virtual/range {p0 .. p0}, Lorg/medhelp/mydiet/model/Meal;->getMealTypeString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1172
    const-string v12, "company_type"

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/medhelp/mydiet/model/Meal;->withWhom:Ljava/lang/String;

    invoke-virtual {v7, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1173
    const-string v12, "location"

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/medhelp/mydiet/model/Meal;->where:Ljava/lang/String;

    invoke-virtual {v7, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1175
    new-instance v12, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-wide v13, v0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    invoke-direct {v12, v13, v14}, Ljava/util/Date;-><init>(J)V

    invoke-static {v12}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v10

    .line 1176
    .local v10, timeAtMidnight:J
    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    sub-long/2addr v12, v10

    const-wide/16 v14, 0x3e8

    div-long v8, v12, v14

    .line 1177
    .local v8, mealTimeOfDay:J
    const-string v12, "meal_time"

    invoke-virtual {v7, v12, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1179
    const-string v12, "user_input_calories"

    move-object/from16 v0, p0

    iget-wide v13, v0, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    invoke-virtual {v7, v12, v13, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 1180
    const-string v12, "quick_note"

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    invoke-virtual {v7, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1182
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lez v12, :cond_0

    .line 1184
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 1186
    .local v6, foodsArray:Lorg/json/JSONArray;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    move-object v5, v4

    .end local v4           #foodObject:Lorg/json/JSONObject;
    .local v5, foodObject:Lorg/json/JSONObject;
    :goto_1
    :try_start_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_2

    .line 1204
    const-string v12, "foods"

    invoke-virtual {v7, v12, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-object v4, v5

    .end local v5           #foodObject:Lorg/json/JSONObject;
    .restart local v4       #foodObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 1186
    .end local v4           #foodObject:Lorg/json/JSONObject;
    .restart local v5       #foodObject:Lorg/json/JSONObject;
    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 1187
    .local v2, fi:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1189
    .end local v5           #foodObject:Lorg/json/JSONObject;
    .restart local v4       #foodObject:Lorg/json/JSONObject;
    :try_start_2
    const-string v13, "serving_id"

    iget-wide v14, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    invoke-virtual {v4, v13, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1190
    const-string v13, "amount"

    iget-wide v14, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    invoke-virtual {v4, v13, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 1191
    const-string v13, "id"

    iget-wide v14, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    invoke-virtual {v4, v13, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1192
    const-string v13, "in_app_id"

    iget-wide v14, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    invoke-virtual {v4, v13, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1199
    iget-wide v13, v2, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    const-wide/16 v15, 0x0

    cmp-long v13, v13, v15

    if-lez v13, :cond_3

    .line 1200
    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_3
    move-object v5, v4

    .end local v4           #foodObject:Lorg/json/JSONObject;
    .restart local v5       #foodObject:Lorg/json/JSONObject;
    goto :goto_1

    .line 1207
    .end local v2           #fi:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v5           #foodObject:Lorg/json/JSONObject;
    .end local v6           #foodsArray:Lorg/json/JSONArray;
    .end local v8           #mealTimeOfDay:J
    .end local v10           #timeAtMidnight:J
    .restart local v4       #foodObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 1208
    .local v1, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 1207
    .end local v1           #e:Lorg/json/JSONException;
    .end local v4           #foodObject:Lorg/json/JSONObject;
    .restart local v5       #foodObject:Lorg/json/JSONObject;
    .restart local v6       #foodsArray:Lorg/json/JSONArray;
    .restart local v8       #mealTimeOfDay:J
    .restart local v10       #timeAtMidnight:J
    :catch_1
    move-exception v1

    move-object v4, v5

    .end local v5           #foodObject:Lorg/json/JSONObject;
    .restart local v4       #foodObject:Lorg/json/JSONObject;
    goto :goto_2
.end method

.method public static getMealsLastSyncTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 67
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 68
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "last_sync_meals"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getMealsToSync(Landroid/content/Context;JJJ)Lorg/json/JSONObject;
    .locals 23
    .parameter "c"
    .parameter "startDate"
    .parameter "endDate"
    .parameter "lastSyncTime"

    .prologue
    .line 1079
    new-instance v20, Lorg/json/JSONObject;

    invoke-direct/range {v20 .. v20}, Lorg/json/JSONObject;-><init>()V

    .line 1081
    .local v20, userDataObject:Lorg/json/JSONObject;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_MEALS:Landroid/net/Uri;

    const/4 v3, 0x0

    .line 1082
    const-string v4, "last_updated_time>=? AND date>=? AND date<? "

    .line 1083
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static/range {p5 .. p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v5, v6

    const/4 v6, 0x1

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v5, v6

    const/4 v6, 0x2

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v5, v6

    const-string v6, "date"

    .line 1081
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1085
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_6

    .line 1086
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1087
    const/4 v10, 0x0

    .line 1088
    .local v10, dateString:Ljava/lang/String;
    const/4 v8, 0x0

    .line 1089
    .local v8, date:Ljava/util/Date;
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    .line 1090
    .local v13, entryObject:Lorg/json/JSONObject;
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 1092
    .local v11, dayDataObject:Lorg/json/JSONObject;
    new-instance v18, Lorg/json/JSONArray;

    invoke-direct/range {v18 .. v18}, Lorg/json/JSONArray;-><init>()V

    .line 1094
    .local v18, mealsArray:Lorg/json/JSONArray;
    const/16 v16, 0x0

    .line 1095
    .local v16, mealString:Ljava/lang/String;
    const/4 v15, 0x0

    .line 1097
    .local v15, meal:Lorg/medhelp/mydiet/model/Meal;
    new-instance v19, Lorg/json/JSONObject;

    invoke-direct/range {v19 .. v19}, Lorg/json/JSONObject;-><init>()V

    .line 1100
    .local v19, tempMeal:Lorg/json/JSONObject;
    :cond_0
    new-instance v18, Lorg/json/JSONArray;

    .end local v18           #mealsArray:Lorg/json/JSONArray;
    invoke-direct/range {v18 .. v18}, Lorg/json/JSONArray;-><init>()V

    .line 1102
    .restart local v18       #mealsArray:Lorg/json/JSONArray;
    new-instance v13, Lorg/json/JSONObject;

    .end local v13           #entryObject:Lorg/json/JSONObject;
    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    .line 1103
    .restart local v13       #entryObject:Lorg/json/JSONObject;
    new-instance v11, Lorg/json/JSONObject;

    .end local v11           #dayDataObject:Lorg/json/JSONObject;
    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 1104
    .restart local v11       #dayDataObject:Lorg/json/JSONObject;
    new-instance v8, Ljava/util/Date;

    .end local v8           #date:Ljava/util/Date;
    const-string v1, "date"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-direct {v8, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 1105
    .restart local v8       #date:Ljava/util/Date;
    const-string v1, "yyyy-MM-dd"

    invoke-static {v8, v1}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1107
    const-string v1, "breakfast"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1109
    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 1110
    invoke-static/range {v16 .. v16}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v15

    .line 1111
    invoke-static {v15}, Lorg/medhelp/mydiet/util/SyncUtil;->getMealJSONToSync(Lorg/medhelp/mydiet/model/Meal;)Lorg/json/JSONObject;

    move-result-object v19

    .line 1112
    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1115
    :cond_1
    const-string v1, "lunch"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1117
    if-eqz v16, :cond_2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 1118
    invoke-static/range {v16 .. v16}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v15

    .line 1119
    invoke-static {v15}, Lorg/medhelp/mydiet/util/SyncUtil;->getMealJSONToSync(Lorg/medhelp/mydiet/model/Meal;)Lorg/json/JSONObject;

    move-result-object v19

    .line 1120
    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1123
    :cond_2
    const-string v1, "dinner"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1125
    if-eqz v16, :cond_3

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 1126
    invoke-static/range {v16 .. v16}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v15

    .line 1127
    invoke-static {v15}, Lorg/medhelp/mydiet/util/SyncUtil;->getMealJSONToSync(Lorg/medhelp/mydiet/model/Meal;)Lorg/json/JSONObject;

    move-result-object v19

    .line 1128
    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1131
    :cond_3
    const-string v1, "snacks"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1133
    invoke-static/range {v16 .. v16}, Lorg/medhelp/mydiet/util/DataUtil;->getMeals(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    .line 1135
    .local v17, meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    if-eqz v17, :cond_4

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 1136
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1143
    :cond_4
    :try_start_0
    const-string v1, "Meal"

    move-object/from16 v0, v18

    invoke-virtual {v13, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1144
    const-string v1, "data"

    invoke-virtual {v11, v1, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1146
    new-instance v9, Ljava/util/Date;

    const-string v1, "last_updated_time"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-direct {v9, v1, v2}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1147
    .end local v8           #date:Ljava/util/Date;
    .local v9, date:Ljava/util/Date;
    :try_start_1
    const-string v1, "updated_at"

    const-string v2, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v9, v2}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1149
    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v8, v9

    .line 1154
    .end local v9           #date:Ljava/util/Date;
    .restart local v8       #date:Ljava/util/Date;
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    .line 1099
    if-nez v1, :cond_0

    .line 1156
    .end local v8           #date:Ljava/util/Date;
    .end local v10           #dateString:Ljava/lang/String;
    .end local v11           #dayDataObject:Lorg/json/JSONObject;
    .end local v13           #entryObject:Lorg/json/JSONObject;
    .end local v15           #meal:Lorg/medhelp/mydiet/model/Meal;
    .end local v16           #mealString:Ljava/lang/String;
    .end local v17           #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    .end local v18           #mealsArray:Lorg/json/JSONArray;
    .end local v19           #tempMeal:Lorg/json/JSONObject;
    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1158
    :cond_6
    return-object v20

    .line 1136
    .restart local v8       #date:Ljava/util/Date;
    .restart local v10       #dateString:Ljava/lang/String;
    .restart local v11       #dayDataObject:Lorg/json/JSONObject;
    .restart local v13       #entryObject:Lorg/json/JSONObject;
    .restart local v15       #meal:Lorg/medhelp/mydiet/model/Meal;
    .restart local v16       #mealString:Ljava/lang/String;
    .restart local v17       #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    .restart local v18       #mealsArray:Lorg/json/JSONArray;
    .restart local v19       #tempMeal:Lorg/json/JSONObject;
    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/medhelp/mydiet/model/Meal;

    .line 1137
    .local v14, m:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v14}, Lorg/medhelp/mydiet/util/SyncUtil;->getMealJSONToSync(Lorg/medhelp/mydiet/model/Meal;)Lorg/json/JSONObject;

    move-result-object v19

    .line 1138
    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 1150
    .end local v14           #m:Lorg/medhelp/mydiet/model/Meal;
    :catch_0
    move-exception v12

    .line 1151
    .local v12, e:Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 1150
    .end local v8           #date:Ljava/util/Date;
    .end local v12           #e:Lorg/json/JSONException;
    .restart local v9       #date:Ljava/util/Date;
    :catch_1
    move-exception v12

    move-object v8, v9

    .end local v9           #date:Ljava/util/Date;
    .restart local v8       #date:Ljava/util/Date;
    goto :goto_2
.end method

.method private static getMissingFoodIds(Landroid/content/Context;Lorg/json/JSONArray;)Lorg/json/JSONArray;
    .locals 17
    .parameter "c"
    .parameter "mealsJSONArray"

    .prologue
    .line 1390
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/16 v16, 0x0

    .line 1429
    :cond_1
    return-object v16

    .line 1392
    :cond_2
    const/4 v7, 0x0

    .line 1394
    .local v7, cursor:Landroid/database/Cursor;
    new-instance v16, Lorg/json/JSONArray;

    invoke-direct/range {v16 .. v16}, Lorg/json/JSONArray;-><init>()V

    .line 1395
    .local v16, missingFoodIds:Lorg/json/JSONArray;
    const/4 v15, 0x0

    .line 1397
    .local v15, mealObject:Lorg/json/JSONObject;
    const/4 v9, 0x0

    .line 1398
    .local v9, foodItemsArray:Lorg/json/JSONArray;
    const/4 v2, 0x0

    .line 1400
    .local v2, tempUri:Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONArray;->length()I

    move-result v13

    .line 1401
    .local v13, itemsLength:I
    const/4 v12, 0x0

    .local v12, i:I
    :goto_0
    if-ge v12, v13, :cond_1

    .line 1403
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v3, "Meal"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v15

    .line 1404
    if-nez v15, :cond_4

    .line 1401
    :cond_3
    :goto_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 1407
    :cond_4
    const-string v1, "foods"

    invoke-virtual {v15, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 1408
    if-eqz v9, :cond_3

    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-eqz v1, :cond_3

    .line 1411
    const/4 v14, 0x0

    .local v14, j:I
    :goto_2
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v14, v1, :cond_3

    .line 1412
    invoke-virtual {v9, v14}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v3, "id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 1413
    .local v10, foodMedHelpId:J
    const-wide/16 v3, 0x0

    cmp-long v1, v10, v3

    if-lez v1, :cond_7

    .line 1414
    sget-object v1, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOOD_BY_MH_ID:Landroid/net/Uri;

    invoke-static {v1, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 1415
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1416
    if-eqz v7, :cond_5

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1417
    :cond_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v10, v11}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    .line 1419
    :cond_6
    if-eqz v7, :cond_7

    .line 1420
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1411
    :cond_7
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 1424
    .end local v10           #foodMedHelpId:J
    .end local v14           #j:I
    :catch_0
    move-exception v8

    .line 1425
    .local v8, e:Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getSyncExercisesStatus(Landroid/content/Context;)Z
    .locals 3
    .parameter "c"

    .prologue
    const/4 v2, 0x0

    .line 80
    const-string v1, "mh_user"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 81
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "sync_exercises_active"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getSyncMealsStatus(Landroid/content/Context;)Z
    .locals 3
    .parameter "c"

    .prologue
    const/4 v2, 0x0

    .line 62
    const-string v1, "mh_user"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 63
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "sync_meals_active"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getSyncWaterStatus(Landroid/content/Context;)Z
    .locals 3
    .parameter "c"

    .prologue
    const/4 v2, 0x0

    .line 98
    const-string v1, "mh_user"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 99
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "sync_water_active"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getSyncWeightStatus(Landroid/content/Context;)Z
    .locals 3
    .parameter "c"

    .prologue
    const/4 v2, 0x0

    .line 116
    const-string v1, "mh_user"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 117
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "sync_weight_active"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getUserConfigsToSync(Landroid/content/Context;)Lorg/json/JSONObject;
    .locals 26
    .parameter "c"

    .prologue
    .line 126
    const/4 v5, 0x0

    .line 127
    .local v5, config:Ljava/lang/String;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 128
    .local v6, configDataObject:Lorg/json/JSONObject;
    new-instance v20, Lorg/json/JSONArray;

    invoke-direct/range {v20 .. v20}, Lorg/json/JSONArray;-><init>()V

    .line 129
    .local v20, supportedConfigs:Lorg/json/JSONArray;
    const/4 v7, 0x0

    .line 130
    .local v7, configObject:Lorg/json/JSONObject;
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/SyncUtil;->getConfigsLastSyncTime(Landroid/content/Context;)J

    move-result-wide v9

    .line 133
    .local v9, configsLastSyncTime:J
    :try_start_0
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGenderUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 134
    .local v17, lastUpdatedTime:J
    const-string v23, "gender"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 135
    cmp-long v23, v17, v9

    if-ltz v23, :cond_13

    .line 136
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 138
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    .end local v7           #configObject:Lorg/json/JSONObject;
    .local v8, configObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 140
    const-string v23, "data"

    const/16 v24, 0x0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v24

    sget-object v25, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 141
    const-string v23, "gender"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 144
    :goto_0
    const/4 v4, 0x0

    .line 145
    .local v4, cal:Ljava/util/Calendar;
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getBirthdayUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 146
    const-string v23, "birthday"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 147
    const-wide/16 v23, 0x0

    cmp-long v23, v17, v23

    if-ltz v23, :cond_0

    cmp-long v23, v17, v9

    if-ltz v23, :cond_0

    .line 148
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 149
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_2
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 150
    const-string v23, "data"

    const/16 v24, 0x0

    const/16 v25, 0x0

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v24

    sget-object v25, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 152
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDateOfBirth(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v4

    .line 153
    const-string v23, "yyyy-MM-dd"

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 154
    const-string v23, "data"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 155
    const-string v23, "birthday"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v8, v7

    .line 158
    .end local v7           #configObject:Lorg/json/JSONObject;
    .restart local v8       #configObject:Lorg/json/JSONObject;
    :cond_0
    :try_start_3
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 159
    const-string v23, "last_height"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 160
    const-wide/16 v23, 0x0

    cmp-long v23, v17, v23

    if-ltz v23, :cond_3

    cmp-long v23, v17, v9

    if-ltz v23, :cond_3

    .line 161
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightInInches(Landroid/content/Context;)F

    move-result v23

    move/from16 v0, v23

    float-to-double v13, v0

    .line 162
    .local v13, height:D
    const-wide/16 v23, 0x0

    cmpg-double v23, v13, v23

    if-gez v23, :cond_1

    const-wide/16 v13, 0x0

    .line 163
    :cond_1
    const-wide/high16 v23, 0x4059

    cmpl-double v23, v13, v23

    if-lez v23, :cond_2

    const-wide/high16 v13, 0x4059

    .line 165
    :cond_2
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 166
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_4
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 167
    const-string v23, "data"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v13, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 168
    const-string v23, "last_height"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    move-object v8, v7

    .line 171
    .end local v7           #configObject:Lorg/json/JSONObject;
    .end local v13           #height:D
    .restart local v8       #configObject:Lorg/json/JSONObject;
    :cond_3
    :try_start_5
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 172
    const-string v23, "last_weight"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 173
    const-wide/16 v23, 0x0

    cmp-long v23, v17, v23

    if-ltz v23, :cond_6

    cmp-long v23, v17, v9

    if-ltz v23, :cond_6

    .line 174
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v23

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v21, v0

    .line 175
    .local v21, weight:D
    const-wide/16 v23, 0x0

    cmpg-double v23, v21, v23

    if-gez v23, :cond_4

    const-wide/16 v21, 0x0

    .line 176
    :cond_4
    const-wide v23, 0x4097700000000000L

    cmpl-double v23, v21, v23

    if-lez v23, :cond_5

    const-wide v21, 0x4097700000000000L

    .line 178
    :cond_5
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    .line 179
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_6
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 180
    const-string v23, "data"

    move-object/from16 v0, v23

    move-wide/from16 v1, v21

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 181
    const-string v23, "last_weight"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0

    move-object v8, v7

    .line 184
    .end local v7           #configObject:Lorg/json/JSONObject;
    .end local v21           #weight:D
    .restart local v8       #configObject:Lorg/json/JSONObject;
    :cond_6
    :try_start_7
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getActivityLevelUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 185
    const-string v23, "activity_level"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 186
    const-wide/16 v23, 0x0

    cmp-long v23, v17, v23

    if-ltz v23, :cond_7

    cmp-long v23, v17, v9

    if-ltz v23, :cond_7

    .line 187
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getActivityLevel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 189
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_1

    .line 190
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_8
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 191
    const-string v23, "data"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 192
    const-string v23, "activity_level"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_0

    move-object v8, v7

    .line 195
    .end local v7           #configObject:Lorg/json/JSONObject;
    .restart local v8       #configObject:Lorg/json/JSONObject;
    :cond_7
    :try_start_9
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnitsUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 196
    const-string v23, "unit_volume"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 197
    const-wide/16 v23, 0x0

    cmp-long v23, v17, v23

    if-ltz v23, :cond_8

    cmp-long v23, v17, v9

    if-ltz v23, :cond_8

    .line 198
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWaterUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 199
    const-string v23, "fl oz"

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 200
    const-string v5, "fluid_ounces"

    .line 207
    :goto_1
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_1

    .line 208
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_a
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 209
    const-string v23, "data"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 210
    const-string v23, "unit_volume"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_0

    move-object v8, v7

    .line 213
    .end local v7           #configObject:Lorg/json/JSONObject;
    .restart local v8       #configObject:Lorg/json/JSONObject;
    :cond_8
    :try_start_b
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightUnitsUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 214
    const-string v23, "unit_length"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 215
    const-wide/16 v23, 0x0

    cmp-long v23, v17, v23

    if-ltz v23, :cond_9

    cmp-long v23, v17, v9

    if-ltz v23, :cond_9

    .line 216
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 217
    const-string v23, "cm"

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 218
    const-string v5, "centimeters"

    .line 223
    :goto_2
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_1

    .line 224
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_c
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 225
    const-string v23, "data"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 226
    const-string v23, "unit_length"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_c
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_0

    move-object v8, v7

    .line 229
    .end local v7           #configObject:Lorg/json/JSONObject;
    .restart local v8       #configObject:Lorg/json/JSONObject;
    :cond_9
    :try_start_d
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnitsUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 230
    const-string v23, "unit_weight"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 231
    const-wide/16 v23, 0x0

    cmp-long v23, v17, v23

    if-ltz v23, :cond_a

    cmp-long v23, v17, v9

    if-ltz v23, :cond_a

    .line 232
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 233
    const-string v23, "kg"

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_10

    .line 234
    const-string v5, "kilograms"

    .line 239
    :goto_3
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_d
    .catch Lorg/json/JSONException; {:try_start_d .. :try_end_d} :catch_1

    .line 240
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_e
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 241
    const-string v23, "data"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 242
    const-string v23, "unit_weight"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_e
    .catch Lorg/json/JSONException; {:try_start_e .. :try_end_e} :catch_0

    move-object v8, v7

    .line 245
    .end local v7           #configObject:Lorg/json/JSONObject;
    .restart local v8       #configObject:Lorg/json/JSONObject;
    :cond_a
    :try_start_f
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDistanceUnitsUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 246
    const-string v23, "unit_distance"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 247
    const-wide/16 v23, 0x0

    cmp-long v23, v17, v23

    if-ltz v23, :cond_b

    cmp-long v23, v17, v9

    if-ltz v23, :cond_b

    .line 248
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDistanceUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 249
    const-string v23, "km"

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_11

    .line 250
    const-string v5, "kilometers"

    .line 255
    :goto_4
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_f
    .catch Lorg/json/JSONException; {:try_start_f .. :try_end_f} :catch_1

    .line 256
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_10
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 257
    const-string v23, "data"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 258
    const-string v23, "unit_distance"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_10
    .catch Lorg/json/JSONException; {:try_start_10 .. :try_end_10} :catch_0

    move-object v8, v7

    .line 261
    .end local v7           #configObject:Lorg/json/JSONObject;
    .restart local v8       #configObject:Lorg/json/JSONObject;
    :cond_b
    :try_start_11
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getAvatarUpdatedTime(Landroid/content/Context;)J

    move-result-wide v17

    .line 262
    const-string v23, "avatar"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 263
    const-wide/16 v23, 0x0

    cmp-long v23, v17, v23

    if-ltz v23, :cond_c

    cmp-long v23, v17, v9

    if-ltz v23, :cond_c

    .line 264
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 265
    .local v12, gender:Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getAvatarPosition(Landroid/content/Context;)I

    move-result v3

    .line 266
    .local v3, avatarPosition:I
    const/16 v19, -0x1

    .line 268
    .local v19, medhelpAvatarPosition:I
    const-string v23, "Male"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 269
    sget-object v23, Lorg/medhelp/mydiet/C$arrays;->maleAvatarPositionOnMedHelp:[I

    aget v19, v23, v3

    .line 274
    :goto_5
    if-lez v19, :cond_c

    .line 275
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_11
    .catch Lorg/json/JSONException; {:try_start_11 .. :try_end_11} :catch_1

    .line 276
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_12
    const-string v23, "updated_at"

    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 277
    const-string v23, "data"

    move-object/from16 v0, v23

    move/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 278
    const-string v23, "avatar"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_12
    .catch Lorg/json/JSONException; {:try_start_12 .. :try_end_12} :catch_0

    move-object v8, v7

    .line 281
    .end local v3           #avatarPosition:I
    .end local v7           #configObject:Lorg/json/JSONObject;
    .end local v12           #gender:Ljava/lang/String;
    .end local v19           #medhelpAvatarPosition:I
    .restart local v8       #configObject:Lorg/json/JSONObject;
    :cond_c
    :try_start_13
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V
    :try_end_13
    .catch Lorg/json/JSONException; {:try_start_13 .. :try_end_13} :catch_1

    .line 282
    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :try_start_14
    const-string v23, "supported_config_keys"

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 283
    const-string v23, "config_data"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 284
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getConfigsLastSyncTime(Landroid/content/Context;)J

    move-result-wide v15

    .line 285
    .local v15, lastConfigsSyncTime:J
    const-string v23, "last_sync"

    .line 286
    new-instance v24, Ljava/util/Date;

    move-object/from16 v0, v24

    move-wide v1, v15

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-string v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static/range {v24 .. v25}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 285
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_14
    .catch Lorg/json/JSONException; {:try_start_14 .. :try_end_14} :catch_0

    .line 292
    .end local v4           #cal:Ljava/util/Calendar;
    .end local v15           #lastConfigsSyncTime:J
    .end local v17           #lastUpdatedTime:J
    :goto_6
    return-object v7

    .line 201
    .end local v7           #configObject:Lorg/json/JSONObject;
    .restart local v4       #cal:Ljava/util/Calendar;
    .restart local v8       #configObject:Lorg/json/JSONObject;
    .restart local v17       #lastUpdatedTime:J
    :cond_d
    :try_start_15
    const-string v23, "mL"

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 202
    const-string v5, "milliliters"

    goto/16 :goto_1

    .line 204
    :cond_e
    const-string v5, "glasses"

    goto/16 :goto_1

    .line 220
    :cond_f
    const-string v5, "inches"

    goto/16 :goto_2

    .line 236
    :cond_10
    const-string v5, "pounds"

    goto/16 :goto_3

    .line 252
    :cond_11
    const-string v5, "miles"

    goto/16 :goto_4

    .line 271
    .restart local v3       #avatarPosition:I
    .restart local v12       #gender:Ljava/lang/String;
    .restart local v19       #medhelpAvatarPosition:I
    :cond_12
    sget-object v23, Lorg/medhelp/mydiet/C$arrays;->femaleAvatarPositionOnMedHelp:[I

    aget v19, v23, v3
    :try_end_15
    .catch Lorg/json/JSONException; {:try_start_15 .. :try_end_15} :catch_1

    goto/16 :goto_5

    .line 288
    .end local v3           #avatarPosition:I
    .end local v4           #cal:Ljava/util/Calendar;
    .end local v8           #configObject:Lorg/json/JSONObject;
    .end local v12           #gender:Ljava/lang/String;
    .end local v17           #lastUpdatedTime:J
    .end local v19           #medhelpAvatarPosition:I
    .restart local v7       #configObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v11

    .line 289
    .local v11, e:Lorg/json/JSONException;
    :goto_7
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_6

    .line 288
    .end local v7           #configObject:Lorg/json/JSONObject;
    .end local v11           #e:Lorg/json/JSONException;
    .restart local v8       #configObject:Lorg/json/JSONObject;
    .restart local v17       #lastUpdatedTime:J
    :catch_1
    move-exception v11

    move-object v7, v8

    .end local v8           #configObject:Lorg/json/JSONObject;
    .restart local v7       #configObject:Lorg/json/JSONObject;
    goto :goto_7

    :cond_13
    move-object v8, v7

    .end local v7           #configObject:Lorg/json/JSONObject;
    .restart local v8       #configObject:Lorg/json/JSONObject;
    goto/16 :goto_0
.end method

.method public static getWaterLastSyncTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 103
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 104
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "last_sync_water"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getWaterToSync(Landroid/content/Context;JJJ)Lorg/json/JSONObject;
    .locals 18
    .parameter "c"
    .parameter "startDate"
    .parameter "endDate"
    .parameter "lastSyncTime"

    .prologue
    .line 466
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    .line 468
    .local v13, userDataObject:Lorg/json/JSONObject;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WATER:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 469
    const-string v3, "last_updated_time>=? AND date>=? AND date<? "

    .line 470
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static/range {p5 .. p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v4, v5

    const/4 v5, 0x1

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v4, v5

    const/4 v5, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v4, v5

    const-string v5, "date"

    .line 468
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 472
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 473
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 474
    const/4 v9, 0x0

    .line 475
    .local v9, dateString:Ljava/lang/String;
    const/4 v7, 0x0

    .line 476
    .local v7, date:Ljava/util/Date;
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 477
    .local v12, entryObject:Lorg/json/JSONObject;
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 478
    .local v10, dayDataObject:Lorg/json/JSONObject;
    const-wide/16 v14, 0x0

    .line 481
    .local v14, waterAmount:D
    :cond_0
    new-instance v12, Lorg/json/JSONObject;

    .end local v12           #entryObject:Lorg/json/JSONObject;
    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 482
    .restart local v12       #entryObject:Lorg/json/JSONObject;
    new-instance v10, Lorg/json/JSONObject;

    .end local v10           #dayDataObject:Lorg/json/JSONObject;
    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 483
    .restart local v10       #dayDataObject:Lorg/json/JSONObject;
    new-instance v7, Ljava/util/Date;

    .end local v7           #date:Ljava/util/Date;
    const-string v0, "date"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 484
    .restart local v7       #date:Ljava/util/Date;
    const-string v0, "yyyy-MM-dd"

    invoke-static {v7, v0}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 487
    :try_start_0
    const-string v0, "amount"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v14

    .line 488
    const-wide/high16 v0, 0x4020

    mul-double/2addr v14, v0

    .line 489
    const-string v0, "Water"

    invoke-virtual {v12, v0, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 490
    const-string v0, "data"

    invoke-virtual {v10, v0, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 492
    new-instance v8, Ljava/util/Date;

    const-string v0, "last_updated_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {v8, v0, v1}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 493
    .end local v7           #date:Ljava/util/Date;
    .local v8, date:Ljava/util/Date;
    :try_start_1
    const-string v0, "updated_at"

    const-string v1, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v8, v1}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 495
    invoke-virtual {v13, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v7, v8

    .line 500
    .end local v8           #date:Ljava/util/Date;
    .restart local v7       #date:Ljava/util/Date;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    .line 480
    if-nez v0, :cond_0

    .line 503
    .end local v7           #date:Ljava/util/Date;
    .end local v9           #dateString:Ljava/lang/String;
    .end local v10           #dayDataObject:Lorg/json/JSONObject;
    .end local v12           #entryObject:Lorg/json/JSONObject;
    .end local v14           #waterAmount:D
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 506
    :cond_2
    return-object v13

    .line 497
    .restart local v7       #date:Ljava/util/Date;
    .restart local v9       #dateString:Ljava/lang/String;
    .restart local v10       #dayDataObject:Lorg/json/JSONObject;
    .restart local v12       #entryObject:Lorg/json/JSONObject;
    .restart local v14       #waterAmount:D
    :catch_0
    move-exception v11

    .line 498
    .local v11, e:Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 497
    .end local v7           #date:Ljava/util/Date;
    .end local v11           #e:Lorg/json/JSONException;
    .restart local v8       #date:Ljava/util/Date;
    :catch_1
    move-exception v11

    move-object v7, v8

    .end local v8           #date:Ljava/util/Date;
    .restart local v7       #date:Ljava/util/Date;
    goto :goto_1
.end method

.method public static getWeightLastSyncTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 121
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 122
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "last_sync_weight"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getWeightToSync(Landroid/content/Context;JJJ)Lorg/json/JSONObject;
    .locals 16
    .parameter "c"
    .parameter "startDate"
    .parameter "endDate"
    .parameter "lastSyncTime"

    .prologue
    .line 614
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    .line 616
    .local v13, userDataObject:Lorg/json/JSONObject;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WEIGHT:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 617
    const-string v3, "last_updated_time>=? AND date>=? AND date<? "

    .line 618
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static/range {p5 .. p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v4, v5

    const/4 v5, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v4, v5

    const/4 v5, 0x2

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v4, v5

    const-string v5, "date"

    .line 616
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 620
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 621
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 622
    const/4 v9, 0x0

    .line 623
    .local v9, dateString:Ljava/lang/String;
    const/4 v7, 0x0

    .line 624
    .local v7, date:Ljava/util/Date;
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 625
    .local v12, entryObject:Lorg/json/JSONObject;
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 628
    .local v10, dayDataObject:Lorg/json/JSONObject;
    :cond_0
    new-instance v12, Lorg/json/JSONObject;

    .end local v12           #entryObject:Lorg/json/JSONObject;
    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 629
    .restart local v12       #entryObject:Lorg/json/JSONObject;
    new-instance v10, Lorg/json/JSONObject;

    .end local v10           #dayDataObject:Lorg/json/JSONObject;
    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 630
    .restart local v10       #dayDataObject:Lorg/json/JSONObject;
    new-instance v7, Ljava/util/Date;

    .end local v7           #date:Ljava/util/Date;
    const-string v0, "date"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 631
    .restart local v7       #date:Ljava/util/Date;
    const-string v0, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v7, v0}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 634
    :try_start_0
    const-string v0, "Weight"

    const-string v1, "weight"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 635
    const-string v0, "data"

    invoke-virtual {v10, v0, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 637
    new-instance v8, Ljava/util/Date;

    const-string v0, "last_updated_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {v8, v0, v1}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 638
    .end local v7           #date:Ljava/util/Date;
    .local v8, date:Ljava/util/Date;
    :try_start_1
    const-string v0, "updated_at"

    const-string v1, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v8, v1}, Lorg/medhelp/mydiet/util/DateUtil;->formatDateToUTC(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 640
    invoke-virtual {v13, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v7, v8

    .line 644
    .end local v8           #date:Ljava/util/Date;
    .restart local v7       #date:Ljava/util/Date;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    .line 627
    if-nez v0, :cond_0

    .line 647
    .end local v7           #date:Ljava/util/Date;
    .end local v9           #dateString:Ljava/lang/String;
    .end local v10           #dayDataObject:Lorg/json/JSONObject;
    .end local v12           #entryObject:Lorg/json/JSONObject;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 650
    :cond_2
    return-object v13

    .line 641
    .restart local v7       #date:Ljava/util/Date;
    .restart local v9       #dateString:Ljava/lang/String;
    .restart local v10       #dayDataObject:Lorg/json/JSONObject;
    .restart local v12       #entryObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v11

    .line 642
    .local v11, e:Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 641
    .end local v7           #date:Ljava/util/Date;
    .end local v11           #e:Lorg/json/JSONException;
    .restart local v8       #date:Ljava/util/Date;
    :catch_1
    move-exception v11

    move-object v7, v8

    .end local v8           #date:Ljava/util/Date;
    .restart local v7       #date:Ljava/util/Date;
    goto :goto_1
.end method

.method public static handleExerciseItemsSyncResponse(Landroid/content/Context;Ljava/lang/String;JJ)V
    .locals 10
    .parameter "c"
    .parameter "responseString"
    .parameter "currentSyncStartTime"
    .parameter "syncMonthStartTime"

    .prologue
    .line 902
    const/4 v6, 0x0

    .line 903
    .local v6, responseObject:Lorg/json/JSONObject;
    const/4 v4, 0x0

    .line 906
    .local v4, hasErrors:Z
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 908
    .end local v6           #responseObject:Lorg/json/JSONObject;
    .local v7, responseObject:Lorg/json/JSONObject;
    if-eqz v7, :cond_0

    :try_start_1
    const-string v9, "status_code"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    :cond_0
    move-object v6, v7

    .line 946
    .end local v7           #responseObject:Lorg/json/JSONObject;
    .restart local v6       #responseObject:Lorg/json/JSONObject;
    :cond_1
    :goto_0
    return-void

    .line 912
    .end local v6           #responseObject:Lorg/json/JSONObject;
    .restart local v7       #responseObject:Lorg/json/JSONObject;
    :cond_2
    const-string v9, "status_code"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    if-eqz v9, :cond_3

    move-object v6, v7

    .line 914
    .end local v7           #responseObject:Lorg/json/JSONObject;
    .restart local v6       #responseObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 917
    .end local v6           #responseObject:Lorg/json/JSONObject;
    .restart local v7       #responseObject:Lorg/json/JSONObject;
    :cond_3
    const-string v9, "data"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 919
    .local v0, dataObject:Lorg/json/JSONObject;
    if-eqz v0, :cond_6

    .line 920
    const-string v9, "user_data"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 921
    .local v8, userDataObject:Lorg/json/JSONObject;
    const/4 v2, 0x0

    .line 924
    .local v2, dayDataArray:Lorg/json/JSONArray;
    invoke-virtual {v8}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    .line 926
    .local v5, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v5, :cond_6

    .line 927
    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v9

    if-nez v9, :cond_5

    move-object v6, v7

    .line 943
    .end local v0           #dataObject:Lorg/json/JSONObject;
    .end local v2           #dayDataArray:Lorg/json/JSONArray;
    .end local v5           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v7           #responseObject:Lorg/json/JSONObject;
    .end local v8           #userDataObject:Lorg/json/JSONObject;
    .restart local v6       #responseObject:Lorg/json/JSONObject;
    :goto_2
    if-nez v4, :cond_1

    .line 944
    const-string v9, "last_sync_exercises"

    invoke-static {p0, p4, p5, v9}, Lorg/medhelp/mydiet/util/SyncUtil;->updateSyncStatus(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0

    .line 928
    .end local v6           #responseObject:Lorg/json/JSONObject;
    .restart local v0       #dataObject:Lorg/json/JSONObject;
    .restart local v2       #dayDataArray:Lorg/json/JSONArray;
    .restart local v5       #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v7       #responseObject:Lorg/json/JSONObject;
    .restart local v8       #userDataObject:Lorg/json/JSONObject;
    :cond_5
    :try_start_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 929
    .local v1, dateString:Ljava/lang/String;
    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 930
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-eqz v9, :cond_4

    .line 934
    invoke-static {p0, p2, p3, v2, v1}, Lorg/medhelp/mydiet/util/SyncUtil;->processExerciseItemsDayData(Landroid/content/Context;JLorg/json/JSONArray;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 938
    .end local v0           #dataObject:Lorg/json/JSONObject;
    .end local v1           #dateString:Ljava/lang/String;
    .end local v2           #dayDataArray:Lorg/json/JSONArray;
    .end local v5           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v8           #userDataObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    move-object v6, v7

    .line 939
    .end local v7           #responseObject:Lorg/json/JSONObject;
    .local v3, e:Lorg/json/JSONException;
    .restart local v6       #responseObject:Lorg/json/JSONObject;
    :goto_3
    const/4 v4, 0x1

    .line 940
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 938
    .end local v3           #e:Lorg/json/JSONException;
    :catch_1
    move-exception v3

    goto :goto_3

    .end local v6           #responseObject:Lorg/json/JSONObject;
    .restart local v0       #dataObject:Lorg/json/JSONObject;
    .restart local v7       #responseObject:Lorg/json/JSONObject;
    :cond_6
    move-object v6, v7

    .end local v7           #responseObject:Lorg/json/JSONObject;
    .restart local v6       #responseObject:Lorg/json/JSONObject;
    goto :goto_2
.end method

.method public static handleFoodsResponse(Landroid/content/Context;Ljava/lang/String;)V
    .locals 13
    .parameter "c"
    .parameter "foodsResponse"

    .prologue
    const-wide/16 v11, -0x270f

    .line 1433
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    .line 1464
    :cond_0
    :goto_0
    return-void

    .line 1435
    :cond_1
    const/4 v5, 0x0

    .line 1436
    .local v5, foodsObject:Lorg/json/JSONObject;
    const/4 v4, 0x0

    .line 1438
    .local v4, foodObject:Lorg/json/JSONObject;
    const/4 v0, 0x0

    .line 1441
    .local v0, cv:Landroid/content/ContentValues;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1443
    .end local v5           #foodsObject:Lorg/json/JSONObject;
    .local v6, foodsObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v8, "status_code"

    const-wide/16 v9, -0x270f

    invoke-virtual {v6, v8, v9, v10}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    cmp-long v8, v8, v11

    if-nez v8, :cond_0

    .line 1448
    invoke-virtual {v6}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v7

    .line 1450
    .local v7, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v8

    if-eqz v8, :cond_3

    :cond_2
    move-object v1, v0

    .line 1452
    .end local v0           #cv:Landroid/content/ContentValues;
    .local v1, cv:Landroid/content/ContentValues;
    :try_start_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1454
    .local v3, foodMedHelpId:Ljava/lang/String;
    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 1455
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1456
    .end local v1           #cv:Landroid/content/ContentValues;
    .restart local v0       #cv:Landroid/content/ContentValues;
    :try_start_3
    const-string v8, "food"

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    invoke-virtual {v8, v9, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1459
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    move-result v8

    .line 1451
    if-nez v8, :cond_2

    move-object v5, v6

    .end local v6           #foodsObject:Lorg/json/JSONObject;
    .restart local v5       #foodsObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 1461
    .end local v3           #foodMedHelpId:Ljava/lang/String;
    .end local v7           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :catch_0
    move-exception v2

    .line 1462
    .local v2, e:Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 1461
    .end local v2           #e:Lorg/json/JSONException;
    .end local v5           #foodsObject:Lorg/json/JSONObject;
    .restart local v6       #foodsObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v2

    move-object v5, v6

    .end local v6           #foodsObject:Lorg/json/JSONObject;
    .restart local v5       #foodsObject:Lorg/json/JSONObject;
    goto :goto_1

    .end local v0           #cv:Landroid/content/ContentValues;
    .end local v5           #foodsObject:Lorg/json/JSONObject;
    .restart local v1       #cv:Landroid/content/ContentValues;
    .restart local v6       #foodsObject:Lorg/json/JSONObject;
    .restart local v7       #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :catch_2
    move-exception v2

    move-object v0, v1

    .end local v1           #cv:Landroid/content/ContentValues;
    .restart local v0       #cv:Landroid/content/ContentValues;
    move-object v5, v6

    .end local v6           #foodsObject:Lorg/json/JSONObject;
    .restart local v5       #foodsObject:Lorg/json/JSONObject;
    goto :goto_1

    .end local v5           #foodsObject:Lorg/json/JSONObject;
    .restart local v6       #foodsObject:Lorg/json/JSONObject;
    :cond_3
    move-object v5, v6

    .end local v6           #foodsObject:Lorg/json/JSONObject;
    .restart local v5       #foodsObject:Lorg/json/JSONObject;
    goto :goto_0
.end method

.method public static handleFoodsUpdateResponse(Landroid/content/Context;Ljava/lang/String;)V
    .locals 18
    .parameter "c"
    .parameter "foodsResponse"

    .prologue
    .line 1467
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 1519
    :cond_0
    :goto_0
    return-void

    .line 1469
    :cond_1
    const/4 v12, 0x0

    .line 1470
    .local v12, foodsObject:Lorg/json/JSONObject;
    const/4 v11, 0x0

    .line 1472
    .local v11, foodObject:Lorg/json/JSONObject;
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1475
    .local v8, cv:Landroid/content/ContentValues;
    :try_start_0
    new-instance v13, Lorg/json/JSONObject;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1477
    .end local v12           #foodsObject:Lorg/json/JSONObject;
    .local v13, foodsObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v1, "status_code"

    const-wide/16 v2, -0x270f

    invoke-virtual {v13, v1, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v1

    const-wide/16 v3, -0x270f

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 1482
    invoke-virtual {v13}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v16

    .line 1484
    .local v16, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v16, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1486
    :cond_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1488
    .local v10, foodMedHelpId:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    const/4 v3, 0x0

    const-string v4, "medhelp_id"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v10, v5, v6

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1490
    .local v7, cursor:Landroid/database/Cursor;
    invoke-virtual {v13, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 1491
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    .line 1493
    if-eqz v11, :cond_3

    .line 1494
    if-eqz v7, :cond_5

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1496
    const-string v1, "_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1498
    .local v14, inAppId:J
    const-string v1, "food"

    const-string v2, "food"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1499
    const-string v1, "servings"

    const-string v2, "food_servings"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1501
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    const-string v3, "_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v8, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1510
    .end local v14           #inAppId:J
    :cond_3
    :goto_1
    if-eqz v7, :cond_4

    .line 1511
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1514
    :cond_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    .line 1485
    if-nez v1, :cond_2

    move-object v12, v13

    .end local v13           #foodsObject:Lorg/json/JSONObject;
    .restart local v12       #foodsObject:Lorg/json/JSONObject;
    goto/16 :goto_0

    .line 1505
    .end local v12           #foodsObject:Lorg/json/JSONObject;
    .restart local v13       #foodsObject:Lorg/json/JSONObject;
    :cond_5
    const-string v1, "food"

    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1506
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    invoke-virtual {v1, v2, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1516
    .end local v7           #cursor:Landroid/database/Cursor;
    .end local v10           #foodMedHelpId:Ljava/lang/String;
    .end local v16           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :catch_0
    move-exception v9

    move-object v12, v13

    .line 1517
    .end local v13           #foodsObject:Lorg/json/JSONObject;
    .local v9, e:Lorg/json/JSONException;
    .restart local v12       #foodsObject:Lorg/json/JSONObject;
    :goto_2
    invoke-virtual {v9}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 1516
    .end local v9           #e:Lorg/json/JSONException;
    :catch_1
    move-exception v9

    goto :goto_2

    .end local v12           #foodsObject:Lorg/json/JSONObject;
    .restart local v13       #foodsObject:Lorg/json/JSONObject;
    .restart local v16       #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_6
    move-object v12, v13

    .end local v13           #foodsObject:Lorg/json/JSONObject;
    .restart local v12       #foodsObject:Lorg/json/JSONObject;
    goto/16 :goto_0
.end method

.method public static handleMealsSyncResponse(Landroid/content/Context;Ljava/lang/String;JJ)V
    .locals 14
    .parameter "c"
    .parameter "responseString"
    .parameter "currentSyncStartTime"
    .parameter "syncMonthStartTime"

    .prologue
    .line 1215
    const/4 v9, 0x0

    .line 1216
    .local v9, responseObject:Lorg/json/JSONObject;
    const/4 v6, 0x0

    .line 1219
    .local v6, hasErrors:Z
    :try_start_0
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221
    .end local v9           #responseObject:Lorg/json/JSONObject;
    .local v10, responseObject:Lorg/json/JSONObject;
    if-eqz v10, :cond_0

    :try_start_1
    const-string v13, "status_code"

    invoke-virtual {v10, v13}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_2

    :cond_0
    move-object v9, v10

    .line 1268
    .end local v10           #responseObject:Lorg/json/JSONObject;
    .restart local v9       #responseObject:Lorg/json/JSONObject;
    :cond_1
    :goto_0
    return-void

    .line 1225
    .end local v9           #responseObject:Lorg/json/JSONObject;
    .restart local v10       #responseObject:Lorg/json/JSONObject;
    :cond_2
    const-string v13, "status_code"

    invoke-virtual {v10, v13}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v13

    if-eqz v13, :cond_3

    move-object v9, v10

    .line 1227
    .end local v10           #responseObject:Lorg/json/JSONObject;
    .restart local v9       #responseObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 1230
    .end local v9           #responseObject:Lorg/json/JSONObject;
    .restart local v10       #responseObject:Lorg/json/JSONObject;
    :cond_3
    const-string v13, "data"

    invoke-virtual {v10, v13}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 1232
    .local v2, dataObject:Lorg/json/JSONObject;
    const-string v13, "updated_foods"

    invoke-virtual {v2, v13}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 1233
    .local v11, updatedFoods:Lorg/json/JSONArray;
    if-eqz v11, :cond_4

    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v13

    if-lez v13, :cond_4

    .line 1234
    invoke-static {p0, v11}, Lorg/medhelp/mydiet/util/SyncUtil;->updateFoodsWithMedHelpIds(Landroid/content/Context;Lorg/json/JSONArray;)V

    .line 1237
    :cond_4
    if-eqz v2, :cond_7

    .line 1238
    const-string v13, "user_data"

    invoke-virtual {v2, v13}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 1239
    .local v12, userDataObject:Lorg/json/JSONObject;
    const/4 v4, 0x0

    .line 1242
    .local v4, dayDataArray:Lorg/json/JSONArray;
    invoke-virtual {v12}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v7

    .line 1244
    .local v7, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v7, :cond_7

    .line 1245
    const/4 v8, 0x0

    .line 1246
    .local v8, processMealsStatus:Z
    :cond_5
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v13

    if-nez v13, :cond_6

    move-object v9, v10

    .line 1265
    .end local v2           #dataObject:Lorg/json/JSONObject;
    .end local v4           #dayDataArray:Lorg/json/JSONArray;
    .end local v7           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v8           #processMealsStatus:Z
    .end local v10           #responseObject:Lorg/json/JSONObject;
    .end local v11           #updatedFoods:Lorg/json/JSONArray;
    .end local v12           #userDataObject:Lorg/json/JSONObject;
    .restart local v9       #responseObject:Lorg/json/JSONObject;
    :goto_2
    if-nez v6, :cond_1

    .line 1266
    const-string v13, "last_sync_meals"

    move-wide/from16 v0, p4

    invoke-static {p0, v0, v1, v13}, Lorg/medhelp/mydiet/util/SyncUtil;->updateSyncStatus(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0

    .line 1247
    .end local v9           #responseObject:Lorg/json/JSONObject;
    .restart local v2       #dataObject:Lorg/json/JSONObject;
    .restart local v4       #dayDataArray:Lorg/json/JSONArray;
    .restart local v7       #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v8       #processMealsStatus:Z
    .restart local v10       #responseObject:Lorg/json/JSONObject;
    .restart local v11       #updatedFoods:Lorg/json/JSONArray;
    .restart local v12       #userDataObject:Lorg/json/JSONObject;
    :cond_6
    :try_start_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1248
    .local v3, dateString:Ljava/lang/String;
    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 1249
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v13

    if-eqz v13, :cond_5

    .line 1253
    move-wide/from16 v0, p2

    invoke-static {p0, v0, v1, v4, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->processMealsDayData(Landroid/content/Context;JLorg/json/JSONArray;Ljava/lang/String;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v8

    .line 1254
    if-nez v8, :cond_5

    .line 1255
    const/4 v6, 0x1

    goto :goto_1

    .line 1260
    .end local v2           #dataObject:Lorg/json/JSONObject;
    .end local v3           #dateString:Ljava/lang/String;
    .end local v4           #dayDataArray:Lorg/json/JSONArray;
    .end local v7           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v8           #processMealsStatus:Z
    .end local v10           #responseObject:Lorg/json/JSONObject;
    .end local v11           #updatedFoods:Lorg/json/JSONArray;
    .end local v12           #userDataObject:Lorg/json/JSONObject;
    .restart local v9       #responseObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v5

    .line 1261
    .local v5, e:Lorg/json/JSONException;
    :goto_3
    const/4 v6, 0x1

    .line 1262
    invoke-virtual {v5}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 1260
    .end local v5           #e:Lorg/json/JSONException;
    .end local v9           #responseObject:Lorg/json/JSONObject;
    .restart local v10       #responseObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v5

    move-object v9, v10

    .end local v10           #responseObject:Lorg/json/JSONObject;
    .restart local v9       #responseObject:Lorg/json/JSONObject;
    goto :goto_3

    .end local v9           #responseObject:Lorg/json/JSONObject;
    .restart local v2       #dataObject:Lorg/json/JSONObject;
    .restart local v10       #responseObject:Lorg/json/JSONObject;
    .restart local v11       #updatedFoods:Lorg/json/JSONArray;
    :cond_7
    move-object v9, v10

    .end local v10           #responseObject:Lorg/json/JSONObject;
    .restart local v9       #responseObject:Lorg/json/JSONObject;
    goto :goto_2
.end method

.method private static handleMissingFoods(Landroid/content/Context;Lorg/json/JSONArray;)Z
    .locals 5
    .parameter "c"
    .parameter "mealsJSONArray"

    .prologue
    .line 1371
    invoke-static {p0, p1}, Lorg/medhelp/mydiet/util/SyncUtil;->getMissingFoodIds(Landroid/content/Context;Lorg/json/JSONArray;)Lorg/json/JSONArray;

    move-result-object v3

    .line 1373
    .local v3, missingFoodIds:Lorg/json/JSONArray;
    const/4 v0, 0x3

    .line 1374
    .local v0, MAX_ATTEMPTS:I
    const/4 v1, 0x0

    .line 1377
    .local v1, attemptCount:I
    :cond_0
    invoke-static {p0, p1}, Lorg/medhelp/mydiet/util/SyncUtil;->getMissingFoodIds(Landroid/content/Context;Lorg/json/JSONArray;)Lorg/json/JSONArray;

    move-result-object v3

    .line 1378
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 1379
    :cond_1
    const/4 v4, 0x1

    .line 1386
    :goto_0
    return v4

    .line 1381
    :cond_2
    invoke-static {p0, v3}, Lorg/medhelp/mydiet/http/MHHttpHandler;->requestFoods(Landroid/content/Context;Lorg/json/JSONArray;)Ljava/lang/String;

    move-result-object v2

    .line 1382
    .local v2, foodsResponse:Ljava/lang/String;
    invoke-static {p0, v2}, Lorg/medhelp/mydiet/util/SyncUtil;->handleFoodsResponse(Landroid/content/Context;Ljava/lang/String;)V

    .line 1383
    add-int/lit8 v1, v1, 0x1

    .line 1384
    const/4 v4, 0x3

    if-lt v1, v4, :cond_0

    .line 1386
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static handleSyncNewFoodsResponse(Landroid/content/Context;Ljava/lang/String;)V
    .locals 13
    .parameter "c"
    .parameter "response"

    .prologue
    .line 1666
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1683
    :cond_0
    :goto_0
    return-void

    .line 1669
    :cond_1
    :try_start_0
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1670
    .local v11, responseObject:Lorg/json/JSONObject;
    const-string v0, "data"

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 1672
    .local v7, dataObject:Lorg/json/JSONObject;
    const-string v0, "foods"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 1673
    .local v10, foodsJSONArray:Lorg/json/JSONArray;
    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v12, 0x0

    invoke-virtual {v0, v12}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v1

    .line 1674
    .local v1, foodInAppId:J
    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v12, 0x1

    invoke-virtual {v0, v12}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v3

    .line 1675
    .local v3, foodMedHelpId:J
    const-string v0, "food_servings"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 1676
    .local v9, foodServingsJSONArray:Lorg/json/JSONArray;
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v12, 0x1

    invoke-virtual {v0, v12}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v5

    .local v5, servingMedHelpId:J
    move-object v0, p0

    .line 1678
    invoke-static/range {v0 .. v6}, Lorg/medhelp/mydiet/util/SyncUtil;->updateFoodWithUpdatedIds(Landroid/content/Context;JJJ)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1680
    .end local v1           #foodInAppId:J
    .end local v3           #foodMedHelpId:J
    .end local v5           #servingMedHelpId:J
    .end local v7           #dataObject:Lorg/json/JSONObject;
    .end local v9           #foodServingsJSONArray:Lorg/json/JSONArray;
    .end local v10           #foodsJSONArray:Lorg/json/JSONArray;
    .end local v11           #responseObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v8

    .line 1681
    .local v8, e:Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static handleWaterSyncResponse(Landroid/content/Context;Ljava/lang/String;JJ)V
    .locals 18
    .parameter "c"
    .parameter "responseString"
    .parameter "currentSyncStartTime"
    .parameter "syncMonthStartTime"

    .prologue
    .line 525
    const/4 v15, 0x0

    .line 526
    .local v15, responseObject:Lorg/json/JSONObject;
    const/4 v13, 0x0

    .line 529
    .local v13, hasErrors:Z
    :try_start_0
    new-instance v16, Lorg/json/JSONObject;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 531
    .end local v15           #responseObject:Lorg/json/JSONObject;
    .local v16, responseObject:Lorg/json/JSONObject;
    if-eqz v16, :cond_0

    :try_start_1
    const-string v3, "status_code"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    move-object/from16 v15, v16

    .line 575
    .end local v16           #responseObject:Lorg/json/JSONObject;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    :cond_1
    :goto_0
    return-void

    .line 535
    .end local v15           #responseObject:Lorg/json/JSONObject;
    .restart local v16       #responseObject:Lorg/json/JSONObject;
    :cond_2
    const-string v3, "status_code"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v15, v16

    .line 537
    .end local v16           #responseObject:Lorg/json/JSONObject;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 540
    .end local v15           #responseObject:Lorg/json/JSONObject;
    .restart local v16       #responseObject:Lorg/json/JSONObject;
    :cond_3
    const-string v3, "data"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 541
    .local v9, dataObject:Lorg/json/JSONObject;
    if-eqz v9, :cond_6

    .line 542
    const-string v3, "user_data"

    invoke-virtual {v9, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v17

    .line 543
    .local v17, userDataObject:Lorg/json/JSONObject;
    const/4 v11, 0x0

    .line 544
    .local v11, dayDataObject:Lorg/json/JSONObject;
    const/4 v10, 0x0

    .line 545
    .local v10, dayDataArray:Lorg/json/JSONArray;
    const-wide/16 v6, 0x0

    .line 548
    .local v6, waterInFlOz:D
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v14

    .line 550
    .local v14, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v14, :cond_6

    .line 551
    :cond_4
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v3

    if-nez v3, :cond_5

    move-object/from16 v15, v16

    .line 571
    .end local v6           #waterInFlOz:D
    .end local v9           #dataObject:Lorg/json/JSONObject;
    .end local v10           #dayDataArray:Lorg/json/JSONArray;
    .end local v11           #dayDataObject:Lorg/json/JSONObject;
    .end local v14           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v16           #responseObject:Lorg/json/JSONObject;
    .end local v17           #userDataObject:Lorg/json/JSONObject;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    :goto_2
    if-nez v13, :cond_1

    .line 572
    const-string v3, "last_sync_water"

    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->updateSyncStatus(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0

    .line 552
    .end local v15           #responseObject:Lorg/json/JSONObject;
    .restart local v6       #waterInFlOz:D
    .restart local v9       #dataObject:Lorg/json/JSONObject;
    .restart local v10       #dayDataArray:Lorg/json/JSONArray;
    .restart local v11       #dayDataObject:Lorg/json/JSONObject;
    .restart local v14       #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v16       #responseObject:Lorg/json/JSONObject;
    .restart local v17       #userDataObject:Lorg/json/JSONObject;
    :cond_5
    :try_start_2
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 553
    .local v8, dateString:Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 554
    if-eqz v10, :cond_4

    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-eqz v3, :cond_4

    .line 559
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 560
    const-string v3, "Water"

    invoke-virtual {v11, v3}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    move-object/from16 v3, p0

    move-wide/from16 v4, p2

    .line 562
    invoke-static/range {v3 .. v8}, Lorg/medhelp/mydiet/util/SyncUtil;->processWaterDayData(Landroid/content/Context;JDLjava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 566
    .end local v6           #waterInFlOz:D
    .end local v8           #dateString:Ljava/lang/String;
    .end local v9           #dataObject:Lorg/json/JSONObject;
    .end local v10           #dayDataArray:Lorg/json/JSONArray;
    .end local v11           #dayDataObject:Lorg/json/JSONObject;
    .end local v14           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v17           #userDataObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v12

    move-object/from16 v15, v16

    .line 567
    .end local v16           #responseObject:Lorg/json/JSONObject;
    .local v12, e:Lorg/json/JSONException;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    :goto_3
    const/4 v13, 0x1

    .line 568
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 566
    .end local v12           #e:Lorg/json/JSONException;
    :catch_1
    move-exception v12

    goto :goto_3

    .end local v15           #responseObject:Lorg/json/JSONObject;
    .restart local v9       #dataObject:Lorg/json/JSONObject;
    .restart local v16       #responseObject:Lorg/json/JSONObject;
    :cond_6
    move-object/from16 v15, v16

    .end local v16           #responseObject:Lorg/json/JSONObject;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    goto :goto_2
.end method

.method public static handleWeightSyncResponse(Landroid/content/Context;Ljava/lang/String;JJ)V
    .locals 18
    .parameter "c"
    .parameter "responseString"
    .parameter "currentSyncStartTime"
    .parameter "syncMonthStartTime"

    .prologue
    .line 654
    const/4 v15, 0x0

    .line 655
    .local v15, responseObject:Lorg/json/JSONObject;
    const/4 v13, 0x0

    .line 658
    .local v13, hasErrors:Z
    :try_start_0
    new-instance v16, Lorg/json/JSONObject;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 660
    .end local v15           #responseObject:Lorg/json/JSONObject;
    .local v16, responseObject:Lorg/json/JSONObject;
    if-eqz v16, :cond_0

    :try_start_1
    const-string v3, "status_code"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    move-object/from16 v15, v16

    .line 703
    .end local v16           #responseObject:Lorg/json/JSONObject;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    :cond_1
    :goto_0
    return-void

    .line 664
    .end local v15           #responseObject:Lorg/json/JSONObject;
    .restart local v16       #responseObject:Lorg/json/JSONObject;
    :cond_2
    const-string v3, "status_code"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v15, v16

    .line 666
    .end local v16           #responseObject:Lorg/json/JSONObject;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 669
    .end local v15           #responseObject:Lorg/json/JSONObject;
    .restart local v16       #responseObject:Lorg/json/JSONObject;
    :cond_3
    const-string v3, "data"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 670
    .local v9, dataObject:Lorg/json/JSONObject;
    if-eqz v9, :cond_6

    .line 671
    const-string v3, "user_data"

    invoke-virtual {v9, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v17

    .line 672
    .local v17, userDataObject:Lorg/json/JSONObject;
    const/4 v11, 0x0

    .line 673
    .local v11, dayDataObject:Lorg/json/JSONObject;
    const/4 v10, 0x0

    .line 674
    .local v10, dayDataArray:Lorg/json/JSONArray;
    const-wide/16 v6, 0x0

    .line 677
    .local v6, weight:D
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v14

    .line 679
    .local v14, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v14, :cond_6

    .line 680
    :cond_4
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v3

    if-nez v3, :cond_5

    move-object/from16 v15, v16

    .line 699
    .end local v6           #weight:D
    .end local v9           #dataObject:Lorg/json/JSONObject;
    .end local v10           #dayDataArray:Lorg/json/JSONArray;
    .end local v11           #dayDataObject:Lorg/json/JSONObject;
    .end local v14           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v16           #responseObject:Lorg/json/JSONObject;
    .end local v17           #userDataObject:Lorg/json/JSONObject;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    :goto_2
    if-nez v13, :cond_1

    .line 700
    const-string v3, "last_sync_weight"

    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2, v3}, Lorg/medhelp/mydiet/util/SyncUtil;->updateSyncStatus(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0

    .line 681
    .end local v15           #responseObject:Lorg/json/JSONObject;
    .restart local v6       #weight:D
    .restart local v9       #dataObject:Lorg/json/JSONObject;
    .restart local v10       #dayDataArray:Lorg/json/JSONArray;
    .restart local v11       #dayDataObject:Lorg/json/JSONObject;
    .restart local v14       #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v16       #responseObject:Lorg/json/JSONObject;
    .restart local v17       #userDataObject:Lorg/json/JSONObject;
    :cond_5
    :try_start_2
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 682
    .local v8, dateString:Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 683
    if-eqz v10, :cond_4

    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-eqz v3, :cond_4

    .line 687
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 688
    const-string v3, "Weight"

    invoke-virtual {v11, v3}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    move-object/from16 v3, p0

    move-wide/from16 v4, p2

    .line 690
    invoke-static/range {v3 .. v8}, Lorg/medhelp/mydiet/util/SyncUtil;->processWeightDayData(Landroid/content/Context;JDLjava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 694
    .end local v6           #weight:D
    .end local v8           #dateString:Ljava/lang/String;
    .end local v9           #dataObject:Lorg/json/JSONObject;
    .end local v10           #dayDataArray:Lorg/json/JSONArray;
    .end local v11           #dayDataObject:Lorg/json/JSONObject;
    .end local v14           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v17           #userDataObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v12

    move-object/from16 v15, v16

    .line 695
    .end local v16           #responseObject:Lorg/json/JSONObject;
    .local v12, e:Lorg/json/JSONException;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    :goto_3
    const/4 v13, 0x1

    .line 696
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 694
    .end local v12           #e:Lorg/json/JSONException;
    :catch_1
    move-exception v12

    goto :goto_3

    .end local v15           #responseObject:Lorg/json/JSONObject;
    .restart local v9       #dataObject:Lorg/json/JSONObject;
    .restart local v16       #responseObject:Lorg/json/JSONObject;
    :cond_6
    move-object/from16 v15, v16

    .end local v16           #responseObject:Lorg/json/JSONObject;
    .restart local v15       #responseObject:Lorg/json/JSONObject;
    goto :goto_2
.end method

.method public static isLastSyncTimeWithinThreshhold(J)Z
    .locals 4
    .parameter "lastSyncTime"

    .prologue
    .line 47
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1, p0, p1}, Lorg/medhelp/mydiet/util/DateUtil;->getDifferenceInMinutes(JJ)J

    move-result-wide v0

    sget-wide v2, Lorg/medhelp/mydiet/util/SyncUtil;->SYNC_THRESHHOLD_MINUTES:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 48
    const/4 v0, 0x1

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isResponseValid(Ljava/lang/String;)Z
    .locals 7
    .parameter "response"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 510
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    move v3, v4

    .line 521
    :cond_1
    :goto_0
    return v3

    .line 513
    :cond_2
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 514
    .local v1, responseObject:Lorg/json/JSONObject;
    const-string v5, "status_code"

    const/4 v6, 0x1

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 515
    .local v2, statusCode:I
    if-eqz v2, :cond_1

    move v3, v4

    .line 521
    goto :goto_0

    .line 516
    .end local v1           #responseObject:Lorg/json/JSONObject;
    .end local v2           #statusCode:I
    :catch_0
    move-exception v0

    .line 517
    .local v0, e:Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move v3, v4

    .line 518
    goto :goto_0
.end method

.method public static loadUserConfigs(Landroid/content/Context;Ljava/lang/String;)V
    .locals 25
    .parameter "c"
    .parameter "syncResult"

    .prologue
    .line 296
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getUserType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v22

    const-string v23, "medhelp"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v22

    if-eqz v22, :cond_0

    .line 303
    const/16 v18, 0x0

    .line 306
    .local v18, resultObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v19, Lorg/json/JSONObject;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 308
    .end local v18           #resultObject:Lorg/json/JSONObject;
    .local v19, resultObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v22, "status_code"

    const/16 v23, -0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v22

    if-nez v22, :cond_1a

    .line 310
    const-string v22, "data"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 312
    .local v9, dataObject:Lorg/json/JSONObject;
    const-string v22, "config"

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 314
    .local v6, configsJSONArray:Lorg/json/JSONArray;
    if-eqz v6, :cond_0

    .line 316
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    .line 318
    .local v7, configsLength:I
    const/4 v5, 0x0

    .line 319
    .local v5, configObject:Lorg/json/JSONObject;
    const/4 v14, 0x0

    .local v14, i:I
    :goto_1
    if-lt v14, v7, :cond_2

    .line 434
    new-instance v22, Ljava/util/Date;

    invoke-direct/range {v22 .. v22}, Ljava/util/Date;-><init>()V

    invoke-virtual/range {v22 .. v22}, Ljava/util/Date;->getTime()J

    move-result-wide v22

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setConfigsLastSyncTime(Landroid/content/Context;J)V

    move-object/from16 v18, v19

    .end local v19           #resultObject:Lorg/json/JSONObject;
    .restart local v18       #resultObject:Lorg/json/JSONObject;
    goto :goto_0

    .line 320
    .end local v18           #resultObject:Lorg/json/JSONObject;
    .restart local v19       #resultObject:Lorg/json/JSONObject;
    :cond_2
    invoke-virtual {v6, v14}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 322
    const-string v22, "gender"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 323
    const-string v22, "gender"

    const-string v23, "F"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 325
    .local v12, gender:Ljava/lang/String;
    const-string v22, "M"

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 326
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGenderAsMale(Landroid/content/Context;)V

    .line 319
    .end local v12           #gender:Ljava/lang/String;
    :cond_3
    :goto_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 328
    .restart local v12       #gender:Ljava/lang/String;
    :cond_4
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGenderAsFemale(Landroid/content/Context;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 436
    .end local v5           #configObject:Lorg/json/JSONObject;
    .end local v6           #configsJSONArray:Lorg/json/JSONArray;
    .end local v7           #configsLength:I
    .end local v9           #dataObject:Lorg/json/JSONObject;
    .end local v12           #gender:Ljava/lang/String;
    .end local v14           #i:I
    :catch_0
    move-exception v11

    move-object/from16 v18, v19

    .line 437
    .end local v19           #resultObject:Lorg/json/JSONObject;
    .local v11, e:Lorg/json/JSONException;
    .restart local v18       #resultObject:Lorg/json/JSONObject;
    :goto_3
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 331
    .end local v11           #e:Lorg/json/JSONException;
    .end local v18           #resultObject:Lorg/json/JSONObject;
    .restart local v5       #configObject:Lorg/json/JSONObject;
    .restart local v6       #configsJSONArray:Lorg/json/JSONArray;
    .restart local v7       #configsLength:I
    .restart local v9       #dataObject:Lorg/json/JSONObject;
    .restart local v14       #i:I
    .restart local v19       #resultObject:Lorg/json/JSONObject;
    :cond_5
    :try_start_2
    const-string v22, "avatar"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 332
    const-string v22, "avatar"

    const/16 v23, -0x1

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v17

    .line 334
    .local v17, mhAvatarPosition:I
    if-lez v17, :cond_3

    .line 335
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 337
    .restart local v12       #gender:Ljava/lang/String;
    const-string v22, "Male"

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 339
    const/4 v15, 0x0

    .local v15, k:I
    :goto_4
    sget-object v22, Lorg/medhelp/mydiet/C$arrays;->femaleAvatarPositionOnMedHelp:[I

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v15, v0, :cond_3

    .line 340
    sget-object v22, Lorg/medhelp/mydiet/C$arrays;->maleAvatarPositionOnMedHelp:[I

    aget v22, v22, v15

    move/from16 v0, v22

    move/from16 v1, v17

    if-ne v0, v1, :cond_6

    .line 341
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setAvatarPosition(Landroid/content/Context;I)V

    .line 339
    :cond_6
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 345
    .end local v15           #k:I
    :cond_7
    const/4 v15, 0x0

    .restart local v15       #k:I
    :goto_5
    sget-object v22, Lorg/medhelp/mydiet/C$arrays;->femaleAvatarPositionOnMedHelp:[I

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v15, v0, :cond_3

    .line 346
    sget-object v22, Lorg/medhelp/mydiet/C$arrays;->femaleAvatarPositionOnMedHelp:[I

    aget v22, v22, v15

    move/from16 v0, v22

    move/from16 v1, v17

    if-ne v0, v1, :cond_8

    .line 347
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setAvatarPosition(Landroid/content/Context;I)V

    .line 345
    :cond_8
    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    .line 353
    .end local v12           #gender:Ljava/lang/String;
    .end local v15           #k:I
    .end local v17           #mhAvatarPosition:I
    :cond_9
    const-string v22, "birthday"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 354
    const-string v22, "birthday"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 356
    .local v4, birthday:Ljava/lang/String;
    const-string v22, "yyyy-MM-dd"

    move-object/from16 v0, v22

    invoke-static {v4, v0}, Lorg/medhelp/mydiet/util/DateUtil;->getDateFromString(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    .line 358
    .local v8, d:Ljava/util/Date;
    invoke-static {v8}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v10

    .line 359
    .local v10, dob:Ljava/util/Calendar;
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/Calendar;->get(I)I

    move-result v22

    const/16 v23, 0x2

    move/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/util/Calendar;->get(I)I

    move-result v23

    const/16 v24, 0x5

    move/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/util/Calendar;->get(I)I

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setDateOfBirth(Landroid/content/Context;III)V

    goto/16 :goto_2

    .line 361
    .end local v4           #birthday:Ljava/lang/String;
    .end local v8           #d:Ljava/util/Date;
    .end local v10           #dob:Ljava/util/Calendar;
    :cond_a
    const-string v22, "last_height"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 362
    const-string v22, "last_height"

    const-wide/16 v23, 0x0

    move-object/from16 v0, v22

    move-wide/from16 v1, v23

    invoke-virtual {v5, v0, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v13, v0

    .line 364
    .local v13, height:F
    const/16 v22, 0x0

    cmpl-float v22, v13, v22

    if-lez v22, :cond_3

    .line 365
    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightInInches(Landroid/content/Context;F)V

    goto/16 :goto_2

    .line 368
    .end local v13           #height:F
    :cond_b
    const-string v22, "last_weight"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 369
    const-string v22, "last_weight"

    const-wide/16 v23, 0x0

    move-object/from16 v0, v22

    move-wide/from16 v1, v23

    invoke-virtual {v5, v0, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v21, v0

    .line 371
    .local v21, weight:F
    const/16 v22, 0x0

    cmpl-float v22, v21, v22

    if-lez v22, :cond_3

    .line 372
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWeightInLb(Landroid/content/Context;F)V

    goto/16 :goto_2

    .line 374
    .end local v21           #weight:F
    :cond_c
    const-string v22, "activity_level"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_11

    .line 375
    const-string v22, "activity_level"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 377
    .local v16, level:Ljava/lang/String;
    const-string v22, "Sedentary"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 378
    const-string v22, "Sedentary"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setActivityLevel(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 380
    :cond_d
    const-string v22, "Lightly Active"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 381
    const-string v22, "Lightly Active"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setActivityLevel(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 383
    :cond_e
    const-string v22, "Moderately Active"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_f

    .line 384
    const-string v22, "Moderately Active"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setActivityLevel(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 386
    :cond_f
    const-string v22, "Very Active"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_10

    .line 387
    const-string v22, "Very Active"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setActivityLevel(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 389
    :cond_10
    const-string v22, "Extremely Active"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 390
    const-string v22, "Extremely Active"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setActivityLevel(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 393
    .end local v16           #level:Ljava/lang/String;
    :cond_11
    const-string v22, "unit_volume"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_14

    .line 394
    const-string v22, "unit_volume"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 396
    .local v20, units:Ljava/lang/String;
    const-string v22, "milliliters"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_12

    .line 397
    const-string v22, "mL"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWaterUnits(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 398
    :cond_12
    const-string v22, "glasses"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_13

    .line 399
    const-string v22, "glasses"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWaterUnits(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 400
    :cond_13
    const-string v22, "fluid_ounces"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 401
    const-string v22, "fl oz"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWaterUnits(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 404
    .end local v20           #units:Ljava/lang/String;
    :cond_14
    const-string v22, "unit_length"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_16

    .line 405
    const-string v22, "unit_length"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 407
    .restart local v20       #units:Ljava/lang/String;
    const-string v22, "inches"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_15

    .line 408
    const-string v22, "in"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightUnits(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 409
    :cond_15
    const-string v22, "centimeters"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 410
    const-string v22, "cm"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightUnits(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 413
    .end local v20           #units:Ljava/lang/String;
    :cond_16
    const-string v22, "unit_distance"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_18

    .line 414
    const-string v22, "unit_distance"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 416
    .restart local v20       #units:Ljava/lang/String;
    const-string v22, "kilometers"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_17

    .line 417
    const-string v22, "km"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setDistanceUnits(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 418
    :cond_17
    const-string v22, "miles"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 419
    const-string v22, "mi"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setDistanceUnits(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 422
    .end local v20           #units:Ljava/lang/String;
    :cond_18
    const-string v22, "unit_weight"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 423
    const-string v22, "unit_weight"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 425
    .restart local v20       #units:Ljava/lang/String;
    const-string v22, "kilograms"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_19

    .line 426
    const-string v22, "kg"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWeightUnits(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 427
    :cond_19
    const-string v22, "pounds"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 428
    const-string v22, "lb"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWeightUnits(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 436
    .end local v5           #configObject:Lorg/json/JSONObject;
    .end local v6           #configsJSONArray:Lorg/json/JSONArray;
    .end local v7           #configsLength:I
    .end local v9           #dataObject:Lorg/json/JSONObject;
    .end local v14           #i:I
    .end local v19           #resultObject:Lorg/json/JSONObject;
    .end local v20           #units:Ljava/lang/String;
    .restart local v18       #resultObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v11

    goto/16 :goto_3

    .end local v18           #resultObject:Lorg/json/JSONObject;
    .restart local v19       #resultObject:Lorg/json/JSONObject;
    :cond_1a
    move-object/from16 v18, v19

    .end local v19           #resultObject:Lorg/json/JSONObject;
    .restart local v18       #resultObject:Lorg/json/JSONObject;
    goto/16 :goto_0
.end method

.method private static processDayMeals(Landroid/content/Context;JLorg/json/JSONArray;J)Z
    .locals 14
    .parameter "c"
    .parameter "currentSyncStartTime"
    .parameter "mealsJSONArray"
    .parameter "dateTimeInMillis"

    .prologue
    .line 1280
    const/4 v10, 0x0

    .line 1281
    .local v10, mealObject:Lorg/json/JSONObject;
    const/4 v5, 0x0

    .line 1283
    .local v5, foodItemsArray:Lorg/json/JSONArray;
    const/4 v9, 0x0

    .line 1284
    .local v9, meal:Lorg/medhelp/mydiet/model/Meal;
    const/4 v2, 0x0

    .line 1287
    .local v2, cv:Landroid/content/ContentValues;
    move-object/from16 v0, p3

    invoke-static {p0, v0}, Lorg/medhelp/mydiet/util/SyncUtil;->handleMissingFoods(Landroid/content/Context;Lorg/json/JSONArray;)Z

    move-result v11

    .line 1289
    .local v11, missingFoodsCheck:Z
    if-nez v11, :cond_0

    .line 1290
    const/4 v12, 0x0

    .line 1322
    :goto_0
    return v12

    .line 1294
    :cond_0
    invoke-static/range {p0 .. p5}, Lorg/medhelp/mydiet/util/SyncUtil;->deleteDayMealsIfNecessary(Landroid/content/Context;JLorg/json/JSONArray;J)V

    .line 1297
    invoke-virtual/range {p3 .. p3}, Lorg/json/JSONArray;->length()I

    move-result v8

    .line 1298
    .local v8, itemsLength:I
    const/4 v6, 0x0

    .local v6, i:I
    move-object v3, v2

    .end local v2           #cv:Landroid/content/ContentValues;
    .local v3, cv:Landroid/content/ContentValues;
    :goto_1
    if-lt v6, v8, :cond_1

    .line 1322
    const/4 v12, 0x1

    move-object v2, v3

    .end local v3           #cv:Landroid/content/ContentValues;
    .restart local v2       #cv:Landroid/content/ContentValues;
    goto :goto_0

    .line 1300
    .end local v2           #cv:Landroid/content/ContentValues;
    .restart local v3       #cv:Landroid/content/ContentValues;
    :cond_1
    :try_start_0
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    const-string v13, "Meal"

    invoke-virtual {v12, v13}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 1301
    if-nez v10, :cond_2

    move-object v2, v3

    .line 1298
    .end local v3           #cv:Landroid/content/ContentValues;
    .restart local v2       #cv:Landroid/content/ContentValues;
    :goto_2
    add-int/lit8 v6, v6, 0x1

    move-object v3, v2

    .end local v2           #cv:Landroid/content/ContentValues;
    .restart local v3       #cv:Landroid/content/ContentValues;
    goto :goto_1

    .line 1304
    :cond_2
    const-string v12, "foods"

    invoke-virtual {v10, v12}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 1305
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-nez v12, :cond_3

    move-object v2, v3

    .line 1306
    .end local v3           #cv:Landroid/content/ContentValues;
    .restart local v2       #cv:Landroid/content/ContentValues;
    goto :goto_2

    .line 1309
    .end local v2           #cv:Landroid/content/ContentValues;
    .restart local v3       #cv:Landroid/content/ContentValues;
    :cond_3
    invoke-virtual {v10}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v12

    move-wide/from16 v0, p4

    invoke-static {p0, v12, v0, v1}, Lorg/medhelp/mydiet/util/SyncUtil;->getMealFromServerMeal(Landroid/content/Context;Ljava/lang/String;J)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v9

    .line 1311
    sget-object v12, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_DAY_MEALS:Landroid/net/Uri;

    move-wide/from16 v0, p4

    invoke-static {v12, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    .line 1313
    .local v7, insertMealUri:Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1314
    .end local v3           #cv:Landroid/content/ContentValues;
    .restart local v2       #cv:Landroid/content/ContentValues;
    :try_start_1
    const-string v12, "meal"

    invoke-virtual {v9}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-virtual {v12, v7, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 1317
    :catch_0
    move-exception v4

    .line 1318
    .end local v7           #insertMealUri:Landroid/net/Uri;
    .local v4, e:Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    .line 1319
    const/4 v12, 0x0

    goto :goto_0

    .line 1317
    .end local v2           #cv:Landroid/content/ContentValues;
    .end local v4           #e:Lorg/json/JSONException;
    .restart local v3       #cv:Landroid/content/ContentValues;
    :catch_1
    move-exception v4

    move-object v2, v3

    .end local v3           #cv:Landroid/content/ContentValues;
    .restart local v2       #cv:Landroid/content/ContentValues;
    goto :goto_3

    .end local v2           #cv:Landroid/content/ContentValues;
    .restart local v3       #cv:Landroid/content/ContentValues;
    :cond_4
    move-object v2, v3

    .end local v3           #cv:Landroid/content/ContentValues;
    .restart local v2       #cv:Landroid/content/ContentValues;
    goto :goto_2
.end method

.method private static processExerciseItemsDayData(Landroid/content/Context;JLorg/json/JSONArray;Ljava/lang/String;)V
    .locals 31
    .parameter "c"
    .parameter "currentSyncStartTime"
    .parameter "itemsJSONArray"
    .parameter "dateString"

    .prologue
    .line 950
    const-string v2, "yyyy-MM-dd"

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lorg/medhelp/mydiet/util/DateUtil;->getDateFromString(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v27

    .line 951
    .local v27, keyDate:Ljava/util/Date;
    invoke-static/range {p0 .. p4}, Lorg/medhelp/mydiet/util/SyncUtil;->deleteExerciseItemsForDay(Landroid/content/Context;JLorg/json/JSONArray;Ljava/lang/String;)V

    .line 954
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 956
    .local v9, cv:Landroid/content/ContentValues;
    const/16 v26, 0x0

    .line 957
    .local v26, itemJSON:Lorg/json/JSONObject;
    const-string v23, ""

    .line 958
    .local v23, exerciseId:Ljava/lang/String;
    const-string v24, ""

    .line 960
    .local v24, exerciseName:Ljava/lang/String;
    const-wide/16 v18, 0x0

    .line 961
    .local v18, eiTime:J
    const-wide/16 v20, 0x0

    .line 962
    .local v20, eiTimePeriod:D
    const-string v22, ""

    .line 963
    .local v22, eiType:Ljava/lang/String;
    const-wide/16 v12, 0x0

    .line 964
    .local v12, eiCalories:D
    const-wide/16 v16, 0x0

    .line 965
    .local v16, eiSpeed:D
    const-wide/16 v14, 0x0

    .line 967
    .local v14, eiDistance:D
    const/16 v28, 0x0

    .line 969
    .local v28, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    const/16 v25, 0x0

    .local v25, i:I
    :goto_0
    invoke-virtual/range {p3 .. p3}, Lorg/json/JSONArray;->length()I

    move-result v2

    move/from16 v0, v25

    if-lt v0, v2, :cond_0

    .line 1033
    return-void

    .line 971
    :cond_0
    :try_start_0
    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v26

    .line 973
    invoke-virtual/range {v26 .. v26}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v28

    .line 974
    :cond_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_9

    .line 983
    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 984
    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISES:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "medhelp_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    aput-object v30, v6, v7

    const/4 v7, 0x0

    .line 983
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 986
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 987
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 988
    const-string v2, "name"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 989
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 992
    :cond_2
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 993
    .end local v9           #cv:Landroid/content/ContentValues;
    .local v10, cv:Landroid/content/ContentValues;
    :try_start_1
    const-string v2, "date"

    invoke-virtual/range {v27 .. v27}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 994
    const-string v2, "medhelp_id"

    move-object/from16 v0, v23

    invoke-virtual {v10, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    const-string v2, "name"

    move-object/from16 v0, v24

    invoke-virtual {v10, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "type"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 998
    if-eqz v22, :cond_3

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 999
    const-string v2, "exercise_type"

    move-object/from16 v0, v22

    invoke-virtual {v10, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    :cond_3
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "distance"

    const-wide/high16 v4, -0x4010

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v14

    .line 1003
    const-wide/16 v2, 0x0

    cmpl-double v2, v14, v2

    if-ltz v2, :cond_4

    .line 1004
    const-string v2, "distance"

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1007
    :cond_4
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "speed"

    const-wide/high16 v4, -0x4010

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v16

    .line 1008
    const-wide/16 v2, 0x0

    cmpl-double v2, v16, v2

    if-ltz v2, :cond_5

    .line 1009
    const-string v2, "speed"

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1012
    :cond_5
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "calories"

    const-wide/high16 v4, -0x4010

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v12

    .line 1013
    const-wide/16 v2, 0x0

    cmpl-double v2, v12, v2

    if-ltz v2, :cond_6

    .line 1014
    const-string v2, "calories"

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1017
    :cond_6
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "time"

    const-wide/high16 v4, -0x4010

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v20

    .line 1018
    const-wide/16 v2, 0x0

    cmpl-double v2, v20, v2

    if-ltz v2, :cond_7

    .line 1019
    const-string v2, "time_period"

    invoke-static/range {v20 .. v21}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1022
    :cond_7
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "exercise_time"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v18

    .line 1023
    const-wide/16 v2, 0x0

    cmp-long v2, v18, v2

    if-ltz v2, :cond_8

    .line 1024
    const-string v2, "time"

    invoke-virtual/range {v27 .. v27}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v5, v5, v18

    add-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1027
    :cond_8
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_EXERCISE_ITEMS:Landroid/net/Uri;

    invoke-virtual {v2, v3, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v9, v10

    .line 969
    .end local v8           #cursor:Landroid/database/Cursor;
    .end local v10           #cv:Landroid/content/ContentValues;
    .restart local v9       #cv:Landroid/content/ContentValues;
    :goto_2
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_0

    .line 975
    :cond_9
    :try_start_2
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 977
    .local v29, temp:Ljava/lang/String;
    const-string v2, "updated_at"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v2

    if-nez v2, :cond_1

    .line 978
    move-object/from16 v23, v29

    .line 979
    goto/16 :goto_1

    .line 1028
    .end local v29           #temp:Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 1029
    .local v11, e:Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 1028
    .end local v9           #cv:Landroid/content/ContentValues;
    .end local v11           #e:Lorg/json/JSONException;
    .restart local v8       #cursor:Landroid/database/Cursor;
    .restart local v10       #cv:Landroid/content/ContentValues;
    :catch_1
    move-exception v11

    move-object v9, v10

    .end local v10           #cv:Landroid/content/ContentValues;
    .restart local v9       #cv:Landroid/content/ContentValues;
    goto :goto_3
.end method

.method private static processMealsDayData(Landroid/content/Context;JLorg/json/JSONArray;Ljava/lang/String;)Z
    .locals 7
    .parameter "c"
    .parameter "currentSyncStartTime"
    .parameter "itemsJSONArray"
    .parameter "dateString"

    .prologue
    .line 1271
    if-nez p3, :cond_0

    const/4 v0, 0x1

    .line 1276
    :goto_0
    return v0

    .line 1273
    :cond_0
    const-string v0, "yyyy-MM-dd"

    invoke-static {p4, v0}, Lorg/medhelp/mydiet/util/DateUtil;->getDateFromString(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    .line 1276
    .local v6, keyDate:Ljava/util/Date;
    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lorg/medhelp/mydiet/util/SyncUtil;->processDayMeals(Landroid/content/Context;JLorg/json/JSONArray;J)Z

    move-result v0

    goto :goto_0
.end method

.method private static processWaterDayData(Landroid/content/Context;JDLjava/lang/String;)V
    .locals 13
    .parameter "c"
    .parameter "currentSyncStartTime"
    .parameter "waterInFlOz"
    .parameter "dateString"

    .prologue
    .line 578
    const-string v1, "yyyy-MM-dd"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/DateUtil;->getDateFromString(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v9

    .line 580
    .local v9, keyDate:Ljava/util/Date;
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 581
    .local v8, cv:Landroid/content/ContentValues;
    const-string v1, "amount"

    const-wide/high16 v2, 0x3fc0

    mul-double v2, v2, p3

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 582
    const-string v1, "last_updated_time"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 584
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 585
    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WATER:Landroid/net/Uri;

    const/4 v3, 0x0

    const-string v4, "date=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v6

    const/4 v6, 0x0

    .line 584
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 586
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 587
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 588
    const-string v1, "last_updated_time"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    cmp-long v1, p1, v1

    if-lez v1, :cond_0

    .line 590
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 591
    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_DAY_WATER:Landroid/net/Uri;

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 592
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 590
    invoke-virtual {v1, v2, v8, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 604
    :cond_0
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 610
    :goto_1
    return-void

    .line 601
    :cond_1
    const-string v1, "date"

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 602
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WATER:Landroid/net/Uri;

    invoke-virtual {v1, v2, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    .line 607
    :cond_2
    const-string v1, "date"

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 608
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WATER:Landroid/net/Uri;

    invoke-virtual {v1, v2, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1
.end method

.method private static processWeightDayData(Landroid/content/Context;JDLjava/lang/String;)V
    .locals 13
    .parameter "c"
    .parameter "currentSyncStartTime"
    .parameter "weight"
    .parameter "dateString"

    .prologue
    .line 706
    const-string v1, "yyyy-MM-dd"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lorg/medhelp/mydiet/util/DateUtil;->getDateFromString(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v9

    .line 708
    .local v9, keyDate:Ljava/util/Date;
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 709
    .local v8, cv:Landroid/content/ContentValues;
    const-string v1, "weight"

    invoke-static/range {p3 .. p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 710
    const-string v1, "last_updated_time"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 712
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 713
    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WEIGHT:Landroid/net/Uri;

    const/4 v3, 0x0

    const-string v4, "date=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v6

    const/4 v6, 0x0

    .line 712
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 714
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 715
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 716
    const-string v1, "last_updated_time"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    cmp-long v1, p1, v1

    if-lez v1, :cond_0

    .line 718
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 719
    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_DAY_WEIGHT:Landroid/net/Uri;

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 720
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 718
    invoke-virtual {v1, v2, v8, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 732
    :cond_0
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 738
    :goto_1
    return-void

    .line 729
    :cond_1
    const-string v1, "date"

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 730
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WEIGHT:Landroid/net/Uri;

    invoke-virtual {v1, v2, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    .line 735
    :cond_2
    const-string v1, "date"

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 736
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_WEIGHT:Landroid/net/Uri;

    invoke-virtual {v1, v2, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1
.end method

.method public static setConfigsLastSyncTime(Landroid/content/Context;J)V
    .locals 4
    .parameter "context"
    .parameter "lastSyncTime"

    .prologue
    .line 40
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 41
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 42
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "configs_last_sync_time"

    invoke-interface {v0, v2, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 43
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 44
    return-void
.end method

.method public static setSyncExercisesStatus(Landroid/content/Context;Z)V
    .locals 5
    .parameter "c"
    .parameter "isSyncActive"

    .prologue
    .line 72
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 73
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 74
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "sync_exercises_active"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 75
    const-string v2, "last_sync_exercises"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 76
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 77
    return-void
.end method

.method public static setSyncMealsStatus(Landroid/content/Context;Z)V
    .locals 5
    .parameter "c"
    .parameter "isSyncActive"

    .prologue
    .line 54
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 55
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 56
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "sync_meals_active"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 57
    const-string v2, "last_sync_meals"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 58
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 59
    return-void
.end method

.method public static setSyncWaterStatus(Landroid/content/Context;Z)V
    .locals 5
    .parameter "c"
    .parameter "isSyncActive"

    .prologue
    .line 90
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 91
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 92
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "sync_water_active"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 93
    const-string v2, "last_sync_water"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 94
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 95
    return-void
.end method

.method public static setSyncWeightStatus(Landroid/content/Context;Z)V
    .locals 5
    .parameter "c"
    .parameter "isSyncActive"

    .prologue
    .line 108
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 109
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 110
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "sync_weight_active"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 111
    const-string v2, "last_sync_weight"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 112
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 113
    return-void
.end method

.method public static updateFoodWithUpdatedIds(Landroid/content/Context;JJJ)V
    .locals 18
    .parameter "c"
    .parameter "inAppFoodId"
    .parameter "foodMHId"
    .parameter "servingMHId"

    .prologue
    .line 1614
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gtz v2, :cond_1

    .line 1653
    :cond_0
    :goto_0
    return-void

    .line 1616
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    const/4 v4, 0x0

    .line 1617
    const-string v5, "_id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v6, v7

    const/4 v7, 0x0

    .line 1616
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1619
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 1620
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1621
    const-string v2, "food"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1622
    .local v12, foodJSON:Ljava/lang/String;
    const-string v2, "servings"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1624
    .local v13, foodServingsJSON:Ljava/lang/String;
    invoke-static {v12, v13}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodDetail(Ljava/lang/String;Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v11

    .line 1626
    .local v11, foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    move-wide/from16 v0, p3

    iput-wide v0, v11, Lorg/medhelp/mydiet/model/FoodDetail;->medhelpId:J

    .line 1628
    const/4 v15, 0x0

    .line 1629
    .local v15, serving:Lorg/medhelp/mydiet/model/FoodServing;
    iget-object v2, v11, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, v11, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1630
    iget-object v2, v11, Lorg/medhelp/mydiet/model/FoodDetail;->servings:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    .end local v15           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    check-cast v15, Lorg/medhelp/mydiet/model/FoodServing;

    .line 1631
    .restart local v15       #serving:Lorg/medhelp/mydiet/model/FoodServing;
    move-wide/from16 v0, p5

    iput-wide v0, v15, Lorg/medhelp/mydiet/model/FoodServing;->medhelpId:J

    .line 1635
    :cond_2
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 1637
    .local v14, foodWithServings:Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "food"

    invoke-virtual {v11}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodJSON()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1638
    const-string v2, "food_servings"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v15}, Lorg/medhelp/mydiet/model/FoodServing;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1643
    :goto_1
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1644
    .local v9, cv:Landroid/content/ContentValues;
    const-string v2, "medhelp_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1645
    const-string v2, "food"

    invoke-virtual {v11}, Lorg/medhelp/mydiet/model/FoodDetail;->getFoodJSON()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    const-string v2, "servings"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v15}, Lorg/medhelp/mydiet/model/FoodServing;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1647
    const-string v2, "serving_id"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1649
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_FOODS:Landroid/net/Uri;

    const-string v4, "_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v9, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1651
    .end local v9           #cv:Landroid/content/ContentValues;
    .end local v11           #foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    .end local v12           #foodJSON:Ljava/lang/String;
    .end local v13           #foodServingsJSON:Ljava/lang/String;
    .end local v14           #foodWithServings:Lorg/json/JSONObject;
    .end local v15           #serving:Lorg/medhelp/mydiet/model/FoodServing;
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1639
    .restart local v11       #foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    .restart local v12       #foodJSON:Ljava/lang/String;
    .restart local v13       #foodServingsJSON:Ljava/lang/String;
    .restart local v14       #foodWithServings:Lorg/json/JSONObject;
    .restart local v15       #serving:Lorg/medhelp/mydiet/model/FoodServing;
    :catch_0
    move-exception v10

    .line 1640
    .local v10, e:Lorg/json/JSONException;
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public static updateFoodsWithMedHelpIds(Landroid/content/Context;Lorg/json/JSONArray;)V
    .locals 1
    .parameter "c"
    .parameter "medhelpIds"

    .prologue
    .line 1656
    invoke-static {p0, p1}, Lorg/medhelp/mydiet/http/MHHttpHandler;->requestFoods(Landroid/content/Context;Lorg/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    .line 1657
    .local v0, foodsUpdateResponse:Ljava/lang/String;
    invoke-static {p0, v0}, Lorg/medhelp/mydiet/util/SyncUtil;->handleFoodsUpdateResponse(Landroid/content/Context;Ljava/lang/String;)V

    .line 1658
    return-void
.end method

.method public static updateSyncStatus(Landroid/content/Context;JLjava/lang/String;)V
    .locals 10
    .parameter "c"
    .parameter "syncMonthStartTime"
    .parameter "syncCategory"

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 442
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 447
    sget-object v1, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    const-string v3, "month_year_timestamp=?"

    new-array v4, v9, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    move-object v5, v2

    .line 446
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 449
    .local v6, cursor:Landroid/database/Cursor;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 450
    .local v7, cv:Landroid/content/ContentValues;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, p3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 452
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 453
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 454
    sget-object v1, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    const-string v2, "month_year_timestamp=?"

    new-array v3, v9, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    .line 453
    invoke-virtual {v0, v1, v7, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 460
    :goto_1
    if-eqz v6, :cond_0

    .line 461
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 456
    :cond_2
    const-string v0, "month_year_timestamp"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 457
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lorg/medhelp/mydiet/provider/MDDProvider;->URI_SYNC_STATUS:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1
.end method
