.class public interface abstract Lorg/medhelp/mydiet/C$userAccount;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "userAccount"
.end annotation


# static fields
.field public static final CONFIGS_LAST_SYNC_TIME:Ljava/lang/String; = "configs_last_sync_time"

.field public static final LAST_LOGIN_PROMPT_TIME:Ljava/lang/String; = "last_login_time"

.field public static final LAST_SYNC_EXERCISES:Ljava/lang/String; = "last_sync_exercises"

.field public static final LAST_SYNC_MEALS:Ljava/lang/String; = "last_sync_meals"

.field public static final LAST_SYNC_TIME:Ljava/lang/String; = "last_sync_time"

.field public static final LAST_SYNC_WATER:Ljava/lang/String; = "last_sync_water"

.field public static final LAST_SYNC_WEIGHT:Ljava/lang/String; = "last_sync_weight"

.field public static final LUT_ACTIVITY_LEVEL:Ljava/lang/String; = "lut_activity_level"

.field public static final LUT_AVATAR:Ljava/lang/String; = "lut_avatar"

.field public static final LUT_BIRTHDAY:Ljava/lang/String; = "lut_birthday"

.field public static final LUT_BMR:Ljava/lang/String; = "lut_bmr"

.field public static final LUT_DISTANCE_UNITS:Ljava/lang/String; = "lut_distance_units"

.field public static final LUT_GENDER:Ljava/lang/String; = "lut_gender"

.field public static final LUT_GOALS:Ljava/lang/String; = "lut_goals"

.field public static final LUT_HEIGHT:Ljava/lang/String; = "lut_height"

.field public static final LUT_HEIGHT_UNITS:Ljava/lang/String; = "lut_height_units"

.field public static final LUT_WATER_UNITS:Ljava/lang/String; = "lut_water_units"

.field public static final LUT_WEIGHT:Ljava/lang/String; = "lut_weight"

.field public static final LUT_WEIGHT_UNITS:Ljava/lang/String; = "lut_weight_units"

.field public static final PASSWORD:Ljava/lang/String; = "password"

.field public static final SYNC_EXERCISES_ACTIVE:Ljava/lang/String; = "sync_exercises_active"

.field public static final SYNC_MEALS_ACTIVE:Ljava/lang/String; = "sync_meals_active"

.field public static final SYNC_WATER_ACTIVE:Ljava/lang/String; = "sync_water_active"

.field public static final SYNC_WEIGHT_ACTIVE:Ljava/lang/String; = "sync_weight_active"

.field public static final USER_FILE:Ljava/lang/String; = "mh_user"

.field public static final USER_ID:Ljava/lang/String; = "user_id"

.field public static final USER_NAME:Ljava/lang/String; = "user_name"

.field public static final USER_TYPE:Ljava/lang/String; = "user_type"

.field public static final USER_TYPE_ANONYMOUS:Ljava/lang/String; = "anonymous"

.field public static final USER_TYPE_MEDHELP:Ljava/lang/String; = "medhelp"

.field public static final USER_TYPE_NONE:Ljava/lang/String; = "none"
