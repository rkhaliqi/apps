.class Lorg/medhelp/mydiet/activity/TabTrackItActivity$2;
.super Ljava/lang/Object;
.source "TabTrackItActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/TabTrackItActivity;->onFoodClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

.field private final synthetic val$mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/adapter/MealTypesAdapter;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$2;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    iput-object p2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$2;->val$mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 278
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$2;->val$mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    invoke-virtual {v2, p2}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v1

    .line 280
    .local v1, mealtype:Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$2;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    const-class v3, Lorg/medhelp/mydiet/activity/foods/AddFoodActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 281
    .local v0, addFoodsIntent:Landroid/content/Intent;
    const-string v2, "date"

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$2;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    #getter for: Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->access$1(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 282
    const-string v2, "meal_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$2;->this$0:Lorg/medhelp/mydiet/activity/TabTrackItActivity;

    const/16 v3, 0xbb9

    invoke-virtual {v2, v0, v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 284
    return-void
.end method
