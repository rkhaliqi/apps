.class public Lorg/medhelp/mydiet/model/DBInitializer;
.super Ljava/lang/Object;
.source "DBInitializer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static readExercisesFromAssets(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 9
    .parameter "c"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Exercise;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/high16 v8, 0x7f05

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 21
    .local v2, inputStream:Ljava/io/InputStream;
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-direct {v7, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 22
    .local v6, reader:Ljava/io/BufferedReader;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .local v0, builder:Ljava/lang/StringBuilder;
    :goto_0
    :try_start_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .local v3, line:Ljava/lang/String;
    if-nez v3, :cond_0

    .line 32
    :try_start_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 38
    .end local v3           #line:Ljava/lang/String;
    :goto_1
    const/4 v4, 0x0

    .line 41
    .local v4, obj:Lorg/json/JSONObject;
    :try_start_2
    new-instance v5, Lorg/json/JSONObject;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_4

    .line 42
    .end local v4           #obj:Lorg/json/JSONObject;
    .local v5, obj:Lorg/json/JSONObject;
    :try_start_3
    const-string v7, "data"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    invoke-virtual {v7}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/medhelp/mydiet/util/DataUtil;->getExercises(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_5

    move-result-object v7

    move-object v4, v5

    .line 47
    .end local v5           #obj:Lorg/json/JSONObject;
    .restart local v4       #obj:Lorg/json/JSONObject;
    :goto_2
    return-object v7

    .line 26
    .end local v4           #obj:Lorg/json/JSONObject;
    .restart local v3       #line:Ljava/lang/String;
    :cond_0
    :try_start_4
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 28
    .end local v3           #line:Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 29
    .local v1, e:Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 32
    :try_start_6
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 33
    :catch_1
    move-exception v1

    .line 34
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 30
    .end local v1           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 32
    :try_start_7
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 36
    :goto_3
    throw v7

    .line 33
    :catch_2
    move-exception v1

    .line 34
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 33
    .end local v1           #e:Ljava/io/IOException;
    .restart local v3       #line:Ljava/lang/String;
    :catch_3
    move-exception v1

    .line 34
    .restart local v1       #e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 43
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #line:Ljava/lang/String;
    .restart local v4       #obj:Lorg/json/JSONObject;
    :catch_4
    move-exception v1

    .line 44
    .local v1, e:Lorg/json/JSONException;
    :goto_4
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 47
    const/4 v7, 0x0

    goto :goto_2

    .line 43
    .end local v1           #e:Lorg/json/JSONException;
    .end local v4           #obj:Lorg/json/JSONObject;
    .restart local v5       #obj:Lorg/json/JSONObject;
    :catch_5
    move-exception v1

    move-object v4, v5

    .end local v5           #obj:Lorg/json/JSONObject;
    .restart local v4       #obj:Lorg/json/JSONObject;
    goto :goto_4
.end method
