.class Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;
.super Ljava/lang/Object;
.source "MealsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/foods/MealsActivity;->deleteMeal(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

.field private final synthetic val$mealPosition:I


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    iput p2, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;->val$mealPosition:I

    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v5, 0x1

    .line 247
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    iget-object v2, v2, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 248
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    #setter for: Lorg/medhelp/mydiet/activity/foods/MealsActivity;->hasEdits:Z
    invoke-static {v2, v5}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/MealsActivity;Z)V

    .line 249
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    iget-object v2, v2, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMeals:Ljava/util/ArrayList;

    iget v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;->val$mealPosition:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/Meal;

    iget-wide v0, v2, Lorg/medhelp/mydiet/model/Meal;->id:J

    .line 250
    .local v0, mealId:J
    new-instance v2, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;-><init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;)V

    new-array v3, v5, [Ljava/lang/Long;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 251
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;->this$0:Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    iget-object v2, v2, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMeals:Ljava/util/ArrayList;

    iget v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;->val$mealPosition:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 252
    return-void
.end method
