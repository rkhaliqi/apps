.class Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$1;
.super Ljava/lang/Object;
.source "PickExerciseActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .parameter
    .parameter "arg1"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    new-instance v1, Lorg/medhelp/mydiet/model/ExerciseItem;

    invoke-direct {v1}, Lorg/medhelp/mydiet/model/ExerciseItem;-><init>()V

    .line 60
    .local v1, exerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;
    const-string v3, "############ PickExerciseActivity "

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "position :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",  id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const-string v4, "############ PickExerciseActivity "

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v3, "Clicked on "

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    #getter for: Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mExercises:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->access$0(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/medhelp/mydiet/model/Exercise;

    iget-object v3, v3, Lorg/medhelp/mydiet/model/Exercise;->medHelpId:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    #getter for: Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mExercises:Ljava/util/ArrayList;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->access$0(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/medhelp/mydiet/model/Exercise;

    iget-object v3, v3, Lorg/medhelp/mydiet/model/Exercise;->medHelpId:Ljava/lang/String;

    iput-object v3, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseMHKey:Ljava/lang/String;

    .line 64
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    #getter for: Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->mDate:Ljava/util/Date;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->access$1(Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    iput-wide v3, v1, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    .line 65
    invoke-static {v1}, Lorg/medhelp/mydiet/model/TransientDataStore;->storeData(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, eiKey:Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    const-class v4, Lorg/medhelp/mydiet/activity/exercises/EditExerciseActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "transient_exercise_item"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity$1;->this$0:Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;

    const/16 v4, 0x7d0

    invoke-virtual {v3, v2, v4}, Lorg/medhelp/mydiet/activity/exercises/PickExerciseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 70
    return-void
.end method
