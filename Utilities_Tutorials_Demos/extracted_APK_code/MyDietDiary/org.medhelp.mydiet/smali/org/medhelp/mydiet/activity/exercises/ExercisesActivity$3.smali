.class Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;
.super Ljava/lang/Object;
.source "ExercisesActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->deleteExerciseItem(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

.field private final synthetic val$itemId:J

.field private final synthetic val$itemPosition:I


# direct methods
.method constructor <init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;JI)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    iput-wide p2, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;->val$itemId:J

    iput p4, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;->val$itemPosition:I

    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v3, 0x1

    .line 244
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 245
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    #setter for: Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->hasEdits:Z
    invoke-static {v0, v3}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->access$7(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;Z)V

    .line 246
    new-instance v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$DeleteExerciseItemTask;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$DeleteExerciseItemTask;-><init>(Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$DeleteExerciseItemTask;)V

    new-array v1, v3, [Ljava/lang/Long;

    const/4 v2, 0x0

    iget-wide v3, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;->val$itemId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$DeleteExerciseItemTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 247
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;->this$0:Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;->mItems:Ljava/util/ArrayList;

    iget v1, p0, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity$3;->val$itemPosition:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 248
    return-void
.end method
