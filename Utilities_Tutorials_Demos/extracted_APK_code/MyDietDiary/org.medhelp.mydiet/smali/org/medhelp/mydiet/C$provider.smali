.class public interface abstract Lorg/medhelp/mydiet/C$provider;
.super Ljava/lang/Object;
.source "C.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/C;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "provider"
.end annotation


# static fields
.field public static final AUTHORITY_MDD:Ljava/lang/String; = "org.medhelp.mydiet.provider.mddprovider"

.field public static final CONTENT:Ljava/lang/String; = "content://"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final EXERCISES:Ljava/lang/String; = "exercises"

.field public static final EXERCISE_ITEMS:Ljava/lang/String; = "exercise_items"

.field public static final FOODS:Ljava/lang/String; = "foods"

.field public static final FREQUENT:Ljava/lang/String; = "frequent"

.field public static final MEALS:Ljava/lang/String; = "meals"

.field public static final MH_ID:Ljava/lang/String; = "medhelp_id"

.field public static final PARAM_LIMIT:Ljava/lang/String; = "limit"

.field public static final PARAM_MAX_DATE:Ljava/lang/String; = "max_date"

.field public static final PARAM_MIN_DATE:Ljava/lang/String; = "min_date"

.field public static final PATH_DAY_EXERCISE_ITEMS:Ljava/lang/String; = "exercise_items/date"

.field public static final PATH_DAY_MEALS:Ljava/lang/String; = "meals/date"

.field public static final PATH_DAY_WATER:Ljava/lang/String; = "water/date"

.field public static final PATH_DAY_WEIGHT:Ljava/lang/String; = "weight/date"

.field public static final PATH_EXERCISES:Ljava/lang/String; = "exercises"

.field public static final PATH_EXERCISE_BY_ID:Ljava/lang/String; = "exercises/_id"

.field public static final PATH_EXERCISE_BY_MH_ID:Ljava/lang/String; = "exercises/medhelp_id"

.field public static final PATH_EXERCISE_ITEMS:Ljava/lang/String; = "exercise_items"

.field public static final PATH_EXERCISE_ITEMS_TO_SYNC:Ljava/lang/String; = "exercise_items/sync"

.field public static final PATH_FOODS:Ljava/lang/String; = "foods"

.field public static final PATH_FOODS_FREQUENT:Ljava/lang/String; = "foods/frequent"

.field public static final PATH_FOODS_RECENT:Ljava/lang/String; = "foods/recent"

.field public static final PATH_FOOD_BY_ID:Ljava/lang/String; = "foods/_id"

.field public static final PATH_FOOD_BY_MH_ID:Ljava/lang/String; = "foods/medhelp_id"

.field public static final PATH_MEALS:Ljava/lang/String; = "meals"

.field public static final PATH_SYNC_CLEAR:Ljava/lang/String; = "sync_status/clear"

.field public static final PATH_SYNC_STATUS:Ljava/lang/String; = "sync_status"

.field public static final PATH_WATER:Ljava/lang/String; = "water"

.field public static final PATH_WEIGHT:Ljava/lang/String; = "weight"

.field public static final RECENT:Ljava/lang/String; = "recent"

.field public static final SYNC:Ljava/lang/String; = "sync"

.field public static final WATER:Ljava/lang/String; = "water"

.field public static final WEIGHT:Ljava/lang/String; = "weight"

.field public static final _ID:Ljava/lang/String; = "_id"
