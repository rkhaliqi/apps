.class Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;
.super Landroid/os/AsyncTask;
.source "WeightActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/WeightActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadWeightTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/WeightActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/WeightActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 186
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/WeightActivity;Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;-><init>(Lorg/medhelp/mydiet/activity/WeightActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->doInBackground([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 4
    .parameter "params"

    .prologue
    .line 199
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 200
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    iget-object v2, v2, Lorg/medhelp/mydiet/activity/WeightActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getRecentWeightOnOrBefore(Ljava/util/Date;)D

    move-result-wide v2

    iput-wide v2, v1, Lorg/medhelp/mydiet/activity/WeightActivity;->mWeight:D

    .line 201
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 219
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->publishProgress([Ljava/lang/Object;)V

    .line 220
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    #calls: Lorg/medhelp/mydiet/activity/WeightActivity;->refreshViewContent()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/WeightActivity;->access$2(Lorg/medhelp/mydiet/activity/WeightActivity;)V

    .line 221
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 222
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 193
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 194
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->publishProgress([Ljava/lang/Object;)V

    .line 195
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 206
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 207
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 215
    :goto_0
    return-void

    .line 209
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    #calls: Lorg/medhelp/mydiet/activity/WeightActivity;->showProgressBar()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/WeightActivity;->access$0(Lorg/medhelp/mydiet/activity/WeightActivity;)V

    goto :goto_0

    .line 212
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->this$0:Lorg/medhelp/mydiet/activity/WeightActivity;

    #calls: Lorg/medhelp/mydiet/activity/WeightActivity;->hideProgressBar()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/WeightActivity;->access$1(Lorg/medhelp/mydiet/activity/WeightActivity;)V

    goto :goto_0

    .line 207
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/WeightActivity$LoadWeightTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
