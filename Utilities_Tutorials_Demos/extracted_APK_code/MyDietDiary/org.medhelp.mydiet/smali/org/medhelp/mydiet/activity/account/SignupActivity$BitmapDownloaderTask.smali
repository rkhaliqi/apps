.class Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;
.super Landroid/os/AsyncTask;
.source "SignupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/account/SignupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BitmapDownloaderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final imageViewReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;


# direct methods
.method public constructor <init>(Lorg/medhelp/mydiet/activity/account/SignupActivity;Landroid/widget/ImageView;)V
    .locals 1
    .parameter
    .parameter "imageView"

    .prologue
    .line 227
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 228
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    .line 229
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "params"

    .prologue
    .line 233
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lorg/medhelp/mydiet/http/MHHttpHandler;->downloadBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;->doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .parameter "bitmap"

    .prologue
    .line 238
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    const/4 p1, 0x0

    .line 242
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    .line 243
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 244
    .local v0, imageView:Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 245
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 248
    .end local v0           #imageView:Landroid/widget/ImageView;
    :cond_1
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;->this$0:Lorg/medhelp/mydiet/activity/account/SignupActivity;

    iget-object v1, v1, Lorg/medhelp/mydiet/activity/account/SignupActivity;->mCaptchaSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 249
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/SignupActivity$BitmapDownloaderTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
