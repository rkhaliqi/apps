.class public Lorg/medhelp/mydiet/activity/NotesActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "NotesActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 29
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 43
    :goto_0
    return-void

    .line 31
    :sswitch_0
    const v2, 0x7f0b00e1

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/NotesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 32
    .local v0, notesEditor:Landroid/widget/EditText;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 33
    .local v1, resultIntent:Landroid/content/Intent;
    const-string v2, "notes"

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 34
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lorg/medhelp/mydiet/activity/NotesActivity;->setResult(ILandroid/content/Intent;)V

    .line 36
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/NotesActivity;->finish()V

    goto :goto_0

    .line 40
    .end local v0           #notesEditor:Landroid/widget/EditText;
    .end local v1           #resultIntent:Landroid/content/Intent;
    :sswitch_1
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/NotesActivity;->finish()V

    goto :goto_0

    .line 29
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0081 -> :sswitch_0
        0x7f0b00e2 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 16
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 17
    const v2, 0x7f030024

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/NotesActivity;->setContentView(I)V

    .line 19
    const v2, 0x7f0b0081

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/NotesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    const v2, 0x7f0b00e2

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/NotesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/NotesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "notes"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    .local v0, note:Ljava/lang/String;
    const v2, 0x7f0b00e1

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/NotesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 24
    .local v1, notesEditor:Landroid/widget/EditText;
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 25
    return-void
.end method
