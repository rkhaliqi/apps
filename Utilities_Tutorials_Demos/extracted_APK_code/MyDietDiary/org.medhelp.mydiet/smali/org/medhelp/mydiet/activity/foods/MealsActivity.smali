.class public Lorg/medhelp/mydiet/activity/foods/MealsActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "MealsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/foods/MealsActivity$DeleteMealTask;,
        Lorg/medhelp/mydiet/activity/foods/MealsActivity$LoadMealsTask;,
        Lorg/medhelp/mydiet/activity/foods/MealsActivity$SyncUserDataTask;
    }
.end annotation


# instance fields
.field private hasEdits:Z

.field private mDate:Ljava/util/Date;

.field mDialog:Landroid/app/AlertDialog;

.field private mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

.field mMeals:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Meal;",
            ">;"
        }
    .end annotation
.end field

.field private mMealsLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->hasEdits:Z

    .line 45
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)Ljava/util/Date;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 338
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->showAlertDialog()V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 350
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->hideAlertDialog()V

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 127
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->refreshMealsContent()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/foods/MealsActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 237
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->deleteMeal(I)V

    return-void
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/foods/MealsActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    iput-boolean p1, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->hasEdits:Z

    return-void
.end method

.method static synthetic access$6(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)Lorg/medhelp/mydiet/adapter/MealTypesAdapter;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    return-object v0
.end method

.method private deleteMeal(I)V
    .locals 3
    .parameter "mealPosition"

    .prologue
    .line 238
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 239
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v1, "Delete Meal?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 241
    const-string v1, "Are you sure you want to delete this meal?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 243
    const-string v1, "Yes"

    new-instance v2, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;

    invoke-direct {v2, p0, p1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$3;-><init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 254
    const-string v1, "Cancel"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 256
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    .line 257
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 258
    return-void
.end method

.method private finishAndShowDetails()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->setResult(I)V

    .line 124
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->finish()V

    .line 125
    return-void
.end method

.method private hideAlertDialog()V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 354
    :cond_0
    return-void
.end method

.method private onAddMealClick()V
    .locals 8

    .prologue
    .line 357
    invoke-static {p0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v1

    .line 358
    .local v1, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v1, v6}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getAvailableMealTypesForDay(Ljava/util/Date;)Ljava/util/ArrayList;

    move-result-object v5

    .line 359
    .local v5, mealTypesSavedForDay:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v5, :cond_0

    new-instance v5, Ljava/util/ArrayList;

    .end local v5           #mealTypesSavedForDay:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 361
    .restart local v5       #mealTypesSavedForDay:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 362
    .local v4, mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "Breakfast"

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    const-string v6, "Lunch"

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    const-string v6, "Dinner"

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 374
    const-string v6, "Snack"

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    new-instance v6, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    invoke-direct {v6, p0, v4}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v6, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    .line 378
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 379
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    new-instance v7, Lorg/medhelp/mydiet/activity/foods/MealsActivity$4;

    invoke-direct {v7, p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$4;-><init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 392
    const-string v6, "Select a meal type"

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 393
    const-string v6, "cancel"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 395
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    .line 396
    iget-object v6, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 397
    return-void

    .line 366
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 367
    .local v3, mealTypeSaved:Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 368
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 369
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 367
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private refreshMealsContent()V
    .locals 31

    .prologue
    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMealsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMeals:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    if-eqz v27, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMeals:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    if-nez v27, :cond_2

    .line 131
    :cond_0
    new-instance v22, Landroid/content/Intent;

    const-class v27, Lorg/medhelp/mydiet/activity/foods/MealTypeActivity;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    .local v22, mealTypeIntent:Landroid/content/Intent;
    const-string v27, "date"

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDate:Ljava/util/Date;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/Date;->getTime()J

    move-result-wide v28

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    move-wide/from16 v2, v28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 133
    const/16 v27, 0xbb9

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 134
    const/16 v27, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->setResult(I)V

    .line 135
    invoke-virtual/range {p0 .. p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->finish()V

    .line 235
    .end local v22           #mealTypeIntent:Landroid/content/Intent;
    :cond_1
    return-void

    .line 139
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    const-string v28, "layout_inflater"

    invoke-virtual/range {v27 .. v28}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/view/LayoutInflater;

    .line 140
    .local v19, inflater:Landroid/view/LayoutInflater;
    const/16 v23, 0x0

    .line 141
    .local v23, rlMealItem:Landroid/widget/RelativeLayout;
    const/4 v13, 0x0

    .line 143
    .local v13, divider:Landroid/view/View;
    const/16 v26, 0x0

    .line 144
    .local v26, tv:Landroid/widget/TextView;
    const/4 v6, 0x0

    .line 146
    .local v6, button:Landroid/widget/ImageButton;
    const-wide/16 v7, 0x0

    .line 147
    .local v7, caloriesInFood:D
    const-wide/16 v9, 0x0

    .line 149
    .local v9, caloriesInMeal:D
    const/16 v18, 0x0

    .local v18, i:I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMeals:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v18

    move/from16 v1, v27

    if-ge v0, v1, :cond_1

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMeals:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lorg/medhelp/mydiet/model/Meal;

    .line 151
    .local v21, meal:Lorg/medhelp/mydiet/model/Meal;
    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 152
    .local v5, __size:I
    const-wide/16 v9, 0x0

    .line 153
    const-wide/16 v7, 0x0

    .line 155
    const v27, 0x7f030018

    const/16 v28, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v23

    .end local v23           #rlMealItem:Landroid/widget/RelativeLayout;
    check-cast v23, Landroid/widget/RelativeLayout;

    .line 156
    .restart local v23       #rlMealItem:Landroid/widget/RelativeLayout;
    const v27, 0x7f0b0049

    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v26

    .end local v26           #tv:Landroid/widget/TextView;
    check-cast v26, Landroid/widget/TextView;

    .line 157
    .restart local v26       #tv:Landroid/widget/TextView;
    invoke-virtual/range {v21 .. v21}, Lorg/medhelp/mydiet/model/Meal;->getMealTypeString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    const v27, 0x7f0b00bf

    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v26

    .end local v26           #tv:Landroid/widget/TextView;
    check-cast v26, Landroid/widget/TextView;

    .line 159
    .restart local v26       #tv:Landroid/widget/TextView;
    invoke-virtual/range {v21 .. v21}, Lorg/medhelp/mydiet/model/Meal;->getFoodTitles()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    const v27, 0x7f0b00be

    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v26

    .end local v26           #tv:Landroid/widget/TextView;
    check-cast v26, Landroid/widget/TextView;

    .line 162
    .restart local v26       #tv:Landroid/widget/TextView;
    new-instance v25, Ljava/text/DecimalFormatSymbols;

    new-instance v27, Ljava/util/Locale;

    const-string v28, "en"

    const-string v29, "en"

    invoke-direct/range {v27 .. v29}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 163
    .local v25, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v27, 0x2e

    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 164
    new-instance v12, Ljava/text/DecimalFormat;

    const-string v27, "0.0"

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-direct {v12, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 166
    .local v12, df:Ljava/text/DecimalFormat;
    if-lez v5, :cond_3

    .line 167
    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    .line 168
    .local v17, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    const/16 v16, 0x0

    .line 169
    .local v16, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const/4 v14, 0x0

    .line 170
    .local v14, food:Lorg/medhelp/mydiet/model/Food;
    const/4 v15, 0x0

    .line 172
    .local v15, foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    const/16 v20, 0x0

    .local v20, j:I
    :goto_1
    move/from16 v0, v20

    if-lt v0, v5, :cond_5

    .line 193
    .end local v14           #food:Lorg/medhelp/mydiet/model/Food;
    .end local v15           #foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    .end local v16           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .end local v17           #foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    .end local v20           #j:I
    :cond_3
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-virtual {v12, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v28, " cal"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    move-object/from16 v0, v21

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    move-wide/from16 v27, v0

    const-wide/16 v29, 0x0

    cmp-long v27, v27, v29

    if-lez v27, :cond_4

    .line 196
    const v27, 0x7f0b00bd

    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v26

    .end local v26           #tv:Landroid/widget/TextView;
    check-cast v26, Landroid/widget/TextView;

    .line 197
    .restart local v26       #tv:Landroid/widget/TextView;
    new-instance v27, Ljava/util/Date;

    move-object/from16 v0, v21

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Meal;->mealTime:J

    move-wide/from16 v28, v0

    invoke-direct/range {v27 .. v29}, Ljava/util/Date;-><init>(J)V

    const-string v28, "h:mm aa"

    invoke-static/range {v27 .. v28}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    :cond_4
    const v27, 0x7f0b00bc

    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6           #button:Landroid/widget/ImageButton;
    check-cast v6, Landroid/widget/ImageButton;

    .line 201
    .restart local v6       #button:Landroid/widget/ImageButton;
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 202
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 204
    new-instance v27, Lorg/medhelp/mydiet/activity/foods/MealsActivity$1;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$1;-><init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    new-instance v27, Lorg/medhelp/mydiet/activity/foods/MealsActivity$2;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$2;-><init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;)V

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMealsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v27, v0

    mul-int/lit8 v28, v18, 0x2

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 231
    const v27, 0x7f03001d

    const/16 v28, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    .line 232
    new-instance v27, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v28, -0x1

    const/16 v29, 0x1

    invoke-direct/range {v27 .. v29}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v27

    invoke-virtual {v13, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 233
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMealsLayout:Landroid/widget/LinearLayout;

    move-object/from16 v27, v0

    mul-int/lit8 v28, v18, 0x2

    add-int/lit8 v28, v28, 0x1

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v0, v13, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 149
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_0

    .line 173
    .restart local v14       #food:Lorg/medhelp/mydiet/model/Food;
    .restart local v15       #foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    .restart local v16       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v17       #foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    .restart local v20       #j:I
    :cond_5
    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    .end local v16           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    check-cast v16, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 174
    .restart local v16       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const-wide/16 v7, 0x0

    .line 176
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v11

    .line 177
    .local v11, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    move-wide/from16 v27, v0

    move-wide/from16 v0, v27

    invoke-virtual {v11, v0, v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodWithInAppId(J)Lorg/medhelp/mydiet/model/Food;

    move-result-object v14

    .line 179
    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    move-wide/from16 v27, v0

    const-wide/16 v29, 0x0

    cmp-long v27, v27, v29

    if-lez v27, :cond_6

    if-nez v14, :cond_7

    .line 180
    :cond_6
    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    move-wide/from16 v27, v0

    move-wide/from16 v0, v27

    invoke-virtual {v11, v0, v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodWithMedHelpId(J)Lorg/medhelp/mydiet/model/Food;

    move-result-object v14

    .line 183
    :cond_7
    iget-object v0, v14, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    move-object/from16 v27, v0

    iget-object v0, v14, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-static/range {v27 .. v28}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodDetail(Ljava/lang/String;Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v15

    .line 184
    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    move-wide/from16 v27, v0

    move-wide/from16 v0, v27

    invoke-virtual {v14, v0, v1}, Lorg/medhelp/mydiet/model/Food;->getFoodServing(J)Lorg/medhelp/mydiet/model/FoodServing;

    move-result-object v24

    .line 186
    .local v24, selectedServing:Lorg/medhelp/mydiet/model/FoodServing;
    if-eqz v15, :cond_8

    .line 187
    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    move-wide/from16 v27, v0

    iget-wide v0, v15, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    move-wide/from16 v29, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v27

    move-wide/from16 v3, v29

    invoke-static {v0, v1, v2, v3, v4}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v7

    .line 188
    add-double/2addr v9, v7

    .line 172
    :cond_8
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_1
.end method

.method private showAlertDialog()V
    .locals 5

    .prologue
    .line 339
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 340
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v4, 0x7f03002c

    .line 341
    const v3, 0x7f0b0111

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 340
    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 343
    .local v2, layout:Landroid/view/View;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 344
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 345
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 346
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    .line 347
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 348
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 113
    packed-switch p1, :pswitch_data_0

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 115
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 116
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->finishAndShowDetails()V

    goto :goto_0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0xbb9
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 109
    :goto_0
    monitor-exit p0

    return-void

    .line 104
    :pswitch_0
    :try_start_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->onAddMealClick()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b007b
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 61
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v3, 0x7f030014

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->setContentView(I)V

    .line 64
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "date"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 66
    .local v0, timeInMillis:J
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-lez v3, :cond_0

    .line 67
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDate:Ljava/util/Date;

    .line 72
    :goto_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->hasEdits:Z

    .line 74
    const v3, 0x7f0b007a

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mMealsLayout:Landroid/widget/LinearLayout;

    .line 76
    const v3, 0x7f0b007b

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    const v3, 0x7f0b007c

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v3, 0x7f0b0036

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 80
    .local v2, tvDate:Landroid/widget/TextView;
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDate:Ljava/util/Date;

    const-string v4, "EEEE MMM d, yyyy"

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void

    .line 69
    .end local v2           #tvDate:Landroid/widget/TextView;
    :cond_0
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->finish()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 89
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 92
    :cond_0
    iget-boolean v0, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->hasEdits:Z

    if-eqz v0, :cond_1

    .line 94
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$SyncUserDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$SyncUserDataTask;-><init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;Lorg/medhelp/mydiet/activity/foods/MealsActivity$SyncUserDataTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$SyncUserDataTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 96
    :cond_1
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 97
    return-void
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 84
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onStart()V

    .line 85
    new-instance v0, Lorg/medhelp/mydiet/activity/foods/MealsActivity$LoadMealsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$LoadMealsTask;-><init>(Lorg/medhelp/mydiet/activity/foods/MealsActivity;Lorg/medhelp/mydiet/activity/foods/MealsActivity$LoadMealsTask;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/MealsActivity;->mDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/foods/MealsActivity$LoadMealsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 86
    return-void
.end method
