.class Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;
.super Landroid/os/AsyncTask;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateAnonymousUserTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final CREATE_ANONYMOUS_SUCCESS:I = 0x64

.field private static final START:I


# instance fields
.field private createAttempts:I

.field final synthetic this$0:Lorg/medhelp/mydiet/activity/HomeActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/HomeActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 248
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 251
    const/4 v0, 0x0

    iput v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->createAttempts:I

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/HomeActivity;Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;-><init>(Lorg/medhelp/mydiet/activity/HomeActivity;)V

    return-void
.end method

.method private createAnonymousUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 302
    invoke-static {}, Lorg/medhelp/mydiet/http/MHHttpHandler;->createAnonymousUser()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->doInBackground([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)[Ljava/lang/String;
    .locals 13
    .parameter "params"

    .prologue
    .line 261
    const/4 v0, 0x0

    .line 263
    .local v0, createAnonymousStatus:Z
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    iget v10, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->createAttempts:I

    const/4 v11, 0x5

    if-le v10, v11, :cond_2

    .line 298
    :cond_1
    const/4 v10, 0x0

    return-object v10

    .line 264
    :cond_2
    iget v10, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->createAttempts:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->createAttempts:I

    .line 265
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->createAnonymousUser()Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, createResult:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_0

    .line 268
    const/4 v6, 0x0

    .line 271
    .local v6, resultObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 273
    .end local v6           #resultObject:Lorg/json/JSONObject;
    .local v7, resultObject:Lorg/json/JSONObject;
    :try_start_1
    const-string v10, "response_code"

    const/4 v11, -0x1

    invoke-virtual {v7, v10, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    .line 274
    .local v5, responseCode:I
    if-nez v5, :cond_0

    .line 275
    const/4 v0, 0x1

    .line 277
    const-string v10, "data"

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 278
    .local v2, dataObject:Lorg/json/JSONObject;
    if-eqz v2, :cond_0

    .line 280
    const-string v10, "username"

    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 281
    .local v9, username:Ljava/lang/String;
    const-string v10, "password"

    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 282
    .local v4, password:Ljava/lang/String;
    const-string v10, "user_id"

    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 284
    .local v8, userId:Ljava/lang/String;
    iget-object v10, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v10, v9}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserName(Landroid/content/Context;Ljava/lang/String;)V

    .line 285
    iget-object v10, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v10, v4}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setPassword(Landroid/content/Context;Ljava/lang/String;)V

    .line 286
    iget-object v10, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    invoke-static {v10, v8}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserId(Landroid/content/Context;Ljava/lang/String;)V

    .line 287
    iget-object v10, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    const-string v11, "anonymous"

    invoke-static {v10, v11}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserType(Landroid/content/Context;Ljava/lang/String;)V

    .line 289
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Integer;

    const/4 v11, 0x0

    const/16 v12, 0x64

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v10}, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 292
    .end local v2           #dataObject:Lorg/json/JSONObject;
    .end local v4           #password:Ljava/lang/String;
    .end local v5           #responseCode:I
    .end local v8           #userId:Ljava/lang/String;
    .end local v9           #username:Ljava/lang/String;
    :catch_0
    move-exception v3

    move-object v6, v7

    .line 293
    .end local v7           #resultObject:Lorg/json/JSONObject;
    .local v3, e:Lorg/json/JSONException;
    .restart local v6       #resultObject:Lorg/json/JSONObject;
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 292
    .end local v3           #e:Lorg/json/JSONException;
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 319
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 320
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->onPostExecute([Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute([Ljava/lang/String;)V
    .locals 0
    .parameter "result"

    .prologue
    .line 324
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 325
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 255
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 256
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->publishProgress([Ljava/lang/Object;)V

    .line 257
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 307
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 308
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 315
    :goto_0
    :sswitch_0
    return-void

    .line 312
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->this$0:Lorg/medhelp/mydiet/activity/HomeActivity;

    #calls: Lorg/medhelp/mydiet/activity/HomeActivity;->startUserConfigsSync()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/HomeActivity;->access$0(Lorg/medhelp/mydiet/activity/HomeActivity;)V

    goto :goto_0

    .line 308
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/HomeActivity$CreateAnonymousUserTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
