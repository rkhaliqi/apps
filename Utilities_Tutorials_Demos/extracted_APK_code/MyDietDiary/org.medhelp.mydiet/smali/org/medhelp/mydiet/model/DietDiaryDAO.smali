.class public Lorg/medhelp/mydiet/model/DietDiaryDAO;
.super Lorg/medhelp/mydiet/model/BaseDAO;
.source "DietDiaryDAO.java"

# interfaces
.implements Lorg/medhelp/mydiet/C$data;


# static fields
.field private static dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;


# instance fields
.field private dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-object v0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;

    .line 28
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/medhelp/mydiet/model/BaseDAO;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->mContext:Landroid/content/Context;

    .line 38
    iget-object v0, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/medhelp/mydiet/model/DBHelper;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DBHelper;

    move-result-object v0

    iput-object v0, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    .line 39
    return-void
.end method

.method private getFoodFromCursor(Landroid/database/Cursor;)Lorg/medhelp/mydiet/model/Food;
    .locals 3
    .parameter "foodCursor"

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    .local v0, food:Lorg/medhelp/mydiet/model/Food;
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    new-instance v0, Lorg/medhelp/mydiet/model/Food;

    .end local v0           #food:Lorg/medhelp/mydiet/model/Food;
    invoke-direct {v0}, Lorg/medhelp/mydiet/model/Food;-><init>()V

    .line 139
    .restart local v0       #food:Lorg/medhelp/mydiet/model/Food;
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/Food;->inAppId:J

    .line 140
    const-string v1, "medhelp_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/Food;->medHelpId:J

    .line 141
    const-string v1, "name"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lorg/medhelp/mydiet/model/Food;->name:Ljava/lang/String;

    .line 142
    const-string v1, "summary"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    .line 143
    const-string v1, "calories"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/Food;->calories:D

    .line 144
    const-string v1, "food"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    .line 145
    const-string v1, "servings"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    .line 146
    const-string v1, "last_accessed_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/Food;->lastAccessedTime:J

    .line 147
    const-string v1, "access_count"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lorg/medhelp/mydiet/model/Food;->accessCount:J

    .line 150
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 152
    :cond_1
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;
    .locals 1
    .parameter "ctx"

    .prologue
    .line 45
    sget-object v0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lorg/medhelp/mydiet/model/DietDiaryDAO;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;

    .line 49
    :cond_0
    sget-object v0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 882
    iget-object v0, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    if-eqz v0, :cond_0

    .line 883
    iget-object v0, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/model/DBHelper;->close()V

    .line 885
    :cond_0
    return-void
.end method

.method public createOrUpdateExerciseItem(Lorg/medhelp/mydiet/model/ExerciseItem;)J
    .locals 16
    .parameter "exerciseItem"

    .prologue
    .line 716
    if-nez p1, :cond_1

    const-wide/16 v14, -0x1

    .line 755
    :cond_0
    :goto_0
    return-wide v14

    .line 718
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 719
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "exercise_items"

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 721
    .local v9, c:Landroid/database/Cursor;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 722
    .local v11, cv:Landroid/content/ContentValues;
    const-string v2, "date"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 723
    move-object/from16 v0, p1

    iget-wide v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    .line 724
    move-object/from16 v0, p1

    iget-wide v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->date:J

    move-object/from16 v0, p1

    iput-wide v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    .line 726
    :cond_2
    const-string v2, "medhelp_id"

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseMHKey:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    const-string v2, "name"

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    if-eqz v2, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 729
    :cond_3
    const-string v2, "exercise_type"

    const-string v3, ""

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    :goto_1
    const-string v2, "distance"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 734
    const-string v2, "speed"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 735
    const-string v2, "time"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 736
    const-string v2, "calories"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 737
    const-string v2, "time_period"

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 738
    const-string v2, "last_updated_time"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 740
    const-wide/16 v14, -0x1

    .line 741
    .local v14, returnValue:J
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_6

    .line 744
    :cond_4
    const-string v2, "exercise_items"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v12

    .line 745
    .local v12, id:J
    move-wide v14, v12

    .line 751
    .end local v12           #id:J
    :goto_2
    if-eqz v9, :cond_0

    .line 752
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 731
    .end local v14           #returnValue:J
    :cond_5
    const-string v2, "exercise_type"

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 747
    .restart local v14       #returnValue:J
    :cond_6
    const-string v2, "exercise_items"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v11, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 748
    .local v10, count:I
    int-to-long v14, v10

    goto :goto_2
.end method

.method public createOrUpdateMeal(Lorg/medhelp/mydiet/model/Meal;Ljava/util/Date;)V
    .locals 18
    .parameter "meal"
    .parameter "date"

    .prologue
    .line 349
    if-nez p1, :cond_1

    .line 423
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v3}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 352
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "meals"

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "date = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 354
    .local v10, c:Landroid/database/Cursor;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 355
    .local v11, cv:Landroid/content/ContentValues;
    const-string v3, "date"

    invoke-virtual/range {p2 .. p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 356
    const-string v3, "last_updated_time"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 358
    if-eqz v10, :cond_2

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_3

    .line 360
    :cond_2
    move-object/from16 v0, p1

    iget v3, v0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    packed-switch v3, :pswitch_data_0

    .line 384
    :goto_1
    const-string v3, "meals"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 420
    :goto_2
    if-eqz v10, :cond_0

    .line 421
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 362
    :pswitch_0
    const-string v3, "breakfast"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 365
    :pswitch_1
    const-string v3, "lunch"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 368
    :pswitch_2
    const-string v3, "dinner"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 371
    :pswitch_3
    new-instance v16, Lorg/json/JSONArray;

    invoke-direct/range {v16 .. v16}, Lorg/json/JSONArray;-><init>()V

    .line 372
    .local v16, mealsArray:Lorg/json/JSONArray;
    const/4 v13, 0x0

    .line 375
    .local v13, mealObject:Lorg/json/JSONObject;
    :try_start_0
    new-instance v14, Lorg/json/JSONObject;

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v14, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    .end local v13           #mealObject:Lorg/json/JSONObject;
    .local v14, mealObject:Lorg/json/JSONObject;
    :try_start_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v13, v14

    .line 381
    .end local v14           #mealObject:Lorg/json/JSONObject;
    .restart local v13       #mealObject:Lorg/json/JSONObject;
    :goto_3
    const-string v3, "snacks"

    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 377
    :catch_0
    move-exception v12

    .line 378
    .local v12, e:Lorg/json/JSONException;
    :goto_4
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    .line 387
    .end local v12           #e:Lorg/json/JSONException;
    .end local v13           #mealObject:Lorg/json/JSONObject;
    .end local v16           #mealsArray:Lorg/json/JSONArray;
    :cond_3
    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/medhelp/mydiet/model/Meal;->id:J

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v3, v4, v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->deleteMeal(JLjava/util/Date;)V

    .line 389
    move-object/from16 v0, p1

    iget v3, v0, Lorg/medhelp/mydiet/model/Meal;->mealType:I

    packed-switch v3, :pswitch_data_1

    .line 417
    :goto_5
    const-string v3, "meals"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "date = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v11, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2

    .line 391
    :pswitch_4
    const-string v3, "breakfast"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 394
    :pswitch_5
    const-string v3, "lunch"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 397
    :pswitch_6
    const-string v3, "dinner"

    invoke-virtual/range {p1 .. p1}, Lorg/medhelp/mydiet/model/Meal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 400
    :pswitch_7
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 401
    const-string v3, "meals"

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "date = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 403
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 404
    const-string v3, "snacks"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 406
    .local v17, oldSnacksJSON:Ljava/lang/String;
    const/4 v15, 0x0

    .line 408
    .local v15, meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    if-eqz v17, :cond_4

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 409
    invoke-static/range {v17 .. v17}, Lorg/medhelp/mydiet/util/DataUtil;->getMeals(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    .line 411
    :cond_4
    if-nez v15, :cond_5

    new-instance v15, Ljava/util/ArrayList;

    .end local v15           #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 412
    .restart local v15       #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_5
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 414
    const-string v3, "snacks"

    invoke-static {v15}, Lorg/medhelp/mydiet/util/DataUtil;->getMealsAsJSONArrayString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 377
    .end local v15           #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    .end local v17           #oldSnacksJSON:Ljava/lang/String;
    .restart local v14       #mealObject:Lorg/json/JSONObject;
    .restart local v16       #mealsArray:Lorg/json/JSONArray;
    :catch_1
    move-exception v12

    move-object v13, v14

    .end local v14           #mealObject:Lorg/json/JSONObject;
    .restart local v13       #mealObject:Lorg/json/JSONObject;
    goto/16 :goto_4

    .line 360
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 389
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public createOrUpdateWater(DLjava/util/Date;)V
    .locals 10
    .parameter "amount"
    .parameter "date"

    .prologue
    const/4 v2, 0x0

    .line 850
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 851
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "water"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 853
    .local v8, c:Landroid/database/Cursor;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 854
    .local v9, cv:Landroid/content/ContentValues;
    const-string v1, "amount"

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v9, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 855
    const-string v1, "last_updated_time"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 857
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 858
    const-string v1, "water"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v9, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 864
    :goto_0
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 865
    :cond_0
    return-void

    .line 860
    :cond_1
    const-string v1, "date"

    invoke-static {p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 861
    const-string v1, "water"

    invoke-virtual {v0, v1, v2, v9}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0
.end method

.method public createOrUpdateWeight(DLjava/util/Date;)V
    .locals 10
    .parameter "weight"
    .parameter "date"

    .prologue
    const/4 v2, 0x0

    .line 799
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 800
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "weight"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 802
    .local v8, c:Landroid/database/Cursor;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 803
    .local v9, cv:Landroid/content/ContentValues;
    const-string v1, "weight"

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v9, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 804
    const-string v1, "last_updated_time"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 806
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 807
    const-string v1, "weight"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v9, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 813
    :goto_0
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 814
    :cond_0
    return-void

    .line 809
    :cond_1
    const-string v1, "date"

    invoke-static {p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 810
    const-string v1, "weight"

    invoke-virtual {v0, v1, v2, v9}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0
.end method

.method public deleteExerciseItem(J)I
    .locals 4
    .parameter "id"

    .prologue
    .line 792
    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-gtz v1, :cond_0

    const/4 v1, -0x1

    .line 795
    :goto_0
    return v1

    .line 794
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 795
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "exercise_items"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public deleteFood(J)I
    .locals 4
    .parameter "foodInAppId"

    .prologue
    .line 110
    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-gtz v1, :cond_0

    const/4 v1, 0x0

    .line 113
    :goto_0
    return v1

    .line 112
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 113
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "foods"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public deleteMeal(JLjava/util/Date;)V
    .locals 18
    .parameter "mealId"
    .parameter "date"

    .prologue
    .line 426
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v2}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 427
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "meals"

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "date = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p3 .. p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 429
    .local v9, c:Landroid/database/Cursor;
    if-eqz v9, :cond_8

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 430
    const/4 v10, 0x0

    .line 432
    .local v10, cv:Landroid/content/ContentValues;
    const-string v2, "breakfast"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 433
    .local v14, mealString:Ljava/lang/String;
    const/4 v13, 0x0

    .line 434
    .local v13, meal:Lorg/medhelp/mydiet/model/Meal;
    if-eqz v14, :cond_1

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 435
    invoke-static {v14}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v13

    .line 436
    iget-wide v2, v13, Lorg/medhelp/mydiet/model/Meal;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    .line 437
    if-nez v10, :cond_0

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 438
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_0
    const-string v2, "breakfast"

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 442
    :cond_1
    const-string v2, "lunch"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 443
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 444
    invoke-static {v14}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v13

    .line 445
    iget-wide v2, v13, Lorg/medhelp/mydiet/model/Meal;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_3

    .line 446
    if-nez v10, :cond_2

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 447
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_2
    const-string v2, "lunch"

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 451
    :cond_3
    const-string v2, "dinner"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 452
    if-eqz v14, :cond_5

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 453
    invoke-static {v14}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v13

    .line 454
    iget-wide v2, v13, Lorg/medhelp/mydiet/model/Meal;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_5

    .line 455
    if-nez v10, :cond_4

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 456
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_4
    const-string v2, "dinner"

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 460
    :cond_5
    const-string v2, "snacks"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 461
    if-eqz v14, :cond_7

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_7

    .line 462
    invoke-static {v14}, Lorg/medhelp/mydiet/util/DataUtil;->getMeals(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    .line 464
    .local v17, snacksList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    if-eqz v17, :cond_7

    .line 465
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 466
    .local v15, size:I
    move v12, v15

    .line 468
    .local v12, initialSize:I
    const/4 v11, 0x0

    .local v11, i:I
    :goto_0
    if-lt v11, v15, :cond_a

    .line 476
    if-ge v15, v12, :cond_7

    .line 477
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_c

    .line 478
    if-nez v10, :cond_6

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 479
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_6
    const-string v2, "snacks"

    invoke-virtual {v10, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 489
    .end local v11           #i:I
    .end local v12           #initialSize:I
    .end local v15           #size:I
    .end local v17           #snacksList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_7
    :goto_1
    if-eqz v10, :cond_8

    .line 490
    const-string v2, "last_updated_time"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 491
    const-string v2, "meals"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p3 .. p3}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v10, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 495
    .end local v10           #cv:Landroid/content/ContentValues;
    .end local v13           #meal:Lorg/medhelp/mydiet/model/Meal;
    .end local v14           #mealString:Ljava/lang/String;
    :cond_8
    if-eqz v9, :cond_9

    .line 496
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 498
    :cond_9
    return-void

    .line 469
    .restart local v10       #cv:Landroid/content/ContentValues;
    .restart local v11       #i:I
    .restart local v12       #initialSize:I
    .restart local v13       #meal:Lorg/medhelp/mydiet/model/Meal;
    .restart local v14       #mealString:Ljava/lang/String;
    .restart local v15       #size:I
    .restart local v17       #snacksList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_a
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/Meal;

    iget-wide v2, v2, Lorg/medhelp/mydiet/model/Meal;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_b

    .line 470
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 471
    add-int/lit8 v11, v11, -0x1

    .line 472
    add-int/lit8 v15, v15, -0x1

    .line 468
    :cond_b
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 481
    :cond_c
    invoke-static/range {v17 .. v17}, Lorg/medhelp/mydiet/util/DataUtil;->getMealsAsJSONArrayString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v16

    .line 482
    .local v16, snacksJSON:Ljava/lang/String;
    if-nez v10, :cond_d

    new-instance v10, Landroid/content/ContentValues;

    .end local v10           #cv:Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 483
    .restart local v10       #cv:Landroid/content/ContentValues;
    :cond_d
    const-string v2, "snacks"

    move-object/from16 v0, v16

    invoke-virtual {v10, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getAvailableMealTypesForDay(Ljava/util/Date;)Ljava/util/ArrayList;
    .locals 11
    .parameter "date"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 542
    const/4 v10, 0x0

    .line 544
    .local v10, mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "breakfast"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "lunch"

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const-string v3, "dinner"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const-string v3, "snacks"

    aput-object v3, v2, v1

    .line 546
    .local v2, columns:[Ljava/lang/String;
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 547
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 549
    .local v8, c:Landroid/database/Cursor;
    const-string v1, "meals"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "date = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 550
    if-eqz v8, :cond_7

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 552
    const-string v1, "breakfast"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 553
    .local v9, mealType:Ljava/lang/String;
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 554
    if-nez v10, :cond_0

    new-instance v10, Ljava/util/ArrayList;

    .end local v10           #mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 555
    .restart local v10       #mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    const-string v1, "Breakfast"

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 558
    :cond_1
    const-string v1, "lunch"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 559
    if-eqz v9, :cond_3

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 560
    if-nez v10, :cond_2

    new-instance v10, Ljava/util/ArrayList;

    .end local v10           #mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 561
    .restart local v10       #mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    const-string v1, "Lunch"

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    :cond_3
    const-string v1, "dinner"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 565
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 566
    if-nez v10, :cond_4

    new-instance v10, Ljava/util/ArrayList;

    .end local v10           #mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 567
    .restart local v10       #mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    const-string v1, "Dinner"

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    :cond_5
    const-string v1, "snacks"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 571
    if-eqz v9, :cond_7

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_7

    .line 572
    if-nez v10, :cond_6

    new-instance v10, Ljava/util/ArrayList;

    .end local v10           #mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 573
    .restart local v10       #mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    const-string v1, "Snack"

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 577
    .end local v9           #mealType:Ljava/lang/String;
    :cond_7
    if-eqz v8, :cond_8

    .line 578
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 581
    :cond_8
    return-object v10
.end method

.method public getExercise(J)Lorg/medhelp/mydiet/model/Exercise;
    .locals 10
    .parameter "id"

    .prologue
    const/4 v2, 0x0

    .line 683
    const-wide/16 v3, 0x0

    cmp-long v1, p1, v3

    if-gtz v1, :cond_0

    .line 697
    :goto_0
    return-object v2

    .line 685
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 686
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "exercises"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "name"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 688
    .local v8, c:Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 690
    .local v9, exercise:Lorg/medhelp/mydiet/model/Exercise;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 691
    const-string v1, "exercise_json"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/medhelp/mydiet/util/DataUtil;->getExercise(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Exercise;

    move-result-object v9

    .line 692
    iput-wide p1, v9, Lorg/medhelp/mydiet/model/Exercise;->inAppId:J

    .line 695
    :cond_1
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v2, v9

    .line 697
    goto :goto_0
.end method

.method public getExercise(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Exercise;
    .locals 10
    .parameter "medhelpId"

    .prologue
    const/4 v2, 0x0

    .line 701
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move-object v9, v2

    .line 712
    :cond_1
    :goto_0
    return-object v9

    .line 703
    :cond_2
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 704
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "exercises"

    const-string v3, "medhelp_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string v7, "name"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 706
    .local v8, c:Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 707
    .local v9, exercise:Lorg/medhelp/mydiet/model/Exercise;
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 708
    const-string v1, "exercise_json"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/medhelp/mydiet/util/DataUtil;->getExercise(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Exercise;

    move-result-object v9

    .line 710
    :cond_3
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getExerciseItems(Ljava/util/Date;)Ljava/util/ArrayList;
    .locals 11
    .parameter "date"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/ExerciseItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 759
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-gtz v1, :cond_0

    .line 788
    :goto_0
    return-object v2

    .line 761
    :cond_0
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 762
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "exercise_items"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 764
    .local v8, c:Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 765
    .local v10, exerciseItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/ExerciseItem;>;"
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 767
    new-instance v10, Ljava/util/ArrayList;

    .end local v10           #exerciseItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/ExerciseItem;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 768
    .restart local v10       #exerciseItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/ExerciseItem;>;"
    const/4 v9, 0x0

    .line 770
    .local v9, exerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;
    :cond_1
    new-instance v9, Lorg/medhelp/mydiet/model/ExerciseItem;

    .end local v9           #exerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;
    invoke-direct {v9}, Lorg/medhelp/mydiet/model/ExerciseItem;-><init>()V

    .line 772
    .restart local v9       #exerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->inAppId:J

    .line 773
    const-string v1, "medhelp_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseMHKey:Ljava/lang/String;

    .line 774
    const-string v1, "name"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    .line 775
    const-string v1, "exercise_type"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->exerciseType:Ljava/lang/String;

    .line 776
    const-string v1, "distance"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->distance:D

    .line 777
    const-string v1, "speed"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->speed:D

    .line 778
    const-string v1, "time"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->time:J

    .line 779
    const-string v1, "calories"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    .line 780
    const-string v1, "time_period"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v9, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    .line 782
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 783
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    .line 769
    if-nez v1, :cond_1

    .line 786
    .end local v9           #exerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;
    :cond_2
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v2, v10

    .line 788
    goto/16 :goto_0
.end method

.method public getExercises()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Exercise;",
            ">;"
        }
    .end annotation

    .prologue
    .line 661
    invoke-virtual {p0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getExercisesCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 663
    .local v0, c:Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 664
    .local v2, exercises:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Exercise;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 665
    new-instance v2, Ljava/util/ArrayList;

    .end local v2           #exercises:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Exercise;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 666
    .restart local v2       #exercises:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Exercise;>;"
    const/4 v1, 0x0

    .line 669
    .local v1, e:Lorg/medhelp/mydiet/model/Exercise;
    :cond_0
    new-instance v1, Lorg/medhelp/mydiet/model/Exercise;

    .end local v1           #e:Lorg/medhelp/mydiet/model/Exercise;
    invoke-direct {v1}, Lorg/medhelp/mydiet/model/Exercise;-><init>()V

    .line 670
    .restart local v1       #e:Lorg/medhelp/mydiet/model/Exercise;
    const-string v3, "_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, v1, Lorg/medhelp/mydiet/model/Exercise;->id:J

    .line 671
    const-string v3, "name"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lorg/medhelp/mydiet/model/Exercise;->name:Ljava/lang/String;

    .line 672
    const-string v3, "medhelp_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lorg/medhelp/mydiet/model/Exercise;->medHelpId:Ljava/lang/String;

    .line 673
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 674
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    .line 668
    if-nez v3, :cond_0

    .line 677
    .end local v1           #e:Lorg/medhelp/mydiet/model/Exercise;
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 679
    :cond_2
    return-object v2
.end method

.method public getExercisesCursor()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 656
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 657
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "exercises"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "name"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "medhelp_id"

    aput-object v5, v2, v4

    const-string v7, "name"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public getFoodCursorWithInAppId(J)Landroid/database/Cursor;
    .locals 8
    .parameter "id"

    .prologue
    const/4 v2, 0x0

    .line 122
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 123
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "foods"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public getFoodCursorWithMedHelpID(J)Landroid/database/Cursor;
    .locals 8
    .parameter "id"

    .prologue
    const/4 v2, 0x0

    .line 117
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 118
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "foods"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "medhelp_id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public getFoodWithInAppId(J)Lorg/medhelp/mydiet/model/Food;
    .locals 1
    .parameter "id"

    .prologue
    .line 131
    invoke-virtual {p0, p1, p2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodCursorWithInAppId(J)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodFromCursor(Landroid/database/Cursor;)Lorg/medhelp/mydiet/model/Food;

    move-result-object v0

    return-object v0
.end method

.method public getFoodWithMedHelpId(J)Lorg/medhelp/mydiet/model/Food;
    .locals 1
    .parameter "id"

    .prologue
    .line 127
    invoke-virtual {p0, p1, p2}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodCursorWithMedHelpID(J)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodFromCursor(Landroid/database/Cursor;)Lorg/medhelp/mydiet/model/Food;

    move-result-object v0

    return-object v0
.end method

.method public getFoodsWithInAppIds([J)Ljava/util/ArrayList;
    .locals 14
    .parameter "inAppIds"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Food;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 156
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-object v2

    .line 157
    :cond_1
    const-string v12, "("

    .line 158
    .local v12, ids:Ljava/lang/String;
    array-length v13, p1

    .line 159
    .local v13, length:I
    const/4 v11, 0x0

    .local v11, i:I
    :goto_1
    if-lt v11, v13, :cond_4

    .line 163
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 164
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 165
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "foods"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 167
    .local v8, c:Landroid/database/Cursor;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 169
    .local v10, foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    if-eqz v8, :cond_6

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 170
    const/4 v9, 0x0

    .line 172
    .local v9, food:Lorg/medhelp/mydiet/model/Food;
    :cond_2
    new-instance v9, Lorg/medhelp/mydiet/model/Food;

    .end local v9           #food:Lorg/medhelp/mydiet/model/Food;
    invoke-direct {v9}, Lorg/medhelp/mydiet/model/Food;-><init>()V

    .line 174
    .restart local v9       #food:Lorg/medhelp/mydiet/model/Food;
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v9, Lorg/medhelp/mydiet/model/Food;->inAppId:J

    .line 175
    const-string v1, "medhelp_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v9, Lorg/medhelp/mydiet/model/Food;->medHelpId:J

    .line 176
    const-string v1, "name"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lorg/medhelp/mydiet/model/Food;->name:Ljava/lang/String;

    .line 177
    const-string v1, "summary"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    .line 178
    const-string v1, "calories"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v9, Lorg/medhelp/mydiet/model/Food;->calories:D

    .line 179
    const-string v1, "food"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    .line 180
    const-string v1, "servings"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    .line 182
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    .line 171
    if-nez v1, :cond_2

    .line 189
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v2, v10

    .line 190
    goto/16 :goto_0

    .line 160
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v9           #food:Lorg/medhelp/mydiet/model/Food;
    .end local v10           #foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-wide v3, p1, v11

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 161
    add-int/lit8 v1, v13, -0x1

    if-ge v11, v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 159
    :cond_5
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 185
    .restart local v0       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v8       #c:Landroid/database/Cursor;
    .restart local v10       #foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    :cond_6
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public getFrequentFoodItems(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .parameter "like"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 316
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFrequentFoods(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 318
    .local v9, foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move-object v8, v5

    .line 344
    :cond_1
    return-object v8

    .line 320
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 321
    .local v8, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    const/4 v0, 0x0

    .line 322
    .local v0, foodJSONWithServings:Ljava/lang/String;
    const/4 v7, 0x0

    .line 323
    .local v7, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const-wide/16 v10, 0x1

    .line 324
    .local v10, i:J
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_3
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/medhelp/mydiet/model/Food;

    .line 325
    .local v6, food:Lorg/medhelp/mydiet/model/Food;
    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    iget-object v2, v6, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    invoke-static {v1, v2}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodJSONWithServings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->servingId:J

    iget-wide v3, v6, Lorg/medhelp/mydiet/model/Food;->amount:D

    invoke-static/range {v0 .. v5}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemFromJSON(Ljava/lang/String;JDLjava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v7

    .line 327
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->amount:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-lez v1, :cond_4

    .line 328
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->amount:D

    iput-wide v1, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 330
    :cond_4
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->servingId:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_5

    .line 331
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->servingId:J

    iput-wide v1, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 333
    :cond_5
    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    const-string v2, "null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 334
    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    iput-object v1, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 337
    :cond_6
    if-eqz v7, :cond_3

    .line 338
    iput-wide v10, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 339
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 340
    const-wide/16 v1, 0x1

    add-long/2addr v10, v1

    goto :goto_0
.end method

.method public getFrequentFoods(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .parameter "like"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Food;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 238
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 239
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .local v3, selection:Ljava/lang/String;
    move-object v4, v2

    .line 240
    check-cast v4, [Ljava/lang/String;

    .line 241
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v8, 0x1

    .line 243
    .local v8, MIN_ACCESS_COUNT:I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    .line 244
    :cond_0
    const-string p1, ""

    .line 245
    const-string v3, "access_count >= \'1\'"

    .line 251
    :goto_0
    const-string v1, "foods"

    const-string v7, "access_count DESC"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 253
    .local v9, c:Landroid/database/Cursor;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .local v11, foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 256
    const/4 v10, 0x0

    .line 258
    .local v10, food:Lorg/medhelp/mydiet/model/Food;
    :cond_1
    new-instance v10, Lorg/medhelp/mydiet/model/Food;

    .end local v10           #food:Lorg/medhelp/mydiet/model/Food;
    invoke-direct {v10}, Lorg/medhelp/mydiet/model/Food;-><init>()V

    .line 260
    .restart local v10       #food:Lorg/medhelp/mydiet/model/Food;
    const-string v1, "_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->inAppId:J

    .line 261
    const-string v1, "medhelp_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->medHelpId:J

    .line 262
    const-string v1, "name"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lorg/medhelp/mydiet/model/Food;->name:Ljava/lang/String;

    .line 263
    const-string v1, "summary"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    .line 264
    const-string v1, "calories"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->calories:D

    .line 265
    const-string v1, "serving_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->servingId:J

    .line 266
    const-string v1, "amount"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->amount:D

    .line 267
    const-string v1, "food"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    .line 268
    const-string v1, "servings"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    .line 270
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    .line 257
    if-nez v1, :cond_1

    .line 277
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 279
    .end local v10           #food:Lorg/medhelp/mydiet/model/Food;
    .end local v11           #foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    :cond_2
    :goto_1
    return-object v11

    .line 247
    .end local v9           #c:Landroid/database/Cursor;
    :cond_3
    const-string v3, "name LIKE ? AND access_count >= \'1\'"

    .line 248
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    .end local v4           #selectionArgs:[Ljava/lang/String;
    const/4 v1, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "%"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .restart local v4       #selectionArgs:[Ljava/lang/String;
    goto/16 :goto_0

    .line 273
    .restart local v9       #c:Landroid/database/Cursor;
    .restart local v11       #foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    :cond_4
    if-eqz v9, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v11, v2

    .line 274
    goto :goto_1
.end method

.method public getMealsCursor(Ljava/util/Date;)Landroid/database/Cursor;
    .locals 8
    .parameter "date"

    .prologue
    const/4 v2, 0x0

    .line 501
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 502
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "meals"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public getMealsList(Ljava/util/Date;)Ljava/util/ArrayList;
    .locals 4
    .parameter "date"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Meal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 506
    const/4 v1, 0x0

    .line 507
    .local v1, meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getMealsCursor(Ljava/util/Date;)Landroid/database/Cursor;

    move-result-object v0

    .line 509
    .local v0, c:Landroid/database/Cursor;
    if-eqz v0, :cond_8

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 510
    const-string v3, "breakfast"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 511
    .local v2, mealsJSON:Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 512
    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    .end local v1           #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 513
    .restart local v1       #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_0
    invoke-static {v2}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 516
    :cond_1
    const-string v3, "lunch"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 517
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 518
    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    .end local v1           #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 519
    .restart local v1       #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_2
    invoke-static {v2}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 522
    :cond_3
    const-string v3, "dinner"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 523
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_5

    .line 524
    if-nez v1, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    .end local v1           #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 525
    .restart local v1       #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_4
    invoke-static {v2}, Lorg/medhelp/mydiet/util/DataUtil;->getMeal(Ljava/lang/String;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 529
    :cond_5
    const-string v3, "snacks"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 530
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_7

    .line 531
    if-nez v1, :cond_6

    new-instance v1, Ljava/util/ArrayList;

    .end local v1           #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 532
    .restart local v1       #meals:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Meal;>;"
    :cond_6
    invoke-static {v2}, Lorg/medhelp/mydiet/util/DataUtil;->getMeals(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 534
    :cond_7
    if-eqz v1, :cond_8

    .line 535
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 538
    .end local v2           #mealsJSON:Ljava/lang/String;
    :cond_8
    return-object v1
.end method

.method public getRecentFoodItems(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .parameter "like"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/FoodItemInMeal;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 283
    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getRecentFoods(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 285
    .local v9, foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move-object v8, v5

    .line 312
    :cond_1
    return-object v8

    .line 287
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 288
    .local v8, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    const/4 v0, 0x0

    .line 289
    .local v0, foodJSONWithServings:Ljava/lang/String;
    const/4 v7, 0x0

    .line 291
    .local v7, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const-wide/16 v10, 0x1

    .line 292
    .local v10, i:J
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_3
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/medhelp/mydiet/model/Food;

    .line 293
    .local v6, food:Lorg/medhelp/mydiet/model/Food;
    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    iget-object v2, v6, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    invoke-static {v1, v2}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodJSONWithServings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 294
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->servingId:J

    iget-wide v3, v6, Lorg/medhelp/mydiet/model/Food;->amount:D

    invoke-static/range {v0 .. v5}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodItemFromJSON(Ljava/lang/String;JDLjava/lang/String;)Lorg/medhelp/mydiet/model/FoodItemInMeal;

    move-result-object v7

    .line 295
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->amount:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-lez v1, :cond_4

    .line 296
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->amount:D

    iput-wide v1, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    .line 298
    :cond_4
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->servingId:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_5

    .line 299
    iget-wide v1, v6, Lorg/medhelp/mydiet/model/Food;->servingId:J

    iput-wide v1, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    .line 301
    :cond_5
    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    const-string v2, "null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 302
    iget-object v1, v6, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    iput-object v1, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->summary:Ljava/lang/String;

    .line 305
    :cond_6
    if-eqz v7, :cond_3

    .line 306
    iput-wide v10, v7, Lorg/medhelp/mydiet/model/FoodItemInMeal;->id:J

    .line 307
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    const-wide/16 v1, 0x1

    add-long/2addr v10, v1

    goto :goto_0
.end method

.method public getRecentFoods(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .parameter "like"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Food;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 194
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 195
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .local v3, selection:Ljava/lang/String;
    move-object v4, v2

    .line 196
    check-cast v4, [Ljava/lang/String;

    .line 197
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v8, 0x1

    .line 199
    .local v8, MIN_ACCESS_COUNT:I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    .line 200
    :cond_0
    const-string p1, ""

    .line 201
    const-string v3, "last_accessed_time>=\'0\' AND access_count >= \'1\'"

    .line 207
    :goto_0
    const-string v1, "foods"

    const-string v7, "last_accessed_time DESC"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 209
    .local v9, c:Landroid/database/Cursor;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v11, foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 212
    const/4 v10, 0x0

    .line 214
    .local v10, food:Lorg/medhelp/mydiet/model/Food;
    :cond_1
    new-instance v10, Lorg/medhelp/mydiet/model/Food;

    .end local v10           #food:Lorg/medhelp/mydiet/model/Food;
    invoke-direct {v10}, Lorg/medhelp/mydiet/model/Food;-><init>()V

    .line 216
    .restart local v10       #food:Lorg/medhelp/mydiet/model/Food;
    const-string v1, "_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->inAppId:J

    .line 217
    const-string v1, "medhelp_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->medHelpId:J

    .line 218
    const-string v1, "name"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lorg/medhelp/mydiet/model/Food;->name:Ljava/lang/String;

    .line 219
    const-string v1, "summary"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    .line 220
    const-string v1, "calories"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->calories:D

    .line 221
    const-string v1, "serving_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->servingId:J

    .line 222
    const-string v1, "amount"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    iput-wide v1, v10, Lorg/medhelp/mydiet/model/Food;->amount:D

    .line 223
    const-string v1, "food"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    .line 224
    const-string v1, "servings"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    .line 226
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    .line 213
    if-nez v1, :cond_1

    .line 233
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 234
    .end local v10           #food:Lorg/medhelp/mydiet/model/Food;
    .end local v11           #foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    :cond_2
    :goto_1
    return-object v11

    .line 203
    .end local v9           #c:Landroid/database/Cursor;
    :cond_3
    const-string v3, "name LIKE ? AND last_accessed_time>=\'0\' AND access_count >= \'1\'"

    .line 204
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    .end local v4           #selectionArgs:[Ljava/lang/String;
    const/4 v1, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "%"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .restart local v4       #selectionArgs:[Ljava/lang/String;
    goto/16 :goto_0

    .line 229
    .restart local v9       #c:Landroid/database/Cursor;
    .restart local v11       #foods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Food;>;"
    :cond_4
    if-eqz v9, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v11, v2

    .line 230
    goto :goto_1
.end method

.method public getRecentWeightOnOrBefore(Ljava/util/Date;)D
    .locals 7
    .parameter "date"

    .prologue
    .line 817
    if-nez p1, :cond_0

    new-instance p1, Ljava/util/Date;

    .end local p1
    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    .line 819
    .restart local p1
    :cond_0
    iget-object v4, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v4}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 820
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SELECT * FROM weight WHERE date = ( SELECT max(date) FROM weight WHERE date <= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 821
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ");"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 820
    invoke-virtual {v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 823
    .local v0, c:Landroid/database/Cursor;
    const-wide/high16 v2, -0x4010

    .line 825
    .local v2, weight:D
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 826
    const-string v4, "weight"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    .line 829
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 831
    :cond_2
    return-wide v2
.end method

.method public getWater(Ljava/util/Date;)D
    .locals 11
    .parameter "date"

    .prologue
    const/4 v2, 0x0

    .line 868
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 869
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "water"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 871
    .local v10, c:Landroid/database/Cursor;
    const-wide/high16 v8, -0x4010

    .line 873
    .local v8, amount:D
    if-eqz v10, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 874
    const-string v1, "amount"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v8

    .line 876
    :cond_0
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 878
    :cond_1
    return-wide v8
.end method

.method public getWeight(Ljava/util/Date;)D
    .locals 11
    .parameter "date"

    .prologue
    const/4 v2, 0x0

    .line 835
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 836
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "weight"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "date = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lorg/medhelp/mydiet/util/DateUtil;->getDateAtMidnightInMilliseconds(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 838
    .local v8, c:Landroid/database/Cursor;
    const-wide/high16 v9, -0x4010

    .line 840
    .local v9, weight:D
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 841
    const-string v1, "weight"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v9

    .line 844
    :cond_0
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_1
    return-wide v9
.end method

.method public initTables()J
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 585
    iget-object v1, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v1}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 587
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "exercises"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 589
    .local v8, c:Landroid/database/Cursor;
    const-wide/16 v9, 0x0

    .line 590
    .local v9, count:J
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 591
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    int-to-long v9, v1

    .line 593
    :cond_0
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 595
    :cond_1
    return-wide v9
.end method

.method public insertFood(Lorg/medhelp/mydiet/model/Food;)J
    .locals 20
    .parameter "food"

    .prologue
    .line 58
    if-nez p1, :cond_0

    const-wide/16 v14, -0x1

    .line 106
    :goto_0
    return-wide v14

    .line 60
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v3}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 61
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 63
    .local v13, cv:Landroid/content/ContentValues;
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    .line 65
    .local v16, now:J
    const-string v3, "medhelp_id"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->medHelpId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 66
    const-string v3, "name"

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/medhelp/mydiet/model/Food;->name:Ljava/lang/String;

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v3, "summary"

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/medhelp/mydiet/model/Food;->summary:Ljava/lang/String;

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "food_group_id"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->groupId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 69
    const-string v3, "food"

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "servings"

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "calories"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->calories:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 72
    const-string v3, "last_updated_time"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 73
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/medhelp/mydiet/model/Food;->isNewAccess:Z

    if-eqz v3, :cond_1

    .line 74
    const-string v3, "last_accessed_time"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 77
    :cond_1
    const-string v3, "foods"

    const/4 v4, 0x0

    const-string v5, "(medhelp_id > 0 AND medhelp_id = ?)  OR (_id > 0 AND _id = ?)"

    .line 79
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Food;->medHelpId:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Food;->inAppId:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 77
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 81
    .local v12, c:Landroid/database/Cursor;
    const-wide/16 v14, -0x1

    .line 82
    .local v14, insertedFoodId:J
    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_5

    .line 83
    :cond_2
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/medhelp/mydiet/model/Food;->isNewAccess:Z

    if-eqz v3, :cond_3

    .line 85
    const-string v3, "access_count"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 86
    const-string v3, "serving_id"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->servingId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 87
    const-string v3, "amount"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->amount:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 90
    :cond_3
    const-string v3, "foods"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v13}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v14

    .line 105
    :cond_4
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 92
    :cond_5
    if-eqz v12, :cond_4

    .line 93
    const-string v3, "access_count"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 94
    .local v10, accessCount:J
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/medhelp/mydiet/model/Food;->isNewAccess:Z

    if-eqz v3, :cond_6

    .line 95
    const-wide/16 v3, 0x1

    add-long/2addr v10, v3

    .line 96
    const-string v3, "access_count"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 97
    const-string v3, "serving_id"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->servingId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 98
    const-string v3, "amount"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/medhelp/mydiet/model/Food;->amount:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 101
    :cond_6
    const-string v3, "_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 102
    const-string v3, "foods"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v13, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method public populateExercises(Landroid/content/Context;)V
    .locals 9
    .parameter "context"

    .prologue
    const/4 v8, 0x0

    .line 599
    iget-object v4, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v4}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 601
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v4, "exercises"

    const-string v5, "_id > 0"

    invoke-virtual {v1, v4, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 603
    invoke-static {p1}, Lorg/medhelp/mydiet/model/DBInitializer;->readExercisesFromAssets(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    .line 604
    .local v3, exercises:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/Exercise;>;"
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 619
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    const/4 v0, 0x0

    .line 607
    .local v0, cv:Landroid/content/ContentValues;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 618
    iget-object v4, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lorg/medhelp/mydiet/util/DBSetupUtil;->setExercisesLoadFlag(Landroid/content/Context;Z)V

    goto :goto_0

    .line 607
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/medhelp/mydiet/model/Exercise;

    .line 608
    .local v2, exercise:Lorg/medhelp/mydiet/model/Exercise;
    new-instance v0, Landroid/content/ContentValues;

    .end local v0           #cv:Landroid/content/ContentValues;
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 610
    .restart local v0       #cv:Landroid/content/ContentValues;
    const-string v5, "medhelp_id"

    iget-object v6, v2, Lorg/medhelp/mydiet/model/Exercise;->medHelpId:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    const-string v5, "name"

    iget-object v6, v2, Lorg/medhelp/mydiet/model/Exercise;->name:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    const-string v5, "exercise_json"

    iget-object v6, v2, Lorg/medhelp/mydiet/model/Exercise;->exerciseJSON:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    const-string v5, "last_updated_time"

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 615
    const-string v5, "exercises"

    invoke-virtual {v1, v5, v8, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_1
.end method

.method public updateExerciseKeysToMedhelpKeys()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    .line 622
    iget-object v6, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->dbHelper:Lorg/medhelp/mydiet/model/DBHelper;

    invoke-virtual {v6}, Lorg/medhelp/mydiet/model/DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 624
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v4, "SELECT ei._id AS _id, ei.exercise_id AS exercise_id, e.medhelp_id AS medhelp_id FROM exercise_items AS ei INNER JOIN exercises AS e on ei.exercise_id = e._id"

    .line 628
    .local v4, sql:Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 630
    .local v0, c:Landroid/database/Cursor;
    const/4 v5, 0x1

    .line 632
    .local v5, updateStatus:Z
    if-eqz v0, :cond_3

    .line 633
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 635
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 638
    .local v2, cv:Landroid/content/ContentValues;
    :cond_0
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 639
    const-string v6, "medhelp_id"

    const-string v7, "medhelp_id"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    const-string v6, "exercise_items"

    const-string v7, "_id = ?"

    new-array v8, v13, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "_id"

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v3, v6, v2, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 642
    .local v1, count:I
    if-eq v1, v13, :cond_1

    .line 643
    const/4 v5, 0x0

    .line 645
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    .line 637
    if-nez v6, :cond_0

    .line 647
    .end local v1           #count:I
    .end local v2           #cv:Landroid/content/ContentValues;
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 650
    :cond_3
    if-eqz v5, :cond_4

    .line 651
    iget-object v6, p0, Lorg/medhelp/mydiet/model/DietDiaryDAO;->mContext:Landroid/content/Context;

    invoke-static {v6, v13}, Lorg/medhelp/mydiet/util/DBSetupUtil;->setExercisesMHKeyStatus(Landroid/content/Context;Z)V

    .line 653
    :cond_4
    return-void
.end method
