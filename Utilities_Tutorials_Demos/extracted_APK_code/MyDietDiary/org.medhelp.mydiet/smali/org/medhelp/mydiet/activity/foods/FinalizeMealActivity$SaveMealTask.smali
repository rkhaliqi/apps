.class Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;
.super Landroid/os/AsyncTask;
.source "FinalizeMealActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveMealTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x64

.field private static final START:I


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 525
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 525
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;-><init>(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 6
    .parameter "params"

    .prologue
    .line 538
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 540
    .local v2, foodsToSave:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v3

    iget-object v3, v3, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 546
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    invoke-static {v3}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v0

    .line 548
    .local v0, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 549
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v3

    iget-wide v3, v3, Lorg/medhelp/mydiet/model/Meal;->id:J

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;
    invoke-static {v5}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v0, v3, v4, v5}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->deleteMeal(JLjava/util/Date;)V

    .line 554
    :goto_1
    const/4 v3, 0x0

    return-object v3

    .line 540
    .end local v0           #dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 541
    .local v1, food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    iget-boolean v4, v1, Lorg/medhelp/mydiet/model/FoodItemInMeal;->isSelected:Z

    if-eqz v4, :cond_0

    .line 542
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 551
    .end local v1           #food:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    .restart local v0       #dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    :cond_2
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mMeal:Lorg/medhelp/mydiet/model/Meal;
    invoke-static {v3}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Lorg/medhelp/mydiet/model/Meal;

    move-result-object v3

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->mDate:Ljava/util/Date;
    invoke-static {v4}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->createOrUpdateMeal(Lorg/medhelp/mydiet/model/Meal;Ljava/util/Date;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 573
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->publishProgress([Ljava/lang/Object;)V

    .line 574
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->startUserDataSync()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$6(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    .line 575
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 576
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 532
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 533
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->publishProgress([Ljava/lang/Object;)V

    .line 534
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 559
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 560
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 569
    :goto_0
    return-void

    .line 562
    :sswitch_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->showSavingDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$3(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    goto :goto_0

    .line 565
    :sswitch_1
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->hideSavingDialog()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$4(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    .line 566
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->this$0:Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->finishAndShowDetails()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;->access$5(Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity;)V

    goto :goto_0

    .line 560
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/foods/FinalizeMealActivity$SaveMealTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
