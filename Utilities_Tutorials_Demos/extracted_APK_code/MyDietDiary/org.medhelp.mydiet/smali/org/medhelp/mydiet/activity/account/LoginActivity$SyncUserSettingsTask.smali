.class Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;
.super Landroid/os/AsyncTask;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/account/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncUserSettingsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;


# direct methods
.method private constructor <init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 243
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;-><init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs declared-synchronized doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "params"

    .prologue
    .line 256
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-static {v1}, Lorg/medhelp/mydiet/http/MHHttpHandler;->requestUserSettingsSync(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 257
    .local v0, response:Ljava/lang/String;
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-static {v1, v0}, Lorg/medhelp/mydiet/util/SyncUtil;->loadUserConfigs(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    monitor-exit p0

    return-object v0

    .line 256
    .end local v0           #response:Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->dismiss()V

    .line 285
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 286
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 272
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->dismiss()V

    .line 275
    :cond_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    #calls: Lorg/medhelp/mydiet/activity/account/LoginActivity;->completeLogin()V
    invoke-static {v0}, Lorg/medhelp/mydiet/activity/account/LoginActivity;->access$1(Lorg/medhelp/mydiet/activity/account/LoginActivity;)V

    .line 277
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 278
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 246
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    new-instance v1, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    invoke-direct {v1, v2, v3}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;-><init>(Lorg/medhelp/mydiet/activity/account/LoginActivity;Landroid/content/Context;)V

    iput-object v1, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    .line 249
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    const-string v1, "Initializing User Settings"

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    const-string v1, "Initializing user data from MedHelp"

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->this$0:Lorg/medhelp/mydiet/activity/account/LoginActivity;

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/account/LoginActivity;->mDialog:Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;

    invoke-virtual {v0}, Lorg/medhelp/mydiet/activity/account/LoginActivity$LoginDialog;->show()V

    .line 252
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .parameter "values"

    .prologue
    .line 263
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 264
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 268
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lorg/medhelp/mydiet/activity/account/LoginActivity$SyncUserSettingsTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
