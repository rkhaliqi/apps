.class public Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingNameSelectedListener;
.super Ljava/lang/Object;
.source "CreateFoodActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ServingNameSelectedListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;


# direct methods
.method public constructor <init>(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 166
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingNameSelectedListener;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter "view"
    .parameter "pos"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 170
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingNameSelectedListener;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #getter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mItemSelectedByUser:Z
    invoke-static {v2}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$0(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 171
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, item:Ljava/lang/String;
    const-string v2, "other"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lorg/medhelp/mydiet/C$arrays;->servingNames:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne p3, v2, :cond_2

    .line 173
    :cond_0
    const-string v1, ""

    .line 174
    .local v1, servingName:Ljava/lang/String;
    const-string v2, "other"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 175
    const-string v2, " (other)"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 177
    :cond_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingNameSelectedListener;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    #calls: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->showServingNameEntryDialog(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$1(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Ljava/lang/String;)V

    .line 180
    .end local v0           #item:Ljava/lang/String;
    .end local v1           #servingName:Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity$ServingNameSelectedListener;->this$0:Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;

    const/4 v3, 0x1

    #setter for: Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->mItemSelectedByUser:Z
    invoke-static {v2, v3}, Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;->access$2(Lorg/medhelp/mydiet/activity/foods/CreateFoodActivity;Z)V

    .line 181
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method
