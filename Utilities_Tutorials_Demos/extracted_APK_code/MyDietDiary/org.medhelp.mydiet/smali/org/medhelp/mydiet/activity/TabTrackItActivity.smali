.class public Lorg/medhelp/mydiet/activity/TabTrackItActivity;
.super Lorg/medhelp/mydiet/activity/BaseActivity;
.source "TabTrackItActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;,
        Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;,
        Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;
    }
.end annotation


# static fields
.field private static final DATE_PICKER:I = 0x0

.field private static final IS_DETAIL_VISIBLE:Ljava/lang/String; = "is_detail_visible"


# instance fields
.field private dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private hasDetails:Z

.field private isDetailVisible:Z

.field mBtnDate:Landroid/widget/Button;

.field private mCalendar:Ljava/util/Calendar;

.field mCalorieBudget:D

.field mCaloriesFromCarbs:D

.field mCaloriesFromFat:D

.field mCaloriesFromProtein:D

.field private mDialog:Landroid/app/AlertDialog;

.field private mExerciseItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/ExerciseItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadExerciseItemsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;

.field private mLoadMealsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;

.field private mLoadWeightTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;

.field private mMeals:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/medhelp/mydiet/model/Meal;",
            ">;"
        }
    .end annotation
.end field

.field private mProgress:Landroid/widget/ProgressBar;

.field mRemainingCalories:D

.field mSummaryExerciseCalories:D

.field mSummaryFoodCalories:D

.field mSummaryWeight:D

.field mWeight:D


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->hasDetails:Z

    .line 243
    new-instance v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$1;

    invoke-direct {v0, p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$1;-><init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 57
    return-void
.end method

.method static synthetic access$0(Lorg/medhelp/mydiet/activity/TabTrackItActivity;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 250
    invoke-direct {p0, p1, p2, p3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->setDate(III)V

    return-void
.end method

.method static synthetic access$1(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Ljava/util/Calendar;
    .locals 1
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$10(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;
    .locals 1
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadWeightTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;

    return-object v0
.end method

.method static synthetic access$11(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 88
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadWeightTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadWeightTask;

    return-void
.end method

.method static synthetic access$12(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 599
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshWeightContent()V

    return-void
.end method

.method static synthetic access$2(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mMeals:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$3(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 878
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->showProgressBar()V

    return-void
.end method

.method static synthetic access$4(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 332
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshMealsContent()V

    return-void
.end method

.method static synthetic access$5(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 882
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->hideProgressBar()V

    return-void
.end method

.method static synthetic access$6(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;
    .locals 1
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadExerciseItemsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;

    return-object v0
.end method

.method static synthetic access$7(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadExerciseItemsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadExerciseItemsTask;

    return-void
.end method

.method static synthetic access$8(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mExerciseItems:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$9(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 525
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshExercisesContent()V

    return-void
.end method

.method private hideProgressBar()V
    .locals 2

    .prologue
    .line 883
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 884
    return-void
.end method

.method private onExercisesClick()V
    .locals 4

    .prologue
    .line 294
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/medhelp/mydiet/activity/exercises/ExercisesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 295
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "date"

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 296
    const/16 v1, 0xbb9

    invoke-virtual {p0, v0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 297
    return-void
.end method

.method private onFoodClick()V
    .locals 7

    .prologue
    .line 258
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mMeals:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mMeals:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 259
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lorg/medhelp/mydiet/activity/foods/MealsActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 260
    .local v2, mealIntent:Landroid/content/Intent;
    const-string v4, "date"

    iget-object v5, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 261
    const/16 v4, 0xbb9

    invoke-virtual {p0, v2, v4}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 291
    .end local v2           #mealIntent:Landroid/content/Intent;
    :goto_0
    return-void

    .line 265
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 266
    .local v3, mealTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "Breakfast"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    const-string v4, "Lunch"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    const-string v4, "Dinner"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    const-string v4, "Snack"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    new-instance v1, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;

    invoke-direct {v1, p0, v3}, Lorg/medhelp/mydiet/adapter/MealTypesAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 273
    .local v1, mMealTypesAdapter:Lorg/medhelp/mydiet/adapter/MealTypesAdapter;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 274
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    new-instance v4, Lorg/medhelp/mydiet/activity/TabTrackItActivity$2;

    invoke-direct {v4, p0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$2;-><init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/adapter/MealTypesAdapter;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 286
    const-string v4, "Select a meal type"

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 287
    const-string v4, "cancel"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 289
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mDialog:Landroid/app/AlertDialog;

    .line 290
    iget-object v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private refreshAvatar()V
    .locals 4

    .prologue
    .line 300
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getAvatarPosition(Landroid/content/Context;)I

    move-result v0

    .line 301
    .local v0, avatarImagePosition:I
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getGender(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 303
    .local v1, gender:Ljava/lang/String;
    const v3, 0x7f0b00e7

    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 305
    .local v2, ivAvatar:Landroid/widget/ImageView;
    const-string v3, "Male"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 306
    sget-object v3, Lorg/medhelp/mydiet/C$arrays;->maleAvatars:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 310
    :goto_0
    return-void

    .line 308
    :cond_0
    sget-object v3, Lorg/medhelp/mydiet/C$arrays;->femaleAvatars:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private refreshCalorieProgress()V
    .locals 31

    .prologue
    .line 633
    const/16 v26, 0x0

    .line 635
    .local v26, tv:Landroid/widget/TextView;
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDesiredWeightInLb(Landroid/content/Context;)F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v17, v0

    .line 636
    .local v17, desiredWeight:D
    const-wide/16 v27, 0x0

    cmpg-double v3, v17, v27

    if-gtz v3, :cond_0

    .line 637
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mWeight:D

    move-wide/from16 v17, v0

    .line 640
    :cond_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mWeight:D

    move-wide/from16 v27, v0

    sub-double v6, v17, v27

    .line 641
    .local v6, changeInWeight:D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mWeight:D

    move-wide/from16 v27, v0

    const-wide v29, 0x3fdd07a84ab75e51L

    mul-double v4, v27, v29

    .line 642
    .local v4, weightInKg:D
    const-wide/16 v27, 0x0

    cmpg-double v3, v4, v27

    if-gtz v3, :cond_1

    .line 643
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInKG(Landroid/content/Context;)F

    move-result v3

    float-to-double v4, v3

    .line 646
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v8}, Lorg/medhelp/mydiet/util/DataUtil;->getCalorieBudget(Landroid/content/Context;DDLjava/util/Calendar;)D

    move-result-wide v9

    .line 647
    .local v9, calorieBudget:D
    move-object/from16 v0, p0

    iput-wide v9, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalorieBudget:D

    .line 648
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDate(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-static {v3, v8}, Lorg/medhelp/mydiet/util/DateUtil;->getDifferenceInDays(Ljava/util/Calendar;Ljava/util/Calendar;)J

    move-result-wide v27

    move-wide/from16 v0, v27

    long-to-double v15, v0

    .line 650
    .local v15, daysToTarget:D
    new-instance v23, Ljava/text/DecimalFormatSymbols;

    new-instance v3, Ljava/util/Locale;

    const-string v8, "en"

    const-string v27, "en"

    move-object/from16 v0, v27

    invoke-direct {v3, v8, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-direct {v0, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 651
    .local v23, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v3, 0x2e

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 652
    new-instance v19, Ljava/text/DecimalFormat;

    const-string v3, "#"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-direct {v0, v3, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 654
    .local v19, df0:Ljava/text/DecimalFormat;
    const-wide/16 v27, 0x0

    cmpg-double v3, v15, v27

    if-gtz v3, :cond_2

    const-wide/high16 v15, 0x3ff0

    .line 656
    :cond_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    move-wide/from16 v27, v0

    const-wide/16 v29, 0x0

    cmpg-double v3, v27, v29

    if-gez v3, :cond_3

    .line 657
    const-wide/16 v27, 0x0

    move-wide/from16 v0, v27

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    .line 658
    :cond_3
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    move-wide/from16 v27, v0

    const-wide/16 v29, 0x0

    cmpg-double v3, v27, v29

    if-gez v3, :cond_4

    .line 659
    const-wide/16 v27, 0x0

    move-wide/from16 v0, v27

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    .line 661
    :cond_4
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    move-wide/from16 v27, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    move-wide/from16 v29, v0

    sub-double v13, v27, v29

    .line 662
    .local v13, currentDayCalorieIntake:D
    sub-double v11, v9, v13

    .line 664
    .local v11, caloriesRemaining:D
    const v3, 0x7f0b0120

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    .line 666
    .local v20, iv:Landroid/widget/ImageView;
    invoke-virtual/range {v20 .. v20}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    int-to-long v0, v3

    move-wide/from16 v24, v0

    .line 668
    .local v24, totalWidth:J
    move-wide/from16 v0, v24

    long-to-double v0, v0

    move-wide/from16 v27, v0

    mul-double v27, v27, v13

    div-double v21, v27, v9

    .line 670
    .local v21, position:D
    const v3, 0x7f0b0122

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .end local v20           #iv:Landroid/widget/ImageView;
    check-cast v20, Landroid/widget/ImageView;

    .line 671
    .restart local v20       #iv:Landroid/widget/ImageView;
    const-wide/16 v27, 0x0

    cmpg-double v3, v21, v27

    if-gez v3, :cond_5

    const-wide/16 v21, 0x0

    .line 672
    :cond_5
    move-wide/from16 v0, v24

    long-to-double v0, v0

    move-wide/from16 v27, v0

    cmpl-double v3, v21, v27

    if-lez v3, :cond_7

    .line 673
    move-wide/from16 v0, v24

    long-to-double v0, v0

    move-wide/from16 v21, v0

    .line 674
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 679
    :goto_0
    const-wide/16 v27, 0x0

    cmpg-double v3, v11, v27

    if-gez v3, :cond_6

    const-wide/16 v11, 0x0

    .line 680
    :cond_6
    const v3, 0x7f0b011e

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v26

    .end local v26           #tv:Landroid/widget/TextView;
    check-cast v26, Landroid/widget/TextView;

    .line 681
    .restart local v26       #tv:Landroid/widget/TextView;
    move-object/from16 v0, v19

    invoke-virtual {v0, v11, v12}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 683
    const v3, 0x7f0b0124

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v26

    .end local v26           #tv:Landroid/widget/TextView;
    check-cast v26, Landroid/widget/TextView;

    .line 684
    .restart local v26       #tv:Landroid/widget/TextView;
    move-object/from16 v0, v19

    invoke-virtual {v0, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 686
    const v3, 0x7f0b0121

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .end local v20           #iv:Landroid/widget/ImageView;
    check-cast v20, Landroid/widget/ImageView;

    .line 687
    .restart local v20       #iv:Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    move-wide/from16 v0, v21

    double-to-int v8, v0

    const/16 v27, -0x2

    move/from16 v0, v27

    invoke-direct {v3, v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 689
    invoke-direct/range {p0 .. p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshDetailsUnavailableImage()V

    .line 690
    return-void

    .line 676
    :cond_7
    const/16 v3, 0x8

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private refreshData()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 143
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshAvatar()V

    .line 144
    iput-boolean v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->hasDetails:Z

    .line 145
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadMealsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadMealsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;

    invoke-virtual {v0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->cancel(Z)Z

    .line 148
    :cond_0
    new-instance v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;-><init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;)V

    iput-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadMealsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;

    .line 149
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mLoadMealsTask:Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;

    new-array v1, v2, [Ljava/lang/Long;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$LoadMealsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 150
    return-void
.end method

.method private refreshDetailsButton()V
    .locals 2

    .prologue
    .line 313
    const v1, 0x7f0b0116

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 314
    .local v0, btn:Landroid/widget/Button;
    iget-boolean v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->isDetailVisible:Z

    if-eqz v1, :cond_0

    .line 315
    const v1, 0x7f02006b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 319
    :goto_0
    return-void

    .line 317
    :cond_0
    const v1, 0x7f020050

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private refreshDetailsUnavailableImage()V
    .locals 2

    .prologue
    .line 693
    const v1, 0x7f0b0118

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 694
    .local v0, iv:Landroid/widget/ImageView;
    iget-boolean v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->hasDetails:Z

    if-eqz v1, :cond_0

    .line 695
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 699
    :goto_0
    return-void

    .line 697
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private refreshDetailsVisibility()V
    .locals 2

    .prologue
    .line 322
    const v1, 0x7f0b0117

    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 323
    .local v0, detailsView:Landroid/widget/LinearLayout;
    iget-boolean v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->isDetailVisible:Z

    if-eqz v1, :cond_0

    .line 324
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 329
    :goto_0
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshDetailsButton()V

    .line 330
    return-void

    .line 326
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private refreshExercisesContent()V
    .locals 17

    .prologue
    .line 526
    const v13, 0x7f0b0037

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 527
    .local v9, layout:Landroid/widget/LinearLayout;
    const-wide/16 v13, 0x0

    move-object/from16 v0, p0

    iput-wide v13, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    .line 529
    invoke-virtual {v9}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 531
    const/high16 v13, 0x3f80

    invoke-virtual {v9, v13}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    .line 532
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 534
    const/4 v11, 0x0

    .line 536
    .local v11, tv:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mExerciseItems:Ljava/util/ArrayList;

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mExerciseItems:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-nez v13, :cond_1

    .line 537
    :cond_0
    const v13, 0x7f0b0126

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 538
    .restart local v11       #tv:Landroid/widget/TextView;
    const-string v13, "0 cal"

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597
    :goto_0
    return-void

    .line 541
    :cond_1
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->hasDetails:Z

    .line 543
    const v13, 0x7f0b0118

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 544
    .local v8, iv:Landroid/widget/ImageView;
    const/16 v13, 0x8

    invoke-virtual {v8, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 546
    new-instance v10, Ljava/text/DecimalFormatSymbols;

    new-instance v13, Ljava/util/Locale;

    const-string v14, "en"

    const-string v15, "en"

    invoke-direct {v13, v14, v15}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v10, v13}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 547
    .local v10, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v13, 0x2e

    invoke-virtual {v10, v13}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 548
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v13, "#"

    invoke-direct {v3, v13, v10}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 550
    .local v3, df:Ljava/text/DecimalFormat;
    invoke-virtual/range {p0 .. p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "layout_inflater"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 552
    .local v6, inflater:Landroid/view/LayoutInflater;
    const/4 v4, 0x0

    .line 553
    .local v4, exerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;
    const/4 v2, 0x0

    .line 555
    .local v2, childCount:I
    const v13, 0x7f030007

    const/4 v14, 0x0

    invoke-virtual {v6, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 557
    .local v7, itemLayout:Landroid/widget/LinearLayout;
    const v13, 0x7f0b000c

    invoke-virtual {v7, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 558
    .restart local v11       #tv:Landroid/widget/TextView;
    const-string v13, "Exercises"

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 559
    const v13, 0x7f0b000f

    invoke-virtual {v7, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 560
    .local v12, tvHeaderValue:Landroid/widget/TextView;
    const/4 v13, 0x0

    const/16 v14, 0x24

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v7, v13, v14, v15, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 562
    invoke-virtual {v9, v7, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 563
    add-int/lit8 v2, v2, 0x1

    .line 565
    const/4 v1, 0x0

    .line 566
    .local v1, btnEdit:Landroid/widget/Button;
    const v13, 0x7f0b0010

    invoke-virtual {v7, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1           #btnEdit:Landroid/widget/Button;
    check-cast v1, Landroid/widget/Button;

    .line 567
    .restart local v1       #btnEdit:Landroid/widget/Button;
    const/4 v13, 0x0

    invoke-virtual {v1, v13}, Landroid/widget/Button;->setVisibility(I)V

    .line 569
    new-instance v13, Lorg/medhelp/mydiet/activity/TabTrackItActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$4;-><init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;)V

    invoke-virtual {v1, v13}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 577
    const/4 v5, 0x0

    .local v5, i:I
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mExerciseItems:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lt v5, v13, :cond_2

    .line 593
    new-instance v13, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    invoke-virtual {v3, v14, v15}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, " cal"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 595
    const v13, 0x7f0b0126

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 596
    .restart local v11       #tv:Landroid/widget/TextView;
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, " "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    invoke-virtual {v3, v14, v15}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " cal"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 578
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mExerciseItems:Ljava/util/ArrayList;

    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4           #exerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;
    check-cast v4, Lorg/medhelp/mydiet/model/ExerciseItem;

    .line 580
    .restart local v4       #exerciseItem:Lorg/medhelp/mydiet/model/ExerciseItem;
    const v13, 0x7f030006

    const/4 v14, 0x0

    invoke-virtual {v6, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .end local v7           #itemLayout:Landroid/widget/LinearLayout;
    check-cast v7, Landroid/widget/LinearLayout;

    .line 581
    .restart local v7       #itemLayout:Landroid/widget/LinearLayout;
    const v13, 0x7f0b000c

    invoke-virtual {v7, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 582
    .restart local v11       #tv:Landroid/widget/TextView;
    new-instance v13, Ljava/lang/StringBuilder;

    iget-object v14, v4, Lorg/medhelp/mydiet/model/ExerciseItem;->name:Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, ", "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-wide v14, v4, Lorg/medhelp/mydiet/model/ExerciseItem;->timePeriod:D

    invoke-virtual {v3, v14, v15}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "min"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 583
    const v13, 0x7f0b000f

    invoke-virtual {v7, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11           #tv:Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 584
    .restart local v11       #tv:Landroid/widget/TextView;
    iget-wide v13, v4, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    invoke-virtual {v3, v13, v14}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    move-object/from16 v0, p0

    iget-wide v13, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    iget-wide v15, v4, Lorg/medhelp/mydiet/model/ExerciseItem;->calories:D

    add-double/2addr v13, v15

    move-object/from16 v0, p0

    iput-wide v13, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    .line 589
    invoke-virtual {v9, v7, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 590
    add-int/lit8 v2, v2, 0x1

    .line 577
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1
.end method

.method private refreshMealsContent()V
    .locals 40

    .prologue
    .line 333
    const/16 v24, 0x0

    .line 334
    .local v24, layout:Landroid/widget/LinearLayout;
    const-wide/16 v32, 0x0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    .line 335
    const-wide/16 v32, 0x0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromCarbs:D

    .line 336
    const-wide/16 v32, 0x0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromFat:D

    .line 337
    const-wide/16 v32, 0x0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromProtein:D

    .line 339
    const v32, 0x7f0b007a

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #layout:Landroid/widget/LinearLayout;
    check-cast v24, Landroid/widget/LinearLayout;

    .line 340
    .restart local v24       #layout:Landroid/widget/LinearLayout;
    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 341
    const/16 v30, 0x0

    .line 342
    .local v30, tv:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mMeals:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    if-eqz v32, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mMeals:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v32

    if-nez v32, :cond_1

    .line 343
    :cond_0
    const v32, 0x7f0b0125

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 344
    .restart local v30       #tv:Landroid/widget/TextView;
    const-string v32, "0 cal"

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 523
    :goto_0
    return-void

    .line 347
    :cond_1
    const/16 v32, 0x1

    move/from16 v0, v32

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->hasDetails:Z

    .line 349
    new-instance v29, Ljava/text/DecimalFormatSymbols;

    new-instance v32, Ljava/util/Locale;

    const-string v33, "en"

    const-string v34, "en"

    invoke-direct/range {v32 .. v34}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 350
    .local v29, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v32, 0x2e

    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 351
    new-instance v14, Ljava/text/DecimalFormat;

    const-string v32, "#"

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-direct {v14, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 353
    .local v14, df:Ljava/text/DecimalFormat;
    invoke-virtual/range {p0 .. p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v32

    const-string v33, "layout_inflater"

    invoke-virtual/range {v32 .. v33}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/view/LayoutInflater;

    .line 356
    .local v20, inflater:Landroid/view/LayoutInflater;
    const/16 v18, 0x0

    .line 357
    .local v18, foodItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/medhelp/mydiet/model/FoodItemInMeal;>;"
    const/16 v17, 0x0

    .line 358
    .local v17, foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const/4 v15, 0x0

    .line 359
    .local v15, food:Lorg/medhelp/mydiet/model/Food;
    const/16 v16, 0x0

    .line 360
    .local v16, foodDetail:Lorg/medhelp/mydiet/model/FoodDetail;
    const-wide/16 v27, 0x0

    .line 362
    .local v27, servingId:J
    const/16 v31, 0x0

    .line 364
    .local v31, tvHeaderValue:Landroid/widget/TextView;
    const/4 v11, 0x0

    .line 365
    .local v11, childCount:I
    const-wide/16 v7, 0x0

    .line 366
    .local v7, caloriesInFood:D
    const-wide/16 v9, 0x0

    .line 368
    .local v9, caloriesInMeal:D
    const v32, 0x7f0b0118

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ImageView;

    .line 369
    .local v22, iv:Landroid/widget/ImageView;
    const/16 v32, 0x8

    move-object/from16 v0, v22

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 371
    const/4 v6, 0x0

    .line 373
    .local v6, btnEdit:Landroid/widget/Button;
    const/16 v19, 0x0

    .local v19, i:I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mMeals:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v32

    move/from16 v0, v19

    move/from16 v1, v32

    if-lt v0, v1, :cond_2

    .line 473
    const v32, 0x7f0b0125

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 474
    .restart local v30       #tv:Landroid/widget/TextView;
    new-instance v32, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    move-wide/from16 v33, v0

    move-wide/from16 v0, v33

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v33, " cal"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    const v32, 0x7f0b011b

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .end local v24           #layout:Landroid/widget/LinearLayout;
    check-cast v24, Landroid/widget/LinearLayout;

    .line 478
    .restart local v24       #layout:Landroid/widget/LinearLayout;
    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 480
    const v32, 0x7f030007

    const/16 v33, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/LinearLayout;

    .line 481
    .local v21, itemLayout:Landroid/widget/LinearLayout;
    const v32, 0x7f02002a

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 484
    const v32, 0x7f0b000c

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 485
    .restart local v30       #tv:Landroid/widget/TextView;
    const/16 v32, -0x1

    move-object/from16 v0, v30

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 486
    new-instance v32, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v33, -0x1

    const/16 v34, -0x2

    invoke-direct/range {v32 .. v34}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 487
    const/16 v32, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 488
    new-instance v32, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    move-wide/from16 v33, v0

    move-wide/from16 v0, v33

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v33, " Total Calories"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 489
    const/16 v32, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 491
    const v32, 0x7f030004

    const/16 v33, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 492
    .local v12, contentsLayout:Landroid/widget/LinearLayout;
    const v32, 0x7f020029

    move/from16 v0, v32

    invoke-virtual {v12, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 493
    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0xc

    const/16 v35, 0x0

    move/from16 v0, v32

    move/from16 v1, v33

    move/from16 v2, v34

    move/from16 v3, v35

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 494
    const/16 v32, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v12, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 496
    const/4 v11, 0x0

    .line 497
    const v32, 0x7f030006

    const/16 v33, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v21

    .end local v21           #itemLayout:Landroid/widget/LinearLayout;
    check-cast v21, Landroid/widget/LinearLayout;

    .line 498
    .restart local v21       #itemLayout:Landroid/widget/LinearLayout;
    const v32, 0x7f0b000c

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 499
    .restart local v30       #tv:Landroid/widget/TextView;
    const v32, 0x7f02007d

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 500
    const-string v32, "From Carbs"

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    const v32, 0x7f0b000f

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 502
    .restart local v30       #tv:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromCarbs:D

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 503
    move-object/from16 v0, v21

    invoke-virtual {v12, v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 504
    add-int/lit8 v11, v11, 0x1

    .line 506
    const v32, 0x7f030006

    const/16 v33, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v21

    .end local v21           #itemLayout:Landroid/widget/LinearLayout;
    check-cast v21, Landroid/widget/LinearLayout;

    .line 507
    .restart local v21       #itemLayout:Landroid/widget/LinearLayout;
    const v32, 0x7f0b000c

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 508
    .restart local v30       #tv:Landroid/widget/TextView;
    const v32, 0x7f02007e

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 509
    const-string v32, "From Fats"

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 510
    const v32, 0x7f0b000f

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 511
    .restart local v30       #tv:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromFat:D

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 512
    move-object/from16 v0, v21

    invoke-virtual {v12, v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 513
    add-int/lit8 v11, v11, 0x1

    .line 515
    const v32, 0x7f030006

    const/16 v33, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v21

    .end local v21           #itemLayout:Landroid/widget/LinearLayout;
    check-cast v21, Landroid/widget/LinearLayout;

    .line 516
    .restart local v21       #itemLayout:Landroid/widget/LinearLayout;
    const v32, 0x7f0b000c

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 517
    .restart local v30       #tv:Landroid/widget/TextView;
    const v32, 0x7f02007f

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 518
    const-string v32, "From Protein"

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 519
    const v32, 0x7f0b000f

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 520
    .restart local v30       #tv:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromProtein:D

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v14, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 521
    move-object/from16 v0, v21

    invoke-virtual {v12, v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 522
    add-int/lit8 v11, v11, 0x1

    .line 523
    goto/16 :goto_0

    .line 375
    .end local v12           #contentsLayout:Landroid/widget/LinearLayout;
    .end local v21           #itemLayout:Landroid/widget/LinearLayout;
    :cond_2
    const-wide/16 v7, 0x0

    .line 376
    const-wide/16 v9, 0x0

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mMeals:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lorg/medhelp/mydiet/model/Meal;

    .line 379
    .local v25, meal:Lorg/medhelp/mydiet/model/Meal;
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 381
    .local v5, __size:I
    if-lez v5, :cond_3

    .line 382
    const v32, 0x7f030007

    const/16 v33, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/LinearLayout;

    .line 383
    .restart local v21       #itemLayout:Landroid/widget/LinearLayout;
    const v32, 0x7f0b000c

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 384
    .restart local v30       #tv:Landroid/widget/TextView;
    invoke-virtual/range {v25 .. v25}, Lorg/medhelp/mydiet/model/Meal;->getMealTypeString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    const v32, 0x7f0b000f

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v31

    .end local v31           #tvHeaderValue:Landroid/widget/TextView;
    check-cast v31, Landroid/widget/TextView;

    .line 386
    .restart local v31       #tvHeaderValue:Landroid/widget/TextView;
    const/16 v32, 0x0

    const/16 v33, 0x24

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 388
    const v32, 0x7f0b0010

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6           #btnEdit:Landroid/widget/Button;
    check-cast v6, Landroid/widget/Button;

    .line 389
    .restart local v6       #btnEdit:Landroid/widget/Button;
    const/16 v32, 0x0

    move/from16 v0, v32

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 391
    new-instance v32, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity$3;-><init>(Lorg/medhelp/mydiet/activity/TabTrackItActivity;Lorg/medhelp/mydiet/model/Meal;)V

    move-object/from16 v0, v32

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 410
    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 411
    add-int/lit8 v11, v11, 0x1

    .line 413
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/medhelp/mydiet/model/Meal;->foods:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    .line 415
    const/16 v23, 0x0

    .local v23, j:I
    :goto_2
    move/from16 v0, v23

    if-lt v0, v5, :cond_4

    .line 454
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    move-object/from16 v32, v0

    if-eqz v32, :cond_8

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v32

    if-lez v32, :cond_8

    .line 455
    const v32, 0x7f030006

    const/16 v33, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v21

    .end local v21           #itemLayout:Landroid/widget/LinearLayout;
    check-cast v21, Landroid/widget/LinearLayout;

    .line 456
    .restart local v21       #itemLayout:Landroid/widget/LinearLayout;
    const v32, 0x7f0b000c

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 457
    .restart local v30       #tv:Landroid/widget/TextView;
    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "Notes: "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/medhelp/mydiet/model/Meal;->note:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 458
    const v32, 0x7f070007

    move-object/from16 v0, v30

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 460
    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 461
    add-int/lit8 v11, v11, 0x1

    .line 467
    .end local v21           #itemLayout:Landroid/widget/LinearLayout;
    .end local v23           #j:I
    :cond_3
    :goto_3
    move-object/from16 v0, v25

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    move-wide/from16 v32, v0

    const-wide/high16 v34, -0x4010

    cmpl-double v32, v32, v34

    if-nez v32, :cond_9

    const-wide/16 v32, 0x0

    :goto_4
    add-double v9, v9, v32

    .line 468
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-virtual {v14, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v33, " cal"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    move-wide/from16 v32, v0

    add-double v32, v32, v9

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    .line 373
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_1

    .line 416
    .restart local v21       #itemLayout:Landroid/widget/LinearLayout;
    .restart local v23       #j:I
    :cond_4
    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    .end local v17           #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    check-cast v17, Lorg/medhelp/mydiet/model/FoodItemInMeal;

    .line 418
    .restart local v17       #foodItem:Lorg/medhelp/mydiet/model/FoodItemInMeal;
    const v32, 0x7f030006

    const/16 v33, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v21

    .end local v21           #itemLayout:Landroid/widget/LinearLayout;
    check-cast v21, Landroid/widget/LinearLayout;

    .line 419
    .restart local v21       #itemLayout:Landroid/widget/LinearLayout;
    const v32, 0x7f0b000c

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 420
    .restart local v30       #tv:Landroid/widget/TextView;
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->name:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    const v32, 0x7f0b000f

    move-object/from16 v0, v21

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .end local v30           #tv:Landroid/widget/TextView;
    check-cast v30, Landroid/widget/TextView;

    .line 423
    .restart local v30       #tv:Landroid/widget/TextView;
    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 424
    add-int/lit8 v11, v11, 0x1

    .line 426
    invoke-static/range {p0 .. p0}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getInstance(Landroid/content/Context;)Lorg/medhelp/mydiet/model/DietDiaryDAO;

    move-result-object v13

    .line 428
    .local v13, dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    move-object/from16 v0, v17

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v13, v0, v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodWithInAppId(J)Lorg/medhelp/mydiet/model/Food;

    move-result-object v15

    .line 430
    move-object/from16 v0, v17

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->inAppId:J

    move-wide/from16 v32, v0

    const-wide/16 v34, 0x0

    cmp-long v32, v32, v34

    if-lez v32, :cond_5

    if-nez v15, :cond_6

    .line 431
    :cond_5
    move-object/from16 v0, v17

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->medhelpId:J

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v13, v0, v1}, Lorg/medhelp/mydiet/model/DietDiaryDAO;->getFoodWithMedHelpId(J)Lorg/medhelp/mydiet/model/Food;

    move-result-object v15

    .line 434
    :cond_6
    iget-object v0, v15, Lorg/medhelp/mydiet/model/Food;->foodJSON:Ljava/lang/String;

    move-object/from16 v32, v0

    iget-object v0, v15, Lorg/medhelp/mydiet/model/Food;->foodServingsJSON:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v32 .. v33}, Lorg/medhelp/mydiet/util/DataUtil;->getFoodDetail(Ljava/lang/String;Ljava/lang/String;)Lorg/medhelp/mydiet/model/FoodDetail;

    move-result-object v16

    .line 435
    move-object/from16 v0, v17

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->servingId:J

    move-wide/from16 v27, v0

    .line 436
    move-wide/from16 v0, v27

    invoke-virtual {v15, v0, v1}, Lorg/medhelp/mydiet/model/Food;->getFoodServing(J)Lorg/medhelp/mydiet/model/FoodServing;

    move-result-object v26

    .line 439
    .local v26, selectedServing:Lorg/medhelp/mydiet/model/FoodServing;
    if-eqz v16, :cond_7

    .line 440
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromProtein:D

    move-wide/from16 v32, v0

    const-wide/high16 v34, 0x4010

    .line 441
    move-object/from16 v0, v17

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    move-wide/from16 v36, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodDetail;->protein:D

    move-wide/from16 v38, v0

    move-object/from16 v0, v26

    move-wide/from16 v1, v36

    move-wide/from16 v3, v38

    invoke-static {v0, v1, v2, v3, v4}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v36

    mul-double v34, v34, v36

    add-double v32, v32, v34

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromProtein:D

    .line 442
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromCarbs:D

    move-wide/from16 v32, v0

    const-wide/high16 v34, 0x4010

    .line 443
    move-object/from16 v0, v17

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    move-wide/from16 v36, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodDetail;->totalCarbohydrate:D

    move-wide/from16 v38, v0

    move-object/from16 v0, v26

    move-wide/from16 v1, v36

    move-wide/from16 v3, v38

    invoke-static {v0, v1, v2, v3, v4}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v36

    mul-double v34, v34, v36

    add-double v32, v32, v34

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromCarbs:D

    .line 444
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromFat:D

    move-wide/from16 v32, v0

    const-wide/high16 v34, 0x4022

    .line 445
    move-object/from16 v0, v17

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    move-wide/from16 v36, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodDetail;->totalFat:D

    move-wide/from16 v38, v0

    move-object/from16 v0, v26

    move-wide/from16 v1, v36

    move-wide/from16 v3, v38

    invoke-static {v0, v1, v2, v3, v4}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v36

    mul-double v34, v34, v36

    add-double v32, v32, v34

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCaloriesFromFat:D

    .line 448
    move-object/from16 v0, v17

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodItemInMeal;->amount:D

    move-wide/from16 v32, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/FoodDetail;->calories:D

    move-wide/from16 v34, v0

    .line 447
    move-object/from16 v0, v26

    move-wide/from16 v1, v32

    move-wide/from16 v3, v34

    invoke-static {v0, v1, v2, v3, v4}, Lorg/medhelp/mydiet/util/DataUtil;->getCalculatedQuantity(Lorg/medhelp/mydiet/model/FoodServing;DD)D

    move-result-wide v7

    .line 449
    add-double/2addr v9, v7

    .line 450
    invoke-virtual {v14, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    :cond_7
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_2

    .line 463
    .end local v13           #dao:Lorg/medhelp/mydiet/model/DietDiaryDAO;
    .end local v26           #selectedServing:Lorg/medhelp/mydiet/model/FoodServing;
    :cond_8
    const/16 v32, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 467
    .end local v21           #itemLayout:Landroid/widget/LinearLayout;
    .end local v23           #j:I
    :cond_9
    move-object/from16 v0, v25

    iget-wide v0, v0, Lorg/medhelp/mydiet/model/Meal;->userInputCalories:D

    move-wide/from16 v32, v0

    goto/16 :goto_4
.end method

.method private refreshWeightContent()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 600
    iget-wide v6, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mWeight:D

    const-wide/high16 v8, -0x4010

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_0

    .line 601
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightInLb(Landroid/content/Context;)F

    move-result v6

    float-to-double v6, v6

    iput-wide v6, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mWeight:D

    .line 604
    :cond_0
    new-instance v1, Ljava/text/DecimalFormatSymbols;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "en"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 605
    .local v1, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v6, 0x2e

    invoke-virtual {v1, v6}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 606
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v6, "#.#"

    invoke-direct {v0, v6, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 608
    .local v0, df1:Ljava/text/DecimalFormat;
    const/4 v5, 0x0

    .line 609
    .local v5, weightString:Ljava/lang/String;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getWeightUnits(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "lb"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 610
    iget-wide v6, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mWeight:D

    cmpl-double v6, v6, v10

    if-lez v6, :cond_2

    .line 611
    new-instance v6, Ljava/lang/StringBuilder;

    iget-wide v7, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mWeight:D

    invoke-virtual {v0, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " lb"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 624
    :goto_0
    if-eqz v5, :cond_1

    .line 625
    const v6, 0x7f0b0127

    invoke-virtual {p0, v6}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 626
    .local v2, tv:Landroid/widget/TextView;
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 629
    .end local v2           #tv:Landroid/widget/TextView;
    :cond_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshCalorieProgress()V

    .line 630
    return-void

    .line 613
    :cond_2
    const-string v5, "0 lb"

    goto :goto_0

    .line 616
    :cond_3
    iget-wide v6, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mWeight:D

    const-wide v8, 0x3fdd07a84ab75e51L

    mul-double v3, v6, v8

    .line 617
    .local v3, weightInKG:D
    cmpl-double v6, v3, v10

    if-lez v6, :cond_4

    .line 618
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " kg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 620
    :cond_4
    const-string v5, "0 kg"

    goto :goto_0
.end method

.method private declared-synchronized setDate(III)V
    .locals 3
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    .prologue
    .line 251
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 252
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mBtnDate:Landroid/widget/Button;

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    const-string v2, "EEE, MM/dd/yyyy"

    invoke-static {v1, v2}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 253
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshData()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    monitor-exit p0

    return-void

    .line 251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private showCalorieBreakDownDialog()V
    .locals 13

    .prologue
    .line 702
    move-object v1, p0

    .line 703
    .local v1, ctx:Landroid/content/Context;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 704
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v9, "layout_inflater"

    invoke-virtual {v1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 706
    .local v3, inflater:Landroid/view/LayoutInflater;
    const v10, 0x7f030008

    const v9, 0x7f0b0011

    invoke-virtual {p0, v9}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    invoke-virtual {v3, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 707
    .local v4, layout:Landroid/view/View;
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 708
    const-string v9, "Remaining Calories"

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 710
    new-instance v7, Ljava/text/DecimalFormatSymbols;

    new-instance v9, Ljava/util/Locale;

    const-string v10, "en"

    const-string v11, "en"

    invoke-direct {v9, v10, v11}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v7, v9}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 711
    .local v7, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v9, 0x2e

    invoke-virtual {v7, v9}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 712
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v9, "0"

    invoke-direct {v2, v9, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 714
    .local v2, df0:Ljava/text/DecimalFormat;
    const v9, 0x7f0b0012

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 715
    .local v8, tv:Landroid/widget/TextView;
    iget-wide v9, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalorieBudget:D

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 717
    const v9, 0x7f0b0013

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8           #tv:Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 718
    .restart local v8       #tv:Landroid/widget/TextView;
    iget-wide v9, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 720
    const v9, 0x7f0b0014

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8           #tv:Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 721
    .restart local v8       #tv:Landroid/widget/TextView;
    iget-wide v9, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 723
    iget-wide v9, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalorieBudget:D

    iget-wide v11, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryFoodCalories:D

    sub-double/2addr v9, v11

    iget-wide v11, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mSummaryExerciseCalories:D

    add-double v5, v9, v11

    .line 725
    .local v5, remainingCalories:D
    const v9, 0x7f0b0016

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8           #tv:Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 726
    .restart local v8       #tv:Landroid/widget/TextView;
    const-wide/16 v9, 0x0

    cmpg-double v9, v5, v9

    if-gez v9, :cond_1

    .line 727
    const/high16 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 728
    const-wide/high16 v9, -0x4010

    mul-double/2addr v9, v5

    invoke-virtual {v2, v9, v10}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 734
    :goto_0
    const v9, 0x7f0b0015

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8           #tv:Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 735
    .restart local v8       #tv:Landroid/widget/TextView;
    const-wide/16 v9, 0x0

    cmpg-double v9, v5, v9

    if-gez v9, :cond_0

    .line 736
    const/high16 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 737
    const-string v9, "Calories over budget"

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 740
    :cond_0
    const-string v9, "OK"

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 741
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    iput-object v9, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mDialog:Landroid/app/AlertDialog;

    .line 742
    iget-object v9, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    .line 743
    return-void

    .line 730
    :cond_1
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070005

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 731
    invoke-virtual {v2, v5, v6}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private showProgressBar()V
    .locals 2

    .prologue
    .line 879
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 880
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 233
    packed-switch p1, :pswitch_data_0

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 235
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->isDetailVisible:Z

    .line 237
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshDetailsVisibility()V

    goto :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0xbb9
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    const/4 v6, 0x5

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 186
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 229
    :goto_0
    return-void

    .line 188
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lorg/medhelp/mydiet/activity/SetupScreen1;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 189
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "edit_preferences"

    const-string v4, "edit_preferences"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    invoke-virtual {p0, v0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 193
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_1
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->onFoodClick()V

    goto :goto_0

    .line 196
    :sswitch_2
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->onExercisesClick()V

    goto :goto_0

    .line 199
    :sswitch_3
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lorg/medhelp/mydiet/activity/WeightActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 200
    .local v2, weightIntent:Landroid/content/Intent;
    const-string v3, "date"

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 201
    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 204
    .end local v2           #weightIntent:Landroid/content/Intent;
    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lorg/medhelp/mydiet/activity/WaterActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 205
    .local v1, waterIntent:Landroid/content/Intent;
    const-string v3, "date"

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 206
    invoke-virtual {p0, v1}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 209
    .end local v1           #waterIntent:Landroid/content/Intent;
    :sswitch_5
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    const/4 v4, -0x1

    invoke-virtual {v3, v6, v4}, Ljava/util/Calendar;->add(II)V

    .line 210
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mBtnDate:Landroid/widget/Button;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    const-string v5, "EEE, MM/dd/yyyy"

    invoke-static {v4, v5}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 211
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshData()V

    goto :goto_0

    .line 214
    :sswitch_6
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v3, v6, v4}, Ljava/util/Calendar;->add(II)V

    .line 215
    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mBtnDate:Landroid/widget/Button;

    iget-object v4, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    const-string v5, "EEE, MM/dd/yyyy"

    invoke-static {v4, v5}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 216
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshData()V

    goto :goto_0

    .line 219
    :sswitch_7
    invoke-virtual {p0, v3}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->showDialog(I)V

    goto :goto_0

    .line 222
    :sswitch_8
    iget-boolean v5, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->isDetailVisible:Z

    if-eqz v5, :cond_0

    :goto_1
    iput-boolean v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->isDetailVisible:Z

    .line 223
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshDetailsVisibility()V

    goto/16 :goto_0

    :cond_0
    move v3, v4

    .line 222
    goto :goto_1

    .line 226
    :sswitch_9
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->showCalorieBreakDownDialog()V

    goto/16 :goto_0

    .line 186
    :sswitch_data_0
    .sparse-switch
        0x7f0b00e6 -> :sswitch_5
        0x7f0b00e7 -> :sswitch_0
        0x7f0b00e8 -> :sswitch_6
        0x7f0b00eb -> :sswitch_3
        0x7f0b0115 -> :sswitch_7
        0x7f0b0116 -> :sswitch_8
        0x7f0b011d -> :sswitch_9
        0x7f0b0128 -> :sswitch_1
        0x7f0b0129 -> :sswitch_2
        0x7f0b012a -> :sswitch_4
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    const-wide/16 v4, 0x0

    .line 95
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    const v2, 0x7f03002f

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->setContentView(I)V

    .line 98
    const v2, 0x7f0b0115

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mBtnDate:Landroid/widget/Button;

    .line 99
    invoke-virtual {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->getParent()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0b0001

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mProgress:Landroid/widget/ProgressBar;

    .line 101
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mBtnDate:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v2, 0x7f0b0128

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    const v2, 0x7f0b0129

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    const v2, 0x7f0b00eb

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    const v2, 0x7f0b012a

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    const v2, 0x7f0b00e6

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    const v2, 0x7f0b00e8

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    const v2, 0x7f0b0116

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    const v2, 0x7f0b011d

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    const v2, 0x7f0b00e7

    invoke-virtual {p0, v2}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    if-eqz p1, :cond_1

    .line 113
    const-string v2, "date"

    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 114
    .local v0, timeInMillis:J
    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    .line 115
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    .line 118
    :cond_0
    const-string v2, "is_detail_visible"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->isDetailVisible:Z

    .line 121
    .end local v0           #timeInMillis:J
    :cond_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    .line 122
    :cond_2
    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getTodayMidnightCalendar()Ljava/util/Calendar;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    .line 124
    :cond_3
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mBtnDate:Landroid/widget/Button;

    iget-object v3, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    const-string v4, "EEE, MM/dd/yyyy"

    invoke-static {v3, v4}, Lorg/medhelp/mydiet/util/DateUtil;->formatDate(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 125
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "id"

    .prologue
    .line 154
    packed-switch p1, :pswitch_data_0

    .line 160
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 156
    :pswitch_0
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->dateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 157
    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    const/4 v5, 0x5

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v1, p0

    .line 156
    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 158
    .local v0, datePickerDialog:Landroid/app/DatePickerDialog;
    goto :goto_0

    .line 154
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 139
    :cond_0
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onDestroy()V

    .line 140
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    const-wide/16 v4, 0x0

    .line 170
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 172
    if-nez p1, :cond_0

    .line 182
    :goto_0
    return-void

    .line 174
    :cond_0
    const-string v2, "date"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 175
    .local v0, timeInMillis:J
    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    .line 176
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lorg/medhelp/mydiet/util/DateUtil;->dateToCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    .line 178
    :cond_1
    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    .line 179
    :cond_2
    invoke-static {}, Lorg/medhelp/mydiet/util/DateUtil;->getTodayMidnightCalendar()Ljava/util/Calendar;

    move-result-object v2

    iput-object v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    .line 181
    :cond_3
    const-string v2, "is_detail_visible"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->isDetailVisible:Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter "outState"

    .prologue
    .line 164
    const-string v0, "date"

    iget-object v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 165
    const-string v0, "is_detail_visible"

    iget-boolean v1, p0, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->isDetailVisible:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    invoke-super {p0, p1}, Lorg/medhelp/mydiet/activity/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 167
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 128
    invoke-super {p0}, Lorg/medhelp/mydiet/activity/BaseActivity;->onStart()V

    .line 129
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshData()V

    .line 132
    invoke-direct {p0}, Lorg/medhelp/mydiet/activity/TabTrackItActivity;->refreshDetailsVisibility()V

    .line 133
    return-void
.end method
