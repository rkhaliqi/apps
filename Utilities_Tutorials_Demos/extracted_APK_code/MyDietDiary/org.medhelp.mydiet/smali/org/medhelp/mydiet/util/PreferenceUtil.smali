.class public Lorg/medhelp/mydiet/util/PreferenceUtil;
.super Ljava/lang/Object;
.source "PreferenceUtil.java"

# interfaces
.implements Lorg/medhelp/mydiet/C$preferenceData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getActivityLevel(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .parameter "c"

    .prologue
    const/4 v4, 0x0

    .line 735
    const-string v1, "mh_preferences"

    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 736
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "activity_level"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getActivityLevelUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 748
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 749
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_activity_level"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getAvatarPosition(Landroid/content/Context;)I
    .locals 3
    .parameter "context"

    .prologue
    const/4 v2, 0x0

    .line 213
    const-string v1, "mh_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 214
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "avatar_position"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getAvatarUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 226
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 227
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_avatar"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getBMRUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 787
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 788
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_bmr"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getBirthdayUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 250
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 251
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_birthday"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getConfigsLastSyncTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 200
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 201
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "last_sync_time"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getDateOfBirth(Landroid/content/Context;)Ljava/util/Calendar;
    .locals 7
    .parameter "c"

    .prologue
    const/4 v6, 0x1

    .line 279
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 280
    .local v3, preferences:Landroid/content/SharedPreferences;
    const-string v4, "dob_year"

    const/16 v5, 0x7b9

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 281
    .local v2, dobYear:I
    const-string v4, "dob_month"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 282
    .local v1, dobMonth:I
    const-string v4, "dob_day"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 285
    .local v0, dobDay:I
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4, v2, v1, v0}, Ljava/util/GregorianCalendar;-><init>(III)V

    return-object v4
.end method

.method public static getDateOfBirthDay(Landroid/content/Context;)I
    .locals 3
    .parameter "c"

    .prologue
    .line 274
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 275
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "dob_day"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getDateOfBirthMonth(Landroid/content/Context;)I
    .locals 3
    .parameter "c"

    .prologue
    .line 269
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 270
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "dob_month"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getDateOfBirthString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "c"
    .parameter "format"

    .prologue
    .line 291
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 292
    .local v0, dateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getDateOfBirth(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v1

    .line 293
    .local v1, dob:Ljava/util/Calendar;
    if-eqz v1, :cond_0

    .line 294
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 296
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getDateOfBirthYear(Landroid/content/Context;)I
    .locals 3
    .parameter "c"

    .prologue
    .line 264
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 265
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "dob_year"

    const/16 v2, 0x7b9

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getDaysSinceLastLogin(Landroid/content/Context;)J
    .locals 6
    .parameter "context"

    .prologue
    .line 190
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getLastLoginPromptTime(Landroid/content/Context;)J

    move-result-wide v0

    .line 192
    .local v0, lastLoginTime:J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 193
    const-wide/16 v2, 0x3e8

    .line 195
    :goto_0
    return-wide v2

    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/32 v4, 0x5265c00

    div-long/2addr v2, v4

    goto :goto_0
.end method

.method public static getDesiredWeightInKG(Landroid/content/Context;)F
    .locals 10
    .parameter "c"

    .prologue
    .line 702
    const-string v7, "mh_preferences"

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 704
    .local v2, sp:Landroid/content/SharedPreferences;
    new-instance v3, Ljava/text/DecimalFormatSymbols;

    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 705
    .local v3, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v3, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 706
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v7, "#.##"

    invoke-direct {v0, v7, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 708
    .local v0, df:Ljava/text/DecimalFormat;
    const-string v7, "desired_weight_in_kg"

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v5

    .line 715
    .local v5, weightInKG:F
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 716
    .local v6, weightString:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 717
    const/4 v5, 0x0

    .line 721
    :cond_0
    float-to-double v7, v5

    :try_start_0
    invoke-virtual {v0, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    move v7, v5

    .line 730
    :goto_0
    return v7

    .line 722
    :catch_0
    move-exception v1

    .line 726
    .local v1, e:Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    .line 727
    float-to-int v4, v5

    .line 728
    .local v4, weight:I
    int-to-float v7, v4

    goto :goto_0
.end method

.method public static getDesiredWeightInLb(Landroid/content/Context;)F
    .locals 10
    .parameter "c"

    .prologue
    .line 671
    const-string v7, "mh_preferences"

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 673
    .local v2, sp:Landroid/content/SharedPreferences;
    new-instance v3, Ljava/text/DecimalFormatSymbols;

    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 674
    .local v3, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v3, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 675
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v7, "#.##"

    invoke-direct {v0, v7, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 677
    .local v0, df:Ljava/text/DecimalFormat;
    const-string v7, "desired_weight_in_lb"

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v5

    .line 683
    .local v5, weightInLb:F
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 684
    .local v6, weightString:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 685
    const/4 v5, 0x0

    .line 689
    :cond_0
    float-to-double v7, v5

    :try_start_0
    invoke-virtual {v0, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    move v7, v5

    .line 698
    :goto_0
    return v7

    .line 690
    :catch_0
    move-exception v1

    .line 694
    .local v1, e:Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    .line 695
    float-to-int v4, v5

    .line 696
    .local v4, weight:I
    int-to-float v7, v4

    goto :goto_0
.end method

.method public static getDistanceUnits(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "c"

    .prologue
    .line 416
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 417
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "units_distance"

    const-string v2, "mi"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getDistanceUnitsUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 429
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 430
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_distance_units"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getGender(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "context"

    .prologue
    .line 66
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 67
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "user_gender"

    const-string v2, "Female"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getGenderUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 87
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 88
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_gender"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getGoal(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .parameter "c"

    .prologue
    const/4 v4, 0x0

    .line 761
    const-string v1, "mh_preferences"

    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 762
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "weight_goal"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getGoalsUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 312
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 313
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_goals"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getHeightInCm(Landroid/content/Context;)F
    .locals 3
    .parameter "c"

    .prologue
    .line 523
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 524
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "height_in_cm"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    return v1
.end method

.method public static getHeightInInches(Landroid/content/Context;)F
    .locals 3
    .parameter "c"

    .prologue
    .line 518
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 519
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "height_in_inches"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    return v1
.end method

.method public static getHeightString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 7
    .parameter "c"
    .parameter "units"

    .prologue
    const/4 v6, 0x0

    .line 528
    const/4 v0, 0x0

    .line 529
    .local v0, height:Ljava/lang/String;
    const/4 v5, 0x1

    if-ne p1, v5, :cond_2

    .line 530
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightInInches(Landroid/content/Context;)F

    move-result v3

    .line 531
    .local v3, heightInInches:F
    cmpl-float v5, v3, v6

    if-lez v5, :cond_1

    .line 532
    const/high16 v5, 0x4140

    div-float v5, v3, v5

    float-to-int v1, v5

    .line 533
    .local v1, heightFeet:I
    mul-int/lit8 v5, v1, 0xc

    int-to-float v5, v5

    sub-float v5, v3, v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 534
    .local v4, heightInches:I
    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    .line 535
    add-int/lit8 v1, v1, 0x1

    .line 536
    const/4 v4, 0x0

    .line 538
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " ft, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 550
    .end local v1           #heightFeet:I
    .end local v3           #heightInInches:F
    .end local v4           #heightInches:I
    :goto_0
    return-object v0

    .line 540
    .restart local v3       #heightInInches:F
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 543
    .end local v3           #heightInInches:F
    :cond_2
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getHeightInCm(Landroid/content/Context;)F

    move-result v2

    .line 544
    .local v2, heightInCm:F
    cmpl-float v5, v2, v6

    if-lez v5, :cond_3

    .line 545
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " cm"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 547
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method public static getHeightUnits(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "c"

    .prologue
    .line 390
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 391
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "units_height"

    const-string v2, "in"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getHeightUnitsUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 403
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 404
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_height_units"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getHeightUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 505
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 506
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_height"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getLastLoginPromptTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 173
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 174
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "last_login_time"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getSavedUUID(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .parameter "context"

    .prologue
    .line 41
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 43
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v2, "generated_uuid"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, uuid:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 45
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-static {p0, v1}, Lorg/medhelp/mydiet/util/PreferenceUtil;->saveUUID(Landroid/content/Context;Ljava/lang/String;)V

    .line 49
    :cond_1
    return-object v1
.end method

.method public static getTargetDate(Landroid/content/Context;)Ljava/util/Calendar;
    .locals 7
    .parameter "c"

    .prologue
    const/4 v6, -0x1

    .line 340
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 341
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v4, "target_date_year"

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 342
    .local v3, targetDateYear:I
    const-string v4, "target_date_month"

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 343
    .local v2, targetDateMonth:I
    const-string v4, "target_date_day"

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 345
    .local v1, targetDateDay:I
    if-le v3, v6, :cond_0

    if-le v2, v6, :cond_0

    if-le v1, v6, :cond_0

    .line 346
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4, v3, v2, v1}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 348
    :goto_0
    return-object v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static getTargetDateDay(Landroid/content/Context;)I
    .locals 3
    .parameter "c"

    .prologue
    .line 335
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 336
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "target_date_day"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getTargetDateMonth(Landroid/content/Context;)I
    .locals 3
    .parameter "c"

    .prologue
    .line 330
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 331
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "target_date_month"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getTargetDateString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "c"
    .parameter "format"

    .prologue
    .line 352
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 353
    .local v0, dateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->getTargetDate(Landroid/content/Context;)Ljava/util/Calendar;

    move-result-object v1

    .line 354
    .local v1, targetDate:Ljava/util/Calendar;
    if-eqz v1, :cond_0

    .line 355
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 357
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getTargetDateYear(Landroid/content/Context;)I
    .locals 3
    .parameter "c"

    .prologue
    .line 325
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 326
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "target_date_year"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getUsageCount(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 61
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 62
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "usage_count"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getUserId(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "context"

    .prologue
    .line 125
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 126
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "user_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getUserInputBMR(Landroid/content/Context;)F
    .locals 3
    .parameter "c"

    .prologue
    .line 782
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 783
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "user_input_bmr"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    return v1
.end method

.method public static getUserName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "context"

    .prologue
    .line 113
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 114
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "user_name"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getUserPassword(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "context"

    .prologue
    .line 137
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 138
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "password"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getUserType(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "context"

    .prologue
    .line 149
    const-string v1, "mh_user"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 150
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "user_type"

    const-string v2, "none"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getWaterUnits(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "c"

    .prologue
    .line 442
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 443
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "units_water"

    const-string v2, "glasses"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getWaterUnitsUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 455
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 456
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_water_units"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getWeightInKG(Landroid/content/Context;)F
    .locals 10
    .parameter "c"

    .prologue
    .line 619
    const-string v7, "mh_preferences"

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 621
    .local v2, sp:Landroid/content/SharedPreferences;
    new-instance v3, Ljava/text/DecimalFormatSymbols;

    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 622
    .local v3, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v3, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 623
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v7, "#.##"

    invoke-direct {v0, v7, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 625
    .local v0, df:Ljava/text/DecimalFormat;
    const-string v7, "weight_in_kg"

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v5

    .line 632
    .local v5, weightInKG:F
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 633
    .local v6, weightString:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 634
    const/4 v5, 0x0

    .line 638
    :cond_0
    float-to-double v7, v5

    :try_start_0
    invoke-virtual {v0, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    move v7, v5

    .line 647
    :goto_0
    return v7

    .line 639
    :catch_0
    move-exception v1

    .line 643
    .local v1, e:Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    .line 644
    float-to-int v4, v5

    .line 645
    .local v4, weight:I
    int-to-float v7, v4

    goto :goto_0
.end method

.method public static getWeightInLb(Landroid/content/Context;)F
    .locals 10
    .parameter "c"

    .prologue
    .line 588
    const-string v7, "mh_preferences"

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 590
    .local v2, sp:Landroid/content/SharedPreferences;
    new-instance v3, Ljava/text/DecimalFormatSymbols;

    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 591
    .local v3, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v3, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 592
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v7, "#.##"

    invoke-direct {v0, v7, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 594
    .local v0, df:Ljava/text/DecimalFormat;
    const-string v7, "weight_in_lb"

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v5

    .line 600
    .local v5, weightInLb:F
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 601
    .local v6, weightString:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 602
    const/4 v5, 0x0

    .line 606
    :cond_0
    float-to-double v7, v5

    :try_start_0
    invoke-virtual {v0, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    move v7, v5

    .line 615
    :goto_0
    return v7

    .line 607
    :catch_0
    move-exception v1

    .line 611
    .local v1, e:Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    .line 612
    float-to-int v4, v5

    .line 613
    .local v4, weight:I
    int-to-float v7, v4

    goto :goto_0
.end method

.method public static getWeightUnits(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter "c"

    .prologue
    .line 364
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 365
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "units_weight"

    const-string v2, "lb"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getWeightUnitsUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 377
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 378
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_weight_units"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getWeightUpdatedTime(Landroid/content/Context;)J
    .locals 4
    .parameter "context"

    .prologue
    .line 575
    const-string v1, "mh_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 576
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "lut_weight"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static incrementUsageCount(Landroid/content/Context;)V
    .locals 7
    .parameter "context"

    .prologue
    .line 53
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 54
    .local v1, preferences:Landroid/content/SharedPreferences;
    const-string v4, "usage_count"

    const-wide/16 v5, 0x0

    invoke-interface {v1, v4, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 55
    .local v2, usageCount:J
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 56
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "usage_count"

    const-wide/16 v5, 0x1

    add-long/2addr v5, v2

    invoke-interface {v0, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 57
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 58
    return-void
.end method

.method public static isFirstRunTracked(Landroid/content/Context;)Z
    .locals 3
    .parameter "c"

    .prologue
    const/4 v2, 0x0

    .line 29
    const-string v1, "mh_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 30
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "first_use"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isSetupDone(Landroid/content/Context;)Z
    .locals 3
    .parameter "context"

    .prologue
    const/4 v2, 0x0

    .line 100
    const-string v1, "mh_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 101
    .local v0, preferences:Landroid/content/SharedPreferences;
    const-string v1, "setup_done"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static logout(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 165
    const-string v0, ""

    invoke-static {p0, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserId(Landroid/content/Context;Ljava/lang/String;)V

    .line 166
    const-string v0, ""

    invoke-static {p0, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserName(Landroid/content/Context;Ljava/lang/String;)V

    .line 167
    const-string v0, ""

    invoke-static {p0, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setPassword(Landroid/content/Context;Ljava/lang/String;)V

    .line 169
    const-string v0, "none"

    invoke-static {p0, v0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setUserType(Landroid/content/Context;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public static markFirstRunTracked(Landroid/content/Context;Z)V
    .locals 4
    .parameter "c"
    .parameter "firstRunTracked"

    .prologue
    .line 22
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 23
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 24
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "first_use"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 25
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 26
    return-void
.end method

.method public static saveUUID(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "context"
    .parameter "generatedUUID"

    .prologue
    .line 34
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 35
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 36
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "generated_uuid"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 37
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 38
    return-void
.end method

.method public static setActivityLevel(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "c"
    .parameter "activityLevel"

    .prologue
    .line 740
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 741
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 742
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "activity_level"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 743
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 744
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setActivityLevelUpdatedTime(Landroid/content/Context;)V

    .line 745
    return-void
.end method

.method public static setActivityLevelUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 753
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 754
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 755
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 756
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_activity_level"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 757
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 758
    return-void
.end method

.method public static setAvatarPosition(Landroid/content/Context;I)V
    .locals 4
    .parameter "context"
    .parameter "position"

    .prologue
    .line 218
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 219
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 220
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "avatar_position"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 221
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 222
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setAvatarUpdatedTime(Landroid/content/Context;)V

    .line 223
    return-void
.end method

.method public static setAvatarUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 231
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 232
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 233
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 234
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_avatar"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 235
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 236
    return-void
.end method

.method public static setBMRUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 792
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 793
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 794
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 795
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_bmr"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 796
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 797
    return-void
.end method

.method public static setBirthdayUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 255
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 256
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 257
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 258
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_birthday"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 259
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 260
    return-void
.end method

.method public static setConfigsLastSyncTime(Landroid/content/Context;J)V
    .locals 4
    .parameter "context"
    .parameter "lastSyncTime"

    .prologue
    .line 205
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 206
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 207
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "last_sync_time"

    invoke-interface {v0, v2, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 208
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 209
    return-void
.end method

.method public static setDateOfBirth(Landroid/content/Context;III)V
    .locals 4
    .parameter "c"
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    .prologue
    .line 240
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 241
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 242
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "dob_year"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 243
    const-string v2, "dob_month"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 244
    const-string v2, "dob_day"

    invoke-interface {v0, v2, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 245
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 246
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setBirthdayUpdatedTime(Landroid/content/Context;)V

    .line 247
    return-void
.end method

.method public static setDesiredWeightInKG(Landroid/content/Context;F)V
    .locals 7
    .parameter "c"
    .parameter "weight"

    .prologue
    .line 661
    const-string v3, "mh_preferences"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 662
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 663
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "desired_weight_in_kg"

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 664
    float-to-double v3, p1

    const-wide v5, 0x4001a3112f275febL

    mul-double/2addr v3, v5

    double-to-float v2, v3

    .line 665
    .local v2, weightInLb:F
    const-string v3, "desired_weight_in_lb"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 666
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 667
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGoalsUpdatedTime(Landroid/content/Context;)V

    .line 668
    return-void
.end method

.method public static setDesiredWeightInLb(Landroid/content/Context;F)V
    .locals 7
    .parameter "c"
    .parameter "weight"

    .prologue
    .line 652
    const-string v3, "mh_preferences"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 653
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 654
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "desired_weight_in_lb"

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 655
    float-to-double v3, p1

    const-wide v5, 0x3fdd07a84ab75e51L

    mul-double/2addr v3, v5

    double-to-float v2, v3

    .line 656
    .local v2, weightInKG:F
    const-string v3, "desired_weight_in_kg"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 657
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 658
    return-void
.end method

.method public static setDistanceUnits(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "c"
    .parameter "units"

    .prologue
    .line 421
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 422
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 423
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "units_distance"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 424
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 425
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setDistanceUnitsUpdatedTime(Landroid/content/Context;)V

    .line 426
    return-void
.end method

.method public static setDistanceUnitsUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 434
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 435
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 436
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 437
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_distance_units"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 438
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 439
    return-void
.end method

.method public static setGenderAsFemale(Landroid/content/Context;)V
    .locals 4
    .parameter "context"

    .prologue
    .line 79
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 80
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 81
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "user_gender"

    const-string v3, "Female"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 82
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 83
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGenderUpdatedTime(Landroid/content/Context;)V

    .line 84
    return-void
.end method

.method public static setGenderAsMale(Landroid/content/Context;)V
    .locals 4
    .parameter "context"

    .prologue
    .line 71
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 72
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 73
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "user_gender"

    const-string v3, "Male"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 74
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 75
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGenderUpdatedTime(Landroid/content/Context;)V

    .line 76
    return-void
.end method

.method public static setGenderUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 92
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 93
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 94
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 95
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_gender"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 96
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 97
    return-void
.end method

.method public static setGoal(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "c"
    .parameter "goal"

    .prologue
    .line 766
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 767
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 768
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "weight_goal"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 769
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 770
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGoalsUpdatedTime(Landroid/content/Context;)V

    .line 771
    return-void
.end method

.method public static setGoalsUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 317
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 318
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 319
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 320
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_goals"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 321
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 322
    return-void
.end method

.method public static setHeightInCm(Landroid/content/Context;F)V
    .locals 7
    .parameter "c"
    .parameter "height"

    .prologue
    .line 495
    const-string v3, "mh_preferences"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 496
    .local v2, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 497
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "height_in_cm"

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 498
    float-to-double v3, p1

    const-wide v5, 0x3fd93264c924c3fcL

    mul-double/2addr v3, v5

    double-to-float v1, v3

    .line 499
    .local v1, heightInInches:F
    const-string v3, "height_in_inches"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 500
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 501
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightUpdatedTime(Landroid/content/Context;)V

    .line 502
    return-void
.end method

.method public static setHeightInInches(Landroid/content/Context;F)V
    .locals 11
    .parameter "c"
    .parameter "height"

    .prologue
    .line 468
    const-string v7, "mh_preferences"

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 470
    .local v5, preferences:Landroid/content/SharedPreferences;
    new-instance v6, Ljava/text/DecimalFormatSymbols;

    new-instance v7, Ljava/util/Locale;

    const-string v8, "en"

    const-string v9, "en"

    invoke-direct {v7, v8, v9}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v6, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 471
    .local v6, symbols:Ljava/text/DecimalFormatSymbols;
    const/16 v7, 0x2e

    invoke-virtual {v6, v7}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 472
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v7, "#.##"

    invoke-direct {v0, v7, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 474
    .local v0, df:Ljava/text/DecimalFormat;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 475
    .local v2, editor:Landroid/content/SharedPreferences$Editor;
    const-string v7, "height_in_inches"

    invoke-interface {v2, v7, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 476
    float-to-double v7, p1

    const-wide v9, 0x400451eb851eb852L

    mul-double/2addr v7, v9

    double-to-float v4, v7

    .line 479
    .local v4, heightInCm:F
    float-to-double v7, v4

    :try_start_0
    invoke-virtual {v0, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 489
    :goto_0
    const-string v7, "height_in_cm"

    invoke-interface {v2, v7, v4}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 490
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 491
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightUpdatedTime(Landroid/content/Context;)V

    .line 492
    return-void

    .line 480
    :catch_0
    move-exception v1

    .line 484
    .local v1, e:Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    .line 485
    float-to-int v3, v4

    .line 486
    .local v3, heightCM:I
    int-to-float v4, v3

    goto :goto_0
.end method

.method public static setHeightUnits(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "c"
    .parameter "units"

    .prologue
    .line 395
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 396
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 397
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "units_height"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 398
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 399
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setHeightUnitsUpdatedTime(Landroid/content/Context;)V

    .line 400
    return-void
.end method

.method public static setHeightUnitsUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 408
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 409
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 410
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 411
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_height_units"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 412
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 413
    return-void
.end method

.method public static setHeightUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 510
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 511
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 512
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 513
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_height"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 514
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 515
    return-void
.end method

.method public static setLastLoginPromptTime(Landroid/content/Context;J)V
    .locals 4
    .parameter "context"
    .parameter "lastLoginPromptTime"

    .prologue
    .line 178
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 179
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 180
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "last_login_time"

    invoke-interface {v0, v2, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 181
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 182
    return-void
.end method

.method public static setPassword(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "context"
    .parameter "password"

    .prologue
    .line 142
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 143
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 144
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "password"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 145
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 146
    return-void
.end method

.method public static setSetupDone(Landroid/content/Context;Z)V
    .locals 4
    .parameter "context"
    .parameter "setupDone"

    .prologue
    .line 105
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 106
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 107
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "setup_done"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 109
    return-void
.end method

.method public static setTargetDate(Landroid/content/Context;III)V
    .locals 4
    .parameter "c"
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    .prologue
    .line 302
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 303
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 304
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "target_date_year"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 305
    const-string v2, "target_date_month"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 306
    const-string v2, "target_date_day"

    invoke-interface {v0, v2, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 307
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 308
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setGoalsUpdatedTime(Landroid/content/Context;)V

    .line 309
    return-void
.end method

.method public static setUserId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "context"
    .parameter "userId"

    .prologue
    .line 130
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 131
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 132
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "user_id"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 133
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 134
    return-void
.end method

.method public static setUserInputBMR(Landroid/content/Context;F)V
    .locals 4
    .parameter "c"
    .parameter "bmr"

    .prologue
    .line 774
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 775
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 776
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "user_input_bmr"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 777
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 778
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setBMRUpdatedTime(Landroid/content/Context;)V

    .line 779
    return-void
.end method

.method public static setUserName(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "context"
    .parameter "userName"

    .prologue
    .line 118
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 119
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 120
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "user_name"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 121
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 122
    return-void
.end method

.method public static setUserType(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "context"
    .parameter "userType"

    .prologue
    .line 158
    const-string v2, "mh_user"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 159
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 160
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "user_type"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 161
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 162
    return-void
.end method

.method public static setWaterUnits(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "c"
    .parameter "units"

    .prologue
    .line 447
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 448
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 449
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "units_water"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 450
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 451
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWaterUnitsUpdatedTime(Landroid/content/Context;)V

    .line 452
    return-void
.end method

.method public static setWaterUnitsUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 460
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 461
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 462
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 463
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_water_units"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 464
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 465
    return-void
.end method

.method public static setWeightInKG(Landroid/content/Context;F)V
    .locals 7
    .parameter "c"
    .parameter "weight"

    .prologue
    .line 565
    const-string v3, "mh_preferences"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 566
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 567
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "weight_in_kg"

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 568
    float-to-double v3, p1

    const-wide v5, 0x4001a3112f275febL

    mul-double/2addr v3, v5

    double-to-float v2, v3

    .line 569
    .local v2, weightInLb:F
    const-string v3, "weight_in_lb"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 570
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 571
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWeightUpdatedTime(Landroid/content/Context;)V

    .line 572
    return-void
.end method

.method public static setWeightInLb(Landroid/content/Context;F)V
    .locals 7
    .parameter "c"
    .parameter "weight"

    .prologue
    .line 555
    const-string v3, "mh_preferences"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 556
    .local v1, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 557
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "weight_in_lb"

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 558
    float-to-double v3, p1

    const-wide v5, 0x3fdd07a84ab75e51L

    mul-double/2addr v3, v5

    double-to-float v2, v3

    .line 559
    .local v2, weightInKG:F
    const-string v3, "weight_in_kg"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 560
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 561
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWeightUpdatedTime(Landroid/content/Context;)V

    .line 562
    return-void
.end method

.method public static setWeightUnits(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter "c"
    .parameter "units"

    .prologue
    .line 369
    const-string v2, "mh_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 370
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 371
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "units_weight"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 372
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 373
    invoke-static {p0}, Lorg/medhelp/mydiet/util/PreferenceUtil;->setWeightUnitsUpdatedTime(Landroid/content/Context;)V

    .line 374
    return-void
.end method

.method public static setWeightUnitsUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 382
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 383
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 384
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 385
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_weight_units"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 386
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 387
    return-void
.end method

.method public static setWeightUpdatedTime(Landroid/content/Context;)V
    .locals 6
    .parameter "context"

    .prologue
    .line 580
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 581
    .local v1, now:J
    const-string v4, "mh_preferences"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 582
    .local v3, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 583
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "lut_weight"

    invoke-interface {v0, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 584
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 585
    return-void
.end method
