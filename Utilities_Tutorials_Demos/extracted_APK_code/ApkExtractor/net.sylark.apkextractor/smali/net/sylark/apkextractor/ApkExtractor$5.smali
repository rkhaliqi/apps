.class Lnet/sylark/apkextractor/ApkExtractor$5;
.super Ljava/lang/Thread;
.source "ApkExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/sylark/apkextractor/ApkExtractor;->loadOnBackgrount(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/sylark/apkextractor/ApkExtractor;

.field private final synthetic val$dlg:Landroid/app/Dialog;

.field private final synthetic val$sort:I


# direct methods
.method constructor <init>(Lnet/sylark/apkextractor/ApkExtractor;ILandroid/app/Dialog;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->this$0:Lnet/sylark/apkextractor/ApkExtractor;

    iput p2, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->val$sort:I

    iput-object p3, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->val$dlg:Landroid/app/Dialog;

    .line 126
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lnet/sylark/apkextractor/ApkExtractor$5;)Lnet/sylark/apkextractor/ApkExtractor;
    .locals 1
    .parameter

    .prologue
    .line 126
    iget-object v0, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->this$0:Lnet/sylark/apkextractor/ApkExtractor;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 131
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    const/4 v5, 0x0

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 132
    .local v1, mainIntent:Landroid/content/Intent;
    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    iget-object v4, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->this$0:Lnet/sylark/apkextractor/ApkExtractor;

    invoke-virtual {v4}, Lnet/sylark/apkextractor/ApkExtractor;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v4, v1, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 135
    .local v2, pkgAppsList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, i:I
    :goto_0
    if-gez v0, :cond_0

    .line 147
    iget v4, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->val$sort:I

    packed-switch v4, :pswitch_data_0

    .line 163
    :goto_1
    iget-object v4, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->this$0:Lnet/sylark/apkextractor/ApkExtractor;

    new-instance v5, Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;

    iget-object v6, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->this$0:Lnet/sylark/apkextractor/ApkExtractor;

    invoke-virtual {v6}, Lnet/sylark/apkextractor/ApkExtractor;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f040004

    invoke-direct {v5, v6, v7, v2}, Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    #setter for: Lnet/sylark/apkextractor/ApkExtractor;->adapter:Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;
    invoke-static {v4, v5}, Lnet/sylark/apkextractor/ApkExtractor;->access$3(Lnet/sylark/apkextractor/ApkExtractor;Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;)V

    .line 164
    iget-object v4, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->this$0:Lnet/sylark/apkextractor/ApkExtractor;

    new-instance v5, Lnet/sylark/apkextractor/ApkExtractor$5$1;

    invoke-direct {v5, p0}, Lnet/sylark/apkextractor/ApkExtractor$5$1;-><init>(Lnet/sylark/apkextractor/ApkExtractor$5;)V

    invoke-virtual {v4, v5}, Lnet/sylark/apkextractor/ApkExtractor;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 174
    :try_start_0
    iget-object v4, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->val$dlg:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_2
    return-void

    .line 137
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 139
    .local v3, ri:Landroid/content/pm/ResolveInfo;
    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    .line 135
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 151
    .end local v3           #ri:Landroid/content/pm/ResolveInfo;
    :pswitch_0
    iget-object v4, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->this$0:Lnet/sylark/apkextractor/ApkExtractor;

    #getter for: Lnet/sylark/apkextractor/ApkExtractor;->fileSizeComparator:Ljava/util/Comparator;
    invoke-static {v4}, Lnet/sylark/apkextractor/ApkExtractor;->access$1(Lnet/sylark/apkextractor/ApkExtractor;)Ljava/util/Comparator;

    move-result-object v4

    invoke-static {v2, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    .line 155
    :pswitch_1
    iget-object v4, p0, Lnet/sylark/apkextractor/ApkExtractor$5;->this$0:Lnet/sylark/apkextractor/ApkExtractor;

    #getter for: Lnet/sylark/apkextractor/ApkExtractor;->appNameComparator:Ljava/util/Comparator;
    invoke-static {v4}, Lnet/sylark/apkextractor/ApkExtractor;->access$2(Lnet/sylark/apkextractor/ApkExtractor;)Ljava/util/Comparator;

    move-result-object v4

    invoke-static {v2, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    .line 175
    :catch_0
    move-exception v4

    goto :goto_2

    .line 147
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
