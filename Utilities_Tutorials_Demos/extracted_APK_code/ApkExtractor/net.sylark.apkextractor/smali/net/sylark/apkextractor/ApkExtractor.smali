.class public Lnet/sylark/apkextractor/ApkExtractor;
.super Landroid/app/ListActivity;
.source "ApkExtractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;
    }
.end annotation


# static fields
.field private static final ARCHIVE_APK:I = 0x64

.field private static final DELETE_PACKAGE:I = 0x66

.field private static final SEND_APK:I = 0x65

.field private static final SORT_DATE:I = 0x69

.field private static final SORT_NAME:I = 0x68

.field private static final SORT_SIZE:I = 0x67


# instance fields
.field private adView:Lcom/google/ads/AdView;

.field private adapter:Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;

.field private final appNameComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private clickListner:Landroid/widget/AdapterView$OnItemClickListener;

.field private final fileSizeComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 56
    new-instance v0, Lnet/sylark/apkextractor/ApkExtractor$1;

    invoke-direct {v0, p0}, Lnet/sylark/apkextractor/ApkExtractor$1;-><init>(Lnet/sylark/apkextractor/ApkExtractor;)V

    iput-object v0, p0, Lnet/sylark/apkextractor/ApkExtractor;->fileSizeComparator:Ljava/util/Comparator;

    .line 68
    new-instance v0, Lnet/sylark/apkextractor/ApkExtractor$2;

    invoke-direct {v0, p0}, Lnet/sylark/apkextractor/ApkExtractor$2;-><init>(Lnet/sylark/apkextractor/ApkExtractor;)V

    iput-object v0, p0, Lnet/sylark/apkextractor/ApkExtractor;->appNameComparator:Ljava/util/Comparator;

    .line 307
    new-instance v0, Lnet/sylark/apkextractor/ApkExtractor$3;

    invoke-direct {v0, p0}, Lnet/sylark/apkextractor/ApkExtractor$3;-><init>(Lnet/sylark/apkextractor/ApkExtractor;)V

    iput-object v0, p0, Lnet/sylark/apkextractor/ApkExtractor;->clickListner:Landroid/widget/AdapterView$OnItemClickListener;

    .line 42
    return-void
.end method

.method static synthetic access$0(Lnet/sylark/apkextractor/ApkExtractor;)Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lnet/sylark/apkextractor/ApkExtractor;->adapter:Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;

    return-object v0
.end method

.method static synthetic access$1(Lnet/sylark/apkextractor/ApkExtractor;)Ljava/util/Comparator;
    .locals 1
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lnet/sylark/apkextractor/ApkExtractor;->fileSizeComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic access$2(Lnet/sylark/apkextractor/ApkExtractor;)Ljava/util/Comparator;
    .locals 1
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lnet/sylark/apkextractor/ApkExtractor;->appNameComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic access$3(Lnet/sylark/apkextractor/ApkExtractor;Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lnet/sylark/apkextractor/ApkExtractor;->adapter:Lnet/sylark/apkextractor/ApkExtractor$AppListAdapter;

    return-void
.end method

.method private archive(Landroid/content/pm/ResolveInfo;)Ljava/io/File;
    .locals 14
    .parameter "info"

    .prologue
    .line 251
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v10

    .line 252
    .local v10, state:Ljava/lang/String;
    const-string v11, "mounted"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 253
    invoke-virtual {p0}, Lnet/sylark/apkextractor/ApkExtractor;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f05000b

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {p0, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    .line 254
    const/4 v1, 0x0

    .line 295
    :cond_0
    :goto_0
    return-object v1

    .line 257
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    const-string v12, "AppManager"

    invoke-direct {v3, v11, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 258
    .local v3, folder:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_2

    .line 259
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    .line 262
    :cond_2
    iget-object v11, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v9, v11, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 263
    .local v9, src:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    iget-object v12, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v12, v12, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, ".apk"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v1, v3, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 265
    .local v1, dest:Ljava/io/File;
    const/4 v4, 0x0

    .line 266
    .local v4, in:Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 270
    .local v6, out:Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 271
    .end local v4           #in:Ljava/io/FileInputStream;
    .local v5, in:Ljava/io/FileInputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 273
    .end local v6           #out:Ljava/io/FileOutputStream;
    .local v7, out:Ljava/io/FileOutputStream;
    const/16 v11, 0x1000

    :try_start_2
    new-array v0, v11, [B

    .line 276
    .local v0, buffer:[B
    :goto_1
    const/4 v11, 0x0

    array-length v12, v0

    invoke-virtual {v5, v0, v11, v12}, Ljava/io/FileInputStream;->read([BII)I

    move-result v8

    .local v8, readSize:I
    const/4 v11, -0x1

    if-ne v8, v11, :cond_4

    .line 280
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p0}, Lnet/sylark/apkextractor/ApkExtractor;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f050009

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {p0, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 288
    if-eqz v5, :cond_3

    .line 289
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 290
    :cond_3
    if-eqz v7, :cond_0

    .line 291
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 292
    :catch_0
    move-exception v11

    goto/16 :goto_0

    .line 277
    :cond_4
    const/4 v11, 0x0

    :try_start_4
    invoke-virtual {v7, v0, v11, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 283
    .end local v0           #buffer:[B
    .end local v8           #readSize:I
    :catch_1
    move-exception v2

    move-object v6, v7

    .end local v7           #out:Ljava/io/FileOutputStream;
    .restart local v6       #out:Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 284
    .end local v5           #in:Ljava/io/FileInputStream;
    .local v2, e:Ljava/lang/Exception;
    .restart local v4       #in:Ljava/io/FileInputStream;
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 285
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lnet/sylark/apkextractor/ApkExtractor;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    invoke-virtual {p1, v12}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p0}, Lnet/sylark/apkextractor/ApkExtractor;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f05000a

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {p0, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 288
    if-eqz v4, :cond_5

    .line 289
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 290
    :cond_5
    if-eqz v6, :cond_6

    .line 291
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 295
    :cond_6
    :goto_3
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 286
    .end local v2           #e:Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    .line 288
    :goto_4
    if-eqz v4, :cond_7

    .line 289
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 290
    :cond_7
    if-eqz v6, :cond_8

    .line 291
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 294
    :cond_8
    :goto_5
    throw v11

    .line 292
    :catch_2
    move-exception v12

    goto :goto_5

    .line 286
    .end local v4           #in:Ljava/io/FileInputStream;
    .restart local v5       #in:Ljava/io/FileInputStream;
    :catchall_1
    move-exception v11

    move-object v4, v5

    .end local v5           #in:Ljava/io/FileInputStream;
    .restart local v4       #in:Ljava/io/FileInputStream;
    goto :goto_4

    .end local v4           #in:Ljava/io/FileInputStream;
    .end local v6           #out:Ljava/io/FileOutputStream;
    .restart local v5       #in:Ljava/io/FileInputStream;
    .restart local v7       #out:Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v11

    move-object v6, v7

    .end local v7           #out:Ljava/io/FileOutputStream;
    .restart local v6       #out:Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5           #in:Ljava/io/FileInputStream;
    .restart local v4       #in:Ljava/io/FileInputStream;
    goto :goto_4

    .line 292
    .restart local v2       #e:Ljava/lang/Exception;
    :catch_3
    move-exception v11

    goto :goto_3

    .line 283
    .end local v2           #e:Ljava/lang/Exception;
    :catch_4
    move-exception v2

    goto :goto_2

    .end local v4           #in:Ljava/io/FileInputStream;
    .restart local v5       #in:Ljava/io/FileInputStream;
    :catch_5
    move-exception v2

    move-object v4, v5

    .end local v5           #in:Ljava/io/FileInputStream;
    .restart local v4       #in:Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method private deletePackage(Landroid/content/pm/ResolveInfo;)V
    .locals 5
    .parameter "info"

    .prologue
    .line 300
    const-string v2, "package"

    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 301
    .local v1, uri:Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.DELETE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 302
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 304
    invoke-virtual {p0, v0}, Lnet/sylark/apkextractor/ApkExtractor;->startActivity(Landroid/content/Intent;)V

    .line 305
    return-void
.end method

.method private loadOnBackgrount(I)V
    .locals 2
    .parameter "sort"

    .prologue
    .line 114
    invoke-direct {p0}, Lnet/sylark/apkextractor/ApkExtractor;->makeWaitDlg()Landroid/app/Dialog;

    move-result-object v0

    .line 115
    .local v0, dlg:Landroid/app/Dialog;
    new-instance v1, Lnet/sylark/apkextractor/ApkExtractor$4;

    invoke-direct {v1, p0, v0}, Lnet/sylark/apkextractor/ApkExtractor$4;-><init>(Lnet/sylark/apkextractor/ApkExtractor;Landroid/app/Dialog;)V

    invoke-virtual {p0, v1}, Lnet/sylark/apkextractor/ApkExtractor;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 126
    new-instance v1, Lnet/sylark/apkextractor/ApkExtractor$5;

    invoke-direct {v1, p0, p1, v0}, Lnet/sylark/apkextractor/ApkExtractor$5;-><init>(Lnet/sylark/apkextractor/ApkExtractor;ILandroid/app/Dialog;)V

    .line 178
    invoke-virtual {v1}, Lnet/sylark/apkextractor/ApkExtractor$5;->start()V

    .line 180
    return-void
.end method

.method private makeWaitDlg()Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 369
    new-instance v0, Landroid/app/Dialog;

    const/high16 v1, 0x7f06

    invoke-direct {v0, p0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 370
    .local v0, dialog:Landroid/app/Dialog;
    new-instance v1, Landroid/widget/ProgressBar;

    invoke-direct {v1, p0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Dialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 372
    return-object v0
.end method

.method public static readableFileSize(J)Ljava/lang/String;
    .locals 10
    .parameter "size"

    .prologue
    const-wide/high16 v8, 0x4090

    .line 360
    const-wide/16 v2, 0x0

    cmp-long v2, p0, v2

    if-gtz v2, :cond_0

    .line 361
    const-string v2, "0"

    .line 364
    :goto_0
    return-object v2

    .line 362
    :cond_0
    const/4 v2, 0x5

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "B"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "KB"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "MB"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "GB"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "TB"

    aput-object v3, v1, v2

    .line 363
    .local v1, units:[Ljava/lang/String;
    long-to-double v2, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    invoke-static {v8, v9}, Ljava/lang/Math;->log10(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-int v0, v2

    .line 364
    .local v0, digitGroups:I
    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "#,##0.#"

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    long-to-double v4, p0

    int-to-double v6, v0

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private sendEmail(Ljava/io/File;Landroid/content/pm/ResolveInfo;)V
    .locals 7
    .parameter "file"
    .parameter "info"

    .prologue
    .line 237
    invoke-virtual {p0}, Lnet/sylark/apkextractor/ApkExtractor;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 238
    .local v1, pm:Landroid/content/pm/PackageManager;
    invoke-virtual {p2, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 240
    .local v0, labelSeq:Ljava/lang/CharSequence;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 241
    .local v2, sendIntent:Landroid/content/Intent;
    const-string v3, "android.intent.extra.SUBJECT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lnet/sylark/apkextractor/ApkExtractor;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050006

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v3, "android.intent.extra.TEXT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lnet/sylark/apkextractor/ApkExtractor;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050008

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    const-string v3, "text/plain"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    const-string v3, "android.intent.extra.STREAM"

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 246
    const-string v3, "Email:"

    invoke-static {v2, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lnet/sylark/apkextractor/ApkExtractor;->startActivity(Landroid/content/Intent;)V

    .line 247
    return-void
.end method


# virtual methods
.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .parameter "item"

    .prologue
    .line 212
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v2

    check-cast v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 213
    .local v2, info:Landroid/widget/AdapterView$AdapterContextMenuInfo;,"Landroid/widget/AdapterView$AdapterContextMenuInfo;"
    invoke-virtual {p0}, Lnet/sylark/apkextractor/ApkExtractor;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    iget v4, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v3, v4}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 215
    .local v0, appInfo:Landroid/content/pm/ResolveInfo;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 232
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    return v3

    .line 218
    :pswitch_0
    invoke-direct {p0, v0}, Lnet/sylark/apkextractor/ApkExtractor;->archive(Landroid/content/pm/ResolveInfo;)Ljava/io/File;

    goto :goto_0

    .line 221
    :pswitch_1
    invoke-direct {p0, v0}, Lnet/sylark/apkextractor/ApkExtractor;->archive(Landroid/content/pm/ResolveInfo;)Ljava/io/File;

    move-result-object v1

    .line 222
    .local v1, f:Ljava/io/File;
    if-eqz v1, :cond_0

    .line 223
    invoke-direct {p0, v1, v0}, Lnet/sylark/apkextractor/ApkExtractor;->sendEmail(Ljava/io/File;Landroid/content/pm/ResolveInfo;)V

    goto :goto_0

    .line 227
    .end local v1           #f:Ljava/io/File;
    :pswitch_2
    invoke-direct {p0, v0}, Lnet/sylark/apkextractor/ApkExtractor;->deletePackage(Landroid/content/pm/ResolveInfo;)V

    goto :goto_0

    .line 215
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    const v2, 0x7f030001

    invoke-virtual {p0, v2}, Lnet/sylark/apkextractor/ApkExtractor;->setContentView(I)V

    .line 88
    const v2, 0x7f040007

    invoke-virtual {p0, v2}, Lnet/sylark/apkextractor/ApkExtractor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/ads/AdView;

    iput-object v2, p0, Lnet/sylark/apkextractor/ApkExtractor;->adView:Lcom/google/ads/AdView;

    .line 89
    new-instance v1, Lcom/google/ads/AdRequest;

    invoke-direct {v1}, Lcom/google/ads/AdRequest;-><init>()V

    .line 90
    .local v1, re:Lcom/google/ads/AdRequest;
    iget-object v2, p0, Lnet/sylark/apkextractor/ApkExtractor;->adView:Lcom/google/ads/AdView;

    invoke-virtual {v2, v1}, Lcom/google/ads/AdView;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 92
    invoke-virtual {p0}, Lnet/sylark/apkextractor/ApkExtractor;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 93
    .local v0, list:Landroid/widget/ListView;
    iget-object v2, p0, Lnet/sylark/apkextractor/ApkExtractor;->clickListner:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 95
    invoke-virtual {p0, v0}, Lnet/sylark/apkextractor/ApkExtractor;->registerForContextMenu(Landroid/view/View;)V

    .line 96
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 4
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    const/4 v3, 0x0

    .line 202
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 204
    const/16 v0, 0x64

    const v1, 0x7f050005

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 205
    const/16 v0, 0x65

    const/4 v1, 0x1

    const v2, 0x7f050006

    invoke-interface {p1, v3, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 206
    const/16 v0, 0x66

    const/4 v1, 0x2

    const v2, 0x7f050007

    invoke-interface {p1, v3, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 207
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .parameter "menu"

    .prologue
    const/4 v3, 0x0

    .line 185
    const/16 v0, 0x68

    const v1, 0x7f050002

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 186
    const/16 v0, 0x67

    const/4 v1, 0x1

    const v2, 0x7f050003

    invoke-interface {p1, v3, v0, v1, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 187
    const/16 v0, 0x69

    const/4 v1, 0x2

    const v2, 0x7f050004

    invoke-interface {p1, v3, v0, v1, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 189
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lnet/sylark/apkextractor/ApkExtractor;->adView:Lcom/google/ads/AdView;

    invoke-virtual {v0}, Lcom/google/ads/AdView;->destroy()V

    .line 102
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 103
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 195
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-direct {p0, v0}, Lnet/sylark/apkextractor/ApkExtractor;->loadOnBackgrount(I)V

    .line 197
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/ListActivity;->onStart()V

    .line 109
    const/16 v0, 0x69

    invoke-direct {p0, v0}, Lnet/sylark/apkextractor/ApkExtractor;->loadOnBackgrount(I)V

    .line 110
    return-void
.end method
