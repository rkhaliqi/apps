package com.main.quietSalahLite;

import java.util.Calendar;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import android.media.AudioManager;
import android.os.Bundle;

public class AlarmReceiver extends BroadcastReceiver {

	boolean isDevMode = false;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			Bundle bundle = intent.getExtras();
			String message = bundle.getString("alarm_message");

			AudioManager audMangr;
			audMangr = (AudioManager) context
					.getSystemService(Context.AUDIO_SERVICE);

			String[] messageParts = message.split(":");

			Calendar cal = Calendar.getInstance(); 

			boolean doAlarmChange = false;
			boolean isOneTimeAlarm = false;
			// for One-Time Alarm
			if ("x".equalsIgnoreCase(String.valueOf(messageParts[1].charAt(0)))) {
				doAlarmChange = true;
				isOneTimeAlarm = true;
			}

			// for Recurring Alarm
			else {
				int day = cal.get(Calendar.DAY_OF_WEEK); // sunday-1,
															// monday-2,tuesday-3,wed-4,thur-5,fri-6,sat-7
				if (day == 1 && "1".equalsIgnoreCase(String.valueOf(messageParts[1].charAt(6)))) {
					doAlarmChange = true;
				} else if (day == 2 && "1".equalsIgnoreCase(String.valueOf(messageParts[1].charAt(0)))) {
					doAlarmChange = true;
				} else if (day == 3 && "1".equalsIgnoreCase(String.valueOf(messageParts[1].charAt(1)))) {
					doAlarmChange = true;
				} else if (day == 4 && "1".equalsIgnoreCase(String.valueOf(messageParts[1].charAt(2)))) {
					doAlarmChange = true;
				} else if (day == 5 && "1".equalsIgnoreCase(String.valueOf(messageParts[1].charAt(3)))) {
					doAlarmChange = true;
				} else if (day == 6 && "1".equalsIgnoreCase(String.valueOf(messageParts[1].charAt(4)))) {
					doAlarmChange = true;
				} else if (day == 7 && "1".equalsIgnoreCase(String.valueOf(messageParts[1].charAt(5)))) {
					doAlarmChange = true;
				}
			}

			// For Normal mode
			// audMangr.setRingerMode(AudioManager.RINGER_MODE_NORMAL);

			// For Silent mode
			// audMangr.setRingerMode(AudioManager.RINGER_MODE_SILENT);

			// For Vibrate mode
			// audMangr.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);

			// int prevRingerMode = audMangr.getRingerMode();
			// audMangr.setRingerMode(AudioManager.VIBRATE_SETTING_ON);

			if (messageParts[0].equalsIgnoreCase("Start")) {
				if (doAlarmChange) {
					updatePreviousMode(Integer.parseInt(messageParts[3]), audMangr.getRingerMode(), context);
					if ("0".equalsIgnoreCase(messageParts[2])) {
						audMangr.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
					} else if ("1".equalsIgnoreCase(messageParts[2])) {
						audMangr.setRingerMode(AudioManager.RINGER_MODE_SILENT);
					} else if ("2".equalsIgnoreCase(messageParts[2])) {
						audMangr.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
					}
				}
			} else if (messageParts[0].equalsIgnoreCase("End")) {
				if (doAlarmChange) {
					String previousMode = getPreviousMode(Integer.parseInt(messageParts[2]), context);
					if ("0".equalsIgnoreCase(previousMode)) {
						audMangr.setRingerMode(AudioManager.RINGER_MODE_SILENT);
					} else if ("1".equalsIgnoreCase(previousMode)) {
						audMangr.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
					} else if ("2".equalsIgnoreCase(previousMode)) {
						audMangr.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
					}
					//  if One-time alarm then disable it after it�s end time
					if (isOneTimeAlarm){
						Log.v("TAG","in one time alarm disable");
						disableOneTimeSchedule(Integer.parseInt(messageParts[2]), context);
						Log.v("TAG","in one time alarm disable done");
					}
				}
				

			} else {
				Toast.makeText(context, "No Match", Toast.LENGTH_SHORT).show();
			}
			if(isDevMode){
				Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
			Toast.makeText(
					context,
					"There was an error somewhere, but we still received an alarm in AlarmReceiver",
					Toast.LENGTH_SHORT).show();
			e.printStackTrace();

		}
	}

	public void disableOneTimeSchedule(int scheduleId, Context context) {
		ContentValues values = new ContentValues();
		values.put(SchedulerDBOpenHelper.IS_ENABLED, false);
		String querySql = "_id = " + scheduleId;
		SQLiteDatabase db = null;
		try {
			SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(context, null, null, 0);
			db = dbHelper.getWritableDatabase();
			db.update(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, values, querySql, null);
			db.close();
		} catch (Exception e) {
			db.close();
		}
	}

	public void updatePreviousMode(int schedulerId, int previousMode, Context context) {
		String querySql = "_id = " + schedulerId;
		ContentValues content = new ContentValues();
		SQLiteDatabase db = null;
		try {
			content.put(SchedulerDBOpenHelper.PREVIOUS_MODE, String.valueOf(previousMode));
			SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(context, null, null, 0);
			db = dbHelper.getWritableDatabase();
			db.update(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, content,querySql, null);
			db.close();
		} catch (Exception e) {
			db.close();
		}
	}
	
	public String getPreviousMode(int schedulerId, Context context) {
		String previousMode = null;
		String querySql = "_id = " + schedulerId;
		SQLiteDatabase db = null;
		Cursor results = null;
		try {
			SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(context, null, null, 0);
			db = dbHelper.getWritableDatabase();
			results = db.query(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME,
					null, querySql, null, null, null, null);
			results.moveToFirst();
			previousMode = results.getString(results.getColumnIndex(SchedulerDBOpenHelper.PREVIOUS_MODE));
			results.close();
			db.close();
		} catch (Exception e) {
			results.close();
			db.close();
		}
		
		return previousMode;
		
	}

}
