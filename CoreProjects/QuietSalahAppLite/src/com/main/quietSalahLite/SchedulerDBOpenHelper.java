package com.main.quietSalahLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SchedulerDBOpenHelper extends SQLiteOpenHelper{
	
	public static final String TAG = "SchedulerAppActivity";
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "quitesalah.db";
	public static final String SCHEDULE_TABLE_NAME ="schedules";
	public static final String SCH_COLUMN_ID = "_id";
	public static final String SCHEDULE_NAME = "scheduleName";
	public static final String MODE = "mode";
	public static final String START_TIME_MIN = "startTimeMin";
	public static final String START_TIME_HR = "startTimeHr";
	public static final String END_TIME_MIN = "endTimeMin";
	public static final String END_TIME_HR = "endTimeHr";
	public static final String FREQUENCY = "frequency";
	public static final String REPEAT_DAYS = "repeatDays";
	public static final String IS_ENABLED ="isEnabled";
	public static final String PREVIOUS_RING_TABLE_NAME ="previousRingMode";
	public static final String PREV_COLUMN_ID = "previousId";
	public static final String PREVIOUS_MODE = "previousMode";
	public static final String ALL_ENABLED = "isAllEnabled";
	
	private static final String SCHEDULE_TABLE_CREATE = "CREATE TABLE " + SCHEDULE_TABLE_NAME + " (" + 
			SCH_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
			SCHEDULE_NAME + " TEXT not null, " +
			MODE + " TEXT not null, " +
			START_TIME_HR + " INTEGER not null, " +
			START_TIME_MIN + " INTEGER not null, " +
			END_TIME_HR + " INTEGER not null, " +
			END_TIME_MIN + " INTEGER not null, " +
			FREQUENCY + " TEXT not null, " +
			REPEAT_DAYS + " TEXT, " +
			IS_ENABLED + " TEXT not null," +
			PREVIOUS_MODE + " TEXT" +
					");";


	public SchedulerDBOpenHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SCHEDULE_TABLE_CREATE);
		
		// create 6 entries for 5 salah + Jumuah
		insertSalah(db, "Fajr", "1", 5, 30, 6, 0, "1", "1111111");
		insertSalah(db, "Zuhr", "1", 13, 0, 13, 30, "1", "1111011");
		insertSalah(db, "Asr", "1", 17, 0, 17, 30, "1", "1111111");
		insertSalah(db, "Maghrib", "1", 19, 0, 19, 30, "1", "1111111");
		insertSalah(db, "Isha", "1", 20, 30, 21, 0, "1", "1111111");
		insertSalah(db, "Jumuah", "1", 13, 0, 13, 45, "1", "0000100");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS "+ SCHEDULE_TABLE_NAME);
        onCreate(db);
	}
	
	public void insertSalah(SQLiteDatabase db, String salahName, String mode, int startHour, int startMin, 
			int endHour, int endMin, String frequency, String repeatDays) {
		ContentValues content = new ContentValues();
		content.put(SchedulerDBOpenHelper.SCHEDULE_NAME, salahName);
		content.put(SchedulerDBOpenHelper.MODE, mode);
		content.put(SchedulerDBOpenHelper.START_TIME_HR, startHour);
		content.put(SchedulerDBOpenHelper.START_TIME_MIN, startMin);
		content.put(SchedulerDBOpenHelper.END_TIME_HR, endHour);
		content.put(SchedulerDBOpenHelper.END_TIME_MIN, endMin);
		content.put(SchedulerDBOpenHelper.FREQUENCY, frequency);
		content.put(SchedulerDBOpenHelper.REPEAT_DAYS, repeatDays);
		content.put(SchedulerDBOpenHelper.IS_ENABLED, false);
		long id = db.insert(SCHEDULE_TABLE_NAME, null, content);
	}
	

}
