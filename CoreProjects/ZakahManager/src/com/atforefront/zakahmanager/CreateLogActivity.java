package com.atforefront.zakahmanager;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class CreateLogActivity extends Activity {
	
	public static final String TAG = "CreateLogActivity";
	static final int DATE_DIALOG_ID = 999;
	
	boolean isEditMode;
	int editingLogId;
	private EditText nameText;
	private EditText amountText;
	private EditText descriptionText;
	private Button logDateButton;
	private Button saveButton;
	private Button cancelButton;
	private boolean isSuccessfulInsert;
	
	private int year;
	private int month;
	private int day;
	
	public ImageButton homeButton;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	Log.v(TAG, "Activity State: onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_log);
        
        Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String value = extras.getString("itemSelected");
			if (!value.equals("0")) {
				isEditMode = true;
				editingLogId = Integer.parseInt(value);
			}
		}
		
		isSuccessfulInsert = true;
		
		nameText = (EditText) findViewById(R.id.nameText);
		amountText = (EditText) findViewById(R.id.amountText);
		descriptionText = (EditText) findViewById(R.id.descriptionText);
		logDateButton = (Button) findViewById(R.id.logDateButton);
		
		saveButton = (Button) findViewById(R.id.saveButton);
		cancelButton = (Button) findViewById(R.id.cancelButton);
		
		homeButton = (ImageButton) findViewById(R.id.btn_go_home);
		
		//System.out.println("DATE IS ********* "+ logDateButton.getMonth() + " " + logDatePicker.getDayOfMonth() + " "+ logDatePicker.getYear());
		
		addListenerOnButton();
		
		final Calendar c = Calendar.getInstance();
		if (isEditMode) {
			getExistingSchedule(editingLogId);
		} else {
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DAY_OF_MONTH);
		}
		
		saveButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (validateFields()) {
					if(isEditMode){
						updateLog(editingLogId);
					}
					else {
						insertNewLog();
					}
					
					if (isSuccessfulInsert){
						launchLogListScreen();
					} else {
						Toast.makeText(v.getContext(), "Error occured while saving", Toast.LENGTH_SHORT).show();
					}
				}	
				
			}
		});
		
		cancelButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				launchLogListScreen();
			}
		});
		
		homeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(v.getContext(),
						ZakahManagerActivity.class);
				startActivity(i);
			}

		});
	}
    
    @Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
		   // set date picker as current date
		   return new DatePickerDialog(this, datePickerListener, 
                         year, month, day);
		}
		return null;
	}
 
	private DatePickerDialog.OnDateSetListener datePickerListener 
                = new DatePickerDialog.OnDateSetListener() {
 
		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;
 
			// set selected date into textview
			logDateButton.setText(new StringBuilder().append(month + 1)
			   .append("-").append(day).append("-").append(year)
			   .append(" "));
 
		}
	};
    
    private boolean validateFields() {
		if (nameText.getText().toString().trim().length() < 1) {
			Toast.makeText(this.getApplicationContext(), "Name should not be empty", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!Utility.validateNumberFields(amountText.getText().toString())) {
			Toast.makeText(this.getApplicationContext(), "Amount should be a positive number", Toast.LENGTH_SHORT).show();
			return false;
		}
		if ("select date".equals(logDateButton.getText().toString().toLowerCase().trim())) {
			Toast.makeText(this.getApplicationContext(), "You must select a date", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
    
    public void insertNewLog(){
    	isSuccessfulInsert = true;
    	SQLiteDatabase db = null;
    	long logId;
    	ContentValues logContent = new ContentValues();
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_NAME, nameText.getText().toString().trim());
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_AMOUNT, amountText.getText().toString());
    	String dateString = logDateButton.getText().toString();
    	String[] dateParts = dateString.split("-");
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_MONTH, dateParts[0]);
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_DAY, dateParts[1]);
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_YEAR, dateParts[2]);
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_DESC, descriptionText.getText().toString());
    	try {
			ZakahManagerDBOpenHelper dbHelper = new ZakahManagerDBOpenHelper(this.getApplicationContext(), null, null, 0);
			db = dbHelper.getWritableDatabase();
			logId = db.insert(ZakahManagerDBOpenHelper.Z_LOG_TABLE, null, logContent);
			db.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			isSuccessfulInsert = false;
			db.close();
			e.printStackTrace();
		}
    	
    }
    
    public void updateLog(int logId){
    	String querySql = "_id = " + logId;
    	isSuccessfulInsert = true;
    	SQLiteDatabase db = null;
    	ContentValues logContent = new ContentValues();
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_NAME, nameText.getText().toString().trim());
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_AMOUNT, amountText.getText().toString());
    	String dateString = logDateButton.getText().toString();
    	String[] dateParts = dateString.split("-");
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_MONTH, dateParts[0]);
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_DAY, dateParts[1]);
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_YEAR, dateParts[2]);
    	logContent.put(ZakahManagerDBOpenHelper.Z_LOG_DESC, descriptionText.getText().toString());
    	try {
			ZakahManagerDBOpenHelper dbHelper = new ZakahManagerDBOpenHelper(this.getApplicationContext(), null, null, 0);
			db = dbHelper.getWritableDatabase();
			db.update(
					ZakahManagerDBOpenHelper.Z_LOG_TABLE, logContent,
					querySql, null);
			db.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			isSuccessfulInsert = false;
			db.close();
			e.printStackTrace();
		}
    }
    
    public void getExistingSchedule(int logId){
    	String querySql = "_id = " + logId;
		SQLiteDatabase db = null;
		Cursor results = null;
		try {
			ZakahManagerDBOpenHelper dbHelper = new ZakahManagerDBOpenHelper(
					this.getApplicationContext(), null, null, 0);
			db  = dbHelper.getWritableDatabase();
			results = db.query(ZakahManagerDBOpenHelper.Z_LOG_TABLE,
					null, querySql, null, null, null, null);
		
		
			if (results != null) {
				results.moveToFirst();
				
				int logMonth = results.getInt(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_MONTH));
				nameText.setText(results.getString(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_NAME)));
				amountText.setText(results.getString(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_AMOUNT)));
				descriptionText.setText(results.getString(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_DESC)));
				String formattedDate = logMonth + "-" + results.getInt(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_DAY)) 
						+ "-" + results.getInt(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_YEAR));
				logDateButton.setText(formattedDate);
				year = results.getInt(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_YEAR));
				month = logMonth - 1;
				day = results.getInt(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_DAY));
				/*logDatePicker.updateDate(results.getInt(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_YEAR)), 
											month - 1, 
											results.getInt(results.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_DAY)));
			*/
			}
	
			results.close();
			db.close();
		} catch (Exception e) {
			results.close();
			db.close();
		}
    }
 
    
    
    public void addListenerOnButton() {
    	 
    	logDateButton = (Button) findViewById(R.id.logDateButton);
 
    	logDateButton.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View v) {
 
				showDialog(DATE_DIALOG_ID);
 
			}
 
		});
 
	}
    
    protected void launchLogListScreen() {
		Intent i = new Intent(this, LogListActivity.class);
		startActivity(i);
	}
    
    @Override
	public boolean dispatchTouchEvent(MotionEvent event) {

	    View v = getCurrentFocus();
	    boolean ret = super.dispatchTouchEvent(event);

	    if (v instanceof EditText) {
	        View w = getCurrentFocus();
	        int scrcoords[] = new int[2];
	        w.getLocationOnScreen(scrcoords);
	        float x = event.getRawX() + w.getLeft() - scrcoords[0];
	        float y = event.getRawY() + w.getTop() - scrcoords[1];

	        Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
	        if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) { 

	            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
	        }
	    }
	return ret;
	}

}
