package com.atforefront.zakahmanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class ZakahManagerDBOpenHelper extends SQLiteOpenHelper {
	
	public static final String TAG = "ZakahManagerDBOpenHelper";
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "zakahmanager.db";
		
	public static final String ZAKAH_CALCULATOR_TABLE = "zakahcalculation";
	public static final String Z_CALC_ID = "_id";
	// value of Z_CALC_NISAB will be one of these GOLD/SILVER
	public static final String Z_C_NISAB = "zCalcNisab";
	public static final String Z_C_A_GOLD_WEIGHT = "zcAGoldWeight";
	public static final String Z_C_A_SILVER_WEIGHT = "zcASilverWeight";
	public static final String Z_C_A_REAL_ESTATE = "zcARealEstate";
	public static final String Z_C_CASH_IN_HAND = "zcCashInHand";
	public static final String Z_C_CASH_IN_BANK = "zcCashInBank";
	public static final String Z_C_CASH_IN_LOANS_GIVEN = "zcCashLoansGiven";
	public static final String Z_C_TRADE_STOCK_VALUE = "zcTradeStockVal";
	public static final String Z_C_TRADE_INVESTMENTS = "zcTradeInvestments";
	public static final String Z_C_LIAB_BORROWED_MONEY = "zcLiabBorrowedMoney";
	public static final String Z_C_LIAB_WAGES_TO_EMPLOYEES = "zcLiabWagesToEmp";
	public static final String Z_C_LIAB_EXPENSES = "zcLiabExpneses";
	
	
	public static final String Z_LOG_TABLE = "zakahlog";
	public static final String Z_LOG_ID = "_id";
	public static final String Z_LOG_NAME = "logName";
	public static final String Z_LOG_DAY = "logDay";
	public static final String Z_LOG_MONTH = "logMonth";
	public static final String Z_LOG_YEAR = "logYear";
	public static final String Z_LOG_AMOUNT = "logAmount";
	public static final String Z_LOG_DESC = "logDesc";
	
	private static final String ZAKAH_CALCULATOR_TABLE_CREATE = "CREATE TABLE " + ZAKAH_CALCULATOR_TABLE + " (" + 
			Z_CALC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
			Z_C_NISAB + " TEXT not null, " +
			Z_C_A_GOLD_WEIGHT + " TEXT, " +
			Z_C_A_SILVER_WEIGHT + " TEXT, " +
			Z_C_A_REAL_ESTATE + " TEXT, " +
			Z_C_CASH_IN_HAND + " TEXT, " +
			Z_C_CASH_IN_BANK + " TEXT, " +
			Z_C_CASH_IN_LOANS_GIVEN + " TEXT, " +
			Z_C_TRADE_STOCK_VALUE + " TEXT, " +
			Z_C_TRADE_INVESTMENTS + " TEXT," +
			Z_C_LIAB_BORROWED_MONEY + " TEXT," +
			Z_C_LIAB_WAGES_TO_EMPLOYEES + " TEXT," +
			Z_C_LIAB_EXPENSES + " TEXT" +
					");";
	
	private static final String ZAKAH_LOG_TABLE_CREATE = "CREATE TABLE " + Z_LOG_TABLE + " (" + 
			Z_LOG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
			Z_LOG_NAME + " TEXT not null, " +
			Z_LOG_DAY + " INTEGER," +
			Z_LOG_MONTH + " INTEGER," +
			Z_LOG_YEAR + " INTEGER," +
			Z_LOG_AMOUNT + " TEXT," +
			Z_LOG_DESC + " TEXT" +
					");";
	
	public ZakahManagerDBOpenHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(ZAKAH_CALCULATOR_TABLE_CREATE);
		db.execSQL(ZAKAH_LOG_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
