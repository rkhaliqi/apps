package com.atforefront.zakahmanager;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ZakahManagerActivity extends Activity {
    
	private Button calculatorButton;
	private Button logButton;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        calculatorButton = (Button) findViewById(R.id.btn_calculator);
        logButton = (Button) findViewById(R.id.btn_log);
        
        ZakahManagerDBOpenHelper dbHelper = new ZakahManagerDBOpenHelper(this.getApplicationContext(), null, null, 0);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.close();
        
        // Calculator Button's OnClickListener
        calculatorButton.setOnClickListener(new View.OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				launchCalculatorScreen();
 			}
 		});
     		
        // Log Button's OnClickListener
        logButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				launchLogListScreen();
			}
		});
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.layout.menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        	case R.id.about_us:
        		/*
        		 * Code for About Us pop-up screen goes here 
        		 */
        		return true;
        	case R.id.exitApp:
        		finish();
        		return true;
        	case R.id.clear_calculations_log:
        		/*
        		 * Code for Clear Calculations and Logs goes here 
        		 * Need to truncate all DB tables
        		 */
        		return true;
            case R.id.send_email:
//            	Intent i = new Intent(Intent.ACTION_SEND);
//            	i.setType("text/plain");
//            	i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"chowdhurysarwar@gmail.com"});
//            	i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
//            	i.putExtra(Intent.EXTRA_TEXT   , "body of email");
//            	try {
//            	    startActivity(Intent.createChooser(i, "Send mail..."));
//            	} catch (android.content.ActivityNotFoundException ex) {
//            	    Toast.makeText(ZakahManagerActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
//            	}
            	Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
            	intent.setType("text/plain");
            	intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
            	intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
            	intent.setData(Uri.parse("mailto:chowdhurysarwar@gmail.com")); // or just "mailto:" for blank
            	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
            	startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    protected void launchCalculatorScreen() {
		Intent i = new Intent(this, CalculatorActivity.class);
		startActivity(i);
	}
    
    protected void launchLogListScreen() {
		Intent i = new Intent(this, LogListActivity.class);
		startActivity(i);
	}
}