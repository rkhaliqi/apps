package com.atforefront.zakahmanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class LogListActivity extends Activity {

	public static final String TAG = "LogListActivity";
	public Button addPaymentButton;
	private ListView paymentLogList;
	public ImageButton homeButton;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	Log.v(TAG, "Activity State: onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_all_payment);
        
        addPaymentButton = (Button) findViewById(R.id.addLogButton);
        paymentLogList = (ListView) findViewById(R.id.paymentLogList);
        homeButton = (ImageButton) findViewById(R.id.btn_home);
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.payment_entry, R.id.paymentEntryText);
//        dataAdapter.add("Payment1");
//        dataAdapter.add("Payment2");
//        dataAdapter.add("Payment3");
//        dataAdapter.add("Payment4");
//        dataAdapter.add("Payment5");
//        paymentLogList.setAdapter(dataAdapter);
        ZakahManagerDBOpenHelper dbHelper = new ZakahManagerDBOpenHelper(this.getApplicationContext(), null, null, 0);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
		paymentLogList.setAdapter(new CustomCursorAdapter(this, paymentLogList, getLogs(db)));
		db.close();
		
        addPaymentButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				launchCreateLogScreen();
			}
		});
        
        homeButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
					Intent i = new Intent(v.getContext(), ZakahManagerActivity.class);
					startActivity(i);
			}

			
		});
        
    }
    
    private Cursor getLogs(SQLiteDatabase db)
    {
    	Cursor results = null;
    	try {
			results = db.query(ZakahManagerDBOpenHelper.Z_LOG_TABLE, null, null, null, null, null, null);
			results.moveToFirst();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}

        return results;
    }
    
    protected void launchCreateLogScreen() {
		Intent i = new Intent(this, CreateLogActivity.class);
		startActivity(i);
	}
    
    private class CustomCursorAdapter extends CursorAdapter { 
      	 
	    protected ListView mListView;
	    protected Activity listActivity;
	
	    protected class RowViewHolder {
	        public TextView logNameTextbox;
	        public TextView paymentAmountAndDateTextBox;
	        ImageButton deletePaymentButton;
	    }
	
	    public CustomCursorAdapter(Activity activity, ListView list, Cursor cursor) {
	        super(activity, cursor);
	        mListView = list;
	        this.listActivity = activity;
	    }
	
	    @Override
	    public void bindView(View view, Context context, Cursor cursor) {
	        String scheduleName = cursor.getString(cursor.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_NAME));
	        String paymentAmount = cursor.getString(cursor.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_AMOUNT));
	        String paymentDate = cursor.getString(cursor.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_MONTH)) + "/" +
	        						cursor.getString(cursor.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_DAY)) + "/" +
	        						cursor.getString(cursor.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_YEAR));
	        String paymentAmountAndDateText = "Amount: " + paymentAmount + " Date: " + paymentDate;
	        
	        TextView logNameTextbox = (TextView) view.findViewById(R.id.paymentEntryText);
	        if (logNameTextbox != null) {
	        	String formattedLogName = scheduleName;
	        	if (formattedLogName.length() > 21) 
	        	{
	        		formattedLogName = formattedLogName.substring(0, 20) + "...";
	        	}
	        	
	        	logNameTextbox.setText(formattedLogName);
	        }
	        
	        TextView paymentAmountAndDateTextBox = (TextView) view.findViewById(R.id.showPaymentAmountAndDate);
	        paymentAmountAndDateTextBox.setText(paymentAmountAndDateText);
//	        ImageButton deletePaymentButton = (ImageButton) view.findViewById(R.id.btn_delete_payment);

	        //logNameTextbox.setTextColor(Color.BLACK);
	        //logNameTextbox.setTextSize(18.0f);        
	    }
	
	    @Override
	    public View newView(Context context, Cursor cursor, ViewGroup parent) {
	        View view = View.inflate(context, R.layout.payment_entry, null);
	        
	        String paymentAmount = cursor.getString(cursor.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_AMOUNT));
	        String paymentDate = cursor.getString(cursor.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_MONTH)) + "/" +
	        						cursor.getString(cursor.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_DAY)) + "/" +
	        						cursor.getString(cursor.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_YEAR));
	        String paymentAmountAndDateText = "Amount: " + paymentAmount + " Date: " + paymentDate;
	        
	        
	
	        RowViewHolder holder = new RowViewHolder();
	        holder.logNameTextbox = (TextView) view.findViewById(R.id.paymentEntryText);
	        
	        holder.paymentAmountAndDateTextBox = (TextView) view.findViewById(R.id.showPaymentAmountAndDate);
	        holder.paymentAmountAndDateTextBox.setText(paymentAmountAndDateText);
	        
	        holder.deletePaymentButton = (ImageButton) view.findViewById(R.id.btn_delete_payment);
	        holder.deletePaymentButton.setOnClickListener(deletePaymentButtonOnClickListener);
	        
	        holder.logNameTextbox.setOnClickListener(logNameTextboxOnClickListener);
	
//	        holder.logNameTextbox.setOnClickListener(mOnScheduleNameClickListener);
//	        holder.mDeleteButton.setOnClickListener(mOnDeleteButtonClickListener);
//	        holder.mEnableCheckbox.setOnClickListener(mOnEnableCheckboxClickListener);
		        
	        view.setTag(holder);
	
	        return view;
	    }
	    
//	    private OnClickListener paymentAmountAndDateTextBoxListener = new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				
//				
//			}
//		};
	    
	    private OnClickListener deletePaymentButtonOnClickListener = new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				int position = mListView.getPositionForView((View) v.getParent());
	            final Cursor listItem = (Cursor) mListView.getItemAtPosition(position);
	            AlertDialog.Builder askDialog = new AlertDialog.Builder( listActivity );
	            askDialog.setTitle( "Alert" );
	            askDialog.setMessage( "Are you sure you want to delete this?" );
	            askDialog.setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
		             public void onClick(DialogInterface dialog, int which) {
		            	 Log.d( "AlertDialog", "Positive" );
		            	 String querySql = "_id = " + listItem.getInt(listItem.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_ID));
		            	 
		            	 SQLiteDatabase db = null;
		 	             try {
		 	            	ZakahManagerDBOpenHelper dbHelper = new ZakahManagerDBOpenHelper(v.getContext(), null, null, 0);
		 					db = dbHelper.getWritableDatabase();
		 					// delete selected LOG from DB
			 	             db.delete(ZakahManagerDBOpenHelper.Z_LOG_TABLE, querySql, null);
		 		            db.close();
		 	             } catch (Exception e) {
		 	             	db.close();
		 	             }
		 	             
		 	             //Reload LogListActivity after delete
		 	             Intent i = new Intent(listActivity, LogListActivity.class);
		                 startActivity(i);
		             }
	            });
	            askDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.d("AlertDialog", "Negative");
						dialog.cancel();
					}
				});
	            askDialog.show();
	        }
		};
		
		private OnClickListener logNameTextboxOnClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final int position = mListView.getPositionForView((View) v.getParent());
				Cursor listItem = (Cursor) mListView.getItemAtPosition(position);
				Intent i = new Intent(listActivity, CreateLogActivity.class);
		        i.putExtra("itemSelected", String.valueOf(listItem.getInt(listItem.getColumnIndex(ZakahManagerDBOpenHelper.Z_LOG_ID))));
		        startActivity(i);
			}
		};
    }
}
