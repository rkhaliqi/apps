package com.atforefront.zakahmanager;

import java.util.List;

public class Utility {

	public static boolean validateNumberFields(String input ) {
    	boolean isValid = true;      	
    	if (input.matches("[0-9]+(\\.[0-9]+)?")) {  
            System.out.println("Is a number");  
        } else {  
        	isValid = false;  
        } 
    		
    	return isValid;
    }
	
}
