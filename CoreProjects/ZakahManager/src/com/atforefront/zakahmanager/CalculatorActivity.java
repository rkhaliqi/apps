package com.atforefront.zakahmanager;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

public class CalculatorActivity extends Activity {

	
	private RelativeLayout cashRL;
	private ImageView cashBtn;
	private static boolean cashBtnToggle;

	private RelativeLayout investmentsRL;
	private ImageView investmentsBtn;
	private static boolean investmentsBtnToggle;
	
	private RelativeLayout propertyValueRL;
	private ImageView propertyValueBtn;
	private static boolean propertyValueBtnToggle;
	
	private RelativeLayout loansRL;
	private ImageView loansBtn;
	private static boolean loansBtnToggle;
	
	private RelativeLayout jewelryRL;
	private ImageView jewelryBtn;
	private static String latestNisab;
	private ImageButton homeButton;
	private static boolean jewelryBtnToggle;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator_layout);
        
        setTitleColor(Color.WHITE);
        
        LayoutAnimationController localLayoutAnimationController = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_top_to_bottom_slide);
        
        this.cashRL = ((RelativeLayout)findViewById(R.id.cashLayout));
        this.cashRL.setLayoutAnimation(localLayoutAnimationController);
        
        this.cashBtn = ((ImageView)findViewById(R.id.cashBtn));
        
        this.cashBtn.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramView)
          {
            controlExpandCollapsedForCash();
          }
        });
        
        this.investmentsRL = ((RelativeLayout)findViewById(R.id.investmentsLayout));
        this.investmentsRL.setLayoutAnimation(localLayoutAnimationController);
        
        this.investmentsBtn = ((ImageView)findViewById(R.id.investmentBtn));
        
        this.investmentsBtn.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramView)
          {
            controlExpandCollapsedForInvestments();
          }
        });
        
        this.jewelryRL = ((RelativeLayout)findViewById(R.id.jewelryLayout));
        this.jewelryRL.setLayoutAnimation(localLayoutAnimationController);
        
        this.jewelryBtn = ((ImageView)findViewById(R.id.jewelryBtn));
        
        this.jewelryBtn.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramView)
          {
            controlExpandCollapsedForJewelry();
          }
        });
        
        this.propertyValueRL = ((RelativeLayout)findViewById(R.id.propertyLayout));
        this.propertyValueRL.setLayoutAnimation(localLayoutAnimationController);
        
        this.propertyValueBtn = ((ImageView)findViewById(R.id.propertyBtn));
        
        this.propertyValueBtn.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramView)
          {
            controlExpandCollapsedForPropertyValue();
          }
        });
        
        this.loansRL = ((RelativeLayout)findViewById(R.id.loansLayout));
        this.loansRL.setLayoutAnimation(localLayoutAnimationController);
        
        this.loansBtn = ((ImageView)findViewById(R.id.loansBtn));
        
        this.loansBtn.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramView)
          {
            controlExpandCollapsedForLoans();
          }
        });
        
        homeButton = (ImageButton) findViewById(R.id.btn_go_home);
        
        homeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(v.getContext(),
						ZakahManagerActivity.class);
				startActivity(i);
			}

		});
        
        try {
        	ArrayList<Nisab> currencyList = getNisabCurrencies();
        	
        	final List<String> list=new ArrayList<String>();
        	for (Nisab obj : currencyList) {
        		list.add(obj.getCurrency());
        	}

        	final Spinner sp=(Spinner) findViewById(R.id.currencySpinner);
        	ArrayAdapter<String> adp= new ArrayAdapter<String>(this,
        	                                R.layout.spinner_item,list);
        	adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        	sp.setAdapter(adp);
        	sp.setSelection(adp.getPosition("USD"));
        	
        	final List<String> listCalcYear =new ArrayList<String>();
        	listCalcYear.add("Lunar");
        	listCalcYear.add("Solar");
        	final Spinner spCalcYear=(Spinner) findViewById(R.id.calcYearSpinner);
        	ArrayAdapter<String> adpCalcYear= new ArrayAdapter<String>(this,
        	                                R.layout.spinner_item,listCalcYear);
        	adpCalcYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        	spCalcYear.setAdapter(adpCalcYear);
        	spCalcYear.setSelection(adpCalcYear.getPosition("Lunar"));
        	
		} catch (Exception e) {
			
			e.printStackTrace();
		}
    }
    
    public void controlExpandCollapsedForCash()
    {
      if (!cashBtnToggle ) 
      {
        cashBtn.setImageDrawable(getResources().getDrawable(R.drawable.minus_button_selector));
        cashRL.setVisibility(0);
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_row_right_slide);
        cashRL.startLayoutAnimation();
        cashBtnToggle = true;
      }
      else
      {
    	cashBtnToggle = false;
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_bottom_to_top_slide);
        cashRL.startLayoutAnimation();
        cashBtn.setImageDrawable(getResources().getDrawable(R.drawable.plus_button_selector));
        cashRL.setVisibility(8);
        
      }
    }
    
    public void controlExpandCollapsedForInvestments()
    {
      if (!investmentsBtnToggle ) 
      {
    	  investmentsBtn.setImageDrawable(getResources().getDrawable(R.drawable.minus_button_selector));
    	  investmentsRL.setVisibility(0);
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_row_right_slide);
        investmentsRL.startLayoutAnimation();
        investmentsBtnToggle = true;
      }
      else
      {
    	  investmentsBtnToggle = false;
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_bottom_to_top_slide);
        investmentsRL.startLayoutAnimation();
        investmentsBtn.setImageDrawable(getResources().getDrawable(R.drawable.plus_button_selector));
        investmentsRL.setVisibility(8);
        
      }
    }
    
    public void controlExpandCollapsedForJewelry()
    {
      if (!jewelryBtnToggle ) 
      {
    	jewelryBtn.setImageDrawable(getResources().getDrawable(R.drawable.minus_button_selector));
    	jewelryRL.setVisibility(0);
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_row_right_slide);
        jewelryRL.startLayoutAnimation();
        jewelryBtnToggle = true;
      }
      else
      {
    	jewelryBtnToggle = false;
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_bottom_to_top_slide);
        jewelryRL.startLayoutAnimation();
        jewelryBtn.setImageDrawable(getResources().getDrawable(R.drawable.plus_button_selector));
        jewelryRL.setVisibility(8);
        
      }
    }
    
    public void controlExpandCollapsedForPropertyValue()
    {
      if (!propertyValueBtnToggle ) 
      {
    	propertyValueBtn.setImageDrawable(getResources().getDrawable(R.drawable.minus_button_selector));
    	propertyValueRL.setVisibility(0);
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_row_right_slide);
        propertyValueRL.startLayoutAnimation();
        propertyValueBtnToggle = true;
      }
      else
      {
    	propertyValueBtnToggle = false;
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_bottom_to_top_slide);
        propertyValueRL.startLayoutAnimation();
        propertyValueBtn.setImageDrawable(getResources().getDrawable(R.drawable.plus_button_selector));
        propertyValueRL.setVisibility(8);
        
      }
    }
    
    public void controlExpandCollapsedForLoans()
    {
      if (!loansBtnToggle ) 
      {
    	loansBtn.setImageDrawable(getResources().getDrawable(R.drawable.minus_button_selector));
    	loansRL.setVisibility(0);
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_row_right_slide);
        loansRL.startLayoutAnimation();
        loansBtnToggle = true;
      }
      else
      {
    	loansBtnToggle = false;
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_bottom_to_top_slide);
        loansRL.startLayoutAnimation();
        loansBtn.setImageDrawable(getResources().getDrawable(R.drawable.plus_button_selector));
        loansRL.setVisibility(8);
        
      }
    }
    
    private static String doHttpGet(String paramString)
    	    throws Exception
    	  {
    	    HttpURLConnection localHttpURLConnection = null;
    	    InputStream localInputStream = null;
    	    byte[] arrayOfByte;
            ByteArrayOutputStream localByteArrayOutputStream;
            int j = 0;
            String str = null;
    	    try
    	    {
    	      localHttpURLConnection = (HttpURLConnection)new URL(paramString).openConnection();
    	      localHttpURLConnection.setRequestMethod("GET");
    	      localHttpURLConnection.setRequestProperty("User-Agent", "");
    	      localHttpURLConnection.setConnectTimeout(5000);
    	      int i = localHttpURLConnection.getResponseCode();
    	      if ((i != 401) && (i != 200))
    	        throw new IOException("HTTP Response Code:" + i);
    	      
    	      localInputStream = localHttpURLConnection.getInputStream();
	  	        arrayOfByte = new byte[20156];
	  	        localByteArrayOutputStream = new ByteArrayOutputStream();
	  	        while (j != -1) {
		  	        j = localInputStream.read(arrayOfByte);
		  	        if (j == -1)
		  	        {
		  	          str = new String(localByteArrayOutputStream.toByteArray());
		  	        } else {
		  	        	localByteArrayOutputStream.write(arrayOfByte, 0, j);
	    	            localByteArrayOutputStream.flush();
		  	        }

	  	        }
    	    } catch (Exception e) {
    	    	e.printStackTrace();
    	    } finally {
    	    	localInputStream.close();
    	    	localHttpURLConnection.disconnect();
    	    }
    	    
    	    return str;
    }

    
    public static String getTodaysNisab()
    	    throws Exception
    {
    	    if (latestNisab == null)
    	    {
    	    	latestNisab = convertHtmlTages(doHttpGet("http://e-nisab.com/resources/daily-values.xml"));
    	      //System.out.println("FeedXML = " + str1);
    	    
    	    }
    	    return latestNisab;
    }
    
    public static ArrayList<Nisab> getNisabCurrencies() {
		ArrayList<Nisab> nisabList = new ArrayList<Nisab>();
		
		try{
			   getTodaysNisab();
			   
			   InputStream in = new ByteArrayInputStream(latestNisab.getBytes());

			   BufferedReader br = new BufferedReader(new InputStreamReader(in));
			   String strLine;
			   //Read File Line By Line
			   while ((strLine = br.readLine()) != null)   {
				   if (strLine.startsWith("<tr><td>") ){
				   // Print the content on the console  <tr><td>AED</td>
				   String currency = strLine.substring(8, 11);
				   String usdUnit = br.readLine();
				   int startingIndex = usdUnit.indexOf(">")+1;
				   usdUnit = usdUnit.substring(startingIndex, usdUnit.indexOf("<", startingIndex));
				   String unitsUsd = br.readLine();
				   unitsUsd = unitsUsd.substring(startingIndex, unitsUsd.indexOf("<", startingIndex));
				   String goldNisab = br.readLine();
				   goldNisab = goldNisab.substring(startingIndex, goldNisab.indexOf("<", startingIndex));
				   String silverNisabNonHanafi = br.readLine();
				   silverNisabNonHanafi = silverNisabNonHanafi.substring(startingIndex, silverNisabNonHanafi.indexOf("<", startingIndex));
				   String silverNisab = br.readLine();
				   silverNisab = silverNisab.substring(startingIndex, silverNisab.indexOf("<", startingIndex));
				   //nisabList.add(new Nisab(currency,usdUnit,unitsUsd,goldNisab,silverNisabNonHanafi,silverNisab ));
				   //String currency, double goldNisab, double hanfiNisab,double silverNisab, double unitPerUSD, double usdPerUnit
				   nisabList.add(new Nisab(currency,Double.parseDouble(goldNisab),Double.parseDouble(silverNisabNonHanafi),Double.parseDouble(silverNisab),Double.parseDouble(usdUnit),Double.parseDouble(unitsUsd) ));
				   //System.out.println (strLine.substring(8, 11));
				   //System.out.println(silverNisab.substring(startingIndex, silverNisab.indexOf("<", startingIndex)));
				   }
			   }
			   //Close the input stream
			   in.close();
		   } catch (Exception e){//Catch exception if any
			   System.err.println("Error: " + e.getMessage());
		   }
		
		return nisabList;
	}
    
    public static String convertHtmlTages(String paramString)
    {
      return paramString.replace("&lt;", "<").replace("&gt;", ">").trim();
    }
}
