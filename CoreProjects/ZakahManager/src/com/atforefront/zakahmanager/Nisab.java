package com.atforefront.zakahmanager;

public class Nisab {

	String currency;
	double goldNisab;
	double hanfiNisab;
	double silverNisab;
	double unitPerUSD;
	double usdPerUnit; 

	  public Nisab(String currency, double goldNisab, double hanfiNisab,
			double silverNisab, double unitPerUSD, double usdPerUnit) {
		this.currency = currency;
		this.goldNisab = goldNisab;
		this.hanfiNisab = hanfiNisab;
		this.silverNisab = silverNisab;
		this.unitPerUSD = unitPerUSD;
		this.usdPerUnit = usdPerUnit;
	}

	public String getCurrency()
	  {
	    return this.currency;
	  }

	  public double getGoldNisab()
	  {
	    return this.goldNisab;
	  }

	  public double getHanfiNisab()
	  {
	    return this.hanfiNisab;
	  }

	  public double getSilverNisab()
	  {
	    return this.silverNisab;
	  }

	  public double getUnitPerUSD()
	  {
	    return this.unitPerUSD;
	  }

	  public double getUsdPerUnit()
	  {
	    return this.usdPerUnit;
	  }

	  public void setCurrency(String paramString)
	  {
	    this.currency = paramString;
	  }

	  public void setGoldNisab(double paramDouble)
	  {
	    this.goldNisab = paramDouble;
	  }

	  public void setHanfiNisab(double paramDouble)
	  {
	    this.hanfiNisab = paramDouble;
	  }

	  public void setSilverNisab(double paramDouble)
	  {
	    this.silverNisab = paramDouble;
	  }

	  public void setUnitPerUSD(double paramDouble)
	  {
	    this.unitPerUSD = paramDouble;
	  }

	  public void setUsdPerUnit(double paramDouble)
	  {
	    this.usdPerUnit = paramDouble;
	  }

	  public String toString()
	  {
	    System.out.println("\nCurrency      : " + this.currency);
	    System.out.println("--goldNisab   : " + this.goldNisab);
	    System.out.println("--silverNisab : " + this.silverNisab);
	    System.out.println("--hanfiNisab  : " + this.hanfiNisab);
	    return super.toString();
	  }
}
