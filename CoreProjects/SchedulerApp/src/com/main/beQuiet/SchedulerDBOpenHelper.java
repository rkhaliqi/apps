package com.main.beQuiet;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SchedulerDBOpenHelper extends SQLiteOpenHelper{
	
	public static final String TAG = "SchedulerAppActivity";
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "scheduler.db";
	public static final String SCHEDULE_TABLE_NAME ="schedules";
	public static final String SCH_COLUMN_ID = "_id";
	public static final String SCHEDULE_NAME = "scheduleName";
	public static final String MODE = "mode";
	public static final String START_TIME_MIN = "startTimeMin";
	public static final String START_TIME_HR = "startTimeHr";
	public static final String END_TIME_MIN = "endTimeMin";
	public static final String END_TIME_HR = "endTimeHr";
	public static final String FREQUENCY = "frequency";
	public static final String REPEAT_DAYS = "repeatDays";
	public static final String IS_ENABLED ="isEnabled";
	public static final String PREVIOUS_RING_TABLE_NAME ="previousRingMode";
	public static final String PREV_COLUMN_ID = "previousId";
	public static final String PREVIOUS_MODE = "previousMode";
	public static final String ALL_ENABLED = "isAllEnabled";
	
	private static final String SCHEDULE_TABLE_CREATE = "CREATE TABLE " + SCHEDULE_TABLE_NAME + " (" + 
			SCH_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
			SCHEDULE_NAME + " TEXT not null, " +
			MODE + " TEXT not null, " +
			START_TIME_HR + " INTEGER not null, " +
			START_TIME_MIN + " INTEGER not null, " +
			END_TIME_HR + " INTEGER not null, " +
			END_TIME_MIN + " INTEGER not null, " +
			FREQUENCY + " TEXT not null, " +
			REPEAT_DAYS + " TEXT, " +
			IS_ENABLED + " TEXT not null," +
			PREVIOUS_MODE + " TEXT" +
					");";


	public SchedulerDBOpenHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SCHEDULE_TABLE_CREATE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS "+ SCHEDULE_TABLE_NAME);
        onCreate(db);
	}

}
