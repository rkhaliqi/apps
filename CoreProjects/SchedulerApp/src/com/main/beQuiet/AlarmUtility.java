package com.main.beQuiet;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class AlarmUtility {
	//TODO: later on move all alarm related code here so that they are easy to maintain and we're Joe compliant :)

	public static final int startId = 8000;
	
	public void addAlarms(int id, int startHour, int startMinute, int endHour,
			int endMinute, String mode, String frequencyDays, Context context) {
		boolean isStartPm = false;
		int alteredStartTime = startHour;
		if (startHour > 11) {
			alteredStartTime = startHour - 12;
			isStartPm = true;
		}

		// get a Calendar object with current time
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, alteredStartTime);
		cal.set(Calendar.MINUTE, startMinute);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.AM_PM, isStartPm == true ? Calendar.PM : Calendar.AM);
		
		String overnightDelivery = "0";
		Intent startIntent = new Intent(context,
				AlarmReceiver.class);
		startIntent.putExtra("alarm_message", "Start:" + frequencyDays + ":" + overnightDelivery + "0" +":" + mode + ":" + id);
		// In reality, you would want to have a static variable for the request
		// code instead of 192837
		PendingIntent sender = PendingIntent.getBroadcast(context, startId + id,
				startIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		// Get the AlarmManager service
		AlarmManager am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
		// am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), sender);
		
		boolean isEndPm = false;
		int alteredEndTime = endHour;
		if (endHour > 11) {
			alteredEndTime = endHour - 12;
			isEndPm = true;
		}
		
		// get a Calendar object with current time
		Calendar cal2 = Calendar.getInstance();
		cal2.set(Calendar.HOUR, alteredEndTime);
		cal2.set(Calendar.MINUTE, endMinute);
		cal2.set(Calendar.SECOND, 0);
		cal2.set(Calendar.AM_PM, isEndPm == true ? Calendar.PM : Calendar.AM);
		
		Intent endIntent = new Intent(context, AlarmReceiver.class);
		
		if (isStartPm && !isEndPm) {
			overnightDelivery = "1";
		}
		endIntent.putExtra("alarm_message",
				"End:" + frequencyDays + ":" + overnightDelivery + "0" + ":" + id);
		// In reality, you would want to have a static variable for the request
		// code instead of 192837
		PendingIntent sender2 = PendingIntent.getBroadcast(context, 9000 + id,
				endIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		
		if (isStartPm && !isEndPm) {
		
			// one-time alarm
			if(frequencyDays.toString().equalsIgnoreCase("x")){
				am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
						sender);
			}
			// recurring alarm
			else {
				am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
						AlarmManager.INTERVAL_DAY, sender);
			}		
			
			cal2.set(Calendar.HOUR, 11);
			cal2.set(Calendar.MINUTE, 59);
			cal2.set(Calendar.SECOND, 59);
			cal2.set(Calendar.AM_PM, Calendar.PM);
			
			endIntent = new Intent(context, AlarmReceiver.class);
			endIntent.putExtra("alarm_message",
					"End:" + frequencyDays + ":" + overnightDelivery + "1" + ":" + id);
			sender2 = PendingIntent.getBroadcast(context, 4000 + id,
					endIntent, PendingIntent.FLAG_CANCEL_CURRENT);
			
			if(frequencyDays.toString().equalsIgnoreCase("x")){
				am.set(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
						sender2);
			}
			// recurring alarm
			else {
				am.setRepeating(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
						AlarmManager.INTERVAL_DAY, sender2);
			}
			
			cal.set(Calendar.HOUR, 12);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 17);
			cal.set(Calendar.AM_PM, Calendar.AM);
			cal.add(Calendar.DAY_OF_MONTH, 1);

			startIntent = new Intent(context,
					AlarmReceiver.class);
			startIntent.putExtra("alarm_message", "Start:" + frequencyDays + ":" + overnightDelivery + "1" + ":" + mode + ":" + id);
			
			sender = PendingIntent.getBroadcast(context, 7000 + id,
					startIntent, PendingIntent.FLAG_CANCEL_CURRENT);
			
			// one-time alarm
			if(frequencyDays.toString().equalsIgnoreCase("x")){
				am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
						sender);
			}
			// recurring alarm
			else {
				am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
						AlarmManager.INTERVAL_DAY, sender);
			}
			
			cal2.set(Calendar.HOUR, alteredEndTime);
			cal2.set(Calendar.MINUTE, endMinute);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.AM_PM, Calendar.AM);
			cal2.add(Calendar.DAY_OF_MONTH, 1);
			
			endIntent = new Intent(context, AlarmReceiver.class);
			
			endIntent.putExtra("alarm_message",
					"End:" + frequencyDays + ":" + overnightDelivery + "0" + ":" + id);
			
			sender2 = PendingIntent.getBroadcast(context, 9000 + id,
					endIntent, PendingIntent.FLAG_CANCEL_CURRENT);
			
			if(frequencyDays.toString().equalsIgnoreCase("x")){
				am.set(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
						sender2);
			}
			// recurring alarm
			else {
				am.setRepeating(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
						AlarmManager.INTERVAL_DAY, sender2);
			}
			
			
		} else {
			// one-time alarm
			if(frequencyDays.toString().equalsIgnoreCase("x")){
				am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
						sender);
			}
			// recurring alarm
			else {
				am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
						AlarmManager.INTERVAL_DAY, sender);
			}
			
			if(frequencyDays.toString().equalsIgnoreCase("x")){
				am.set(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
						sender2);
			}
			// recurring alarm
			else {
				am.setRepeating(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
						AlarmManager.INTERVAL_DAY, sender2);
			}
			
		}
		
	}
	
	public void cancelAlarm(int id, Context context) {
		// Get the AlarmManager service
		AlarmManager am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
		Intent intent = new Intent(context, AlarmReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 8000 + id,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);
		am.cancel(sender);
		sender = PendingIntent.getBroadcast(context, 7000 + id,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);
		am.cancel(sender);
		PendingIntent sender2 = PendingIntent.getBroadcast(context, 4000 + id,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);
		am.cancel(sender2);
		sender2 = PendingIntent.getBroadcast(context, 9000 + id,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);
		am.cancel(sender2);

	}

}
