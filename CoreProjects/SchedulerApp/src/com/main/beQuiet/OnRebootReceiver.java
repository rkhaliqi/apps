package com.main.beQuiet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

// following http://stackoverflow.com/questions/8063144/on-device-reboot-alarm-is-not-going-off-at-specific-time

public class OnRebootReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context2, Intent intent) {
		Context context = Application.getContext();
		Cursor schedulesCursor = null;
		SQLiteDatabase db = null;
		try {
			SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(context, null, null, 0);
			db = dbHelper.getWritableDatabase();
			
			schedulesCursor = getSchedules(context, db);
			schedulesCursor.moveToFirst();
			
			// add alarms for each existing schedule if it's ENABLED
			while(!schedulesCursor.isAfterLast()){
				if(schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.IS_ENABLED)).equalsIgnoreCase("1")){
					AlarmUtility alarmUtility = new AlarmUtility();		
					if(schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.FREQUENCY)).equalsIgnoreCase("0")){
						alarmUtility.addAlarms(schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID)), 
	            				schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.START_TIME_HR)), 
	            				schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.START_TIME_MIN)), 
	            				schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.END_TIME_HR)), 
	            				schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.END_TIME_MIN)), 
	            				schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.MODE)),
	            			"x", context);
	            	}
	            	// for Recurring
	            	else if(schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.FREQUENCY)).equalsIgnoreCase("1")){
	            		alarmUtility.addAlarms(schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID)), 
	            			schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.START_TIME_HR)), 
	            			schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.START_TIME_MIN)), 
	            			schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.END_TIME_HR)), 
	            			schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.END_TIME_MIN)), 
	            			schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.MODE)), 
	            			schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.REPEAT_DAYS)), context);
	            	}
				}
			    schedulesCursor.moveToNext();
			}
			
			schedulesCursor.close();
			db.close();
					
		} 
		catch (Exception e) {
			Toast.makeText(context, "There was an error somewhere, but OnRebootReceiver",
					Toast.LENGTH_SHORT).show();
			e.printStackTrace();
			schedulesCursor.close();
			db.close();
		}
	}

	private Cursor getSchedules(Context context, SQLiteDatabase db)
    {
		Cursor results = null;
		try {
			results = db.query(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, null, null, null, null, null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return results;
    }
}
