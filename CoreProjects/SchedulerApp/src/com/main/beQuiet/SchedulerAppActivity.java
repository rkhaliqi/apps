package com.main.beQuiet;

import java.util.Calendar;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

public class SchedulerAppActivity extends Activity {

	public static final String TAG = "SchedulerAppActivity";
	private Spinner modeSpinner;
	private EditText nameText;
	private LinearLayout daysLabelLayout;
	private LinearLayout daysBoxesLayout;
	private Spinner frequencySpinner;
	private TimePicker startTimePicker;
	private TimePicker endTimePicker;
	private Button saveButton;
	private Button cancelButton;
	boolean isEditMode;
	int editingScheduleId;
	private String itemValue;
	CheckBox mondayCheckBox;
	CheckBox tuesdayCheckBox;
	CheckBox wednesdayCheckBox;
	CheckBox thursdayCheckBox;
	CheckBox fridayCheckBox;
	CheckBox saturdayCheckBox;
	CheckBox sundayCheckBox;
	boolean isSuccessfulInsert;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.v(TAG, "Activity State: onCreate()");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String value = extras.getString("itemSelected");
			if (!value.equals("0")) {
				isEditMode = true;
				editingScheduleId = Integer.parseInt(value);
			}
		}

		Log.v(TAG, "Finding spinner");
		
		isSuccessfulInsert = true;

		if (isEditMode) {
			setTitle(R.string.editScheduleTitle);
		} else {
			setTitle(R.string.addScheduleTitle);
		}

		modeSpinner = (Spinner) findViewById(R.id.modeSpinner);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter
				.createFromResource(this, R.array.modes_array,
						android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		modeSpinner.setAdapter(adapter);
		modeSpinner.setPrompt(getString(R.string.selectLabel));
		Log.v(TAG, "mode spinner setAdapter");

		// Log.v(TAG, "Added the adapter string labels");

		daysLabelLayout = (LinearLayout) findViewById(R.id.daysLabelLayout);
		daysBoxesLayout = (LinearLayout) findViewById(R.id.daysBoxesLayout);

		daysLabelLayout.setVisibility(View.GONE);
		daysBoxesLayout.setVisibility(View.INVISIBLE);

		frequencySpinner = (Spinner) findViewById(R.id.frequencySpinner);

		startTimePicker = (TimePicker) findViewById(R.id.startTimePicker);
		endTimePicker = (TimePicker) findViewById(R.id.endTimePicker);

		adapter = ArrayAdapter
				.createFromResource(this, R.array.frequencies_array,
						android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		frequencySpinner.setAdapter(adapter);
		frequencySpinner.setPrompt(getString(R.string.frequencyLabel));
		itemValue = frequencySpinner.getSelectedItem().toString();

		frequencySpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View arg1, int pos, long id) {
						itemValue = parent.getItemAtPosition(pos).toString();
						// nameText.setText(itemValue);
						if (itemValue.equalsIgnoreCase("Recurring")) {
							daysLabelLayout.setVisibility(View.VISIBLE);
							daysBoxesLayout.setVisibility(View.VISIBLE);
						} else if (itemValue.equalsIgnoreCase("One Time")) {
							daysLabelLayout.setVisibility(View.GONE);
							daysBoxesLayout.setVisibility(View.GONE);
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}

				});

		nameText = (EditText) findViewById(R.id.nameText);

		mondayCheckBox = (CheckBox) findViewById(R.id.mondaycheckbox);
		tuesdayCheckBox = (CheckBox) findViewById(R.id.tuesdaycheckbox);
		wednesdayCheckBox = (CheckBox) findViewById(R.id.wednesdaycheckbox);
		thursdayCheckBox = (CheckBox) findViewById(R.id.thursdaycheckbox);
		fridayCheckBox = (CheckBox) findViewById(R.id.fridaycheckbox);
		saturdayCheckBox = (CheckBox) findViewById(R.id.saturdaycheckbox);
		sundayCheckBox = (CheckBox) findViewById(R.id.sundaycheckbox);

		saveButton = (Button) findViewById(R.id.saveButton);

		// Save Button's OnClickListener
		saveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isEditMode) {
					updateSchedule(editingScheduleId);
				} else {
					insertNewSchedule();
				}
				if (isSuccessfulInsert){
					launchScheduleList();
				}
				// finish();
			}
		});

		cancelButton = (Button) findViewById(R.id.cancelButton);

		// Cancel Button's OnClickListener
		cancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
//				finish();
				launchScheduleList();
				

			}
		});

		if (isEditMode) {
			getExistingSchedule(editingScheduleId);
		}

		// db.close();

		Log.v(TAG, "Completed");
	}
	
	@Override
    protected void onResume() {
    	super.onResume();
//    	startTimePicker = (TimePicker) findViewById(R.id.startTimePicker);
    	if (isEditMode) {
			getExistingSchedule(editingScheduleId);
		}
    	else{
    		Calendar calendar = Calendar.getInstance();
    		int currentHour = calendar.get(Calendar.HOUR);
			int currentMinute = calendar.get(Calendar.MINUTE);
			int am_pm = calendar.get(Calendar.AM_PM);
			if (am_pm == 1){
				currentHour = currentHour + 12;
			}
			startTimePicker.setCurrentHour(currentHour);
			startTimePicker.setCurrentMinute(currentMinute);
			endTimePicker.setCurrentHour(currentHour);
			endTimePicker.setCurrentMinute(currentMinute);
    	}
        
    }

	public void insertNewSchedule() {
		boolean anyDaySelected = false;
		isSuccessfulInsert = true;
		ContentValues content = new ContentValues();
		content.put(SchedulerDBOpenHelper.SCHEDULE_NAME, nameText.getText().toString().trim());
		content.put(SchedulerDBOpenHelper.MODE,
				Long.toString(modeSpinner.getSelectedItemId()));
		content.put(SchedulerDBOpenHelper.START_TIME_HR,
				startTimePicker.getCurrentHour());
		content.put(SchedulerDBOpenHelper.START_TIME_MIN,
				startTimePicker.getCurrentMinute());
		content.put(SchedulerDBOpenHelper.END_TIME_HR,
				endTimePicker.getCurrentHour());
		content.put(SchedulerDBOpenHelper.END_TIME_MIN,
				endTimePicker.getCurrentMinute());
		content.put(SchedulerDBOpenHelper.FREQUENCY,
				Long.toString(frequencySpinner.getSelectedItemId()));
		content.put(SchedulerDBOpenHelper.IS_ENABLED, true);

		String frequencyDays = null;
		
		// TODO: refactor to loop through all checkbox objects to append 0/1
		if (frequencySpinner.getSelectedItem().toString().equalsIgnoreCase("Recurring")) {
			frequencyDays = constructFrequencyBinaryString();
			if (frequencyDays.indexOf("1") != -1) {
				anyDaySelected = true;
			}

			content.put(SchedulerDBOpenHelper.REPEAT_DAYS, frequencyDays.toString());
		}
		// validation for name
		if (nameText.getText().toString().trim().length() == 0) {
			isSuccessfulInsert = false;
			nameText.setError("Name is required!");
		}
//		// validation for start-end time. end time should be greater than start time
//		if (startTimePicker.getCurrentHour() > endTimePicker.getCurrentHour()){
//			isSuccessfulInsert = false;
//			Toast.makeText(this.getApplicationContext(), "End time cannot be earlier than start time", Toast.LENGTH_SHORT).show();
//		}
		if (startTimePicker.getCurrentHour() == endTimePicker.getCurrentHour() &&
				startTimePicker.getCurrentMinute() ==  endTimePicker.getCurrentMinute()){
			
				isSuccessfulInsert = false;
				Toast.makeText(this.getApplicationContext(), "Start and end time cannot be same", Toast.LENGTH_SHORT).show();
		}
		if (startTimePicker.getCurrentHour() == endTimePicker.getCurrentHour()){
			if(startTimePicker.getCurrentMinute() >  endTimePicker.getCurrentMinute()){
				isSuccessfulInsert = false;
				Toast.makeText(this.getApplicationContext(), "Can't create PM to earlier PM (i.e. 10:25pm-10:15pm) ", Toast.LENGTH_SHORT).show();
			}
			
		}
		if ((startTimePicker.getCurrentHour() > 12 && endTimePicker.getCurrentHour() > 12) &&
				(startTimePicker.getCurrentHour() > endTimePicker.getCurrentHour())) {
			isSuccessfulInsert = false;
			Toast.makeText(this.getApplicationContext(), "Can't create PM to earlier PM (i.e. 10:25pm-9:15pm) ", Toast.LENGTH_SHORT).show();
		}
		
		//Error handling: one-time alarm�s start time cannot be earlier than current time
		if (anyDaySelected == false) // one-time
		{	
			Calendar calendar = Calendar.getInstance();
			int currentHour = calendar.get(Calendar.HOUR);
			int currentMinute = calendar.get(Calendar.MINUTE);
			int am_pm = calendar.get(Calendar.AM_PM);
			if (am_pm == 1){
				currentHour = currentHour + 12;
			}
			
//			if( currentHour > startTimePicker.getCurrentHour()){
//				isSuccessfulInsert = false;
//				Toast.makeText(this.getApplicationContext(), "One time alarm's start time cannot be earlier than current time", 
//						Toast.LENGTH_SHORT).show();
//			}
//			else if (currentHour == startTimePicker.getCurrentHour() && currentMinute > startTimePicker.getCurrentMinute()){
//				isSuccessfulInsert = false;
//				Toast.makeText(this.getApplicationContext(), "One time alarm's start time cannot be earlier than current time", 
//						Toast.LENGTH_SHORT).show();
//			}
		}
		
		if (frequencySpinner.getSelectedItem().toString().equalsIgnoreCase("Recurring") && anyDaySelected==false){
			isSuccessfulInsert = false;
			Toast.makeText(this.getApplicationContext(), "Select at least one day", Toast.LENGTH_SHORT).show();
		}
		if(isSuccessfulInsert ){
			AlarmUtility alarmUtility = new AlarmUtility();
			SQLiteDatabase db = null;
			Long scheduleId = null;
			try {
				SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(
						this.getApplicationContext(), null, null, 0);
				db = dbHelper.getWritableDatabase();
				scheduleId = db.insert(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, null, content);
				db.close();
			} catch (Exception e) {
				db.close();
			}
			if (frequencySpinner.getSelectedItem().toString()
					.equalsIgnoreCase("One Time")) {
				alarmUtility.addAlarms(scheduleId.intValue(), startTimePicker.getCurrentHour(),
						startTimePicker.getCurrentMinute(),
						endTimePicker.getCurrentHour(),
						endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()), "x", this.getApplicationContext());
			} else {
				alarmUtility.addAlarms(scheduleId.intValue(), startTimePicker.getCurrentHour(),
						startTimePicker.getCurrentMinute(),
						endTimePicker.getCurrentHour(),
						endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()),
						frequencyDays.toString(), this.getApplicationContext());
			}
		}
	}

	public void editAlarm(int id, int startHour, int startMinute, int endHour,
			int endMinute, String mode, String frequency) {
		
		AlarmUtility alarmUtility = new AlarmUtility();
		
		alarmUtility.cancelAlarm(id, this.getApplicationContext());
		
		if (frequencySpinner.getSelectedItem().toString()
				.equalsIgnoreCase("One Time")) {
			alarmUtility.addAlarms(id, startTimePicker.getCurrentHour(),
					startTimePicker.getCurrentMinute(),
					endTimePicker.getCurrentHour(),
					endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()), "x", 
					this.getApplicationContext());
		} else {
			String frequencyDays = constructFrequencyBinaryString();
			alarmUtility.addAlarms(id, startTimePicker.getCurrentHour(),
					startTimePicker.getCurrentMinute(),
					endTimePicker.getCurrentHour(),
					endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()),
					frequencyDays, this.getApplicationContext());
		}
	}

	private String constructFrequencyBinaryString() {
		StringBuffer frequencyDays = null;
		if (frequencySpinner.getSelectedItem().toString()
				.equalsIgnoreCase("Recurring")) {
			frequencyDays = new StringBuffer();
			if (mondayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (tuesdayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (wednesdayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (thursdayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (fridayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (saturdayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (sundayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
		}
		return frequencyDays.toString();
	}

	public void getExistingSchedule(int schedulerId) {
		String querySql = "_id = " + schedulerId;
		SQLiteDatabase db = null;
		Cursor results = null;
		try {
			SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(
					this.getApplicationContext(), null, null, 0);
			db  = dbHelper.getWritableDatabase();
			results = db.query(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME,
					null, querySql, null, null, null, null);
		
		
			String days = null;
	
			if (results != null) {
				results.moveToFirst();
	
				nameText.setText(results.getString(results
						.getColumnIndex(SchedulerDBOpenHelper.SCHEDULE_NAME)));
				modeSpinner.setSelection(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.MODE)));
				startTimePicker.setCurrentHour(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.START_TIME_HR)));
				startTimePicker.setCurrentMinute(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.START_TIME_MIN)));
				endTimePicker.setCurrentHour(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.END_TIME_HR)));
				endTimePicker.setCurrentMinute(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.END_TIME_MIN)));
				frequencySpinner.setSelection(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.FREQUENCY)));
	
				if (frequencySpinner.getSelectedItem().toString()
						.equalsIgnoreCase("Recurring")) {
					days = results.getString(results
							.getColumnIndex(SchedulerDBOpenHelper.REPEAT_DAYS));
	
					if (days != null) {
						mondayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(0))) ? true : false);
						tuesdayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(1))) ? true : false);
						wednesdayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(2))) ? true : false);
						thursdayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(3))) ? true : false);
						fridayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(4))) ? true : false);
						saturdayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(5))) ? true : false);
						sundayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(6))) ? true : false);
					}
				}
	
			}
			results.close();
		
			db.close();
		} catch (Exception e) {
			results.close();
			db.close();
		}
	}

	public void updateSchedule(int schedulerId) {
		boolean anyDaySelected = false;
		
		isSuccessfulInsert = true;
		
		String querySql = "_id = " + schedulerId;
		ContentValues content = new ContentValues();
		content.put(SchedulerDBOpenHelper.SCHEDULE_NAME, nameText.getText().toString().trim());
		content.put(SchedulerDBOpenHelper.MODE,
				Long.toString(modeSpinner.getSelectedItemId()));
		content.put(SchedulerDBOpenHelper.START_TIME_HR,
				startTimePicker.getCurrentHour());
		content.put(SchedulerDBOpenHelper.START_TIME_MIN,
				startTimePicker.getCurrentMinute());
		content.put(SchedulerDBOpenHelper.END_TIME_HR,
				endTimePicker.getCurrentHour());
		content.put(SchedulerDBOpenHelper.END_TIME_MIN,
				endTimePicker.getCurrentMinute());
		content.put(SchedulerDBOpenHelper.FREQUENCY,
				Long.toString(frequencySpinner.getSelectedItemId()));
		// content.put(SchedulerDBOpenHelper.REPEAT_DAYS, "");
		content.put(SchedulerDBOpenHelper.IS_ENABLED, true);
		String frequencyDays = null;
		if (frequencySpinner.getSelectedItem().toString()
				.equalsIgnoreCase("Recurring")) {
			frequencyDays = constructFrequencyBinaryString();
			if (frequencyDays.indexOf("1") != -1) {
				anyDaySelected = true;
			}

			content.put(SchedulerDBOpenHelper.REPEAT_DAYS,
					frequencyDays.toString());
		}
		// update alarms 1. Cancel previous alarms associated w/ this scheduleId
		// 2. create new alarms
		
		if (nameText.getText().toString().trim().length() == 0) {
			isSuccessfulInsert = false;
			nameText.setError("Name is required!");
		}
		
		// validation for start-end time. end time should be greater than start
		// time
//		if (startTimePicker.getCurrentHour() > endTimePicker.getCurrentHour()) {
//			isSuccessfulInsert = false;
//			Toast.makeText(this.getApplicationContext(),
//					"End time cannot be earlier than start time",
//					Toast.LENGTH_SHORT).show();
//		}
		if (startTimePicker.getCurrentHour() == endTimePicker.getCurrentHour() && 
				startTimePicker.getCurrentMinute() == endTimePicker.getCurrentMinute()) {

				isSuccessfulInsert = false;
				Toast.makeText(this.getApplicationContext(), 
						"Start and end time cannot be same", Toast.LENGTH_SHORT).show();
		}
//		if (startTimePicker.getCurrentHour() == endTimePicker.getCurrentHour()) {
//			if (startTimePicker.getCurrentMinute() > endTimePicker.getCurrentMinute()) {
//				isSuccessfulInsert = false;
//				Toast.makeText(this.getApplicationContext(),
//						"End time cannot be earlier than start time ", Toast.LENGTH_SHORT).show();
//			}
//		}
		
		//Error handling: one-time alarm�s start time cannot be earlier than current time
		if (anyDaySelected == false) // one-time
		{	
			Calendar calendar = Calendar.getInstance();
			int currentHour = calendar.get(Calendar.HOUR);
			int currentMinute = calendar.get(Calendar.MINUTE);
			int am_pm = calendar.get(Calendar.AM_PM);
			if (am_pm == 1){
				currentHour = currentHour + 12;
			}
			
//			if( currentHour > startTimePicker.getCurrentHour()){
//				isSuccessfulInsert = false;
//				Toast.makeText(this.getApplicationContext(), "One time alarm's start time cannot be earlier than current time", 
//						Toast.LENGTH_SHORT).show();
//			}
//			else if (currentHour == startTimePicker.getCurrentHour() && currentMinute > startTimePicker.getCurrentMinute()){
//				isSuccessfulInsert = false;
//				Toast.makeText(this.getApplicationContext(), "One time alarm's start time cannot be earlier than current time", 
//						Toast.LENGTH_SHORT).show();
//			}
		}
				
		if (frequencySpinner.getSelectedItem().toString().equalsIgnoreCase("Recurring") && anyDaySelected==false){
			isSuccessfulInsert = false;
			Toast.makeText(this.getApplicationContext(), "Select at least one day", Toast.LENGTH_SHORT).show();
		}
		if (isSuccessfulInsert) {
			SQLiteDatabase db = null;
			try {
				SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(
						this.getApplicationContext(), null, null, 0);
				db = dbHelper.getWritableDatabase();
				db.update(
						SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, content,
						querySql, null);
				db.close();
			} catch (Exception e) {
				db.close();
			}
			

			if (frequencySpinner.getSelectedItem().toString()
					.equalsIgnoreCase("One Time")) {
				editAlarm(schedulerId, startTimePicker.getCurrentHour(),
						startTimePicker.getCurrentMinute(),
						endTimePicker.getCurrentHour(),
						endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()), "x");
			} else {
				editAlarm(schedulerId, startTimePicker.getCurrentHour(),
						startTimePicker.getCurrentMinute(),
						endTimePicker.getCurrentHour(),
						endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()),
						frequencyDays.toString());
			}
		}

	}

	protected void launchScheduleList() {
		Intent i = new Intent(this, ScheduleListActivity.class);
		startActivity(i);
	}
	
	protected void relaunchSchedulerAppActivity() {
		Intent i = new Intent(this, SchedulerAppActivity.class);
		startActivity(i);
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

	    View v = getCurrentFocus();
	    boolean ret = super.dispatchTouchEvent(event);

	    if (v instanceof EditText) {
	        View w = getCurrentFocus();
	        int scrcoords[] = new int[2];
	        w.getLocationOnScreen(scrcoords);
	        float x = event.getRawX() + w.getLeft() - scrcoords[0];
	        float y = event.getRawY() + w.getTop() - scrcoords[1];

	        Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
	        if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) { 

	            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
	        }
	    }
	return ret;
	}
}