package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
//import android.widget.EditText;

//import android.content.Intent.VIEW_ACTION;

public class Hello extends Activity {
	   /** Called when the activity is first created. */
	   @Override
	   public void onCreate(Bundle savedInstanceState) {
	       super.onCreate(savedInstanceState);
	       TextView tv = new TextView(this);
	       tv.setText("Hello, Android!");
	       //tv.setTextColor(180);
	       
	       //EditText et = new EditText(this);
	       //et.setText("This is a text box");
	       //setContentView(et);
	       setContentView(tv);
	   }
	}