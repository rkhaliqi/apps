package com.main.quietSalah;

import java.util.Calendar;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class SchedulerAppActivity extends Activity {

	public static final String TAG = "SchedulerAppActivity";
	private Spinner modeSpinner;
	private EditText nameText;
	private LinearLayout daysLabelLayout;
	private LinearLayout daysBoxesLayout;
	private Spinner frequencySpinner;
	private TimePicker startTimePicker;
	private TimePicker endTimePicker;
	private Button saveButton;
	private Button cancelButton;
	int editingScheduleId;
	private String itemValue;
	CheckBox mondayCheckBox;
	CheckBox tuesdayCheckBox;
	CheckBox wednesdayCheckBox;
	CheckBox thursdayCheckBox;
	CheckBox fridayCheckBox;
	CheckBox saturdayCheckBox;
	CheckBox sundayCheckBox;
	boolean isSuccessfulInsert;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.v(TAG, "Activity State: onCreate()");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String value = extras.getString("itemSelected");
			if (!value.equals("0")) {
				editingScheduleId = Integer.parseInt(value);
			}
		}

		Log.v(TAG, "Finding spinner");
		
		isSuccessfulInsert = true;

		setTitle(R.string.editScheduleTitle);

		modeSpinner = (Spinner) findViewById(R.id.modeSpinner);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter
				.createFromResource(this, R.array.modes_array,
						android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		modeSpinner.setAdapter(adapter);
		modeSpinner.setPrompt(getString(R.string.selectLabel));
		Log.v(TAG, "mode spinner setAdapter");

		// Log.v(TAG, "Added the adapter string labels");

		daysLabelLayout = (LinearLayout) findViewById(R.id.daysLabelLayout);
		daysBoxesLayout = (LinearLayout) findViewById(R.id.daysBoxesLayout);

		frequencySpinner = (Spinner) findViewById(R.id.frequencySpinner);

		startTimePicker = (TimePicker) findViewById(R.id.startTimePicker);
		endTimePicker = (TimePicker) findViewById(R.id.endTimePicker);

		adapter = ArrayAdapter
				.createFromResource(this, R.array.frequencies_array,
						android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		frequencySpinner.setAdapter(adapter);
		frequencySpinner.setPrompt(getString(R.string.frequencyLabel));
		itemValue = frequencySpinner.getSelectedItem().toString();

		frequencySpinner.setEnabled(false);
		frequencySpinner.setBackgroundColor(Color.LTGRAY);

		// Start: make all labels bigger, bold, and WHITE
		TextView nameTextView = (TextView) findViewById(R.id.nameLabel);
		nameTextView.setTextSize(21.0f);
		nameTextView.setTypeface(Typeface.DEFAULT_BOLD);
		nameTextView.setTextColor(Color.WHITE);
		
		TextView modeTextView = (TextView) findViewById(R.id.modeLabel);
		modeTextView.setTextSize(21.0f);
		modeTextView.setTypeface(Typeface.DEFAULT_BOLD);
		modeTextView.setTextColor(Color.WHITE);
		
		TextView startTimeTextView = (TextView) findViewById(R.id.startTimeLabel);
		startTimeTextView.setTextSize(21.0f);
		startTimeTextView.setTypeface(Typeface.DEFAULT_BOLD);
		startTimeTextView.setTextColor(Color.WHITE);
		
		TextView endTimeTextView = (TextView) findViewById(R.id.endTimeLabel);
		endTimeTextView.setTextSize(21.0f);
		endTimeTextView.setTypeface(Typeface.DEFAULT_BOLD);
		endTimeTextView.setTextColor(Color.WHITE);
		
		TextView frequencyTextView = (TextView) findViewById(R.id.frequencyLabel);
		frequencyTextView.setTextSize(21.0f);
		frequencyTextView.setTypeface(Typeface.DEFAULT_BOLD);
		frequencyTextView.setTextColor(Color.WHITE);
		
		TextView mondayTextView = (TextView) findViewById(R.id.mondayLabel);
		mondayTextView.setTextSize(18.0f);
		mondayTextView.setTypeface(Typeface.DEFAULT_BOLD);
		mondayTextView.setTextColor(Color.BLACK);
		mondayTextView.setBackgroundColor(Color.WHITE);
		
		TextView tuesdayTextView = (TextView) findViewById(R.id.tuesdayLabel);
		tuesdayTextView.setTextSize(18.0f);
		tuesdayTextView.setTypeface(Typeface.DEFAULT_BOLD);
		tuesdayTextView.setTextColor(Color.BLACK);
		tuesdayTextView.setBackgroundColor(Color.WHITE);
		
		TextView wednesdayTextView = (TextView) findViewById(R.id.wednesdayLabel);
		wednesdayTextView.setTextSize(18.0f);
		wednesdayTextView.setTypeface(Typeface.DEFAULT_BOLD);
		wednesdayTextView.setTextColor(Color.BLACK);
		wednesdayTextView.setBackgroundColor(Color.WHITE);

		TextView thursdayTextView = (TextView) findViewById(R.id.thursdayLabel);
		thursdayTextView.setTextSize(18.0f);
		thursdayTextView.setTypeface(Typeface.DEFAULT_BOLD);
		thursdayTextView.setTextColor(Color.BLACK);
		thursdayTextView.setBackgroundColor(Color.WHITE);
		
		TextView fridayTextView = (TextView) findViewById(R.id.fridayLabel);
		fridayTextView.setTextSize(18.0f);
		fridayTextView.setTypeface(Typeface.DEFAULT_BOLD);
		fridayTextView.setTextColor(Color.BLACK);
		fridayTextView.setBackgroundColor(Color.WHITE);
		
		TextView saturdayTextView = (TextView) findViewById(R.id.saturdayLabel);
		saturdayTextView.setTextSize(18.0f);
		saturdayTextView.setTypeface(Typeface.DEFAULT_BOLD);
		saturdayTextView.setTextColor(Color.BLACK);
		saturdayTextView.setBackgroundColor(Color.WHITE);
		
		TextView sundayTextView = (TextView) findViewById(R.id.sundayLabel);
		sundayTextView.setTextSize(18.0f);
		sundayTextView.setTypeface(Typeface.DEFAULT_BOLD);
		sundayTextView.setTextColor(Color.BLACK);
		sundayTextView.setBackgroundColor(Color.WHITE);
		// End: make all labels bigger, bold, and WHITE
		
		nameText = (EditText) findViewById(R.id.nameText);
		//nameText.setTextSize(22.0f);
		nameText.setEnabled(false);

		mondayCheckBox = (CheckBox) findViewById(R.id.mondaycheckbox);
		mondayCheckBox.setClickable(false);
		mondayCheckBox.setBackgroundColor(Color.WHITE);
		tuesdayCheckBox = (CheckBox) findViewById(R.id.tuesdaycheckbox);
		tuesdayCheckBox.setClickable(false);
		tuesdayCheckBox.setBackgroundColor(Color.WHITE);
		wednesdayCheckBox = (CheckBox) findViewById(R.id.wednesdaycheckbox);
		wednesdayCheckBox.setClickable(false);
		wednesdayCheckBox.setBackgroundColor(Color.WHITE);
		thursdayCheckBox = (CheckBox) findViewById(R.id.thursdaycheckbox);
		thursdayCheckBox.setClickable(false);
		thursdayCheckBox.setBackgroundColor(Color.WHITE);
		fridayCheckBox = (CheckBox) findViewById(R.id.fridaycheckbox);
		fridayCheckBox.setClickable(false);
		fridayCheckBox.setBackgroundColor(Color.WHITE);
		saturdayCheckBox = (CheckBox) findViewById(R.id.saturdaycheckbox);
		saturdayCheckBox.setClickable(false);
		saturdayCheckBox.setBackgroundColor(Color.WHITE);
		sundayCheckBox = (CheckBox) findViewById(R.id.sundaycheckbox);
		sundayCheckBox.setClickable(false);
		sundayCheckBox.setBackgroundColor(Color.WHITE);

		saveButton = (Button) findViewById(R.id.saveButton);

		// Save Button's OnClickListener
		saveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				updateSchedule(editingScheduleId);
				if (isSuccessfulInsert){
					launchScheduleList();
				}
			}
		});

		cancelButton = (Button) findViewById(R.id.cancelButton);

		// Cancel Button's OnClickListener
		cancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				launchScheduleList();
			}
		});
		
		getExistingSchedule(editingScheduleId);

		Log.v(TAG, "Completed");
	}
	
	@Override
    protected void onResume() {
    	super.onResume();
		getExistingSchedule(editingScheduleId);
    }

	public void editAlarm(int id, int startHour, int startMinute, int endHour,
			int endMinute, String mode, String frequency) {
		
		AlarmUtility alarmUtility = new AlarmUtility();
		
		alarmUtility.cancelAlarm(id, this.getApplicationContext());
		
		if (frequencySpinner.getSelectedItem().toString()
				.equalsIgnoreCase("One Time")) {
			alarmUtility.addAlarms(id, startTimePicker.getCurrentHour(),
					startTimePicker.getCurrentMinute(),
					endTimePicker.getCurrentHour(),
					endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()), "x", 
					this.getApplicationContext());
		} else {
			String frequencyDays = constructFrequencyBinaryString();
			alarmUtility.addAlarms(id, startTimePicker.getCurrentHour(),
					startTimePicker.getCurrentMinute(),
					endTimePicker.getCurrentHour(),
					endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()),
					frequencyDays, this.getApplicationContext());
		}
	}

	private String constructFrequencyBinaryString() {
		StringBuffer frequencyDays = null;
		if (frequencySpinner.getSelectedItem().toString()
				.equalsIgnoreCase("Recurring")) {
			frequencyDays = new StringBuffer();
			if (mondayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (tuesdayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (wednesdayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (thursdayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (fridayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (saturdayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
			if (sundayCheckBox.isChecked())
				frequencyDays.append('1');
			else
				frequencyDays.append('0');
		}
		return frequencyDays.toString();
	}

	public void getExistingSchedule(int schedulerId) {
		String querySql = "_id = " + schedulerId;
		SQLiteDatabase db = null;
		Cursor results = null;
		try {
			SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(
					this.getApplicationContext(), null, null, 0);
			db  = dbHelper.getWritableDatabase();
			results = db.query(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME,
					null, querySql, null, null, null, null);
		
		
			String days = null;
	
			if (results != null) {
				results.moveToFirst();
	
				nameText.setText(results.getString(results
						.getColumnIndex(SchedulerDBOpenHelper.SCHEDULE_NAME)));
				modeSpinner.setSelection(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.MODE)));
				startTimePicker.setCurrentHour(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.START_TIME_HR)));
				startTimePicker.setCurrentMinute(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.START_TIME_MIN)));
				endTimePicker.setCurrentHour(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.END_TIME_HR)));
				endTimePicker.setCurrentMinute(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.END_TIME_MIN)));
				frequencySpinner.setSelection(results.getInt(results
						.getColumnIndex(SchedulerDBOpenHelper.FREQUENCY)));
	
				if (frequencySpinner.getSelectedItem().toString()
						.equalsIgnoreCase("Recurring")) {
					days = results.getString(results
							.getColumnIndex(SchedulerDBOpenHelper.REPEAT_DAYS));
	
					if (days != null) {
						mondayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(0))) ? true : false);
						tuesdayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(1))) ? true : false);
						wednesdayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(2))) ? true : false);
						thursdayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(3))) ? true : false);
						fridayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(4))) ? true : false);
						saturdayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(5))) ? true : false);
						sundayCheckBox.setChecked("1".equals(String.valueOf(days
								.charAt(6))) ? true : false);
					}
				}
	
			}
			results.close();
		
			db.close();
		} catch (Exception e) {
			results.close();
			db.close();
		}
	}

	public void updateSchedule(int schedulerId) {
		boolean anyDaySelected = false;
		
		isSuccessfulInsert = true;
		
		String querySql = "_id = " + schedulerId;
		ContentValues content = new ContentValues();
		content.put(SchedulerDBOpenHelper.SCHEDULE_NAME, nameText.getText().toString().trim());
		content.put(SchedulerDBOpenHelper.MODE,
				Long.toString(modeSpinner.getSelectedItemId()));
		content.put(SchedulerDBOpenHelper.START_TIME_HR,
				startTimePicker.getCurrentHour());
		content.put(SchedulerDBOpenHelper.START_TIME_MIN,
				startTimePicker.getCurrentMinute());
		content.put(SchedulerDBOpenHelper.END_TIME_HR,
				endTimePicker.getCurrentHour());
		content.put(SchedulerDBOpenHelper.END_TIME_MIN,
				endTimePicker.getCurrentMinute());
		content.put(SchedulerDBOpenHelper.FREQUENCY,
				Long.toString(frequencySpinner.getSelectedItemId()));
		// content.put(SchedulerDBOpenHelper.REPEAT_DAYS, "");
		content.put(SchedulerDBOpenHelper.IS_ENABLED, true);
		String frequencyDays = null;
		if (frequencySpinner.getSelectedItem().toString()
				.equalsIgnoreCase("Recurring")) {
			frequencyDays = constructFrequencyBinaryString();
			if (frequencyDays.indexOf("1") != -1) {
				anyDaySelected = true;
			}

			content.put(SchedulerDBOpenHelper.REPEAT_DAYS,
					frequencyDays.toString());
		}
		// update alarms 1. Cancel previous alarms associated w/ this scheduleId
		// 2. create new alarms
		
		if (nameText.getText().toString().trim().length() == 0) {
			isSuccessfulInsert = false;
			nameText.setError("Name is required!");
		}
		
		// validation for start-end time. end time should be greater than start
		// time
		if (startTimePicker.getCurrentHour() > endTimePicker.getCurrentHour()) {
			isSuccessfulInsert = false;
			Toast.makeText(this.getApplicationContext(),
					"End time cannot be earlier than start time",
					Toast.LENGTH_SHORT).show();
		}
		if (startTimePicker.getCurrentHour() == endTimePicker.getCurrentHour()) {

			if (startTimePicker.getCurrentMinute() == endTimePicker
					.getCurrentMinute()) {
				isSuccessfulInsert = false;
				Toast.makeText(this.getApplicationContext(), 
						"Start and end time cannot be same", Toast.LENGTH_SHORT).show();
			}
		}
		if (startTimePicker.getCurrentHour() == endTimePicker.getCurrentHour()) {
			if (startTimePicker.getCurrentMinute() > endTimePicker.getCurrentMinute()) {
				isSuccessfulInsert = false;
				Toast.makeText(this.getApplicationContext(),
						"End time cannot be earlier than start time ", Toast.LENGTH_SHORT).show();
			}
		}
		
		//Error handling: one-time alarm�s start time cannot be earlier than current time
		if (anyDaySelected == false) // one-time
		{	
			Calendar calendar = Calendar.getInstance();
			int currentHour = calendar.get(Calendar.HOUR);
			int currentMinute = calendar.get(Calendar.MINUTE);
			int am_pm = calendar.get(Calendar.AM_PM);
			if (am_pm == 1){
				currentHour = currentHour + 12;
			}
			
			if( currentHour > startTimePicker.getCurrentHour()){
				isSuccessfulInsert = false;
				Toast.makeText(this.getApplicationContext(), "One time alarm's start time cannot be earlier than current time", 
						Toast.LENGTH_SHORT).show();
			}
			else if (currentHour == startTimePicker.getCurrentHour() && currentMinute > startTimePicker.getCurrentMinute()){
				isSuccessfulInsert = false;
				Toast.makeText(this.getApplicationContext(), "One time alarm's start time cannot be earlier than current time", 
						Toast.LENGTH_SHORT).show();
			}
		}
				
		if (frequencySpinner.getSelectedItem().toString().equalsIgnoreCase("Recurring") && anyDaySelected==false){
			isSuccessfulInsert = false;
			Toast.makeText(this.getApplicationContext(), "Select at least one day", Toast.LENGTH_SHORT).show();
		}
		if (isSuccessfulInsert) {
			SQLiteDatabase db = null;
			try {
				SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(
						this.getApplicationContext(), null, null, 0);
				db = dbHelper.getWritableDatabase();
				db.update(
						SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, content,
						querySql, null);
				db.close();
			} catch (Exception e) {
				db.close();
			}
			

			if (frequencySpinner.getSelectedItem().toString()
					.equalsIgnoreCase("One Time")) {
				editAlarm(schedulerId, startTimePicker.getCurrentHour(),
						startTimePicker.getCurrentMinute(),
						endTimePicker.getCurrentHour(),
						endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()), "x");
			} else {
				editAlarm(schedulerId, startTimePicker.getCurrentHour(),
						startTimePicker.getCurrentMinute(),
						endTimePicker.getCurrentHour(),
						endTimePicker.getCurrentMinute(), Long.toString(modeSpinner.getSelectedItemId()),
						frequencyDays.toString());
			}
		}

	}

	protected void launchScheduleList() {
		Intent i = new Intent(this, ScheduleListActivity.class);
		startActivity(i);
	}
	
	protected void relaunchSchedulerAppActivity() {
		Intent i = new Intent(this, SchedulerAppActivity.class);
		startActivity(i);
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

	    View v = getCurrentFocus();
	    boolean ret = super.dispatchTouchEvent(event);

	    if (v instanceof EditText) {
	        View w = getCurrentFocus();
	        int scrcoords[] = new int[2];
	        w.getLocationOnScreen(scrcoords);
	        float x = event.getRawX() + w.getLeft() - scrcoords[0];
	        float y = event.getRawY() + w.getTop() - scrcoords[1];

	        Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
	        if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) { 

	            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
	        }
	    }
	return ret;
	}
}