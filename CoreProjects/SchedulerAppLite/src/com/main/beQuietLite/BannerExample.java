package com.main.beQuietLite;
import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.google.ads.*;

public class BannerExample extends Activity {
  private static final String MY_AD_UNIT_ID = "a14f64a9a7cf600";
private AdView adView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    // Create the adView
    adView = new AdView(this, AdSize.BANNER, MY_AD_UNIT_ID);

    // Lookup your LinearLayout assuming it�s been given
    // the attribute android:id="@+id/mainLayout"
    LinearLayout layout = (LinearLayout)findViewById(R.layout.schedule_list);

    // Add the adView to it
    layout.addView(adView);

    // Initiate a generic request to load it with an ad
    adView.loadAd(new AdRequest());
  }

  @Override
  public void onDestroy() {
    if (adView != null) {
      adView.destroy();
    }
    super.onDestroy();
  }
}