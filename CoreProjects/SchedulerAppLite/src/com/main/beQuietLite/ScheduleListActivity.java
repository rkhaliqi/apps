package com.main.beQuietLite;

import java.util.Calendar;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.main.beQuietLite.AlarmUtility;
import com.main.beQuietLite.SchedulerDBOpenHelper;
import com.main.beQuietLite.R;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class ScheduleListActivity extends Activity {
	public static final String TAG = "ScheduleListActivity";
	private Button mAddScheduleButton;
    private ListView mScheduleList;
    private static final String MY_AD_UNIT_ID = "a14f64a9a7cf600";
    private AdView adView;
    private Button buyProButton;
	
	/**
     * Called when the activity is first created. Responsible for initializing the UI.testing
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.v(TAG, "Activity State: onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_list);

        setTitle(R.string.scheduleListTitle);
        
        // Obtain handles to UI objects
        mAddScheduleButton = (Button) findViewById(R.id.addScheduleButton);
        mScheduleList = (ListView) findViewById(R.id.mylist);         
        
        
        // Register handler for UI elements
        mAddScheduleButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "mAddAccountButton clicked");
                
                
                
                SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(v.getContext(), null, null, 0);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                int scheduleCount = getSchedules(db).getCount();
                db.close();
             // check if one schedule already exists
                if ( scheduleCount== 1) {
                	AlertDialog.Builder liteDialog = new AlertDialog.Builder( v.getContext() );
                	liteDialog.setTitle( "Alert" );
                	liteDialog.setMessage( "To be able to have unlimited number of schedules, buy Be Quiet Pro." );
                	liteDialog.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
    		             public void onClick(DialogInterface dialog, int which) {
    		            	 Log.d( "Buy Pro", "Positive" );            	 
    		             }
    	            });
                	liteDialog.show();
                } else {
                	launchScheduleAdder(0);
                }
                
            }
        });
        
        buyProButton = (Button) findViewById(R.id.buyProButton);
        buyProButton.setOnClickListener(new View.OnClickListener(
        		) {
			
			@Override
			public void onClick(View v) {
				Log.d(TAG, "Buy Be Queit Pro button clicked");
                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.main.beQuiet");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
				
			}
		});

        SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(this.getApplicationContext(), null, null, 0);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
      //here we set our adapter
        mScheduleList.setAdapter(new CustomCursorAdapter(this, mScheduleList, getSchedules(db)));
        db.close();
             // Lookup your LinearLayout assuming it�s been given
        // the attribute android:id="@+id/mainLayout"
        LinearLayout layout = (LinearLayout)findViewById(R.id.mainListLayout);
        
     // Create the adView
        adView = new AdView(this, AdSize.BANNER, MY_AD_UNIT_ID);
        
        adView.loadAd(new AdRequest());

//        
//        
//        // Add the adView to it
////        layout.addView(adView);
//
//        // Initiate a generic request to load it with an ad
//        
//        AdRequest adRequest = new AdRequest();
//        adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
//        adView.loadAd(adRequest);

        layout.addView(adView);
        // redraw stuff for refreshing list ... doesn't work yet
//        mScheduleList.invalidateViews();
//        mScheduleList.postInvalidate();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.layout.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Cursor schedulesCursor = null;
    	boolean result;
    	SQLiteDatabase db = null;
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.refresh_schedules:
            	
            	AlarmUtility alarmUtility = new AlarmUtility();
            	SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(this.getApplicationContext(), null, null, 0);
    			db = dbHelper.getWritableDatabase();
            	schedulesCursor = getSchedules(db);
            	    			
    			// add alarms for each existing schedule if it's ENABLED
    			while(!schedulesCursor.isAfterLast()){
    				if(schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.IS_ENABLED)).equalsIgnoreCase("1")){
    							
    					if(schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.FREQUENCY)).equalsIgnoreCase("0")){
    						
    						alarmUtility.cancelAlarm(schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID)), this.getApplicationContext());
    						
    						alarmUtility.addAlarms(schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID)), 
    	            				schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.START_TIME_HR)), 
    	            				schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.START_TIME_MIN)), 
    	            				schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.END_TIME_HR)), 
    	            				schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.END_TIME_MIN)), 
    	            				schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.MODE)),
    	            				"x", this.getApplicationContext());
    	            	}
    	            	// for Recurring
    	            	else if(schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.FREQUENCY)).equalsIgnoreCase("1")){
    	            		
    	            		alarmUtility.cancelAlarm(schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID)), this.getApplicationContext());
    	            		
    	            		alarmUtility.addAlarms(schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID)), 
    	            			schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.START_TIME_HR)), 
    	            			schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.START_TIME_MIN)), 
    	            			schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.END_TIME_HR)), 
    	            			schedulesCursor.getInt(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.END_TIME_MIN)), 
    	            			schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.MODE)), 
    	            			schedulesCursor.getString(schedulesCursor.getColumnIndex(SchedulerDBOpenHelper.REPEAT_DAYS)), this.getApplicationContext());
    	            	}
    				}
    			    schedulesCursor.moveToNext();
    			}
    			relaunchScheduleListActivity();
    			result = true;
            default:
            	result = super.onOptionsItemSelected(item);
        }
        schedulesCursor.close();
        db.close();
        
        return result;
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
        SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(this.getApplicationContext(), null, null, 0);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		//here we set our adapter
		mScheduleList.setAdapter(new CustomCursorAdapter(this, mScheduleList, getSchedules(db)));
		db.close();
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(this.getApplicationContext(), null, null, 0);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
      //here we set our adapter
        mScheduleList.setAdapter(new CustomCursorAdapter(this, mScheduleList, getSchedules(db)));
        db.close();
    }
    

    /**
     * Obtains schedule list 
     *
     * @return A cursor for accessing the schedule list.
     */
    private Cursor getSchedules(SQLiteDatabase db)
    {
    	Cursor results = null;
    	try{
			results = db.query(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, null, null, null, null, null, null);
			results.moveToFirst();
    	} catch(Exception e){
    		e.printStackTrace();
    	}
    	
        return results;
    }

    
    protected void launchScheduleAdder(int id) {
        Intent i = new Intent(this, SchedulerAppActivity.class);
        i.putExtra("itemSelected", String.valueOf(id));
        startActivity(i);
    }
    
    /**
     * Launches the ContactAdder activity to add a new contact to the selected accont.
     */
    protected void relaunchScheduleListActivity() {
        Intent i = new Intent(this, ScheduleListActivity.class);
        startActivity(i);
    }
    
    private class CustomCursorAdapter extends CursorAdapter { 
   	 
	    protected ListView mListView;
	    protected Activity listActivity;
	
	    protected class RowViewHolder {
	        public TextView mScheduleNameTextbox;
	        public CheckBox mEnableCheckbox;
	        public ImageButton mDeleteButton;
	    }
	
	    public CustomCursorAdapter(Activity activity, ListView list, Cursor cursor) {
	        super(activity, cursor);
	        mListView = list;
	        this.listActivity = activity;
	    }
	
	    @Override
	    public void bindView(View view, Context context, Cursor cursor) {
	        String scheduleName = cursor.getString(cursor.getColumnIndex(SchedulerDBOpenHelper.SCHEDULE_NAME));
	        String isEnabled = cursor.getString(cursor.getColumnIndex(SchedulerDBOpenHelper.IS_ENABLED));
	        
	        TextView mScheduleNameTextbox = (TextView) view.findViewById(R.id.scheduleEntryText);
	        if (mScheduleNameTextbox != null) {
	        	String formattedScheduleName = scheduleName;
	        	if (formattedScheduleName.length() > 21) 
	        	{
	        		formattedScheduleName = formattedScheduleName.substring(0, 20) + "...";
	        	}
	        	
	        	mScheduleNameTextbox.setText(formattedScheduleName);
	        }
	        
	        CheckBox isEnabledCheckBox = (CheckBox) view.findViewById(R.id.chekcbox);
	        isEnabledCheckBox.setChecked(isEnabled.equals("1") ? true : false);
	        if (isEnabled.equals("0")) {
	        	mScheduleNameTextbox.setTextColor(Color.GRAY);
	        } else {
	        	mScheduleNameTextbox.setTextColor(Color.WHITE);
	    	}
	        mScheduleNameTextbox.setTextSize(18.0f);        
	    }
	
	    @Override
	    public View newView(Context context, Cursor cursor, ViewGroup parent) {
	        View view = View.inflate(context, R.layout.schedule_entry, null);
	
	        RowViewHolder holder = new RowViewHolder();
	        holder.mScheduleNameTextbox = (TextView) view.findViewById(R.id.scheduleEntryText);
	        holder.mEnableCheckbox = (CheckBox) view.findViewById(R.id.chekcbox);
	        
	        holder.mDeleteButton = (ImageButton) view.findViewById(R.id.deleteButton);
	
	        holder.mScheduleNameTextbox.setOnClickListener(mOnScheduleNameClickListener);
	        holder.mDeleteButton.setOnClickListener(mOnDeleteButtonClickListener);
	        holder.mEnableCheckbox.setOnClickListener(mOnEnableCheckboxClickListener);
		        
	        view.setTag(holder);
	
	        return view;
	    }
	
	    private OnClickListener mOnScheduleNameClickListener = new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	            final int position = mListView.getPositionForView((View) v.getParent());
	            //Toast.makeText(context.getApplicationContext(), "name pos: " + position, Toast.LENGTH_SHORT).show();
	            //Toast.makeText(getApplicationContext(), "rameen", Toast.LENGTH_SHORT).show();
				Cursor listItem = (Cursor) mListView.getItemAtPosition(position);
				Intent i = new Intent(listActivity, SchedulerAppActivity.class);
		        i.putExtra("itemSelected", String.valueOf(listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID))));
		        startActivity(i);

	        }
	    };
	    
	    private OnClickListener mOnDeleteButtonClickListener = new OnClickListener() {
	        @Override
	        public void onClick(final View v) {
	            final int position = mListView.getPositionForView((View) v.getParent());
	            final Cursor listItem = (Cursor) mListView.getItemAtPosition(position);
	            AlertDialog.Builder askDialog = new AlertDialog.Builder( listActivity );
	            askDialog.setTitle( "Alert" );
	            askDialog.setMessage( "Are you sure you want to delete this?" );
	            askDialog.setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
		             public void onClick(DialogInterface dialog, int which) {
		            	 Log.d( "AlertDialog", "Positive" );
		            	 //delete selected schedule from DB
		            	 String querySql = "_id = " + listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID));
		            	 
		            	 // cancel corresponding Alarms
		            	 cancelAlarm(listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID)));
		            	 SQLiteDatabase db = null;
		 	             try {
		 	            	SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(v.getContext(), null, null, 0);
		 		            db = dbHelper.getWritableDatabase();
		 					// delete schedule from DB
			 	            db.delete(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, querySql, null);
		 		            db.close();
		 	             } catch (Exception e) {
		 	             	db.close();
		 	             }
		            	 // delete schedule from DB
		 	             //db.delete(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, querySql, null);
		 	             
		 	             //Reload ScheduleListActivity after delete
		 	             Intent i = new Intent(listActivity, ScheduleListActivity.class);
		                 startActivity(i);
		             }
	            });
	            askDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.d("AlertDialog", "Negative");
						dialog.cancel();
					}
				});
	            askDialog.show();
	        }
	    };
	
	    private OnClickListener mOnEnableCheckboxClickListener = new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	            final int position = mListView.getPositionForView((View) v.getParent());
	            Cursor listItem = (Cursor) mListView.getItemAtPosition(position);
	            
	            CheckBox clickedCheckbox = (CheckBox) v;
	            boolean isEnabled = clickedCheckbox.isChecked();
	            ContentValues content = new ContentValues();
	            content.put(SchedulerDBOpenHelper.IS_ENABLED, isEnabled);
	            String querySql = "_id = " + listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID)); 
	            SQLiteDatabase db = null;
	            try {
		            SchedulerDBOpenHelper dbHelper = new SchedulerDBOpenHelper(v.getContext(), null, null, 0);
					db = dbHelper.getWritableDatabase();
		            db.update(SchedulerDBOpenHelper.SCHEDULE_TABLE_NAME, content, querySql, null);
		            db.close();
	            } catch (Exception e) {
	            	db.close();
	            }
	            
//	            public void addAlarms(int id, int startHour, int startMinute, int endHour,
//	        			int endMinute, String mode, String frequency)
            	int scheduleId = listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID));
            	String freq = listItem.getString(listItem.getColumnIndex(SchedulerDBOpenHelper.FREQUENCY)); 

	            // create alarm
	            if(isEnabled){
//	            	// for one-time
	            	if(freq.equalsIgnoreCase("0")){
	            		addAlarms(scheduleId, 
	            			listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.START_TIME_HR)), 
	            			listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.START_TIME_MIN)), 
	            			listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.END_TIME_HR)), 
	            			listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.END_TIME_MIN)), 
	            			listItem.getString(listItem.getColumnIndex(SchedulerDBOpenHelper.MODE)),
	            			"x");
	            	}
	            	// for Recurring
	            	else if(freq.equalsIgnoreCase("1")){
	            		addAlarms(scheduleId, 
	            			listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.START_TIME_HR)), 
	            			listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.START_TIME_MIN)), 
	            			listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.END_TIME_HR)), 
	            			listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.END_TIME_MIN)), 
	            			listItem.getString(listItem.getColumnIndex(SchedulerDBOpenHelper.MODE)), 
	            			listItem.getString(listItem.getColumnIndex(SchedulerDBOpenHelper.REPEAT_DAYS)));
	            	}
	            }
	            
	            View textbox = ((View) v.getParent()).findViewById(R.id.scheduleEntryText);
	            
	            // cancel alarm
	            if(isEnabled == false){
	            	cancelAlarm(listItem.getInt(listItem.getColumnIndex(SchedulerDBOpenHelper.SCH_COLUMN_ID)));
	            	((TextView) textbox).setTextColor(Color.GRAY);
	            } else {
	            	((TextView) textbox).setTextColor(Color.WHITE);
		    	} 
	        }
	    };
	    
	 // Method used for canceling alarm when user Deletes or Disables a schedule 
	    public void cancelAlarm(int id) {
			// Get the AlarmManager service
			AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
			Intent intent = new Intent(listActivity,
					AlarmReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(listActivity, 8000 + id,
					intent, PendingIntent.FLAG_CANCEL_CURRENT);
			am.cancel(sender);
			PendingIntent sender2 = PendingIntent.getBroadcast(listActivity, 9000 + id,
					intent, PendingIntent.FLAG_CANCEL_CURRENT);
			am.cancel(sender2);

		}
	    
	    public void addAlarms(int id, int startHour, int startMinute, int endHour,
				int endMinute, String mode, String frequencyDays) {
			boolean isStartPm = false;
			int alteredStartTime = startHour;
			if (startHour > 11) {
				alteredStartTime = startHour - 12;
				isStartPm = true;
			}

			// get a Calendar object with current time
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR, alteredStartTime);
			cal.set(Calendar.MINUTE, startMinute);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.AM_PM, isStartPm == true ? Calendar.PM : Calendar.AM);
			Intent intent = new Intent(listActivity,
					AlarmReceiver.class);
			intent.putExtra("alarm_message", "Start:" + frequencyDays + ":" + mode + ":" + id);
			// In reality, you would want to have a static variable for the request
			// code instead of 192837
			PendingIntent sender = PendingIntent.getBroadcast(listActivity, 8000 + id,
					intent, PendingIntent.FLAG_CANCEL_CURRENT);

			// Get the AlarmManager service
			AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
			// am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), sender);
			
			// one-time alarm
			if(frequencyDays.toString().equalsIgnoreCase("x")){
				am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
						sender);
			}
			// recurring alarm
			else {
				am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
						AlarmManager.INTERVAL_DAY, sender);
			}

			boolean isEndPm = false;
			int alteredEndTime = endHour;
			if (endHour > 11) {
				alteredEndTime = endHour - 12;
				isEndPm = true;
			}
			// get a Calendar object with current time
			Calendar cal2 = Calendar.getInstance();
			cal2.set(Calendar.HOUR, alteredEndTime);
			cal2.set(Calendar.MINUTE, endMinute);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.AM_PM, isEndPm == true ? Calendar.PM : Calendar.AM);
			intent = new Intent(listActivity, AlarmReceiver.class);
			intent.putExtra("alarm_message",
					"End:" + frequencyDays + ":" + id);
			// In reality, you would want to have a static variable for the request
			// code instead of 192837
			PendingIntent sender2 = PendingIntent.getBroadcast(listActivity, 9000 + id,
					intent, PendingIntent.FLAG_CANCEL_CURRENT);
			
			if(frequencyDays.toString().equalsIgnoreCase("x")){
				am.set(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
						sender2);
			}
			// recurring alarm
			else {
				am.setRepeating(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
						AlarmManager.INTERVAL_DAY, sender2);
			}
			
		}
	    
    } 
} 

