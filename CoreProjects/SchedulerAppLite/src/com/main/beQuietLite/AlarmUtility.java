package com.main.beQuietLite;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class AlarmUtility {
	//TODO: later on move all alarm related code here so that they are easy to maintain and we're Joe compliant :)

	public void addAlarms(int id, int startHour, int startMinute, int endHour,
			int endMinute, String mode, String frequencyDays, Context context) {
		boolean isStartPm = false;
		int alteredStartTime = startHour;
		if (startHour > 11) {
			alteredStartTime = startHour - 12;
			isStartPm = true;
		}

		// get a Calendar object with current time
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, alteredStartTime);
		cal.set(Calendar.MINUTE, startMinute);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.AM_PM, isStartPm == true ? Calendar.PM : Calendar.AM);
		Intent intent = new Intent(context,
				AlarmReceiver.class);
		intent.putExtra("alarm_message", "Start:" + frequencyDays + ":" + mode + ":" + id);
		// In reality, you would want to have a static variable for the request
		// code instead of 192837
		PendingIntent sender = PendingIntent.getBroadcast(context, 8000 + id,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);

		// Get the AlarmManager service
		AlarmManager am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
		// am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), sender);
		
		// one-time alarm
		if(frequencyDays.toString().equalsIgnoreCase("x")){
			am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
					sender);
		}
		// recurring alarm
		else {
			am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
					AlarmManager.INTERVAL_DAY, sender);
		}

		boolean isEndPm = false;
		int alteredEndTime = endHour;
		if (endHour > 11) {
			alteredEndTime = endHour - 12;
			isEndPm = true;
		}
		// get a Calendar object with current time
		Calendar cal2 = Calendar.getInstance();
		cal2.set(Calendar.HOUR, alteredEndTime);
		cal2.set(Calendar.MINUTE, endMinute);
		cal2.set(Calendar.SECOND, 0);
		cal2.set(Calendar.AM_PM, isEndPm == true ? Calendar.PM : Calendar.AM);
		intent = new Intent(context, AlarmReceiver.class);
		intent.putExtra("alarm_message",
				"End:" + frequencyDays + ":" + id);
		// In reality, you would want to have a static variable for the request
		// code instead of 192837
		PendingIntent sender2 = PendingIntent.getBroadcast(context, 9000 + id,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);
		
		if(frequencyDays.toString().equalsIgnoreCase("x")){
			am.set(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
					sender2);
		}
		// recurring alarm
		else {
			am.setRepeating(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(),
					AlarmManager.INTERVAL_DAY, sender2);
		}
		
	}
	
	public void cancelAlarm(int id, Context context) {
		// Get the AlarmManager service
		AlarmManager am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
		Intent intent = new Intent(context,
				AlarmReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 8000 + id,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);
		am.cancel(sender);
		PendingIntent sender2 = PendingIntent.getBroadcast(context, 9000 + id,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);
		am.cancel(sender2);

	}

}
